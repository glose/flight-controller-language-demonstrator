////////////////
// custom macros to be used in the document
////////////////


:LWB: => image:icons/IconeGemocLanguage_16.png[width=16, height=16, title="in language workbench"]
:MWB: => image:icons/IconeGemocModel_16.png[width=16, height=16, title="in modeling workbench"]
:LWB_in_title: image:icons/IconeGemocLanguage_16.png[width=16, height=16, role=right, title="in language workbench"]
:MWB_in_title: image:icons/IconeGemocModel_16.png[width=16, height=16, role=right, title="in modeling workbench"]

:LANGUAGE_ENGINEER: image:icons/IconeGemocLanguage_16.png[width=12, height=12, title="language engineer"]Language engineer 
:MODELER: image:icons/IconeGemocModel_16.png[width=12, height=12, title="language engineer"] Modeler

:TODO: icon:tags[role="red"]  TODO
