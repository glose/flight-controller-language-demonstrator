package fr.inria.glose.fcl.k3based.addons.input;

import fr.inria.glose.fcl.k3based.addons.Activator;
import net.java.games.input.Component.Identifier;
import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;

public class ControllerInputInstance {
	
	private Controller controller;

	private double x;
	private double y;
	private double rx;
	private double ry;
	private boolean mode;
	private boolean select;
	private boolean trigger;
	private boolean start;
	private boolean pinkie;
	private boolean base;
	private boolean base2;
	private boolean base3;
	private boolean top;
	private boolean top2;
	
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getRX() {
		return rx;
	}

	public double getRY() {
		return ry;
	}
	
	public boolean getMode() {
		return mode;
	}
	
	public boolean getSelect() {
		return select;
	}
	
	public boolean getStart() {
		return start;
	}
	
	public boolean getTrigger() {
		return trigger;
	}
	
	public boolean getPinkie() {
		return pinkie;
	}
	
	public boolean getBase() {
		return base;
	}	
	
	public boolean getBase2() {
		return base2;
	}	
	
	public boolean getBase3() {
		return base3;
	}
	public boolean getTop() {
		return top;
	}
	public boolean getTop2() {
		return top2;
	}

	private boolean continuePolling;

	private boolean debug;

	public ControllerInputInstance(boolean debug) {
		this.debug = debug;
		this.continuePolling = true;
		/* Get the available controllers */
		Controller[] controllers = ControllerEnvironment.getDefaultEnvironment().getControllers();
		for (int i = 0; i < controllers.length; i++) {
			if (controllers[i].getType() == Controller.Type.GAMEPAD) { // Pierre's joystick
				this.controller = controllers[i];
				break;
			}
			if (controllers[i].getType() == Controller.Type.STICK) { // freebox joystick
				this.controller = controllers[i];
				break;
			}
		}
	}
	
	public void startPolling() {
		new Thread(() -> {
			if (this.controller != null) {
				while (this.continuePolling) {
					this.controller.poll();
					
					//this.controller.getComponents();
					this.x = this.controller.getComponent(Identifier.Axis.X).getPollData();
					this.y = this.controller.getComponent(Identifier.Axis.Y).getPollData();
					this.rx = this.controller.getComponent(Identifier.Axis.RX).getPollData();
					if(this.controller.getComponent(Identifier.Axis.RY) != null) {
						this.ry = this.controller.getComponent(Identifier.Axis.RY).getPollData();
					} else {
						this.ry = this.controller.getComponent(Identifier.Axis.RZ).getPollData();
					}
					//this.controller.getComponents();
					// sur la manette free Trigger, Thumb, Thumb 2, Top, Top 2, Pinkie, Base, Base 2, Base 3, Base 4, Base 5, Base 6, x, y, z, rx, rz, pov
//					double pinkie = this.controller.getComponent(Identifier.Button.PINKIE).getPollData(); // haut droit
//					double base = this.controller.getComponent(Identifier.Button.BASE).getPollData(); // bouton bas gauche free
//					double top = this.controller.getComponent(Identifier.Button.TOP).getPollData();
//					double top2 = this.controller.getComponent(Identifier.Button.TOP2).getPollData(); // bouton haut gauche free
//					double base2 = this.controller.getComponent(Identifier.Button.BASE2).getPollData(); // bas droit
//					double base3 = this.controller.getComponent(Identifier.Button.BASE3).getPollData(); // select sur pad free
//					double base4 = this.controller.getComponent(Identifier.Button.BASE4).getPollData();
//					double base5 = this.controller.getComponent(Identifier.Button.BASE5).getPollData();
//					double base6 = this.controller.getComponent(Identifier.Button.BASE6).getPollData();
					if(this.controller.getComponent(Identifier.Button.MODE) != null) {
						// may not exist on some controller
						this.mode = this.controller.getComponent(Identifier.Button.MODE).getPollData() > 0.0;
					}
					if(this.controller.getComponent(Identifier.Button.SELECT) != null) {
						this.select = this.controller.getComponent(Identifier.Button.SELECT).getPollData() > 0.0;
					}
					if(this.controller.getComponent(Identifier.Button.TRIGGER) != null) {
						this.trigger = this.controller.getComponent(Identifier.Button.TRIGGER).getPollData() > 0.0;
					}
					if(this.controller.getComponent(Identifier.Button.START) != null) {
						this.start = this.controller.getComponent(Identifier.Button.START).getPollData() > 0.0;
					}
					if(this.controller.getComponent(Identifier.Button.PINKIE) != null) {
						this.pinkie = this.controller.getComponent(Identifier.Button.PINKIE).getPollData() > 0.0;
					}
					if(this.controller.getComponent(Identifier.Button.BASE) != null) {
						this.base = this.controller.getComponent(Identifier.Button.BASE).getPollData() > 0.0;
					}
					if(this.controller.getComponent(Identifier.Button.BASE2) != null) {
						this.base2 = this.controller.getComponent(Identifier.Button.BASE2).getPollData() > 0.0;
					}
					if(this.controller.getComponent(Identifier.Button.BASE3) != null) {
						this.base3 = this.controller.getComponent(Identifier.Button.BASE3).getPollData() > 0.0;
					}
					if(this.controller.getComponent(Identifier.Button.TOP) != null) {
						this.top = this.controller.getComponent(Identifier.Button.TOP).getPollData() > 0.0;
					}
					if(this.controller.getComponent(Identifier.Button.TOP2) != null) {
						this.top2 = this.controller.getComponent(Identifier.Button.TOP2).getPollData() > 0.0;
					}
					if(debug) System.out.println(this.toString());
					
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						Activator.error(e.getMessage(), e);
					}
				}
			}
		}).start();
	}
	
	public String toString() {
		return "x: " + this.x + " | "
				+ "y: " + this.y + " | "
				+ "rx: " + this.rx + " | "
				+ "ry: " + this.ry + " | "
				+ "mode: " + this.mode+ " | "
				+ "select: " + this.select+ " | "
				+ "trigger: " + this.trigger+ " | "
				+ "pinkie: " + this.pinkie+ " | "
				+ "base: " + this.base+ " | "
				+ "base2: " + this.base2+ " | "
				+ "base3: " + this.base3+ " | "
				+ "top: " + this.top+ " | "
				+ "top2: " + this.top2+ " | "
				+ "start: " + this.start;
	}
	
	public void stopPolling() {
		this.continuePolling = false;
	}
	
	public static void main(String args[]) {
		new ControllerInputInstance(true).startPolling();
	}
	
}
