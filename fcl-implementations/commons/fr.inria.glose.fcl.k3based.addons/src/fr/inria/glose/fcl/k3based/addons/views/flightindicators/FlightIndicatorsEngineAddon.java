package fr.inria.glose.fcl.k3based.addons.views.flightindicators;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Optional;
import java.util.Properties;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.URIUtil;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gemoc.commons.eclipse.core.resources.IFileUtils;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;
import org.eclipse.gemoc.trace.commons.model.trace.Step;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.gemoc.xdsmlframework.api.engine_addon.IEngineAddon;
import org.eclipse.swt.widgets.Display;
import org.osgi.framework.Bundle;

import fr.inria.glose.fcl.k3based.addons.Activator;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FloatValue;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.FunctionPort;
import fr.inria.glose.fcl.model.fcl.StructPropertyValue;
import fr.inria.glose.fcl.model.fcl.StructValue;
import fr.inria.glose.fcl.model.fcl.Value;

public class FlightIndicatorsEngineAddon  implements IEngineAddon {

	// properties file dsl keys
	public static String ALTITUDE_PORT = "altitude_port";
	public static String ROLL_PORT = "roll_port";
	public static String PITCH_PORT = "pitch_port";
	public static String HEADING_PORT = "heading_port";
	public static String VIEW_NAME = "viewName";
	
	public enum Indicator {
		ALT, ROLL, PITCH, HEADING
	}
	
	public static String PNAV = "->";
	
	protected FlightIndicatorInstance flightIndicatorInstance;
	
	
	String altitude_port_accessor;
	String roll_port_accessor;
	String pitch_port_accessor;
	String heading_port_accessor;

	
	protected boolean enabled = false;

	@Override
	public void engineStarted(IExecutionEngine<?> executionEngine) {
		
		// find a *.flightindicators_properties file for the model
		String userDefinedPropertiesLocation = executionEngine.getExecutionContext().getRunConfiguration().getAttribute("fr.inria.glose.fcl.k3fcl.xdsml.flightindicators.addon_stringOption", "");
		Path p;
		if(!userDefinedPropertiesLocation.isEmpty()) {
			p = new Path(userDefinedPropertiesLocation);
		} else {
			Resource res = executionEngine.getExecutionContext().getResourceModel();
			IFile f = EMFResource.getIFile(res);
			p = (Path) f.getLocation().removeFileExtension().addFileExtension("flightindicators_properties");
		}
		try {
			IFile propertiesFile = IFileUtils.getIFileFromWorkspaceOrFileSystem(p);
			if(propertiesFile.exists()) {
				
				// read property file and collect its content in a more convenient structure
				Properties prop = new Properties();	
				try {
					prop.load(propertiesFile.getContents());
					
					this.altitude_port_accessor = prop.getProperty(ALTITUDE_PORT);
					this.roll_port_accessor = prop.getProperty(ROLL_PORT);
					this.pitch_port_accessor = prop.getProperty(PITCH_PORT);
					this.heading_port_accessor = prop.getProperty(HEADING_PORT);
					
					// start the browser
					
					// look for the index.html file
					Bundle bundle = Platform.getBundle("fr.inria.glose.fcl.k3based.addons");
					URL url = FileLocator.find(bundle, new Path("web/jscript_flight_indicators"), null);

					url = FileLocator.toFileURL(url);

					File file = URIUtil.toFile(URIUtil.toURI(url));
					
					flightIndicatorInstance = new FlightIndicatorInstance(prop.getProperty(VIEW_NAME), file.getCanonicalPath()+"/index.html");
					
					enabled = true;
				} catch (IOException e) {
					Activator.warn("failed to load "+propertiesFile.getLocation()+" "+e.getMessage(),e);
				} catch (URISyntaxException e) {
					Activator.warn("failed to load web/jscript_flight_indicators/index.html, " + e.getMessage(), e);
				}
			
			} else {
				Activator.warn("unity properties file " +p +" not found");
			}
		} catch (CoreException e) {
			Activator.warn("unity properties file not found, "+e.getMessage(),e);
		}
	}
	
	@Override
	public void engineAboutToDispose(IExecutionEngine<?> engine) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				flightIndicatorInstance.dispose();
				flightIndicatorInstance = null;
			}
		});
	}

	@Override
	public void stepExecuted(IExecutionEngine<?> engine, Step<?> stepExecuted) {
		
		Resource res = engine.getExecutionContext().getResourceModel();
		if(res.getContents().get(0) instanceof FCLModel) {
			FCLModel fclModel = (FCLModel)res.getContents().get(0);
			// only when the master function is executed
			if(	stepExecuted.getMseoccurrence() != null &&
				stepExecuted.getMseoccurrence().getMse() != null &&
				fclModel.equals(stepExecuted.getMseoccurrence().getMse().getCaller())){
				//stepExecuted.getMseoccurrence().getMse().getAction().getName()
				
				setIndicator(fclModel.getDataflow().getFunctionPorts(), Indicator.ALT, this.altitude_port_accessor);
				setIndicator(fclModel.getDataflow().getFunctionPorts(), Indicator.HEADING, this.heading_port_accessor);
				setIndicator(fclModel.getDataflow().getFunctionPorts(), Indicator.ROLL, this.roll_port_accessor);
				setIndicator(fclModel.getDataflow().getFunctionPorts(), Indicator.PITCH, this.pitch_port_accessor);
				
				// update the view
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						flightIndicatorInstance.sendVariablesBufferToBrowser();
					}
				});
			}	
		}
	}
	
	protected void setIndicator(EList<FunctionPort> ports, Indicator indicator, String dataAccessor) {
		// Find port
		String functionPortName = dataAccessor.split(PNAV)[0];
		Optional<FunctionBlockDataPort> port = ports.stream()
				.filter(p -> p.getName().equals(functionPortName))
				.map(obj -> (FunctionBlockDataPort) obj)
				.findAny();
		if(port.isPresent()) { 
			// Get FloatValue
			FloatValue fv = null;
			if(dataAccessor.contains(PNAV)) {
				// TODO deal with more than one level of properties
				String functionPortPropName = dataAccessor.split(PNAV)[1];
				Value v =FunctionBlockDataPortAspect.currentValue(port.get());
				if(v instanceof StructValue) {
					StructValue sv = (StructValue) v;
					Optional<StructPropertyValue> vp = sv.getStructProperties().stream().filter(p -> p.getName().equals(functionPortPropName))
							.findAny();
					if(vp.isPresent()) {
						fv = ValueAspect.toFloatValue(vp.get().getValue());
					}
				}
			} else {
				fv = ValueAspect.toFloatValue(FunctionBlockDataPortAspect.currentValue(port.get()));
			}
			// Apply Value to indicator
			if(fv!= null) {
				switch (indicator) {
				case HEADING:
					flightIndicatorInstance.setHeading(fv.getFloatValue());
					break;
				case ALT:
					flightIndicatorInstance.setAltitude(fv.getFloatValue());
					break;
				case PITCH:
					flightIndicatorInstance.setPitch(fv.getFloatValue());
					break;
				case ROLL:
					flightIndicatorInstance.setRoll(fv.getFloatValue());
					break;
				default:
					break;
				}
			}
		}
		
	}
}
