package fr.inria.glose.fcl.k3based.addons.input;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gemoc.commons.eclipse.core.resources.IFileUtils;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;
import org.eclipse.gemoc.executionframework.engine.core.CommandExecution;
import org.eclipse.gemoc.trace.commons.model.trace.Step;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.gemoc.xdsmlframework.api.engine_addon.IEngineAddon;

import fr.inria.glose.fcl.k3based.addons.Activator;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EventAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect;
import fr.inria.glose.fcl.model.fcl.DirectionKind;
import fr.inria.glose.fcl.model.fcl.Event;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.Function;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;

public class ControllerInputAddon implements IEngineAddon {

	public static String BUTTON_PORTNAME_PROP = ".button.port";
	public static String BUTTON_PORT_VALUES_PROP = ".button.switchValue";
	public static String BUTTON_EVENT_PROP = ".button.sendEvent";

	public ControllerInputAddon() {

	}

	IExecutionEngine<?> _currentEngine;
	ControllerInputInstance inputInstance;
	boolean modeChanged;

	boolean debug = false;
	// private Frame frame;

	String x_portName;
	String y_portName;
	String rx_portName;
	String ry_portName;

	Range xRange;
	Range yRange;
	Range rxRange;
	Range ryRange;

	HashMap<String, ButtonProxy> buttons = new HashMap<String, ButtonProxy>();

	class Range {
		double min;
		double med;
		double max;

		public Range(double min, double med, double max) {
			super();
			this.min = min;
			this.med = med;
			this.max = max;
		}
	}

	class ButtonProxy {
		boolean isSent = false;
		String portName;
		ArrayList<String> possibleValues = new ArrayList<String>();
		Event eventToSend;

		int currentValueIndex = 0;

		protected boolean hasValueToSend;
		protected boolean hasEventToSend;

		public ButtonProxy() {

		}

		public void setPort(String portName, ArrayList<String> possibleValues) {
			this.portName = portName;
			for (String possibleVal : possibleValues) {
				this.possibleValues.add(possibleVal.trim());
			}
			hasValueToSend = (portName != null && !portName.isEmpty() && !possibleValues.isEmpty());
		}

		public String nextValue() {
			String v = possibleValues.get(currentValueIndex);
			this.currentValueIndex = (currentValueIndex + 1) % possibleValues.size();
			return v;
		}

		public Event getEventToSend() {
			return eventToSend;
		}

		public void setEventToSend(Event eventToSend) {
			this.eventToSend = eventToSend;
			hasEventToSend = eventToSend != null;
		}

	}

	@Override
	public void engineAboutToStart(IExecutionEngine<?> engine) {
		IEngineAddon.super.engineAboutToStart(engine);

		loadProperties(engine);

		modeChanged = false;

		if (_currentEngine != null) {
			// ignore double call
			return;
		}
		_currentEngine =  engine;

		inputInstance = new ControllerInputInstance(debug);
		inputInstance.startPolling();

	}

	protected void loadProperties(IExecutionEngine<?> engine) {
		// find a *.flightindicators_properties file for the model
		String userDefinedPropertiesLocation = engine.getExecutionContext().getRunConfiguration()
				.getAttribute("fr.inria.glose.fcl.k3fcl.xdsml.controllerinput.addon_stringOption", "");
		Path p;
		if (!userDefinedPropertiesLocation.isEmpty()) {
			p = new Path(userDefinedPropertiesLocation);
		} else {
			Resource res = engine.getExecutionContext().getResourceModel();
			IFile f = EMFResource.getIFile(res);
			p = (Path) f.getLocation().removeFileExtension().addFileExtension("controllerinput_properties");
		}
		try {
			IFile propertiesFile = IFileUtils.getIFileFromWorkspaceOrFileSystem(p);
			if (propertiesFile.exists()) {

				// read property file and collect its content in a more convenient structure
				Properties prop = new Properties();
				try {
					prop.load(propertiesFile.getContents());

					if (prop.getProperty("debug") != null) {
						this.debug = prop.getProperty("debug").equalsIgnoreCase("true");
					}

					this.x_portName = prop.getProperty("x.port");
					this.y_portName = prop.getProperty("y.port");
					this.rx_portName = prop.getProperty("rx.port");
					this.ry_portName = prop.getProperty("ry.port");

					this.xRange = getRangeForAxis(prop, "x");
					this.yRange = getRangeForAxis(prop, "y");
					this.rxRange = getRangeForAxis(prop, "rx");
					this.ryRange = getRangeForAxis(prop, "ry");

					Set<String> buttonNames = new HashSet<String>();
					for (Object k : prop.keySet()) {
						String s = (String) k;
						if (s.endsWith(BUTTON_PORTNAME_PROP)) {
							buttonNames.add(s.substring(0, s.indexOf(BUTTON_PORTNAME_PROP)));
						} else if (s.endsWith(BUTTON_EVENT_PROP)) {
							buttonNames.add(s.substring(0, s.indexOf(BUTTON_EVENT_PROP)));
						}
					}

					for (String buttonName : buttonNames) {
						ButtonProxy button = new ButtonProxy();
						buttons.put(buttonName, button);
						if (prop.getProperty(buttonName + BUTTON_PORTNAME_PROP) != null
								&& prop.getProperty(buttonName + BUTTON_PORT_VALUES_PROP) != null) {
							button.setPort(prop.getProperty(buttonName + BUTTON_PORTNAME_PROP), new ArrayList<String>(
									Arrays.asList(prop.getProperty(buttonName + BUTTON_PORT_VALUES_PROP).split(","))));
						}
						if (prop.getProperty(buttonName + BUTTON_EVENT_PROP) != null) {
							Resource res = engine.getExecutionContext().getResourceModel();
							if (res.getContents().get(0) instanceof FCLModel) {
								FCLModel fclModel = (FCLModel) res.getContents().get(0);
								String eventName = prop.getProperty(buttonName + BUTTON_EVENT_PROP);
								button.setEventToSend(fclModel.getEvents().stream()
										.filter(e -> e.getName().equals(eventName)).findFirst().get());
							}
						}
					}

				} catch (IOException e) {
					Activator.warn("failed to load " + propertiesFile.getLocation() + " " + e.getMessage(), e);
				}

			} else {
				Activator.warn("controllerinput_properties file " + p + " not found");
			}
		} catch (CoreException e) {
			Activator.warn("controllerinput_properties file not found, " + e.getMessage(), e);
		}
	}

	@Override
	public void engineAboutToStop(IExecutionEngine<?> engine) {
		inputInstance.stopPolling();
	}

	protected Range getRangeForAxis(Properties prop, String axis) {
		String rangeString = prop.getProperty(axis + ".range");
		String[] rangeStrings = rangeString.split(",");

		// TODO deal with float

		return new Range(Integer.parseInt(rangeStrings[0].trim()), Integer.parseInt(rangeStrings[1].trim()),
				Integer.parseInt(rangeStrings[2].trim()));

	}

	@Override
	public void aboutToExecuteStep(IExecutionEngine<?> engine, Step<?> stepToExecute) {
		EObject caller = stepToExecute.getMseoccurrence().getMse().getCaller();

		if (caller instanceof Function) {
			Function functionCaller = (Function) caller;
			if (functionCaller.eContainer() instanceof FCLModel) {
				List<FunctionBlockDataPort> ports = functionCaller.getFunctionPorts().stream()
						.filter(p -> (p.getDirection() == DirectionKind.IN || p.getDirection() == DirectionKind.IN_OUT)
								&& p instanceof FunctionBlockDataPort)
						.map(p -> (FunctionBlockDataPort) p).collect(Collectors.toList());
				for (FunctionBlockDataPort port : ports) {
					TransactionalEditingDomain ed = TransactionUtil.getEditingDomain(port.eResource());
					RecordingCommand command = new RecordingCommand(ed, "") {
						protected void doExecute() {
							try {
								if (port.getName().equals(y_portName)) {
									FunctionBlockDataPortAspect.setValueFromString(port,
											getIntegerValueForRange(-inputInstance.getY(), yRange));
								} else if (port.getName().equals(x_portName)) {
									FunctionBlockDataPortAspect.setValueFromString(port,
											getIntegerValueForRange(inputInstance.getX(), xRange));
								} else if (port.getName().equals(ry_portName)) {
									FunctionBlockDataPortAspect.setValueFromString(port,
											getIntegerValueForRange(inputInstance.getRY(), ryRange));
								} else if (port.getName().equals(rx_portName)) {
									FunctionBlockDataPortAspect.setValueFromString(port,
											getIntegerValueForRange(inputInstance.getRX(), rxRange));
								}

							} catch (Exception t) {
								Activator.error(t.getMessage(), t);
							}
						}
					};
					CommandExecution.execute(ed, command);
				}
				for (String buttonName : buttons.keySet()) {
					ButtonProxy button = buttons.get(buttonName);
					boolean buttonIsPressed = false;
					switch (buttonName) {
					case "TRIGGER":
						buttonIsPressed = inputInstance.getTrigger();
						break;
					case "MODE":
						buttonIsPressed = inputInstance.getMode();
						break;
					case "START":
						buttonIsPressed = inputInstance.getStart();
						break;
					case "SELECT":
						buttonIsPressed = inputInstance.getSelect();
						break;
					case "PINKIE":
						buttonIsPressed = inputInstance.getPinkie();
						break;
					case "BASE":
						buttonIsPressed = inputInstance.getBase();
						break;
					case "BASE2":
						buttonIsPressed = inputInstance.getBase2();
						break;
					case "BASE3":
						buttonIsPressed = inputInstance.getBase3();
						break;
					case "TOP":
						buttonIsPressed = inputInstance.getTop();
						break;
					case "TOP2":
						buttonIsPressed = inputInstance.getTop2();
						break;
					default:
						break;
					}
					if (buttonIsPressed && !button.isSent) {
						if (button.hasValueToSend) {
							Optional<FunctionBlockDataPort> port = functionCaller.getFunctionPorts().stream()
									.filter(p -> (p.getDirection() == DirectionKind.IN
											|| p.getDirection() == DirectionKind.IN_OUT)
											&& p instanceof FunctionBlockDataPort
											&& p.getName().equals(button.portName))
									.map(p -> (FunctionBlockDataPort) p).findAny();
							if (port.isPresent()) {
								TransactionalEditingDomain ed = TransactionUtil.getEditingDomain(port.get().eResource());
								if (ed != null) {
									RecordingCommand command = new RecordingCommand(ed, "") {
										protected void doExecute() {
											try {
												FunctionBlockDataPortAspect.setValueFromString(port.get(), button.nextValue());
											} catch (Exception t) {
												Activator.error(t.getMessage(), t);
											}
										}
									};
									CommandExecution.execute(ed, command);
								} else {
									FunctionBlockDataPortAspect.setValueFromString(port.get(), button.nextValue());
								}
							}
						}
						if (button.hasEventToSend) {
							Event event = button.getEventToSend();
							TransactionalEditingDomain ed = TransactionUtil.getEditingDomain(event.eResource());
							if (ed != null) {
								RecordingCommand command = new RecordingCommand(ed, "") {
									protected void doExecute() {
										try {
											EventAspect.send(event);
										} catch (Exception t) {
											Activator.error(t.getMessage(), t);
										}
									}
								};
								CommandExecution.execute(ed, command);
							} else {
								EventAspect.send(event);
							}
						}
						button.isSent = true;
					}
					if (!buttonIsPressed && button.isSent) {
						button.isSent = false;
					}
				}
			}
		}

	}

	protected String getIntegerValueForRange(double originalValue, Range range) {
		double newVal = originalValue;
		if (originalValue < 0) {
			double newRange = (range.med - range.min);
			newVal = (((originalValue + 1.0)) * newRange) + range.min;
		} else {
			double newRange = (range.max - range.med);
			newVal = (originalValue * newRange) + range.med;
		}

		return "" + (int) newVal;
	}

	public IExecutionEngine<?> getEngine() {
		return _currentEngine;
	}

}
