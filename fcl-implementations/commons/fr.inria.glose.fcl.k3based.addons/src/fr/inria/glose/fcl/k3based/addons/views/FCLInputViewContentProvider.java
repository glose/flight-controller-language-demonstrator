package fr.inria.glose.fcl.k3based.addons.views;

import java.util.ArrayList;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import fr.inria.glose.fcl.model.fcl.DirectionKind;
import fr.inria.glose.fcl.model.fcl.FCLModel;

public class FCLInputViewContentProvider implements IStructuredContentProvider 
{
	
	public void inputChanged(Viewer v, Object oldInput, Object newInput) {}

	public void dispose() {}

	public Object[] getElements(Object parent) 
	{
		if(parent instanceof FCLModel)
		{
			FCLModel cache = (FCLModel) parent;
			ArrayList<EObject> list = new ArrayList<EObject>();
			list.addAll(cache.getDataflow().getFunctionPorts().stream()
					.filter(p -> p.getDirection() != DirectionKind.OUT)
					.collect(Collectors.toList()));
			list.addAll(cache.getEvents());
			return list.toArray();
		}
		return new Object[0];
	}

}
