package fr.inria.glose.fcl.k3based.addons.views.oscilloscup;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JFrame;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gemoc.trace.commons.model.trace.Step;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.gemoc.xdsmlframework.api.engine_addon.IEngineAddon;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import fr.inria.glose.fcl.k3based.addons.Activator;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.DataStructProperty;
import fr.inria.glose.fcl.model.fcl.DataStructType;
import fr.inria.glose.fcl.model.fcl.DirectionKind;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FloatValueType;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.FunctionVarDecl;
import fr.inria.glose.fcl.model.fcl.Value;
import oscilloscup.data.rendering.DataElementRenderer;
import oscilloscup.data.rendering.figure.ConnectedLineFigureRenderer;
import oscilloscup.multiscup.Clock;
import oscilloscup.multiscup.Multiscope;
import oscilloscup.multiscup.Property;
import oscilloscup.multiscup.PropertyValueFormatter;


public class FCLOutputOscilloscupViewAddon implements IEngineAddon {

	public FCLOutputOscilloscupViewAddon() {
		
	}

	IExecutionEngine<?> _currentEngine;
	FunctionVarDecl globalClock;
	private Frame frame;
	private Multiscope<ComplexFunctionBlockDataPort> multiscope;
	

	class FCLGlobalClock extends Clock
	{
		@Override
		public double getTime() {
			if(globalClock != null) {
				Value v = FunctionVarDeclAspect.currentValue(globalClock);
				if(v != null) {
					try {
						return (double) ValueAspect.rawValue(v);
					} catch (Exception e) {
						Activator.error(e.getMessage(), e);
					}
				}
			}
			
			return 0;
		}

		@Override
		public String getTimeUnit() {
			return "s";
		}
		
	}
	
	
	@Override
	public void engineAboutToStart(IExecutionEngine<?> engine) {
		IEngineAddon.super.engineAboutToStart(engine);
		
		if(_currentEngine != null ) {
			// ignore double call
			return;
		}
		_currentEngine = engine;
		
		// grab the frame from the view or fallback to a JFrame (ie. new window)
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				IViewPart v = null;
				try{
					v = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(OscilloscupView.ID);
					frame = ((OscilloscupView)v).frame;
					frame.removeAll();
				} catch(PartInitException e){
					Activator.warn(e.getMessage()+ "; falling back to JFrame", e);
					frame = new JFrame();
				}
				
			}
		});
		frame.setSize(800, 600);
		
		ArrayList<Property<ComplexFunctionBlockDataPort>> allProperties = new ArrayList<Property<ComplexFunctionBlockDataPort>>();
		
		_currentEngine = engine;
		if(_currentEngine != null && _currentEngine.getExecutionContext() != null) {
			Resource res = _currentEngine.getExecutionContext().getResourceModel();
			if(res.getContents().get(0) instanceof FCLModel) {
				FCLModel fclModel = (FCLModel)res.getContents().get(0);
				
				if(fclModel.getDataflow().getTimeReference() != null) {
					globalClock = fclModel.getDataflow().getTimeReference().getObservableVar();
				}
				
				List<FunctionBlockDataPort> ports = fclModel.getDataflow().getFunctionPorts().stream()
					.filter(p -> p.getDirection() == DirectionKind.OUT && p instanceof FunctionBlockDataPort)
					.map(obj -> (FunctionBlockDataPort) obj)
					.collect(Collectors.toList());
				
				List<ComplexFunctionBlockDataPort> cports = new ArrayList<>();
				for(FunctionBlockDataPort p : ports) {
					if(p.getDataType() instanceof DataStructType) {
						// get all first level float properties of the port
						DataStructType t = (DataStructType) p.getDataType();
						for(DataStructProperty prop : t.getProperties()) {
							if(prop.getType() instanceof FloatValueType) {
								// port->property
								cports.add(new ComplexFunctionBlockDataPort(p, prop.getName()));
							}
						}
					} else if(p.getDataType() instanceof FloatValueType) {
						// simple float type port
						cports.add(new ComplexFunctionBlockDataPort(p, null));
					}
				}
				
				PortOscilloProperty inputProp = new PortOscilloProperty();
				inputProp.setName("currentValue");
				inputProp.setReformatter(new PropertyValueFormatter.PrettyDecimals(3));
//				inputProp.setPlotBounds(0d, null);
				inputProp.setClock(new FCLGlobalClock());
				allProperties.add(inputProp);
				multiscope = new Multiscope<ComplexFunctionBlockDataPort>(allProperties){
					/**
					 * 
					 */
					private static final long serialVersionUID = -485104137536535369L;
					@Override
					protected String getRowNameFor(ComplexFunctionBlockDataPort i)
					{
						
						if(i.dataStrucPropertyAccessor != null && !i.dataStrucPropertyAccessor.isEmpty()) {
							return i.dataPort.getName() + "->" +i.dataStrucPropertyAccessor;
						} else {
							return i.dataPort.getName();
						}
					}
					@Override
					protected int getNbPointsInSlidingWindow(ComplexFunctionBlockDataPort row,
							Property<ComplexFunctionBlockDataPort> p)
					{
						return 1000;
					}
					@Override
					protected DataElementRenderer getSpecificRenderer(ComplexFunctionBlockDataPort row,
							Property<ComplexFunctionBlockDataPort> property) {
						return new ConnectedLineFigureRenderer();
					}
					
				};
				multiscope.setRows(cports);
				multiscope.setRefreshPeriodMs(-1);
				if(frame instanceof JFrame) {
					// this is the fallback JFrame,
					((JFrame)frame).setContentPane(multiscope);
				} else {
					frame.add(multiscope);
				}
				frame.setVisible(true);
				
			}
		}
	}




	@Override
	public void engineAboutToDispose(IExecutionEngine<?> engine) {
		IEngineAddon.super.engineAboutToDispose(engine);
		if(frame instanceof JFrame) {
			// this is the fallback JFrame, close it
			frame.dispose();
		}
		this._currentEngine = null;
		globalClock = null;
	}




	@Override
	public void stepExecuted(IExecutionEngine<?> engine, Step<?> stepExecuted) {
		IEngineAddon.super.stepExecuted(engine, stepExecuted);
		Resource res = engine.getExecutionContext().getResourceModel();
		if(res.getContents().get(0) instanceof FCLModel) {
			FCLModel fclModel = (FCLModel)res.getContents().get(0);
			// only when the master function is executed
			if( stepExecuted.getMseoccurrence() != null &&
				stepExecuted.getMseoccurrence().getMse() != null &&
				fclModel.equals(stepExecuted.getMseoccurrence().getMse().getCaller())){
				if(stepExecuted.getMseoccurrence().getMse().getAction().getName()
						.equals("evaluateModeAutomata")) {
					multiscope.newStep();
				}
			}
		}
		
	}
	
	public IExecutionEngine<?> getEngine() 
	{
		return _currentEngine;
	}

}
