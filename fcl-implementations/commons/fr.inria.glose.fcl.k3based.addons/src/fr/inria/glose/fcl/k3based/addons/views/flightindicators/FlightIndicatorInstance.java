package fr.inria.glose.fcl.k3based.addons.views.flightindicators;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

public class FlightIndicatorInstance {
	Shell shell;
	String name;
	String browserURL;
	
	// current values
	int currentAltitude = 0; // in feet
	int currentRoll = 0;	// in degree
	int currentPitch = 0;	// in degree
	int currentHeading = 0;	// in degree
	
	private Browser browser;
	public FlightIndicatorInstance(String name, String browserURL ) {
		this.name = name;
		this.browserURL = browserURL;
		//Thread t = new Thread() {
		Display.getDefault().syncExec(new Runnable() {
		    //public void run() {
		        // do any work that updates the screen ...
		        // remember to check if the widget
		        // still exists
		        // might happen if the part was closed
		   
			@Override
			public void run() {
				//display = new Display();
				Display display = PlatformUI.getWorkbench().getDisplay();
				shell = new Shell(display);
				shell.setText("Filght indicators for "+name);
				shell.setSize(630, 225);
				shell.setLayout(new FillLayout());
				
				browser = new Browser(shell, SWT.NONE);
				browser.setUrl(browserURL);
				
				shell.open();
			}
		});
	}
	
	public void setRoll(double roll) {
		this.currentRoll = (int)roll;
		//this.browser.execute("$.flightIndicator('#attitude', 'attitude', {roll:"+(int)roll+", size:200, showBox : true});");
	}
	public void setPitch(double pitch) {
		this.currentPitch = (int)pitch;
		//this.browser.execute("$.flightIndicator('#attitude', 'attitude', {pitch:"+(int)pitch+", size:200, showBox : true});");
	}
	public void setHeading(double heading) {
		this.currentHeading = (int)heading;
		//this.browser.execute("$.flightIndicator('#heading', 'heading', {heading:"+(int)heading+", showBox: true});");	
	}
	/**
	 * set the altitude
	 * @param altitude
	 */
	public void setAltitude(double altitude) {
		this.currentAltitude = (int)(altitude*10);
		//this.browser.execute("$.flightIndicator('#altimeter', 'altimeter', {altitude:"+(int)altitude+", showBox: true});");	
	}
	
	/**
	 * use a cache to send the update in one javascript command
	 */
	public void sendVariablesBufferToBrowser() {
		
		this.browser.execute("window.attitude.setRoll("+(int)currentRoll+");\n"+
				"window.attitude.setPitch("+(int)currentPitch+");\n"+
				"window.heading.setHeading("+(int)currentHeading+");\n"+
				"window.altimeter.setAltitude("+(int)currentAltitude+");");
	}
	
	public void dispose() {
		shell.dispose();
	}
}
