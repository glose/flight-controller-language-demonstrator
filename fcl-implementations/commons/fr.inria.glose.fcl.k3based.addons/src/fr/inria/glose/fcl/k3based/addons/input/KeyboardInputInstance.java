package fr.inria.glose.fcl.k3based.addons.input;

import net.java.games.input.Component.Identifier.Key;
import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;
import net.java.games.input.Keyboard;

public class KeyboardInputInstance {
	
	private Keyboard kb;

	public boolean isZ() {
		return z;
	}

	public boolean isQ() {
		return q;
	}

	public boolean isS() {
		return s;
	}

	public boolean isD() {
		return d;
	}

	public boolean isUp() {
		return up;
	}

	public boolean isLeft() {
		return left;
	}

	public boolean isDown() {
		return down;
	}

	public boolean isRight() {
		return right;
	}
	
	public boolean isTab() {
		return tab;
	}

	private boolean z;
	private boolean q;
	private boolean s;
	private boolean d;
	private boolean up;
	private boolean left;
	private boolean down;
	private boolean right;
	private boolean tab;
	
	private boolean continuePolling;

	public KeyboardInputInstance() {
		this.continuePolling = true;
		/* Get the available controllers */
		Controller[] controllers = ControllerEnvironment.getDefaultEnvironment().getControllers();
		for (int i = 0; i < controllers.length; i++) {
			if (controllers[i].getType() == Controller.Type.KEYBOARD) {
				this.kb = (Keyboard) controllers[i];
				if (this.kb.getName().contains("keyboard")) {
					break;
				}
			}
		}
	}
	
	public void startPolling() {
		new Thread(() -> {
			if (this.kb != null) {
				while (this.continuePolling) {
					this.kb.poll();
					this.z = this.kb.isKeyDown(Key.W);
					this.q = this.kb.isKeyDown(Key.A);
					this.s = this.kb.isKeyDown(Key.S);
					this.d = this.kb.isKeyDown(Key.D);
					this.up = this.kb.isKeyDown(Key.UP);
					this.left = this.kb.isKeyDown(Key.LEFT);
					this.down = this.kb.isKeyDown(Key.DOWN);
					this.right = this.kb.isKeyDown(Key.RIGHT);
					this.tab = this.kb.isKeyDown(Key.TAB);
					
				//	System.out.println(this.toString());
					
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
	
	public String toString() {
		return "z: " + this.z + " | "
				+ "q: " + this.q + " | "
				+ "s: " + this.s + " | "
				+ "d: " + this.d + " | "
				+ "up: " + this.up + " | "
				+ "left: " + this.left + " | "
				+ "down: " + this.down + " | "
				+ "right: " + this.right + " | "
				+ "tab: " + this.tab;
	}
	
	public void stopPolling() {
		this.continuePolling = false;
	}
	
	public static void main(String args[]) {
		new KeyboardInputInstance().startPolling();
	}
	
}
