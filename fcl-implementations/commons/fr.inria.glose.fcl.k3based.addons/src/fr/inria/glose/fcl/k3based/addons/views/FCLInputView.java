package fr.inria.glose.fcl.k3based.addons.views;


import javax.inject.Inject;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gemoc.executionframework.engine.core.CommandExecution;
//import org.eclipse.gemoc.executionframework.debugger.MutableField;
import org.eclipse.gemoc.executionframework.ui.views.engine.EngineSelectionDependentViewPart;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DecorationOverlayIcon;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PlatformUI;

import fr.inria.glose.fcl.k3based.addons.Activator;
import fr.inria.glose.fcl.k3based.addons.views.dialogs.NewValueDialog;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EventAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.DirectionKind;
import fr.inria.glose.fcl.model.fcl.Event;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.NamedElement;
import fr.inria.glose.fcl.model.fcl.Value;


/**
 * This sample class demonstrates how to plug-in a new
 * workbench view. The view shows data obtained from the
 * model. The sample creates a dummy model on the fly,
 * but a real implementation would connect to the model
 * available either in this or another plug-in (e.g. the workspace).
 * The view is connected to the model using a content provider.
 * <p>
 * The view uses a label provider to define how model
 * objects should be presented in the view. Each
 * view can present the same model objects using
 * different labels and icons, if needed. Alternatively,
 * a single label provider can be shared between views
 * in order to ensure that objects of the same type are
 * presented in the same way everywhere.
 * <p>
 */

public class FCLInputView extends EngineSelectionDependentViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "fr.inria.glose.fcl.k3based.addons.views.FCLInputView";

	@Inject IWorkbench workbench;
	
	private TableViewer viewer;
	private IExecutionEngine<?> _currentSelectedEngine;
	private Action action1;
	private Action recordAction;
	private Action doubleClickAction;
	
	private boolean recording = false;
	 

	@Override
	public void createPartControl(Composite parent) {
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		// make lines and header visible
		final Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		createColumn();
		
		viewer.setContentProvider(new FCLInputViewContentProvider());
		//viewer.setInput(new String[] { "One", "Two", "Three" });
		viewer.setInput(null);
		//viewer.setLabelProvider(new ViewLabelProvider());
		getSite().setSelectionProvider(viewer);
		makeActions();
		hookContextMenu();
		hookDoubleClickAction();
		contributeToActionBars();
	}

	/**
	 * Create the column(s) of the tableViewer
	 */
	private void createColumn()
	{	
		TableViewerColumn viewerColumn1 = new TableViewerColumn(viewer, SWT.LEFT);
		TableColumn column1 = viewerColumn1.getColumn();
		column1.setText("Port/Event");
		column1.setWidth(100);
		column1.setResizable(true);
		column1.setMoveable(true);
		ColumnLabelProvider column1LabelProvider = new ColumnLabelProvider() 
		{
			@Override
			public String getText(Object element) 
			{
				String result = new String();          
				if (element instanceof NamedElement)
				{
					result = ((NamedElement)element).getName() ;
				} else {
					return element.toString();
				}
				return result;
			}

			@Override
			public Image getImage(Object element) 
			{
				if (element instanceof FunctionBlockDataPort) 
				{
					DirectionKind direction = ((FunctionBlockDataPort) element).getDirection();
					switch(direction)
					{
						case IN: return SharedIcons.getSharedImage(SharedIcons.IN_PORT);
						case OUT: return SharedIcons.getSharedImage(SharedIcons.OUT_PORT);
						case IN_OUT: return SharedIcons.getSharedImage(SharedIcons.INOUT_PORT);
						default: break;
					}
				} else if (element instanceof Event) {
					return SharedIcons.getSharedImage(SharedIcons.EVENT);
				}
				return null;
			}
		};
		viewerColumn1.setLabelProvider(column1LabelProvider);
		
		TableViewerColumn viewerColumn2 = new TableViewerColumn(viewer, SWT.LEFT);
		TableColumn column2 = viewerColumn2.getColumn();
		column2.setText("Value");
		column2.setWidth(100);
		column2.setResizable(true);
		column2.setMoveable(true);
		ColumnLabelProvider column2LabelProvider = new ColumnLabelProvider() 
		{
			
			
			@Override
			public String getText(Object element) 
			{
				String result = new String();          
				if (element instanceof FunctionBlockDataPort)
				{
					result = getPortValueString((FunctionBlockDataPort) element);
					
				} else if (element instanceof Event) {
					return "";
				} else {
					return element.toString();
				}
				return result;
			}

			@Override
			public Image getImage(Object element) 
			{
				return null;
			}
		};
		viewerColumn2.setLabelProvider(column2LabelProvider);
		
	}
	
	private String getPortValueString(FunctionBlockDataPort port) {
		Value v = FunctionBlockDataPortAspect.currentValue(port);
		if(v != null) {
			return ValueAspect.valueToString(v);
		}
		return "";
	}
	
	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				FCLInputView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		
		//manager.add(action1);
		//manager.add(new Separator());
		//manager.add(action2);
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(action1);
		// Other plug-ins can contribute their actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}
	
	private void fillLocalToolBar(IToolBarManager manager) {
		//manager.add(action1);
		//manager.add(action2);
		manager.add(recordAction);
	}

	private void makeActions() {
		action1 = new Action() {
			public void run() {
				IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
				if(selection.getFirstElement() instanceof FunctionBlockDataPort) {
					FunctionBlockDataPort port = (FunctionBlockDataPort)selection.getFirstElement();
					String portName = port.getName();
					String portValue = getPortValueString(port);
					
					NewValueDialog dialog = new NewValueDialog(viewer.getControl().getShell(), portName, portValue);
					dialog.create();
					if (dialog.open() == Window.OK) {
						String newValString = dialog.getValueString();
						TransactionalEditingDomain ed = TransactionUtil.getEditingDomain(port.eResource());
						if(ed!= null) {
							RecordingCommand command = new RecordingCommand(ed, "") {
								protected void doExecute() {
									try {
										FunctionBlockDataPortAspect.setValueFromString(port, newValString);
									} catch (Exception t) {
										Activator.error(t.getMessage(), t);
									}
								}
							};
							CommandExecution.execute(ed, command);
						} else {
							FunctionBlockDataPortAspect.setValueFromString(port, newValString);
						}
						updateView();
					}
				} else if(selection.getFirstElement() instanceof Event) {
					Event event = (Event) selection.getFirstElement();
					TransactionalEditingDomain ed = TransactionUtil.getEditingDomain(event.eResource());
					if(ed!= null) {
						RecordingCommand command = new RecordingCommand(ed, "") {
							protected void doExecute() {
								try {
									EventAspect.send(event);
								} catch (Exception t) {
									Activator.error(t.getMessage(), t);
								}
							}
						};
						CommandExecution.execute(ed, command);
					} else {
						EventAspect.send(event);
					}
				}
			}
			
		};
		action1.setText("Send");
		action1.setToolTipText("Set a new value on port or send Event occurence");
		action1.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
			getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));
		
		
		
		/* doubleClickAction = new Action() {
			public void run() {
				IStructuredSelection selection = viewer.getStructuredSelection();
				Object obj = selection.getFirstElement();
				showMessage("Double-click detected on "+obj.toString());
			}
		}; */
		doubleClickAction = action1;
		
		
		recordAction = new Action() {
			public void run() {
				recording = !recording;
				if(recording) {
					recordAction.setImageDescriptor(new DecorationOverlayIcon(SharedIcons.SAVE,
						SharedIcons.OVR_RECORDING, IDecoration.BOTTOM_RIGHT));
				} else {
					recordAction.setImageDescriptor(SharedIcons.SAVE);
				}
			}
		};
		
		recordAction.setImageDescriptor(SharedIcons.SAVE);
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}
	protected void showMessage(String message) {
		MessageDialog.openInformation(
			viewer.getControl().getShell(),
			"FCL External Ports View",
			message);
	}

	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}
	
	
	/**
	 * Refresh the input of the ContentProvider with the selected strategy
	 */
	public void updateView()
	{
		Display.getDefault().asyncExec(new Runnable() 
		{
			@Override
			public void run() 
			{
				if(getEngine() != null && getEngine().getExecutionContext() != null) {
					Resource res = getEngine().getExecutionContext().getResourceModel();
					if(res.getContents().get(0) instanceof FCLModel) {
						viewer.setInput(res.getContents().get(0));
					} else {
						viewer.setInput(null);										
					}
				} else {
					viewer.setInput(null);										
				}
			}				
		});
	}
	
	public void disposeEngine()
	{
		_currentSelectedEngine = null;
	}
	
	/**
	 * Listen the engine selection in the enginesStatusView
	 */
	@Override
	public void engineSelectionChanged(IExecutionEngine<?> engine) {
		if (engine != null) 
		{
			_currentSelectedEngine = engine;
			updateView();
		}
		else
		{
			viewer.setInput(null);
		}
	}
	
	public IExecutionEngine<?> getEngine() 
	{
		return _currentSelectedEngine;
	}

	public boolean isRecording() {
		return recording;
	}

	public void setRecording(boolean recording) {
		this.recording = recording;
		if(recording) {
			recordAction.setImageDescriptor(new DecorationOverlayIcon(SharedIcons.SAVE,
				SharedIcons.OVR_RECORDING, IDecoration.BOTTOM_RIGHT));
		} else {
			recordAction.setImageDescriptor(SharedIcons.SAVE);
		}
	}

	

	
}
