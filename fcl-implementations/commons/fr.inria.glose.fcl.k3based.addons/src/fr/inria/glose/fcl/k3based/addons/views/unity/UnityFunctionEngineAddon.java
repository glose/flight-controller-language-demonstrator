package fr.inria.glose.fcl.k3based.addons.views.unity;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gemoc.trace.commons.model.trace.Step;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.gemoc.xdsmlframework.api.engine_addon.IEngineAddon;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.widgets.Display;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspect;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.UnityFunction;
import fr.inria.glose.fcl.model.unity.UnityInstance;

public class UnityFunctionEngineAddon  implements IEngineAddon {

	
	ArrayList<UnityInstance> unityInstances = new ArrayList<UnityInstance>();
	IExecutionEngine<?> _currentEngine;
	private Browser browser;
	private Frame frame;
	
	
	@Override
	public void engineAboutToDispose(IExecutionEngine<?> engine) {
		IEngineAddon.super.engineAboutToDispose(engine);
		
		Resource res = engine.getExecutionContext().getResourceModel();
		if(res.getContents().get(0) instanceof FCLModel) {
			FCLModel fclModel = (FCLModel)res.getContents().get(0);
			
			
			List<UnityFunction> unityFunctions = fclModel.allFunctions().stream()
				.filter(f -> f instanceof UnityFunction)
				.map(obj -> (UnityFunction) obj)
				.collect(Collectors.toList());
			
			// for each UnityFunction in FCL get the instance and update the variables using the buffer
			for (UnityFunction unityFunction : unityFunctions) {
				final UnityInstance unityInstance = UnityFunctionAspect.unityWrapper(unityFunction).getUnityInstance();
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						unityInstance.dispose();
					}
				});
			}
		}
		
		_currentEngine = null;
		
	}

	
	
	@Override
	public void stepExecuted(IExecutionEngine<?> engine, Step<?> stepExecuted) {
		
		Resource res = engine.getExecutionContext().getResourceModel();
		if(res.getContents().get(0) instanceof FCLModel) {
			FCLModel fclModel = (FCLModel)res.getContents().get(0);
			
			
			List<UnityFunction> unityFunctions = fclModel.allFunctions().stream()
				.filter(f -> f instanceof UnityFunction)
				.map(obj -> (UnityFunction) obj)
				.collect(Collectors.toList());
			
			// for each UnityFunction in FCL get the instance and update the variables using the buffer
			for (UnityFunction unityFunction : unityFunctions) {
				final UnityInstance unityInstance = UnityFunctionAspect.unityWrapper(unityFunction).getUnityInstance();

				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						unityInstance.sendVariablesBufferToBrowser();
					}
				});
			}
		}
	}



	@Override
	public void engineAboutToStart(IExecutionEngine<?> executionEngine) {
		
		
		Resource res = executionEngine.getExecutionContext().getResourceModel();
		if(res.getContents().get(0) instanceof FCLModel) {
			FCLModel fclModel = (FCLModel)res.getContents().get(0);
			
			
			List<UnityFunction> unityFunctions = fclModel.allFunctions().stream()
				.filter(f -> f instanceof UnityFunction)
				.map(obj -> (UnityFunction) obj)
				.collect(Collectors.toList());
			
			// for each UnityFunction in FCL create an instance
			for (UnityFunction unityFunction : unityFunctions) {
				//unityFunction.getUnityWebPagePath();
				// grab the frame from the view or fallback to a JFrame (ie. new window)
		/*		Display.getDefault().syncExec(new Runnable() {
					@Override
					public void run() {
						IViewPart v = null;
						try{
							v = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(UnityView.ID);
							Composite parent = ((UnityView)v).composite;
							Shell myShell = new Shell (parent.getShell(), SWT.EMBEDDED);
							myShell.setLayout (new FillLayout()); 
							//Browser mybrowser = new Browser (myShell, SWT.NONE);
							browser = new Browser(myShell, SWT.MULTI);
							browser.setUrl(unityFunction.getUnityWebPagePath());
							browser.update();
							parent.update();
							// TODO deal with multiple unity function in a given model ...
							
						} catch(PartInitException e){
							Activator.warn(e.getMessage()+ "; falling back to new window ", e);
							Display display = PlatformUI.getWorkbench().getDisplay();
							Shell shell = new Shell(display);
							shell.setText("Unity Visualization for "+unityFunction.getName());
							shell.setSize(800, 600);
							shell.setLayout(new FillLayout());
							
							browser = new Browser(shell,  SWT.NONE);
							browser.setUrl(unityFunction.getUnityWebPagePath());
							
							shell.open();
						}
						
					}
				});
				*/
			}
		}
		
		
	}

	public IExecutionEngine<?> getEngine() 
	{
		return _currentEngine;
	}

}
