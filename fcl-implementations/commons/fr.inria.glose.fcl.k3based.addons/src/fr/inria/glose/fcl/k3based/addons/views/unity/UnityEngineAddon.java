package fr.inria.glose.fcl.k3based.addons.views.unity;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gemoc.commons.eclipse.core.resources.IFileUtils;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;
import org.eclipse.gemoc.trace.commons.model.trace.Step;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.gemoc.xdsmlframework.api.engine_addon.IEngineAddon;
import org.eclipse.swt.widgets.Display;

import fr.inria.glose.fcl.k3based.addons.Activator;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FloatValue;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.StructPropertyValue;
import fr.inria.glose.fcl.model.fcl.StructValue;
import fr.inria.glose.fcl.model.fcl.Value;
import fr.inria.glose.fcl.model.unity.UnityInstance;

public class UnityEngineAddon  implements IEngineAddon {

	public static String NODE_NAME = ".port.NodeName";
	public static String VAR_NAME = ".port.VarName";
	public static String VIEW_NAME = "unityViewName";
	public static String WEB_PAGE_PATH = "unityWebPagePath";
	
	protected UnityInstance unityInstance;
	
	class UnityVar {
		public String nodeName = "";
		public String varName = "";
		public UnityVar(String nodeName, String varName) {
			super();
			this.nodeName = nodeName;
			this.varName = varName;
		}
	}
	HashMap<String, UnityVar> unityVarMap =  new HashMap<>();
	protected boolean enabled = false;

	@Override
	public void engineStarted(IExecutionEngine<?> executionEngine) {
		
		// find a *.unity_properties file for the model
		String userDefinedPropertiesLocation = executionEngine.getExecutionContext().getRunConfiguration().getAttribute("fr.inria.glose.fcl.k3fcl.xdsml.unity.addon_stringOption", "");
		Path p;
		if(!userDefinedPropertiesLocation.isEmpty()) {
			p = new Path(userDefinedPropertiesLocation);
		} else {
			// find a *.unity_properties file using the model file as base name
			Resource res = executionEngine.getExecutionContext().getResourceModel();
			IFile f = EMFResource.getIFile(res);
			p = (Path) f.getLocation().removeFileExtension().addFileExtension("unity_properties");
		}
		try {
			IFile propertiesFile = IFileUtils.getIFileFromWorkspaceOrFileSystem(p);
			if(propertiesFile.exists()) {
				
				// read property file and collect its content in a more convenient structure
				Properties prop = new Properties();	
				try {
					prop.load(propertiesFile.getContents());
					Set<String> portNames = new HashSet<String>();
					for(Object k : prop.keySet()) {
						String s = (String)k;
						if(s.endsWith(VAR_NAME)) {
							portNames.add(s.substring(0, s.indexOf(VAR_NAME)));
						}
					}
					
					for(String portName : portNames) {
						unityVarMap.put(portName, new UnityVar(prop.getProperty(portName+NODE_NAME), prop.getProperty(portName+VAR_NAME)));
					}

					// start the unity browser
					IFile webPageIFile = IFileUtils.getIFileFromWorkspaceOrFileSystem(prop.getProperty(WEB_PAGE_PATH));
					unityInstance = new UnityInstance(prop.getProperty(VIEW_NAME), webPageIFile.getLocation().toString());
					Thread.sleep(2000);
					enabled = true;
				} catch (IOException e) {
					Activator.warn("failed to load "+propertiesFile.getLocation()+" "+e.getMessage(),e);
				} catch (InterruptedException e) {
					Activator.warn(e.getMessage(),e);
				}
			
			} else {
				Activator.warn("unity properties file " +p +" not found");
			}
		} catch (CoreException e) {
			Activator.warn("unity properties file not found, "+e.getMessage(),e);
		}
	}
	
	@Override
	public void engineAboutToDispose(IExecutionEngine<?> engine) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				if(unityInstance != null) {
					unityInstance.dispose();
					unityInstance = null;
				}
			}
		});
	}

	@Override
	public void stepExecuted(IExecutionEngine<?> engine, Step<?> stepExecuted) {
		
		Resource res = engine.getExecutionContext().getResourceModel();
		if(res.getContents().get(0) instanceof FCLModel) {
			FCLModel fclModel = (FCLModel)res.getContents().get(0);


			// only when the master function is executed
			if(		stepExecuted.getMseoccurrence() != null &&
					stepExecuted.getMseoccurrence().getMse() != null &&
					fclModel.equals(stepExecuted.getMseoccurrence().getMse().getCaller())){
				//stepExecuted.getMseoccurrence().getMse().getAction().getName()
				
				// grab all relevant ports of the master function and send to the view
				
				for(Entry<String, UnityVar> unityEntry : unityVarMap.entrySet()) {
					if(unityEntry.getKey().contains("->")){
						// this is a property in the port
						String functionPortName = unityEntry.getKey().split("->")[0];
						String functionPortPropName = unityEntry.getKey().split("->")[1];
						Optional<FunctionBlockDataPort> port = fclModel.getDataflow().getFunctionPorts().stream()
								.filter(p -> p.getName().equals(functionPortName))
								.map(obj -> (FunctionBlockDataPort) obj)
								.findAny();
						if(port.isPresent()) {
							Value v =FunctionBlockDataPortAspect.currentValue(port.get());
							if(v instanceof StructValue) {
								StructValue sv = (StructValue) v;
								Optional<StructPropertyValue> vp = sv.getStructProperties().stream().filter(p -> p.getName().equals(functionPortPropName))
										.findAny();
								if(vp.isPresent()) {
									FloatValue fv = ValueAspect.toFloatValue(vp.get().getValue());
									if(fv!= null) {
										unityInstance.setVar(unityEntry.getValue().nodeName, unityEntry.getValue().varName, 
												fv.getFloatValue());
									}
								}
							}
						}
						
					} else {
						// this is a primitive port
						// find the functionPort and grab its value
						Optional<FunctionBlockDataPort> port = fclModel.getDataflow().getFunctionPorts().stream()
							.filter(p -> unityVarMap.containsKey(unityEntry.getKey()))
							.map(obj -> (FunctionBlockDataPort) obj)
							.findAny();
						if(port.isPresent()) {
							FloatValue v = ValueAspect.toFloatValue(FunctionBlockDataPortAspect.currentValue(port.get()));
							if(v!= null) {
								unityInstance.setVar(unityEntry.getValue().nodeName, unityEntry.getValue().varName, 
										v.getFloatValue());
							}
						}
					}
				}
				
				// update the view
				// but limit the refresh rate
				long elapsedTimeSinceLastRefresh = System.currentTimeMillis() - lastRefresh;
				if(elapsedTimeSinceLastRefresh >= intervalBetweenRefresh) {
					// trigger a notification now
					if(refreshTimer != null) {
						refreshTimer.cancel();
						refreshTimer= null;
					}
					lastRefresh = System.currentTimeMillis();
					// use the latestTaskExecutor here too in order to run in another thread 
					latestTaskExecutor.execute(new Runnable() {
						@Override
						public void run() {
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									unityInstance.sendVariablesBufferToBrowser();
								}
							});
						}
					});
				}  else {
					
					if(refreshTimer != null) {
						// if a timer is already pending , ignore notification	
						// System.err.println("ignoring sirius refresh due to already pending refresh");
					} else {
						// no timer pending, trigger notification after a delay using timer
						refreshTimer = new Timer("SiriusNotificationTimer");
						TimerTask task = new TimerTask() {
					        public void run() {
					        	lastRefresh = System.currentTimeMillis();
					        	refreshTimer = null;
					        	Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										unityInstance.sendVariablesBufferToBrowser();
									}
								});
							//	System.err.println("sirius animation refresh after delay");
								this.cancel();
					        }
					    };
					    try {
					    	refreshTimer.schedule(task, intervalBetweenRefresh - elapsedTimeSinceLastRefresh);
					    } catch (Exception e) {
					    	refreshTimer = null;
						}
					}
				}
				
			}	
		}
	}
	
	// cf. https://stackoverflow.com/questions/11306425/executor-queue-process-last-known-task-only
	Executor latestTaskExecutor = new ThreadPoolExecutor(1, 1, // Single threaded 
	        30L, TimeUnit.SECONDS, // Keep alive, not really important here
	        new ArrayBlockingQueue<>(1), // Single element queue
	        new ThreadPoolExecutor.DiscardOldestPolicy()); // When new work is submitted discard oldest
	protected long lastRefresh = System.currentTimeMillis();
	public int intervalBetweenRefresh = 150;
	protected Timer refreshTimer;

}
