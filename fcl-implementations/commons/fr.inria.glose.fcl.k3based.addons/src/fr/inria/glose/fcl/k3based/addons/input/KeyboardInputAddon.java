package fr.inria.glose.fcl.k3based.addons.input;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gemoc.commons.eclipse.core.resources.IFileUtils;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;
import org.eclipse.gemoc.trace.commons.model.trace.Step;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.gemoc.xdsmlframework.api.engine_addon.IEngineAddon;

import fr.inria.glose.fcl.k3based.addons.Activator;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.DirectionKind;
import fr.inria.glose.fcl.model.fcl.EnumerationLiteral;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.Function;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.Value;


public class KeyboardInputAddon implements IEngineAddon {

	public KeyboardInputAddon() {
		
	}

	IExecutionEngine<?> _currentEngine;
	KeyboardInputInstance inputInstance;
	boolean modeChanged;
	//private Frame frame;
	
	
	@Override
	public void engineAboutToStart(IExecutionEngine<?> engine) {
		IEngineAddon.super.engineAboutToStart(engine);
		
		modeChanged = false;
		
		if(_currentEngine != null ) {
			// ignore double call
			return;
		}
		_currentEngine = engine;
		
		
		String userDefinedPropertiesLocation = engine.getExecutionContext().getRunConfiguration().getAttribute("fr.inria.glose.fcl.k3fcl.xdsml.keyboardinput.addon_stringOption", "");
		Path p;
		if(!userDefinedPropertiesLocation.isEmpty()) {
			p = new Path(userDefinedPropertiesLocation);
		} else {
			Resource res = engine.getExecutionContext().getResourceModel();
			IFile f = EMFResource.getIFile(res);
			p = (Path) f.getLocation().removeFileExtension().addFileExtension("keyboardinput_properties");
		}
		try {
			IFile propertiesFile = IFileUtils.getIFileFromWorkspaceOrFileSystem(p);
			if(propertiesFile.exists()) {
				
				// read property file and collect its content in a more convenient structure
				Properties prop = new Properties();	
				try {
					prop.load(propertiesFile.getContents());
					
					
					
				} catch (IOException e) {
					Activator.warn("failed to load "+propertiesFile.getLocation()+" "+e.getMessage(),e);
				}
			
			} else {
				Activator.warn("unity properties file " +p +" not found");
			}
		} catch (CoreException e) {
			Activator.warn("unity properties file not found, "+e.getMessage(),e);
		}
		

		
		inputInstance = new KeyboardInputInstance();
		inputInstance.startPolling();

	}


	
	
	@Override
	public void engineAboutToStop(IExecutionEngine<?> engine) {
		inputInstance.stopPolling();
	}

	@Override
	public void aboutToExecuteStep(IExecutionEngine<?> engine, Step<?> stepToExecute) {
		EObject caller = stepToExecute.getMseoccurrence().getMse().getCaller();
		
		if (caller instanceof Function) {
			Function functionCaller = (Function) caller;
			if (functionCaller.eContainer() instanceof FCLModel) {
				List<FunctionBlockDataPort> ports = functionCaller.getFunctionPorts().stream()
						.filter(p -> p.getDirection() == DirectionKind.IN || p.getDirection() == DirectionKind.IN_OUT
								|| p instanceof FunctionBlockDataPort)
						.map(p -> (FunctionBlockDataPort) p)
						.collect(Collectors.toList());
				for (FunctionBlockDataPort port : ports) {
					if (inputInstance.isTab() && !modeChanged && port.getName().equals("flightModeDirective")) {
						modeChanged = true;
						Value v = FunctionBlockDataPortAspect.currentValue(port);
						EnumerationLiteral mode = (EnumerationLiteral) ValueAspect.rawValue(v);
						if (mode.getName().equals("STABILIZED")) {
							FunctionBlockDataPortAspect.setValueFromString(port, "ACROBATIC");
						} else if (mode.getName().equals("ACROBATIC")) {
							FunctionBlockDataPortAspect.setValueFromString(port, "STABILIZED");
						} 
					} else if (modeChanged && !inputInstance.isTab()) {
						modeChanged = false;
					} 
					if (port.getName().equals("joystickLeftVertical")) {
						if (inputInstance.isZ()) {
							FunctionBlockDataPortAspect.setValueFromString(port, "100");
						} else if (inputInstance.isS()) {
							FunctionBlockDataPortAspect.setValueFromString(port, "-100");
						} else {
							FunctionBlockDataPortAspect.setValueFromString(port, "0");
						}
					} else if (port.getName().equals("joystickLeftHorizontal")) {
						if (inputInstance.isQ()) {
							FunctionBlockDataPortAspect.setValueFromString(port, "-100");
						} else if (inputInstance.isD()) {
							FunctionBlockDataPortAspect.setValueFromString(port, "100");
						} else {
							FunctionBlockDataPortAspect.setValueFromString(port, "0");
						}
					} else if (port.getName().equals("joystickRightVertical")) {
						if (inputInstance.isUp()) {
							FunctionBlockDataPortAspect.setValueFromString(port, "100");
						} else if (inputInstance.isDown()) {
							FunctionBlockDataPortAspect.setValueFromString(port, "-100");
						} else {
							FunctionBlockDataPortAspect.setValueFromString(port, "0");
						}
					} else if (port.getName().equals("joystickRightHorizontal")) {
						if (inputInstance.isLeft()) {
							FunctionBlockDataPortAspect.setValueFromString(port, "-100");
						} else if (inputInstance.isRight()) {
							FunctionBlockDataPortAspect.setValueFromString(port, "100");
						} else {
							FunctionBlockDataPortAspect.setValueFromString(port, "0");
						}
					}
				}
			}
		}
		
	}
	
	public IExecutionEngine<?> getEngine() 
	{
		return _currentEngine;
	}

}
