package fr.inria.glose.fcl.k3based.addons.views.oscilloscup;

import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;

public class ComplexFunctionBlockDataPort {

	FunctionBlockDataPort dataPort;
	String dataStrucPropertyAccessor;
	
	public ComplexFunctionBlockDataPort(FunctionBlockDataPort dataPort, String dataStrucPropertyAccessor) {
		super();
		this.dataPort = dataPort;
		this.dataStrucPropertyAccessor = dataStrucPropertyAccessor;
	}
	
}
