package fr.inria.glose.fcl.k3based.addons.views.oscilloscup;

import java.util.Optional;

import fr.inria.glose.fcl.k3based.addons.Activator;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.FloatValue;
import fr.inria.glose.fcl.model.fcl.StructPropertyValue;
import fr.inria.glose.fcl.model.fcl.StructValue;
import fr.inria.glose.fcl.model.fcl.Value;
import oscilloscup.multiscup.Property;

class PortOscilloProperty extends Property<ComplexFunctionBlockDataPort>{

	@Override
	public Object getRawValue(ComplexFunctionBlockDataPort target) {
		
		Value v = FunctionBlockDataPortAspect.currentValue(target.dataPort);
		if(v != null) {
			try {
				if(target.dataStrucPropertyAccessor == null || target.dataStrucPropertyAccessor.isEmpty())
					return (double) ValueAspect.rawValue(v);
				else {
					// deal with accessor to the property
					// TODO deal with more than one level of datastruct properties
					if(v instanceof StructValue) {
						StructValue sv = (StructValue) v;
						Optional<StructPropertyValue> vp = sv.getStructProperties().stream().filter(p -> p.getName().equals(target.dataStrucPropertyAccessor))
								.findAny();
						if(vp.isPresent()) {
							FloatValue fv = ValueAspect.toFloatValue(vp.get().getValue());
							return (double) ValueAspect.rawValue(fv);
						}
					}
				}
			} catch (Exception e) {
				Activator.error(e.getMessage(), e);
			}
		}
		return null;
	}
	
}
