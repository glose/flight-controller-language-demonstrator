package fr.inria.glose.fcl.k3based.addons.views;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gemoc.commons.eclipse.emf.EMFResource;
import org.eclipse.gemoc.trace.commons.model.trace.Step;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.gemoc.xdsmlframework.api.engine_addon.IEngineAddon;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

import fr.inria.glose.fcl.k3based.addons.Activator;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.DirectionKind;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.Value;



public class FCLInputViewAddon implements IEngineAddon {

	public FCLInputViewAddon() {
		
	}


	List<FunctionBlockDataPort> inputPorts;
	
	
	@Override
	public void engineAboutToStart(IExecutionEngine<?> engine) {
		Display.getDefault().asyncExec(new Runnable() 
		{
			@Override
			public void run() 
			{
				try {
					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			
					page.showView(FCLInputView.ID);
				
				} catch(Exception e) {
					Activator.error(e.getMessage(),e);
				}
			}				
		});
		Resource res = engine.getExecutionContext().getResourceModel();
		FCLModel fclModel = (FCLModel)res.getContents().get(0);
		inputPorts = fclModel.getDataflow().getFunctionPorts().stream()
				.filter( port -> port.getDirection() != DirectionKind.OUT
							&& port instanceof FunctionBlockDataPort)
				.map(obj -> (FunctionBlockDataPort) obj)
				.collect(Collectors.toList());
	}




	@Override
	public void aboutToExecuteStep(IExecutionEngine<?> engine, Step<?> stepToExecute) {
		IEngineAddon.super.aboutToExecuteStep(engine, stepToExecute);
		
		
		long elapsedTimeSinceLastNotif = System.currentTimeMillis() - lastRefresh;
		if(elapsedTimeSinceLastNotif >= intervalBetweenRefresh) {
			// trigger a notification now
			if(refreshTimer != null) {
				refreshTimer.cancel();
			}
			lastRefresh = System.currentTimeMillis();
			refreshView();
		} else {
			
			if(refreshTimer != null) {
				// if a timer is already pending , ignore notification	
				// System.err.println("ignoring FCLInputView refresh due to already pending refresh");
			} else {
				// no timer pending, trigger notification after a delay using timer
				refreshTimer = new Timer("SiriusNotificationTimer");
				TimerTask task = new TimerTask() {
			        public void run() {
			        	lastRefresh = System.currentTimeMillis();
			        	refreshTimer = null;
			        	refreshView();
						//System.err.println("FCLInputView refresh after delay");
						this.cancel();
			        }
			    };
			    refreshTimer.schedule(task, intervalBetweenRefresh - elapsedTimeSinceLastNotif);
			}
		}
		
		manageInputRecord(engine, stepToExecute);
		
	}
		
	private void refreshView() {
		Display.getDefault().asyncExec(new Runnable() 
		{
			@Override
			public void run() 
			{
				try {
					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			
					IViewPart viewPart =page.findView(FCLInputView.ID);
					//page.isPartVisible(viewPart);
					//IViewPart viewPart = page.showView(FCLInputView.ID);
				
					if(viewPart != null && viewPart instanceof FCLInputView) {
						((FCLInputView)viewPart).updateView();
					} 
				} catch(Exception e) {
					Activator.error(e.getMessage(),e);
				}
			}				
		});
	}
	
	
	
	boolean isRecording = false;
	

	public void manageInputRecord(IExecutionEngine<?> engine, Step<?> stepToExecute) {
		IEngineAddon.super.stepExecuted(engine, stepToExecute);
		// deal with the the recording
		// only on master 
		
		try {
			Resource res = engine.getExecutionContext().getResourceModel();
			if(res.getContents().get(0) instanceof FCLModel) {
				FCLModel fclModel = (FCLModel)res.getContents().get(0);
				// only when the master function is executed
				if(	stepToExecute.getMseoccurrence() != null &&
					stepToExecute.getMseoccurrence().getMse() != null &&
					fclModel.equals(stepToExecute.getMseoccurrence().getMse().getCaller())){
					if(stepToExecute.getMseoccurrence().getMse().getAction().getName()
							.equals("evaluateModeAutomata")) {
						Display.getDefault().syncExec(new Runnable() 
						{
							@Override
							public void run() 
							{
								try {
									IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
									IViewPart viewPart =page.findView(FCLInputView.ID);
									FCLInputViewAddon.this.isRecording = viewPart != null && viewPart instanceof FCLInputView && ((FCLInputView)viewPart).isRecording();
								} catch(Exception e) {
									Activator.error(e.getMessage(),e);
								}
							}
										
						});
						if(isRecording) {
							appendStepRecord(res);
						}
					}
				}
			}
		} catch(Exception e) {
			Activator.error(e.getMessage(),e);
		}
	}



	
	public void appendStepRecord(Resource res) throws CoreException, IOException {
		IFile f = EMFResource.getIFile(res);
		Path path = (Path) f.getFullPath().removeFileExtension().addFileExtension("inputrecord.csv");
		f =  ResourcesPlugin.getWorkspace().getRoot()
				.getFile(path);
		FCLModel fclModel = (FCLModel)res.getContents().get(0);
		StringBuilder sb= new StringBuilder();
		if(!f.exists()) {
			// create csv header collect(Collectors.joining(","))
			sb.append("# ");
			sb.append(inputPorts.stream().map(port -> port.getName()).collect(Collectors.joining(",")));
			
			sb.append(",[<received events>]\n# Recorded before "+fclModel.getName()+".evaluateModeAutomata() call\n");
			//IFileUtils.writeInFile(f, sb.toString(), null);
			try( InputStream stream =  new ByteArrayInputStream(sb.toString().getBytes(("UTF-8")))){
				f.create(stream, IResource.NONE, null);
			}
		}
		sb= new StringBuilder();
		sb.append(inputPorts.stream().map(port -> 
			 	{	Value v = FunctionBlockDataPortAspect.currentValue(port);
			 		if(v != null) return ValueAspect.valueToString(v);
			 		else return null;
				}
			).collect(Collectors.joining(",")));
		
		sb.append(",[");
		sb.append(FCLModelAspect.receivedEvents(fclModel).stream().map(evtOccurence -> evtOccurence.getEvent().getName()).collect(Collectors.joining(",")));
		sb.append("]\n");
		
		try( InputStream stream =  new ByteArrayInputStream(sb.toString().getBytes(("UTF-8")))){
			f.appendContents(stream, IResource.NONE, null);
		}
	}


	@Override
	public void engineAboutToStop(IExecutionEngine<?> engine) {
		refreshView();
		Display.getDefault().asyncExec(new Runnable() 
		{
			@Override
			public void run() 
			{
				try {
					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			
					IViewPart viewPart =page.findView(FCLInputView.ID);
					//IViewPart viewPart = page.showView(FCLInputView.ID);
				
					if(viewPart != null && viewPart instanceof FCLInputView) {
						((FCLInputView)viewPart).setRecording(false);
					} 
				} catch(Exception e) {
					Activator.error(e.getMessage(),e);
				}
			}				
		});
	}



	protected long lastRefresh = System.currentTimeMillis();
	public int intervalBetweenRefresh = 1000;
	protected Timer refreshTimer;
	
	@Override
	public void engineAboutToDispose(IExecutionEngine<?> engine) {
		IEngineAddon.super.engineAboutToDispose(engine);
		
		Display.getDefault().asyncExec(new Runnable() 
		{
			@Override
			public void run() 
			{
				try {
					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			
					IViewPart viewPart =page.findView(FCLInputView.ID);
					//IViewPart viewPart = page.showView(FCLInputView.ID);
				
					if(viewPart != null && viewPart instanceof FCLInputView) {
						((FCLInputView)viewPart).disposeEngine();
					} 
				} catch(Exception e) {
					Activator.error(e.getMessage(),e);
				}
			}				
		});
	}

}
