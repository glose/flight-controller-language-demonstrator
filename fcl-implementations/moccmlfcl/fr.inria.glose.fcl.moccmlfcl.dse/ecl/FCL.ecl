import 'platform:/resource/fr.inria.glose.fcl.model/model/fcl.ecore'
import _'http://www.eclipse.org/emf/2002/Ecore'

 
ECLimport "platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/kernel.ccslLib"
ECLimport "platform:/plugin/fr.inria.aoste.timesquare.ccslkernel.model/ccsllibrary/CCSL.ccslLib"

ECLimport	"platform:/resource/fr.inria.glose.fcl.moccmlfcl.mocc/mocc/functionEnableDisableConstraint.moccml" 
ECLimport	"platform:/resource/fr.inria.glose.fcl.moccmlfcl.mocc/mocc/functionGlobalConstraint.moccml" 
ECLimport	"platform:/resource/fr.inria.glose.fcl.moccmlfcl.mocc/mocc/FCLsystem.moccml" 


package fcl

    context Function
    	def: evaluatedModeTrue : Event = self
    	def: evaluatedModeFalse : Event = self
    	def if (true) : evaluateMode : Event = self.isEnabled() 
			    	[res] switch 	
			    		case (res = true) force evaluatedModeTrue;  
			     		case (res = false) force evaluatedModeFalse;
		def: evalResult : Event = self
    	def: startEvaluate : Event = self.dse_startEvalFunction() 
    	def: stopEvaluate : Event = self.dse_stopEvalFunction()	
		
		inv functionBehavior:
			Relation FCLfunction(self.evaluateMode, 
								 self.evalResult,
								 self.evaluatedModeTrue, 
								 self.evaluatedModeFalse,
								 self.startEvaluate,
								 self.stopEvaluate
			)

		-- ensure to start containing function before inner function in order to ensure input port propagation
		inv InnerFunctionsStartsAfterOuterFunction:
			(self.oclAsType(ecore::EObject).eContainer().oclIsKindOf(Function)
			) implies (
				Relation AlternatesOrFree( self.oclAsType(ecore::EObject).eContainer().oclAsType(Function).startEvaluate, self.startEvaluate, self.evaluatedModeTrue, self.evaluatedModeFalse)
			)
			
    context FunctionConnector
	
		def : connectEnable : Event = self
		def : connectDisable : Event = self

		inv defineConnectEnable:
			let connectEnable1 : Event = 
				Expression Intersection(self.emitter.oclAsType(ecore::EObject).eContainer().oclAsType(Function).evaluatedModeTrue, 
										self.receiver.oclAsType(ecore::EObject).eContainer().oclAsType(Function).evaluatedModeTrue
			) in
			Relation Coincides(connectEnable1, self.connectEnable)
		inv defineConnectDisable:
			let connectDisable1 : Event = 
				Expression Union(self.emitter.oclAsType(ecore::EObject).eContainer().oclAsType(Function).evaluatedModeFalse, 
								 self.receiver.oclAsType(ecore::EObject).eContainer().oclAsType(Function).evaluatedModeFalse
			) in
			Relation Coincides(connectDisable1, self.connectDisable)	

		

		inv startNextFunctionOnlyIfPreviousHasFinished:
			(	    (self.emitter.oclAsType(FunctionPort).direction = DirectionKind::Out or self.emitter.oclAsType(FunctionPort).direction = DirectionKind::InOut)
				and (self.receiver.oclAsType(FunctionPort).direction = DirectionKind::In or self.receiver.oclAsType(FunctionPort).direction = DirectionKind::InOut)
				and (not (self.delayed))
				and (not (self.receiver.oclAsType(ecore::EObject).eContainer().oclAsType(ecore::EObject).eContainer() = self.emitter.oclAsType(ecore::EObject).eContainer())) 
				and (not (self.emitter.oclAsType(ecore::EObject).eContainer().oclAsType(ecore::EObject).eContainer() = self.receiver.oclAsType(ecore::EObject).eContainer()))
			) implies (
				Relation AlternatesOrFree(
	    			self.emitter.oclAsType(ecore::EObject).eContainer().oclAsType(Function).stopEvaluate,
	    			self.receiver.oclAsType(ecore::EObject).eContainer().oclAsType(Function).startEvaluate,
		    		self.connectEnable,
		    		self.connectDisable
	    	))
	    	
	    	
	    inv ensurePropagateInputToInnerFunctions:
			(		(self.emitter.oclAsType(FunctionPort).direction = DirectionKind::In  or self.emitter.oclAsType(FunctionPort).direction = DirectionKind::InOut)
				and (self.receiver.oclAsType(FunctionPort).direction = DirectionKind::In or self.receiver.oclAsType(FunctionPort).direction = DirectionKind::InOut)
				and not (self.delayed)
				and self.receiver.oclAsType(ecore::EObject).eContainer().oclAsType(ecore::EObject).eContainer() = self.emitter.oclAsType(ecore::EObject).eContainer()
			) implies (
				Relation AlternatesOrFree(
	    			self.emitter.oclAsType(ecore::EObject).eContainer().oclAsType(Function).startEvaluate,
	    			self.receiver.oclAsType(ecore::EObject).eContainer().oclAsType(Function).startEvaluate,
		    		self.connectEnable,
		    		self.connectDisable
	    	))

	    inv ensurePropagateOutToOuterFunctions:
			(		(self.emitter.oclAsType(FunctionPort).direction = DirectionKind::Out  or self.emitter.oclAsType(FunctionPort).direction = DirectionKind::InOut)
				and (self.receiver.oclAsType(FunctionPort).direction = DirectionKind::Out or self.receiver.oclAsType(FunctionPort).direction = DirectionKind::InOut)
				and (not (self.delayed))
				and self.emitter.oclAsType(ecore::EObject).eContainer().oclAsType(ecore::EObject).eContainer() = self.receiver.oclAsType(ecore::EObject).eContainer()
			) implies (
				Relation AlternatesOrFree(
		    		self.emitter.oclAsType(ecore::EObject).eContainer().oclAsType(Function).stopEvaluate,
		    		self.receiver.oclAsType(ecore::EObject).eContainer().oclAsType(Function).stopEvaluate,
		    		self.connectEnable,
		    		self.connectDisable 
	    	))

	  
	context FCLModel 
		--trick to avoid unrecognized QVTo functions
		def: allSubobjects() : Set(ecore::EObject) = self.oclAsType(Set(ecore::EObject))  
		
	
		-- DSE->DSA
		def: propagateDelayedResults : Event = self.propagateDelayedResults()
		def: startEvaluateModeAutomata : Event = self.evaluateModeAutomata()
		def: stopEvaluateModeAutomata : Event = self
	
	
		inv mainLoop:
			Relation FCLSystem(
				self.startEvaluateModeAutomata,
				self.dataflow.evaluateMode,
				self.dataflow.evalResult,
				self.stopEvaluateModeAutomata,
				self.dataflow.startEvaluate,
				self.dataflow.stopEvaluate,
				self.propagateDelayedResults
			)
	
		 
		-- all functions evaluateMode is done synchronously
		inv synchronousEvaluateEnabledModesOnMainFunctionStart:
		 	Relation Coincides(self.allSubobjects()->select(eO | eO.oclIsKindOf(Function)).oclAsType(Function).evaluateMode)	
 
		-- all functions evalResult is done synchronously
		inv synchronousEvalResult:
		 	Relation Coincides(self.allSubobjects()->select(eO | eO.oclIsKindOf(Function)).oclAsType(Function).evalResult)		
 
		inv masterAlwaysEnabled:
			Relation Exclusion(self.dataflow.evaluatedModeFalse, self.dataflow.evaluatedModeFalse)	 
	 
endpackage