/* GENERATED FILE, do not modify manually                                                    *
 * If you need to modify it, copy it first */
package moccmlfcl.xdsml.api.impl;
import org.eclipse.emf.ecore.EObject;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.lang.reflect.Method;
import org.eclipse.gemoc.executionframework.engine.commons.K3DslHelper;


public class MOCCMLFCLRTDAccessor {
	public static java.util.List getReceivedEvents(fr.inria.glose.fcl.model.fcl.FCLModel eObject) {
		return (java.util.List)  getAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect", "receivedEvents");
	}
	public static boolean setReceivedEvents(fr.inria.glose.fcl.model.fcl.FCLModel eObject, java.util.List newValue) {
		return setAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect", "receivedEvents", newValue);
	}
	public static java.util.List getProcedureStack(fr.inria.glose.fcl.model.fcl.FCLModel eObject) {
		return (java.util.List)  getAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect", "procedureStack");
	}
	public static boolean setProcedureStack(fr.inria.glose.fcl.model.fcl.FCLModel eObject, java.util.List newValue) {
		return setAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect", "procedureStack", newValue);
	}
	public static java.lang.String getIndentation(fr.inria.glose.fcl.model.fcl.FCLModel eObject) {
		return (java.lang.String)  getAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect", "indentation");
	}
	public static boolean setIndentation(fr.inria.glose.fcl.model.fcl.FCLModel eObject, java.lang.String newValue) {
		return setAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect", "indentation", newValue);
	}
	public static java.io.BufferedReader getInputScenarioReader(fr.inria.glose.fcl.model.fcl.FCLModel eObject) {
		return (java.io.BufferedReader)  getAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect", "inputScenarioReader");
	}
	public static boolean setInputScenarioReader(fr.inria.glose.fcl.model.fcl.FCLModel eObject, java.io.BufferedReader newValue) {
		return setAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect", "inputScenarioReader", newValue);
	}
	public static fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper getFmuSimulationWrapper(fr.inria.glose.fcl.model.fcl.FMUFunction eObject) {
		return (fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper)  getAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect", "fmuSimulationWrapper");
	}
	public static boolean setFmuSimulationWrapper(fr.inria.glose.fcl.model.fcl.FMUFunction eObject, fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper newValue) {
		return setAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect", "fmuSimulationWrapper", newValue);
	}
	public static fr.inria.glose.fcl.model.fcl.Value getPreviousValue(fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort eObject) {
		return (fr.inria.glose.fcl.model.fcl.Value)  getAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect", "previousValue");
	}
	public static boolean setPreviousValue(fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort eObject, fr.inria.glose.fcl.model.fcl.Value newValue) {
		return setAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect", "previousValue", newValue);
	}
	public static fr.inria.glose.fcl.model.fcl.Value getCurrentValue(fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort eObject) {
		return (fr.inria.glose.fcl.model.fcl.Value)  getAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect", "currentValue");
	}
	public static boolean setCurrentValue(fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort eObject, fr.inria.glose.fcl.model.fcl.Value newValue) {
		return setAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect", "currentValue", newValue);
	}
	public static fr.inria.glose.fcl.model.fcl.Value getPreviousValue(fr.inria.glose.fcl.model.fcl.FunctionVarDecl eObject) {
		return (fr.inria.glose.fcl.model.fcl.Value)  getAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect", "previousValue");
	}
	public static boolean setPreviousValue(fr.inria.glose.fcl.model.fcl.FunctionVarDecl eObject, fr.inria.glose.fcl.model.fcl.Value newValue) {
		return setAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect", "previousValue", newValue);
	}
	public static fr.inria.glose.fcl.model.fcl.Value getCurrentValue(fr.inria.glose.fcl.model.fcl.FunctionVarDecl eObject) {
		return (fr.inria.glose.fcl.model.fcl.Value)  getAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect", "currentValue");
	}
	public static boolean setCurrentValue(fr.inria.glose.fcl.model.fcl.FunctionVarDecl eObject, fr.inria.glose.fcl.model.fcl.Value newValue) {
		return setAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect", "currentValue", newValue);
	}
	public static fr.inria.glose.fcl.model.fcl.Mode getCurrentMode(fr.inria.glose.fcl.model.fcl.ModeAutomata eObject) {
		return (fr.inria.glose.fcl.model.fcl.Mode)  getAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspect", "currentMode");
	}
	public static boolean setCurrentMode(fr.inria.glose.fcl.model.fcl.ModeAutomata eObject, fr.inria.glose.fcl.model.fcl.Mode newValue) {
		return setAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspect", "currentMode", newValue);
	}
	public static fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper getUnityWrapper(fr.inria.glose.fcl.model.fcl.UnityFunction eObject) {
		return (fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper)  getAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspect", "unityWrapper");
	}
	public static boolean setUnityWrapper(fr.inria.glose.fcl.model.fcl.UnityFunction eObject, fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper newValue) {
		return setAspectProperty(eObject, "fr.inria.glose.fcl.moccmlfcl.MOCCMLFCL", "fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspect", "unityWrapper", newValue);
	}

	public static Object getAspectProperty(EObject eObject, String languageName, String aspectName, String propertyName) {
		List<Class<?>> aspects = K3DslHelper.getAspectsOn(languageName, eObject.getClass());
		Class<?> aspect = null;
		for (Class<?> a : aspects) {
			try {
				if (Class.forName(aspectName).isAssignableFrom(a)) {
					aspect = a;
				}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		if (aspect == null) {
			return null;
		}
		Object res = null;
		 try {
			res = aspect.getDeclaredMethod(propertyName, ((fr.inria.diverse.k3.al.annotationprocessor.Aspect)aspect.getAnnotations()[0]).className()).invoke(eObject, eObject);
			if (res != null) {
				return res;
			}else {
				return null;
			}
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	
	public static boolean setAspectProperty(EObject eObject, String languageName, String aspectName, String propertyName, Object newValue) {
		List<Class<?>> aspects = K3DslHelper.getAspectsOn(languageName, eObject.getClass());
		Class<?> aspect = null;
		for (Class<?> a : aspects) {
			try {
				if (Class.forName(aspectName).isAssignableFrom(a)) {
					aspect = a;
				}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				return false;
			}
		}
		if (aspect == null) {
			return false;
		}
			 try {
				 aspect.getMethod(propertyName, ((fr.inria.diverse.k3.al.annotationprocessor.Aspect)aspect.getAnnotations()[0]).className(), newValue.getClass()).invoke(eObject, eObject, newValue);
				return true;
				} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					Method m = null;
					for(Class<?> c : ((fr.inria.diverse.k3.al.annotationprocessor.Aspect)aspect.getAnnotations()[0]).getClass().getInterfaces()) {
						
						try {
							aspect.getMethod(propertyName, c, newValue.getClass()).invoke(eObject, eObject, newValue);
							return true;
						} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
						}
						if (m == null) {
							throw new RuntimeException("not method found for "+((fr.inria.diverse.k3.al.annotationprocessor.Aspect)aspect.getAnnotations()[0]).className()+"::set"+propertyName);
						}
					}
				}
			return false;
	}
};