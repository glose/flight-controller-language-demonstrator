/**
 */
package fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl;

import fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.Alefcl_vmFactory;
import fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.Alefcl_vmPackage;
import fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.EventSequence;
import fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.FunctionConnectorSequence;
import fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.FunctionSequence;
import fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.TransitionSequence;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Alefcl_vmPackageImpl extends EPackageImpl implements Alefcl_vmPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionSequenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventSequenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionConnectorSequenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transitionSequenceEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.Alefcl_vmPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Alefcl_vmPackageImpl() {
		super(eNS_URI, Alefcl_vmFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link Alefcl_vmPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Alefcl_vmPackage init() {
		if (isInited)
			return (Alefcl_vmPackage) EPackage.Registry.INSTANCE.getEPackage(Alefcl_vmPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredAlefcl_vmPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		Alefcl_vmPackageImpl theAlefcl_vmPackage = registeredAlefcl_vmPackage instanceof Alefcl_vmPackageImpl
				? (Alefcl_vmPackageImpl) registeredAlefcl_vmPackage
				: new Alefcl_vmPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theAlefcl_vmPackage.createPackageContents();

		// Initialize created meta-data
		theAlefcl_vmPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAlefcl_vmPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Alefcl_vmPackage.eNS_URI, theAlefcl_vmPackage);
		return theAlefcl_vmPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunctionSequence() {
		return functionSequenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventSequence() {
		return eventSequenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunctionConnectorSequence() {
		return functionConnectorSequenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransitionSequence() {
		return transitionSequenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Alefcl_vmFactory getAlefcl_vmFactory() {
		return (Alefcl_vmFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		functionSequenceEClass = createEClass(FUNCTION_SEQUENCE);

		eventSequenceEClass = createEClass(EVENT_SEQUENCE);

		functionConnectorSequenceEClass = createEClass(FUNCTION_CONNECTOR_SEQUENCE);

		transitionSequenceEClass = createEClass(TRANSITION_SEQUENCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(functionSequenceEClass, FunctionSequence.class, "FunctionSequence", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(eventSequenceEClass, EventSequence.class, "EventSequence", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(functionConnectorSequenceEClass, FunctionConnectorSequence.class, "FunctionConnectorSequence",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(transitionSequenceEClass, TransitionSequence.class, "TransitionSequence", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //Alefcl_vmPackageImpl
