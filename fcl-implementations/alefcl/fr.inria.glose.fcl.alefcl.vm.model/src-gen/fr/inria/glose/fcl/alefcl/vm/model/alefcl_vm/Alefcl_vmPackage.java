/**
 */
package fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.Alefcl_vmFactory
 * @model kind="package"
 * @generated
 */
public interface Alefcl_vmPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "alefcl_vm";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/alefcl_vm";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "alefcl_vm";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Alefcl_vmPackage eINSTANCE = fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.Alefcl_vmPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.FunctionSequenceImpl <em>Function Sequence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.FunctionSequenceImpl
	 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.Alefcl_vmPackageImpl#getFunctionSequence()
	 * @generated
	 */
	int FUNCTION_SEQUENCE = 0;

	/**
	 * The number of structural features of the '<em>Function Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SEQUENCE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Function Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_SEQUENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.EventSequenceImpl <em>Event Sequence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.EventSequenceImpl
	 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.Alefcl_vmPackageImpl#getEventSequence()
	 * @generated
	 */
	int EVENT_SEQUENCE = 1;

	/**
	 * The number of structural features of the '<em>Event Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_SEQUENCE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Event Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_SEQUENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.FunctionConnectorSequenceImpl <em>Function Connector Sequence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.FunctionConnectorSequenceImpl
	 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.Alefcl_vmPackageImpl#getFunctionConnectorSequence()
	 * @generated
	 */
	int FUNCTION_CONNECTOR_SEQUENCE = 2;

	/**
	 * The number of structural features of the '<em>Function Connector Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CONNECTOR_SEQUENCE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Function Connector Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CONNECTOR_SEQUENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.TransitionSequenceImpl <em>Transition Sequence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.TransitionSequenceImpl
	 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.Alefcl_vmPackageImpl#getTransitionSequence()
	 * @generated
	 */
	int TRANSITION_SEQUENCE = 3;

	/**
	 * The number of structural features of the '<em>Transition Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_SEQUENCE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Transition Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_SEQUENCE_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.FunctionSequence <em>Function Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Sequence</em>'.
	 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.FunctionSequence
	 * @generated
	 */
	EClass getFunctionSequence();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.EventSequence <em>Event Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Sequence</em>'.
	 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.EventSequence
	 * @generated
	 */
	EClass getEventSequence();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.FunctionConnectorSequence <em>Function Connector Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Connector Sequence</em>'.
	 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.FunctionConnectorSequence
	 * @generated
	 */
	EClass getFunctionConnectorSequence();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.TransitionSequence <em>Transition Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition Sequence</em>'.
	 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.TransitionSequence
	 * @generated
	 */
	EClass getTransitionSequence();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Alefcl_vmFactory getAlefcl_vmFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.FunctionSequenceImpl <em>Function Sequence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.FunctionSequenceImpl
		 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.Alefcl_vmPackageImpl#getFunctionSequence()
		 * @generated
		 */
		EClass FUNCTION_SEQUENCE = eINSTANCE.getFunctionSequence();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.EventSequenceImpl <em>Event Sequence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.EventSequenceImpl
		 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.Alefcl_vmPackageImpl#getEventSequence()
		 * @generated
		 */
		EClass EVENT_SEQUENCE = eINSTANCE.getEventSequence();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.FunctionConnectorSequenceImpl <em>Function Connector Sequence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.FunctionConnectorSequenceImpl
		 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.Alefcl_vmPackageImpl#getFunctionConnectorSequence()
		 * @generated
		 */
		EClass FUNCTION_CONNECTOR_SEQUENCE = eINSTANCE.getFunctionConnectorSequence();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.TransitionSequenceImpl <em>Transition Sequence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.TransitionSequenceImpl
		 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.Alefcl_vmPackageImpl#getTransitionSequence()
		 * @generated
		 */
		EClass TRANSITION_SEQUENCE = eINSTANCE.getTransitionSequence();

	}

} //Alefcl_vmPackage
