/**
 */
package fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.Alefcl_vmPackage
 * @generated
 */
public interface Alefcl_vmFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Alefcl_vmFactory eINSTANCE = fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl.Alefcl_vmFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Function Sequence</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Sequence</em>'.
	 * @generated
	 */
	FunctionSequence createFunctionSequence();

	/**
	 * Returns a new object of class '<em>Event Sequence</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Sequence</em>'.
	 * @generated
	 */
	EventSequence createEventSequence();

	/**
	 * Returns a new object of class '<em>Function Connector Sequence</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Connector Sequence</em>'.
	 * @generated
	 */
	FunctionConnectorSequence createFunctionConnectorSequence();

	/**
	 * Returns a new object of class '<em>Transition Sequence</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transition Sequence</em>'.
	 * @generated
	 */
	TransitionSequence createTransitionSequence();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Alefcl_vmPackage getAlefcl_vmPackage();

} //Alefcl_vmFactory
