/**
 */
package fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition Sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.Alefcl_vmPackage#getTransitionSequence()
 * @model
 * @generated
 */
public interface TransitionSequence extends EObject {
} // TransitionSequence
