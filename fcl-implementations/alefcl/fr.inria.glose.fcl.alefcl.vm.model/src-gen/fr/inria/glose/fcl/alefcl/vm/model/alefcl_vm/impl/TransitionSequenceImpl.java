/**
 */
package fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl;

import fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.Alefcl_vmPackage;
import fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.TransitionSequence;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transition Sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TransitionSequenceImpl extends MinimalEObjectImpl.Container implements TransitionSequence {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransitionSequenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Alefcl_vmPackage.Literals.TRANSITION_SEQUENCE;
	}

} //TransitionSequenceImpl
