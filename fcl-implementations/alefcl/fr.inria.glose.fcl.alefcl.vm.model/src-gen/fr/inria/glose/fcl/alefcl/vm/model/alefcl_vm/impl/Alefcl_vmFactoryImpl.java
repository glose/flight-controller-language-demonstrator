/**
 */
package fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl;

import fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Alefcl_vmFactoryImpl extends EFactoryImpl implements Alefcl_vmFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Alefcl_vmFactory init() {
		try {
			Alefcl_vmFactory theAlefcl_vmFactory = (Alefcl_vmFactory) EPackage.Registry.INSTANCE
					.getEFactory(Alefcl_vmPackage.eNS_URI);
			if (theAlefcl_vmFactory != null) {
				return theAlefcl_vmFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Alefcl_vmFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Alefcl_vmFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case Alefcl_vmPackage.FUNCTION_SEQUENCE:
			return createFunctionSequence();
		case Alefcl_vmPackage.EVENT_SEQUENCE:
			return createEventSequence();
		case Alefcl_vmPackage.FUNCTION_CONNECTOR_SEQUENCE:
			return createFunctionConnectorSequence();
		case Alefcl_vmPackage.TRANSITION_SEQUENCE:
			return createTransitionSequence();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionSequence createFunctionSequence() {
		FunctionSequenceImpl functionSequence = new FunctionSequenceImpl();
		return functionSequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventSequence createEventSequence() {
		EventSequenceImpl eventSequence = new EventSequenceImpl();
		return eventSequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionConnectorSequence createFunctionConnectorSequence() {
		FunctionConnectorSequenceImpl functionConnectorSequence = new FunctionConnectorSequenceImpl();
		return functionConnectorSequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransitionSequence createTransitionSequence() {
		TransitionSequenceImpl transitionSequence = new TransitionSequenceImpl();
		return transitionSequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Alefcl_vmPackage getAlefcl_vmPackage() {
		return (Alefcl_vmPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Alefcl_vmPackage getPackage() {
		return Alefcl_vmPackage.eINSTANCE;
	}

} //Alefcl_vmFactoryImpl
