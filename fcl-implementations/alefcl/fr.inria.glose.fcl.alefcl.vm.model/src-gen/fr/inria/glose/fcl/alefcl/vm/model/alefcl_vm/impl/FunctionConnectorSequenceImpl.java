/**
 */
package fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl;

import fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.Alefcl_vmPackage;
import fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.FunctionConnectorSequence;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Connector Sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FunctionConnectorSequenceImpl extends MinimalEObjectImpl.Container implements FunctionConnectorSequence {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionConnectorSequenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Alefcl_vmPackage.Literals.FUNCTION_CONNECTOR_SEQUENCE;
	}

} //FunctionConnectorSequenceImpl
