/**
 */
package fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl;

import fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.Alefcl_vmPackage;
import fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.EventSequence;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EventSequenceImpl extends MinimalEObjectImpl.Container implements EventSequence {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventSequenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Alefcl_vmPackage.Literals.EVENT_SEQUENCE;
	}

} //EventSequenceImpl
