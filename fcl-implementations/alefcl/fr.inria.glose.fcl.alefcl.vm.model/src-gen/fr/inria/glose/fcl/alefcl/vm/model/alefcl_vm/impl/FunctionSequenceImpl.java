/**
 */
package fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.impl;

import fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.Alefcl_vmPackage;
import fr.inria.glose.fcl.alefcl.vm.model.alefcl_vm.FunctionSequence;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FunctionSequenceImpl extends MinimalEObjectImpl.Container implements FunctionSequence {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionSequenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Alefcl_vmPackage.Literals.FUNCTION_SEQUENCE;
	}

} //FunctionSequenceImpl
