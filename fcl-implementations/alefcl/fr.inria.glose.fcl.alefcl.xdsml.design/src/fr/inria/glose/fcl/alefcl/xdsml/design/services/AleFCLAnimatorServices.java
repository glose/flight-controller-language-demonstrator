package fr.inria.glose.fcl.alefcl.xdsml.design.services;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gemoc.executionframework.extensions.sirius.services.AbstractGemocAnimatorServices;

public class AleFCLAnimatorServices extends AbstractGemocAnimatorServices {

	@Override
	protected List<StringCouple> getRepresentationRefreshList() {
		final List<StringCouple> res = new ArrayList<StringCouple>();
		// Add in res the list of layers that should be activated and refreshed while debugging the model
		//	in case of a single odesign with all layers in a single viewpoint:
        //		- the first String is the id of the Diagram Description
        //		- the second String is the id of the Layer
		//  
		//	in case of a diagram extension:
		//		- the first String is the Representation Name of the Diagram Extension (do not confuse with the Name !!)
		//		- the second String is the id of the Layer
				
		res.add(new StringCouple("FunctionDiagramAleFCLExtension", "Animation"));
		res.add(new StringCouple("FCLFunctionDiagram", "Animation"));

		res.add(new StringCouple("ModeDiagramAleFCLExtension", "Animation"));
		res.add(new StringCouple("FCLModeDiagram", "Animation"));
		return res;
	}

}
