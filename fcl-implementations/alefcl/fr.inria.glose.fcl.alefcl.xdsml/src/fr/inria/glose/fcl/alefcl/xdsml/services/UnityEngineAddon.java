package fr.inria.glose.fcl.alefcl.xdsml.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecoretools.ale.ALEInterpreter;
import org.eclipse.gemoc.ale.interpreted.engine.AleEngine;
import org.eclipse.gemoc.ale.interpreted.engine.debug.AleDynamicAccessor;
import org.eclipse.gemoc.ale.interpreted.engine.sirius.ALESiriusInterpreter;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.gemoc.xdsmlframework.api.engine_addon.IEngineAddon;
import org.eclipse.sirius.common.tools.api.interpreter.EvaluationException;
import org.eclipse.sirius.common.tools.api.interpreter.IInterpreterWithDiagnostic.IEvaluationResult;

import fr.inria.glose.fcl.alefcl.xdsml.Activator;
import fr.inria.glose.fcl.model.fcl.DirectionKind;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FMUFunction;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.UnityFunction;
import fr.inria.glose.fcl.model.unity.UnityInstance;

public class UnityEngineAddon  implements IEngineAddon {

	
	ArrayList<UnityInstance> unityInstances = new ArrayList<UnityInstance>();
	AleEngine _currentAleEngine;
	
	/*
	@Override
	public void engineInitialized(IExecutionEngine<?> executionEngine) {
		IEngineAddon.super.engineInitialized(executionEngine);
	//}

	//@Override
	//public void engineStarted(IExecutionEngine<?> executionEngine) {
		
		
		if(_currentAleEngine != null ) {
			// ignore double call
			return;
		}
		_currentAleEngine = (AleEngine)executionEngine;
		
		
		Resource res = _currentAleEngine.getExecutionContext().getResourceModel();
		if(res.getContents().get(0) instanceof FCLModel) {
			FCLModel fclModel = (FCLModel)res.getContents().get(0);
			
			
			List<UnityFunction> unityFunctions = fclModel.allFunctions().stream()
				.filter(f -> f instanceof UnityFunction)
				.map(obj -> (UnityFunction) obj)
				.collect(Collectors.toList());
			
			// for each UnityFunction in FCL create an instance
			for (UnityFunction unityFunction : unityFunctions) {
				try {
					IEvaluationResult evalRes = getSiriusInterpreter().evaluateExpression(
							unityFunction, 
							"ale: self.initializeUnity()");
					System.out.println(evalRes);
				} catch (EvaluationException e) {
					Activator.error(e.getMessage(), e);
				}
			}
		}
	}
*/
	@Override
	public void engineAboutToDispose(IExecutionEngine<?> engine) {
		IEngineAddon.super.engineAboutToDispose(engine);
		for (UnityInstance unityInstance : unityInstances) {
			unityInstance.dispose();
		}
		unityInstances.clear();
		_currentAleEngine = null;
	}

	public AleEngine getEngine() 
	{
		return _currentAleEngine;
	}
	
	AleDynamicAccessor _dynaccess = null;
	ALEInterpreter _currentALEInterpreter = null;
	private AleDynamicAccessor updateDynamicAccessor() {
		if(!getEngine().getInterpreter().equals(_currentALEInterpreter)) {
			_dynaccess = new AleDynamicAccessor(getEngine().getInterpreter(),getEngine().getModelUnits());
		}
		return _dynaccess;
	}
	
	ALESiriusInterpreter _siriusInterpreter = null;
	private ALESiriusInterpreter getSiriusInterpreter() {
		if(!getEngine().getInterpreter().equals(_currentALEInterpreter)) {
			_siriusInterpreter = new ALESiriusInterpreter(getEngine());
		}
		return _siriusInterpreter;
	}
}
