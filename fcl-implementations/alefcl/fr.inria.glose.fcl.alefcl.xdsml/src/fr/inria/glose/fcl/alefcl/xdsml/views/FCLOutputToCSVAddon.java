package fr.inria.glose.fcl.alefcl.xdsml.views;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecoretools.ale.ALEInterpreter;
import org.eclipse.gemoc.ale.interpreted.engine.AleEngine;
import org.eclipse.gemoc.ale.interpreted.engine.debug.AleDynamicAccessor;
import org.eclipse.gemoc.ale.interpreted.engine.sirius.ALESiriusInterpreter;
import org.eclipse.gemoc.commons.eclipse.core.resources.IFileUtils;
import org.eclipse.gemoc.trace.commons.model.trace.Step;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.gemoc.xdsmlframework.api.engine_addon.IEngineAddon;
import org.eclipse.sirius.common.tools.api.interpreter.EvaluationException;
import org.eclipse.sirius.common.tools.api.interpreter.IInterpreterWithDiagnostic.IEvaluationResult;

import fr.inria.glose.fcl.alefcl.xdsml.Activator;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.FunctionVarDecl;


public class FCLOutputToCSVAddon implements IEngineAddon {

	public FCLOutputToCSVAddon() {
		
	}

	AleEngine _currentAleEngine;
	IFile ifile;
	List<FunctionBlockDataPort> ports;
	FunctionVarDecl globalClock;
	
	
	@Override
	public void engineAboutToStart(IExecutionEngine<?> engine) {
		IEngineAddon.super.engineAboutToStart(engine);
		
		if(_currentAleEngine != null ) {
			// ignore double call
			return;
		}
		_currentAleEngine = (AleEngine)engine;
		
		_currentAleEngine = (AleEngine)engine;
		if(_currentAleEngine != null && _currentAleEngine.getExecutionContext() != null) {
			Resource res = _currentAleEngine.getExecutionContext().getResourceModel();
			if(res.getContents().get(0) instanceof FCLModel) {
				FCLModel fclModel = (FCLModel)res.getContents().get(0);

				ports = fclModel.getDataflow().getFunctionPorts().stream()
						.filter(p -> /*(p.getDirection() == DirectionKind.OUT || p.getDirection() == DirectionKind.IN_OUT) 
										&&*/
									p instanceof FunctionBlockDataPort)
						.map(obj -> (FunctionBlockDataPort) obj)
						.collect(Collectors.toList());
				
				
				URI uri = res.getURI();
				String filePath = uri.appendFileExtension("output.csv").toPlatformString(true);
				ifile = ResourcesPlugin.getWorkspace().getRoot()
						.getFile(new Path(filePath));
				ifile.getLocationURI();
				try {
					// create the csv header
						
					StringBuilder sb = new StringBuilder();	
					// add global clock
					if(fclModel.getDataflow().getTimeReference() != null) {
						globalClock = fclModel.getDataflow().getTimeReference().getObservableVar();
						sb.append(globalClock.getName());
					}
					// add output ports
					for(FunctionBlockDataPort port : ports) {
						sb.append(", "+port.getName());
					};sb.append("\n");
					IFileUtils.writeInFile(ifile, sb.toString(), null);
				} catch (CoreException | IOException e) {
					Activator.error(e.getMessage(), e);
				}
				
			}
		}
	}



	@Override
	public void engineStopped(IExecutionEngine<?> engine) {
		IEngineAddon.super.engineStopped(engine);
		
		try {
			ifile.refreshLocal(0, null);
		} catch (CoreException e) {

			Activator.error(e.getMessage(), e);
		}

		this._currentAleEngine = null;
		_siriusInterpreter = null;
		_currentALEInterpreter = null;
	}






	@Override
	public void aboutToExecuteStep(IExecutionEngine<?> engine, Step<?> stepToExecute) {
		IEngineAddon.super.aboutToExecuteStep(engine, stepToExecute);
		
		if(stepToExecute.getMseoccurrence().getMse().getAction().getName()
				.equals("doMainLoop")) {

			try {
				StringBuilder sb= new StringBuilder();
			     
				if(globalClock != null) {
					IEvaluationResult evalRes = getSiriusInterpreter().evaluateExpression(
							globalClock, 
							"ale: self.currentValue.rawValue()");
					sb.append(evalRes.getValue().toString());
				}
				// add output ports
				for(FunctionBlockDataPort port : ports) {
					IEvaluationResult evalRes = getSiriusInterpreter().evaluateExpression(
							port, 
							"ale: self.currentValue.rawValue()");
					sb.append(", "+evalRes.getValue());
				};
				
				//Set true for append mode
			    FileWriter fileWriter = new FileWriter(ifile.getLocation().toOSString(), 
			    		true);
				
			    PrintWriter printWriter = new PrintWriter(fileWriter);
			    printWriter.println(sb.toString());  //New line
			    printWriter.close();
			} catch (IOException | EvaluationException e) {

				Activator.error(e.getMessage(), e);
			} 
		}
		
	}
	
	public AleEngine getEngine() 
	{
		return _currentAleEngine;
	}
	
	AleDynamicAccessor _dynaccess = null;
	ALEInterpreter _currentALEInterpreter = null;
	private AleDynamicAccessor updateDynamicAccessor() {
		if(!getEngine().getInterpreter().equals(_currentALEInterpreter)) {
			_dynaccess = new AleDynamicAccessor(getEngine().getInterpreter(),getEngine().getModelUnits());
		}
		return _dynaccess;
	}
	
	ALESiriusInterpreter _siriusInterpreter = null;
	private ALESiriusInterpreter getSiriusInterpreter() {
		if(!getEngine().getInterpreter().equals(_currentALEInterpreter)) {
			_siriusInterpreter = new ALESiriusInterpreter(getEngine());
		}
		return _siriusInterpreter;
	}

}
