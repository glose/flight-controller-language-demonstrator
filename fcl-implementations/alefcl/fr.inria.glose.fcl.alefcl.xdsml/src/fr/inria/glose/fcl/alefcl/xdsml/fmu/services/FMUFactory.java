/* % Function name : FMUFactory
%-------------------------------------------------------------------------
% Created by           :    Jean-Philippe LAM-YEE-MUI (EDF R&D)
% Date                 :    01/01/2015
% SVN version          :    0
% Last modification by :    xxxxxxxxx
% Date                 :    xx/xx/xxxx
% SVN version          :    xxx
%-------------------------------------------------------------------------
% DACCOSIM 2015 - Copyright � 2015 EDF and CentraleSupelec
% Code under GNU AGPL v3 license.
%-------------------------------------------------------------------------*/
package fr.inria.glose.fcl.alefcl.xdsml.fmu.services;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.javafmi.modeldescription.FmiVersion;
import org.javafmi.modeldescription.v2.Capabilities.Capability;
import org.javafmi.wrapper.generic.Simulation2;
import org.javafmi.wrapper.v2.Access;

import fr.inria.glose.fcl.alefcl.xdsml.Activator;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper;


public class FMUFactory
{
	//public Fmu m_FMUModel;
	private boolean m_bInitialized = false;

	public FMUFactory(String FMUFilePath)
	{
		if(!FMUFilePath.isEmpty())
		{
//			if(FMUFilePath.toLowerCase().endsWith("daccosimfmu"))
//			{
//				// Initialize the model
//				DACCOSIMFMUPackage.eINSTANCE.eClass();
//
//				ResourceSet resourceSet = new ResourceSetImpl();
//				Resource resource = resourceSet.getResource(URI.createURI((new File(FMUFilePath)).toURI().toString()), true);
//				try {
//					resource.load(null);
//					m_FMUModel = (Fmu) resource.getContents().get(0);
//					m_bInitialized = true;
//				} catch(IOException e) {
//					e.printStackTrace();
//				}
//			}
//			else 
			if(FMUFilePath.toLowerCase().endsWith("fmu"))
			{
				// Try to load the FMU at FMUFilePath
				Simulation2 javaFMI_FMU = null;
				try {javaFMI_FMU = new Simulation2(FMUFilePath);}
				catch(Exception e)
				{
					System.err.println(e.toString());
					Activator.error(e.toString());
					Activator.error("Failed to load " + FMUFilePath);
//					new MessageBoxErrorIcon("Failed to load " + FMUFilePath);
					return;
				}
				
				boolean bSuccessfulInitialization = true;
	//not needed just to import it	
	try {javaFMI_FMU.init(0.0);}	catch(Exception e){bSuccessfulInitialization = false;}
				
				// Initialize m_FMUModel
	/*			m_FMUModel = FmiAssemblerFactory.eINSTANCE.createFmu();
				m_FMUModel.setPath(FMUFilePath);
				String FMUFileLastName = (new File(FMUFilePath)).getName();
				if(javaFMI_FMU.getModelDescription().getModelName() == null){
					m_FMUModel.setName(FMUFileLastName.substring(0, FMUFileLastName.toLowerCase().indexOf(".fmu")));					
				}
				else{
					m_FMUModel.setName(javaFMI_FMU.getModelDescription().getModelName() );
				}
*/
				//*******************************************************************
				// Populate m_FMUModel inputs and outputs and initializable variables
				Access access = null;
//				Map<String, List<String>> mapOutputDependencies = null;
				// Try to load the FMU as a FMI V2.0 FMU
				try
				{
					access = new Access(javaFMI_FMU);
		/*			m_FMUModel.setFmiVersion(FMIVersion.V2_0);
					m_FMUModel.setCanBeInstantiatedOnlyOncePerProcess(access.check(new Capability("CAN_BE_INSTANTIATED_ONLY_ONCE_PER_PROCESS")));
					m_FMUModel.setCanHandleVariableCommunicationStepSize(access.check(new Capability("CAN_HANDLE_VARIABLE_COMMUNICATION_STEP_SIZE")));
					m_FMUModel.setCanGetAndSetFMUState(access.check(new Capability("CAN_GET_AND_SET_FMU_STATE")));
					m_FMUModel.setCanInterpolateInputs(access.check(new Capability("CAN_INTERPOLATE_INPUTS")));
*/
//TODO must be done only if it is a physicalFMU
//					if (access.getModelDescription().getDefaultExperiment() != null){
//						double solverInternalTolerance = access.getModelDescription().getDefaultExperiment().getTolerance();
//						if(solverInternalTolerance != 0)
//							m_FMUModel.setInternalSolverTolerance(solverInternalTolerance);
//					}else{
//						m_FMUModel.setInternalSolverTolerance(0.01);
//					}
					
					
//					mapOutputDependencies = CalculationGraphModelUtils.getOutputDependencies(access);
//					for(Entry<String, List<String>> entry : mapOutputDependencies.entrySet())
//						System.out.println("Dependencies of " + entry.getKey() + "=" + Arrays.toString(entry.getValue().toArray()));
				}
				catch(Exception e)
				{
//					new MessageBoxErrorIcon("FMI 1.0 is not supported anymore");
					return;
				}

				// Loop through the FMU inputs, outputs, and initializable variables
	//			Map<String, Input> mapInputs = new HashMap<String, Input>();
				for(org.javafmi.modeldescription.ScalarVariable variable : access.getModelVariables())
				{
					String causality = variable.getCausality();

					String varName = variable.getName();
					if(causality.equals("input"))
					{
						Activator.debug("FMU input: "+varName+" = "+javaFMI_FMU.read(varName).asDouble() + " //"+javaFMI_FMU.readVariable(varName)+ " "+variable.getType());
		/*				Input input = FmiAssemblerFactory.eINSTANCE.createInput();
						CalculationGraphModelUtils.INSTANCE.SetScalarVariableData(input, variable);
						if(access.getVersion() == FmiVersion.TWO)
							CalculationGraphModelUtils.INSTANCE.SetFMI2ScalarVariableData(input, (org.javafmi.modeldescription.v2.ScalarVariable) variable);
						if(bSuccessfulInitialization)
							input.setDefaultInitialValue(javaFMI_FMU.readVariable(variable.getName()).getValue().toString());
						m_FMUModel.getOwnedInputs().add(input);
						mapInputs.put(input.getName(), input);
			*/			
					}
					else if(causality.equals("output"))
					{
						Activator.debug("FMU output: "+varName+" = "+javaFMI_FMU.read(varName).asDouble() + " //"+javaFMI_FMU.readVariable(varName)+ " "+variable.getType());
	/*					Output output = FmiAssemblerFactory.eINSTANCE.createOutput();
						CalculationGraphModelUtils.INSTANCE.SetScalarVariableData(output, variable);
						if(access.getVersion() == FmiVersion.TWO)
							CalculationGraphModelUtils.INSTANCE.SetFMI2ScalarVariableData(output, (org.javafmi.modeldescription.v2.ScalarVariable) variable);
						m_FMUModel.getOwnedOutputs().add(output);
	*/				}
					else
					{
						String variability = variable.getVariability();
						if(access.getVersion() == FmiVersion.TWO && !variability.equals("constant"))
						{
							String initial = ((org.javafmi.modeldescription.v2.ScalarVariable) variable).getInitial();
							if(initial.equals("exact") || initial.equals("approx") || (causality.equals("parameter") && variability.equals("tunable")))
							{
		/*						Variable _variable = FmiAssemblerFactory.eINSTANCE.createVariable();
								CalculationGraphModelUtils.INSTANCE.SetScalarVariableData(_variable, variable);
								CalculationGraphModelUtils.INSTANCE.SetFMI2ScalarVariableData(_variable, (org.javafmi.modeldescription.v2.ScalarVariable) variable);
								if(bSuccessfulInitialization)
									_variable.setDefaultInitialValue(javaFMI_FMU.readVariable(variable.getName()).getValue().toString());
								m_FMUModel.getOwnedVariables().add(_variable);
			*/				}
						}
					}
				}
//				// Add information about the output dependencies (only for FMU compliant with FMI 2.0)
//				if(access.getVersion() == FmiVersion.TWO)
//				{
//					for(Output output : m_FMUModel.getOutputList())
//					{
////						if(mapOutputDependencies.containsKey(output.getName()))
////							for(String outputDependency : mapOutputDependencies.get(output.getName()))
////								output.getDependencyList().add(mapInputs.get(outputDependency));
//					}
//				}
				m_bInitialized = true;
				
				
			}
		}
	}

	
	public FMUFactory(String FMUFilePath, FMUSimulationWrapper wrapper)
	{
		if(!FMUFilePath.isEmpty())
		{
//			if(FMUFilePath.toLowerCase().endsWith("daccosimfmu"))
//			{
//				// Initialize the model
//				DACCOSIMFMUPackage.eINSTANCE.eClass();
//
//				ResourceSet resourceSet = new ResourceSetImpl();
//				Resource resource = resourceSet.getResource(URI.createURI((new File(FMUFilePath)).toURI().toString()), true);
//				try {
//					resource.load(null);
//					m_FMUModel = (Fmu) resource.getContents().get(0);
//					m_bInitialized = true;
//				} catch(IOException e) {
//					e.printStackTrace();
//				}
//			}
//			else 
			if(FMUFilePath.toLowerCase().endsWith("fmu"))
			{
				// Try to load the FMU at FMUFilePath
				Simulation2 javaFMI_FMU = null;
				try {javaFMI_FMU = new Simulation2(FMUFilePath);}
				catch(Exception e)
				{
					System.err.println(e.toString());
					Activator.error(e.toString());
					Activator.error("Failed to load " + FMUFilePath);
//					new MessageBoxErrorIcon("Failed to load " + FMUFilePath);
					return;
				}
				
				boolean bSuccessfulInitialization = true;
				//not needed just to import it	
				try {javaFMI_FMU.init(0.0);}	catch(Exception e){bSuccessfulInitialization = false;}
				
				// Initialize m_FMUModel
	/*			m_FMUModel = FmiAssemblerFactory.eINSTANCE.createFmu();
				m_FMUModel.setPath(FMUFilePath);
				String FMUFileLastName = (new File(FMUFilePath)).getName();
				if(javaFMI_FMU.getModelDescription().getModelName() == null){
					m_FMUModel.setName(FMUFileLastName.substring(0, FMUFileLastName.toLowerCase().indexOf(".fmu")));					
				}
				else{
					m_FMUModel.setName(javaFMI_FMU.getModelDescription().getModelName() );
				}
*/
				//*******************************************************************
				// Populate m_FMUModel inputs and outputs and initializable variables
				Access access = null;
//				Map<String, List<String>> mapOutputDependencies = null;
				// Try to load the FMU as a FMI V2.0 FMU
				try
				{
					access = new Access(javaFMI_FMU);
		/*			m_FMUModel.setFmiVersion(FMIVersion.V2_0);
					m_FMUModel.setCanBeInstantiatedOnlyOncePerProcess(access.check(new Capability("CAN_BE_INSTANTIATED_ONLY_ONCE_PER_PROCESS")));
					m_FMUModel.setCanHandleVariableCommunicationStepSize(access.check(new Capability("CAN_HANDLE_VARIABLE_COMMUNICATION_STEP_SIZE")));
					m_FMUModel.setCanGetAndSetFMUState(access.check(new Capability("CAN_GET_AND_SET_FMU_STATE")));
					m_FMUModel.setCanInterpolateInputs(access.check(new Capability("CAN_INTERPOLATE_INPUTS")));
*/
//TODO must be done only if it is a physicalFMU
//					if (access.getModelDescription().getDefaultExperiment() != null){
//						double solverInternalTolerance = access.getModelDescription().getDefaultExperiment().getTolerance();
//						if(solverInternalTolerance != 0)
//							m_FMUModel.setInternalSolverTolerance(solverInternalTolerance);
//					}else{
//						m_FMUModel.setInternalSolverTolerance(0.01);
//					}
					
					
//					mapOutputDependencies = CalculationGraphModelUtils.getOutputDependencies(access);
//					for(Entry<String, List<String>> entry : mapOutputDependencies.entrySet())
//						System.out.println("Dependencies of " + entry.getKey() + "=" + Arrays.toString(entry.getValue().toArray()));
				}
				catch(Exception e)
				{
//					new MessageBoxErrorIcon("FMI 1.0 is not supported anymore");
					return;
				}

				// Loop through the FMU inputs, outputs, and initializable variables
	//			Map<String, Input> mapInputs = new HashMap<String, Input>();
				for(org.javafmi.modeldescription.ScalarVariable variable : access.getModelVariables())
				{
					String causality = variable.getCausality();

					String varName = variable.getName();
					if(causality.equals("input"))
					{
						Activator.debug("FMU input: "+varName+" = "+javaFMI_FMU.read(varName).asDouble() + " //"+javaFMI_FMU.readVariable(varName)+ " "+variable.getType());
		/*				Input input = FmiAssemblerFactory.eINSTANCE.createInput();
						CalculationGraphModelUtils.INSTANCE.SetScalarVariableData(input, variable);
						if(access.getVersion() == FmiVersion.TWO)
							CalculationGraphModelUtils.INSTANCE.SetFMI2ScalarVariableData(input, (org.javafmi.modeldescription.v2.ScalarVariable) variable);
						if(bSuccessfulInitialization)
							input.setDefaultInitialValue(javaFMI_FMU.readVariable(variable.getName()).getValue().toString());
						m_FMUModel.getOwnedInputs().add(input);
						mapInputs.put(input.getName(), input);
			*/			
					}
					else if(causality.equals("output"))
					{
						Activator.debug("FMU output: "+varName+" = "+javaFMI_FMU.read(varName).asDouble() + " //"+javaFMI_FMU.readVariable(varName)+ " "+variable.getType());
	/*					Output output = FmiAssemblerFactory.eINSTANCE.createOutput();
						CalculationGraphModelUtils.INSTANCE.SetScalarVariableData(output, variable);
						if(access.getVersion() == FmiVersion.TWO)
							CalculationGraphModelUtils.INSTANCE.SetFMI2ScalarVariableData(output, (org.javafmi.modeldescription.v2.ScalarVariable) variable);
						m_FMUModel.getOwnedOutputs().add(output);
	*/				}
					else
					{
						String variability = variable.getVariability();
						if(access.getVersion() == FmiVersion.TWO && !variability.equals("constant"))
						{
							String initial = ((org.javafmi.modeldescription.v2.ScalarVariable) variable).getInitial();
							if(initial.equals("exact") || initial.equals("approx") || (causality.equals("parameter") && variability.equals("tunable")))
							{
		/*						Variable _variable = FmiAssemblerFactory.eINSTANCE.createVariable();
								CalculationGraphModelUtils.INSTANCE.SetScalarVariableData(_variable, variable);
								CalculationGraphModelUtils.INSTANCE.SetFMI2ScalarVariableData(_variable, (org.javafmi.modeldescription.v2.ScalarVariable) variable);
								if(bSuccessfulInitialization)
									_variable.setDefaultInitialValue(javaFMI_FMU.readVariable(variable.getName()).getValue().toString());
								m_FMUModel.getOwnedVariables().add(_variable);
			*/				}
						}
					}
				}
//				// Add information about the output dependencies (only for FMU compliant with FMI 2.0)
//				if(access.getVersion() == FmiVersion.TWO)
//				{
//					for(Output output : m_FMUModel.getOutputList())
//					{
////						if(mapOutputDependencies.containsKey(output.getName()))
////							for(String outputDependency : mapOutputDependencies.get(output.getName()))
////								output.getDependencyList().add(mapInputs.get(outputDependency));
//					}
//				}
				m_bInitialized = true;
				
				
			}
		}
	}
	
	public boolean IsInitialized()
	{
		return m_bInitialized;
	}

//	@Override
//	public Object getNewObject()
//	{
//		FMU FMUInstance = DACCOSIMCalculationGraphFactory.eINSTANCE.createFMU();
//
//		//===================================================
//		// Conversion between fr.daccosim.model.fmu and
//		// fr.daccosim.model.calculationgraph.FMU
//		if(m_FMUModel != null)
//		{
//			switch(m_FMUModel.getFMIVersion())
//			{
//			case V2_0:
//				FMUInstance.setFMIVersion(fr.daccosim.model.calculationgraph.FMIVersion.V2_0);
//				break;
//			}
//			
//			Map<String, Input> mapInputs = new HashMap<String, Input>();
//			for(fr.daccosim.model.fmu.Input FMUModelInput : m_FMUModel.getInputList())
//			{
//				Input input = DACCOSIMCalculationGraphFactory.eINSTANCE.createInput();
//				CalculationGraphModelUtils.INSTANCE.ConvertFromFMUModelToCalculationGraphFMU(input, FMUModelInput);
//				input.setDefaultInitialValue(FMUModelInput.getDefaultInitialValue());
//				FMUInstance.getInputList().add(input);
//				mapInputs.put(input.getName(), input);
//			}
//
//			for(fr.daccosim.model.fmu.Output FMUModelOutput : m_FMUModel.getOutputList())
//			{
//				Output output = DACCOSIMCalculationGraphFactory.eINSTANCE.createOutput();
//				CalculationGraphModelUtils.INSTANCE.ConvertFromFMUModelToCalculationGraphFMU(output, FMUModelOutput);
//				output.setTolerance(FMUModelOutput.getTolerance());
//				FMUInstance.getOutputList().add(output);
//			}		
//
//			for(fr.daccosim.model.fmu.Variable FMUModelVariable : m_FMUModel.getVariableList())
//			{
//				Variable variable = DACCOSIMCalculationGraphFactory.eINSTANCE.createVariable();
//				CalculationGraphModelUtils.INSTANCE.ConvertFromFMUModelToCalculationGraphFMU(variable, FMUModelVariable);
//				variable.setDefaultInitialValue(FMUModelVariable.getDefaultInitialValue());
//				FMUInstance.getVariableList().add(variable);
//			}		
//
//			FMUInstance.setName(m_FMUModel.getInternalName());
//			FMUInstance.setPath(m_FMUModel.getPath());
//			FMUInstance.setCanBeInstantiatedOnlyOncePerProcess(m_FMUModel.isCanBeInstantiatedOnlyOncePerProcess());
//			FMUInstance.setCanHandleVariableCommunicationStepSize(m_FMUModel.isCanHandleVariableCommunicationStepSize());
//			FMUInstance.setCanGetAndSetFMUState(m_FMUModel.isCanGetAndSetFMUState());
//			FMUInstance.setCanInterpolateInputs(m_FMUModel.isCanInterpolateInputs());
//			FMUInstance.setInternalSolverTolerance(m_FMUModel.getInternalSolverTolerance());
//			
//			if(FMUInstance.getFMIVersion() == fr.daccosim.model.calculationgraph.FMIVersion.V2_0)
//			{	
//				int uiOutputIndex = 0;
//				for(fr.daccosim.model.fmu.Output FMUModelOutput : m_FMUModel.getOutputList())
//				{
//					Output FMUInstanceOutput = FMUInstance.getOutputList().get(uiOutputIndex);
//					for(fr.daccosim.model.fmu.Input dependency : FMUModelOutput.getDependencyList())
//						FMUInstanceOutput.getDependencyList().add(mapInputs.get(dependency.getName()));
//					++uiOutputIndex;
//				}
//			}
//		}
//
//		return FMUInstance;
//	}
//
//	@Override public Object getObjectType()
//	{
//		return FMU.class;
//	}
/*
	public String GetFMUName()
	{
		return m_FMUModel.getName();
	}
*/
}
