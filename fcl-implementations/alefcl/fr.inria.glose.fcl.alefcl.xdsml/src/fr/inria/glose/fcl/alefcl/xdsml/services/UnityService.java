package fr.inria.glose.fcl.alefcl.xdsml.services;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.commons.eclipse.core.resources.IFileUtils;
import org.eclipse.swt.widgets.Display;

import fr.inria.glose.fcl.alefcl.xdsml.Activator;
import fr.inria.glose.fcl.model.fcl.UnityFunction;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper;
import fr.inria.glose.fcl.model.unity.UnityInstance;

public class UnityService {

	
	// workaround to ALE bug (typechecker problem), parameter type must be EObject, cannot return EObject (cannot be cast) or precise type  
	public static void loadAndAssignUnityWrapper_workaround(EObject  selfUnityFunction, EObject resultUnityWrapper) {
		UnityWrapper result =	(UnityWrapper)resultUnityWrapper;
		UnityFunction self = (UnityFunction)selfUnityFunction;
		
		
		
		//String FMUFilePath =  self.getRunnableFmuPath();
		try {
			IFile iFile = IFileUtils.getIFileFromWorkspaceOrFileSystem(self.getUnityWebPagePath());
			String unityFilePath =  iFile.getLocation().toString();
			iFile.getRawLocationURI().toString();
			UnityInstance unityInstance = new UnityInstance(self.getName(), unityFilePath);
			
			// connect to wrapper for model access
			//result = Alefcl_vmFactory.eINSTANCE.createFMUSimulationWrapper();
			
			result.setUnityInstance(unityInstance);
		} catch (Exception e) {
			Activator.error("Failed to initialize Unity "+self.getUnityWebPagePath(), e);
		}
	}
	
	public static void setUnityVariable(EObject  selfUnityFunction, EObject resultUnityWrapper, 
			String nodeName, String varName, double newValue) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				UnityWrapper unityWrapper =	(UnityWrapper)resultUnityWrapper;
				unityWrapper.getUnityInstance().setVar(nodeName, varName, newValue);
			}
		});
	}
		
}
