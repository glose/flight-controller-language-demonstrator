package fr.inria.glose.fcl.alefcl.xdsml.views;


import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecoretools.ale.ALEDynamicExpressionEvaluator;
import org.eclipse.emf.ecoretools.ale.ALEInterpreter;
import org.eclipse.gemoc.ale.interpreted.engine.AleEngine;
import org.eclipse.gemoc.ale.interpreted.engine.debug.AleDynamicAccessor;
import org.eclipse.gemoc.executionframework.debugger.MutableField;
import org.eclipse.gemoc.executionframework.ui.views.engine.EngineSelectionDependentViewPart;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.sirius.common.tools.api.interpreter.EvaluationException;
import org.eclipse.sirius.common.tools.api.interpreter.IInterpreterWithDiagnostic.IEvaluationResult;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PlatformUI;

import fr.inria.glose.fcl.alefcl.xdsml.Activator;
import fr.inria.glose.fcl.alefcl.xdsml.views.dialogs.NewValueDialog;
import fr.inria.glose.fcl.model.fcl.DirectionKind;
import fr.inria.glose.fcl.model.fcl.Event;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.NamedElement;


/**
 * This sample class demonstrates how to plug-in a new
 * workbench view. The view shows data obtained from the
 * model. The sample creates a dummy model on the fly,
 * but a real implementation would connect to the model
 * available either in this or another plug-in (e.g. the workspace).
 * The view is connected to the model using a content provider.
 * <p>
 * The view uses a label provider to define how model
 * objects should be presented in the view. Each
 * view can present the same model objects using
 * different labels and icons, if needed. Alternatively,
 * a single label provider can be shared between views
 * in order to ensure that objects of the same type are
 * presented in the same way everywhere.
 * <p>
 */

public class FCLInputView extends EngineSelectionDependentViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "fr.inria.glose.fcl.alefcl.xdsml.views.FCLInputView";

	@Inject IWorkbench workbench;
	
	private TableViewer viewer;
	private AleEngine _currentSelectedEngine;
	private Action action1;
	private Action doubleClickAction;
	 

	@Override
	public void createPartControl(Composite parent) {
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		// make lines and header visible
		final Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		createColumn();
		
		viewer.setContentProvider(new FCLInputViewContentProvider());
		//viewer.setInput(new String[] { "One", "Two", "Three" });
		viewer.setInput(null);
		//viewer.setLabelProvider(new ViewLabelProvider());
		getSite().setSelectionProvider(viewer);
		makeActions();
		hookContextMenu();
		hookDoubleClickAction();
		contributeToActionBars();
	}

	/**
	 * Create the column(s) of the tableViewer
	 */
	private void createColumn()
	{	
		TableViewerColumn viewerColumn1 = new TableViewerColumn(viewer, SWT.LEFT);
		TableColumn column1 = viewerColumn1.getColumn();
		column1.setText("Port/Event");
		column1.setWidth(100);
		column1.setResizable(true);
		column1.setMoveable(true);
		ColumnLabelProvider column1LabelProvider = new ColumnLabelProvider() 
		{
			@Override
			public String getText(Object element) 
			{
				String result = new String();          
				if (element instanceof NamedElement)
				{
					result = ((NamedElement)element).getName() ;
				} else {
					return element.toString();
				}
				return result;
			}

			@Override
			public Image getImage(Object element) 
			{
				if (element instanceof FunctionBlockDataPort) 
				{
					DirectionKind direction = ((FunctionBlockDataPort) element).getDirection();
					switch(direction)
					{
						case IN: return SharedIcons.getSharedImage(SharedIcons.IN_PORT);
						case OUT: return SharedIcons.getSharedImage(SharedIcons.OUT_PORT);
						case IN_OUT: return SharedIcons.getSharedImage(SharedIcons.INOUT_PORT);
						default: break;
					}
				} else if (element instanceof Event) {
					return SharedIcons.getSharedImage(SharedIcons.EVENT);
				}
				return null;
			}
		};
		viewerColumn1.setLabelProvider(column1LabelProvider);
		
		TableViewerColumn viewerColumn2 = new TableViewerColumn(viewer, SWT.LEFT);
		TableColumn column2 = viewerColumn2.getColumn();
		column2.setText("Value");
		column2.setWidth(100);
		column2.setResizable(true);
		column2.setMoveable(true);
		ColumnLabelProvider column2LabelProvider = new ColumnLabelProvider() 
		{
			
			
			@Override
			public String getText(Object element) 
			{
				String result = new String();          
				if (element instanceof FunctionBlockDataPort)
				{
					result = getPortValueString((FunctionBlockDataPort) element);
					
				} else if (element instanceof Event) {
					return "";
				} else {
					return element.toString();
				}
				return result;
			}

			@Override
			public Image getImage(Object element) 
			{
				return null;
			}
		};
		viewerColumn2.setLabelProvider(column2LabelProvider);
		
	}
	
	private String getPortValueString(FunctionBlockDataPort port) {
		String result = "";
		AleDynamicAccessor dynaccess = updateDynamicAccessor();
		List<MutableField> fields =dynaccess.extractMutableField(port);
		Optional<MutableField> field = fields.stream().filter(f -> f.getName().startsWith("currentValue")).findAny();
		if(		field.isPresent() && 
				getEngine().getInterpreter().getCurrentEngine() != null) { // if no evaluation was launched, the getValue on the mutable field will fail
			
			try {
				if(field.get().getValue() != null) {
					IEvaluationResult evalRes = getAleDynamicEvaluator().evaluateExpression(
							(EObject) field.get().getValue(), 
							"ale: self.valueToString()");
					result = evalRes.getValue().toString();
				} else {
					result = null;
				}
			} catch (EvaluationException e) {
				Activator.error(e.getMessage(), e);
				result = field.get().getValue().toString();
			}
			
		}
		return result;
	}
	
	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				FCLInputView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		//manager.add(action1);
		//manager.add(new Separator());
		//manager.add(action2);
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(action1);
		// Other plug-ins can contribute their actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}
	
	private void fillLocalToolBar(IToolBarManager manager) {
		//manager.add(action1);
		//manager.add(action2);
	}

	private void makeActions() {
		action1 = new Action() {
			public void run() {
				IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
				if(selection.getFirstElement() instanceof FunctionBlockDataPort) {
					String portName = ((FunctionBlockDataPort)selection.getFirstElement()).getName();
					String portValue = getPortValueString((FunctionBlockDataPort)selection.getFirstElement());
					
					NewValueDialog dialog = new NewValueDialog(viewer.getControl().getShell(), portName, portValue);
					dialog.create();
					if (dialog.open() == Window.OK) {
						//showMessage("must set new value "+dialog.getValueString() + " on "+selection.getFirstElement());
						try {
							IEvaluationResult evalRes = getAleDynamicEvaluator().evaluateExpression(
									(EObject) selection.getFirstElement(), 
									"ale: self.setValueFromString('"+dialog.getValueString()+"')");
							
							//FCLInputView.this.getEngine().getInterpreter().getLogger().diagnosticForHuman();
							if(evalRes.getDiagnostic().getSeverity() == Diagnostic.ERROR) {
								MessageDialog.openError(
										viewer.getControl().getShell(),
										"FCL Input View",
										"Failed to set value: "+evalRes.getDiagnostic().getMessage());
							}
							updateView();
						} catch (EvaluationException e) {
							Activator.error(e.getMessage(),e);
						}
					}
				} else if(selection.getFirstElement() instanceof Event) {
					try {
						IEvaluationResult evalRes = getAleDynamicEvaluator().evaluateExpression(
								(EObject) selection.getFirstElement(), 
								"ale: self.send()");
						
						if(evalRes.getDiagnostic().getSeverity() == Diagnostic.ERROR) {
							MessageDialog.openError(
									viewer.getControl().getShell(),
									"FCL Input View",
									"Failed to send Event "+((Event)selection.getFirstElement()).getName()+": "+evalRes.getDiagnostic().getMessage());
						}
						//updateView();
					} catch (EvaluationException e) {
						Activator.error(e.getMessage(),e);
					}
				}
			}
			
		};
		action1.setText("Send");
		action1.setToolTipText("Set a new value on port or send Event occurence");
		action1.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
			getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));
		
		
		
		/* doubleClickAction = new Action() {
			public void run() {
				IStructuredSelection selection = viewer.getStructuredSelection();
				Object obj = selection.getFirstElement();
				showMessage("Double-click detected on "+obj.toString());
			}
		}; */
		doubleClickAction = action1;
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}
	private void showMessage(String message) {
		MessageDialog.openInformation(
			viewer.getControl().getShell(),
			"FCL External Ports View",
			message);
	}

	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}
	
	
	/**
	 * Refresh the input of the ContentProvider with the selected strategy
	 */
	public void updateView()
	{
		Display.getDefault().asyncExec(new Runnable() 
		{
			@Override
			public void run() 
			{
				if(getEngine() != null && getEngine().getExecutionContext() != null) {
					Resource res = getEngine().getExecutionContext().getResourceModel();
					if(res.getContents().get(0) instanceof FCLModel) {
						viewer.setInput(res.getContents().get(0));
					} else {
						viewer.setInput(null);										
					}
				} else {
					viewer.setInput(null);										
				}
			}				
		});
	}
	
	public void disposeEngine()
	{
		_currentAleInterpreter = null;
		_dynaccess = null;
		_aleDynamicEvaluator = null;
		_currentSelectedEngine = null;
	}
	
	/**
	 * Listen the engine selection in the enginesStatusView
	 */
	@Override
	public void engineSelectionChanged(IExecutionEngine<?> engine) {
		if (engine != null
			&& engine instanceof AleEngine) 
		{
			_currentSelectedEngine = (AleEngine) engine;
			_currentAleInterpreter = _currentSelectedEngine.getInterpreter();
			updateView();
		}
		else
		{
			viewer.setInput(null);
		}
	}
	
	public AleEngine getEngine() 
	{
		return _currentSelectedEngine;
	}

	
	
	AleDynamicAccessor _dynaccess = null;
	ALEInterpreter _currentAleInterpreter = null;
	private AleDynamicAccessor updateDynamicAccessor() {
		if(_dynaccess == null || !getEngine().getInterpreter().equals(_currentAleInterpreter)) {
			_dynaccess = new AleDynamicAccessor(getEngine().getInterpreter(),getEngine().getModelUnits());
		}
		return _dynaccess;
	}
	
	ALEDynamicExpressionEvaluator _aleDynamicEvaluator = null;
	private ALEDynamicExpressionEvaluator getAleDynamicEvaluator() {
		if(_aleDynamicEvaluator == null || !getEngine().getInterpreter().equals(_currentAleInterpreter)) {
			_aleDynamicEvaluator = new ALEDynamicExpressionEvaluator(getEngine().getInterpreter());
		}
		return _aleDynamicEvaluator;
	}
	
}
