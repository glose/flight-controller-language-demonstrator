package fr.inria.glose.fcl.alefcl.xdsml.services;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;
import java.util.Optional;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gemoc.commons.eclipse.messagingsystem.api.MessagingSystem;

import fr.inria.glose.fcl.alefcl.xdsml.Activator;


public class FCLService {

	
	public static final String MESSAGE_GROUP = "FCL_XDSML";
	
	// log service that allows to use a dedicated eclipse console instead of stdout
	// "dev*" messages are intended to the language designer, while the other are intended to the model designer
	public static void devDebug(EObject self, String message) {
		Activator.getDefault().getMessaggingSystem().log(MessagingSystem.Kind.DevDEBUG, message, MESSAGE_GROUP);
	}
	public static void devDebug(EObject self) {
		Activator.getDefault().getMessaggingSystem().log(MessagingSystem.Kind.DevDEBUG, self.toString(), MESSAGE_GROUP);
	}
	public static void devInfo(EObject self, String message) {
		Activator.getDefault().getMessaggingSystem().devInfo(message, MESSAGE_GROUP);
	}
	public static void devInfo(EObject self) {
		Activator.getDefault().getMessaggingSystem().devInfo(self.toString(), MESSAGE_GROUP);
	}
	public static void devWarn(EObject self, String message) {
		Activator.getDefault().getMessaggingSystem().log(MessagingSystem.Kind.DevWARNING, message, MESSAGE_GROUP);
	}
	public static void devError(EObject self, String message) {
		Activator.getDefault().getMessaggingSystem().log(MessagingSystem.Kind.DevERROR, message, MESSAGE_GROUP);
	}
	public static void info(EObject self, String message) {
		Activator.getDefault().getMessaggingSystem().info(message, MESSAGE_GROUP);
	}
	public static void important(EObject self, String message) {
		Activator.getDefault().getMessaggingSystem().important(message, MESSAGE_GROUP);
	}

	public static void warn(EObject self, String message) {
		Activator.getDefault().getMessaggingSystem().warn(message, MESSAGE_GROUP);
	}
	public static void error(EObject self, String message) {
		Activator.getDefault().getMessaggingSystem().error(message, MESSAGE_GROUP);
	}
	
	public static void raiseException(EObject self, String message) throws FCLTypeException {
		error(self, "throwing exception \""+message+"\" from the java service");
		throw new RuntimeException(message);
		//throw new FCLTypeException(message + " on "+self.toString());
	}
	
	// quick and dirty method to get a double from a double in java.lang.Math for ex
	public static double staticJavaCallDoubleToDouble(EObject self, String methodeQualifiedName, double in) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException {
		int lastindexof = methodeQualifiedName.lastIndexOf(".");
		String methodName = methodeQualifiedName.substring(lastindexof+1);
		String className = methodeQualifiedName.substring(0, lastindexof);
		Class klass = Class.forName(className);
		Method m = klass.getDeclaredMethod(methodName, double.class);
		return (double) m.invoke(null, in);
	}
	
	public static String getQualifiedName(EObject eObject) {
		StringBuilder sb = new StringBuilder();

		if(eObject.eContainer() != null){
			sb.append(getQualifiedName(eObject.eContainer()));
			sb.append("/");
		}
		Optional<EAttribute> feat = eObject.eClass().getEAllAttributes().stream()
				.filter(att -> att.getName().equals("name")).findAny();
		if(feat.isPresent()) {	
			sb.append(eObject.eGet(feat.get()));
		} else {
			String lastFragmentPart = EcoreUtil.getURI(eObject).fragment()
					.substring(EcoreUtil.getURI(eObject).fragment().lastIndexOf("/")+1);
			sb.append(lastFragmentPart);
		}
		return sb.toString();
		
	}
	
	public static String doubleToFormattedString(double d) {
		return String.format(Locale.US, "%.3G",d);
	}
}
