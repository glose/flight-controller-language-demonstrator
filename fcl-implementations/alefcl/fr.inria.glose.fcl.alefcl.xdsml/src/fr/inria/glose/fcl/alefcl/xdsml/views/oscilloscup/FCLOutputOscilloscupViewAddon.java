package fr.inria.glose.fcl.alefcl.xdsml.views.oscilloscup;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JFrame;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecoretools.ale.ALEDynamicExpressionEvaluator;
import org.eclipse.emf.ecoretools.ale.ALEInterpreter;
import org.eclipse.gemoc.ale.interpreted.engine.AleEngine;
import org.eclipse.gemoc.ale.interpreted.engine.debug.AleDynamicAccessor;
import org.eclipse.gemoc.trace.commons.model.trace.Step;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.gemoc.xdsmlframework.api.engine_addon.IEngineAddon;
import org.eclipse.sirius.common.tools.api.interpreter.EvaluationException;
import org.eclipse.sirius.common.tools.api.interpreter.IInterpreterWithDiagnostic.IEvaluationResult;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import fr.inria.glose.fcl.alefcl.xdsml.Activator;
import fr.inria.glose.fcl.model.fcl.DirectionKind;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.FunctionVarDecl;
import oscilloscup.data.rendering.DataElementRenderer;
import oscilloscup.data.rendering.figure.ConnectedLineFigureRenderer;
import oscilloscup.multiscup.Clock;
import oscilloscup.multiscup.Multiscope;
import oscilloscup.multiscup.Property;
import oscilloscup.multiscup.PropertyValueFormatter;


public class FCLOutputOscilloscupViewAddon implements IEngineAddon {

	public FCLOutputOscilloscupViewAddon() {
		
	}

	AleEngine _currentAleEngine;
	FunctionVarDecl globalClock;
	private Frame frame;
	private Multiscope<FunctionBlockDataPort> multiscope;
	

	class FCLGlobalClock extends Clock
	{
		@Override
		public double getTime() {
			if(globalClock != null) {
				IEvaluationResult evalRes;
				try {
					evalRes = getAleDynamicEvaluator().evaluateExpression(
							globalClock, 
							"ale: self.currentValue.rawValue()");
					return (double) evalRes.getValue();
				} catch (EvaluationException e) {
					Activator.error(e.getMessage(), e);
				}
			}
			
			return 0;
		}

		@Override
		public String getTimeUnit() {
			return "s";
		}
		
	}
	
	
	@Override
	public void engineAboutToStart(IExecutionEngine<?> engine) {
		IEngineAddon.super.engineAboutToStart(engine);
		
		if(_currentAleEngine != null ) {
			// ignore double call
			return;
		}
		_currentAleEngine = (AleEngine)engine;
		
		// grab the frame from the view or fallback to a JFrame (ie. new window)
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				IViewPart v = null;
				try{
					v = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(OscilloscupView.ID);
					frame = ((OscilloscupView)v).frame;
					frame.removeAll();
				} catch(PartInitException e){
					Activator.warn(e.getMessage()+ "; falling back to JFrame", e);
					frame = new JFrame();
				}
				
			}
		});
		frame.setSize(800, 600);
		
		ArrayList<Property<FunctionBlockDataPort>> allProperties = new ArrayList<Property<FunctionBlockDataPort>>();
		
		_currentAleEngine = (AleEngine)engine;
		_currentAleInterpreter = _currentAleEngine.getInterpreter();
		if(_currentAleEngine != null && _currentAleEngine.getExecutionContext() != null) {
			Resource res = _currentAleEngine.getExecutionContext().getResourceModel();
			if(res.getContents().get(0) instanceof FCLModel) {
				FCLModel fclModel = (FCLModel)res.getContents().get(0);
				
				if(fclModel.getDataflow().getTimeReference() != null) {
					globalClock = fclModel.getDataflow().getTimeReference().getObservableVar();
				}
				
				List<FunctionBlockDataPort> ports = fclModel.getDataflow().getFunctionPorts().stream()
					.filter(p -> p.getDirection() == DirectionKind.OUT && p instanceof FunctionBlockDataPort)
					.map(obj -> (FunctionBlockDataPort) obj)
					.collect(Collectors.toList());
				
				
				/*for(FunctionBlockDataPort port : ports) {
						PortOscilloProperty inputProp = new PortOscilloProperty();
						inputProp.setName(port.getName());
						inputProp.setReformatter(new PropertyValueFormatter.PrettyDecimals(2));
						inputProp.setPlotBounds(0d, null);
						allProperties.add(inputProp);
					};*/
				PortOscilloProperty inputProp = new PortOscilloProperty();
				inputProp.setName("currentValue");
				inputProp.setReformatter(new PropertyValueFormatter.PrettyDecimals(3));
//				inputProp.setPlotBounds(0d, null);
				inputProp.setClock(new FCLGlobalClock());
				allProperties.add(inputProp);
				multiscope = new Multiscope<FunctionBlockDataPort>(allProperties){
					@Override
					protected String getRowNameFor(FunctionBlockDataPort i)
					{
						return i.getName();
					}
					@Override
					protected int getNbPointsInSlidingWindow(FunctionBlockDataPort row,
							Property<FunctionBlockDataPort> p)
					{
						return 1000;
					}
					@Override
					protected DataElementRenderer getSpecificRenderer(FunctionBlockDataPort row,
							Property<FunctionBlockDataPort> property) {
						return new ConnectedLineFigureRenderer();
					}
					
				};
				multiscope.setRows(ports);
				multiscope.setRefreshPeriodMs(-1);
				if(frame instanceof JFrame) {
					// this is the fallback JFrame,
					((JFrame)frame).setContentPane(multiscope);
				} else {
					frame.add(multiscope);
				}
				frame.setVisible(true);
				
			}
		}
	}


	class PortOscilloProperty extends Property<FunctionBlockDataPort>{

		@Override
		public Object getRawValue(FunctionBlockDataPort target) {
			try {
				IEvaluationResult evalRes = getAleDynamicEvaluator().evaluateExpression(
						target, 
						"ale: self.currentValue.rawValue()");
				return evalRes.getValue();
			} catch (EvaluationException e) {
				Activator.error(e.getMessage(), e);
			}
			
			return null;
		}
		
	}


	@Override
	public void engineAboutToDispose(IExecutionEngine<?> engine) {
		IEngineAddon.super.engineAboutToDispose(engine);
		if(frame instanceof JFrame) {
			// this is the fallback JFrame, close it
			frame.dispose();
		}
		this._currentAleEngine = null;
		_currentAleInterpreter = null;
		globalClock = null;
		_aleDynamicEvaluator = null;
		_dynaccess = null;
	}




	@Override
	public void aboutToExecuteStep(IExecutionEngine<?> engine, Step<?> stepToExecute) {
		IEngineAddon.super.aboutToExecuteStep(engine, stepToExecute);
		
		if(stepToExecute.getMseoccurrence().getMse().getAction().getName()
				.equals("doMainLoop")) {
			multiscope.newStep();
		}
		
	}
	
	public AleEngine getEngine() 
	{
		return _currentAleEngine;
	}
	
	AleDynamicAccessor _dynaccess = null;
	ALEInterpreter _currentAleInterpreter = null;
	private AleDynamicAccessor updateDynamicAccessor() {
		if(_dynaccess==null) {
			_dynaccess = new AleDynamicAccessor(getEngine().getInterpreter(),getEngine().getModelUnits());
		}
		return _dynaccess;
	}
	
	
	ALEDynamicExpressionEvaluator _aleDynamicEvaluator = null;
	private ALEDynamicExpressionEvaluator getAleDynamicEvaluator() {
		if(_aleDynamicEvaluator == null) {
			_aleDynamicEvaluator = new ALEDynamicExpressionEvaluator(getEngine().getInterpreter());
		}
		return _aleDynamicEvaluator;
	}

}
