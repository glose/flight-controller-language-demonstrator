package fr.inria.glose.fcl.alefcl.xdsml.views;

import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.gemoc.trace.commons.model.trace.Step;
import org.eclipse.gemoc.xdsmlframework.api.core.IExecutionEngine;
import org.eclipse.gemoc.xdsmlframework.api.engine_addon.IEngineAddon;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

import fr.inria.glose.fcl.alefcl.xdsml.Activator;


public class FCLInputViewAddon implements IEngineAddon {

	public FCLInputViewAddon() {
		
	}


	@Override
	public void aboutToExecuteStep(IExecutionEngine<?> engine, Step<?> stepToExecute) {
		IEngineAddon.super.aboutToExecuteStep(engine, stepToExecute);
		
		
		long elapsedTimeSinceLastNotif = System.currentTimeMillis() - lastRefresh;
		if(elapsedTimeSinceLastNotif >= intervalBetweenRefresh) {
			// trigger a notification now
			if(refreshTimer != null) {
				refreshTimer.cancel();
			}
			lastRefresh = System.currentTimeMillis();
			refreshView();
		} else {
			
			if(refreshTimer != null) {
				// if a timer is already pending , ignore notification	
				// System.err.println("ignoring FCLInputView refresh due to already pending refresh");
			} else {
				// no timer pending, trigger notification after a delay using timer
				refreshTimer = new Timer("SiriusNotificationTimer");
				TimerTask task = new TimerTask() {
			        public void run() {
			        	lastRefresh = System.currentTimeMillis();
			        	refreshTimer = null;
			        	refreshView();
						//System.err.println("FCLInputView refresh after delay");
						this.cancel();
			        }
			    };
			    refreshTimer.schedule(task, intervalBetweenRefresh - elapsedTimeSinceLastNotif);
			}
		}
		
	}
		
	private void refreshView() {
		Display.getDefault().asyncExec(new Runnable() 
		{
			@Override
			public void run() 
			{
				try {
					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			
					IViewPart viewPart =page.findView(FCLInputView.ID);
					//IViewPart viewPart = page.showView(FCLInputView.ID);
				
					if(viewPart != null && viewPart instanceof FCLInputView) {
						((FCLInputView)viewPart).updateView();
					} 
				} catch(Exception e) {
					Activator.error(e.getMessage(),e);
				}
			}				
		});
	}
	
	protected long lastRefresh = System.currentTimeMillis();
	public int intervalBetweenRefresh = 1000;
	protected Timer refreshTimer;
	
	@Override
	public void engineAboutToDispose(IExecutionEngine<?> engine) {
		IEngineAddon.super.engineAboutToDispose(engine);
		
		Display.getDefault().asyncExec(new Runnable() 
		{
			@Override
			public void run() 
			{
				try {
					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			
					IViewPart viewPart =page.findView(FCLInputView.ID);
					//IViewPart viewPart = page.showView(FCLInputView.ID);
				
					if(viewPart != null && viewPart instanceof FCLInputView) {
						((FCLInputView)viewPart).disposeEngine();
					} 
				} catch(Exception e) {
					Activator.error(e.getMessage(),e);
				}
			}				
		});
	}

}
