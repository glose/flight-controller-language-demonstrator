package fr.inria.glose.fcl.alefcl.xdsml.views.dialogs;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class NewValueDialog extends TitleAreaDialog {

    private Text txtValueString;

    private String valueString;
    
    private String portName;
    private String initialValue;

    public NewValueDialog(Shell parentShell, String portName, String initialValue) {
        super(parentShell);
    	this.portName = portName;
    	this.initialValue = initialValue;
    }

    @Override
    public void create() {
        super.create();
        setTitle("New Value for " +this.portName +" port");
        setMessage("Set the new value for external port", IMessageProvider.INFORMATION);
    }

    @Override
    protected Control createDialogArea(Composite parent) {
        Composite area = (Composite) super.createDialogArea(parent);
        Composite container = new Composite(area, SWT.NONE);
        container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        GridLayout layout = new GridLayout(2, false);
        container.setLayout(layout);

        createValueField(container);

        return area;
    }

    private void createValueField(Composite container) {
        Label lbtFirstName = new Label(container, SWT.NONE);
        lbtFirstName.setText("Value");

        GridData dataFirstName = new GridData();
        dataFirstName.grabExcessHorizontalSpace = true;
        dataFirstName.horizontalAlignment = GridData.FILL;

        txtValueString = new Text(container, SWT.BORDER);
        txtValueString.setLayoutData(dataFirstName);
        txtValueString.setText(initialValue);
    }




    @Override
    protected boolean isResizable() {
        return true;
    }

    // save content of the Text fields because they get disposed
    // as soon as the Dialog closes
    private void saveInput() {
    	valueString = txtValueString.getText();
    }

    @Override
    protected void okPressed() {
        saveInput();
        super.okPressed();
    }

    public String getValueString() {
        return valueString;
    }

}
