package fr.inria.glose.fcl.alefcl.xdsml.services;

public class FCLTypeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6392786436552292674L;

	public FCLTypeException(String message) {
		super(message);
	}
	
	
}