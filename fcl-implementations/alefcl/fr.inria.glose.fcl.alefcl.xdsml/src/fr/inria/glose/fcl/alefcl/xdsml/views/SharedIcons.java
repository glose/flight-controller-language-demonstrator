package fr.inria.glose.fcl.alefcl.xdsml.views;

import java.util.HashMap;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

public class SharedIcons {

	public static ImageDescriptor IN_PORT = ImageDescriptor.createFromFile(SharedIcons.class, "/icons/port_in_icon.png");
	public static ImageDescriptor OUT_PORT = ImageDescriptor.createFromFile(SharedIcons.class, "/icons/port_out_icon.png");
	public static ImageDescriptor INOUT_PORT = ImageDescriptor.createFromFile(SharedIcons.class, "/icons/port_inout_icon.png");
	public static ImageDescriptor EVENT = ImageDescriptor.createFromFile(SharedIcons.class, "/icons/Lightning-16.png");

	static HashMap<ImageDescriptor, Image> imageMap = new HashMap<ImageDescriptor, Image>();
	
	static public Image getSharedImage(ImageDescriptor descriptor){
		Image res = imageMap.get(descriptor);
		if(res == null){
			res = descriptor.createImage();
			imageMap.put(descriptor, res);
		}
		return res;
	}
}
