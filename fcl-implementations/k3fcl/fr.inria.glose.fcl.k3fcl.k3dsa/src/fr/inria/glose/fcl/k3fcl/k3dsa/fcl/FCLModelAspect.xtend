package fr.inria.glose.fcl.k3fcl.k3dsa.fcl

import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import fr.inria.glose.fcl.model.fcl.FCLModel
import fr.inria.glose.fcl.model.fcl.ModeAutomata
import fr.inria.glose.fcl.model.fcl.Transition
import fr.inria.glose.fcl.model.fcl.Mode
import fr.inria.glose.fcl.model.fcl.Action
import fr.inria.glose.fcl.model.fcl.Event
import fr.inria.glose.fcl.model.fcl.Expression
import fr.inria.glose.fcl.model.fcl.Procedure
import fr.inria.glose.fcl.model.fcl.ActionBlock
import fr.inria.glose.fcl.model.fcl.DataStructType
import fr.inria.glose.fcl.model.fcl.NamedElement
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort
import fr.inria.glose.fcl.model.fcl.DataType
import fr.inria.glose.fcl.model.fcl.ProcedureParameter
import fr.inria.glose.fcl.model.fcl.FunctionPort
import fr.inria.glose.fcl.model.fcl.FunctionConnector
import fr.inria.glose.fcl.model.fcl.ExternalEvent
import fr.inria.glose.fcl.model.fcl.BinaryExpression
import fr.inria.glose.fcl.model.fcl.UnaryExpression
import fr.inria.glose.fcl.model.fcl.Cast
import fr.inria.glose.fcl.model.fcl.Call
import fr.inria.glose.fcl.model.fcl.PrimitiveAction
import fr.inria.glose.fcl.model.fcl.Assignment
import fr.inria.glose.fcl.model.fcl.ControlStructureAction
import fr.inria.glose.fcl.model.fcl.IfAction
import fr.inria.glose.fcl.model.fcl.WhileAction
import fr.inria.glose.fcl.model.fcl.ProcedureCall
import fr.inria.glose.fcl.model.fcl.VarDecl
import fr.inria.glose.fcl.model.fcl.CallDeclarationReference
import fr.inria.glose.fcl.model.fcl.FunctionPortReference
import fr.inria.glose.fcl.model.fcl.BasicOperatorUnaryExpression
import fr.inria.glose.fcl.model.fcl.BasicOperatorBinaryExpression
import fr.inria.glose.fcl.model.fcl.ValueType
import fr.inria.glose.fcl.model.fcl.IntegerValueType
import fr.inria.glose.fcl.model.fcl.FloatValueType
import fr.inria.glose.fcl.model.fcl.BooleanValueType
import fr.inria.glose.fcl.model.fcl.StringValueType
import fr.inria.glose.fcl.model.fcl.EnumerationValueType
import fr.inria.glose.fcl.model.fcl.EnumerationLiteral
import fr.inria.glose.fcl.model.fcl.Assignable
import fr.inria.glose.fcl.model.fcl.CallableDeclaration
import fr.inria.glose.fcl.model.fcl.Function
import fr.inria.glose.fcl.model.fcl.FunctionTimeReference
import fr.inria.glose.fcl.model.fcl.ExternalProcedure
import fr.inria.glose.fcl.model.fcl.FCLProcedure
import fr.inria.glose.fcl.model.fcl.FCLProcedureReturn
import fr.inria.glose.fcl.model.fcl.JavaProcedure
import fr.inria.glose.fcl.model.fcl.PythonProcedure
import fr.inria.glose.fcl.model.fcl.IntegerValue
import fr.inria.glose.fcl.model.fcl.FloatValue
import fr.inria.glose.fcl.model.fcl.StringValue
import fr.inria.glose.fcl.model.fcl.BooleanValue
import fr.inria.glose.fcl.model.fcl.FunctionVarDecl
import fr.inria.glose.fcl.model.fcl.Value
import fr.inria.glose.fcl.model.fcl.CallConstant
import fr.inria.glose.fcl.model.fcl.EnumerationValue
import fr.inria.glose.fcl.model.fcl.FMUFunction
import fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort
import fr.inria.glose.fcl.model.fcl.UnityFunction
import fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort
import fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper
import fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper
import fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext
import fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry
import fr.inria.glose.fcl.model.fcl.CallPrevious
import fr.inria.glose.fcl.model.fcl.DataStructProperty
import fr.inria.glose.fcl.model.fcl.DataStructPropertyReference
import fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment
import fr.inria.glose.fcl.model.fcl.CallDataStructProperty
import fr.inria.glose.fcl.model.fcl.NewDataStruct
import fr.inria.glose.fcl.model.fcl.StructValue
import fr.inria.glose.fcl.model.fcl.StructPropertyValue
import fr.inria.glose.fcl.model.fcl.Log


import static extension fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.TransitionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionBlockAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructTypeAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionConnectorAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExternalEventAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BinaryExpressionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnaryExpressionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CastAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.PrimitiveActionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignmentAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ControlStructureActionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IfActionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.WhileActionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureCallAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDeclarationReferenceAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorUnaryExpressionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorBinaryExpressionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueTypeAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueTypeAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueTypeAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueTypeAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueTypeAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueTypeAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationLiteralAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionTimeReferenceAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExternalProcedureAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureReturnAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.JavaProcedureAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallConstantAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionDataPortAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallPreviousAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyReferenceAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDataStructPropertyAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NewDataStructAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructPropertyValueAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.LogAspect.*

import org.eclipse.emf.common.util.EList
import fr.inria.diverse.k3.al.annotationprocessor.Main
import fr.inria.diverse.k3.al.annotationprocessor.Step
import fr.inria.diverse.k3.al.annotationprocessor.InitializeModel
import java.util.List
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EventAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect.*
import fr.inria.glose.fcl.model.fcl.interpreter_vm.EventOccurrence
import fr.inria.diverse.k3.al.annotationprocessor.ReplaceAspectMethod
import java.io.BufferedReader
import fr.inria.glose.fcl.model.fcl.DirectionKind
import java.io.FileReader
import org.eclipse.gemoc.commons.eclipse.core.resources.IFileUtils

@Aspect(className=FCLModel)
class FCLModelAspect /* extends NamedElementAspect */ {
	
	public List<EventOccurrence> receivedEvents;
	public List<ProcedureContext> procedureStack;
	
	// used to improve console display
	public String indentation;
	
	@Main
	def void main() {
		_self.devInfo('-> main() '+_self.name);
		// while not on a final mode
		while(! _self.modeAutomata.currentMode.final){ 
			_self.manageInputScenario();
			_self.evaluateModeAutomata();
			_self.evaluateDataflow();
		}
	}
	
	// step for the sequential engine in order to have a nice debug stack view separating automata and data flow
	// DSE for the concurrent engine
	@Step
	@ReplaceAspectMethod
	def void evaluateModeAutomata(){
		_self.info('### Evaluate mode automata '+_self.modeAutomata);
		_self.modeAutomata.evalModeStateMachine
	}
	
	// step for the sequential engine in order to have a nice debug stack view separating automata and data flow
	@Step
	def void evaluateDataflow(){
			
		_self.dataflow.evalFunction(); 
		// process delayed connectors
		_self.dataflow.sendDataThroughAllDelayedConnectors();
		// no one will update currentValues anymore,  
		_self.updatePrevValues();
	}
	
	// DSE for the concurrent engine
	@ReplaceAspectMethod
	def void propagateDelayedResults(){
		_self.sendDataThroughAllDelayedConnectors()
		_self.updatePrevValues()
	}
	def void sendDataThroughAllDelayedConnectors(){
		_self.info('process delayed connections');
		_self.dataflow.sendDataThroughAllDelayedConnectors();
	}
	def void updatePrevValues(){
		_self.info('update previous values');  
		_self.dataflow.updatePrevValues();
	}
		
	// @DSE This is a DSE in the  concurrent engine
	@Step 												
	@InitializeModel									
	def void initializeModel(EList<String> input){
		_self.devInfo('-> initializeModel() input='+input.get(0));
		
		_self.indentation = ''
		
		// initialize the currentMode
		_self.modeAutomata.currentMode = _self.modeAutomata.initialMode;
		
		// init default values of functionVars and connectors
		_self.dataflow.initFunctionVars();
		_self.dataflow.initPortsDefaultValues();
		
		// on start, previous values are set to current value 
		_self.dataflow.updatePrevValues();
		
		// procedure context
		_self.procedureStack = newArrayList
		
		// event context
		_self.receivedEvents = newArrayList
		
		// init FMUs
		for(fmuFunction : _self.dataflow.eAllContents.filter(FMUFunction).toList){
			fmuFunction.initializeFMURuntime();
		}
		// init Unity
		for(fmuFunction : _self.dataflow.eAllContents.filter(UnityFunction).toList){
			fmuFunction.initializeUnity();
		}
		
		
		// load or create a kind of scenario of input events
		if(!input.get(0).isNullOrEmpty){
			// try to find the input scenario
			val f = IFileUtils.getIFileFromWorkspaceOrFileSystem(input.get(0))
			_self.inputScenarioReader = new BufferedReader(new FileReader(f.location.toOSString))
		}
	}
	
	var BufferedReader inputScenarioReader = null; 
	
	@Step
	def void manageInputScenario(){
		// if there is an input scenario
		if(_self.inputScenarioReader !== null ) {
			// read and apply one line
			var line = _self.inputScenarioReader.readLine
			while(!line.nullOrEmpty && line.startsWith("#")){
				line = _self.inputScenarioReader.readLine
			}
			if(line !== null){
				val valuesString = line.split(",")
				val inPorts = _self.dataflow.functionPorts.filter[ port | port.getDirection() !== DirectionKind.OUT]
														  .filter(FunctionBlockDataPort).toList
				for (var i = 0 ; i < inPorts.length ; i++) {
					inPorts.get(i).valueFromString = valuesString.get(i)
				}
				// now deal with last entry: event occurence
				val receivedEvents  = valuesString.last.replaceAll("\\[","").replaceAll("\\]","").split(",")
				for(evtName : receivedEvents){
					val evt = _self.events.findFirst[evt | evt.name == evtName]
					if(evt !== null) {
						evt.send
					}
				}
			}
		}
	}
	
	def void consumeEventOccurence(){
		_self.devInfo('Consuming event occurrence '+_self.receivedEvents.get(0).event.name);
		_self.receivedEvents.remove(0);
	}
	
	// some helpers method used by logs
	def String pushIndent(){
		if(_self.indentation === null){
			_self.indentation = '';
		}
		_self.indentation = _self.indentation + ' ';
		return _self.indentation;
	}
	
	def String popIndent(){
		var String result = _self.indentation;
		if(_self.indentation.length > 1) {
			_self.indentation = _self.indentation.substring(1,_self.indentation.length - 1);
		} else if(_self.indentation.length == 1){
			_self.indentation = '';
		}
		return result;
	}
}