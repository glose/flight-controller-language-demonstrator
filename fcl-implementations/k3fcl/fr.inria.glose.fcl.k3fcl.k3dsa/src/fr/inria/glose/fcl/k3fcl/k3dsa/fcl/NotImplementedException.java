package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

public class NotImplementedException extends Exception {

	NotImplementedException(String string) {
	  super(string);
	}

}
