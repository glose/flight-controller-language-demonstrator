package fr.inria.glose.fcl.k3fcl.k3dsa.fcl

import fr.inria.diverse.k3.al.annotationprocessor.Aspect
import fr.inria.diverse.k3.al.annotationprocessor.Step
import fr.inria.glose.fcl.k3fcl.k3dsa.Activator
import fr.inria.glose.fcl.model.fcl.Action
import fr.inria.glose.fcl.model.fcl.ActionBlock
import fr.inria.glose.fcl.model.fcl.Assignable
import fr.inria.glose.fcl.model.fcl.Assignment
import fr.inria.glose.fcl.model.fcl.BasicOperatorBinaryExpression
import fr.inria.glose.fcl.model.fcl.BasicOperatorUnaryExpression
import fr.inria.glose.fcl.model.fcl.BinaryExpression
import fr.inria.glose.fcl.model.fcl.BinaryOperator
import fr.inria.glose.fcl.model.fcl.BooleanValue
import fr.inria.glose.fcl.model.fcl.BooleanValueType
import fr.inria.glose.fcl.model.fcl.Call
import fr.inria.glose.fcl.model.fcl.CallConstant
import fr.inria.glose.fcl.model.fcl.CallDataStructProperty
import fr.inria.glose.fcl.model.fcl.CallDeclarationReference
import fr.inria.glose.fcl.model.fcl.CallPrevious
import fr.inria.glose.fcl.model.fcl.CallableDeclaration
import fr.inria.glose.fcl.model.fcl.Cast
import fr.inria.glose.fcl.model.fcl.ControlStructureAction
import fr.inria.glose.fcl.model.fcl.DataStructProperty
import fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment
import fr.inria.glose.fcl.model.fcl.DataStructPropertyReference
import fr.inria.glose.fcl.model.fcl.DataStructType
import fr.inria.glose.fcl.model.fcl.DataType
import fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry
import fr.inria.glose.fcl.model.fcl.DirectionKind
import fr.inria.glose.fcl.model.fcl.EnumerationLiteral
import fr.inria.glose.fcl.model.fcl.EnumerationValue
import fr.inria.glose.fcl.model.fcl.EnumerationValueType
import fr.inria.glose.fcl.model.fcl.Event
import fr.inria.glose.fcl.model.fcl.Expression
import fr.inria.glose.fcl.model.fcl.ExternalEvent
import fr.inria.glose.fcl.model.fcl.ExternalProcedure
import fr.inria.glose.fcl.model.fcl.FCLModel
import fr.inria.glose.fcl.model.fcl.FCLProcedure
import fr.inria.glose.fcl.model.fcl.FCLProcedureReturn
import fr.inria.glose.fcl.model.fcl.FMUFunction
import fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort
import fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper
import fr.inria.glose.fcl.model.fcl.FclFactory
import fr.inria.glose.fcl.model.fcl.FloatValue
import fr.inria.glose.fcl.model.fcl.FloatValueType
import fr.inria.glose.fcl.model.fcl.Function
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort
import fr.inria.glose.fcl.model.fcl.FunctionConnector
import fr.inria.glose.fcl.model.fcl.FunctionPort
import fr.inria.glose.fcl.model.fcl.FunctionPortReference
import fr.inria.glose.fcl.model.fcl.FunctionTimeReference
import fr.inria.glose.fcl.model.fcl.FunctionVarDecl
import fr.inria.glose.fcl.model.fcl.IfAction
import fr.inria.glose.fcl.model.fcl.IntegerValue
import fr.inria.glose.fcl.model.fcl.IntegerValueType
import fr.inria.glose.fcl.model.fcl.JavaProcedure
import fr.inria.glose.fcl.model.fcl.Log
import fr.inria.glose.fcl.model.fcl.Mode
import fr.inria.glose.fcl.model.fcl.ModeAutomata
import fr.inria.glose.fcl.model.fcl.NewDataStruct
import fr.inria.glose.fcl.model.fcl.PrimitiveAction
import fr.inria.glose.fcl.model.fcl.Procedure
import fr.inria.glose.fcl.model.fcl.ProcedureCall
import fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext
import fr.inria.glose.fcl.model.fcl.ProcedureParameter
import fr.inria.glose.fcl.model.fcl.StringValue
import fr.inria.glose.fcl.model.fcl.StringValueType
import fr.inria.glose.fcl.model.fcl.StructPropertyValue
import fr.inria.glose.fcl.model.fcl.StructValue
import fr.inria.glose.fcl.model.fcl.Transition
import fr.inria.glose.fcl.model.fcl.UnaryExpression
import fr.inria.glose.fcl.model.fcl.UnaryOperator
import fr.inria.glose.fcl.model.fcl.UnityFunction
import fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort
import fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper
import fr.inria.glose.fcl.model.fcl.Value
import fr.inria.glose.fcl.model.fcl.ValueType
import fr.inria.glose.fcl.model.unity.UnityInstance
import java.lang.reflect.Method
import java.util.List
import java.util.Locale
import org.eclipse.core.resources.IFile
import org.eclipse.gemoc.commons.eclipse.core.resources.IFileUtils
import org.eclipse.swt.widgets.Display
import org.javafmi.modeldescription.ScalarVariable
import org.javafmi.proxy.Status
import org.javafmi.wrapper.generic.Simulation2
import org.javafmi.wrapper.v2.Access

import static extension fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructPropertyValueAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.TransitionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionDataPortAspect.*
import static extension fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect.*
import static extension org.eclipse.gemoc.commons.eclipse.emf.EObjectUtil.*
import fr.inria.glose.fcl.model.fcl.VarDecl
import fr.inria.glose.fcl.model.fcl.WhileAction
import fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmFactory
import fr.inria.glose.fcl.model.fcl.interpreter_vm.EventOccurrence
import fr.inria.diverse.k3.al.annotationprocessor.ReplaceAspectMethod

//###############
// FunctionAspect
//###############
@Aspect(className=Function)
class FunctionAspect /*extends NamedElementAspect*/ {

	// DSE for the concurrent engine
	def void dse_startEvalFunction(){
		_self.startEvalFunction()
	}
	def void startEvalFunction(){
		if(_self.eContainer instanceof FCLModel) {
			// this is the main Function
			val FCLModel fclModel = _self.eContainer() as FCLModel
			_self.info('### Evaluating Main dataflow '+_self.functionName())
			_self.devInfo('with the following enabled functions:\n\t'+fclModel.modeAutomata.currentMode.enabledFunctions.map[f|f.getQualifiedName()].join('\n\t'));			
		
		} else {
			_self.info('evaluating sub function '+_self.functionName()+'...')
		}
		
		if(_self.subFunctions.size() > 0){	
			// propagateInput()  (to inner functions)
			_self.sendInputDataToActiveInternalFunctions();
		}
		if(_self.timeReference !== null){
			_self.devInfo('Increment time reference for function '+_self.functionName());
			val FunctionVarDecl timeVariable = _self.timeReference.observableVar;
			
			timeVariable.assign(
					timeVariable.currentValue.plus(
						_self.timeReference.increment.evaluate()
					)
			);
		}
		if(_self.action !== null) {
			val ProcedureContext procedureContext = Interpreter_vmFactory.eINSTANCE.createProcedureContext();
			// push context
			_self.eContainerOfType(FCLModel).procedureStack.add(procedureContext);
			// run the action in this context 
			_self.action.doAction();
			// pop context
			_self.eContainerOfType(FCLModel).procedureStack.remove(procedureContext);
		}
	}
	
	// DSE for the concurrent engine
	def void dse_stopEvalFunction(){
		_self.stopEvalFunction()
	}
	
	def void stopEvalFunction(){
		// propagateNonDelayedOutput() 
		// for all no delayed outgoing connections, send data
		_self.sendDataToActiveNonDelayedPorts();
	}
		
	@Step
	def void evalFunction(){
		_self.startEvalFunction();
		if(_self.subFunctions.size() > 0){
			// run-to-completion of nested functions
			// topological sort of functions
			val List<Function> sortedF = _self.sortedSubfunctions();
			//_self.info('   will evaluate functions in the following order: '+sortedF.map[f|'"'+f.name+'"'].join(', '));
			
			for(f : sortedF){
				f.evalFunction();
			}
		}
		_self.stopEvalFunction();
	}	

	//@step
	def void initFunctionVars(){
		for(functionVar : _self.functionVarDecls){
			if(functionVar.initialValue !== null){
			 	val Value defaultValue = functionVar.initialValue.evaluate();
			 	functionVar.assign(defaultValue);
			}	
		}
		for(subFunction : _self.subFunctions){
			subFunction.initFunctionVars();
		}
	}
	
	//@step
	def void initPortsDefaultValues(){
		for(port : _self.functionPorts.filter(FunctionBlockDataPort)){
		 	if(port.defaultInputValue  !== null){
		 		val Value defaultValue =  port.defaultInputValue.evaluate();
		 		port.assign(defaultValue);
			}
		}
		for(subFunction : _self.subFunctions){
			subFunction.initPortsDefaultValues();
		}
	}
	
	/*
	 * store previousValue into previousValue for all function variables and ports
	 */
	@Step
	def void updatePrevValues(){
		//self.important('updatePrevValues() of '+self.functionName());
		for(port : _self.functionPorts.filter(FunctionBlockDataPort)){
		 	port.previousValue = port.currentValue;
		}
		for(functionVar : _self.functionVarDecls){
			functionVar.previousValue = functionVar.currentValue;	
		}
		for(subFunction : _self.subFunctions){
			subFunction.updatePrevValues();
		}
	}
	
	@Step
	def void sendDataThroughAllDelayedConnectors(){
		// for all delayed outgoing connections, send data	
		val List<FunctionBlockDataPort> outputPorts = _self.functionPorts.
				filter[p | p.direction == DirectionKind::OUT
					   	|| p.direction == DirectionKind::IN_OUT
				].map[pp | pp.getTargetDataPort()].toList; 
		// find connection (xtext does not maintains eOpposite :-( )
		for(outPort : outputPorts){
				val List<FunctionConnector> connectors = _self.eContainerOfType(FCLModel).allConnectors()
					.filter[c |
						c.delayed
						&& c.emitter == outPort
						&& c.receiver.eContainerOfType(Function).isEnabled()
					].toList;	
				for(c : connectors){
					if(outPort.currentValue !== null){
						_self.important(c.receiver.getTargetDataPort().getQualifiedName()+' receives '+outPort.currentValue.valueToString());
						c.receiver.getTargetDataPort().assign(outPort.currentValue);
					} else {
						_self.error('Out port '+outPort.getQualifiedName()+ ' has no value. Please verify that your model assigns a value to it or defines a default value.');
						throw new FCLException('Cannot assign null value from Out port '+outPort.getQualifiedName()+ ' to In port '+c.receiver.getQualifiedName());						
					}
				}
		}
		for(func : _self.subFunctions){
			func.sendDataThroughAllDelayedConnectors();
		}

	}
	
	//@Step
	def void sendInputDataToActiveInternalFunctions(){

		
		val List<FunctionBlockDataPort> inputPorts = _self.functionPorts.
				filter[p | p.direction == DirectionKind::IN
					   	|| p.direction == DirectionKind::IN_OUT
				].map[pp | pp.getTargetDataPort()].toList;
		// find connection (xtext does not maintains eOpposite :-( )
		for(inPort : inputPorts){
				val List<FunctionConnector> allconnectors = _self.eContainerOfType(FCLModel).allConnectors();
				val List<FunctionConnector> connectors = allconnectors.
					filter[c |
						(! c.delayed)
						&& c.emitter == inPort
						&& c.receiver.eContainerOfType(Function).isEnabled()
						&& _self.subFunctions.contains(c.receiver.eContainerOfType(Function))
					].toList;	
				
				for(c : connectors){
					if(inPort.currentValue !== null){
						c.receiver.getTargetDataPort().assign(inPort.currentValue);
					} else {
						_self.error('In port '+inPort.getQualifiedName()+ ' has no value. Please verify that your model assigns a value to it or defines a default value.');
						throw new FCLException('Cannot assign null value from Out port '+inPort.getQualifiedName()+ ' to In port '+c.receiver.getQualifiedName());						
					}
				}
		}
	}
	
	def void sendDataToActiveNonDelayedPorts(){

		// for all no delayed outgoing connections, send data
		val List<FunctionBlockDataPort> outputPorts = _self.functionPorts.
				filter[p | p.direction == DirectionKind::OUT
					   	|| p.direction == DirectionKind::IN_OUT
				].map[pp | pp.getTargetDataPort()].toList; 
		// find connection (xtext does not maintains eOpposite :-( )
		for(outPort : outputPorts){
				val List<FunctionConnector> connectors = _self.eContainerOfType(FCLModel).allConnectors().
					filter[c |
						(! c.delayed)
						&& c.emitter == outPort
						&& c.receiver.eContainerOfType(Function).isEnabled()
					].toList;	
				
				for(c : connectors){
					if(outPort.currentValue !== null){
						c.receiver.getTargetDataPort().assign(outPort.currentValue);
					} else {
						_self.error('Out port '+outPort.getQualifiedName()+ ' has no value. Please verify that your model assigns a value to it or defines a default value.');
						throw new FCLException('Cannot assign null value from Out port '+outPort.getQualifiedName()+ ' to In port '+c.receiver.getQualifiedName());						
					}
				}
		}

	}
	
	/**
	 * indicates if this function is enabled in the current mode
	 */
	 @ReplaceAspectMethod
	def boolean isEnabled(){
		if(_self.eContainer() instanceof FCLModel){
			// main function  is always enabled
			return true;
		} else {
			return _self.eContainerOfType(FCLModel).
				modeAutomata.currentMode.enabledFunctions.contains(_self);		
		}	
	}
	def String functionName(){
		if(_self.eContainer() instanceof FCLModel){
			return 'MasterFunction';
		} else {
			return _self.name;
		}
	} 
	
		/**
	 * returns the subfunctions of self in a topologically sorted order
	 * consider only enabled function in current mode
	 * Kahn's algorithm from https://en.wikipedia.org/wiki/Topological_sorting
	 */
	def List<Function> sortedSubfunctions(){
		// L <- Empty list that will contain the sorted elements
		val List<Function> resList = newArrayList; //Empty list that will contain the sorted elements
		val List<FunctionConnector> consideredConnectorsSequence = 	_self.functionConnectors 
			.filter[connector | ! connector.delayed 
				&& (_self.subFunctions.contains(connector.emitter.eContainerOfType(Function))) 
				&& (_self.subFunctions.contains(connector.receiver.eContainerOfType(Function)))
			 	&& (connector.emitter.eContainerOfType(Function).isEnabled())
			 	&& (connector.receiver.eContainerOfType(Function).isEnabled())	
				
			].toList; /* ignore subfunction not active in this mode */
			// ignore	and (connector.receiver.eContainer(fcl::Function).isEnabled())
		//_self.devDebug('Involved connectors are:\n  '+consideredConnectorsSequence.functionConnectors->collect(s | s.emitter.getQualifiedName()+' -> '+s.receiver.getQualifiedName())->sep(',\n  '));
			
		// S <- Set of all nodes with no incoming edge
		val List<Function> nodeWithNoIncomingEdgeSet = newArrayList;
		val List<Function> functionsWithIncomingEdge = consideredConnectorsSequence.
				map[fc | 
					fc.receiver.eContainerOfType(Function)
				].filter[fc2 | fc2.isEnabled()].toList;
		
		for(f : _self.subFunctions){
			if( ! functionsWithIncomingEdge.contains(f)){
				if(f.isEnabled()){
					nodeWithNoIncomingEdgeSet.add(f);	
				}
			}
		}
		// while S is non-empty do
		while(! nodeWithNoIncomingEdgeSet.isEmpty()){
			// remove a node n from S
			val Function n = nodeWithNoIncomingEdgeSet.get(0);
			nodeWithNoIncomingEdgeSet.remove(0);
			// add n to tail of L
			//self.devDebug('\tsortedSubfunctions add '+n.name+' to tail of L ');
			resList.add(n);
			// for each node m with an edge e from n to m do
			val List<FunctionConnector> connectorEmittedByN = 
				consideredConnectorsSequence.filter[c | c.emitter.eContainerOfType(Function) == n].toList;
			for(fc : connectorEmittedByN){
				// remove edge e from the graph
				consideredConnectorsSequence.remove(fc);
				//self.devDebug('\tsortedSubfunctions remove '+fc.getQualifiedName()+' from the graph');
				val Function m = fc.receiver.eContainerOfType(Function);
				// if m has no other incoming edges then
				if(! consideredConnectorsSequence.exists[fc1 | fc1.receiver.eContainerOfType(Function) == m]
				){
	            	// insert m into S
	            	//self.devDebug('\tsortedSubfunctions insert '+m.name+' into S');
	            	nodeWithNoIncomingEdgeSet.add(m);
            	}
			}
		}
		
		// if graph has edges then
		if(! consideredConnectorsSequence.isEmpty()){
	    	// 	return error   (graph has at least one cycle)
			_self.error('Found at least one cycle in sub-functions of '+_self.functionName());
			_self.error('Connectors involved in cycle(s) are:\n'+consideredConnectorsSequence.map[s | s.emitter.getQualifiedName()+' -> '+s.receiver.getQualifiedName()].join(',   \n'));
			
			throw new FCLException('No Topological order can be found');
	    }
		else { 
			//  	 return L   (a topologically sorted order)
			return resList;
		}
		
	} 
}


@Aspect(className=FMUFunction)
class FMUFunctionAspect extends FunctionAspect {
	
	public FMUSimulationWrapper fmuSimulationWrapper;
	
	def void initializeFMURuntime() {
		_self.devInfo('-> initializeFMURuntime() '+_self.name + ' with path='+_self.runnableFmuPath);
		// BUG !! FMUSimulationWrapper wrapper  := self.createAndAssignFMUSimulationWrapper_buggy();
		// cf. https://github.com/gemoc/ale-lang/issues/46
		// workaround create the object first and pass it as an EObject
		
		_self.fmuSimulationWrapper = Interpreter_vmFactory.eINSTANCE.createFMUSimulationWrapper();
		
		//_self.loadAndAssignFMUSimulationWrapper_workaround(_self.fmuSimulationWrapper);
		try {
			val IFile iFile = IFileUtils.getIFileFromWorkspaceOrFileSystem(_self.getRunnableFmuPath());
			val String FMUFilePath =  iFile.getLocation().toString();
		
			val Simulation2 aRunnableFmu = new Simulation2(FMUFilePath);
			aRunnableFmu.init(0.0);	// start time 0.0
			// connect to wrapper for model access
			//result = Alefcl_vmFactory.eINSTANCE.createFMUSimulationWrapper();
			_self.fmuSimulationWrapper.fmiSimulation2 = aRunnableFmu;
			//result.setFmiSimulation2(aRunnableFmu);
		} catch (Exception e) {
			Activator.error("Failed to initialize FMU "+_self.getRunnableFmuPath(), e);
		}
		
		
		
		_self.devDebug('-> initializeFMURuntime() self.fmuSimulationWrapper='+_self.fmuSimulationWrapper);
		_self.devDebug('-> initializeFMURuntime() self.fmuSimulationWrapper.fmiSimulation2='+_self.fmuSimulationWrapper.fmiSimulation2);
		if(_self.fmuSimulationWrapper.fmiSimulation2 === null){ 
			throw new FCLException('Failed to initialize fmu '+_self.name + ' with path='+_self.runnableFmuPath);
			//_self.raiseException('Failed to initialize fmu '+_self.name + ' with path='+_self.runnableFmuPath);
		} else {
			_self.checkPortconsistency();
			//self.setFMUVarFromInputPorts();
			//self.loadOutputPortsFromFMU();
		}
	}
	
	def void checkPortconsistency() {
		// verifies that each port exists on the fmu
		_self.devWarn('-> checkPortconsistency() NOT IMPLEMENTED YET '+_self.name);
		_self.devInfo(_self.getKnownInputVariables());
		_self.devInfo(_self.getKnownOutputVariables());
	}
	
	def void setFMUVarFromInputPorts(){ 
		_self.devInfo('-> setFMUVarFromInputPorts() '+_self.name);
		// for all declared inputport set corresponding variable in Simulation2 runtime
		val List<FMUFunctionBlockDataPort> fmuInputPorts = _self.functionPorts
			.filter(FMUFunctionBlockDataPort)
			.filter[p | p.direction == DirectionKind.IN
					   || p.direction == DirectionKind.IN_OUT
			].toList;
		for(inPort : fmuInputPorts){
			try {
				inPort.setSimulation2RuntimeVariableFromPortValue();
				_self.devDebug(_self.name+'.'+inPort.name+' = '+inPort.getFloat());
			} catch ( InvalidData invalidData){
				_self.warn('ignoring set of fmu variable '+inPort.getFMUVarName()+' from '+_self.name+'.'+inPort.name+' (no known value yet, did you forget to set a default value ?)');
			} 
		}
	}
	
	@Step
	def void evalFunction(){
		
		_self.devDebug(_self.eContainerOfType(FCLModel).pushIndent()+
			'-> evalFunction() ' +_self.getQualifiedName());
		
		
		if(_self.timeReference !== null){
			_self.devInfo(_self.eContainerOfType(FCLModel).indentation+
				'Increment time reference for function '+_self.getQualifiedName());
			val FunctionVarDecl timeVariable = _self.timeReference.observableVar;
			
			timeVariable.assign(
					timeVariable.currentValue.plus(
						_self.timeReference.increment.evaluate()
					)
			);
		}
		
		_self.setFMUVarFromInputPorts();
		
		
		_self.devInfo(_self.getKnownInputVariables());
		_self.devInfo(_self.getKnownOutputVariables());
		
		// DO STEP
		val double stepSize = _self.eContainerOfType(FCLModel).dataflow.timeReference.observableVar.currentValue.toFloatValue().floatValue - 
		_self.eContainerOfType(FCLModel).dataflow.timeReference.observableVar.previousValue.toFloatValue().floatValue;
		_self.devDebug(_self.eContainerOfType(FCLModel).indentation+
			'doing doStep(stepSize='+ stepSize+')...');
		_self.devDebug(_self.eContainerOfType(FCLModel).indentation+
			'stepSize='+ stepSize);			
		_self.doStep(stepSize); 
		
		_self.devInfo(_self.eContainerOfType(FCLModel).indentation+
			'done. FMU internal time='+ _self.getSimulationCurrentTime());
		
		
		_self.devInfo(_self.getKnownInputVariables());
		_self.devInfo(_self.getKnownOutputVariables());
		
		_self.loadOutputPortsFromFMU();
		
		
		// for all non delayed outgoing connections, send data
		_self.sendDataToActiveNonDelayedPorts();
		
		_self.eContainerOfType(FCLModel).popIndent();
		/* self.devDebug(self.eContainer(fcl::FCLModel).popIndent()+
			'<- evalFunction() ' +self.name); */
	}
	
	def void loadOutputPortsFromFMU() {
		_self.devInfo('-> loadPortFromFMU() '+_self.name);
		
		// for all declared ouputport load its variable from Simulation2 runtime variable
		val List<FMUFunctionBlockDataPort> fmuOutputPorts = _self.functionPorts
			.filter(FMUFunctionBlockDataPort)
			.filter[p | p.direction == DirectionKind.OUT
					   || p.direction == DirectionKind.IN_OUT
			].toList; 
			
		for(outPort : fmuOutputPorts){
			outPort.setFloat(outPort.getPortValueFromSimulation2RuntimeVariable());
			_self.devDebug(_self.name+'.'+outPort.name+' = '+outPort.getFloat());				
		}
	}
	
	def void doStep( double stepSize) {
		val FMUSimulationWrapper wrapper =	_self.fmuSimulationWrapper;
		val Simulation2 javaFMI_FMU = wrapper.getFmiSimulation2();
		val Status status = javaFMI_FMU.doStep(stepSize);
		if(status != Status.OK) {
			System.err.println("Failed to doStep "+_self);
		}
	}
	
	def double getSimulationCurrentTime() {
		val Simulation2 javaFMI_FMU = _self.fmuSimulationWrapper.getFmiSimulation2();
		return javaFMI_FMU.getCurrentTime();
	}
	
	
	def String getKnownOutputVariables() {
		val StringBuilder sb = new StringBuilder();
		val Simulation2 javaFMI_FMU = _self.fmuSimulationWrapper.getFmiSimulation2();
		val Access access = new Access(javaFMI_FMU);
		for(ScalarVariable variable : access.getModelVariables())
		{
			val String causality = variable.getCausality();

			val String varName = variable.getName();
			if(causality.equals("output"))
			{
				sb.append("FMU output: "+varName+" = "+javaFMI_FMU.read(varName).asDouble() + " //"+javaFMI_FMU.readVariable(varName)+ " "+variable.getType()+"\n");
			}
		}
		return sb.toString();
	}
	def String getKnownInputVariables() {
		val StringBuilder sb = new StringBuilder();
		val Simulation2 javaFMI_FMU = _self.fmuSimulationWrapper.getFmiSimulation2();
		val Access access = new Access(javaFMI_FMU);
		for(ScalarVariable variable : access.getModelVariables())
		{
			val String causality = variable.getCausality();

			val String varName = variable.getName();
			if(causality.equals("input"))
			{
				sb.append("FMU input: "+varName+" = "+javaFMI_FMU.read(varName).asDouble() + " //"+javaFMI_FMU.readVariable(varName)+ " "+variable.getType()+"\n");
			}
		}
		return sb.toString();
	}

}

@Aspect(className=UnityFunction)
class UnityFunctionAspect extends FunctionAspect {
	
	public UnityWrapper unityWrapper;
	
	def void initializeUnity() {
		_self.devInfo('-> initializeUnity() '+_self.name + ' with path='+_self.unityWebPagePath);
		
		_self.unityWrapper = Interpreter_vmFactory.eINSTANCE.createUnityWrapper();
		try {
			val IFile iFile = IFileUtils.getIFileFromWorkspaceOrFileSystem(_self.getUnityWebPagePath());
			val String unityFilePath =  iFile.getLocation().toString();
			val UnityInstance unityInstance = new UnityInstance(_self.getName(), unityFilePath);
						
			_self.unityWrapper.unityInstance = unityInstance
		} catch (Exception e) {
			Activator.error("Failed to initialize Unity "+_self.getUnityWebPagePath(), e);
		}
	}
	
	@Step
	def void evalFunction(){
		
		_self.devDebug(_self.eContainerOfType(FCLModel).pushIndent()+
			'-> evalFunction() ' +_self.getQualifiedName());
		
		
		if(_self.timeReference !== null){
			_self.devInfo(_self.eContainerOfType(FCLModel).indentation+
				'Increment time reference for function '+_self.getQualifiedName());
			val FunctionVarDecl timeVariable = _self.timeReference.observableVar;
			
			timeVariable.assign(
					timeVariable.currentValue.plus(
						_self.timeReference.increment.evaluate()
					)
			);
		}
		
		_self.setUnityVarFromInputPorts();
		
		
		// DO STEP
	/* 	Real stepSize := self.eContainer(fcl::FCLModel).dataflow.timeReference.observableVar.currentValue.toFloatValue().floatValue - 
			
		self.doStep(self.fmuSimulationWrapper, stepSize); 
		
		self.loadOutputPortsFromFMU();*/
		
		
		// for all non delayed outgoing connections, send data
		_self.sendDataToActiveNonDelayedPorts();
		
		_self.eContainerOfType(FCLModel).popIndent();
	}
	
	def void setUnityVarFromInputPorts(){ 
		_self.devInfo('-> setFMUVarFromInputPorts() '+_self.name);
		// for all declared inputport set corresponding variable in Simulation2 runtime
		
		val List<UnityFunctionDataPort> unityInputPorts = _self.functionPorts
			.filter(UnityFunctionDataPort)
			.filter[p | p.direction == DirectionKind.IN
					   || p.direction == DirectionKind.IN_OUT
			].toList;
		for(inPort : unityInputPorts){
			try {
				inPort.setUnityVariableFromPortValue();
				_self.devDebug(_self.name+'.'+inPort.name+' = '+inPort.getFloat());
			} catch ( InvalidData invalidData){
				_self.warn('ignoring set of unity variable '+inPort.unityNodeName+ '.'+ inPort.unityVariableName+' from '+_self.name+'.'+inPort.name+' (no known value yet, did you forget to set a default value ?)');
			} 
		}
	}
}


@Aspect(className=FunctionConnector)
class FunctionConnectorAspect {

}
@Aspect(className=FunctionTimeReference)
class FunctionTimeReferenceAspect {

}

// #########################
// AssignableAspect
// #########################
@Aspect(className=Assignable)
abstract class AssignableAspect {
	def void assign(Value newValue){
		_self.devError('not implemented, please ask language designer to implement assign() for '+_self);
		throw new NotImplementedException('not implemented, please implement assign() for '+_self);
	}
	
	def DataType getResolvedType(){
		_self.devError('not implemented, please ask language designer to implement getResolvedType() for '+_self);
		throw new NotImplementedException('not implemented, please implement getResolvedType() for '+_self);
	}
}

@Aspect(className=FunctionVarDecl, with=#[CallableDeclarationAspect])
class FunctionVarDeclAspect extends AssignableAspect {
	/*
	* BE CAREFUL :
	*
	* This class has more than one superclass
	* please specify which parent you want with the 'super' expected calling
	*
	*/


	public Value previousValue;
	public Value currentValue;
	
	def void assign(Value newValue) {
		// ignore assignment if the raw values are the same
		if(_self.currentValue === null || _self.currentValue.rawValue != newValue.rawValue){
			_self.currentValue =  newValue;	
		}
		if((! (_self.previousValue !== null)) || ! _self.previousValue.equals(newValue).booleanValue){
			// show log only on value change and initial assignment
			_self.important(''+_self.getQualifiedName()+' := '+newValue.valueToString());
		}
	}
	
	def Value call() {
		return _self.currentValue;
	}
	def Value callPrevious() {
		return _self.previousValue;		
	}
	
	def DataType getResolvedType(){
		return _self.type;
	}
	
	/**
	 * returns a label with smart display of the label + value on several lines
	 * display the previous value only if different from current value
	 */
	def String smartLabelWithValue(){
		val String prevValueString = _self.previousValue.valueToString();
		val String currentValueString = _self.currentValue.valueToString();
		if(prevValueString ==  currentValueString) {
			return  _self.name + '\n['+currentValueString+']';
		} else {
			return  _self.name + '\n['+prevValueString+'](-1)\n['+currentValueString+']';
		}	
	}
}

@Aspect(className=ProcedureParameter, with=#[CallableDeclarationAspect])
class ProcedureParameterAspect extends AssignableAspect {
	/*
	* BE CAREFUL :
	*
	* This class has more than one superclass
	* please specify which parent you want with the 'super' expected calling
	*
	*/


	def Value call() {
		val ProcedureContext context = _self.eContainerOfType(FCLModel).procedureStack.last();
		val DeclarationMapEntry mapEntry = context.declarationMapEntries.findFirst[me | me.callableDeclaration == _self];
		if(mapEntry !== null) {
			_self.eContainerOfType(FCLModel).popIndent();
			return mapEntry.value;
		} else {
			_self.eContainerOfType(FCLModel).popIndent();
			return null;
		}
	} 
	
	def DataType getResolvedType(){
		return _self.type;
	}
	
	def String getSimpleName() {
		return ''+_self.getResolvedType().name +' '+ _self.name;
	}

}

// #########################
// FunctionPortAspect
// #########################
@Aspect(className=FunctionPort, with=#[CallableDeclarationAspect])
class FunctionPortAspect extends AssignableAspect {
	/*
	* BE CAREFUL :
	*
	* This class has more than one superclass
	* please specify which parent you want with the 'super' expected calling
	*
	*/

	def FunctionBlockDataPort getTargetDataPort(){
		_self.devError('not implemented, please ask language designer to implement getTargetDataPort() for '+_self);
		throw new NotImplementedException('not implemented, please implement getTargetDataPort() for '+_self);
	}

}



@Aspect(className=FunctionPortReference)
class FunctionPortReferenceAspect extends FunctionPortAspect {
	def FunctionBlockDataPort getTargetDataPort(){
		return _self.functionPort.getTargetDataPort();
	}
	
	def DataType getResolvedType(){
		return _self.getTargetDataPort().resolvedType;
	}
}

// #########################
// FunctionBlockDataPortAspects
// #########################

@Aspect(className=FunctionBlockDataPort)
class FunctionBlockDataPortAspect extends FunctionPortAspect {
	public Value previousValue;
	public Value currentValue;
	
	
	// some getter and setter to simplify code
	def void setFloat(double newFloatVal) {
		val FloatValue newVal = FclFactory.eINSTANCE.createFloatValue();
		newVal.floatValue = newFloatVal;
		if(_self.functionVarStore  !== null){
			_self.functionVarStore.currentValue = newVal;
		} else {
			_self.currentValue = newVal;	
		}
	}
	def double getFloat() {
		
		var Value cur = _self.currentValue;
		if(_self.functionVarStore  !== null){
			cur = _self.functionVarStore.currentValue;
		} else {
			cur = _self.currentValue;
		}	
		if(cur instanceof FloatValue) {
			return (cur as FloatValue).floatValue;
		} else {
			throw new InvalidData("Not a Float: "+cur);
		}
	}
	
	/**
	 * Method allowing to set the value from a String
	 * first tries to create the value according to the port type, then convert the string into the primitive datatype
	 */
	//@AcceptedEvent
	def void setValueFromString(String newValue) {
		val Value aValue = _self.dataType.createValue(newValue);
		if( aValue !== null ) {
			_self.assign(aValue);
		} else {	
			_self.error('failed to assign '+newValue+' to '+_self.getQualifiedName());
		} 
	}
	
	def Value call() {
		if(_self.functionVarStore  !== null){
			return _self.functionVarStore.currentValue;
		} else {
			return _self.currentValue;	
		}
	}
	def Value callPrevious() {
		if(_self.functionVarStore  !== null){
			return _self.functionVarStore.previousValue;
		} else {
			return _self.previousValue;	
		}
	}
	
	def void assign(Value newValue){		
		var Value prevValue = _self.currentValue;
		if(_self.functionVarStore  !== null){
			prevValue = _self.functionVarStore.currentValue;
 			_self.functionVarStore.currentValue = newValue;
 			// ignore assignment if the raw values are the same
			if(_self.functionVarStore.currentValue === null  || _self.functionVarStore.currentValue.rawValue != newValue.rawValue){
				_self.functionVarStore.currentValue = newValue;
			}
	 	} else {
	 		prevValue = _self.currentValue;
	 		// ignore assignment if the raw values are the same
			if(_self.currentValue === null || _self.currentValue.rawValue != newValue.rawValue){
				_self.currentValue =  newValue;	
			}
		}
		if((! (prevValue !== null)) || ! prevValue.equals(newValue).booleanValue){
			// show log only on value changes and initial assignment
			_self.important(''+_self.getQualifiedName()+' := '+newValue.valueToString());
		}
	}
	def DataType getResolvedType(){
		return _self.dataType;
	}
	def FunctionBlockDataPort getTargetDataPort(){
		return _self;
	}
	
	/**
	 * returns a label with smart display of the label + value on several lines
	 * display the previous value only if different from current value
	 */
	def String smartLabelWithValue(){
		val String prevValueString = _self.previousValue.valueToString();
		val String currentValueString = _self.currentValue.valueToString();
		if(prevValueString ==  currentValueString) {
			return  _self.name + '\n['+currentValueString+']';
		} else {
			return  _self.name + '\n['+prevValueString+'](-1)\n['+currentValueString+']';
		}	
	}
}



@Aspect(className=FMUFunctionBlockDataPort)
class FMUFunctionBlockDataPortAspect extends FunctionBlockDataPortAspect {

	def String getFMUVarName(){
		if(_self.runnableFmuVariableName === null || _self.runnableFmuVariableName == ''){
				return _self.name; 
		} else {
			return _self.runnableFmuVariableName;
		}
	}
	
	
	def void setSimulation2RuntimeVariableFromPortValue() {
		val String portName = _self.getFMUVarName();
		val Double portValue = _self.getFloat();
		val FMUSimulationWrapper wrapper = _self.eContainerOfType(FMUFunction).fmuSimulationWrapper;
		val Simulation2 javaFMI_FMU = wrapper.getFmiSimulation2();
		val Status status = javaFMI_FMU.write(portName).with(portValue);
		if(status != Status.OK) {
			System.err.println("Failed to write "+portValue+" in fmu variable "+portName+" "+status.getValue());
		}
	}
	
	def double getPortValueFromSimulation2RuntimeVariable() {
		val FMUSimulationWrapper wrapper =	_self.eContainerOfType(FMUFunction).fmuSimulationWrapper;
		val Simulation2 javaFMI_FMU = wrapper.getFmiSimulation2();
		return javaFMI_FMU.read(_self.getFMUVarName()).asDouble();
	}
}

@Aspect(className=UnityFunctionDataPort)
class UnityFunctionDataPortAspect extends FunctionBlockDataPortAspect {


	def void setUnityVariableFromPortValue( ) {
		Display.getDefault().asyncExec(new Runnable() {
			override void run() {
				val UnityFunction unityFunction = _self.eContainerOfType(UnityFunction);
				val UnityWrapper unityWrapper = unityFunction.unityWrapper;
				val String nodeName =  _self.unityNodeName;
				val String varName = _self.unityVariableName;
				val double newValue = _self.getFloat();
				unityWrapper.getUnityInstance().setVar(nodeName, varName, newValue);
			}
		});
	}
}
// #############################
// ModeAspect
// #############################
@Aspect(className=Mode)
class ModeAspect /* extends NamedElementAspect */ {

}
@Aspect(className=ModeAutomata)
class ModeAutomataAspect extends ModeAspect {

	public Mode currentMode;
	
	// get the list of fireableTransiion from current mode
	def List<Transition> fireableModeTransitions() {
		val FCLModel fclModel = _self.eContainerOfType(FCLModel);
		if(fclModel.receivedEvents.size() > 0) {
			_self.devInfo(fclModel.pushIndent()+
				'-> Evaluating possible mode switch considering event occurrence '+fclModel.receivedEvents.get(0).event.name);
		} else {
			_self.devInfo(fclModel.pushIndent()+
			'-> Evaluating possible mode switch (without events)');
		}
		
		val List<Transition> result = _self.currentMode.outgoingTransition.filter[t | 
					(t.evalEvent()) && 
					(t.evalGuard())
				].toList;
		fclModel.popIndent();
		return result;
	}
	
	
	def void switchMode(Transition transition){
		transition.fire();
	}
		
	@Step
	def void evalModeStateMachine(){
		val FCLModel fclModel = _self.eContainerOfType(FCLModel);
		
		// get the list of fireableTransition from current mode
		var List<Transition> fireableModeTransitions = _self.fireableModeTransitions();
			
		while(fireableModeTransitions.size() > 0 || fclModel.receivedEvents.size() > 0) {
			_self.devInfo(fclModel.indentation+
				'\t fireableTransitions='+fireableModeTransitions+ ' (out of '+_self.currentMode.outgoingTransition+')');
		
		    if(fireableModeTransitions.size() > 0) {
				_self.switchMode(fireableModeTransitions.get(0));
				// re-evaluate possible mode switch
				fireableModeTransitions = _self.fireableModeTransitions();	
			}
			while(fireableModeTransitions.size() == 0 && fclModel.receivedEvents.size() > 0){
				// flush events that cannot be directly used to switch mode
				_self.devInfo('no more transition can be fired, but some event remains: consume one');
				fclModel.consumeEventOccurence();
				fireableModeTransitions = _self.fireableModeTransitions();
			}
		}
		
		_self.devInfo('no more mode switch are possible and all events have been consumed');
		_self.devInfo('current mode is '+_self.currentMode.name);
	}
}


@Aspect(className=Transition)
class TransitionAspect /* extends NamedElementAspect */ {
	def boolean evalGuard(){
		var boolean result = true;
 		if( _self.guard === null) {
 			result = true; 
 		} else {
 			result = _self.guard.evaluateAsBoolean();
 		}
 		return result;
	}
	def boolean evalEvent(){
		val FCLModel fclModel = _self.eContainerOfType(FCLModel);
		var boolean result;
 		if( _self.event === null) {
 			// this transition is not driven by an event, it can be fired "anytime"
 			result = true; 
 		} else {
 			if(fclModel.receivedEvents.size() > 0) {
 				result = (fclModel.receivedEvents.get(0).event.name == _self.event.name);
 			} else {
 				// not matching event to consume and this transition requires one
 				result = false;
 			}
 			
 		}
		return result;
	}
	
	 
	@Step
	def void fire() {
		val ModeAutomata fsm = _self.source.eContainerOfType(ModeAutomata);
		val FCLModel fclModel = _self.eContainerOfType(FCLModel);
		_self.important('Firing ' + _self.name + ' and entering ' + _self.target.name);
		
		
		fsm.currentMode = _self.target;
		
		// if the transition was event driven, consumes the event
		if(_self.event !== null){
			fclModel.consumeEventOccurence();
		}
		
	}
}
// #############################
// CallableDeclarationAspect
// #############################

@Aspect(className=CallableDeclaration)
abstract class CallableDeclarationAspect {
	def Value call() {
		_self.devError('not implemented, please ask language designer to implement call() for '+_self);
		throw new NotImplementedException('not implemented, please implement call() for '+_self);
	}
	def Value callPrevious() {
		_self.devError('not implemented, please ask language designer to implement callPrevious() for '+_self);
		throw new NotImplementedException('not implemented, please implement callPrevious() for '+_self);
	}
	
	def DataType getResolvedType() {
		_self.devError('not implemented, please ask language designer to implement getResolvedType() for '+_self);
		throw new NotImplementedException('not implemented, please implement getResolvedType() for '+_self);
	}
	
	def String getSimpleName() {
		_self.devError('not implemented, please ask language designer to implement getSimpleName() for '+_self);
		throw new NotImplementedException('not implemented, please implement getSimpleName() for '+_self);
	}
}

@Aspect(className=EnumerationLiteral)
class EnumerationLiteralAspect extends CallableDeclarationAspect {
	/*
	* BE CAREFUL :
	*
	* This class has more than one superclass
	* please specify which parent you want with the 'super' expected calling
	*
	*/

	def Value call() {
		val EnumerationValue eValue = FclFactory.eINSTANCE.createEnumerationValue();
		eValue.enumerationValue = _self;
		return eValue;
	}

}


// #################
// ExpressionAspect
// #################
@Aspect(className=Expression)
abstract class ExpressionAspect {

	/**
	 * evaluate the expression and get the boolean
	 * if not a boolean raise an exception and stop
	 */ 
	def boolean evaluateAsBoolean(){
		val Value internalResult = _self.evaluate();
		var boolean result = false;
		if(internalResult instanceof BooleanValue) {
			result = internalResult.booleanValue;
		} else {
			throw new FCLException('expected a boolean but got '+internalResult);
		}
		return result;
	}
	
	def Value evaluate() {
		_self.devError('not implemented, please ask language designer to implement evaluate() for '+_self);
		throw new NotImplementedException('not implemented, please implement evaluate() for '+_self);
	}
}

@Aspect(className=NewDataStruct)
class NewDataStructAspect extends ExpressionAspect {
	def Value evaluate() {
		val StructValue sValue = FclFactory.eINSTANCE.createStructValue();
		for(propAssign : _self.propertyAssignments){
			propAssign.assignToStruct(sValue);
		}
		return sValue;
	}
}

// #################
// CallAspect
// #################
@Aspect(className=Call)
abstract class CallAspect extends ExpressionAspect {
	def Value evaluate() {
		_self.devError('not implemented, please ask language designer to implement evaluate() for '+_self);
		throw new NotImplementedException('not implemented, please implement evaluate() for '+_self);
	}
}

@Aspect(className=CallConstant)
class CallConstantAspect extends CallAspect {
	def Value evaluate() {
		return _self.value.evaluate();
	}
}

@Aspect(className=CallDeclarationReference)
class CallDeclarationReferenceAspect extends CallAspect {
	def Value evaluate() {
		return _self.callable.call();
	}
}

@Aspect(className=CallDataStructProperty, with=#[DataStructPropertyReferenceAspect])
class CallDataStructPropertyAspect extends CallAspect {
	/*
	* BE CAREFUL :
	*
	* This class has more than one superclass
	* please specify which parent you want with the 'super' expected calling
	*
	*/
	def Value evaluate() {
			
		val Value r = _self.rootTargetDataStruct.callable.call();
		if(r === null) { 
			_self.error('struct navigation error '+_self.rootTargetDataStruct.callable.getQualifiedName()+ ' hasn\'t been initialized');
			throw new FCLException('struct navigation error '+_self.rootTargetDataStruct.callable.getQualifiedName()+ ' hasn\'t been initialized');
		}
		if(r instanceof StructValue) {
			var StructValue currentResult = r;
			val String last = _self.propertyName.last;
			for(propName : _self.propertyName){ 
				val Value r2 = currentResult.getValueInProperty(propName);
				if(r2 instanceof StructValue) {
					currentResult = r2;
				} 
				if(propName != last && (! (r2 instanceof StructValue))) {
					_self.error('struct navigation error '+r2.valueToString()+ ' isn\'t a StructValue while navigating '+
						r.valueToString()+'->'+_self.propertyName.join('->')
					);
					throw new FCLException('struct navigation error '+r2.valueToString()+ ' isn\'t a StructValue');
				} else {
					return r2;
				}
			}
		} else {
			_self.error('struct navigation error '+r+ ' isn\'t a StructValue');
			throw new FCLException('struct navigation error '+r.valueToString()+ ' isn\'t a StructValue');
		}
			
	}

}

@Aspect(className=CallPrevious)
class CallPreviousAspect extends CallAspect {
	def Value evaluate() {
		return _self.callable.callPrevious();
	}
}



// #################
// UnaryExpressionAspect
// #################
@Aspect(className=UnaryExpression)
abstract class UnaryExpressionAspect extends ExpressionAspect {

}

@Aspect(className=BasicOperatorUnaryExpression)
class BasicOperatorUnaryExpressionAspect extends UnaryExpressionAspect {
	def Value evaluate() {
			val Value lhs = _self.operand.evaluate();
			if(_self.operator == UnaryOperator::UNARYMINUS) {
				return lhs.uminus();
			} else if(_self.operator == UnaryOperator::NOT) {
				val BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
				bValue.booleanValue = ! _self.operand.evaluateAsBoolean(); 
				return bValue;
			} else {
				throw new NotImplementedException('not implemented, please implement evaluate() for '+_self);			
			}
	}
}


@Aspect(className=Cast)
class CastAspect extends UnaryExpressionAspect {

}

// #################
// BinaryExpressionAspect
// #################
@Aspect(className=BinaryExpression)
abstract class BinaryExpressionAspect extends ExpressionAspect {

}


@Aspect(className=BasicOperatorBinaryExpression)
class BasicOperatorBinaryExpressionAspect extends BinaryExpressionAspect {
	def Value evaluate() {
		var Value result; 
		if(_self.operator == BinaryOperator.OR) {
			val BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
			bValue.booleanValue = _self.lhsOperand.evaluateAsBoolean() || _self.rhsOperand.evaluateAsBoolean(); 
			result = bValue;
		} else if(_self.operator == BinaryOperator.AND){
				
			val BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
			bValue.booleanValue = _self.lhsOperand.evaluateAsBoolean() && _self.rhsOperand.evaluateAsBoolean(); 
			result = bValue;
		} else {
			// all other operators need both lhs and rhs evaluation
			val Value lhs = _self.lhsOperand.evaluate();
			val Value rhs = _self.rhsOperand.evaluate();
			if(_self.operator == BinaryOperator::EQUAL){
				result = lhs.bEquals(rhs);
			} else if(_self.operator == BinaryOperator::PLUS){ 
				result = lhs.plus(rhs);
			} else if(_self.operator == BinaryOperator::MINUS){ 
				result = lhs.minus(rhs);
			} else if(_self.operator == BinaryOperator::MULT){
				result = lhs.mult(rhs);
			} else if(_self.operator == BinaryOperator::DIV){
				result = lhs.div(rhs);
			} else if(_self.operator == BinaryOperator::MOD){
				result = lhs.modulo(rhs);
			} else if(_self.operator == BinaryOperator::GREATER){
				result = lhs.greater(rhs);
			} else if(_self.operator == BinaryOperator::LOWER){
				result = lhs.lower(rhs);
			} else if(_self.operator == BinaryOperator::GREATEROREQUAL){
				result = lhs.greaterOrEquals(rhs);
			} else if(_self.operator == BinaryOperator::LOWEROREQUAL){
				result = lhs.lowerOrEquals(rhs);
			} else {
				throw new NotImplementedException('not implemented, please implement evaluate() for '+_self);
			}
		}
		return result;
	}
}

// ##################################
// DataStructPropertyReferenceAspect
// ##################################
@Aspect(className=DataStructPropertyReference)
abstract class DataStructPropertyReferenceAspect {

}

@Aspect(className=DataStructPropertyAssignment, with=#[PrimitiveActionAspect])
class DataStructPropertyAssignmentAspect extends DataStructPropertyReferenceAspect {
	/*
	* BE CAREFUL :
	*
	* This class has more than one superclass
	* please specify which parent you want with the 'super' expected calling
	*
	*/
	def void doAction(){
		val Assignable currentTarget = _self.assignable;
		
		val DataType resolvedType = currentTarget.getResolvedType();
		if(resolvedType instanceof DataStructType) {
			if(currentTarget instanceof CallableDeclaration) {
			val Value targetVal = (currentTarget as CallableDeclaration).call();
				if(targetVal instanceof StructValue){
					_self.assignToStruct(targetVal as StructValue);
				} else {
					_self.error('assignment navigation error for '+_self+ ' applied to '+currentTarget+' targetValue '+targetVal+' is not a Structure');
					throw new FCLException('assignment navigation error for '+_self+ ' applied to '+currentTarget+' targetValue '+targetVal+' is not a Structure');
				}
			} else {
				_self.devError('assignment navigation error for '+_self+ ' applied to '+currentTarget+' (not a callable)');
				throw new FCLException('assignment navigation error for '+_self+ ' applied to '+currentTarget);
			}
		} else {
			_self.error('assignment navigation error for '+_self+ ' applied to '+currentTarget);
			throw new FCLException('assignment navigation error for '+_self+ ' applied to '+currentTarget);
		}
		
	}

	def void assignToStruct(StructValue sValue){
		var StructValue finalTarget = sValue;
		var Integer index = 0;
		for(propName : _self.propertyName){
			index = index + 1;
			if(index == _self.propertyName.size()){
				// this is the last
				
			val Value evaluedValue = _self.expression.evaluate().copy();
				finalTarget.setValueInProperty(propName, evaluedValue);
			} else {
				// not the last, navigate in the property value
			val Value newTarget = (finalTarget as StructValue).getValueInProperty(propName);
				if(newTarget instanceof StructValue) {
					finalTarget = newTarget;
				} else {
					// error !
					_self.error('assignment navigation error for '+_self+ ' applied to '+sValue+'; target of '+propName + ' is a '+newTarget + ' instead of a StructValue');
					throw new FCLException('assignment navigation error for '+_self+ ' applied to '+sValue);
				}
			}
		}
	}


}


// ###################
// ActionAspect
// ###################
@Aspect(className=Action)
abstract class ActionAspect {
	def void doAction(){
		_self.devError('not implemented, please ask language designer to implement doAction() for '+_self);
		throw new NotImplementedException('not implemented, please implement doAction() for '+_self);
	}
}

// ###################
// PrimitiveActionAspect
// ###################
@Aspect(className=PrimitiveAction)
abstract class PrimitiveActionAspect extends ActionAspect {

}

@Aspect(className=Assignment)
class AssignmentAspect extends PrimitiveActionAspect {
	def void doAction(){
		val Assignable target = _self.assignable;
		target.assign(_self.expression.evaluate());
	}
}

@Aspect(className=FCLProcedureReturn)
class FCLProcedureReturnAspect extends PrimitiveActionAspect {
	def void doAction(){
		val ProcedureContext context = _self.eContainerOfType(FCLModel).procedureStack.last;
		context.resultValue = _self.expression.evaluate();
	}
}

@Aspect(className=Log)
class LogAspect extends PrimitiveActionAspect {
	def void doAction(){
		_self.info(''+_self.expression.map[exp | exp.evaluate().valueToString()].join(''));
	}
}


@Aspect(className=VarDecl, with=#[AssignableAspect, CallableDeclarationAspect])
class VarDeclAspect extends PrimitiveActionAspect {
	/*
	* BE CAREFUL :
	*
	* This class has more than one superclass
	* please specify which parent you want with the 'super' expected calling
	*
	*/
	def void doAction(){
		val ProcedureContext procedureContext = _self.eContainerOfType(FCLModel).procedureStack.last;
		// create variable
		val DeclarationMapEntry mapEntry = Interpreter_vmFactory.eINSTANCE.createDeclarationMapEntry();
		mapEntry.callableDeclaration = _self;
		if(_self.initialValue !== null){
			// set initial value by evaluating expression
			val Value iValue = _self.initialValue.evaluate();					
			mapEntry.value = iValue.copy();
		}
		procedureContext.declarationMapEntries += mapEntry;
	}
	
	def Value call() {
		val ProcedureContext context = _self.eContainerOfType(FCLModel).procedureStack.last;
		val DeclarationMapEntry mapEntry = context.declarationMapEntries.findFirst[me | me.callableDeclaration == _self];
		if(mapEntry !== null) {
			return mapEntry.value;
		} else {
			return null;
		}
		
	} 
	
	def void assign(Value newValue){
		val ProcedureContext context = _self.eContainerOfType(FCLModel).procedureStack.last;
		val DeclarationMapEntry mapEntry = context.declarationMapEntries.findFirst[me | me.callableDeclaration == _self];
		if(mapEntry !== null) {
			// update value
			mapEntry.value = newValue.copy();
		} else {
			// need to push new entry
			val DeclarationMapEntry newmapEntry = Interpreter_vmFactory.eINSTANCE.createDeclarationMapEntry();
			newmapEntry.callableDeclaration = _self;
			newmapEntry.value = newValue.copy();
			context.declarationMapEntries += newmapEntry;
		}

	}
	
	def DataType getResolvedType(){
		return _self.type;
	}

}

@Aspect(className=ProcedureCall, with=#[ExpressionAspect])
class ProcedureCallAspect extends PrimitiveActionAspect {
	/*
	* BE CAREFUL :
	*
	* This class has more than one superclass
	* please specify which parent you want with the 'super' expected calling
	*
	*/

	def Value evaluate() {
		var ProcedureContext procedureContext = Interpreter_vmFactory.eINSTANCE.createProcedureContext();
		// create context
		var Integer index = 0;
		for( p : _self.procedureCallArguments) {
			// create parameters variable
			val DeclarationMapEntry mapEntry = Interpreter_vmFactory.eINSTANCE.createDeclarationMapEntry();
			mapEntry.callableDeclaration = _self.procedure.procedureParameters.get(index);
			// set parameters variable by evaluating expression
			val Value pValue = p.evaluate();
			mapEntry.value = pValue.copy();
			// checks that the passed value is valid according to the expected type
			if ( ! (mapEntry.value.isKindOf(mapEntry.callableDeclaration.getResolvedType()))) {
				_self.error('parameter call mismatch: expecting '+mapEntry.callableDeclaration.getResolvedType().name+' received '+mapEntry.value.valueToString()+' for '+mapEntry.callableDeclaration.getQualifiedName());
				throw new FCLException('parameter call mismatch: expecting '+mapEntry.callableDeclaration.getResolvedType().name+' received '+mapEntry.value.valueToString());
			} 
			procedureContext.declarationMapEntries += mapEntry;
			index = index +1;
		}
		// push context
		_self.eContainerOfType(FCLModel).procedureStack +=  procedureContext;
		// run the procedure 
		_self.procedure.runProcedure();
		// assign result
		val result = procedureContext.resultValue;
		// pop context
		_self.eContainerOfType(FCLModel).procedureStack -=  procedureContext;
		return result;
	}
}

// ###################
// ControlStructureActionAspect
// ###################
@Aspect(className=ControlStructureAction)
abstract class ControlStructureActionAspect extends ActionAspect {

}

@Aspect(className=ActionBlock)
class ActionBlockAspect extends ControlStructureActionAspect {
	def void doAction(){
		for(action : _self.actions){
			action.doAction();
		}
	}
}

@Aspect(className=IfAction)
class IfActionAspect extends ControlStructureActionAspect {

	def void doAction(){
		if(_self.condition.evaluateAsBoolean()){
			if(_self.thenAction !== null){
				_self.thenAction.doAction();
			}
		} else {
			if(_self.elseAction !== null){
				_self.elseAction.doAction();
			}
		}
	}
}

@Aspect(className=WhileAction)
class WhileActionAspect extends ControlStructureActionAspect {
	def void doAction(){
		while(_self.condition.evaluateAsBoolean()){
			if(_self.action !== null){
				_self.action.doAction();
			}
		} 
	}
}

@Aspect(className=DataStructProperty)
class DataStructPropertyAspect /* extends NamedElementAspect */ {

}

// ##################
// ProcedureAspect
// ##################
@Aspect(className=Procedure)
abstract class ProcedureAspect /*extends NamedElementAspect*/ {
	def void runProcedure(){
		_self.devError('not implemented, please ask language designer to implement runProcedure() for '+_self);
		throw new NotImplementedException('not implemented, please implement runProcedure() for '+_self);
	}
}

@Aspect(className=ExternalProcedure)
abstract class ExternalProcedureAspect extends ProcedureAspect {

}

@Aspect(className=FCLProcedure)
class FCLProcedureAspect extends ProcedureAspect {
	//@Step
	def void runProcedure(){
		if(_self.action !== null){
			_self.action.doAction();
		}
	}
}

@Aspect(className=JavaProcedure)
class JavaProcedureAspect extends ExternalProcedureAspect {
	
	// FIXME must support more use cases than just call to static methods with double
	def void runProcedure(){
		
		val ProcedureContext context = _self.eContainerOfType(FCLModel).procedureStack.last;
		if(_self.static){
			if(_self.returnType !== null && _self.returnType instanceof FloatValueType) {
				val ProcedureParameter param = _self.procedureParameters.get(0);
				val double rawFloat = param.call().toFloatValue().rawValue() as Double;
				val FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
				
				
				val int lastindexof = _self.methodFullQualifiedName.lastIndexOf(".");
				val String methodName = _self.methodFullQualifiedName.substring(lastindexof+1);
				val String className = _self.methodFullQualifiedName.substring(0, lastindexof);
				val Class klass = Class.forName(className);
				val Method m = klass.getDeclaredMethod(methodName, double);
				m.invoke(null, rawFloat);
				aValue.floatValue = m.invoke(null, rawFloat) as Double; 
				context.resultValue = aValue;
			} else {
				// ALE bug on services, this is hard to cast "object"  need further investigation
				_self.devError('not implemented, please ask language designer to implement JavaProcedure.runProcedure() for '+_self);
				throw new NotImplementedException('not implemented, please implement JavaProcedure.runProcedure() for '+_self);
			}
		} else {
			_self.devError('not implemented, please ask language designer to implement JavaProcedure.runProcedure() for non static '+_self);
			throw new NotImplementedException('not implemented, please implement JavaProcedure.runProcedure() for non static '+_self);
		}
	}
}

// ################
// EventAspect
// ################
@Aspect(className=Event)
abstract class EventAspect /* extends NamedElementAspect */ {
	def void send(){
		val EventOccurrence myOccurrence = Interpreter_vmFactory.eINSTANCE.createEventOccurrence();
		myOccurrence.event = _self;
		_self.eContainerOfType(FCLModel).receivedEvents += myOccurrence;
		_self.info('added occurrence of '+_self.name);
	}
}

@Aspect(className=ExternalEvent)
class ExternalEventAspect extends EventAspect {

}


// ###############################
// DataTypeAspect
// ###############################

@Aspect(className=DataType)
abstract class DataTypeAspect /* extends NamedElementAspect*/ {
	/**
	 * Creates a value for the given type
	 * Initiate its internal value by parsing the string parameter
	 */ 
	def Value createValue(String internalValue) {
		_self.devError('not implemented, please ask language designer to implement createValue() for '+_self);
		throw new NotImplementedException('not implemented, please implement createValue() for '+_self);
	}
}

@Aspect(className=DataStructType)
class DataStructTypeAspect extends DataTypeAspect {

}

// ###########################
// ValueTypeAspect
// ###########################
@Aspect(className=ValueType)
class ValueTypeAspect extends DataTypeAspect {

}

@Aspect(className=BooleanValueType)
class BooleanValueTypeAspect extends ValueTypeAspect {
	def Value createValue(String internalValue) {
		val BooleanValue aValue = FclFactory.eINSTANCE.createBooleanValue();
		aValue.booleanValue = Boolean.parseBoolean(internalValue);
		return aValue;
	}
}
@Aspect(className=EnumerationValueType)
class EnumerationValueTypeAspect extends ValueTypeAspect {
	def Value createValue(String internalValue) {
		val EnumerationValue aValue = FclFactory.eINSTANCE.createEnumerationValue();
		val String searchedLit = if(internalValue.startsWith(_self.name+'::') ){
			internalValue.replace(_self.name+'::','');
		} else {
			internalValue;
		}
		val EnumerationLiteral enumLit = _self.enumerationLiteral.findFirst[enumLit | enumLit.name.equals(searchedLit)];
		if(enumLit === null) {
			_self.error('failed to create '+_self.name+' enumeration value from literal '+searchedLit);	
		}
		aValue.enumerationValue = enumLit;
		return aValue;
	}
}
@Aspect(className=IntegerValueType)
class IntegerValueTypeAspect extends ValueTypeAspect {
	def Value createValue(String internalValue) {
		val IntegerValue aValue = FclFactory.eINSTANCE.createIntegerValue();
		aValue.intValue = Integer.parseInt(internalValue);
		return aValue;
	}
}

@Aspect(className=FloatValueType)
class FloatValueTypeAspect extends ValueTypeAspect {
	def Value createValue(String internalValue) {
		val FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
		aValue.floatValue = Double.parseDouble(internalValue);
		return aValue;
	}
}

@Aspect(className=StringValueType)
class StringValueTypeAspect extends ValueTypeAspect {
	def Value createValue(String internalValue) {
		val StringValue aValue = FclFactory.eINSTANCE.createStringValue();
		aValue.stringValue = internalValue;
		return aValue;
	}
}



// #####################
// ValueAspect
// #####################
@Aspect(className=Value)
abstract class ValueAspect {
	def Value evaluate() {
		_self.devError('not implemented, please ask language designer to implement evaluate() for '+_self);
		throw new NotImplementedException('not implemented, please implement evaluate() for '+_self);
	}
	
	def Value copy() {
		_self.devError('not implemented, please ask language designer to implement copy() for '+_self);
		throw new NotImplementedException('not implemented, please implement copy() for '+_self);
	}
	
	def BooleanValue bEquals(Value rhs) {
		_self.devError('not implemented, please ask language designer to implement equals() for '+_self);
		throw new NotImplementedException('not implemented, please implement equals() for '+_self);
	}
	
	def Value plus(Value rhs) {
		_self.devError('not implemented, please ask language designer to implement plus() for '+_self);
		throw new NotImplementedException('not implemented, please implement plus() for '+_self);
	}
	def Value minus(Value rhs) {
		_self.devError('not implemented, please ask language designer to implement minus() for '+_self);
		throw new NotImplementedException('not implemented, please implement minus() for '+_self);
	}
	def Value mult(Value rhs) {
		_self.devError('not implemented, please ask language designer to implement mult() for '+_self);
		throw new NotImplementedException('not implemented, please implement mult() for '+_self);
	}
	def Value div(Value rhs) {
		_self.devError('not implemented, please ask language designer to implement div() for '+_self);
		throw new NotImplementedException('not implemented, please implement div() for '+_self);
	}
	def Value modulo(Value rhs) {
		_self.devError('not implemented, please ask language designer to implement modulo() for '+_self);
		throw new NotImplementedException('not implemented, please implement modulo() for '+_self);
	}
	def BooleanValue greater(Value rhs) {
		_self.devError('not implemented, please ask language designer to implement greater() for '+_self);
		throw new NotImplementedException('not implemented, please implement greater() for '+_self);
	}
	def BooleanValue lower(Value rhs) {
		_self.devError('not implemented, please ask language designer to implement lower() for '+_self);
		throw new NotImplementedException('not implemented, please implement lower() for '+_self);
	}
	def BooleanValue greaterOrEquals(Value rhs) {
		_self.devError('not implemented, please ask language designer to implement greaterOrEquals() for '+_self);
		throw new NotImplementedException('not implemented, please implement greaterOrEquals() for '+_self);
	}
	def BooleanValue lowerOrEquals(Value rhs) {
		_self.devError('not implemented, please ask language designer to implement lowerOrEquals(() for '+_self);
		throw new NotImplementedException('not implemented, please implement lowerOrEquals(() for '+_self);
	}
	def Value uminus() {
		_self.devError('not implemented, please ask language designer to implement uminus() for '+_self);
		throw new NotImplementedException('not implemented, please implement uminus() for '+_self);
	}
	
	// conversions
	def FloatValue toFloatValue() {
		_self.devError('not implemented, please ask language designer to implement toFloatValue() for '+_self);
		throw new NotImplementedException('not implemented, please implement toFloatValue() for '+_self);
	}
	
	def IntegerValue toIntegerValue() {
		_self.devError('not implemented, please ask language designer to implement toIntegerValue() for '+_self);
		throw new NotImplementedException('not implemented, please implement toIntegerValue() for '+_self);
	}
	
	def Boolean isKindOf(DataType type){
		_self.devError('not implemented, please ask language designer to implement isKindOf() for '+_self);
		throw new NotImplementedException('not implemented, please implement isKindOf() for '+_self);
	}
	
	/*
	 * a human readable version of the value
	 */
	def String valueToString(){
		_self.devError('not implemented, please ask language designer to implement valueToString() for '+_self);
		throw new NotImplementedException('not implemented, please implement valueToString() for '+_self);
	}
	
	def Object rawValue(){
		_self.devError('not implemented, please ask language designer to implement rawValue() for '+_self);
		throw new NotImplementedException('not implemented, please implement rawValue() for '+_self);
	}
}

@Aspect(className=BooleanValue)
class BooleanValueAspect extends ValueAspect {

	def Value evaluate() {
//		_self.devInfo(' -> BooleanValue.evaluate '+_self);
		return _self;
	}
	def Value copy() {
		val BooleanValue aValue = FclFactory.eINSTANCE.createBooleanValue();
		aValue.booleanValue = _self.booleanValue;
		return aValue;
	}
	def BooleanValue bEquals(Value rhs) {
		val BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
		if(rhs instanceof BooleanValue) {
			bValue.booleanValue = _self.booleanValue == (rhs as BooleanValue).booleanValue;
		} else {
			bValue.booleanValue = false;
		}
		return bValue;
	}

	def String valueToString(){
		return _self.booleanValue.toString();
	}
	def Object rawValue(){
		return _self.booleanValue;
	}
	
	
	def Boolean isKindOf(DataType type){
		return type instanceof BooleanValueType;
	}
}

@Aspect(className=EnumerationValue)
class EnumerationValueAspect extends ValueAspect {
	def Value evaluate() {
//		_self.devInfo(' -> EnumerationValue.evaluate '+_self);
		return _self;
	}
	def Value copy() {
		val EnumerationValue aValue = FclFactory.eINSTANCE.createEnumerationValue();
		aValue.enumerationValue = _self.enumerationValue;
		return aValue;
	}
	def BooleanValue bEquals(Value rhs) {
		val BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
		if(rhs instanceof EnumerationValue) {
			bValue.booleanValue = _self.enumerationValue == (rhs as EnumerationValue).enumerationValue;
		} else {
			bValue.booleanValue = false;
		}
		return bValue;
	}
	def String valueToString(){
		return _self.enumerationValue.eContainerOfType(EnumerationValueType).name + 
			'::' + 
			_self.enumerationValue.name;
	}
	def Object rawValue(){
		return _self.enumerationValue;
	}
	
	def Boolean isKindOf(DataType type){
		return type == _self.enumerationValue.eContainerOfType(EnumerationValueType);
	}
}

@Aspect(className=IntegerValue)
class IntegerValueAspect extends ValueAspect {

	def Value evaluate() {
//		_self.devInfo(_self.eContainerOfType(FCLModel).indentation+
//			' -> IntegerValue.evaluate '+_self);
		return _self;
	}
	def Value copy() {
		val IntegerValue aValue = FclFactory.eINSTANCE.createIntegerValue();
		aValue.intValue = _self.intValue;
		return aValue;
	}
	def BooleanValue bEquals(Value rhs) {
		val BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
		bValue.booleanValue = _self.intValue == rhs.toIntegerValue().intValue;
		return bValue;
	}
	def BooleanValue lower(Value rhs) {
		val BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
		bValue.booleanValue = _self.intValue < rhs.toIntegerValue().intValue;
		return bValue;
	}
	def BooleanValue lowerOrEquals(Value rhs) {
		val BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
		bValue.booleanValue = _self.intValue <= rhs.toIntegerValue().intValue;
		return bValue;
	}
	def BooleanValue greater(Value rhs) {
		val BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
		bValue.booleanValue = _self.intValue > rhs.toIntegerValue().intValue;
		return bValue;
	}
	def BooleanValue greaterOrEquals(Value rhs) {
		val BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
		bValue.booleanValue = _self.intValue >= rhs.toIntegerValue().intValue;
		return bValue;
	}
	def Value plus(Value rhs) {
		val IntegerValue iValue = FclFactory.eINSTANCE.createIntegerValue();
		iValue.intValue = _self.intValue + rhs.toIntegerValue().intValue;
		return iValue;
	}
	def Value minus(Value rhs) {
		val IntegerValue iValue = FclFactory.eINSTANCE.createIntegerValue();
		iValue.intValue = _self.intValue - rhs.toIntegerValue().intValue;
		return iValue;
	}
	def Value uminus() {
		val IntegerValue iValue = FclFactory.eINSTANCE.createIntegerValue();
		iValue.intValue = - _self.intValue;
		return iValue;
	}
	def Value mult(Value rhs) {
		val IntegerValue iValue = FclFactory.eINSTANCE.createIntegerValue();
		iValue.intValue = _self.intValue * rhs.toIntegerValue().intValue;
		return iValue;
	}
	
	def Value div(Value rhs) {
		val IntegerValue iValue = FclFactory.eINSTANCE.createIntegerValue();
		iValue.intValue = _self.intValue / rhs.toIntegerValue().intValue;
		return iValue;
	}
	
	def Value modulo(Value rhs) {
		val IntegerValue iValue = FclFactory.eINSTANCE.createIntegerValue();
		iValue.intValue = _self.intValue % rhs.toIntegerValue().intValue;
		return iValue;
	}	
	
	def FloatValue toFloatValue() {
		val FloatValue fValue = FclFactory.eINSTANCE.createFloatValue();
		fValue.floatValue = _self.intValue;
		return fValue;
	}
	
	def IntegerValue toIntegerValue() {
		return _self;
	}
	def String valueToString(){
		return _self.intValue.toString();
	}
	
	def Object rawValue(){
		return _self.intValue;
	}
	
	def Boolean isKindOf(DataType type){
		return type instanceof IntegerValueType;
	}
}

@Aspect(className=StringValue)
class StringValueAspect extends ValueAspect {
	def Value evaluate() {
		return _self;
	}
	
	def Value copy() {
		val StringValue aValue = FclFactory.eINSTANCE.createStringValue();
		aValue.stringValue = _self.stringValue;
		return aValue;
	}
	def BooleanValue bEquals(Value rhs) {
		val BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
		if(rhs instanceof StringValue) {
			bValue.booleanValue = _self.stringValue == (rhs as StringValue).stringValue;
		} else {
			bValue.booleanValue = false;
		}
		return bValue;
	}
	def String valueToString(){
		return _self.stringValue.toString();
	}
	def Object rawValue(){
		return _self.stringValue;
	}
	
	def Boolean isKindOf(DataType type){
		return type instanceof StringValueType;
	}
}


@Aspect(className=FloatValue)
class FloatValueAspect extends ValueAspect {

	def Value evaluate() {
		return _self;
	}
	def Value copy() {
		val FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
		aValue.floatValue = _self.floatValue;
		return aValue;
	}
	def BooleanValue bEquals(Value rhs) {
		val BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
		bValue.booleanValue = _self.floatValue == rhs.toFloatValue().floatValue;
		return bValue;
	}
	def Value plus(Value rhs) {
		val FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
		aValue.floatValue = _self.floatValue + rhs.toFloatValue().floatValue;
		return aValue;
	}
	def Value minus(Value rhs) {
		val FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
		aValue.floatValue = _self.floatValue - rhs.toFloatValue().floatValue;
		return aValue;
	}
	def Value uminus() {
		val FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
		aValue.floatValue = - _self.floatValue;
		return aValue;
	}
	def Value mult(Value rhs) {
		val FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
		aValue.floatValue = _self.floatValue * rhs.toFloatValue().floatValue;
		return aValue;
	}
	
	def Value div(Value rhs) {
		val FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
		aValue.floatValue = _self.floatValue / rhs.toFloatValue().floatValue;
		return aValue;
	}
	def Value modulo(Value rhs) {
		val FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
		aValue.floatValue = _self.floatValue % rhs.toFloatValue().floatValue;
		return aValue;
	}
	
	def BooleanValue greater(Value rhs) {
		val BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
		bValue.booleanValue = _self.floatValue > rhs.toFloatValue().floatValue;
		return bValue;
	}
	def BooleanValue greaterOrEquals(Value rhs) {
		val BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
		bValue.booleanValue = _self.floatValue >= rhs.toFloatValue().floatValue;
		return bValue;
	}
	def BooleanValue lower(Value rhs) {
		val BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
		bValue.booleanValue = _self.floatValue < rhs.toFloatValue().floatValue;
		return bValue;
	}
	def BooleanValue lowerOrEquals(Value rhs) {
		val BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
		bValue.booleanValue = _self.floatValue <= rhs.toFloatValue().floatValue;
		return bValue;
	}
	
	def FloatValue toFloatValue() {
		return _self;
	}
	def IntegerValue toIntegerValue() { 
		val IntegerValue iValue = FclFactory.eINSTANCE.createIntegerValue();
		iValue.intValue = Math.round(_self.floatValue).intValue();
		return iValue;
	}
	def String valueToString(){
		return String.format(Locale.US, "%.3G", _self.floatValue);
	}
	def Object rawValue(){
		return _self.floatValue;
	}
	
	def Boolean isKindOf(DataType type){
		return type instanceof FloatValueType;
	}
}

@Aspect(className=StructValue)
class StructValueAspect extends ValueAspect {
	def Value evaluate() {
		return  _self;
	}
	
	def Value getValueInProperty(String propertyName){
		val StructPropertyValue propValue = _self.structProperties.findFirst[p | p.name == propertyName];
		if(propValue !== null){ 
			return  propValue.value;
		} else {
			// return  null;  // bug ALe cannot assign null 
		}
	}
	
	def Value copy() {
		val StructValue aValue = FclFactory.eINSTANCE.createStructValue();
		for(structPropValue : _self.structProperties){
			aValue.structProperties += structPropValue.copy();
		}
		return  aValue;
	}
	
	/**
	 * Set value in property with the given name,
	 * warning, no type control we suppose that the caller knows if the given property is allowed in the given DataStruct
	 */
	def void setValueInProperty(String propertyName, Value newPropValue){
		val StructPropertyValue propValue = _self.structProperties.findFirst[p | p.name == propertyName];
		if(propValue !== null){ 
			propValue.value = newPropValue;
		} else {
			val StructPropertyValue newStructPropValue = FclFactory.eINSTANCE.createStructPropertyValue();
			newStructPropValue.name = propertyName;
			newStructPropValue.value = newPropValue;
			_self.structProperties += newStructPropValue;
		}
	}
	def String valueToString(){
		return  '{' + 
			_self.structProperties.map[p|p.name+'='+p.value.valueToString()].join(', ') + 
			'}';
	}
	def Boolean isKindOf(DataType type){
		return  type instanceof DataStructType;
		// TODO checks that is conforms to the correct DataStructType
	}
	
	def Object rawValue(){
		// return self so we can still compare it
		return _self;
	}
}

@Aspect(className=StructPropertyValue)
class StructPropertyValueAspect /* extends NamedElementAspect */{

	def Value evaluate() {
		return _self.value;
	}
	
	def StructPropertyValue copy() {
		val StructPropertyValue aValue = FclFactory.eINSTANCE.createStructPropertyValue();
		aValue.value = _self.value.copy();
		aValue.name = _self.name;
		return aValue;
	}
}



