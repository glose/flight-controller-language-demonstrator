package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspectDataTypeAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NotImplementedException;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.xtext.xbase.lib.Exceptions;

@Aspect(className = DataType.class)
@SuppressWarnings("all")
public abstract class DataTypeAspect {
  /**
   * Creates a value for the given type
   * Initiate its internal value by parsing the string parameter
   */
  public static Value createValue(final DataType _self, final String internalValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspectDataTypeAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspectDataTypeAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspect#Value createValue(String) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueTypeAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.StringValueType){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueTypeAspect.createValue((fr.inria.glose.fcl.model.fcl.StringValueType)_self,internalValue);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspect#Value createValue(String) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueTypeAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspect#Value createValue(String) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueTypeAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.BooleanValueType){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueTypeAspect.createValue((fr.inria.glose.fcl.model.fcl.BooleanValueType)_self,internalValue);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspect#Value createValue(String) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueTypeAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspect#Value createValue(String) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueTypeAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValueType){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueTypeAspect.createValue((fr.inria.glose.fcl.model.fcl.FloatValueType)_self,internalValue);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspect#Value createValue(String) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueTypeAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspect#Value createValue(String) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueTypeAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.EnumerationValueType){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueTypeAspect.createValue((fr.inria.glose.fcl.model.fcl.EnumerationValueType)_self,internalValue);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspect#Value createValue(String) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueTypeAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspect#Value createValue(String) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueTypeAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValueType){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueTypeAspect.createValue((fr.inria.glose.fcl.model.fcl.IntegerValueType)_self,internalValue);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspect#Value createValue(String) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueTypeAspect
    // #DispatchPointCut_before# Value createValue(String)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.DataType){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspect._privk3_createValue(_self_, (fr.inria.glose.fcl.model.fcl.DataType)_self,internalValue);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static Value _privk3_createValue(final DataTypeAspectDataTypeAspectProperties _self_, final DataType _self, final String internalValue) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement createValue() for " + _self));
      throw new NotImplementedException(("not implemented, please implement createValue() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
