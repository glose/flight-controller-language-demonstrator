package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectProperties;
import fr.inria.glose.fcl.model.fcl.FunctionVarDecl;
import java.util.Map;

@SuppressWarnings("all")
public class FunctionVarDeclAspectFunctionVarDeclAspectContext {
  public final static FunctionVarDeclAspectFunctionVarDeclAspectContext INSTANCE = new FunctionVarDeclAspectFunctionVarDeclAspectContext();
  
  public static FunctionVarDeclAspectFunctionVarDeclAspectProperties getSelf(final FunctionVarDecl _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<FunctionVarDecl, FunctionVarDeclAspectFunctionVarDeclAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.FunctionVarDecl, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectProperties>();
  
  public Map<FunctionVarDecl, FunctionVarDeclAspectFunctionVarDeclAspectProperties> getMap() {
    return map;
  }
}
