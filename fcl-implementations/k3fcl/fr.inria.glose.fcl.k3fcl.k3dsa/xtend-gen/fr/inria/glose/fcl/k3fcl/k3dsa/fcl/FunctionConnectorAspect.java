package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.model.fcl.FunctionConnector;

@Aspect(className = FunctionConnector.class)
@SuppressWarnings("all")
public class FunctionConnectorAspect {
}
