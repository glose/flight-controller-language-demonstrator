package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspectModeAutomataAspectProperties;
import fr.inria.glose.fcl.model.fcl.ModeAutomata;
import java.util.Map;

@SuppressWarnings("all")
public class ModeAutomataAspectModeAutomataAspectContext {
  public final static ModeAutomataAspectModeAutomataAspectContext INSTANCE = new ModeAutomataAspectModeAutomataAspectContext();
  
  public static ModeAutomataAspectModeAutomataAspectProperties getSelf(final ModeAutomata _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspectModeAutomataAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<ModeAutomata, ModeAutomataAspectModeAutomataAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.ModeAutomata, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspectModeAutomataAspectProperties>();
  
  public Map<ModeAutomata, ModeAutomataAspectModeAutomataAspectProperties> getMap() {
    return map;
  }
}
