package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDeclarationReferenceAspectCallDeclarationReferenceAspectProperties;
import fr.inria.glose.fcl.model.fcl.CallDeclarationReference;
import java.util.Map;

@SuppressWarnings("all")
public class CallDeclarationReferenceAspectCallDeclarationReferenceAspectContext {
  public final static CallDeclarationReferenceAspectCallDeclarationReferenceAspectContext INSTANCE = new CallDeclarationReferenceAspectCallDeclarationReferenceAspectContext();
  
  public static CallDeclarationReferenceAspectCallDeclarationReferenceAspectProperties getSelf(final CallDeclarationReference _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDeclarationReferenceAspectCallDeclarationReferenceAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<CallDeclarationReference, CallDeclarationReferenceAspectCallDeclarationReferenceAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.CallDeclarationReference, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDeclarationReferenceAspectCallDeclarationReferenceAspectProperties>();
  
  public Map<CallDeclarationReference, CallDeclarationReferenceAspectCallDeclarationReferenceAspectProperties> getMap() {
    return map;
  }
}
