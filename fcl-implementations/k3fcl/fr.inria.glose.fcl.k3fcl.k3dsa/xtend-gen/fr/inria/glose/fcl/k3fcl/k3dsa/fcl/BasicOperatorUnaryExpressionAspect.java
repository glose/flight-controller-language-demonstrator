package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import com.google.common.base.Objects;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorUnaryExpressionAspectBasicOperatorUnaryExpressionAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NotImplementedException;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnaryExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.BasicOperatorUnaryExpression;
import fr.inria.glose.fcl.model.fcl.BooleanValue;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.UnaryOperator;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.xtext.xbase.lib.Exceptions;

@Aspect(className = BasicOperatorUnaryExpression.class)
@SuppressWarnings("all")
public class BasicOperatorUnaryExpressionAspect extends UnaryExpressionAspect {
  public static Value evaluate(final BasicOperatorUnaryExpression _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorUnaryExpressionAspectBasicOperatorUnaryExpressionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorUnaryExpressionAspectBasicOperatorUnaryExpressionAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.BasicOperatorUnaryExpression){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorUnaryExpressionAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.BasicOperatorUnaryExpression)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static Value _privk3_evaluate(final BasicOperatorUnaryExpressionAspectBasicOperatorUnaryExpressionAspectProperties _self_, final BasicOperatorUnaryExpression _self) {
    try {
      final Value lhs = ExpressionAspect.evaluate(_self.getOperand());
      UnaryOperator _operator = _self.getOperator();
      boolean _equals = Objects.equal(_operator, UnaryOperator.UNARYMINUS);
      if (_equals) {
        return ValueAspect.uminus(lhs);
      } else {
        UnaryOperator _operator_1 = _self.getOperator();
        boolean _equals_1 = Objects.equal(_operator_1, UnaryOperator.NOT);
        if (_equals_1) {
          final BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
          boolean _evaluateAsBoolean = ExpressionAspect.evaluateAsBoolean(_self.getOperand());
          boolean _not = (!_evaluateAsBoolean);
          bValue.setBooleanValue(_not);
          return bValue;
        } else {
          throw new NotImplementedException(("not implemented, please implement evaluate() for " + _self));
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
