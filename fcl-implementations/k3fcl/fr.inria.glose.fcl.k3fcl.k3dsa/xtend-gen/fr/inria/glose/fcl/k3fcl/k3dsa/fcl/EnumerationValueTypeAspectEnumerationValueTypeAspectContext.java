package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueTypeAspectEnumerationValueTypeAspectProperties;
import fr.inria.glose.fcl.model.fcl.EnumerationValueType;
import java.util.Map;

@SuppressWarnings("all")
public class EnumerationValueTypeAspectEnumerationValueTypeAspectContext {
  public final static EnumerationValueTypeAspectEnumerationValueTypeAspectContext INSTANCE = new EnumerationValueTypeAspectEnumerationValueTypeAspectContext();
  
  public static EnumerationValueTypeAspectEnumerationValueTypeAspectProperties getSelf(final EnumerationValueType _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueTypeAspectEnumerationValueTypeAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<EnumerationValueType, EnumerationValueTypeAspectEnumerationValueTypeAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.EnumerationValueType, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueTypeAspectEnumerationValueTypeAspectProperties>();
  
  public Map<EnumerationValueType, EnumerationValueTypeAspectEnumerationValueTypeAspectProperties> getMap() {
    return map;
  }
}
