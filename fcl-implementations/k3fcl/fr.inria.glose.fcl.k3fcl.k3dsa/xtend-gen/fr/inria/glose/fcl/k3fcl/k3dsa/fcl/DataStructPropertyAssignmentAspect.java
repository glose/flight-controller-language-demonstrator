package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspectDataStructPropertyAssignmentAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyReferenceAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLException;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.PrimitiveActionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.Assignable;
import fr.inria.glose.fcl.model.fcl.CallableDeclaration;
import fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment;
import fr.inria.glose.fcl.model.fcl.DataStructType;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.StructValue;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Exceptions;

@Aspect(className = DataStructPropertyAssignment.class, with = { PrimitiveActionAspect.class })
@SuppressWarnings("all")
public class DataStructPropertyAssignmentAspect extends DataStructPropertyReferenceAspect {
  /**
   * BE CAREFUL :
   * 
   * This class has more than one superclass
   * please specify which parent you want with the 'super' expected calling
   */
  public static void doAction(final DataStructPropertyAssignment _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspectDataStructPropertyAssignmentAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspectDataStructPropertyAssignmentAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void doAction()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspect._privk3_doAction(_self_, (fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment)_self);
    };
  }
  
  public static void assignToStruct(final DataStructPropertyAssignment _self, final StructValue sValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspectDataStructPropertyAssignmentAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspectDataStructPropertyAssignmentAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void assignToStruct(StructValue)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspect._privk3_assignToStruct(_self_, (fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment)_self,sValue);
    };
  }
  
  protected static void _privk3_doAction(final DataStructPropertyAssignmentAspectDataStructPropertyAssignmentAspectProperties _self_, final DataStructPropertyAssignment _self) {
    try {
      final Assignable currentTarget = _self.getAssignable();
      final DataType resolvedType = AssignableAspect.getResolvedType(currentTarget);
      if ((resolvedType instanceof DataStructType)) {
        if ((currentTarget instanceof CallableDeclaration)) {
          final Value targetVal = CallableDeclarationAspect.call(((CallableDeclaration) currentTarget));
          if ((targetVal instanceof StructValue)) {
            DataStructPropertyAssignmentAspect.assignToStruct(_self, ((StructValue) targetVal));
          } else {
            EObjectAspect.error(_self, (((((("assignment navigation error for " + _self) + " applied to ") + currentTarget) + " targetValue ") + targetVal) + " is not a Structure"));
            throw new FCLException((((((("assignment navigation error for " + _self) + " applied to ") + currentTarget) + " targetValue ") + targetVal) + " is not a Structure"));
          }
        } else {
          EObjectAspect.devError(_self, (((("assignment navigation error for " + _self) + " applied to ") + currentTarget) + " (not a callable)"));
          throw new FCLException(((("assignment navigation error for " + _self) + " applied to ") + currentTarget));
        }
      } else {
        EObjectAspect.error(_self, ((("assignment navigation error for " + _self) + " applied to ") + currentTarget));
        throw new FCLException(((("assignment navigation error for " + _self) + " applied to ") + currentTarget));
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static void _privk3_assignToStruct(final DataStructPropertyAssignmentAspectDataStructPropertyAssignmentAspectProperties _self_, final DataStructPropertyAssignment _self, final StructValue sValue) {
    try {
      StructValue finalTarget = sValue;
      Integer index = Integer.valueOf(0);
      EList<String> _propertyName = _self.getPropertyName();
      for (final String propName : _propertyName) {
        {
          index = Integer.valueOf(((index).intValue() + 1));
          int _size = _self.getPropertyName().size();
          boolean _equals = ((index).intValue() == _size);
          if (_equals) {
            final Value evaluedValue = ValueAspect.copy(ExpressionAspect.evaluate(_self.getExpression()));
            StructValueAspect.setValueInProperty(finalTarget, propName, evaluedValue);
          } else {
            final Value newTarget = StructValueAspect.getValueInProperty(((StructValue) finalTarget), propName);
            if ((newTarget instanceof StructValue)) {
              finalTarget = ((StructValue)newTarget);
            } else {
              EObjectAspect.error(_self, (((((((("assignment navigation error for " + _self) + " applied to ") + sValue) + "; target of ") + propName) + " is a ") + newTarget) + " instead of a StructValue"));
              throw new FCLException(((("assignment navigation error for " + _self) + " applied to ") + sValue));
            }
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
