package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import com.google.common.base.Objects;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorBinaryExpressionAspectBasicOperatorBinaryExpressionAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BinaryExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NotImplementedException;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.BasicOperatorBinaryExpression;
import fr.inria.glose.fcl.model.fcl.BinaryOperator;
import fr.inria.glose.fcl.model.fcl.BooleanValue;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.xtext.xbase.lib.Exceptions;

@Aspect(className = BasicOperatorBinaryExpression.class)
@SuppressWarnings("all")
public class BasicOperatorBinaryExpressionAspect extends BinaryExpressionAspect {
  public static Value evaluate(final BasicOperatorBinaryExpression _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorBinaryExpressionAspectBasicOperatorBinaryExpressionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorBinaryExpressionAspectBasicOperatorBinaryExpressionAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.BasicOperatorBinaryExpression){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorBinaryExpressionAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.BasicOperatorBinaryExpression)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static Value _privk3_evaluate(final BasicOperatorBinaryExpressionAspectBasicOperatorBinaryExpressionAspectProperties _self_, final BasicOperatorBinaryExpression _self) {
    try {
      Value result = null;
      BinaryOperator _operator = _self.getOperator();
      boolean _equals = Objects.equal(_operator, BinaryOperator.OR);
      if (_equals) {
        final BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
        bValue.setBooleanValue((ExpressionAspect.evaluateAsBoolean(_self.getLhsOperand()) || ExpressionAspect.evaluateAsBoolean(_self.getRhsOperand())));
        result = bValue;
      } else {
        BinaryOperator _operator_1 = _self.getOperator();
        boolean _equals_1 = Objects.equal(_operator_1, BinaryOperator.AND);
        if (_equals_1) {
          final BooleanValue bValue_1 = FclFactory.eINSTANCE.createBooleanValue();
          bValue_1.setBooleanValue((ExpressionAspect.evaluateAsBoolean(_self.getLhsOperand()) && ExpressionAspect.evaluateAsBoolean(_self.getRhsOperand())));
          result = bValue_1;
        } else {
          final Value lhs = ExpressionAspect.evaluate(_self.getLhsOperand());
          final Value rhs = ExpressionAspect.evaluate(_self.getRhsOperand());
          BinaryOperator _operator_2 = _self.getOperator();
          boolean _equals_2 = Objects.equal(_operator_2, BinaryOperator.EQUAL);
          if (_equals_2) {
            result = ValueAspect.bEquals(lhs, rhs);
          } else {
            BinaryOperator _operator_3 = _self.getOperator();
            boolean _equals_3 = Objects.equal(_operator_3, BinaryOperator.PLUS);
            if (_equals_3) {
              result = ValueAspect.plus(lhs, rhs);
            } else {
              BinaryOperator _operator_4 = _self.getOperator();
              boolean _equals_4 = Objects.equal(_operator_4, BinaryOperator.MINUS);
              if (_equals_4) {
                result = ValueAspect.minus(lhs, rhs);
              } else {
                BinaryOperator _operator_5 = _self.getOperator();
                boolean _equals_5 = Objects.equal(_operator_5, BinaryOperator.MULT);
                if (_equals_5) {
                  result = ValueAspect.mult(lhs, rhs);
                } else {
                  BinaryOperator _operator_6 = _self.getOperator();
                  boolean _equals_6 = Objects.equal(_operator_6, BinaryOperator.DIV);
                  if (_equals_6) {
                    result = ValueAspect.div(lhs, rhs);
                  } else {
                    BinaryOperator _operator_7 = _self.getOperator();
                    boolean _equals_7 = Objects.equal(_operator_7, BinaryOperator.MOD);
                    if (_equals_7) {
                      result = ValueAspect.modulo(lhs, rhs);
                    } else {
                      BinaryOperator _operator_8 = _self.getOperator();
                      boolean _equals_8 = Objects.equal(_operator_8, BinaryOperator.GREATER);
                      if (_equals_8) {
                        result = ValueAspect.greater(lhs, rhs);
                      } else {
                        BinaryOperator _operator_9 = _self.getOperator();
                        boolean _equals_9 = Objects.equal(_operator_9, BinaryOperator.LOWER);
                        if (_equals_9) {
                          result = ValueAspect.lower(lhs, rhs);
                        } else {
                          BinaryOperator _operator_10 = _self.getOperator();
                          boolean _equals_10 = Objects.equal(_operator_10, BinaryOperator.GREATEROREQUAL);
                          if (_equals_10) {
                            result = ValueAspect.greaterOrEquals(lhs, rhs);
                          } else {
                            BinaryOperator _operator_11 = _self.getOperator();
                            boolean _equals_11 = Objects.equal(_operator_11, BinaryOperator.LOWEROREQUAL);
                            if (_equals_11) {
                              result = ValueAspect.lowerOrEquals(lhs, rhs);
                            } else {
                              throw new NotImplementedException(("not implemented, please implement evaluate() for " + _self));
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return result;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
