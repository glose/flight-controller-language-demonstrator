package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureCallAspectProcedureCallAspectProperties;
import fr.inria.glose.fcl.model.fcl.ProcedureCall;
import java.util.Map;

@SuppressWarnings("all")
public class ProcedureCallAspectProcedureCallAspectContext {
  public final static ProcedureCallAspectProcedureCallAspectContext INSTANCE = new ProcedureCallAspectProcedureCallAspectContext();
  
  public static ProcedureCallAspectProcedureCallAspectProperties getSelf(final ProcedureCall _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureCallAspectProcedureCallAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<ProcedureCall, ProcedureCallAspectProcedureCallAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.ProcedureCall, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureCallAspectProcedureCallAspectProperties>();
  
  public Map<ProcedureCall, ProcedureCallAspectProcedureCallAspectProperties> getMap() {
    return map;
  }
}
