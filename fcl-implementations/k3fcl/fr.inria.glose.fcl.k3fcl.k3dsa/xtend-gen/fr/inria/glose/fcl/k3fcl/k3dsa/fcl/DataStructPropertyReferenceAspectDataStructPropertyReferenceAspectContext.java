package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyReferenceAspectDataStructPropertyReferenceAspectProperties;
import fr.inria.glose.fcl.model.fcl.DataStructPropertyReference;
import java.util.Map;

@SuppressWarnings("all")
public class DataStructPropertyReferenceAspectDataStructPropertyReferenceAspectContext {
  public final static DataStructPropertyReferenceAspectDataStructPropertyReferenceAspectContext INSTANCE = new DataStructPropertyReferenceAspectDataStructPropertyReferenceAspectContext();
  
  public static DataStructPropertyReferenceAspectDataStructPropertyReferenceAspectProperties getSelf(final DataStructPropertyReference _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyReferenceAspectDataStructPropertyReferenceAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<DataStructPropertyReference, DataStructPropertyReferenceAspectDataStructPropertyReferenceAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.DataStructPropertyReference, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyReferenceAspectDataStructPropertyReferenceAspectProperties>();
  
  public Map<DataStructPropertyReference, DataStructPropertyReferenceAspectDataStructPropertyReferenceAspectProperties> getMap() {
    return map;
  }
}
