package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NewDataStructAspectNewDataStructAspectProperties;
import fr.inria.glose.fcl.model.fcl.NewDataStruct;
import java.util.Map;

@SuppressWarnings("all")
public class NewDataStructAspectNewDataStructAspectContext {
  public final static NewDataStructAspectNewDataStructAspectContext INSTANCE = new NewDataStructAspectNewDataStructAspectContext();
  
  public static NewDataStructAspectNewDataStructAspectProperties getSelf(final NewDataStruct _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NewDataStructAspectNewDataStructAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<NewDataStruct, NewDataStructAspectNewDataStructAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.NewDataStruct, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NewDataStructAspectNewDataStructAspectProperties>();
  
  public Map<NewDataStruct, NewDataStructAspectNewDataStructAspectProperties> getMap() {
    return map;
  }
}
