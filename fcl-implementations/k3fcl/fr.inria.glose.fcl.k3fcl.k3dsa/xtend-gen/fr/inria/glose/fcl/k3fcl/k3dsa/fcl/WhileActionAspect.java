package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ControlStructureActionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.WhileActionAspectWhileActionAspectProperties;
import fr.inria.glose.fcl.model.fcl.Action;
import fr.inria.glose.fcl.model.fcl.WhileAction;

@Aspect(className = WhileAction.class)
@SuppressWarnings("all")
public class WhileActionAspect extends ControlStructureActionAspect {
  public static void doAction(final WhileAction _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.WhileActionAspectWhileActionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.WhileActionAspectWhileActionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void doAction()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.WhileAction){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.WhileActionAspect._privk3_doAction(_self_, (fr.inria.glose.fcl.model.fcl.WhileAction)_self);
    };
  }
  
  protected static void _privk3_doAction(final WhileActionAspectWhileActionAspectProperties _self_, final WhileAction _self) {
    while (ExpressionAspect.evaluateAsBoolean(_self.getCondition())) {
      Action _action = _self.getAction();
      boolean _tripleNotEquals = (_action != null);
      if (_tripleNotEquals) {
        ActionAspect.doAction(_self.getAction());
      }
    }
  }
}
