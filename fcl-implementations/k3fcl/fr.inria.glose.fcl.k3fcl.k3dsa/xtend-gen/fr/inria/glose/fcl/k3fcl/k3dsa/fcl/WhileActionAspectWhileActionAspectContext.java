package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.WhileActionAspectWhileActionAspectProperties;
import fr.inria.glose.fcl.model.fcl.WhileAction;
import java.util.Map;

@SuppressWarnings("all")
public class WhileActionAspectWhileActionAspectContext {
  public final static WhileActionAspectWhileActionAspectContext INSTANCE = new WhileActionAspectWhileActionAspectContext();
  
  public static WhileActionAspectWhileActionAspectProperties getSelf(final WhileAction _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.WhileActionAspectWhileActionAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<WhileAction, WhileActionAspectWhileActionAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.WhileAction, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.WhileActionAspectWhileActionAspectProperties>();
  
  public Map<WhileAction, WhileActionAspectWhileActionAspectProperties> getMap() {
    return map;
  }
}
