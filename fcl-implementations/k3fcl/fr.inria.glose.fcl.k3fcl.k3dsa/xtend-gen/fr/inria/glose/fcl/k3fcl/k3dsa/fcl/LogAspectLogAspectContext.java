package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.LogAspectLogAspectProperties;
import fr.inria.glose.fcl.model.fcl.Log;
import java.util.Map;

@SuppressWarnings("all")
public class LogAspectLogAspectContext {
  public final static LogAspectLogAspectContext INSTANCE = new LogAspectLogAspectContext();
  
  public static LogAspectLogAspectProperties getSelf(final Log _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.LogAspectLogAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<Log, LogAspectLogAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.Log, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.LogAspectLogAspectProperties>();
  
  public Map<Log, LogAspectLogAspectProperties> getMap() {
    return map;
  }
}
