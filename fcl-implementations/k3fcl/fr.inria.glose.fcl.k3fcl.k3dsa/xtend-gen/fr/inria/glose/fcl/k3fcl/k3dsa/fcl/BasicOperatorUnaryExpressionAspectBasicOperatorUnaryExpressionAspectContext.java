package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorUnaryExpressionAspectBasicOperatorUnaryExpressionAspectProperties;
import fr.inria.glose.fcl.model.fcl.BasicOperatorUnaryExpression;
import java.util.Map;

@SuppressWarnings("all")
public class BasicOperatorUnaryExpressionAspectBasicOperatorUnaryExpressionAspectContext {
  public final static BasicOperatorUnaryExpressionAspectBasicOperatorUnaryExpressionAspectContext INSTANCE = new BasicOperatorUnaryExpressionAspectBasicOperatorUnaryExpressionAspectContext();
  
  public static BasicOperatorUnaryExpressionAspectBasicOperatorUnaryExpressionAspectProperties getSelf(final BasicOperatorUnaryExpression _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorUnaryExpressionAspectBasicOperatorUnaryExpressionAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<BasicOperatorUnaryExpression, BasicOperatorUnaryExpressionAspectBasicOperatorUnaryExpressionAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.BasicOperatorUnaryExpression, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorUnaryExpressionAspectBasicOperatorUnaryExpressionAspectProperties>();
  
  public Map<BasicOperatorUnaryExpression, BasicOperatorUnaryExpressionAspectBasicOperatorUnaryExpressionAspectProperties> getMap() {
    return map;
  }
}
