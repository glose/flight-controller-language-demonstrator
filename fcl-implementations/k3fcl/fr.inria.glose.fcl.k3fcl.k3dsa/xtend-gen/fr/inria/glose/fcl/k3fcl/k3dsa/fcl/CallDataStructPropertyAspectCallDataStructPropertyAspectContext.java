package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDataStructPropertyAspectCallDataStructPropertyAspectProperties;
import fr.inria.glose.fcl.model.fcl.CallDataStructProperty;
import java.util.Map;

@SuppressWarnings("all")
public class CallDataStructPropertyAspectCallDataStructPropertyAspectContext {
  public final static CallDataStructPropertyAspectCallDataStructPropertyAspectContext INSTANCE = new CallDataStructPropertyAspectCallDataStructPropertyAspectContext();
  
  public static CallDataStructPropertyAspectCallDataStructPropertyAspectProperties getSelf(final CallDataStructProperty _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDataStructPropertyAspectCallDataStructPropertyAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<CallDataStructProperty, CallDataStructPropertyAspectCallDataStructPropertyAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.CallDataStructProperty, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDataStructPropertyAspectCallDataStructPropertyAspectProperties>();
  
  public Map<CallDataStructProperty, CallDataStructPropertyAspectCallDataStructPropertyAspectProperties> getMap() {
    return map;
  }
}
