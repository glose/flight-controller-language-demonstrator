package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExternalEventAspectExternalEventAspectProperties;
import fr.inria.glose.fcl.model.fcl.ExternalEvent;
import java.util.Map;

@SuppressWarnings("all")
public class ExternalEventAspectExternalEventAspectContext {
  public final static ExternalEventAspectExternalEventAspectContext INSTANCE = new ExternalEventAspectExternalEventAspectContext();
  
  public static ExternalEventAspectExternalEventAspectProperties getSelf(final ExternalEvent _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExternalEventAspectExternalEventAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<ExternalEvent, ExternalEventAspectExternalEventAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.ExternalEvent, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExternalEventAspectExternalEventAspectProperties>();
  
  public Map<ExternalEvent, ExternalEventAspectExternalEventAspectProperties> getMap() {
    return map;
  }
}
