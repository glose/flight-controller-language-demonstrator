package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureAspectFCLProcedureAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureAspect;
import fr.inria.glose.fcl.model.fcl.Action;
import fr.inria.glose.fcl.model.fcl.FCLProcedure;

@Aspect(className = FCLProcedure.class)
@SuppressWarnings("all")
public class FCLProcedureAspect extends ProcedureAspect {
  public static void runProcedure(final FCLProcedure _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureAspectFCLProcedureAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureAspectFCLProcedureAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void runProcedure()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLProcedure){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureAspect._privk3_runProcedure(_self_, (fr.inria.glose.fcl.model.fcl.FCLProcedure)_self);
    };
  }
  
  protected static void _privk3_runProcedure(final FCLProcedureAspectFCLProcedureAspectProperties _self_, final FCLProcedure _self) {
    Action _action = _self.getAction();
    boolean _tripleNotEquals = (_action != null);
    if (_tripleNotEquals) {
      ActionAspect.doAction(_self.getAction());
    }
  }
}
