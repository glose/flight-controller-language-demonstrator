package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignmentAspectAssignmentAspectProperties;
import fr.inria.glose.fcl.model.fcl.Assignment;
import java.util.Map;

@SuppressWarnings("all")
public class AssignmentAspectAssignmentAspectContext {
  public final static AssignmentAspectAssignmentAspectContext INSTANCE = new AssignmentAspectAssignmentAspectContext();
  
  public static AssignmentAspectAssignmentAspectProperties getSelf(final Assignment _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignmentAspectAssignmentAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<Assignment, AssignmentAspectAssignmentAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.Assignment, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignmentAspectAssignmentAspectProperties>();
  
  public Map<Assignment, AssignmentAspectAssignmentAspectProperties> getMap() {
    return map;
  }
}
