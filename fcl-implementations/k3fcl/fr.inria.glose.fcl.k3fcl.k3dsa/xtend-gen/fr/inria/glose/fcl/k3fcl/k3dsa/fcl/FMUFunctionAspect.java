package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.diverse.k3.al.annotationprocessor.Step;
import fr.inria.glose.fcl.k3fcl.k3dsa.Activator;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLException;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.InvalidData;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.DirectionKind;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FMUFunction;
import fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.FunctionTimeReference;
import fr.inria.glose.fcl.model.fcl.FunctionVarDecl;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmFactory;
import java.util.List;
import org.eclipse.core.resources.IFile;
import org.eclipse.gemoc.commons.eclipse.core.resources.IFileUtils;
import org.eclipse.gemoc.commons.eclipse.emf.EObjectUtil;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.javafmi.modeldescription.SimpleType;
import org.javafmi.modeldescription.v2.ScalarVariable;
import org.javafmi.proxy.Status;
import org.javafmi.wrapper.Variable;
import org.javafmi.wrapper.generic.Simulation2;
import org.javafmi.wrapper.v2.Access;

@Aspect(className = FMUFunction.class)
@SuppressWarnings("all")
public class FMUFunctionAspect extends FunctionAspect {
  public static void initializeFMURuntime(final FMUFunction _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void initializeFMURuntime()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FMUFunction){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect._privk3_initializeFMURuntime(_self_, (fr.inria.glose.fcl.model.fcl.FMUFunction)_self);
    };
  }
  
  public static void checkPortconsistency(final FMUFunction _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void checkPortconsistency()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FMUFunction){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect._privk3_checkPortconsistency(_self_, (fr.inria.glose.fcl.model.fcl.FMUFunction)_self);
    };
  }
  
  public static void setFMUVarFromInputPorts(final FMUFunction _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void setFMUVarFromInputPorts()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FMUFunction){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect._privk3_setFMUVarFromInputPorts(_self_, (fr.inria.glose.fcl.model.fcl.FMUFunction)_self);
    };
  }
  
  @Step
  public static void evalFunction(final FMUFunction _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void evalFunction()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FMUFunction){
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand command = new fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand() {
    		@Override
    		public void execute() {
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect._privk3_evalFunction(_self_, (fr.inria.glose.fcl.model.fcl.FMUFunction)_self);
    		}
    	};
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager stepManager = fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepManagerRegistry.getInstance().findStepManager(_self);
    	if (stepManager != null) {
    		stepManager.executeStep(_self, new Object[] {_self}, command, "FMUFunction", "evalFunction");
    	} else {
    		command.execute();
    	}
    	;
    };
  }
  
  public static void loadOutputPortsFromFMU(final FMUFunction _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void loadOutputPortsFromFMU()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FMUFunction){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect._privk3_loadOutputPortsFromFMU(_self_, (fr.inria.glose.fcl.model.fcl.FMUFunction)_self);
    };
  }
  
  public static void doStep(final FMUFunction _self, final double stepSize) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void doStep(double)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FMUFunction){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect._privk3_doStep(_self_, (fr.inria.glose.fcl.model.fcl.FMUFunction)_self,stepSize);
    };
  }
  
  public static double getSimulationCurrentTime(final FMUFunction _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# double getSimulationCurrentTime()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FMUFunction){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect._privk3_getSimulationCurrentTime(_self_, (fr.inria.glose.fcl.model.fcl.FMUFunction)_self);
    };
    return (double)result;
  }
  
  public static String getKnownOutputVariables(final FMUFunction _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# String getKnownOutputVariables()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FMUFunction){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect._privk3_getKnownOutputVariables(_self_, (fr.inria.glose.fcl.model.fcl.FMUFunction)_self);
    };
    return (java.lang.String)result;
  }
  
  public static String getKnownInputVariables(final FMUFunction _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# String getKnownInputVariables()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FMUFunction){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect._privk3_getKnownInputVariables(_self_, (fr.inria.glose.fcl.model.fcl.FMUFunction)_self);
    };
    return (java.lang.String)result;
  }
  
  public static FMUSimulationWrapper fmuSimulationWrapper(final FMUFunction _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# FMUSimulationWrapper fmuSimulationWrapper()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FMUFunction){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect._privk3_fmuSimulationWrapper(_self_, (fr.inria.glose.fcl.model.fcl.FMUFunction)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper)result;
  }
  
  public static void fmuSimulationWrapper(final FMUFunction _self, final FMUSimulationWrapper fmuSimulationWrapper) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void fmuSimulationWrapper(FMUSimulationWrapper)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FMUFunction){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect._privk3_fmuSimulationWrapper(_self_, (fr.inria.glose.fcl.model.fcl.FMUFunction)_self,fmuSimulationWrapper);
    };
  }
  
  protected static void _privk3_initializeFMURuntime(final FMUFunctionAspectFMUFunctionAspectProperties _self_, final FMUFunction _self) {
    try {
      String _name = _self.getName();
      String _plus = ("-> initializeFMURuntime() " + _name);
      String _plus_1 = (_plus + " with path=");
      String _runnableFmuPath = _self.getRunnableFmuPath();
      String _plus_2 = (_plus_1 + _runnableFmuPath);
      EObjectAspect.devInfo(_self, _plus_2);
      _self.setFmuSimulationWrapper(Interpreter_vmFactory.eINSTANCE.createFMUSimulationWrapper());
      try {
        final IFile iFile = IFileUtils.getIFileFromWorkspaceOrFileSystem(_self.getRunnableFmuPath());
        final String FMUFilePath = iFile.getLocation().toString();
        final Simulation2 aRunnableFmu = new Simulation2(FMUFilePath);
        aRunnableFmu.init(0.0);
        FMUSimulationWrapper _fmuSimulationWrapper = _self.getFmuSimulationWrapper();
        _fmuSimulationWrapper.setFmiSimulation2(aRunnableFmu);
      } catch (final Throwable _t) {
        if (_t instanceof Exception) {
          final Exception e = (Exception)_t;
          String _runnableFmuPath_1 = _self.getRunnableFmuPath();
          String _plus_3 = ("Failed to initialize FMU " + _runnableFmuPath_1);
          Activator.error(_plus_3, e);
        } else {
          throw Exceptions.sneakyThrow(_t);
        }
      }
      FMUSimulationWrapper _fmuSimulationWrapper_1 = _self.getFmuSimulationWrapper();
      String _plus_4 = ("-> initializeFMURuntime() self.fmuSimulationWrapper=" + _fmuSimulationWrapper_1);
      EObjectAspect.devDebug(_self, _plus_4);
      Simulation2 _fmiSimulation2 = _self.getFmuSimulationWrapper().getFmiSimulation2();
      String _plus_5 = ("-> initializeFMURuntime() self.fmuSimulationWrapper.fmiSimulation2=" + _fmiSimulation2);
      EObjectAspect.devDebug(_self, _plus_5);
      Simulation2 _fmiSimulation2_1 = _self.getFmuSimulationWrapper().getFmiSimulation2();
      boolean _tripleEquals = (_fmiSimulation2_1 == null);
      if (_tripleEquals) {
        String _name_1 = _self.getName();
        String _plus_6 = ("Failed to initialize fmu " + _name_1);
        String _plus_7 = (_plus_6 + " with path=");
        String _runnableFmuPath_2 = _self.getRunnableFmuPath();
        String _plus_8 = (_plus_7 + _runnableFmuPath_2);
        throw new FCLException(_plus_8);
      } else {
        FMUFunctionAspect.checkPortconsistency(_self);
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static void _privk3_checkPortconsistency(final FMUFunctionAspectFMUFunctionAspectProperties _self_, final FMUFunction _self) {
    String _name = _self.getName();
    String _plus = ("-> checkPortconsistency() NOT IMPLEMENTED YET " + _name);
    EObjectAspect.devWarn(_self, _plus);
    EObjectAspect.devInfo(_self, FMUFunctionAspect.getKnownInputVariables(_self));
    EObjectAspect.devInfo(_self, FMUFunctionAspect.getKnownOutputVariables(_self));
  }
  
  protected static void _privk3_setFMUVarFromInputPorts(final FMUFunctionAspectFMUFunctionAspectProperties _self_, final FMUFunction _self) {
    String _name = _self.getName();
    String _plus = ("-> setFMUVarFromInputPorts() " + _name);
    EObjectAspect.devInfo(_self, _plus);
    final Function1<FMUFunctionBlockDataPort, Boolean> _function = (FMUFunctionBlockDataPort p) -> {
      return Boolean.valueOf((Objects.equal(p.getDirection(), DirectionKind.IN) || Objects.equal(p.getDirection(), DirectionKind.IN_OUT)));
    };
    final List<FMUFunctionBlockDataPort> fmuInputPorts = IterableExtensions.<FMUFunctionBlockDataPort>toList(IterableExtensions.<FMUFunctionBlockDataPort>filter(Iterables.<FMUFunctionBlockDataPort>filter(_self.getFunctionPorts(), FMUFunctionBlockDataPort.class), _function));
    for (final FMUFunctionBlockDataPort inPort : fmuInputPorts) {
      try {
        FMUFunctionBlockDataPortAspect.setSimulation2RuntimeVariableFromPortValue(inPort);
        String _name_1 = _self.getName();
        String _plus_1 = (_name_1 + ".");
        String _name_2 = inPort.getName();
        String _plus_2 = (_plus_1 + _name_2);
        String _plus_3 = (_plus_2 + " = ");
        double _float = FunctionBlockDataPortAspect.getFloat(inPort);
        String _plus_4 = (_plus_3 + Double.valueOf(_float));
        EObjectAspect.devDebug(_self, _plus_4);
      } catch (final Throwable _t) {
        if (_t instanceof InvalidData) {
          String _fMUVarName = FMUFunctionBlockDataPortAspect.getFMUVarName(inPort);
          String _plus_5 = ("ignoring set of fmu variable " + _fMUVarName);
          String _plus_6 = (_plus_5 + " from ");
          String _name_3 = _self.getName();
          String _plus_7 = (_plus_6 + _name_3);
          String _plus_8 = (_plus_7 + ".");
          String _name_4 = inPort.getName();
          String _plus_9 = (_plus_8 + _name_4);
          String _plus_10 = (_plus_9 + " (no known value yet, did you forget to set a default value ?)");
          EObjectAspect.warn(_self, _plus_10);
        } else {
          throw Exceptions.sneakyThrow(_t);
        }
      }
    }
  }
  
  protected static void _privk3_evalFunction(final FMUFunctionAspectFMUFunctionAspectProperties _self_, final FMUFunction _self) {
    String _pushIndent = FCLModelAspect.pushIndent(EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class));
    String _plus = (_pushIndent + 
      "-> evalFunction() ");
    String _qualifiedName = EObjectAspect.getQualifiedName(_self);
    String _plus_1 = (_plus + _qualifiedName);
    EObjectAspect.devDebug(_self, _plus_1);
    FunctionTimeReference _timeReference = _self.getTimeReference();
    boolean _tripleNotEquals = (_timeReference != null);
    if (_tripleNotEquals) {
      String _indentation = EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getIndentation();
      String _plus_2 = (_indentation + 
        "Increment time reference for function ");
      String _qualifiedName_1 = EObjectAspect.getQualifiedName(_self);
      String _plus_3 = (_plus_2 + _qualifiedName_1);
      EObjectAspect.devInfo(_self, _plus_3);
      final FunctionVarDecl timeVariable = _self.getTimeReference().getObservableVar();
      FunctionVarDeclAspect.assign(timeVariable, 
        ValueAspect.plus(timeVariable.getCurrentValue(), 
          ExpressionAspect.evaluate(_self.getTimeReference().getIncrement())));
    }
    FMUFunctionAspect.setFMUVarFromInputPorts(_self);
    EObjectAspect.devInfo(_self, FMUFunctionAspect.getKnownInputVariables(_self));
    EObjectAspect.devInfo(_self, FMUFunctionAspect.getKnownOutputVariables(_self));
    double _floatValue = ValueAspect.toFloatValue(EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getDataflow().getTimeReference().getObservableVar().getCurrentValue()).getFloatValue();
    double _floatValue_1 = ValueAspect.toFloatValue(FunctionVarDeclAspect.previousValue(EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getDataflow().getTimeReference().getObservableVar())).getFloatValue();
    final double stepSize = (_floatValue - _floatValue_1);
    String _indentation_1 = EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getIndentation();
    String _plus_4 = (_indentation_1 + 
      "doing doStep(stepSize=");
    String _plus_5 = (_plus_4 + Double.valueOf(stepSize));
    String _plus_6 = (_plus_5 + ")...");
    EObjectAspect.devDebug(_self, _plus_6);
    String _indentation_2 = EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getIndentation();
    String _plus_7 = (_indentation_2 + 
      "stepSize=");
    String _plus_8 = (_plus_7 + Double.valueOf(stepSize));
    EObjectAspect.devDebug(_self, _plus_8);
    FMUFunctionAspect.doStep(_self, stepSize);
    String _indentation_3 = EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getIndentation();
    String _plus_9 = (_indentation_3 + 
      "done. FMU internal time=");
    double _simulationCurrentTime = FMUFunctionAspect.getSimulationCurrentTime(_self);
    String _plus_10 = (_plus_9 + Double.valueOf(_simulationCurrentTime));
    EObjectAspect.devInfo(_self, _plus_10);
    EObjectAspect.devInfo(_self, FMUFunctionAspect.getKnownInputVariables(_self));
    EObjectAspect.devInfo(_self, FMUFunctionAspect.getKnownOutputVariables(_self));
    FMUFunctionAspect.loadOutputPortsFromFMU(_self);
    FunctionAspect.sendDataToActiveNonDelayedPorts(_self);
    FCLModelAspect.popIndent(EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class));
  }
  
  protected static void _privk3_loadOutputPortsFromFMU(final FMUFunctionAspectFMUFunctionAspectProperties _self_, final FMUFunction _self) {
    String _name = _self.getName();
    String _plus = ("-> loadPortFromFMU() " + _name);
    EObjectAspect.devInfo(_self, _plus);
    final Function1<FMUFunctionBlockDataPort, Boolean> _function = (FMUFunctionBlockDataPort p) -> {
      return Boolean.valueOf((Objects.equal(p.getDirection(), DirectionKind.OUT) || Objects.equal(p.getDirection(), DirectionKind.IN_OUT)));
    };
    final List<FMUFunctionBlockDataPort> fmuOutputPorts = IterableExtensions.<FMUFunctionBlockDataPort>toList(IterableExtensions.<FMUFunctionBlockDataPort>filter(Iterables.<FMUFunctionBlockDataPort>filter(_self.getFunctionPorts(), FMUFunctionBlockDataPort.class), _function));
    for (final FMUFunctionBlockDataPort outPort : fmuOutputPorts) {
      {
        FunctionBlockDataPortAspect.setFloat(outPort, FMUFunctionBlockDataPortAspect.getPortValueFromSimulation2RuntimeVariable(outPort));
        String _name_1 = _self.getName();
        String _plus_1 = (_name_1 + ".");
        String _name_2 = outPort.getName();
        String _plus_2 = (_plus_1 + _name_2);
        String _plus_3 = (_plus_2 + " = ");
        double _float = FunctionBlockDataPortAspect.getFloat(outPort);
        String _plus_4 = (_plus_3 + Double.valueOf(_float));
        EObjectAspect.devDebug(_self, _plus_4);
      }
    }
  }
  
  protected static void _privk3_doStep(final FMUFunctionAspectFMUFunctionAspectProperties _self_, final FMUFunction _self, final double stepSize) {
    final FMUSimulationWrapper wrapper = _self.getFmuSimulationWrapper();
    final Simulation2 javaFMI_FMU = wrapper.getFmiSimulation2();
    final Status status = javaFMI_FMU.doStep(stepSize);
    boolean _notEquals = (!Objects.equal(status, Status.OK));
    if (_notEquals) {
      System.err.println(("Failed to doStep " + _self));
    }
  }
  
  protected static double _privk3_getSimulationCurrentTime(final FMUFunctionAspectFMUFunctionAspectProperties _self_, final FMUFunction _self) {
    final Simulation2 javaFMI_FMU = _self.getFmuSimulationWrapper().getFmiSimulation2();
    return javaFMI_FMU.getCurrentTime();
  }
  
  protected static String _privk3_getKnownOutputVariables(final FMUFunctionAspectFMUFunctionAspectProperties _self_, final FMUFunction _self) {
    final StringBuilder sb = new StringBuilder();
    final Simulation2 javaFMI_FMU = _self.getFmuSimulationWrapper().getFmiSimulation2();
    final Access access = new Access(javaFMI_FMU);
    ScalarVariable[] _modelVariables = access.getModelVariables();
    for (final org.javafmi.modeldescription.ScalarVariable variable : _modelVariables) {
      {
        final String causality = variable.getCausality();
        final String varName = variable.getName();
        boolean _equals = causality.equals("output");
        if (_equals) {
          double _asDouble = javaFMI_FMU.read(varName).asDouble();
          String _plus = ((("FMU output: " + varName) + " = ") + Double.valueOf(_asDouble));
          String _plus_1 = (_plus + " //");
          Variable _readVariable = javaFMI_FMU.readVariable(varName);
          String _plus_2 = (_plus_1 + _readVariable);
          String _plus_3 = (_plus_2 + " ");
          SimpleType _type = variable.getType();
          String _plus_4 = (_plus_3 + _type);
          String _plus_5 = (_plus_4 + "\n");
          sb.append(_plus_5);
        }
      }
    }
    return sb.toString();
  }
  
  protected static String _privk3_getKnownInputVariables(final FMUFunctionAspectFMUFunctionAspectProperties _self_, final FMUFunction _self) {
    final StringBuilder sb = new StringBuilder();
    final Simulation2 javaFMI_FMU = _self.getFmuSimulationWrapper().getFmiSimulation2();
    final Access access = new Access(javaFMI_FMU);
    ScalarVariable[] _modelVariables = access.getModelVariables();
    for (final org.javafmi.modeldescription.ScalarVariable variable : _modelVariables) {
      {
        final String causality = variable.getCausality();
        final String varName = variable.getName();
        boolean _equals = causality.equals("input");
        if (_equals) {
          double _asDouble = javaFMI_FMU.read(varName).asDouble();
          String _plus = ((("FMU input: " + varName) + " = ") + Double.valueOf(_asDouble));
          String _plus_1 = (_plus + " //");
          Variable _readVariable = javaFMI_FMU.readVariable(varName);
          String _plus_2 = (_plus_1 + _readVariable);
          String _plus_3 = (_plus_2 + " ");
          SimpleType _type = variable.getType();
          String _plus_4 = (_plus_3 + _type);
          String _plus_5 = (_plus_4 + "\n");
          sb.append(_plus_5);
        }
      }
    }
    return sb.toString();
  }
  
  protected static FMUSimulationWrapper _privk3_fmuSimulationWrapper(final FMUFunctionAspectFMUFunctionAspectProperties _self_, final FMUFunction _self) {
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("getFmuSimulationWrapper") &&
    			m.getParameterTypes().length == 0) {
    				Object ret = m.invoke(_self);
    				if (ret != null) {
    					return (fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper) ret;
    				} else {
    					return null;
    				}
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    return _self_.fmuSimulationWrapper;
  }
  
  protected static void _privk3_fmuSimulationWrapper(final FMUFunctionAspectFMUFunctionAspectProperties _self_, final FMUFunction _self, final FMUSimulationWrapper fmuSimulationWrapper) {
    boolean setterCalled = false;
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("setFmuSimulationWrapper")
    				&& m.getParameterTypes().length == 1) {
    			m.invoke(_self, fmuSimulationWrapper);
    			setterCalled = true;
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    if (!setterCalled) {
    	_self_.fmuSimulationWrapper = fmuSimulationWrapper;
    }
  }
}
