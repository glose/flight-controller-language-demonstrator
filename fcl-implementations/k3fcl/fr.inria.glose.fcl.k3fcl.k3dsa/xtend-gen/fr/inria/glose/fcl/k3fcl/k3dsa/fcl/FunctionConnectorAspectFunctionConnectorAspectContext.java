package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionConnectorAspectFunctionConnectorAspectProperties;
import fr.inria.glose.fcl.model.fcl.FunctionConnector;
import java.util.Map;

@SuppressWarnings("all")
public class FunctionConnectorAspectFunctionConnectorAspectContext {
  public final static FunctionConnectorAspectFunctionConnectorAspectContext INSTANCE = new FunctionConnectorAspectFunctionConnectorAspectContext();
  
  public static FunctionConnectorAspectFunctionConnectorAspectProperties getSelf(final FunctionConnector _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionConnectorAspectFunctionConnectorAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<FunctionConnector, FunctionConnectorAspectFunctionConnectorAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.FunctionConnector, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionConnectorAspectFunctionConnectorAspectProperties>();
  
  public Map<FunctionConnector, FunctionConnectorAspectFunctionConnectorAspectProperties> getMap() {
    return map;
  }
}
