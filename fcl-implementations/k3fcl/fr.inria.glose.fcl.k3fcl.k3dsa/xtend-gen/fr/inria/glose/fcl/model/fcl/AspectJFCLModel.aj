// AspectJ classes that have been aspectized and name
package fr.inria.glose.fcl.model.fcl;
public aspect AspectJFCLModel{
void around (fr.inria.glose.fcl.model.fcl.FCLModel self)  :target (self) && (call ( void fr.inria.glose.fcl.model.fcl.FCLModel.evaluateModeAutomata(  ) ) ) { fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect.evaluateModeAutomata(self );}
void around (fr.inria.glose.fcl.model.fcl.FCLModel self)  :target (self) && (call ( void fr.inria.glose.fcl.model.fcl.FCLModel.propagateDelayedResults(  ) ) ) { fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect.propagateDelayedResults(self );}

}
