package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspectDataStructPropertyAssignmentAspectProperties;
import fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment;
import java.util.Map;

@SuppressWarnings("all")
public class DataStructPropertyAssignmentAspectDataStructPropertyAssignmentAspectContext {
  public final static DataStructPropertyAssignmentAspectDataStructPropertyAssignmentAspectContext INSTANCE = new DataStructPropertyAssignmentAspectDataStructPropertyAssignmentAspectContext();
  
  public static DataStructPropertyAssignmentAspectDataStructPropertyAssignmentAspectProperties getSelf(final DataStructPropertyAssignment _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspectDataStructPropertyAssignmentAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<DataStructPropertyAssignment, DataStructPropertyAssignmentAspectDataStructPropertyAssignmentAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspectDataStructPropertyAssignmentAspectProperties>();
  
  public Map<DataStructPropertyAssignment, DataStructPropertyAssignmentAspectDataStructPropertyAssignmentAspectProperties> getMap() {
    return map;
  }
}
