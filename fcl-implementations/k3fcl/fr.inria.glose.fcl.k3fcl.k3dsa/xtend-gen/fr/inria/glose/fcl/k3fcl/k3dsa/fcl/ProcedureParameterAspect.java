package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import com.google.common.base.Objects;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspectProcedureParameterAspectProperties;
import fr.inria.glose.fcl.model.fcl.CallableDeclaration;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.ProcedureParameter;
import fr.inria.glose.fcl.model.fcl.Value;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext;
import org.eclipse.gemoc.commons.eclipse.emf.EObjectUtil;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@Aspect(className = ProcedureParameter.class, with = { CallableDeclarationAspect.class })
@SuppressWarnings("all")
public class ProcedureParameterAspect extends AssignableAspect {
  /**
   * BE CAREFUL :
   * 
   * This class has more than one superclass
   * please specify which parent you want with the 'super' expected calling
   */
  public static Value call(final ProcedureParameter _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspectProcedureParameterAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspectProcedureParameterAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value call()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.ProcedureParameter){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect._privk3_call(_self_, (fr.inria.glose.fcl.model.fcl.ProcedureParameter)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static DataType getResolvedType(final ProcedureParameter _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspectProcedureParameterAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspectProcedureParameterAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# DataType getResolvedType()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.ProcedureParameter){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect._privk3_getResolvedType(_self_, (fr.inria.glose.fcl.model.fcl.ProcedureParameter)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.DataType)result;
  }
  
  public static String getSimpleName(final ProcedureParameter _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspectProcedureParameterAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspectProcedureParameterAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# String getSimpleName()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.ProcedureParameter){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect._privk3_getSimpleName(_self_, (fr.inria.glose.fcl.model.fcl.ProcedureParameter)_self);
    };
    return (java.lang.String)result;
  }
  
  protected static Value _privk3_call(final ProcedureParameterAspectProcedureParameterAspectProperties _self_, final ProcedureParameter _self) {
    final ProcedureContext context = IterableExtensions.<ProcedureContext>last(EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getProcedureStack());
    final Function1<DeclarationMapEntry, Boolean> _function = (DeclarationMapEntry me) -> {
      CallableDeclaration _callableDeclaration = me.getCallableDeclaration();
      return Boolean.valueOf(Objects.equal(_callableDeclaration, _self));
    };
    final DeclarationMapEntry mapEntry = IterableExtensions.<DeclarationMapEntry>findFirst(context.getDeclarationMapEntries(), _function);
    if ((mapEntry != null)) {
      FCLModelAspect.popIndent(EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class));
      return mapEntry.getValue();
    } else {
      FCLModelAspect.popIndent(EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class));
      return null;
    }
  }
  
  protected static DataType _privk3_getResolvedType(final ProcedureParameterAspectProcedureParameterAspectProperties _self_, final ProcedureParameter _self) {
    return _self.getType();
  }
  
  protected static String _privk3_getSimpleName(final ProcedureParameterAspectProcedureParameterAspectProperties _self_, final ProcedureParameter _self) {
    String _name = ProcedureParameterAspect.getResolvedType(_self).getName();
    String _plus = ("" + _name);
    String _plus_1 = (_plus + " ");
    String _name_1 = _self.getName();
    return (_plus_1 + _name_1);
  }
  
  public static Value callPrevious(final ProcedureParameter _self) {
    return CallableDeclarationAspect.callPrevious(_self);
  }
}
