package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorBinaryExpressionAspectBasicOperatorBinaryExpressionAspectProperties;
import fr.inria.glose.fcl.model.fcl.BasicOperatorBinaryExpression;
import java.util.Map;

@SuppressWarnings("all")
public class BasicOperatorBinaryExpressionAspectBasicOperatorBinaryExpressionAspectContext {
  public final static BasicOperatorBinaryExpressionAspectBasicOperatorBinaryExpressionAspectContext INSTANCE = new BasicOperatorBinaryExpressionAspectBasicOperatorBinaryExpressionAspectContext();
  
  public static BasicOperatorBinaryExpressionAspectBasicOperatorBinaryExpressionAspectProperties getSelf(final BasicOperatorBinaryExpression _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorBinaryExpressionAspectBasicOperatorBinaryExpressionAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<BasicOperatorBinaryExpression, BasicOperatorBinaryExpressionAspectBasicOperatorBinaryExpressionAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.BasicOperatorBinaryExpression, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorBinaryExpressionAspectBasicOperatorBinaryExpressionAspectProperties>();
  
  public Map<BasicOperatorBinaryExpression, BasicOperatorBinaryExpressionAspectBasicOperatorBinaryExpressionAspectProperties> getMap() {
    return map;
  }
}
