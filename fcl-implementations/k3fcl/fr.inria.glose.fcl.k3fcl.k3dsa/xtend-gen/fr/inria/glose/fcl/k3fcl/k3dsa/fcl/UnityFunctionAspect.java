package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.diverse.k3.al.annotationprocessor.Step;
import fr.inria.glose.fcl.k3fcl.k3dsa.Activator;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.InvalidData;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspectUnityFunctionAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionDataPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.DirectionKind;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FunctionTimeReference;
import fr.inria.glose.fcl.model.fcl.FunctionVarDecl;
import fr.inria.glose.fcl.model.fcl.UnityFunction;
import fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmFactory;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper;
import fr.inria.glose.fcl.model.unity.UnityInstance;
import java.util.List;
import org.eclipse.core.resources.IFile;
import org.eclipse.gemoc.commons.eclipse.core.resources.IFileUtils;
import org.eclipse.gemoc.commons.eclipse.emf.EObjectUtil;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@Aspect(className = UnityFunction.class)
@SuppressWarnings("all")
public class UnityFunctionAspect extends FunctionAspect {
  public static void initializeUnity(final UnityFunction _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspectUnityFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspectUnityFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void initializeUnity()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.UnityFunction){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspect._privk3_initializeUnity(_self_, (fr.inria.glose.fcl.model.fcl.UnityFunction)_self);
    };
  }
  
  @Step
  public static void evalFunction(final UnityFunction _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspectUnityFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspectUnityFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void evalFunction()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.UnityFunction){
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand command = new fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand() {
    		@Override
    		public void execute() {
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspect._privk3_evalFunction(_self_, (fr.inria.glose.fcl.model.fcl.UnityFunction)_self);
    		}
    	};
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager stepManager = fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepManagerRegistry.getInstance().findStepManager(_self);
    	if (stepManager != null) {
    		stepManager.executeStep(_self, new Object[] {_self}, command, "UnityFunction", "evalFunction");
    	} else {
    		command.execute();
    	}
    	;
    };
  }
  
  public static void setUnityVarFromInputPorts(final UnityFunction _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspectUnityFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspectUnityFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void setUnityVarFromInputPorts()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.UnityFunction){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspect._privk3_setUnityVarFromInputPorts(_self_, (fr.inria.glose.fcl.model.fcl.UnityFunction)_self);
    };
  }
  
  public static UnityWrapper unityWrapper(final UnityFunction _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspectUnityFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspectUnityFunctionAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# UnityWrapper unityWrapper()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.UnityFunction){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspect._privk3_unityWrapper(_self_, (fr.inria.glose.fcl.model.fcl.UnityFunction)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper)result;
  }
  
  public static void unityWrapper(final UnityFunction _self, final UnityWrapper unityWrapper) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspectUnityFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspectUnityFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void unityWrapper(UnityWrapper)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.UnityFunction){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspect._privk3_unityWrapper(_self_, (fr.inria.glose.fcl.model.fcl.UnityFunction)_self,unityWrapper);
    };
  }
  
  protected static void _privk3_initializeUnity(final UnityFunctionAspectUnityFunctionAspectProperties _self_, final UnityFunction _self) {
    String _name = _self.getName();
    String _plus = ("-> initializeUnity() " + _name);
    String _plus_1 = (_plus + " with path=");
    String _unityWebPagePath = _self.getUnityWebPagePath();
    String _plus_2 = (_plus_1 + _unityWebPagePath);
    EObjectAspect.devInfo(_self, _plus_2);
    _self.setUnityWrapper(Interpreter_vmFactory.eINSTANCE.createUnityWrapper());
    try {
      final IFile iFile = IFileUtils.getIFileFromWorkspaceOrFileSystem(_self.getUnityWebPagePath());
      final String unityFilePath = iFile.getLocation().toString();
      String _name_1 = _self.getName();
      final UnityInstance unityInstance = new UnityInstance(_name_1, unityFilePath);
      UnityWrapper _unityWrapper = _self.getUnityWrapper();
      _unityWrapper.setUnityInstance(unityInstance);
    } catch (final Throwable _t) {
      if (_t instanceof Exception) {
        final Exception e = (Exception)_t;
        String _unityWebPagePath_1 = _self.getUnityWebPagePath();
        String _plus_3 = ("Failed to initialize Unity " + _unityWebPagePath_1);
        Activator.error(_plus_3, e);
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
  
  protected static void _privk3_evalFunction(final UnityFunctionAspectUnityFunctionAspectProperties _self_, final UnityFunction _self) {
    String _pushIndent = FCLModelAspect.pushIndent(EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class));
    String _plus = (_pushIndent + 
      "-> evalFunction() ");
    String _qualifiedName = EObjectAspect.getQualifiedName(_self);
    String _plus_1 = (_plus + _qualifiedName);
    EObjectAspect.devDebug(_self, _plus_1);
    FunctionTimeReference _timeReference = _self.getTimeReference();
    boolean _tripleNotEquals = (_timeReference != null);
    if (_tripleNotEquals) {
      String _indentation = EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getIndentation();
      String _plus_2 = (_indentation + 
        "Increment time reference for function ");
      String _qualifiedName_1 = EObjectAspect.getQualifiedName(_self);
      String _plus_3 = (_plus_2 + _qualifiedName_1);
      EObjectAspect.devInfo(_self, _plus_3);
      final FunctionVarDecl timeVariable = _self.getTimeReference().getObservableVar();
      FunctionVarDeclAspect.assign(timeVariable, 
        ValueAspect.plus(timeVariable.getCurrentValue(), 
          ExpressionAspect.evaluate(_self.getTimeReference().getIncrement())));
    }
    UnityFunctionAspect.setUnityVarFromInputPorts(_self);
    FunctionAspect.sendDataToActiveNonDelayedPorts(_self);
    FCLModelAspect.popIndent(EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class));
  }
  
  protected static void _privk3_setUnityVarFromInputPorts(final UnityFunctionAspectUnityFunctionAspectProperties _self_, final UnityFunction _self) {
    String _name = _self.getName();
    String _plus = ("-> setFMUVarFromInputPorts() " + _name);
    EObjectAspect.devInfo(_self, _plus);
    final Function1<UnityFunctionDataPort, Boolean> _function = (UnityFunctionDataPort p) -> {
      return Boolean.valueOf((Objects.equal(p.getDirection(), DirectionKind.IN) || Objects.equal(p.getDirection(), DirectionKind.IN_OUT)));
    };
    final List<UnityFunctionDataPort> unityInputPorts = IterableExtensions.<UnityFunctionDataPort>toList(IterableExtensions.<UnityFunctionDataPort>filter(Iterables.<UnityFunctionDataPort>filter(_self.getFunctionPorts(), UnityFunctionDataPort.class), _function));
    for (final UnityFunctionDataPort inPort : unityInputPorts) {
      try {
        UnityFunctionDataPortAspect.setUnityVariableFromPortValue(inPort);
        String _name_1 = _self.getName();
        String _plus_1 = (_name_1 + ".");
        String _name_2 = inPort.getName();
        String _plus_2 = (_plus_1 + _name_2);
        String _plus_3 = (_plus_2 + " = ");
        double _float = FunctionBlockDataPortAspect.getFloat(inPort);
        String _plus_4 = (_plus_3 + Double.valueOf(_float));
        EObjectAspect.devDebug(_self, _plus_4);
      } catch (final Throwable _t) {
        if (_t instanceof InvalidData) {
          String _unityNodeName = inPort.getUnityNodeName();
          String _plus_5 = ("ignoring set of unity variable " + _unityNodeName);
          String _plus_6 = (_plus_5 + ".");
          String _unityVariableName = inPort.getUnityVariableName();
          String _plus_7 = (_plus_6 + _unityVariableName);
          String _plus_8 = (_plus_7 + " from ");
          String _name_3 = _self.getName();
          String _plus_9 = (_plus_8 + _name_3);
          String _plus_10 = (_plus_9 + ".");
          String _name_4 = inPort.getName();
          String _plus_11 = (_plus_10 + _name_4);
          String _plus_12 = (_plus_11 + " (no known value yet, did you forget to set a default value ?)");
          EObjectAspect.warn(_self, _plus_12);
        } else {
          throw Exceptions.sneakyThrow(_t);
        }
      }
    }
  }
  
  protected static UnityWrapper _privk3_unityWrapper(final UnityFunctionAspectUnityFunctionAspectProperties _self_, final UnityFunction _self) {
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("getUnityWrapper") &&
    			m.getParameterTypes().length == 0) {
    				Object ret = m.invoke(_self);
    				if (ret != null) {
    					return (fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper) ret;
    				} else {
    					return null;
    				}
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    return _self_.unityWrapper;
  }
  
  protected static void _privk3_unityWrapper(final UnityFunctionAspectUnityFunctionAspectProperties _self_, final UnityFunction _self, final UnityWrapper unityWrapper) {
    boolean setterCalled = false;
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("setUnityWrapper")
    				&& m.getParameterTypes().length == 1) {
    			m.invoke(_self, unityWrapper);
    			setterCalled = true;
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    if (!setterCalled) {
    	_self_.unityWrapper = unityWrapper;
    }
  }
}
