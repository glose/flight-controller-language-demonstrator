package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallPreviousAspectCallPreviousAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect;
import fr.inria.glose.fcl.model.fcl.CallPrevious;
import fr.inria.glose.fcl.model.fcl.Value;

@Aspect(className = CallPrevious.class)
@SuppressWarnings("all")
public class CallPreviousAspect extends CallAspect {
  public static Value evaluate(final CallPrevious _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallPreviousAspectCallPreviousAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallPreviousAspectCallPreviousAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.CallPrevious){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallPreviousAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.CallPrevious)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static Value _privk3_evaluate(final CallPreviousAspectCallPreviousAspectProperties _self_, final CallPrevious _self) {
    return CallableDeclarationAspect.callPrevious(_self.getCallable());
  }
}
