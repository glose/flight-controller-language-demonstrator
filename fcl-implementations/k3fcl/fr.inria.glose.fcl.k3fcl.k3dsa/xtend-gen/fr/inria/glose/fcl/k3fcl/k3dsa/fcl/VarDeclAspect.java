package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import com.google.common.base.Objects;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.PrimitiveActionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspectVarDeclAspectProperties;
import fr.inria.glose.fcl.model.fcl.CallableDeclaration;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.Expression;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.Value;
import fr.inria.glose.fcl.model.fcl.VarDecl;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmFactory;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext;
import org.eclipse.emf.common.util.EList;
import org.eclipse.gemoc.commons.eclipse.emf.EObjectUtil;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@Aspect(className = VarDecl.class, with = { AssignableAspect.class, CallableDeclarationAspect.class })
@SuppressWarnings("all")
public class VarDeclAspect extends PrimitiveActionAspect {
  /**
   * BE CAREFUL :
   * 
   * This class has more than one superclass
   * please specify which parent you want with the 'super' expected calling
   */
  public static void doAction(final VarDecl _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspectVarDeclAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspectVarDeclAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void doAction()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.VarDecl){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect._privk3_doAction(_self_, (fr.inria.glose.fcl.model.fcl.VarDecl)_self);
    };
  }
  
  public static Value call(final VarDecl _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspectVarDeclAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspectVarDeclAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value call()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.VarDecl){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect._privk3_call(_self_, (fr.inria.glose.fcl.model.fcl.VarDecl)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static void assign(final VarDecl _self, final Value newValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspectVarDeclAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspectVarDeclAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void assign(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.VarDecl){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect._privk3_assign(_self_, (fr.inria.glose.fcl.model.fcl.VarDecl)_self,newValue);
    };
  }
  
  public static DataType getResolvedType(final VarDecl _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspectVarDeclAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspectVarDeclAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# DataType getResolvedType()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.VarDecl){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect._privk3_getResolvedType(_self_, (fr.inria.glose.fcl.model.fcl.VarDecl)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.DataType)result;
  }
  
  protected static void _privk3_doAction(final VarDeclAspectVarDeclAspectProperties _self_, final VarDecl _self) {
    final ProcedureContext procedureContext = IterableExtensions.<ProcedureContext>last(EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getProcedureStack());
    final DeclarationMapEntry mapEntry = Interpreter_vmFactory.eINSTANCE.createDeclarationMapEntry();
    mapEntry.setCallableDeclaration(_self);
    Expression _initialValue = _self.getInitialValue();
    boolean _tripleNotEquals = (_initialValue != null);
    if (_tripleNotEquals) {
      final Value iValue = ExpressionAspect.evaluate(_self.getInitialValue());
      mapEntry.setValue(ValueAspect.copy(iValue));
    }
    EList<DeclarationMapEntry> _declarationMapEntries = procedureContext.getDeclarationMapEntries();
    _declarationMapEntries.add(mapEntry);
  }
  
  protected static Value _privk3_call(final VarDeclAspectVarDeclAspectProperties _self_, final VarDecl _self) {
    final ProcedureContext context = IterableExtensions.<ProcedureContext>last(EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getProcedureStack());
    final Function1<DeclarationMapEntry, Boolean> _function = (DeclarationMapEntry me) -> {
      CallableDeclaration _callableDeclaration = me.getCallableDeclaration();
      return Boolean.valueOf(Objects.equal(_callableDeclaration, _self));
    };
    final DeclarationMapEntry mapEntry = IterableExtensions.<DeclarationMapEntry>findFirst(context.getDeclarationMapEntries(), _function);
    if ((mapEntry != null)) {
      return mapEntry.getValue();
    } else {
      return null;
    }
  }
  
  protected static void _privk3_assign(final VarDeclAspectVarDeclAspectProperties _self_, final VarDecl _self, final Value newValue) {
    final ProcedureContext context = IterableExtensions.<ProcedureContext>last(EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getProcedureStack());
    final Function1<DeclarationMapEntry, Boolean> _function = (DeclarationMapEntry me) -> {
      CallableDeclaration _callableDeclaration = me.getCallableDeclaration();
      return Boolean.valueOf(Objects.equal(_callableDeclaration, _self));
    };
    final DeclarationMapEntry mapEntry = IterableExtensions.<DeclarationMapEntry>findFirst(context.getDeclarationMapEntries(), _function);
    if ((mapEntry != null)) {
      mapEntry.setValue(ValueAspect.copy(newValue));
    } else {
      final DeclarationMapEntry newmapEntry = Interpreter_vmFactory.eINSTANCE.createDeclarationMapEntry();
      newmapEntry.setCallableDeclaration(_self);
      newmapEntry.setValue(ValueAspect.copy(newValue));
      EList<DeclarationMapEntry> _declarationMapEntries = context.getDeclarationMapEntries();
      _declarationMapEntries.add(newmapEntry);
    }
  }
  
  protected static DataType _privk3_getResolvedType(final VarDeclAspectVarDeclAspectProperties _self_, final VarDecl _self) {
    return _self.getType();
  }
  
  public static Value callPrevious(final VarDecl _self) {
    return CallableDeclarationAspect.callPrevious(_self);
  }
  
  public static String getSimpleName(final VarDecl _self) {
    return CallableDeclarationAspect.getSimpleName(_self);
  }
}
