package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnaryExpressionAspect;
import fr.inria.glose.fcl.model.fcl.Cast;

@Aspect(className = Cast.class)
@SuppressWarnings("all")
public class CastAspect extends UnaryExpressionAspect {
}
