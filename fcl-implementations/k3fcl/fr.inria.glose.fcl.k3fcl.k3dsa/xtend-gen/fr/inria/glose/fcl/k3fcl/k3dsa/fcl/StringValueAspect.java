package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import com.google.common.base.Objects;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspectStringValueAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.BooleanValue;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.StringValue;
import fr.inria.glose.fcl.model.fcl.StringValueType;
import fr.inria.glose.fcl.model.fcl.Value;

@Aspect(className = StringValue.class)
@SuppressWarnings("all")
public class StringValueAspect extends ValueAspect {
  public static Value evaluate(final StringValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspectStringValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspectStringValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.StringValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.StringValue)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value copy(final StringValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspectStringValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspectStringValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value copy()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.StringValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect._privk3_copy(_self_, (fr.inria.glose.fcl.model.fcl.StringValue)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static BooleanValue bEquals(final StringValue _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspectStringValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspectStringValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# BooleanValue bEquals(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.StringValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect._privk3_bEquals(_self_, (fr.inria.glose.fcl.model.fcl.StringValue)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.BooleanValue)result;
  }
  
  public static String valueToString(final StringValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspectStringValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspectStringValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# String valueToString()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.StringValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect._privk3_valueToString(_self_, (fr.inria.glose.fcl.model.fcl.StringValue)_self);
    };
    return (java.lang.String)result;
  }
  
  public static Object rawValue(final StringValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspectStringValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspectStringValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Object rawValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.StringValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect._privk3_rawValue(_self_, (fr.inria.glose.fcl.model.fcl.StringValue)_self);
    };
    return (java.lang.Object)result;
  }
  
  public static Boolean isKindOf(final StringValue _self, final DataType type) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspectStringValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspectStringValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Boolean isKindOf(DataType)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.StringValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect._privk3_isKindOf(_self_, (fr.inria.glose.fcl.model.fcl.StringValue)_self,type);
    };
    return (java.lang.Boolean)result;
  }
  
  protected static Value _privk3_evaluate(final StringValueAspectStringValueAspectProperties _self_, final StringValue _self) {
    return _self;
  }
  
  protected static Value _privk3_copy(final StringValueAspectStringValueAspectProperties _self_, final StringValue _self) {
    final StringValue aValue = FclFactory.eINSTANCE.createStringValue();
    aValue.setStringValue(_self.getStringValue());
    return aValue;
  }
  
  protected static BooleanValue _privk3_bEquals(final StringValueAspectStringValueAspectProperties _self_, final StringValue _self, final Value rhs) {
    final BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
    if ((rhs instanceof StringValue)) {
      String _stringValue = _self.getStringValue();
      String _stringValue_1 = ((StringValue) rhs).getStringValue();
      boolean _equals = Objects.equal(_stringValue, _stringValue_1);
      bValue.setBooleanValue(_equals);
    } else {
      bValue.setBooleanValue(false);
    }
    return bValue;
  }
  
  protected static String _privk3_valueToString(final StringValueAspectStringValueAspectProperties _self_, final StringValue _self) {
    return _self.getStringValue().toString();
  }
  
  protected static Object _privk3_rawValue(final StringValueAspectStringValueAspectProperties _self_, final StringValue _self) {
    return _self.getStringValue();
  }
  
  protected static Boolean _privk3_isKindOf(final StringValueAspectStringValueAspectProperties _self_, final StringValue _self, final DataType type) {
    return Boolean.valueOf((type instanceof StringValueType));
  }
}
