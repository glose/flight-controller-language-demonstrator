package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueTypeAspectIntegerValueTypeAspectProperties;
import fr.inria.glose.fcl.model.fcl.IntegerValueType;
import java.util.Map;

@SuppressWarnings("all")
public class IntegerValueTypeAspectIntegerValueTypeAspectContext {
  public final static IntegerValueTypeAspectIntegerValueTypeAspectContext INSTANCE = new IntegerValueTypeAspectIntegerValueTypeAspectContext();
  
  public static IntegerValueTypeAspectIntegerValueTypeAspectProperties getSelf(final IntegerValueType _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueTypeAspectIntegerValueTypeAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<IntegerValueType, IntegerValueTypeAspectIntegerValueTypeAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.IntegerValueType, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueTypeAspectIntegerValueTypeAspectProperties>();
  
  public Map<IntegerValueType, IntegerValueTypeAspectIntegerValueTypeAspectProperties> getMap() {
    return map;
  }
}
