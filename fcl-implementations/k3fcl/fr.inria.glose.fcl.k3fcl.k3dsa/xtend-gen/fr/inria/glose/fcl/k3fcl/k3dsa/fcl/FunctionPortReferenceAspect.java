package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspectFunctionPortReferenceAspectProperties;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.FunctionPortReference;

@Aspect(className = FunctionPortReference.class)
@SuppressWarnings("all")
public class FunctionPortReferenceAspect extends FunctionPortAspect {
  public static FunctionBlockDataPort getTargetDataPort(final FunctionPortReference _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspectFunctionPortReferenceAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspectFunctionPortReferenceAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# FunctionBlockDataPort getTargetDataPort()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionPortReference){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspect._privk3_getTargetDataPort(_self_, (fr.inria.glose.fcl.model.fcl.FunctionPortReference)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)result;
  }
  
  public static DataType getResolvedType(final FunctionPortReference _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspectFunctionPortReferenceAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspectFunctionPortReferenceAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# DataType getResolvedType()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionPortReference){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspect._privk3_getResolvedType(_self_, (fr.inria.glose.fcl.model.fcl.FunctionPortReference)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.DataType)result;
  }
  
  protected static FunctionBlockDataPort _privk3_getTargetDataPort(final FunctionPortReferenceAspectFunctionPortReferenceAspectProperties _self_, final FunctionPortReference _self) {
    return FunctionPortAspect.getTargetDataPort(_self.getFunctionPort());
  }
  
  protected static DataType _privk3_getResolvedType(final FunctionPortReferenceAspectFunctionPortReferenceAspectProperties _self_, final FunctionPortReference _self) {
    return FunctionBlockDataPortAspect.getResolvedType(FunctionPortReferenceAspect.getTargetDataPort(_self));
  }
}
