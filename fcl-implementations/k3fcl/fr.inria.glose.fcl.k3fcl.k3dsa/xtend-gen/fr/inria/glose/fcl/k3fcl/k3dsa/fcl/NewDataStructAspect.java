package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NewDataStructAspectNewDataStructAspectProperties;
import fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.NewDataStruct;
import fr.inria.glose.fcl.model.fcl.StructValue;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.emf.common.util.EList;

@Aspect(className = NewDataStruct.class)
@SuppressWarnings("all")
public class NewDataStructAspect extends ExpressionAspect {
  public static Value evaluate(final NewDataStruct _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NewDataStructAspectNewDataStructAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NewDataStructAspectNewDataStructAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.NewDataStruct){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NewDataStructAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.NewDataStruct)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static Value _privk3_evaluate(final NewDataStructAspectNewDataStructAspectProperties _self_, final NewDataStruct _self) {
    final StructValue sValue = FclFactory.eINSTANCE.createStructValue();
    EList<DataStructPropertyAssignment> _propertyAssignments = _self.getPropertyAssignments();
    for (final DataStructPropertyAssignment propAssign : _propertyAssignments) {
      DataStructPropertyAssignmentAspect.assignToStruct(propAssign, sValue);
    }
    return sValue;
  }
}
