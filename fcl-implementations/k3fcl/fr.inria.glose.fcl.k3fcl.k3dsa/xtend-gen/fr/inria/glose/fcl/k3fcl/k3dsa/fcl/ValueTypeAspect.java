package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspect;
import fr.inria.glose.fcl.model.fcl.ValueType;

@Aspect(className = ValueType.class)
@SuppressWarnings("all")
public class ValueTypeAspect extends DataTypeAspect {
}
