package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureReturnAspectFCLProcedureReturnAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.PrimitiveActionAspect;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FCLProcedureReturn;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext;
import org.eclipse.gemoc.commons.eclipse.emf.EObjectUtil;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@Aspect(className = FCLProcedureReturn.class)
@SuppressWarnings("all")
public class FCLProcedureReturnAspect extends PrimitiveActionAspect {
  public static void doAction(final FCLProcedureReturn _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureReturnAspectFCLProcedureReturnAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureReturnAspectFCLProcedureReturnAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void doAction()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLProcedureReturn){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureReturnAspect._privk3_doAction(_self_, (fr.inria.glose.fcl.model.fcl.FCLProcedureReturn)_self);
    };
  }
  
  protected static void _privk3_doAction(final FCLProcedureReturnAspectFCLProcedureReturnAspectProperties _self_, final FCLProcedureReturn _self) {
    final ProcedureContext context = IterableExtensions.<ProcedureContext>last(EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getProcedureStack());
    context.setResultValue(ExpressionAspect.evaluate(_self.getExpression()));
  }
}
