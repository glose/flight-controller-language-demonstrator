package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionTimeReferenceAspectFunctionTimeReferenceAspectProperties;
import fr.inria.glose.fcl.model.fcl.FunctionTimeReference;
import java.util.Map;

@SuppressWarnings("all")
public class FunctionTimeReferenceAspectFunctionTimeReferenceAspectContext {
  public final static FunctionTimeReferenceAspectFunctionTimeReferenceAspectContext INSTANCE = new FunctionTimeReferenceAspectFunctionTimeReferenceAspectContext();
  
  public static FunctionTimeReferenceAspectFunctionTimeReferenceAspectProperties getSelf(final FunctionTimeReference _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionTimeReferenceAspectFunctionTimeReferenceAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<FunctionTimeReference, FunctionTimeReferenceAspectFunctionTimeReferenceAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.FunctionTimeReference, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionTimeReferenceAspectFunctionTimeReferenceAspectProperties>();
  
  public Map<FunctionTimeReference, FunctionTimeReferenceAspectFunctionTimeReferenceAspectProperties> getMap() {
    return map;
  }
}
