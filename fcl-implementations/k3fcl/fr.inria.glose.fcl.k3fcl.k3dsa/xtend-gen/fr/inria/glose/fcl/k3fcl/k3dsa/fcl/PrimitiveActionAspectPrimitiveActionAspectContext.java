package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.PrimitiveActionAspectPrimitiveActionAspectProperties;
import fr.inria.glose.fcl.model.fcl.PrimitiveAction;
import java.util.Map;

@SuppressWarnings("all")
public class PrimitiveActionAspectPrimitiveActionAspectContext {
  public final static PrimitiveActionAspectPrimitiveActionAspectContext INSTANCE = new PrimitiveActionAspectPrimitiveActionAspectContext();
  
  public static PrimitiveActionAspectPrimitiveActionAspectProperties getSelf(final PrimitiveAction _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.PrimitiveActionAspectPrimitiveActionAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<PrimitiveAction, PrimitiveActionAspectPrimitiveActionAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.PrimitiveAction, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.PrimitiveActionAspectPrimitiveActionAspectProperties>();
  
  public Map<PrimitiveAction, PrimitiveActionAspectPrimitiveActionAspectProperties> getMap() {
    return map;
  }
}
