package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExternalProcedureAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.JavaProcedureAspectJavaProcedureAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NotImplementedException;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.FloatValue;
import fr.inria.glose.fcl.model.fcl.FloatValueType;
import fr.inria.glose.fcl.model.fcl.JavaProcedure;
import fr.inria.glose.fcl.model.fcl.ProcedureParameter;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext;
import java.lang.reflect.Method;
import org.eclipse.gemoc.commons.eclipse.emf.EObjectUtil;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@Aspect(className = JavaProcedure.class)
@SuppressWarnings("all")
public class JavaProcedureAspect extends ExternalProcedureAspect {
  public static void runProcedure(final JavaProcedure _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.JavaProcedureAspectJavaProcedureAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.JavaProcedureAspectJavaProcedureAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void runProcedure()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.JavaProcedure){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.JavaProcedureAspect._privk3_runProcedure(_self_, (fr.inria.glose.fcl.model.fcl.JavaProcedure)_self);
    };
  }
  
  protected static void _privk3_runProcedure(final JavaProcedureAspectJavaProcedureAspectProperties _self_, final JavaProcedure _self) {
    try {
      final ProcedureContext context = IterableExtensions.<ProcedureContext>last(EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getProcedureStack());
      boolean _isStatic = _self.isStatic();
      if (_isStatic) {
        if (((_self.getReturnType() != null) && (_self.getReturnType() instanceof FloatValueType))) {
          final ProcedureParameter param = _self.getProcedureParameters().get(0);
          Object _rawValue = ValueAspect.rawValue(ValueAspect.toFloatValue(CallableDeclarationAspect.call(param)));
          final double rawFloat = (((Double) _rawValue)).doubleValue();
          final FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
          final int lastindexof = _self.getMethodFullQualifiedName().lastIndexOf(".");
          final String methodName = _self.getMethodFullQualifiedName().substring((lastindexof + 1));
          final String className = _self.getMethodFullQualifiedName().substring(0, lastindexof);
          final Class klass = Class.forName(className);
          final Method m = klass.getDeclaredMethod(methodName, double.class);
          m.invoke(null, Double.valueOf(rawFloat));
          Object _invoke = m.invoke(null, Double.valueOf(rawFloat));
          aValue.setFloatValue((((Double) _invoke)).doubleValue());
          context.setResultValue(aValue);
        } else {
          EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement JavaProcedure.runProcedure() for " + _self));
          throw new NotImplementedException(("not implemented, please implement JavaProcedure.runProcedure() for " + _self));
        }
      } else {
        EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement JavaProcedure.runProcedure() for non static " + _self));
        throw new NotImplementedException(("not implemented, please implement JavaProcedure.runProcedure() for non static " + _self));
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
