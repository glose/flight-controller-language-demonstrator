package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import com.google.common.base.Objects;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDataStructPropertyAspectCallDataStructPropertyAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyReferenceAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLException;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.CallDataStructProperty;
import fr.inria.glose.fcl.model.fcl.StructValue;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@Aspect(className = CallDataStructProperty.class, with = { DataStructPropertyReferenceAspect.class })
@SuppressWarnings("all")
public class CallDataStructPropertyAspect extends CallAspect {
  /**
   * BE CAREFUL :
   * 
   * This class has more than one superclass
   * please specify which parent you want with the 'super' expected calling
   */
  public static Value evaluate(final CallDataStructProperty _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDataStructPropertyAspectCallDataStructPropertyAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDataStructPropertyAspectCallDataStructPropertyAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.CallDataStructProperty){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDataStructPropertyAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.CallDataStructProperty)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static Value _privk3_evaluate(final CallDataStructPropertyAspectCallDataStructPropertyAspectProperties _self_, final CallDataStructProperty _self) {
    try {
      Value _xblockexpression = null;
      {
        final Value r = CallableDeclarationAspect.call(_self.getRootTargetDataStruct().getCallable());
        if ((r == null)) {
          String _qualifiedName = EObjectAspect.getQualifiedName(_self.getRootTargetDataStruct().getCallable());
          String _plus = ("struct navigation error " + _qualifiedName);
          String _plus_1 = (_plus + " hasn\'t been initialized");
          EObjectAspect.error(_self, _plus_1);
          String _qualifiedName_1 = EObjectAspect.getQualifiedName(_self.getRootTargetDataStruct().getCallable());
          String _plus_2 = ("struct navigation error " + _qualifiedName_1);
          String _plus_3 = (_plus_2 + " hasn\'t been initialized");
          throw new FCLException(_plus_3);
        }
        Value _xifexpression = null;
        if ((r instanceof StructValue)) {
          StructValue currentResult = ((StructValue)r);
          final String last = IterableExtensions.<String>last(_self.getPropertyName());
          EList<String> _propertyName = _self.getPropertyName();
          for (final String propName : _propertyName) {
            {
              final Value r2 = StructValueAspect.getValueInProperty(currentResult, propName);
              if ((r2 instanceof StructValue)) {
                currentResult = ((StructValue)r2);
              }
              if (((!Objects.equal(propName, last)) && (!(r2 instanceof StructValue)))) {
                String _valueToString = ValueAspect.valueToString(r2);
                String _plus_4 = ("struct navigation error " + _valueToString);
                String _plus_5 = (_plus_4 + " isn\'t a StructValue while navigating ");
                String _valueToString_1 = StructValueAspect.valueToString(((StructValue)r));
                String _plus_6 = (_plus_5 + _valueToString_1);
                String _plus_7 = (_plus_6 + "->");
                String _join = IterableExtensions.join(_self.getPropertyName(), "->");
                String _plus_8 = (_plus_7 + _join);
                EObjectAspect.error(_self, _plus_8);
                String _valueToString_2 = ValueAspect.valueToString(r2);
                String _plus_9 = ("struct navigation error " + _valueToString_2);
                String _plus_10 = (_plus_9 + " isn\'t a StructValue");
                throw new FCLException(_plus_10);
              } else {
                return r2;
              }
            }
          }
        } else {
          EObjectAspect.error(_self, (("struct navigation error " + r) + " isn\'t a StructValue"));
          String _valueToString = ValueAspect.valueToString(r);
          String _plus_4 = ("struct navigation error " + _valueToString);
          String _plus_5 = (_plus_4 + " isn\'t a StructValue");
          throw new FCLException(_plus_5);
        }
        _xblockexpression = _xifexpression;
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
