package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureAspectProcedureAspectProperties;
import fr.inria.glose.fcl.model.fcl.Procedure;
import java.util.Map;

@SuppressWarnings("all")
public class ProcedureAspectProcedureAspectContext {
  public final static ProcedureAspectProcedureAspectContext INSTANCE = new ProcedureAspectProcedureAspectContext();
  
  public static ProcedureAspectProcedureAspectProperties getSelf(final Procedure _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureAspectProcedureAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<Procedure, ProcedureAspectProcedureAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.Procedure, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureAspectProcedureAspectProperties>();
  
  public Map<Procedure, ProcedureAspectProcedureAspectProperties> getMap() {
    return map;
  }
}
