package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties;
import fr.inria.glose.fcl.model.fcl.FloatValue;
import java.util.Map;

@SuppressWarnings("all")
public class FloatValueAspectFloatValueAspectContext {
  public final static FloatValueAspectFloatValueAspectContext INSTANCE = new FloatValueAspectFloatValueAspectContext();
  
  public static FloatValueAspectFloatValueAspectProperties getSelf(final FloatValue _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<FloatValue, FloatValueAspectFloatValueAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.FloatValue, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties>();
  
  public Map<FloatValue, FloatValueAspectFloatValueAspectProperties> getMap() {
    return map;
  }
}
