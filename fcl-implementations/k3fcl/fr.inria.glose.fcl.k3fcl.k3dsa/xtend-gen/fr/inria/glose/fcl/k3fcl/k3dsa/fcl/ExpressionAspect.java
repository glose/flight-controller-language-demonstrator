package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspectExpressionAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLException;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NotImplementedException;
import fr.inria.glose.fcl.model.fcl.BooleanValue;
import fr.inria.glose.fcl.model.fcl.Expression;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.xtext.xbase.lib.Exceptions;

@Aspect(className = Expression.class)
@SuppressWarnings("all")
public abstract class ExpressionAspect {
  /**
   * evaluate the expression and get the boolean
   * if not a boolean raise an exception and stop
   */
  public static boolean evaluateAsBoolean(final Expression _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspectExpressionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspectExpressionAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# boolean evaluateAsBoolean()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Expression){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect._privk3_evaluateAsBoolean(_self_, (fr.inria.glose.fcl.model.fcl.Expression)_self);
    };
    return (boolean)result;
  }
  
  public static Value evaluate(final Expression _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspectExpressionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspectExpressionAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorUnaryExpressionAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.BasicOperatorUnaryExpression){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorUnaryExpressionAspect.evaluate((fr.inria.glose.fcl.model.fcl.BasicOperatorUnaryExpression)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorUnaryExpressionAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureCallAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.ProcedureCall){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureCallAspect.evaluate((fr.inria.glose.fcl.model.fcl.ProcedureCall)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureCallAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallConstantAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.CallConstant){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallConstantAspect.evaluate((fr.inria.glose.fcl.model.fcl.CallConstant)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallConstantAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.Call){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect.evaluate((fr.inria.glose.fcl.model.fcl.Call)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDataStructPropertyAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.CallDataStructProperty){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDataStructPropertyAspect.evaluate((fr.inria.glose.fcl.model.fcl.CallDataStructProperty)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDataStructPropertyAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDeclarationReferenceAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.CallDeclarationReference){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDeclarationReferenceAspect.evaluate((fr.inria.glose.fcl.model.fcl.CallDeclarationReference)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDeclarationReferenceAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NewDataStructAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.NewDataStruct){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NewDataStructAspect.evaluate((fr.inria.glose.fcl.model.fcl.NewDataStruct)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NewDataStructAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorBinaryExpressionAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.BasicOperatorBinaryExpression){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorBinaryExpressionAspect.evaluate((fr.inria.glose.fcl.model.fcl.BasicOperatorBinaryExpression)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorBinaryExpressionAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallPreviousAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.CallPrevious){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallPreviousAspect.evaluate((fr.inria.glose.fcl.model.fcl.CallPrevious)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallPreviousAspect
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Expression){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.Expression)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static boolean _privk3_evaluateAsBoolean(final ExpressionAspectExpressionAspectProperties _self_, final Expression _self) {
    try {
      final Value internalResult = ExpressionAspect.evaluate(_self);
      boolean result = false;
      if ((internalResult instanceof BooleanValue)) {
        result = ((BooleanValue)internalResult).isBooleanValue();
      } else {
        throw new FCLException(("expected a boolean but got " + internalResult));
      }
      return result;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static Value _privk3_evaluate(final ExpressionAspectExpressionAspectProperties _self_, final Expression _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement evaluate() for " + _self));
      throw new NotImplementedException(("not implemented, please implement evaluate() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
