package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspectCallableDeclarationAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NotImplementedException;
import fr.inria.glose.fcl.model.fcl.CallableDeclaration;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.xtext.xbase.lib.Exceptions;

@Aspect(className = CallableDeclaration.class)
@SuppressWarnings("all")
public abstract class CallableDeclarationAspect {
  public static Value call(final CallableDeclaration _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspectCallableDeclarationAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspectCallableDeclarationAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#Value call() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationLiteralAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.EnumerationLiteral){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationLiteralAspect.call((fr.inria.glose.fcl.model.fcl.EnumerationLiteral)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#Value call() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationLiteralAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#Value call() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.VarDecl){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect.call((fr.inria.glose.fcl.model.fcl.VarDecl)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#Value call() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#Value call() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionVarDecl){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect.call((fr.inria.glose.fcl.model.fcl.FunctionVarDecl)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#Value call() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#Value call() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect.call((fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#Value call() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#Value call() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.ProcedureParameter){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect.call((fr.inria.glose.fcl.model.fcl.ProcedureParameter)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#Value call() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect
    // #DispatchPointCut_before# Value call()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.CallableDeclaration){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect._privk3_call(_self_, (fr.inria.glose.fcl.model.fcl.CallableDeclaration)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value callPrevious(final CallableDeclaration _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspectCallableDeclarationAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspectCallableDeclarationAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#Value callPrevious() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect.callPrevious((fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#Value callPrevious() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#Value callPrevious() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionVarDecl){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect.callPrevious((fr.inria.glose.fcl.model.fcl.FunctionVarDecl)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#Value callPrevious() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect
    // #DispatchPointCut_before# Value callPrevious()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.CallableDeclaration){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect._privk3_callPrevious(_self_, (fr.inria.glose.fcl.model.fcl.CallableDeclaration)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static DataType getResolvedType(final CallableDeclaration _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspectCallableDeclarationAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspectCallableDeclarationAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.VarDecl){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect.getResolvedType((fr.inria.glose.fcl.model.fcl.VarDecl)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect.getResolvedType((fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.ProcedureParameter){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect.getResolvedType((fr.inria.glose.fcl.model.fcl.ProcedureParameter)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionPortReference){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspect.getResolvedType((fr.inria.glose.fcl.model.fcl.FunctionPortReference)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionVarDecl){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect.getResolvedType((fr.inria.glose.fcl.model.fcl.FunctionVarDecl)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect
    // #DispatchPointCut_before# DataType getResolvedType()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.CallableDeclaration){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect._privk3_getResolvedType(_self_, (fr.inria.glose.fcl.model.fcl.CallableDeclaration)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.DataType)result;
  }
  
  public static String getSimpleName(final CallableDeclaration _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspectCallableDeclarationAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspectCallableDeclarationAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#String getSimpleName() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.ProcedureParameter){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect.getSimpleName((fr.inria.glose.fcl.model.fcl.ProcedureParameter)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect#String getSimpleName() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect
    // #DispatchPointCut_before# String getSimpleName()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.CallableDeclaration){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect._privk3_getSimpleName(_self_, (fr.inria.glose.fcl.model.fcl.CallableDeclaration)_self);
    };
    return (java.lang.String)result;
  }
  
  protected static Value _privk3_call(final CallableDeclarationAspectCallableDeclarationAspectProperties _self_, final CallableDeclaration _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement call() for " + _self));
      throw new NotImplementedException(("not implemented, please implement call() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static Value _privk3_callPrevious(final CallableDeclarationAspectCallableDeclarationAspectProperties _self_, final CallableDeclaration _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement callPrevious() for " + _self));
      throw new NotImplementedException(("not implemented, please implement callPrevious() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static DataType _privk3_getResolvedType(final CallableDeclarationAspectCallableDeclarationAspectProperties _self_, final CallableDeclaration _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement getResolvedType() for " + _self));
      throw new NotImplementedException(("not implemented, please implement getResolvedType() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static String _privk3_getSimpleName(final CallableDeclarationAspectCallableDeclarationAspectProperties _self_, final CallableDeclaration _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement getSimpleName() for " + _self));
      throw new NotImplementedException(("not implemented, please implement getSimpleName() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
