package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect;
import fr.inria.glose.fcl.model.fcl.ControlStructureAction;

@Aspect(className = ControlStructureAction.class)
@SuppressWarnings("all")
public abstract class ControlStructureActionAspect extends ActionAspect {
}
