package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueTypeAspectBooleanValueTypeAspectProperties;
import fr.inria.glose.fcl.model.fcl.BooleanValueType;
import java.util.Map;

@SuppressWarnings("all")
public class BooleanValueTypeAspectBooleanValueTypeAspectContext {
  public final static BooleanValueTypeAspectBooleanValueTypeAspectContext INSTANCE = new BooleanValueTypeAspectBooleanValueTypeAspectContext();
  
  public static BooleanValueTypeAspectBooleanValueTypeAspectProperties getSelf(final BooleanValueType _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueTypeAspectBooleanValueTypeAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<BooleanValueType, BooleanValueTypeAspectBooleanValueTypeAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.BooleanValueType, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueTypeAspectBooleanValueTypeAspectProperties>();
  
  public Map<BooleanValueType, BooleanValueTypeAspectBooleanValueTypeAspectProperties> getMap() {
    return map;
  }
}
