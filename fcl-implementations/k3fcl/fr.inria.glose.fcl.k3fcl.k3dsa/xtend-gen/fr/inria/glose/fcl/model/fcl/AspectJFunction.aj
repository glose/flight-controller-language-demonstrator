// AspectJ classes that have been aspectized and name
package fr.inria.glose.fcl.model.fcl;
public aspect AspectJFunction{
boolean around (fr.inria.glose.fcl.model.fcl.Function self)  :target (self) && (call ( boolean fr.inria.glose.fcl.model.fcl.Function.isEnabled(  ) ) ) { return fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect.isEnabled(self );}

}
