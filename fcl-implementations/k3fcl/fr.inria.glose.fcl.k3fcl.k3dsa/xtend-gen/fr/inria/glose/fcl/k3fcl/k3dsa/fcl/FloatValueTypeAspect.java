package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueTypeAspectFloatValueTypeAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueTypeAspect;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.FloatValue;
import fr.inria.glose.fcl.model.fcl.FloatValueType;
import fr.inria.glose.fcl.model.fcl.Value;

@Aspect(className = FloatValueType.class)
@SuppressWarnings("all")
public class FloatValueTypeAspect extends ValueTypeAspect {
  public static Value createValue(final FloatValueType _self, final String internalValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueTypeAspectFloatValueTypeAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueTypeAspectFloatValueTypeAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value createValue(String)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValueType){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueTypeAspect._privk3_createValue(_self_, (fr.inria.glose.fcl.model.fcl.FloatValueType)_self,internalValue);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static Value _privk3_createValue(final FloatValueTypeAspectFloatValueTypeAspectProperties _self_, final FloatValueType _self, final String internalValue) {
    final FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
    aValue.setFloatValue(Double.parseDouble(internalValue));
    return aValue;
  }
}
