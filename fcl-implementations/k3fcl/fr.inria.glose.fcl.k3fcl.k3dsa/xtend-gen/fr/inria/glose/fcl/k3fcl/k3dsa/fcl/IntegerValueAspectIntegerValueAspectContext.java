package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspectIntegerValueAspectProperties;
import fr.inria.glose.fcl.model.fcl.IntegerValue;
import java.util.Map;

@SuppressWarnings("all")
public class IntegerValueAspectIntegerValueAspectContext {
  public final static IntegerValueAspectIntegerValueAspectContext INSTANCE = new IntegerValueAspectIntegerValueAspectContext();
  
  public static IntegerValueAspectIntegerValueAspectProperties getSelf(final IntegerValue _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspectIntegerValueAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<IntegerValue, IntegerValueAspectIntegerValueAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.IntegerValue, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspectIntegerValueAspectProperties>();
  
  public Map<IntegerValue, IntegerValueAspectIntegerValueAspectProperties> getMap() {
    return map;
  }
}
