package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import com.google.common.base.Objects;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect;
import fr.inria.glose.fcl.model.fcl.FMUFunction;
import fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper;
import org.eclipse.gemoc.commons.eclipse.emf.EObjectUtil;
import org.javafmi.proxy.Status;
import org.javafmi.wrapper.generic.Simulation2;

@Aspect(className = FMUFunctionBlockDataPort.class)
@SuppressWarnings("all")
public class FMUFunctionBlockDataPortAspect extends FunctionBlockDataPortAspect {
  public static String getFMUVarName(final FMUFunctionBlockDataPort _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# String getFMUVarName()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspect._privk3_getFMUVarName(_self_, (fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort)_self);
    };
    return (java.lang.String)result;
  }
  
  public static void setSimulation2RuntimeVariableFromPortValue(final FMUFunctionBlockDataPort _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void setSimulation2RuntimeVariableFromPortValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspect._privk3_setSimulation2RuntimeVariableFromPortValue(_self_, (fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort)_self);
    };
  }
  
  public static double getPortValueFromSimulation2RuntimeVariable(final FMUFunctionBlockDataPort _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# double getPortValueFromSimulation2RuntimeVariable()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspect._privk3_getPortValueFromSimulation2RuntimeVariable(_self_, (fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort)_self);
    };
    return (double)result;
  }
  
  protected static String _privk3_getFMUVarName(final FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectProperties _self_, final FMUFunctionBlockDataPort _self) {
    if (((_self.getRunnableFmuVariableName() == null) || Objects.equal(_self.getRunnableFmuVariableName(), ""))) {
      return _self.getName();
    } else {
      return _self.getRunnableFmuVariableName();
    }
  }
  
  protected static void _privk3_setSimulation2RuntimeVariableFromPortValue(final FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectProperties _self_, final FMUFunctionBlockDataPort _self) {
    final String portName = FMUFunctionBlockDataPortAspect.getFMUVarName(_self);
    final Double portValue = Double.valueOf(FunctionBlockDataPortAspect.getFloat(_self));
    final FMUSimulationWrapper wrapper = EObjectUtil.<FMUFunction>eContainerOfType(_self, FMUFunction.class).getFmuSimulationWrapper();
    final Simulation2 javaFMI_FMU = wrapper.getFmiSimulation2();
    final Status status = javaFMI_FMU.write(portName).with((portValue).doubleValue());
    boolean _notEquals = (!Objects.equal(status, Status.OK));
    if (_notEquals) {
      String _value = status.getValue();
      String _plus = ((((("Failed to write " + portValue) + " in fmu variable ") + portName) + " ") + _value);
      System.err.println(_plus);
    }
  }
  
  protected static double _privk3_getPortValueFromSimulation2RuntimeVariable(final FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectProperties _self_, final FMUFunctionBlockDataPort _self) {
    final FMUSimulationWrapper wrapper = EObjectUtil.<FMUFunction>eContainerOfType(_self, FMUFunction.class).getFmuSimulationWrapper();
    final Simulation2 javaFMI_FMU = wrapper.getFmiSimulation2();
    return javaFMI_FMU.read(FMUFunctionBlockDataPortAspect.getFMUVarName(_self)).asDouble();
  }
}
