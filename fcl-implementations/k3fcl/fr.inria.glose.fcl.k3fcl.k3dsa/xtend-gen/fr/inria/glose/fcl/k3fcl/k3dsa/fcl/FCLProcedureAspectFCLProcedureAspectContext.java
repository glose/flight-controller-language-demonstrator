package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureAspectFCLProcedureAspectProperties;
import fr.inria.glose.fcl.model.fcl.FCLProcedure;
import java.util.Map;

@SuppressWarnings("all")
public class FCLProcedureAspectFCLProcedureAspectContext {
  public final static FCLProcedureAspectFCLProcedureAspectContext INSTANCE = new FCLProcedureAspectFCLProcedureAspectContext();
  
  public static FCLProcedureAspectFCLProcedureAspectProperties getSelf(final FCLProcedure _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureAspectFCLProcedureAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<FCLProcedure, FCLProcedureAspectFCLProcedureAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.FCLProcedure, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureAspectFCLProcedureAspectProperties>();
  
  public Map<FCLProcedure, FCLProcedureAspectFCLProcedureAspectProperties> getMap() {
    return map;
  }
}
