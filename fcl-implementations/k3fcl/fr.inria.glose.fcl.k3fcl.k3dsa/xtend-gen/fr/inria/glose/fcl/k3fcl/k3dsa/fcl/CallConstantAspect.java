package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallConstantAspectCallConstantAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.CallConstant;
import fr.inria.glose.fcl.model.fcl.Value;

@Aspect(className = CallConstant.class)
@SuppressWarnings("all")
public class CallConstantAspect extends CallAspect {
  public static Value evaluate(final CallConstant _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallConstantAspectCallConstantAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallConstantAspectCallConstantAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.CallConstant){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallConstantAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.CallConstant)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static Value _privk3_evaluate(final CallConstantAspectCallConstantAspectProperties _self_, final CallConstant _self) {
    return ValueAspect.evaluate(_self.getValue());
  }
}
