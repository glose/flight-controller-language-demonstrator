package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspectCallAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NotImplementedException;
import fr.inria.glose.fcl.model.fcl.Call;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.xtext.xbase.lib.Exceptions;

@Aspect(className = Call.class)
@SuppressWarnings("all")
public abstract class CallAspect extends ExpressionAspect {
  public static Value evaluate(final Call _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspectCallAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspectCallAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallPreviousAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.CallPrevious){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallPreviousAspect.evaluate((fr.inria.glose.fcl.model.fcl.CallPrevious)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallPreviousAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDataStructPropertyAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.CallDataStructProperty){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDataStructPropertyAspect.evaluate((fr.inria.glose.fcl.model.fcl.CallDataStructProperty)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDataStructPropertyAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallConstantAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.CallConstant){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallConstantAspect.evaluate((fr.inria.glose.fcl.model.fcl.CallConstant)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallConstantAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDeclarationReferenceAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.CallDeclarationReference){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDeclarationReferenceAspect.evaluate((fr.inria.glose.fcl.model.fcl.CallDeclarationReference)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDeclarationReferenceAspect
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Call){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.Call)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static Value _privk3_evaluate(final CallAspectCallAspectProperties _self_, final Call _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement evaluate() for " + _self));
      throw new NotImplementedException(("not implemented, please implement evaluate() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
