package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import java.util.Map;

@SuppressWarnings("all")
public class FCLModelAspectFCLModelAspectContext {
  public final static FCLModelAspectFCLModelAspectContext INSTANCE = new FCLModelAspectFCLModelAspectContext();
  
  public static FCLModelAspectFCLModelAspectProperties getSelf(final FCLModel _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<FCLModel, FCLModelAspectFCLModelAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.FCLModel, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties>();
  
  public Map<FCLModel, FCLModelAspectFCLModelAspectProperties> getMap() {
    return map;
  }
}
