package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructPropertyValueAspectStructPropertyValueAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.StructPropertyValue;
import fr.inria.glose.fcl.model.fcl.Value;

@Aspect(className = StructPropertyValue.class)
@SuppressWarnings("all")
public class StructPropertyValueAspect {
  public static Value evaluate(final StructPropertyValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructPropertyValueAspectStructPropertyValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructPropertyValueAspectStructPropertyValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.StructPropertyValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructPropertyValueAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.StructPropertyValue)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static StructPropertyValue copy(final StructPropertyValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructPropertyValueAspectStructPropertyValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructPropertyValueAspectStructPropertyValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# StructPropertyValue copy()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.StructPropertyValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructPropertyValueAspect._privk3_copy(_self_, (fr.inria.glose.fcl.model.fcl.StructPropertyValue)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.StructPropertyValue)result;
  }
  
  protected static Value _privk3_evaluate(final StructPropertyValueAspectStructPropertyValueAspectProperties _self_, final StructPropertyValue _self) {
    return _self.getValue();
  }
  
  protected static StructPropertyValue _privk3_copy(final StructPropertyValueAspectStructPropertyValueAspectProperties _self_, final StructPropertyValue _self) {
    final StructPropertyValue aValue = FclFactory.eINSTANCE.createStructPropertyValue();
    aValue.setValue(ValueAspect.copy(_self.getValue()));
    aValue.setName(_self.getName());
    return aValue;
  }
}
