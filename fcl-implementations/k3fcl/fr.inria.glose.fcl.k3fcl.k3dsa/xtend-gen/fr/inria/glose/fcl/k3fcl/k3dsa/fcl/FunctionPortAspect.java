package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspectFunctionPortAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NotImplementedException;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.FunctionPort;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.xtext.xbase.lib.Exceptions;

@Aspect(className = FunctionPort.class, with = { CallableDeclarationAspect.class })
@SuppressWarnings("all")
public class FunctionPortAspect extends AssignableAspect {
  /**
   * BE CAREFUL :
   * 
   * This class has more than one superclass
   * please specify which parent you want with the 'super' expected calling
   */
  public static FunctionBlockDataPort getTargetDataPort(final FunctionPort _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspectFunctionPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspectFunctionPortAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspect#FunctionBlockDataPort getTargetDataPort() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionPortReference){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspect.getTargetDataPort((fr.inria.glose.fcl.model.fcl.FunctionPortReference)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspect#FunctionBlockDataPort getTargetDataPort() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspect#FunctionBlockDataPort getTargetDataPort() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect.getTargetDataPort((fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspect#FunctionBlockDataPort getTargetDataPort() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect
    // #DispatchPointCut_before# FunctionBlockDataPort getTargetDataPort()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionPort){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspect._privk3_getTargetDataPort(_self_, (fr.inria.glose.fcl.model.fcl.FunctionPort)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)result;
  }
  
  protected static FunctionBlockDataPort _privk3_getTargetDataPort(final FunctionPortAspectFunctionPortAspectProperties _self_, final FunctionPort _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement getTargetDataPort() for " + _self));
      throw new NotImplementedException(("not implemented, please implement getTargetDataPort() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public static Value call(final FunctionPort _self) {
    return CallableDeclarationAspect.call(_self);
  }
  
  public static Value callPrevious(final FunctionPort _self) {
    return CallableDeclarationAspect.callPrevious(_self);
  }
  
  public static DataType getResolvedType(final FunctionPort _self) {
    return CallableDeclarationAspect.getResolvedType(_self);
  }
  
  public static String getSimpleName(final FunctionPort _self) {
    return CallableDeclarationAspect.getSimpleName(_self);
  }
}
