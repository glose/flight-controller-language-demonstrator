package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLException;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.PrimitiveActionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureCallAspectProcedureCallAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.Expression;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.ProcedureCall;
import fr.inria.glose.fcl.model.fcl.Value;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmFactory;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext;
import org.eclipse.emf.common.util.EList;
import org.eclipse.gemoc.commons.eclipse.emf.EObjectUtil;
import org.eclipse.xtext.xbase.lib.Exceptions;

@Aspect(className = ProcedureCall.class, with = { ExpressionAspect.class })
@SuppressWarnings("all")
public class ProcedureCallAspect extends PrimitiveActionAspect {
  /**
   * BE CAREFUL :
   * 
   * This class has more than one superclass
   * please specify which parent you want with the 'super' expected calling
   */
  public static Value evaluate(final ProcedureCall _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureCallAspectProcedureCallAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureCallAspectProcedureCallAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.ProcedureCall){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureCallAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.ProcedureCall)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static Value _privk3_evaluate(final ProcedureCallAspectProcedureCallAspectProperties _self_, final ProcedureCall _self) {
    try {
      ProcedureContext procedureContext = Interpreter_vmFactory.eINSTANCE.createProcedureContext();
      Integer index = Integer.valueOf(0);
      EList<Expression> _procedureCallArguments = _self.getProcedureCallArguments();
      for (final Expression p : _procedureCallArguments) {
        {
          final DeclarationMapEntry mapEntry = Interpreter_vmFactory.eINSTANCE.createDeclarationMapEntry();
          mapEntry.setCallableDeclaration(_self.getProcedure().getProcedureParameters().get((index).intValue()));
          final Value pValue = ExpressionAspect.evaluate(p);
          mapEntry.setValue(ValueAspect.copy(pValue));
          Boolean _isKindOf = ValueAspect.isKindOf(mapEntry.getValue(), CallableDeclarationAspect.getResolvedType(mapEntry.getCallableDeclaration()));
          boolean _not = (!(_isKindOf).booleanValue());
          if (_not) {
            String _name = CallableDeclarationAspect.getResolvedType(mapEntry.getCallableDeclaration()).getName();
            String _plus = ("parameter call mismatch: expecting " + _name);
            String _plus_1 = (_plus + " received ");
            String _valueToString = ValueAspect.valueToString(mapEntry.getValue());
            String _plus_2 = (_plus_1 + _valueToString);
            String _plus_3 = (_plus_2 + " for ");
            String _qualifiedName = EObjectAspect.getQualifiedName(mapEntry.getCallableDeclaration());
            String _plus_4 = (_plus_3 + _qualifiedName);
            EObjectAspect.error(_self, _plus_4);
            String _name_1 = CallableDeclarationAspect.getResolvedType(mapEntry.getCallableDeclaration()).getName();
            String _plus_5 = ("parameter call mismatch: expecting " + _name_1);
            String _plus_6 = (_plus_5 + " received ");
            String _valueToString_1 = ValueAspect.valueToString(mapEntry.getValue());
            String _plus_7 = (_plus_6 + _valueToString_1);
            throw new FCLException(_plus_7);
          }
          EList<DeclarationMapEntry> _declarationMapEntries = procedureContext.getDeclarationMapEntries();
          _declarationMapEntries.add(mapEntry);
          index = Integer.valueOf(((index).intValue() + 1));
        }
      }
      EList<ProcedureContext> _procedureStack = EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getProcedureStack();
      _procedureStack.add(procedureContext);
      ProcedureAspect.runProcedure(_self.getProcedure());
      final Value result = procedureContext.getResultValue();
      EList<ProcedureContext> _procedureStack_1 = EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getProcedureStack();
      _procedureStack_1.remove(procedureContext);
      return result;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public static boolean evaluateAsBoolean(final ProcedureCall _self) {
    return ExpressionAspect.evaluateAsBoolean(_self);
  }
}
