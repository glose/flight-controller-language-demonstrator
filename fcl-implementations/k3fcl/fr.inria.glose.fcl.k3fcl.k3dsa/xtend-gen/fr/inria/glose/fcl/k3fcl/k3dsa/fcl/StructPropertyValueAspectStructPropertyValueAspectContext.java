package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructPropertyValueAspectStructPropertyValueAspectProperties;
import fr.inria.glose.fcl.model.fcl.StructPropertyValue;
import java.util.Map;

@SuppressWarnings("all")
public class StructPropertyValueAspectStructPropertyValueAspectContext {
  public final static StructPropertyValueAspectStructPropertyValueAspectContext INSTANCE = new StructPropertyValueAspectStructPropertyValueAspectContext();
  
  public static StructPropertyValueAspectStructPropertyValueAspectProperties getSelf(final StructPropertyValue _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructPropertyValueAspectStructPropertyValueAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<StructPropertyValue, StructPropertyValueAspectStructPropertyValueAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.StructPropertyValue, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructPropertyValueAspectStructPropertyValueAspectProperties>();
  
  public Map<StructPropertyValue, StructPropertyValueAspectStructPropertyValueAspectProperties> getMap() {
    return map;
  }
}
