package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.diverse.k3.al.annotationprocessor.ReplaceAspectMethod;
import fr.inria.diverse.k3.al.annotationprocessor.Step;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLException;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.Action;
import fr.inria.glose.fcl.model.fcl.DirectionKind;
import fr.inria.glose.fcl.model.fcl.Expression;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.Function;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.FunctionConnector;
import fr.inria.glose.fcl.model.fcl.FunctionPort;
import fr.inria.glose.fcl.model.fcl.FunctionTimeReference;
import fr.inria.glose.fcl.model.fcl.FunctionVarDecl;
import fr.inria.glose.fcl.model.fcl.Value;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmFactory;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.commons.eclipse.emf.EObjectUtil;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@Aspect(className = Function.class)
@SuppressWarnings("all")
public class FunctionAspect {
  public static void dse_startEvalFunction(final Function _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void dse_startEvalFunction()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Function){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect._privk3_dse_startEvalFunction(_self_, (fr.inria.glose.fcl.model.fcl.Function)_self);
    };
  }
  
  public static void startEvalFunction(final Function _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void startEvalFunction()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Function){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect._privk3_startEvalFunction(_self_, (fr.inria.glose.fcl.model.fcl.Function)_self);
    };
  }
  
  public static void dse_stopEvalFunction(final Function _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void dse_stopEvalFunction()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Function){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect._privk3_dse_stopEvalFunction(_self_, (fr.inria.glose.fcl.model.fcl.Function)_self);
    };
  }
  
  public static void stopEvalFunction(final Function _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void stopEvalFunction()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Function){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect._privk3_stopEvalFunction(_self_, (fr.inria.glose.fcl.model.fcl.Function)_self);
    };
  }
  
  @Step
  public static void evalFunction(final Function _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectContext.getSelf(_self);
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect#void evalFunction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FMUFunction){
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect.evalFunction((fr.inria.glose.fcl.model.fcl.FMUFunction)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect#void evalFunction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect#void evalFunction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.UnityFunction){
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspect.evalFunction((fr.inria.glose.fcl.model.fcl.UnityFunction)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect#void evalFunction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspect
    // #DispatchPointCut_before# void evalFunction()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Function){
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand command = new fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand() {
    		@Override
    		public void execute() {
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect._privk3_evalFunction(_self_, (fr.inria.glose.fcl.model.fcl.Function)_self);
    		}
    	};
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager stepManager = fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepManagerRegistry.getInstance().findStepManager(_self);
    	if (stepManager != null) {
    		stepManager.executeStep(_self, new Object[] {_self}, command, "Function", "evalFunction");
    	} else {
    		command.execute();
    	}
    	;
    };
  }
  
  public static void initFunctionVars(final Function _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void initFunctionVars()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Function){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect._privk3_initFunctionVars(_self_, (fr.inria.glose.fcl.model.fcl.Function)_self);
    };
  }
  
  public static void initPortsDefaultValues(final Function _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void initPortsDefaultValues()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Function){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect._privk3_initPortsDefaultValues(_self_, (fr.inria.glose.fcl.model.fcl.Function)_self);
    };
  }
  
  /**
   * store previousValue into previousValue for all function variables and ports
   */
  @Step
  public static void updatePrevValues(final Function _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void updatePrevValues()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Function){
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand command = new fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand() {
    		@Override
    		public void execute() {
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect._privk3_updatePrevValues(_self_, (fr.inria.glose.fcl.model.fcl.Function)_self);
    		}
    	};
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager stepManager = fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepManagerRegistry.getInstance().findStepManager(_self);
    	if (stepManager != null) {
    		stepManager.executeStep(_self, new Object[] {_self}, command, "Function", "updatePrevValues");
    	} else {
    		command.execute();
    	}
    	;
    };
  }
  
  @Step
  public static void sendDataThroughAllDelayedConnectors(final Function _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void sendDataThroughAllDelayedConnectors()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Function){
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand command = new fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand() {
    		@Override
    		public void execute() {
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect._privk3_sendDataThroughAllDelayedConnectors(_self_, (fr.inria.glose.fcl.model.fcl.Function)_self);
    		}
    	};
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager stepManager = fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepManagerRegistry.getInstance().findStepManager(_self);
    	if (stepManager != null) {
    		stepManager.executeStep(_self, new Object[] {_self}, command, "Function", "sendDataThroughAllDelayedConnectors");
    	} else {
    		command.execute();
    	}
    	;
    };
  }
  
  public static void sendInputDataToActiveInternalFunctions(final Function _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void sendInputDataToActiveInternalFunctions()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Function){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect._privk3_sendInputDataToActiveInternalFunctions(_self_, (fr.inria.glose.fcl.model.fcl.Function)_self);
    };
  }
  
  public static void sendDataToActiveNonDelayedPorts(final Function _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void sendDataToActiveNonDelayedPorts()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Function){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect._privk3_sendDataToActiveNonDelayedPorts(_self_, (fr.inria.glose.fcl.model.fcl.Function)_self);
    };
  }
  
  /**
   * indicates if this function is enabled in the current mode
   */
  @ReplaceAspectMethod
  public static boolean isEnabled(final Function _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# boolean isEnabled()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Function){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect._privk3_isEnabled(_self_, (fr.inria.glose.fcl.model.fcl.Function)_self);
    };
    return (boolean)result;
  }
  
  public static String functionName(final Function _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# String functionName()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Function){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect._privk3_functionName(_self_, (fr.inria.glose.fcl.model.fcl.Function)_self);
    };
    return (java.lang.String)result;
  }
  
  /**
   * returns the subfunctions of self in a topologically sorted order
   * consider only enabled function in current mode
   * Kahn's algorithm from https://en.wikipedia.org/wiki/Topological_sorting
   */
  public static List<Function> sortedSubfunctions(final Function _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspectFunctionAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# List<Function> sortedSubfunctions()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Function){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect._privk3_sortedSubfunctions(_self_, (fr.inria.glose.fcl.model.fcl.Function)_self);
    };
    return (java.util.List<fr.inria.glose.fcl.model.fcl.Function>)result;
  }
  
  protected static void _privk3_dse_startEvalFunction(final FunctionAspectFunctionAspectProperties _self_, final Function _self) {
    FunctionAspect.startEvalFunction(_self);
  }
  
  protected static void _privk3_startEvalFunction(final FunctionAspectFunctionAspectProperties _self_, final Function _self) {
    EObject _eContainer = _self.eContainer();
    if ((_eContainer instanceof FCLModel)) {
      EObject _eContainer_1 = _self.eContainer();
      final FCLModel fclModel = ((FCLModel) _eContainer_1);
      String _functionName = FunctionAspect.functionName(_self);
      String _plus = ("### Evaluating Main dataflow " + _functionName);
      EObjectAspect.info(_self, _plus);
      final Function1<Function, String> _function = (Function f) -> {
        return EObjectAspect.getQualifiedName(f);
      };
      String _join = IterableExtensions.join(ListExtensions.<Function, String>map(fclModel.getModeAutomata().getCurrentMode().getEnabledFunctions(), _function), "\n\t");
      String _plus_1 = ("with the following enabled functions:\n\t" + _join);
      EObjectAspect.devInfo(_self, _plus_1);
    } else {
      String _functionName_1 = FunctionAspect.functionName(_self);
      String _plus_2 = ("evaluating sub function " + _functionName_1);
      String _plus_3 = (_plus_2 + "...");
      EObjectAspect.info(_self, _plus_3);
    }
    int _size = _self.getSubFunctions().size();
    boolean _greaterThan = (_size > 0);
    if (_greaterThan) {
      FunctionAspect.sendInputDataToActiveInternalFunctions(_self);
    }
    FunctionTimeReference _timeReference = _self.getTimeReference();
    boolean _tripleNotEquals = (_timeReference != null);
    if (_tripleNotEquals) {
      String _functionName_2 = FunctionAspect.functionName(_self);
      String _plus_4 = ("Increment time reference for function " + _functionName_2);
      EObjectAspect.devInfo(_self, _plus_4);
      final FunctionVarDecl timeVariable = _self.getTimeReference().getObservableVar();
      FunctionVarDeclAspect.assign(timeVariable, 
        ValueAspect.plus(timeVariable.getCurrentValue(), 
          ExpressionAspect.evaluate(_self.getTimeReference().getIncrement())));
    }
    Action _action = _self.getAction();
    boolean _tripleNotEquals_1 = (_action != null);
    if (_tripleNotEquals_1) {
      final ProcedureContext procedureContext = Interpreter_vmFactory.eINSTANCE.createProcedureContext();
      EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getProcedureStack().add(procedureContext);
      ActionAspect.doAction(_self.getAction());
      EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getProcedureStack().remove(procedureContext);
    }
  }
  
  protected static void _privk3_dse_stopEvalFunction(final FunctionAspectFunctionAspectProperties _self_, final Function _self) {
    FunctionAspect.stopEvalFunction(_self);
  }
  
  protected static void _privk3_stopEvalFunction(final FunctionAspectFunctionAspectProperties _self_, final Function _self) {
    FunctionAspect.sendDataToActiveNonDelayedPorts(_self);
  }
  
  protected static void _privk3_evalFunction(final FunctionAspectFunctionAspectProperties _self_, final Function _self) {
    FunctionAspect.startEvalFunction(_self);
    int _size = _self.getSubFunctions().size();
    boolean _greaterThan = (_size > 0);
    if (_greaterThan) {
      final List<Function> sortedF = FunctionAspect.sortedSubfunctions(_self);
      for (final Function f : sortedF) {
        FunctionAspect.evalFunction(f);
      }
    }
    FunctionAspect.stopEvalFunction(_self);
  }
  
  protected static void _privk3_initFunctionVars(final FunctionAspectFunctionAspectProperties _self_, final Function _self) {
    EList<FunctionVarDecl> _functionVarDecls = _self.getFunctionVarDecls();
    for (final FunctionVarDecl functionVar : _functionVarDecls) {
      Expression _initialValue = functionVar.getInitialValue();
      boolean _tripleNotEquals = (_initialValue != null);
      if (_tripleNotEquals) {
        final Value defaultValue = ExpressionAspect.evaluate(functionVar.getInitialValue());
        FunctionVarDeclAspect.assign(functionVar, defaultValue);
      }
    }
    EList<Function> _subFunctions = _self.getSubFunctions();
    for (final Function subFunction : _subFunctions) {
      FunctionAspect.initFunctionVars(subFunction);
    }
  }
  
  protected static void _privk3_initPortsDefaultValues(final FunctionAspectFunctionAspectProperties _self_, final Function _self) {
    Iterable<FunctionBlockDataPort> _filter = Iterables.<FunctionBlockDataPort>filter(_self.getFunctionPorts(), FunctionBlockDataPort.class);
    for (final FunctionBlockDataPort port : _filter) {
      Expression _defaultInputValue = port.getDefaultInputValue();
      boolean _tripleNotEquals = (_defaultInputValue != null);
      if (_tripleNotEquals) {
        final Value defaultValue = ExpressionAspect.evaluate(port.getDefaultInputValue());
        FunctionBlockDataPortAspect.assign(port, defaultValue);
      }
    }
    EList<Function> _subFunctions = _self.getSubFunctions();
    for (final Function subFunction : _subFunctions) {
      FunctionAspect.initPortsDefaultValues(subFunction);
    }
  }
  
  protected static void _privk3_updatePrevValues(final FunctionAspectFunctionAspectProperties _self_, final Function _self) {
    Iterable<FunctionBlockDataPort> _filter = Iterables.<FunctionBlockDataPort>filter(_self.getFunctionPorts(), FunctionBlockDataPort.class);
    for (final FunctionBlockDataPort port : _filter) {
      FunctionBlockDataPortAspect.previousValue(port, port.getCurrentValue());
    }
    EList<FunctionVarDecl> _functionVarDecls = _self.getFunctionVarDecls();
    for (final FunctionVarDecl functionVar : _functionVarDecls) {
      FunctionVarDeclAspect.previousValue(functionVar, functionVar.getCurrentValue());
    }
    EList<Function> _subFunctions = _self.getSubFunctions();
    for (final Function subFunction : _subFunctions) {
      FunctionAspect.updatePrevValues(subFunction);
    }
  }
  
  protected static void _privk3_sendDataThroughAllDelayedConnectors(final FunctionAspectFunctionAspectProperties _self_, final Function _self) {
    try {
      final Function1<FunctionPort, Boolean> _function = (FunctionPort p) -> {
        return Boolean.valueOf((Objects.equal(p.getDirection(), DirectionKind.OUT) || Objects.equal(p.getDirection(), DirectionKind.IN_OUT)));
      };
      final Function1<FunctionPort, FunctionBlockDataPort> _function_1 = (FunctionPort pp) -> {
        return FunctionPortAspect.getTargetDataPort(pp);
      };
      final List<FunctionBlockDataPort> outputPorts = IterableExtensions.<FunctionBlockDataPort>toList(IterableExtensions.<FunctionPort, FunctionBlockDataPort>map(IterableExtensions.<FunctionPort>filter(_self.getFunctionPorts(), _function), _function_1));
      for (final FunctionBlockDataPort outPort : outputPorts) {
        {
          final Function1<FunctionConnector, Boolean> _function_2 = (FunctionConnector c) -> {
            return Boolean.valueOf(((c.isDelayed() && Objects.equal(c.getEmitter(), outPort)) && EObjectUtil.<Function>eContainerOfType(c.getReceiver(), Function.class).isEnabled()));
          };
          final List<FunctionConnector> connectors = IterableExtensions.<FunctionConnector>toList(IterableExtensions.<FunctionConnector>filter(EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).allConnectors(), _function_2));
          for (final FunctionConnector c : connectors) {
            Value _currentValue = outPort.getCurrentValue();
            boolean _tripleNotEquals = (_currentValue != null);
            if (_tripleNotEquals) {
              String _qualifiedName = EObjectAspect.getQualifiedName(FunctionPortAspect.getTargetDataPort(c.getReceiver()));
              String _plus = (_qualifiedName + " receives ");
              String _valueToString = ValueAspect.valueToString(outPort.getCurrentValue());
              String _plus_1 = (_plus + _valueToString);
              EObjectAspect.important(_self, _plus_1);
              FunctionBlockDataPortAspect.assign(FunctionPortAspect.getTargetDataPort(c.getReceiver()), outPort.getCurrentValue());
            } else {
              String _qualifiedName_1 = EObjectAspect.getQualifiedName(outPort);
              String _plus_2 = ("Out port " + _qualifiedName_1);
              String _plus_3 = (_plus_2 + " has no value. Please verify that your model assigns a value to it or defines a default value.");
              EObjectAspect.error(_self, _plus_3);
              String _qualifiedName_2 = EObjectAspect.getQualifiedName(outPort);
              String _plus_4 = ("Cannot assign null value from Out port " + _qualifiedName_2);
              String _plus_5 = (_plus_4 + " to In port ");
              String _qualifiedName_3 = EObjectAspect.getQualifiedName(c.getReceiver());
              String _plus_6 = (_plus_5 + _qualifiedName_3);
              throw new FCLException(_plus_6);
            }
          }
        }
      }
      EList<Function> _subFunctions = _self.getSubFunctions();
      for (final Function func : _subFunctions) {
        FunctionAspect.sendDataThroughAllDelayedConnectors(func);
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static void _privk3_sendInputDataToActiveInternalFunctions(final FunctionAspectFunctionAspectProperties _self_, final Function _self) {
    try {
      final Function1<FunctionPort, Boolean> _function = (FunctionPort p) -> {
        return Boolean.valueOf((Objects.equal(p.getDirection(), DirectionKind.IN) || Objects.equal(p.getDirection(), DirectionKind.IN_OUT)));
      };
      final Function1<FunctionPort, FunctionBlockDataPort> _function_1 = (FunctionPort pp) -> {
        return FunctionPortAspect.getTargetDataPort(pp);
      };
      final List<FunctionBlockDataPort> inputPorts = IterableExtensions.<FunctionBlockDataPort>toList(IterableExtensions.<FunctionPort, FunctionBlockDataPort>map(IterableExtensions.<FunctionPort>filter(_self.getFunctionPorts(), _function), _function_1));
      for (final FunctionBlockDataPort inPort : inputPorts) {
        {
          final List<FunctionConnector> allconnectors = EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).allConnectors();
          final Function1<FunctionConnector, Boolean> _function_2 = (FunctionConnector c) -> {
            return Boolean.valueOf(((((!c.isDelayed()) && Objects.equal(c.getEmitter(), inPort)) && EObjectUtil.<Function>eContainerOfType(c.getReceiver(), Function.class).isEnabled()) && _self.getSubFunctions().contains(EObjectUtil.<Function>eContainerOfType(c.getReceiver(), Function.class))));
          };
          final List<FunctionConnector> connectors = IterableExtensions.<FunctionConnector>toList(IterableExtensions.<FunctionConnector>filter(allconnectors, _function_2));
          for (final FunctionConnector c : connectors) {
            Value _currentValue = inPort.getCurrentValue();
            boolean _tripleNotEquals = (_currentValue != null);
            if (_tripleNotEquals) {
              FunctionBlockDataPortAspect.assign(FunctionPortAspect.getTargetDataPort(c.getReceiver()), inPort.getCurrentValue());
            } else {
              String _qualifiedName = EObjectAspect.getQualifiedName(inPort);
              String _plus = ("In port " + _qualifiedName);
              String _plus_1 = (_plus + " has no value. Please verify that your model assigns a value to it or defines a default value.");
              EObjectAspect.error(_self, _plus_1);
              String _qualifiedName_1 = EObjectAspect.getQualifiedName(inPort);
              String _plus_2 = ("Cannot assign null value from Out port " + _qualifiedName_1);
              String _plus_3 = (_plus_2 + " to In port ");
              String _qualifiedName_2 = EObjectAspect.getQualifiedName(c.getReceiver());
              String _plus_4 = (_plus_3 + _qualifiedName_2);
              throw new FCLException(_plus_4);
            }
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static void _privk3_sendDataToActiveNonDelayedPorts(final FunctionAspectFunctionAspectProperties _self_, final Function _self) {
    try {
      final Function1<FunctionPort, Boolean> _function = (FunctionPort p) -> {
        return Boolean.valueOf((Objects.equal(p.getDirection(), DirectionKind.OUT) || Objects.equal(p.getDirection(), DirectionKind.IN_OUT)));
      };
      final Function1<FunctionPort, FunctionBlockDataPort> _function_1 = (FunctionPort pp) -> {
        return FunctionPortAspect.getTargetDataPort(pp);
      };
      final List<FunctionBlockDataPort> outputPorts = IterableExtensions.<FunctionBlockDataPort>toList(IterableExtensions.<FunctionPort, FunctionBlockDataPort>map(IterableExtensions.<FunctionPort>filter(_self.getFunctionPorts(), _function), _function_1));
      for (final FunctionBlockDataPort outPort : outputPorts) {
        {
          final Function1<FunctionConnector, Boolean> _function_2 = (FunctionConnector c) -> {
            return Boolean.valueOf((((!c.isDelayed()) && Objects.equal(c.getEmitter(), outPort)) && EObjectUtil.<Function>eContainerOfType(c.getReceiver(), Function.class).isEnabled()));
          };
          final List<FunctionConnector> connectors = IterableExtensions.<FunctionConnector>toList(IterableExtensions.<FunctionConnector>filter(EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).allConnectors(), _function_2));
          for (final FunctionConnector c : connectors) {
            Value _currentValue = outPort.getCurrentValue();
            boolean _tripleNotEquals = (_currentValue != null);
            if (_tripleNotEquals) {
              FunctionBlockDataPortAspect.assign(FunctionPortAspect.getTargetDataPort(c.getReceiver()), outPort.getCurrentValue());
            } else {
              String _qualifiedName = EObjectAspect.getQualifiedName(outPort);
              String _plus = ("Out port " + _qualifiedName);
              String _plus_1 = (_plus + " has no value. Please verify that your model assigns a value to it or defines a default value.");
              EObjectAspect.error(_self, _plus_1);
              String _qualifiedName_1 = EObjectAspect.getQualifiedName(outPort);
              String _plus_2 = ("Cannot assign null value from Out port " + _qualifiedName_1);
              String _plus_3 = (_plus_2 + " to In port ");
              String _qualifiedName_2 = EObjectAspect.getQualifiedName(c.getReceiver());
              String _plus_4 = (_plus_3 + _qualifiedName_2);
              throw new FCLException(_plus_4);
            }
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static boolean _privk3_isEnabled(final FunctionAspectFunctionAspectProperties _self_, final Function _self) {
    EObject _eContainer = _self.eContainer();
    if ((_eContainer instanceof FCLModel)) {
      return true;
    } else {
      return EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getModeAutomata().getCurrentMode().getEnabledFunctions().contains(_self);
    }
  }
  
  protected static String _privk3_functionName(final FunctionAspectFunctionAspectProperties _self_, final Function _self) {
    EObject _eContainer = _self.eContainer();
    if ((_eContainer instanceof FCLModel)) {
      return "MasterFunction";
    } else {
      return _self.getName();
    }
  }
  
  protected static List<Function> _privk3_sortedSubfunctions(final FunctionAspectFunctionAspectProperties _self_, final Function _self) {
    try {
      final List<Function> resList = CollectionLiterals.<Function>newArrayList();
      final Function1<FunctionConnector, Boolean> _function = (FunctionConnector connector) -> {
        return Boolean.valueOf((((((!connector.isDelayed()) && _self.getSubFunctions().contains(EObjectUtil.<Function>eContainerOfType(connector.getEmitter(), Function.class))) && _self.getSubFunctions().contains(EObjectUtil.<Function>eContainerOfType(connector.getReceiver(), Function.class))) && EObjectUtil.<Function>eContainerOfType(connector.getEmitter(), Function.class).isEnabled()) && EObjectUtil.<Function>eContainerOfType(connector.getReceiver(), Function.class).isEnabled()));
      };
      final List<FunctionConnector> consideredConnectorsSequence = IterableExtensions.<FunctionConnector>toList(IterableExtensions.<FunctionConnector>filter(_self.getFunctionConnectors(), _function));
      final List<Function> nodeWithNoIncomingEdgeSet = CollectionLiterals.<Function>newArrayList();
      final Function1<FunctionConnector, Function> _function_1 = (FunctionConnector fc) -> {
        return EObjectUtil.<Function>eContainerOfType(fc.getReceiver(), Function.class);
      };
      final Function1<Function, Boolean> _function_2 = (Function fc2) -> {
        return Boolean.valueOf(fc2.isEnabled());
      };
      final List<Function> functionsWithIncomingEdge = IterableExtensions.<Function>toList(IterableExtensions.<Function>filter(ListExtensions.<FunctionConnector, Function>map(consideredConnectorsSequence, _function_1), _function_2));
      EList<Function> _subFunctions = _self.getSubFunctions();
      for (final Function f : _subFunctions) {
        boolean _contains = functionsWithIncomingEdge.contains(f);
        boolean _not = (!_contains);
        if (_not) {
          boolean _isEnabled = f.isEnabled();
          if (_isEnabled) {
            nodeWithNoIncomingEdgeSet.add(f);
          }
        }
      }
      while ((!nodeWithNoIncomingEdgeSet.isEmpty())) {
        {
          final Function n = nodeWithNoIncomingEdgeSet.get(0);
          nodeWithNoIncomingEdgeSet.remove(0);
          resList.add(n);
          final Function1<FunctionConnector, Boolean> _function_3 = (FunctionConnector c) -> {
            Function _eContainerOfType = EObjectUtil.<Function>eContainerOfType(c.getEmitter(), Function.class);
            return Boolean.valueOf(Objects.equal(_eContainerOfType, n));
          };
          final List<FunctionConnector> connectorEmittedByN = IterableExtensions.<FunctionConnector>toList(IterableExtensions.<FunctionConnector>filter(consideredConnectorsSequence, _function_3));
          for (final FunctionConnector fc : connectorEmittedByN) {
            {
              consideredConnectorsSequence.remove(fc);
              final Function m = EObjectUtil.<Function>eContainerOfType(fc.getReceiver(), Function.class);
              final Function1<FunctionConnector, Boolean> _function_4 = (FunctionConnector fc1) -> {
                Function _eContainerOfType = EObjectUtil.<Function>eContainerOfType(fc1.getReceiver(), Function.class);
                return Boolean.valueOf(Objects.equal(_eContainerOfType, m));
              };
              boolean _exists = IterableExtensions.<FunctionConnector>exists(consideredConnectorsSequence, _function_4);
              boolean _not_1 = (!_exists);
              if (_not_1) {
                nodeWithNoIncomingEdgeSet.add(m);
              }
            }
          }
        }
      }
      boolean _isEmpty = consideredConnectorsSequence.isEmpty();
      boolean _not_1 = (!_isEmpty);
      if (_not_1) {
        String _functionName = FunctionAspect.functionName(_self);
        String _plus = ("Found at least one cycle in sub-functions of " + _functionName);
        EObjectAspect.error(_self, _plus);
        final Function1<FunctionConnector, String> _function_3 = (FunctionConnector s) -> {
          String _qualifiedName = EObjectAspect.getQualifiedName(s.getEmitter());
          String _plus_1 = (_qualifiedName + " -> ");
          String _qualifiedName_1 = EObjectAspect.getQualifiedName(s.getReceiver());
          return (_plus_1 + _qualifiedName_1);
        };
        String _join = IterableExtensions.join(ListExtensions.<FunctionConnector, String>map(consideredConnectorsSequence, _function_3), ",   \n");
        String _plus_1 = ("Connectors involved in cycle(s) are:\n" + _join);
        EObjectAspect.error(_self, _plus_1);
        throw new FCLException("No Topological order can be found");
      } else {
        return resList;
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
