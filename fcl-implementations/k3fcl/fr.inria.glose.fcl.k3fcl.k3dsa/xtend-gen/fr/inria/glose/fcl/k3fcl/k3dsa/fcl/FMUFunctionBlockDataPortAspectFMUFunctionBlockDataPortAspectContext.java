package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectProperties;
import fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort;
import java.util.Map;

@SuppressWarnings("all")
public class FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectContext {
  public final static FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectContext INSTANCE = new FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectContext();
  
  public static FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectProperties getSelf(final FMUFunctionBlockDataPort _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<FMUFunctionBlockDataPort, FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectProperties>();
  
  public Map<FMUFunctionBlockDataPort, FMUFunctionBlockDataPortAspectFMUFunctionBlockDataPortAspectProperties> getMap() {
    return map;
  }
}
