package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspectUnityFunctionAspectProperties;
import fr.inria.glose.fcl.model.fcl.UnityFunction;
import java.util.Map;

@SuppressWarnings("all")
public class UnityFunctionAspectUnityFunctionAspectContext {
  public final static UnityFunctionAspectUnityFunctionAspectContext INSTANCE = new UnityFunctionAspectUnityFunctionAspectContext();
  
  public static UnityFunctionAspectUnityFunctionAspectProperties getSelf(final UnityFunction _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspectUnityFunctionAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<UnityFunction, UnityFunctionAspectUnityFunctionAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.UnityFunction, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspectUnityFunctionAspectProperties>();
  
  public Map<UnityFunction, UnityFunctionAspectUnityFunctionAspectProperties> getMap() {
    return map;
  }
}
