package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueTypeAspectIntegerValueTypeAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueTypeAspect;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.IntegerValue;
import fr.inria.glose.fcl.model.fcl.IntegerValueType;
import fr.inria.glose.fcl.model.fcl.Value;

@Aspect(className = IntegerValueType.class)
@SuppressWarnings("all")
public class IntegerValueTypeAspect extends ValueTypeAspect {
  public static Value createValue(final IntegerValueType _self, final String internalValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueTypeAspectIntegerValueTypeAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueTypeAspectIntegerValueTypeAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value createValue(String)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValueType){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueTypeAspect._privk3_createValue(_self_, (fr.inria.glose.fcl.model.fcl.IntegerValueType)_self,internalValue);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static Value _privk3_createValue(final IntegerValueTypeAspectIntegerValueTypeAspectProperties _self_, final IntegerValueType _self, final String internalValue) {
    final IntegerValue aValue = FclFactory.eINSTANCE.createIntegerValue();
    aValue.setIntValue(Integer.parseInt(internalValue));
    return aValue;
  }
}
