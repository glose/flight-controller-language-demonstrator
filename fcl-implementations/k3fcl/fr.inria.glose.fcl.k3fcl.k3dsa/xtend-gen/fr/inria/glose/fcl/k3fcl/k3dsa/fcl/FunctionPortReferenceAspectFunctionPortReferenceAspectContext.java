package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspectFunctionPortReferenceAspectProperties;
import fr.inria.glose.fcl.model.fcl.FunctionPortReference;
import java.util.Map;

@SuppressWarnings("all")
public class FunctionPortReferenceAspectFunctionPortReferenceAspectContext {
  public final static FunctionPortReferenceAspectFunctionPortReferenceAspectContext INSTANCE = new FunctionPortReferenceAspectFunctionPortReferenceAspectContext();
  
  public static FunctionPortReferenceAspectFunctionPortReferenceAspectProperties getSelf(final FunctionPortReference _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspectFunctionPortReferenceAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<FunctionPortReference, FunctionPortReferenceAspectFunctionPortReferenceAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.FunctionPortReference, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspectFunctionPortReferenceAspectProperties>();
  
  public Map<FunctionPortReference, FunctionPortReferenceAspectFunctionPortReferenceAspectProperties> getMap() {
    return map;
  }
}
