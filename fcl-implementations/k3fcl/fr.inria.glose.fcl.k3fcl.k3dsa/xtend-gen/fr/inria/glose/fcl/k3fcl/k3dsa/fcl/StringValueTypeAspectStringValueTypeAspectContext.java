package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueTypeAspectStringValueTypeAspectProperties;
import fr.inria.glose.fcl.model.fcl.StringValueType;
import java.util.Map;

@SuppressWarnings("all")
public class StringValueTypeAspectStringValueTypeAspectContext {
  public final static StringValueTypeAspectStringValueTypeAspectContext INSTANCE = new StringValueTypeAspectStringValueTypeAspectContext();
  
  public static StringValueTypeAspectStringValueTypeAspectProperties getSelf(final StringValueType _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueTypeAspectStringValueTypeAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<StringValueType, StringValueTypeAspectStringValueTypeAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.StringValueType, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueTypeAspectStringValueTypeAspectProperties>();
  
  public Map<StringValueType, StringValueTypeAspectStringValueTypeAspectProperties> getMap() {
    return map;
  }
}
