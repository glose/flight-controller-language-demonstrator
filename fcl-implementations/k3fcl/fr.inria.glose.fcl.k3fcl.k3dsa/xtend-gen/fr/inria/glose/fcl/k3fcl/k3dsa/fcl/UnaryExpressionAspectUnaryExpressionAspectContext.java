package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnaryExpressionAspectUnaryExpressionAspectProperties;
import fr.inria.glose.fcl.model.fcl.UnaryExpression;
import java.util.Map;

@SuppressWarnings("all")
public class UnaryExpressionAspectUnaryExpressionAspectContext {
  public final static UnaryExpressionAspectUnaryExpressionAspectContext INSTANCE = new UnaryExpressionAspectUnaryExpressionAspectContext();
  
  public static UnaryExpressionAspectUnaryExpressionAspectProperties getSelf(final UnaryExpression _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnaryExpressionAspectUnaryExpressionAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<UnaryExpression, UnaryExpressionAspectUnaryExpressionAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.UnaryExpression, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnaryExpressionAspectUnaryExpressionAspectProperties>();
  
  public Map<UnaryExpression, UnaryExpressionAspectUnaryExpressionAspectProperties> getMap() {
    return map;
  }
}
