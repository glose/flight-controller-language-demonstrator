package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallPreviousAspectCallPreviousAspectProperties;
import fr.inria.glose.fcl.model.fcl.CallPrevious;
import java.util.Map;

@SuppressWarnings("all")
public class CallPreviousAspectCallPreviousAspectContext {
  public final static CallPreviousAspectCallPreviousAspectContext INSTANCE = new CallPreviousAspectCallPreviousAspectContext();
  
  public static CallPreviousAspectCallPreviousAspectProperties getSelf(final CallPrevious _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallPreviousAspectCallPreviousAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<CallPrevious, CallPreviousAspectCallPreviousAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.CallPrevious, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallPreviousAspectCallPreviousAspectProperties>();
  
  public Map<CallPrevious, CallPreviousAspectCallPreviousAspectProperties> getMap() {
    return map;
  }
}
