package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAspectDataStructPropertyAspectProperties;
import fr.inria.glose.fcl.model.fcl.DataStructProperty;
import java.util.Map;

@SuppressWarnings("all")
public class DataStructPropertyAspectDataStructPropertyAspectContext {
  public final static DataStructPropertyAspectDataStructPropertyAspectContext INSTANCE = new DataStructPropertyAspectDataStructPropertyAspectContext();
  
  public static DataStructPropertyAspectDataStructPropertyAspectProperties getSelf(final DataStructProperty _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAspectDataStructPropertyAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<DataStructProperty, DataStructPropertyAspectDataStructPropertyAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.DataStructProperty, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAspectDataStructPropertyAspectProperties>();
  
  public Map<DataStructProperty, DataStructPropertyAspectDataStructPropertyAspectProperties> getMap() {
    return map;
  }
}
