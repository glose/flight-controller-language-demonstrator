package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectProperties;
import fr.inria.glose.fcl.model.fcl.StructValue;
import java.util.Map;

@SuppressWarnings("all")
public class StructValueAspectStructValueAspectContext {
  public final static StructValueAspectStructValueAspectContext INSTANCE = new StructValueAspectStructValueAspectContext();
  
  public static StructValueAspectStructValueAspectProperties getSelf(final StructValue _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<StructValue, StructValueAspectStructValueAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.StructValue, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectProperties>();
  
  public Map<StructValue, StructValueAspectStructValueAspectProperties> getMap() {
    return map;
  }
}
