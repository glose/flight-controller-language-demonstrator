package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignmentAspectAssignmentAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.PrimitiveActionAspect;
import fr.inria.glose.fcl.model.fcl.Assignable;
import fr.inria.glose.fcl.model.fcl.Assignment;

@Aspect(className = Assignment.class)
@SuppressWarnings("all")
public class AssignmentAspect extends PrimitiveActionAspect {
  public static void doAction(final Assignment _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignmentAspectAssignmentAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignmentAspectAssignmentAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void doAction()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Assignment){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignmentAspect._privk3_doAction(_self_, (fr.inria.glose.fcl.model.fcl.Assignment)_self);
    };
  }
  
  protected static void _privk3_doAction(final AssignmentAspectAssignmentAspectProperties _self_, final Assignment _self) {
    final Assignable target = _self.getAssignable();
    AssignableAspect.assign(target, ExpressionAspect.evaluate(_self.getExpression()));
  }
}
