package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDeclarationReferenceAspectCallDeclarationReferenceAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect;
import fr.inria.glose.fcl.model.fcl.CallDeclarationReference;
import fr.inria.glose.fcl.model.fcl.Value;

@Aspect(className = CallDeclarationReference.class)
@SuppressWarnings("all")
public class CallDeclarationReferenceAspect extends CallAspect {
  public static Value evaluate(final CallDeclarationReference _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDeclarationReferenceAspectCallDeclarationReferenceAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDeclarationReferenceAspectCallDeclarationReferenceAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.CallDeclarationReference){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDeclarationReferenceAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.CallDeclarationReference)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static Value _privk3_evaluate(final CallDeclarationReferenceAspectCallDeclarationReferenceAspectProperties _self_, final CallDeclarationReference _self) {
    return CallableDeclarationAspect.call(_self.getCallable());
  }
}
