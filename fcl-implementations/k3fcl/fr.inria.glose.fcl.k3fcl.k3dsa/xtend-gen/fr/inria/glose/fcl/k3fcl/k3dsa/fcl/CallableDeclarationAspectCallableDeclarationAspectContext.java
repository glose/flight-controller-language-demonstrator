package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspectCallableDeclarationAspectProperties;
import fr.inria.glose.fcl.model.fcl.CallableDeclaration;
import java.util.Map;

@SuppressWarnings("all")
public class CallableDeclarationAspectCallableDeclarationAspectContext {
  public final static CallableDeclarationAspectCallableDeclarationAspectContext INSTANCE = new CallableDeclarationAspectCallableDeclarationAspectContext();
  
  public static CallableDeclarationAspectCallableDeclarationAspectProperties getSelf(final CallableDeclaration _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspectCallableDeclarationAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<CallableDeclaration, CallableDeclarationAspectCallableDeclarationAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.CallableDeclaration, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspectCallableDeclarationAspectProperties>();
  
  public Map<CallableDeclaration, CallableDeclarationAspectCallableDeclarationAspectProperties> getMap() {
    return map;
  }
}
