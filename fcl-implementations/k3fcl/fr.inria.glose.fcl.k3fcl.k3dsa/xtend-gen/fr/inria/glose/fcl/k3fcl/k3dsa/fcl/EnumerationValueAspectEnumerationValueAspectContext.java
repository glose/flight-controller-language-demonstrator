package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspectEnumerationValueAspectProperties;
import fr.inria.glose.fcl.model.fcl.EnumerationValue;
import java.util.Map;

@SuppressWarnings("all")
public class EnumerationValueAspectEnumerationValueAspectContext {
  public final static EnumerationValueAspectEnumerationValueAspectContext INSTANCE = new EnumerationValueAspectEnumerationValueAspectContext();
  
  public static EnumerationValueAspectEnumerationValueAspectProperties getSelf(final EnumerationValue _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspectEnumerationValueAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<EnumerationValue, EnumerationValueAspectEnumerationValueAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.EnumerationValue, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspectEnumerationValueAspectProperties>();
  
  public Map<EnumerationValue, EnumerationValueAspectEnumerationValueAspectProperties> getMap() {
    return map;
  }
}
