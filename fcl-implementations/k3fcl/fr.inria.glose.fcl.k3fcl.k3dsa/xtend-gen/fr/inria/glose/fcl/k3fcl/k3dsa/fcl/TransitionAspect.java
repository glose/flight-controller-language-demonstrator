package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import com.google.common.base.Objects;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.diverse.k3.al.annotationprocessor.Step;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.TransitionAspectTransitionAspectProperties;
import fr.inria.glose.fcl.model.fcl.Event;
import fr.inria.glose.fcl.model.fcl.Expression;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.ModeAutomata;
import fr.inria.glose.fcl.model.fcl.Transition;
import org.eclipse.gemoc.commons.eclipse.emf.EObjectUtil;

@Aspect(className = Transition.class)
@SuppressWarnings("all")
public class TransitionAspect {
  public static boolean evalGuard(final Transition _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.TransitionAspectTransitionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.TransitionAspectTransitionAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# boolean evalGuard()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Transition){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.TransitionAspect._privk3_evalGuard(_self_, (fr.inria.glose.fcl.model.fcl.Transition)_self);
    };
    return (boolean)result;
  }
  
  public static boolean evalEvent(final Transition _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.TransitionAspectTransitionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.TransitionAspectTransitionAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# boolean evalEvent()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Transition){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.TransitionAspect._privk3_evalEvent(_self_, (fr.inria.glose.fcl.model.fcl.Transition)_self);
    };
    return (boolean)result;
  }
  
  @Step
  public static void fire(final Transition _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.TransitionAspectTransitionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.TransitionAspectTransitionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void fire()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Transition){
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand command = new fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand() {
    		@Override
    		public void execute() {
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.TransitionAspect._privk3_fire(_self_, (fr.inria.glose.fcl.model.fcl.Transition)_self);
    		}
    	};
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager stepManager = fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepManagerRegistry.getInstance().findStepManager(_self);
    	if (stepManager != null) {
    		stepManager.executeStep(_self, new Object[] {_self}, command, "Transition", "fire");
    	} else {
    		command.execute();
    	}
    	;
    };
  }
  
  protected static boolean _privk3_evalGuard(final TransitionAspectTransitionAspectProperties _self_, final Transition _self) {
    boolean result = true;
    Expression _guard = _self.getGuard();
    boolean _tripleEquals = (_guard == null);
    if (_tripleEquals) {
      result = true;
    } else {
      result = ExpressionAspect.evaluateAsBoolean(_self.getGuard());
    }
    return result;
  }
  
  protected static boolean _privk3_evalEvent(final TransitionAspectTransitionAspectProperties _self_, final Transition _self) {
    final FCLModel fclModel = EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class);
    boolean result = false;
    Event _event = _self.getEvent();
    boolean _tripleEquals = (_event == null);
    if (_tripleEquals) {
      result = true;
    } else {
      int _size = fclModel.getReceivedEvents().size();
      boolean _greaterThan = (_size > 0);
      if (_greaterThan) {
        String _name = fclModel.getReceivedEvents().get(0).getEvent().getName();
        String _name_1 = _self.getEvent().getName();
        boolean _equals = Objects.equal(_name, _name_1);
        result = _equals;
      } else {
        result = false;
      }
    }
    return result;
  }
  
  protected static void _privk3_fire(final TransitionAspectTransitionAspectProperties _self_, final Transition _self) {
    final ModeAutomata fsm = EObjectUtil.<ModeAutomata>eContainerOfType(_self.getSource(), ModeAutomata.class);
    final FCLModel fclModel = EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class);
    String _name = _self.getName();
    String _plus = ("Firing " + _name);
    String _plus_1 = (_plus + " and entering ");
    String _name_1 = _self.getTarget().getName();
    String _plus_2 = (_plus_1 + _name_1);
    EObjectAspect.important(_self, _plus_2);
    fsm.setCurrentMode(_self.getTarget());
    Event _event = _self.getEvent();
    boolean _tripleNotEquals = (_event != null);
    if (_tripleNotEquals) {
      FCLModelAspect.consumeEventOccurence(fclModel);
    }
  }
}
