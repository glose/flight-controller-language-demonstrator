package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NotImplementedException;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureAspectProcedureAspectProperties;
import fr.inria.glose.fcl.model.fcl.Procedure;
import org.eclipse.xtext.xbase.lib.Exceptions;

@Aspect(className = Procedure.class)
@SuppressWarnings("all")
public abstract class ProcedureAspect {
  public static void runProcedure(final Procedure _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureAspectProcedureAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureAspectProcedureAspectContext.getSelf(_self);
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureAspect#void runProcedure() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.JavaProcedureAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.JavaProcedure){
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.JavaProcedureAspect.runProcedure((fr.inria.glose.fcl.model.fcl.JavaProcedure)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureAspect#void runProcedure() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.JavaProcedureAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureAspect#void runProcedure() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLProcedure){
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureAspect.runProcedure((fr.inria.glose.fcl.model.fcl.FCLProcedure)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureAspect#void runProcedure() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureAspect
    // #DispatchPointCut_before# void runProcedure()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Procedure){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureAspect._privk3_runProcedure(_self_, (fr.inria.glose.fcl.model.fcl.Procedure)_self);
    };
  }
  
  protected static void _privk3_runProcedure(final ProcedureAspectProcedureAspectProperties _self_, final Procedure _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement runProcedure() for " + _self));
      throw new NotImplementedException(("not implemented, please implement runProcedure() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
