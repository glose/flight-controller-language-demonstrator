package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionBlockAspectActionBlockAspectProperties;
import fr.inria.glose.fcl.model.fcl.ActionBlock;
import java.util.Map;

@SuppressWarnings("all")
public class ActionBlockAspectActionBlockAspectContext {
  public final static ActionBlockAspectActionBlockAspectContext INSTANCE = new ActionBlockAspectActionBlockAspectContext();
  
  public static ActionBlockAspectActionBlockAspectProperties getSelf(final ActionBlock _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionBlockAspectActionBlockAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<ActionBlock, ActionBlockAspectActionBlockAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.ActionBlock, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionBlockAspectActionBlockAspectProperties>();
  
  public Map<ActionBlock, ActionBlockAspectActionBlockAspectProperties> getMap() {
    return map;
  }
}
