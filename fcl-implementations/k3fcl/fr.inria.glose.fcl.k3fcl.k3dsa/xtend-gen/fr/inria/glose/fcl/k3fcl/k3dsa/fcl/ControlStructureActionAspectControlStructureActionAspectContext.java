package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ControlStructureActionAspectControlStructureActionAspectProperties;
import fr.inria.glose.fcl.model.fcl.ControlStructureAction;
import java.util.Map;

@SuppressWarnings("all")
public class ControlStructureActionAspectControlStructureActionAspectContext {
  public final static ControlStructureActionAspectControlStructureActionAspectContext INSTANCE = new ControlStructureActionAspectControlStructureActionAspectContext();
  
  public static ControlStructureActionAspectControlStructureActionAspectProperties getSelf(final ControlStructureAction _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ControlStructureActionAspectControlStructureActionAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<ControlStructureAction, ControlStructureActionAspectControlStructureActionAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.ControlStructureAction, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ControlStructureActionAspectControlStructureActionAspectProperties>();
  
  public Map<ControlStructureAction, ControlStructureActionAspectControlStructureActionAspectProperties> getMap() {
    return map;
  }
}
