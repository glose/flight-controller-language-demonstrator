package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionBlockAspectActionBlockAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ControlStructureActionAspect;
import fr.inria.glose.fcl.model.fcl.Action;
import fr.inria.glose.fcl.model.fcl.ActionBlock;
import org.eclipse.emf.common.util.EList;

@Aspect(className = ActionBlock.class)
@SuppressWarnings("all")
public class ActionBlockAspect extends ControlStructureActionAspect {
  public static void doAction(final ActionBlock _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionBlockAspectActionBlockAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionBlockAspectActionBlockAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void doAction()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.ActionBlock){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionBlockAspect._privk3_doAction(_self_, (fr.inria.glose.fcl.model.fcl.ActionBlock)_self);
    };
  }
  
  protected static void _privk3_doAction(final ActionBlockAspectActionBlockAspectProperties _self_, final ActionBlock _self) {
    EList<Action> _actions = _self.getActions();
    for (final Action action : _actions) {
      ActionAspect.doAction(action);
    }
  }
}
