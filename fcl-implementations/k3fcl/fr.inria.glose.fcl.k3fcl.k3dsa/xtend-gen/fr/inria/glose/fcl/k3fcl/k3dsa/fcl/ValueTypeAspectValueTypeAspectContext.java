package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueTypeAspectValueTypeAspectProperties;
import fr.inria.glose.fcl.model.fcl.ValueType;
import java.util.Map;

@SuppressWarnings("all")
public class ValueTypeAspectValueTypeAspectContext {
  public final static ValueTypeAspectValueTypeAspectContext INSTANCE = new ValueTypeAspectValueTypeAspectContext();
  
  public static ValueTypeAspectValueTypeAspectProperties getSelf(final ValueType _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueTypeAspectValueTypeAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<ValueType, ValueTypeAspectValueTypeAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.ValueType, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueTypeAspectValueTypeAspectProperties>();
  
  public Map<ValueType, ValueTypeAspectValueTypeAspectProperties> getMap() {
    return map;
  }
}
