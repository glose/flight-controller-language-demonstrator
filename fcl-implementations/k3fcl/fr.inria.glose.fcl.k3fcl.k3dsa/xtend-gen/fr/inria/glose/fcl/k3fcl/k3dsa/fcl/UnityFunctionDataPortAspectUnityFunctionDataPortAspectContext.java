package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionDataPortAspectUnityFunctionDataPortAspectProperties;
import fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort;
import java.util.Map;

@SuppressWarnings("all")
public class UnityFunctionDataPortAspectUnityFunctionDataPortAspectContext {
  public final static UnityFunctionDataPortAspectUnityFunctionDataPortAspectContext INSTANCE = new UnityFunctionDataPortAspectUnityFunctionDataPortAspectContext();
  
  public static UnityFunctionDataPortAspectUnityFunctionDataPortAspectProperties getSelf(final UnityFunctionDataPort _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionDataPortAspectUnityFunctionDataPortAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<UnityFunctionDataPort, UnityFunctionDataPortAspectUnityFunctionDataPortAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionDataPortAspectUnityFunctionDataPortAspectProperties>();
  
  public Map<UnityFunctionDataPort, UnityFunctionDataPortAspectUnityFunctionDataPortAspectProperties> getMap() {
    return map;
  }
}
