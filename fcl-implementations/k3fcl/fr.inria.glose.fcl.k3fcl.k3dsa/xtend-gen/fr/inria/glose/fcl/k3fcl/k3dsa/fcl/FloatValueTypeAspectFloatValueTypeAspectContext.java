package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueTypeAspectFloatValueTypeAspectProperties;
import fr.inria.glose.fcl.model.fcl.FloatValueType;
import java.util.Map;

@SuppressWarnings("all")
public class FloatValueTypeAspectFloatValueTypeAspectContext {
  public final static FloatValueTypeAspectFloatValueTypeAspectContext INSTANCE = new FloatValueTypeAspectFloatValueTypeAspectContext();
  
  public static FloatValueTypeAspectFloatValueTypeAspectProperties getSelf(final FloatValueType _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueTypeAspectFloatValueTypeAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<FloatValueType, FloatValueTypeAspectFloatValueTypeAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.FloatValueType, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueTypeAspectFloatValueTypeAspectProperties>();
  
  public Map<FloatValueType, FloatValueTypeAspectFloatValueTypeAspectProperties> getMap() {
    return map;
  }
}
