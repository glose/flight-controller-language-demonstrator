package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspectDataTypeAspectProperties;
import fr.inria.glose.fcl.model.fcl.DataType;
import java.util.Map;

@SuppressWarnings("all")
public class DataTypeAspectDataTypeAspectContext {
  public final static DataTypeAspectDataTypeAspectContext INSTANCE = new DataTypeAspectDataTypeAspectContext();
  
  public static DataTypeAspectDataTypeAspectProperties getSelf(final DataType _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspectDataTypeAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<DataType, DataTypeAspectDataTypeAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.DataType, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspectDataTypeAspectProperties>();
  
  public Map<DataType, DataTypeAspectDataTypeAspectProperties> getMap() {
    return map;
  }
}
