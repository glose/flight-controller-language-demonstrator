package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspectAssignableAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NotImplementedException;
import fr.inria.glose.fcl.model.fcl.Assignable;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.xtext.xbase.lib.Exceptions;

@Aspect(className = Assignable.class)
@SuppressWarnings("all")
public abstract class AssignableAspect {
  public static void assign(final Assignable _self, final Value newValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspectAssignableAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspectAssignableAspectContext.getSelf(_self);
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect#void assign(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect.assign((fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self,newValue);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect#void assign(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect#void assign(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.VarDecl){
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect.assign((fr.inria.glose.fcl.model.fcl.VarDecl)_self,newValue);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect#void assign(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect#void assign(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionVarDecl){
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect.assign((fr.inria.glose.fcl.model.fcl.FunctionVarDecl)_self,newValue);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect#void assign(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect
    // #DispatchPointCut_before# void assign(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Assignable){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect._privk3_assign(_self_, (fr.inria.glose.fcl.model.fcl.Assignable)_self,newValue);
    };
  }
  
  public static DataType getResolvedType(final Assignable _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspectAssignableAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspectAssignableAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.ProcedureParameter){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect.getResolvedType((fr.inria.glose.fcl.model.fcl.ProcedureParameter)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionVarDecl){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect.getResolvedType((fr.inria.glose.fcl.model.fcl.FunctionVarDecl)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionPortReference){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspect.getResolvedType((fr.inria.glose.fcl.model.fcl.FunctionPortReference)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect.getResolvedType((fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.VarDecl){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect.getResolvedType((fr.inria.glose.fcl.model.fcl.VarDecl)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect#DataType getResolvedType() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect
    // #DispatchPointCut_before# DataType getResolvedType()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Assignable){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect._privk3_getResolvedType(_self_, (fr.inria.glose.fcl.model.fcl.Assignable)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.DataType)result;
  }
  
  protected static void _privk3_assign(final AssignableAspectAssignableAspectProperties _self_, final Assignable _self, final Value newValue) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement assign() for " + _self));
      throw new NotImplementedException(("not implemented, please implement assign() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static DataType _privk3_getResolvedType(final AssignableAspectAssignableAspectProperties _self_, final Assignable _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement getResolvedType() for " + _self));
      throw new NotImplementedException(("not implemented, please implement getResolvedType() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
