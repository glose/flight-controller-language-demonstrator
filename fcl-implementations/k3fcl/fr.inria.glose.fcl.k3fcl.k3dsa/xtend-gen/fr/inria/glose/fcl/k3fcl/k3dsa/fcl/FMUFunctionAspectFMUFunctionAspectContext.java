package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectProperties;
import fr.inria.glose.fcl.model.fcl.FMUFunction;
import java.util.Map;

@SuppressWarnings("all")
public class FMUFunctionAspectFMUFunctionAspectContext {
  public final static FMUFunctionAspectFMUFunctionAspectContext INSTANCE = new FMUFunctionAspectFMUFunctionAspectContext();
  
  public static FMUFunctionAspectFMUFunctionAspectProperties getSelf(final FMUFunction _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<FMUFunction, FMUFunctionAspectFMUFunctionAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.FMUFunction, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspectFMUFunctionAspectProperties>();
  
  public Map<FMUFunction, FMUFunctionAspectFMUFunctionAspectProperties> getMap() {
    return map;
  }
}
