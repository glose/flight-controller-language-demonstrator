package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationLiteralAspectEnumerationLiteralAspectProperties;
import fr.inria.glose.fcl.model.fcl.EnumerationLiteral;
import fr.inria.glose.fcl.model.fcl.EnumerationValue;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.Value;

@Aspect(className = EnumerationLiteral.class)
@SuppressWarnings("all")
public class EnumerationLiteralAspect extends CallableDeclarationAspect {
  /**
   * BE CAREFUL :
   * 
   * This class has more than one superclass
   * please specify which parent you want with the 'super' expected calling
   */
  public static Value call(final EnumerationLiteral _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationLiteralAspectEnumerationLiteralAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationLiteralAspectEnumerationLiteralAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value call()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.EnumerationLiteral){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationLiteralAspect._privk3_call(_self_, (fr.inria.glose.fcl.model.fcl.EnumerationLiteral)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static Value _privk3_call(final EnumerationLiteralAspectEnumerationLiteralAspectProperties _self_, final EnumerationLiteral _self) {
    final EnumerationValue eValue = FclFactory.eINSTANCE.createEnumerationValue();
    eValue.setEnumerationValue(_self);
    return eValue;
  }
}
