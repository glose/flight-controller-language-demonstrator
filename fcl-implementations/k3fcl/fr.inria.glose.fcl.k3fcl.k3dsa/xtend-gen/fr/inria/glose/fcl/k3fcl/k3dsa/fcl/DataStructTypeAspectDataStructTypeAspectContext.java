package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructTypeAspectDataStructTypeAspectProperties;
import fr.inria.glose.fcl.model.fcl.DataStructType;
import java.util.Map;

@SuppressWarnings("all")
public class DataStructTypeAspectDataStructTypeAspectContext {
  public final static DataStructTypeAspectDataStructTypeAspectContext INSTANCE = new DataStructTypeAspectDataStructTypeAspectContext();
  
  public static DataStructTypeAspectDataStructTypeAspectProperties getSelf(final DataStructType _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructTypeAspectDataStructTypeAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<DataStructType, DataStructTypeAspectDataStructTypeAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.DataStructType, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructTypeAspectDataStructTypeAspectProperties>();
  
  public Map<DataStructType, DataStructTypeAspectDataStructTypeAspectProperties> getMap() {
    return map;
  }
}
