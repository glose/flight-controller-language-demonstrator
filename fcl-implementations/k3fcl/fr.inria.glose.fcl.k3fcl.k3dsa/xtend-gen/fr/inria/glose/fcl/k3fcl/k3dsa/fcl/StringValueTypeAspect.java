package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueTypeAspectStringValueTypeAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueTypeAspect;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.StringValue;
import fr.inria.glose.fcl.model.fcl.StringValueType;
import fr.inria.glose.fcl.model.fcl.Value;

@Aspect(className = StringValueType.class)
@SuppressWarnings("all")
public class StringValueTypeAspect extends ValueTypeAspect {
  public static Value createValue(final StringValueType _self, final String internalValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueTypeAspectStringValueTypeAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueTypeAspectStringValueTypeAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value createValue(String)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.StringValueType){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueTypeAspect._privk3_createValue(_self_, (fr.inria.glose.fcl.model.fcl.StringValueType)_self,internalValue);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static Value _privk3_createValue(final StringValueTypeAspectStringValueTypeAspectProperties _self_, final StringValueType _self, final String internalValue) {
    final StringValue aValue = FclFactory.eINSTANCE.createStringValue();
    aValue.setStringValue(internalValue);
    return aValue;
  }
}
