package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.BooleanValue;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.FloatValue;
import fr.inria.glose.fcl.model.fcl.FloatValueType;
import fr.inria.glose.fcl.model.fcl.IntegerValue;
import fr.inria.glose.fcl.model.fcl.Value;
import java.util.Locale;

@Aspect(className = FloatValue.class)
@SuppressWarnings("all")
public class FloatValueAspect extends ValueAspect {
  public static Value evaluate(final FloatValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value copy(final FloatValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value copy()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_copy(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static BooleanValue bEquals(final FloatValue _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# BooleanValue bEquals(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_bEquals(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.BooleanValue)result;
  }
  
  public static Value plus(final FloatValue _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value plus(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_plus(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value minus(final FloatValue _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value minus(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_minus(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value uminus(final FloatValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value uminus()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_uminus(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value mult(final FloatValue _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value mult(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_mult(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value div(final FloatValue _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value div(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_div(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value modulo(final FloatValue _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value modulo(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_modulo(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static BooleanValue greater(final FloatValue _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# BooleanValue greater(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_greater(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.BooleanValue)result;
  }
  
  public static BooleanValue greaterOrEquals(final FloatValue _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# BooleanValue greaterOrEquals(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_greaterOrEquals(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.BooleanValue)result;
  }
  
  public static BooleanValue lower(final FloatValue _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# BooleanValue lower(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_lower(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.BooleanValue)result;
  }
  
  public static BooleanValue lowerOrEquals(final FloatValue _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# BooleanValue lowerOrEquals(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_lowerOrEquals(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.BooleanValue)result;
  }
  
  public static FloatValue toFloatValue(final FloatValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# FloatValue toFloatValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_toFloatValue(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.FloatValue)result;
  }
  
  public static IntegerValue toIntegerValue(final FloatValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# IntegerValue toIntegerValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_toIntegerValue(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.IntegerValue)result;
  }
  
  public static String valueToString(final FloatValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# String valueToString()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_valueToString(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self);
    };
    return (java.lang.String)result;
  }
  
  public static Object rawValue(final FloatValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Object rawValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_rawValue(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self);
    };
    return (java.lang.Object)result;
  }
  
  public static Boolean isKindOf(final FloatValue _self, final DataType type) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspectFloatValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Boolean isKindOf(DataType)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect._privk3_isKindOf(_self_, (fr.inria.glose.fcl.model.fcl.FloatValue)_self,type);
    };
    return (java.lang.Boolean)result;
  }
  
  protected static Value _privk3_evaluate(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self) {
    return _self;
  }
  
  protected static Value _privk3_copy(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self) {
    final FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
    aValue.setFloatValue(_self.getFloatValue());
    return aValue;
  }
  
  protected static BooleanValue _privk3_bEquals(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self, final Value rhs) {
    final BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
    double _floatValue = _self.getFloatValue();
    double _floatValue_1 = ValueAspect.toFloatValue(rhs).getFloatValue();
    boolean _equals = (_floatValue == _floatValue_1);
    bValue.setBooleanValue(_equals);
    return bValue;
  }
  
  protected static Value _privk3_plus(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self, final Value rhs) {
    final FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
    double _floatValue = _self.getFloatValue();
    double _floatValue_1 = ValueAspect.toFloatValue(rhs).getFloatValue();
    double _plus = (_floatValue + _floatValue_1);
    aValue.setFloatValue(_plus);
    return aValue;
  }
  
  protected static Value _privk3_minus(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self, final Value rhs) {
    final FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
    double _floatValue = _self.getFloatValue();
    double _floatValue_1 = ValueAspect.toFloatValue(rhs).getFloatValue();
    double _minus = (_floatValue - _floatValue_1);
    aValue.setFloatValue(_minus);
    return aValue;
  }
  
  protected static Value _privk3_uminus(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self) {
    final FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
    double _floatValue = _self.getFloatValue();
    double _minus = (-_floatValue);
    aValue.setFloatValue(_minus);
    return aValue;
  }
  
  protected static Value _privk3_mult(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self, final Value rhs) {
    final FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
    double _floatValue = _self.getFloatValue();
    double _floatValue_1 = ValueAspect.toFloatValue(rhs).getFloatValue();
    double _multiply = (_floatValue * _floatValue_1);
    aValue.setFloatValue(_multiply);
    return aValue;
  }
  
  protected static Value _privk3_div(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self, final Value rhs) {
    final FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
    double _floatValue = _self.getFloatValue();
    double _floatValue_1 = ValueAspect.toFloatValue(rhs).getFloatValue();
    double _divide = (_floatValue / _floatValue_1);
    aValue.setFloatValue(_divide);
    return aValue;
  }
  
  protected static Value _privk3_modulo(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self, final Value rhs) {
    final FloatValue aValue = FclFactory.eINSTANCE.createFloatValue();
    double _floatValue = _self.getFloatValue();
    double _floatValue_1 = ValueAspect.toFloatValue(rhs).getFloatValue();
    double _modulo = (_floatValue % _floatValue_1);
    aValue.setFloatValue(_modulo);
    return aValue;
  }
  
  protected static BooleanValue _privk3_greater(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self, final Value rhs) {
    final BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
    double _floatValue = _self.getFloatValue();
    double _floatValue_1 = ValueAspect.toFloatValue(rhs).getFloatValue();
    boolean _greaterThan = (_floatValue > _floatValue_1);
    bValue.setBooleanValue(_greaterThan);
    return bValue;
  }
  
  protected static BooleanValue _privk3_greaterOrEquals(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self, final Value rhs) {
    final BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
    double _floatValue = _self.getFloatValue();
    double _floatValue_1 = ValueAspect.toFloatValue(rhs).getFloatValue();
    boolean _greaterEqualsThan = (_floatValue >= _floatValue_1);
    bValue.setBooleanValue(_greaterEqualsThan);
    return bValue;
  }
  
  protected static BooleanValue _privk3_lower(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self, final Value rhs) {
    final BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
    double _floatValue = _self.getFloatValue();
    double _floatValue_1 = ValueAspect.toFloatValue(rhs).getFloatValue();
    boolean _lessThan = (_floatValue < _floatValue_1);
    bValue.setBooleanValue(_lessThan);
    return bValue;
  }
  
  protected static BooleanValue _privk3_lowerOrEquals(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self, final Value rhs) {
    final BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
    double _floatValue = _self.getFloatValue();
    double _floatValue_1 = ValueAspect.toFloatValue(rhs).getFloatValue();
    boolean _lessEqualsThan = (_floatValue <= _floatValue_1);
    bValue.setBooleanValue(_lessEqualsThan);
    return bValue;
  }
  
  protected static FloatValue _privk3_toFloatValue(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self) {
    return _self;
  }
  
  protected static IntegerValue _privk3_toIntegerValue(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self) {
    final IntegerValue iValue = FclFactory.eINSTANCE.createIntegerValue();
    iValue.setIntValue(Long.valueOf(Math.round(_self.getFloatValue())).intValue());
    return iValue;
  }
  
  protected static String _privk3_valueToString(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self) {
    return String.format(Locale.US, "%.3G", Double.valueOf(_self.getFloatValue()));
  }
  
  protected static Object _privk3_rawValue(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self) {
    return Double.valueOf(_self.getFloatValue());
  }
  
  protected static Boolean _privk3_isKindOf(final FloatValueAspectFloatValueAspectProperties _self_, final FloatValue _self, final DataType type) {
    return Boolean.valueOf((type instanceof FloatValueType));
  }
}
