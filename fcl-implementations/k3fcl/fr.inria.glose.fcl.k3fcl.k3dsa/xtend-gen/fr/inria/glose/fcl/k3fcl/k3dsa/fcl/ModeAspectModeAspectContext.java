package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAspectModeAspectProperties;
import fr.inria.glose.fcl.model.fcl.Mode;
import java.util.Map;

@SuppressWarnings("all")
public class ModeAspectModeAspectContext {
  public final static ModeAspectModeAspectContext INSTANCE = new ModeAspectModeAspectContext();
  
  public static ModeAspectModeAspectProperties getSelf(final Mode _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAspectModeAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<Mode, ModeAspectModeAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.Mode, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAspectModeAspectProperties>();
  
  public Map<Mode, ModeAspectModeAspectProperties> getMap() {
    return map;
  }
}
