package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import com.google.common.base.Objects;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspectEnumerationValueAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.BooleanValue;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.EnumerationLiteral;
import fr.inria.glose.fcl.model.fcl.EnumerationValue;
import fr.inria.glose.fcl.model.fcl.EnumerationValueType;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.gemoc.commons.eclipse.emf.EObjectUtil;

@Aspect(className = EnumerationValue.class)
@SuppressWarnings("all")
public class EnumerationValueAspect extends ValueAspect {
  public static Value evaluate(final EnumerationValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspectEnumerationValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspectEnumerationValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.EnumerationValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.EnumerationValue)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value copy(final EnumerationValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspectEnumerationValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspectEnumerationValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value copy()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.EnumerationValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect._privk3_copy(_self_, (fr.inria.glose.fcl.model.fcl.EnumerationValue)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static BooleanValue bEquals(final EnumerationValue _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspectEnumerationValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspectEnumerationValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# BooleanValue bEquals(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.EnumerationValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect._privk3_bEquals(_self_, (fr.inria.glose.fcl.model.fcl.EnumerationValue)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.BooleanValue)result;
  }
  
  public static String valueToString(final EnumerationValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspectEnumerationValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspectEnumerationValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# String valueToString()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.EnumerationValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect._privk3_valueToString(_self_, (fr.inria.glose.fcl.model.fcl.EnumerationValue)_self);
    };
    return (java.lang.String)result;
  }
  
  public static Object rawValue(final EnumerationValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspectEnumerationValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspectEnumerationValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Object rawValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.EnumerationValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect._privk3_rawValue(_self_, (fr.inria.glose.fcl.model.fcl.EnumerationValue)_self);
    };
    return (java.lang.Object)result;
  }
  
  public static Boolean isKindOf(final EnumerationValue _self, final DataType type) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspectEnumerationValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspectEnumerationValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Boolean isKindOf(DataType)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.EnumerationValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect._privk3_isKindOf(_self_, (fr.inria.glose.fcl.model.fcl.EnumerationValue)_self,type);
    };
    return (java.lang.Boolean)result;
  }
  
  protected static Value _privk3_evaluate(final EnumerationValueAspectEnumerationValueAspectProperties _self_, final EnumerationValue _self) {
    return _self;
  }
  
  protected static Value _privk3_copy(final EnumerationValueAspectEnumerationValueAspectProperties _self_, final EnumerationValue _self) {
    final EnumerationValue aValue = FclFactory.eINSTANCE.createEnumerationValue();
    aValue.setEnumerationValue(_self.getEnumerationValue());
    return aValue;
  }
  
  protected static BooleanValue _privk3_bEquals(final EnumerationValueAspectEnumerationValueAspectProperties _self_, final EnumerationValue _self, final Value rhs) {
    final BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
    if ((rhs instanceof EnumerationValue)) {
      EnumerationLiteral _enumerationValue = _self.getEnumerationValue();
      EnumerationLiteral _enumerationValue_1 = ((EnumerationValue) rhs).getEnumerationValue();
      boolean _equals = Objects.equal(_enumerationValue, _enumerationValue_1);
      bValue.setBooleanValue(_equals);
    } else {
      bValue.setBooleanValue(false);
    }
    return bValue;
  }
  
  protected static String _privk3_valueToString(final EnumerationValueAspectEnumerationValueAspectProperties _self_, final EnumerationValue _self) {
    String _name = EObjectUtil.<EnumerationValueType>eContainerOfType(_self.getEnumerationValue(), EnumerationValueType.class).getName();
    String _plus = (_name + 
      "::");
    String _name_1 = _self.getEnumerationValue().getName();
    return (_plus + _name_1);
  }
  
  protected static Object _privk3_rawValue(final EnumerationValueAspectEnumerationValueAspectProperties _self_, final EnumerationValue _self) {
    return _self.getEnumerationValue();
  }
  
  protected static Boolean _privk3_isKindOf(final EnumerationValueAspectEnumerationValueAspectProperties _self_, final EnumerationValue _self, final DataType type) {
    EnumerationValueType _eContainerOfType = EObjectUtil.<EnumerationValueType>eContainerOfType(_self.getEnumerationValue(), EnumerationValueType.class);
    return Boolean.valueOf(Objects.equal(type, _eContainerOfType));
  }
}
