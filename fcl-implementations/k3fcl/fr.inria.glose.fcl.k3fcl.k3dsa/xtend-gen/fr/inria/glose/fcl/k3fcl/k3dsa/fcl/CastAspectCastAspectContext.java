package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CastAspectCastAspectProperties;
import fr.inria.glose.fcl.model.fcl.Cast;
import java.util.Map;

@SuppressWarnings("all")
public class CastAspectCastAspectContext {
  public final static CastAspectCastAspectContext INSTANCE = new CastAspectCastAspectContext();
  
  public static CastAspectCastAspectProperties getSelf(final Cast _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CastAspectCastAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<Cast, CastAspectCastAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.Cast, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CastAspectCastAspectProperties>();
  
  public Map<Cast, CastAspectCastAspectProperties> getMap() {
    return map;
  }
}
