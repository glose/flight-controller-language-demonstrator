package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import java.util.Map;

@SuppressWarnings("all")
public class FunctionBlockDataPortAspectFunctionBlockDataPortAspectContext {
  public final static FunctionBlockDataPortAspectFunctionBlockDataPortAspectContext INSTANCE = new FunctionBlockDataPortAspectFunctionBlockDataPortAspectContext();
  
  public static FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties getSelf(final FunctionBlockDataPort _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<FunctionBlockDataPort, FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties>();
  
  public Map<FunctionBlockDataPort, FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties> getMap() {
    return map;
  }
}
