package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationLiteralAspectEnumerationLiteralAspectProperties;
import fr.inria.glose.fcl.model.fcl.EnumerationLiteral;
import java.util.Map;

@SuppressWarnings("all")
public class EnumerationLiteralAspectEnumerationLiteralAspectContext {
  public final static EnumerationLiteralAspectEnumerationLiteralAspectContext INSTANCE = new EnumerationLiteralAspectEnumerationLiteralAspectContext();
  
  public static EnumerationLiteralAspectEnumerationLiteralAspectProperties getSelf(final EnumerationLiteral _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationLiteralAspectEnumerationLiteralAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<EnumerationLiteral, EnumerationLiteralAspectEnumerationLiteralAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.EnumerationLiteral, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationLiteralAspectEnumerationLiteralAspectProperties>();
  
  public Map<EnumerationLiteral, EnumerationLiteralAspectEnumerationLiteralAspectProperties> getMap() {
    return map;
  }
}
