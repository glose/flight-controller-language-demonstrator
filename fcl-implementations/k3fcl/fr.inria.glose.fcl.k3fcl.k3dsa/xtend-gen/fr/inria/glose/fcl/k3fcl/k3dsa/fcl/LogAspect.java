package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.LogAspectLogAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.PrimitiveActionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.Expression;
import fr.inria.glose.fcl.model.fcl.Log;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@Aspect(className = Log.class)
@SuppressWarnings("all")
public class LogAspect extends PrimitiveActionAspect {
  public static void doAction(final Log _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.LogAspectLogAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.LogAspectLogAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void doAction()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Log){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.LogAspect._privk3_doAction(_self_, (fr.inria.glose.fcl.model.fcl.Log)_self);
    };
  }
  
  protected static void _privk3_doAction(final LogAspectLogAspectProperties _self_, final Log _self) {
    final Function1<Expression, String> _function = (Expression exp) -> {
      return ValueAspect.valueToString(ExpressionAspect.evaluate(exp));
    };
    String _join = IterableExtensions.join(ListExtensions.<Expression, String>map(_self.getExpression(), _function), "");
    String _plus = ("" + _join);
    EObjectAspect.info(_self, _plus);
  }
}
