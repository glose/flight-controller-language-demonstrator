package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.diverse.k3.al.annotationprocessor.InitializeModel;
import fr.inria.diverse.k3.al.annotationprocessor.Main;
import fr.inria.diverse.k3.al.annotationprocessor.ReplaceAspectMethod;
import fr.inria.diverse.k3.al.annotationprocessor.Step;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EventAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspect;
import fr.inria.glose.fcl.model.fcl.DirectionKind;
import fr.inria.glose.fcl.model.fcl.Event;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FMUFunction;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.FunctionPort;
import fr.inria.glose.fcl.model.fcl.ModeAutomata;
import fr.inria.glose.fcl.model.fcl.UnityFunction;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.EventOccurrence;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;
import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.EList;
import org.eclipse.gemoc.commons.eclipse.core.resources.IFileUtils;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@Aspect(className = FCLModel.class)
@SuppressWarnings("all")
public class FCLModelAspect {
  @Main
  public static void main(final FCLModel _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void main()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_main(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self);
    };
  }
  
  @Step
  @ReplaceAspectMethod
  public static void evaluateModeAutomata(final FCLModel _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void evaluateModeAutomata()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand command = new fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand() {
    		@Override
    		public void execute() {
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_evaluateModeAutomata(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self);
    		}
    	};
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager stepManager = fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepManagerRegistry.getInstance().findStepManager(_self);
    	if (stepManager != null) {
    		stepManager.executeStep(_self, new Object[] {_self}, command, "FCLModel", "evaluateModeAutomata");
    	} else {
    		command.execute();
    	}
    	;
    };
  }
  
  @Step
  public static void evaluateDataflow(final FCLModel _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void evaluateDataflow()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand command = new fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand() {
    		@Override
    		public void execute() {
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_evaluateDataflow(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self);
    		}
    	};
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager stepManager = fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepManagerRegistry.getInstance().findStepManager(_self);
    	if (stepManager != null) {
    		stepManager.executeStep(_self, new Object[] {_self}, command, "FCLModel", "evaluateDataflow");
    	} else {
    		command.execute();
    	}
    	;
    };
  }
  
  @ReplaceAspectMethod
  public static void propagateDelayedResults(final FCLModel _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void propagateDelayedResults()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_propagateDelayedResults(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self);
    };
  }
  
  public static void sendDataThroughAllDelayedConnectors(final FCLModel _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void sendDataThroughAllDelayedConnectors()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_sendDataThroughAllDelayedConnectors(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self);
    };
  }
  
  public static void updatePrevValues(final FCLModel _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void updatePrevValues()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_updatePrevValues(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self);
    };
  }
  
  @Step
  @InitializeModel
  public static void initializeModel(final FCLModel _self, final EList<String> input) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void initializeModel(EList<String>)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand command = new fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand() {
    		@Override
    		public void execute() {
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_initializeModel(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self,input);
    		}
    	};
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager stepManager = fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepManagerRegistry.getInstance().findStepManager(_self);
    	if (stepManager != null) {
    		stepManager.executeStep(_self, new Object[] {input}, command, "FCLModel", "initializeModel");
    	} else {
    		command.execute();
    	}
    	;
    };
  }
  
  @Step
  public static void manageInputScenario(final FCLModel _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void manageInputScenario()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand command = new fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand() {
    		@Override
    		public void execute() {
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_manageInputScenario(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self);
    		}
    	};
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager stepManager = fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepManagerRegistry.getInstance().findStepManager(_self);
    	if (stepManager != null) {
    		stepManager.executeStep(_self, new Object[] {_self}, command, "FCLModel", "manageInputScenario");
    	} else {
    		command.execute();
    	}
    	;
    };
  }
  
  public static void consumeEventOccurence(final FCLModel _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void consumeEventOccurence()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_consumeEventOccurence(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self);
    };
  }
  
  public static String pushIndent(final FCLModel _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# String pushIndent()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_pushIndent(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self);
    };
    return (java.lang.String)result;
  }
  
  public static String popIndent(final FCLModel _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# String popIndent()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_popIndent(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self);
    };
    return (java.lang.String)result;
  }
  
  public static List<EventOccurrence> receivedEvents(final FCLModel _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# List<EventOccurrence> receivedEvents()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_receivedEvents(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self);
    };
    return (java.util.List<fr.inria.glose.fcl.model.fcl.interpreter_vm.EventOccurrence>)result;
  }
  
  public static void receivedEvents(final FCLModel _self, final List<EventOccurrence> receivedEvents) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void receivedEvents(List<EventOccurrence>)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_receivedEvents(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self,receivedEvents);
    };
  }
  
  public static List<ProcedureContext> procedureStack(final FCLModel _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# List<ProcedureContext> procedureStack()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_procedureStack(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self);
    };
    return (java.util.List<fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext>)result;
  }
  
  public static void procedureStack(final FCLModel _self, final List<ProcedureContext> procedureStack) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void procedureStack(List<ProcedureContext>)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_procedureStack(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self,procedureStack);
    };
  }
  
  public static String indentation(final FCLModel _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# String indentation()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_indentation(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self);
    };
    return (java.lang.String)result;
  }
  
  public static void indentation(final FCLModel _self, final String indentation) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void indentation(String)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_indentation(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self,indentation);
    };
  }
  
  private static BufferedReader inputScenarioReader(final FCLModel _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# BufferedReader inputScenarioReader()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_inputScenarioReader(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self);
    };
    return (java.io.BufferedReader)result;
  }
  
  private static void inputScenarioReader(final FCLModel _self, final BufferedReader inputScenarioReader) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspectFCLModelAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void inputScenarioReader(BufferedReader)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLModel){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect._privk3_inputScenarioReader(_self_, (fr.inria.glose.fcl.model.fcl.FCLModel)_self,inputScenarioReader);
    };
  }
  
  protected static void _privk3_main(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self) {
    String _name = _self.getName();
    String _plus = ("-> main() " + _name);
    EObjectAspect.devInfo(_self, _plus);
    while ((!_self.getModeAutomata().getCurrentMode().isFinal())) {
      {
        FCLModelAspect.manageInputScenario(_self);
        _self.evaluateModeAutomata();
        FCLModelAspect.evaluateDataflow(_self);
      }
    }
  }
  
  protected static void _privk3_evaluateModeAutomata(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self) {
    ModeAutomata _modeAutomata = _self.getModeAutomata();
    String _plus = ("### Evaluate mode automata " + _modeAutomata);
    EObjectAspect.info(_self, _plus);
    ModeAutomataAspect.evalModeStateMachine(_self.getModeAutomata());
  }
  
  protected static void _privk3_evaluateDataflow(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self) {
    FunctionAspect.evalFunction(_self.getDataflow());
    FunctionAspect.sendDataThroughAllDelayedConnectors(_self.getDataflow());
    FCLModelAspect.updatePrevValues(_self);
  }
  
  protected static void _privk3_propagateDelayedResults(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self) {
    FCLModelAspect.sendDataThroughAllDelayedConnectors(_self);
    FCLModelAspect.updatePrevValues(_self);
  }
  
  protected static void _privk3_sendDataThroughAllDelayedConnectors(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self) {
    EObjectAspect.info(_self, "process delayed connections");
    FunctionAspect.sendDataThroughAllDelayedConnectors(_self.getDataflow());
  }
  
  protected static void _privk3_updatePrevValues(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self) {
    EObjectAspect.info(_self, "update previous values");
    FunctionAspect.updatePrevValues(_self.getDataflow());
  }
  
  protected static void _privk3_initializeModel(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self, final EList<String> input) {
    try {
      String _get = input.get(0);
      String _plus = ("-> initializeModel() input=" + _get);
      EObjectAspect.devInfo(_self, _plus);
      _self.setIndentation("");
      ModeAutomata _modeAutomata = _self.getModeAutomata();
      _modeAutomata.setCurrentMode(_self.getModeAutomata().getInitialMode());
      FunctionAspect.initFunctionVars(_self.getDataflow());
      FunctionAspect.initPortsDefaultValues(_self.getDataflow());
      FunctionAspect.updatePrevValues(_self.getDataflow());
      FCLModelAspect.procedureStack(_self, CollectionLiterals.<ProcedureContext>newArrayList());
      FCLModelAspect.receivedEvents(_self, CollectionLiterals.<EventOccurrence>newArrayList());
      List<FMUFunction> _list = IteratorExtensions.<FMUFunction>toList(Iterators.<FMUFunction>filter(_self.getDataflow().eAllContents(), FMUFunction.class));
      for (final FMUFunction fmuFunction : _list) {
        FMUFunctionAspect.initializeFMURuntime(fmuFunction);
      }
      List<UnityFunction> _list_1 = IteratorExtensions.<UnityFunction>toList(Iterators.<UnityFunction>filter(_self.getDataflow().eAllContents(), UnityFunction.class));
      for (final UnityFunction fmuFunction_1 : _list_1) {
        UnityFunctionAspect.initializeUnity(fmuFunction_1);
      }
      boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(input.get(0));
      boolean _not = (!_isNullOrEmpty);
      if (_not) {
        final IFile f = IFileUtils.getIFileFromWorkspaceOrFileSystem(input.get(0));
        String _oSString = f.getLocation().toOSString();
        FileReader _fileReader = new FileReader(_oSString);
        BufferedReader _bufferedReader = new BufferedReader(_fileReader);
        FCLModelAspect.inputScenarioReader(_self, _bufferedReader);
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static void _privk3_manageInputScenario(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self) {
    try {
      BufferedReader _inputScenarioReader = FCLModelAspect.inputScenarioReader(_self);
      boolean _tripleNotEquals = (_inputScenarioReader != null);
      if (_tripleNotEquals) {
        String line = FCLModelAspect.inputScenarioReader(_self).readLine();
        while (((!StringExtensions.isNullOrEmpty(line)) && line.startsWith("#"))) {
          line = FCLModelAspect.inputScenarioReader(_self).readLine();
        }
        if ((line != null)) {
          final String[] valuesString = line.split(",");
          final Function1<FunctionPort, Boolean> _function = (FunctionPort port) -> {
            DirectionKind _direction = port.getDirection();
            return Boolean.valueOf((_direction != DirectionKind.OUT));
          };
          final List<FunctionBlockDataPort> inPorts = IterableExtensions.<FunctionBlockDataPort>toList(Iterables.<FunctionBlockDataPort>filter(IterableExtensions.<FunctionPort>filter(_self.getDataflow().getFunctionPorts(), _function), FunctionBlockDataPort.class));
          for (int i = 0; (i < ((Object[])Conversions.unwrapArray(inPorts, Object.class)).length); i++) {
            FunctionBlockDataPort _get = inPorts.get(i);
            FunctionBlockDataPortAspect.setValueFromString(_get, valuesString[i]);
          }
          final String[] receivedEvents = IterableExtensions.<String>last(((Iterable<String>)Conversions.doWrapArray(valuesString))).replaceAll("\\[", "").replaceAll("\\]", "").split(",");
          for (final String evtName : receivedEvents) {
            {
              final Function1<Event, Boolean> _function_1 = (Event evt) -> {
                String _name = evt.getName();
                return Boolean.valueOf(Objects.equal(_name, evtName));
              };
              final Event evt = IterableExtensions.<Event>findFirst(_self.getEvents(), _function_1);
              if ((evt != null)) {
                EventAspect.send(evt);
              }
            }
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static void _privk3_consumeEventOccurence(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self) {
    String _name = _self.getReceivedEvents().get(0).getEvent().getName();
    String _plus = ("Consuming event occurrence " + _name);
    EObjectAspect.devInfo(_self, _plus);
    _self.getReceivedEvents().remove(0);
  }
  
  protected static String _privk3_pushIndent(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self) {
    String _indentation = _self.getIndentation();
    boolean _tripleEquals = (_indentation == null);
    if (_tripleEquals) {
      _self.setIndentation("");
    }
    String _indentation_1 = _self.getIndentation();
    String _plus = (_indentation_1 + " ");
    _self.setIndentation(_plus);
    return _self.getIndentation();
  }
  
  protected static String _privk3_popIndent(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self) {
    String result = _self.getIndentation();
    int _length = _self.getIndentation().length();
    boolean _greaterThan = (_length > 1);
    if (_greaterThan) {
      String _indentation = _self.getIndentation();
      int _length_1 = _self.getIndentation().length();
      int _minus = (_length_1 - 1);
      _self.setIndentation(_indentation.substring(1, _minus));
    } else {
      int _length_2 = _self.getIndentation().length();
      boolean _equals = (_length_2 == 1);
      if (_equals) {
        _self.setIndentation("");
      }
    }
    return result;
  }
  
  protected static List<EventOccurrence> _privk3_receivedEvents(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self) {
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("getReceivedEvents") &&
    			m.getParameterTypes().length == 0) {
    				Object ret = m.invoke(_self);
    				if (ret != null) {
    					return (java.util.List) ret;
    				} else {
    					return null;
    				}
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    return _self_.receivedEvents;
  }
  
  protected static void _privk3_receivedEvents(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self, final List<EventOccurrence> receivedEvents) {
    boolean setterCalled = false;
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("setReceivedEvents")
    				&& m.getParameterTypes().length == 1) {
    			m.invoke(_self, receivedEvents);
    			setterCalled = true;
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    if (!setterCalled) {
    	_self_.receivedEvents = receivedEvents;
    }
  }
  
  protected static List<ProcedureContext> _privk3_procedureStack(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self) {
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("getProcedureStack") &&
    			m.getParameterTypes().length == 0) {
    				Object ret = m.invoke(_self);
    				if (ret != null) {
    					return (java.util.List) ret;
    				} else {
    					return null;
    				}
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    return _self_.procedureStack;
  }
  
  protected static void _privk3_procedureStack(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self, final List<ProcedureContext> procedureStack) {
    boolean setterCalled = false;
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("setProcedureStack")
    				&& m.getParameterTypes().length == 1) {
    			m.invoke(_self, procedureStack);
    			setterCalled = true;
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    if (!setterCalled) {
    	_self_.procedureStack = procedureStack;
    }
  }
  
  protected static String _privk3_indentation(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self) {
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("getIndentation") &&
    			m.getParameterTypes().length == 0) {
    				Object ret = m.invoke(_self);
    				if (ret != null) {
    					return (java.lang.String) ret;
    				} else {
    					return null;
    				}
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    return _self_.indentation;
  }
  
  protected static void _privk3_indentation(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self, final String indentation) {
    boolean setterCalled = false;
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("setIndentation")
    				&& m.getParameterTypes().length == 1) {
    			m.invoke(_self, indentation);
    			setterCalled = true;
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    if (!setterCalled) {
    	_self_.indentation = indentation;
    }
  }
  
  protected static BufferedReader _privk3_inputScenarioReader(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self) {
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("getInputScenarioReader") &&
    			m.getParameterTypes().length == 0) {
    				Object ret = m.invoke(_self);
    				if (ret != null) {
    					return (java.io.BufferedReader) ret;
    				} else {
    					return null;
    				}
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    return _self_.inputScenarioReader;
  }
  
  protected static void _privk3_inputScenarioReader(final FCLModelAspectFCLModelAspectProperties _self_, final FCLModel _self, final BufferedReader inputScenarioReader) {
    boolean setterCalled = false;
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("setInputScenarioReader")
    				&& m.getParameterTypes().length == 1) {
    			m.invoke(_self, inputScenarioReader);
    			setterCalled = true;
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    if (!setterCalled) {
    	_self_.inputScenarioReader = inputScenarioReader;
    }
  }
}
