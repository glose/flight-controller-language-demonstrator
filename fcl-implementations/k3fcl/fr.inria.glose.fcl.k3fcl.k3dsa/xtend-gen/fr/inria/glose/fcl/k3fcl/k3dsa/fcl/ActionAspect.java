package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspectActionAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NotImplementedException;
import fr.inria.glose.fcl.model.fcl.Action;
import org.eclipse.xtext.xbase.lib.Exceptions;

@Aspect(className = Action.class)
@SuppressWarnings("all")
public abstract class ActionAspect {
  public static void doAction(final Action _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspectActionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspectActionAspectContext.getSelf(_self);
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect#void doAction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignmentAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.Assignment){
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignmentAspect.doAction((fr.inria.glose.fcl.model.fcl.Assignment)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect#void doAction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignmentAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect#void doAction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment){
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspect.doAction((fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect#void doAction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect#void doAction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.WhileActionAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.WhileAction){
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.WhileActionAspect.doAction((fr.inria.glose.fcl.model.fcl.WhileAction)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect#void doAction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.WhileActionAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect#void doAction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.LogAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.Log){
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.LogAspect.doAction((fr.inria.glose.fcl.model.fcl.Log)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect#void doAction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.LogAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect#void doAction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionBlockAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.ActionBlock){
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionBlockAspect.doAction((fr.inria.glose.fcl.model.fcl.ActionBlock)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect#void doAction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionBlockAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect#void doAction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IfActionAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IfAction){
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IfActionAspect.doAction((fr.inria.glose.fcl.model.fcl.IfAction)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect#void doAction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IfActionAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect#void doAction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureReturnAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FCLProcedureReturn){
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureReturnAspect.doAction((fr.inria.glose.fcl.model.fcl.FCLProcedureReturn)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect#void doAction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureReturnAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect#void doAction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.VarDecl){
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect.doAction((fr.inria.glose.fcl.model.fcl.VarDecl)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect#void doAction() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect
    // #DispatchPointCut_before# void doAction()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Action){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect._privk3_doAction(_self_, (fr.inria.glose.fcl.model.fcl.Action)_self);
    };
  }
  
  protected static void _privk3_doAction(final ActionAspectActionAspectProperties _self_, final Action _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement doAction() for " + _self));
      throw new NotImplementedException(("not implemented, please implement doAction() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
