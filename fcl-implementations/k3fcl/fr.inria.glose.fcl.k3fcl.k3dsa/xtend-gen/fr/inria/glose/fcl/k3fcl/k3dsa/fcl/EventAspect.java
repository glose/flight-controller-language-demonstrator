package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EventAspectEventAspectProperties;
import fr.inria.glose.fcl.model.fcl.Event;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.EventOccurrence;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmFactory;
import org.eclipse.emf.common.util.EList;
import org.eclipse.gemoc.commons.eclipse.emf.EObjectUtil;

@Aspect(className = Event.class)
@SuppressWarnings("all")
public abstract class EventAspect {
  public static void send(final Event _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EventAspectEventAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EventAspectEventAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void send()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Event){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EventAspect._privk3_send(_self_, (fr.inria.glose.fcl.model.fcl.Event)_self);
    };
  }
  
  protected static void _privk3_send(final EventAspectEventAspectProperties _self_, final Event _self) {
    final EventOccurrence myOccurrence = Interpreter_vmFactory.eINSTANCE.createEventOccurrence();
    myOccurrence.setEvent(_self);
    EList<EventOccurrence> _receivedEvents = EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class).getReceivedEvents();
    _receivedEvents.add(myOccurrence);
    String _name = _self.getName();
    String _plus = ("added occurrence of " + _name);
    EObjectAspect.info(_self, _plus);
  }
}
