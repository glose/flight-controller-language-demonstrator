package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspectBooleanValueAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.BooleanValue;
import fr.inria.glose.fcl.model.fcl.BooleanValueType;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.Value;

@Aspect(className = BooleanValue.class)
@SuppressWarnings("all")
public class BooleanValueAspect extends ValueAspect {
  public static Value evaluate(final BooleanValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspectBooleanValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspectBooleanValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.BooleanValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.BooleanValue)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value copy(final BooleanValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspectBooleanValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspectBooleanValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value copy()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.BooleanValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect._privk3_copy(_self_, (fr.inria.glose.fcl.model.fcl.BooleanValue)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static BooleanValue bEquals(final BooleanValue _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspectBooleanValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspectBooleanValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# BooleanValue bEquals(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.BooleanValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect._privk3_bEquals(_self_, (fr.inria.glose.fcl.model.fcl.BooleanValue)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.BooleanValue)result;
  }
  
  public static String valueToString(final BooleanValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspectBooleanValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspectBooleanValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# String valueToString()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.BooleanValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect._privk3_valueToString(_self_, (fr.inria.glose.fcl.model.fcl.BooleanValue)_self);
    };
    return (java.lang.String)result;
  }
  
  public static Object rawValue(final BooleanValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspectBooleanValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspectBooleanValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Object rawValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.BooleanValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect._privk3_rawValue(_self_, (fr.inria.glose.fcl.model.fcl.BooleanValue)_self);
    };
    return (java.lang.Object)result;
  }
  
  public static Boolean isKindOf(final BooleanValue _self, final DataType type) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspectBooleanValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspectBooleanValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Boolean isKindOf(DataType)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.BooleanValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect._privk3_isKindOf(_self_, (fr.inria.glose.fcl.model.fcl.BooleanValue)_self,type);
    };
    return (java.lang.Boolean)result;
  }
  
  protected static Value _privk3_evaluate(final BooleanValueAspectBooleanValueAspectProperties _self_, final BooleanValue _self) {
    return _self;
  }
  
  protected static Value _privk3_copy(final BooleanValueAspectBooleanValueAspectProperties _self_, final BooleanValue _self) {
    final BooleanValue aValue = FclFactory.eINSTANCE.createBooleanValue();
    aValue.setBooleanValue(_self.isBooleanValue());
    return aValue;
  }
  
  protected static BooleanValue _privk3_bEquals(final BooleanValueAspectBooleanValueAspectProperties _self_, final BooleanValue _self, final Value rhs) {
    final BooleanValue bValue = FclFactory.eINSTANCE.createBooleanValue();
    if ((rhs instanceof BooleanValue)) {
      boolean _isBooleanValue = _self.isBooleanValue();
      boolean _isBooleanValue_1 = ((BooleanValue) rhs).isBooleanValue();
      boolean _equals = (_isBooleanValue == _isBooleanValue_1);
      bValue.setBooleanValue(_equals);
    } else {
      bValue.setBooleanValue(false);
    }
    return bValue;
  }
  
  protected static String _privk3_valueToString(final BooleanValueAspectBooleanValueAspectProperties _self_, final BooleanValue _self) {
    return Boolean.valueOf(_self.isBooleanValue()).toString();
  }
  
  protected static Object _privk3_rawValue(final BooleanValueAspectBooleanValueAspectProperties _self_, final BooleanValue _self) {
    return Boolean.valueOf(_self.isBooleanValue());
  }
  
  protected static Boolean _privk3_isKindOf(final BooleanValueAspectBooleanValueAspectProperties _self_, final BooleanValue _self, final DataType type) {
    return Boolean.valueOf((type instanceof BooleanValueType));
  }
}
