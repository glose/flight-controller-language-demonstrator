package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallConstantAspectCallConstantAspectProperties;
import fr.inria.glose.fcl.model.fcl.CallConstant;
import java.util.Map;

@SuppressWarnings("all")
public class CallConstantAspectCallConstantAspectContext {
  public final static CallConstantAspectCallConstantAspectContext INSTANCE = new CallConstantAspectCallConstantAspectContext();
  
  public static CallConstantAspectCallConstantAspectProperties getSelf(final CallConstant _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallConstantAspectCallConstantAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<CallConstant, CallConstantAspectCallConstantAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.CallConstant, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallConstantAspectCallConstantAspectProperties>();
  
  public Map<CallConstant, CallConstantAspectCallConstantAspectProperties> getMap() {
    return map;
  }
}
