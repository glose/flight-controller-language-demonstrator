package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IfActionAspectIfActionAspectProperties;
import fr.inria.glose.fcl.model.fcl.IfAction;
import java.util.Map;

@SuppressWarnings("all")
public class IfActionAspectIfActionAspectContext {
  public final static IfActionAspectIfActionAspectContext INSTANCE = new IfActionAspectIfActionAspectContext();
  
  public static IfActionAspectIfActionAspectProperties getSelf(final IfAction _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IfActionAspectIfActionAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<IfAction, IfActionAspectIfActionAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.IfAction, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IfActionAspectIfActionAspectProperties>();
  
  public Map<IfAction, IfActionAspectIfActionAspectProperties> getMap() {
    return map;
  }
}
