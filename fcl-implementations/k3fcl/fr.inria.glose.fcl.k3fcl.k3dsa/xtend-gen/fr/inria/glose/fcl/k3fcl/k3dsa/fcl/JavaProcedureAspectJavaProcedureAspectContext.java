package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.JavaProcedureAspectJavaProcedureAspectProperties;
import fr.inria.glose.fcl.model.fcl.JavaProcedure;
import java.util.Map;

@SuppressWarnings("all")
public class JavaProcedureAspectJavaProcedureAspectContext {
  public final static JavaProcedureAspectJavaProcedureAspectContext INSTANCE = new JavaProcedureAspectJavaProcedureAspectContext();
  
  public static JavaProcedureAspectJavaProcedureAspectProperties getSelf(final JavaProcedure _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.JavaProcedureAspectJavaProcedureAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<JavaProcedure, JavaProcedureAspectJavaProcedureAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.JavaProcedure, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.JavaProcedureAspectJavaProcedureAspectProperties>();
  
  public Map<JavaProcedure, JavaProcedureAspectJavaProcedureAspectProperties> getMap() {
    return map;
  }
}
