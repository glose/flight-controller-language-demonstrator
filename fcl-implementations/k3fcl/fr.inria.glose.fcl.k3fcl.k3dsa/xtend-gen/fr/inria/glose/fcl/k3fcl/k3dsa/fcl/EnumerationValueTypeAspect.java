package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueTypeAspectEnumerationValueTypeAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueTypeAspect;
import fr.inria.glose.fcl.model.fcl.EnumerationLiteral;
import fr.inria.glose.fcl.model.fcl.EnumerationValue;
import fr.inria.glose.fcl.model.fcl.EnumerationValueType;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@Aspect(className = EnumerationValueType.class)
@SuppressWarnings("all")
public class EnumerationValueTypeAspect extends ValueTypeAspect {
  public static Value createValue(final EnumerationValueType _self, final String internalValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueTypeAspectEnumerationValueTypeAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueTypeAspectEnumerationValueTypeAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value createValue(String)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.EnumerationValueType){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueTypeAspect._privk3_createValue(_self_, (fr.inria.glose.fcl.model.fcl.EnumerationValueType)_self,internalValue);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static Value _privk3_createValue(final EnumerationValueTypeAspectEnumerationValueTypeAspectProperties _self_, final EnumerationValueType _self, final String internalValue) {
    final EnumerationValue aValue = FclFactory.eINSTANCE.createEnumerationValue();
    String _xifexpression = null;
    String _name = _self.getName();
    String _plus = (_name + "::");
    boolean _startsWith = internalValue.startsWith(_plus);
    if (_startsWith) {
      String _name_1 = _self.getName();
      String _plus_1 = (_name_1 + "::");
      _xifexpression = internalValue.replace(_plus_1, "");
    } else {
      _xifexpression = internalValue;
    }
    final String searchedLit = _xifexpression;
    final Function1<EnumerationLiteral, Boolean> _function = (EnumerationLiteral enumLit) -> {
      return Boolean.valueOf(enumLit.getName().equals(searchedLit));
    };
    final EnumerationLiteral enumLit = IterableExtensions.<EnumerationLiteral>findFirst(_self.getEnumerationLiteral(), _function);
    if ((enumLit == null)) {
      String _name_2 = _self.getName();
      String _plus_2 = ("failed to create " + _name_2);
      String _plus_3 = (_plus_2 + " enumeration value from literal ");
      String _plus_4 = (_plus_3 + searchedLit);
      EObjectAspect.error(_self, _plus_4);
    }
    aValue.setEnumerationValue(enumLit);
    return aValue;
  }
}
