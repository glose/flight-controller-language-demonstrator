package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspectProcedureParameterAspectProperties;
import fr.inria.glose.fcl.model.fcl.ProcedureParameter;
import java.util.Map;

@SuppressWarnings("all")
public class ProcedureParameterAspectProcedureParameterAspectContext {
  public final static ProcedureParameterAspectProcedureParameterAspectContext INSTANCE = new ProcedureParameterAspectProcedureParameterAspectContext();
  
  public static ProcedureParameterAspectProcedureParameterAspectProperties getSelf(final ProcedureParameter _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspectProcedureParameterAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<ProcedureParameter, ProcedureParameterAspectProcedureParameterAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.ProcedureParameter, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspectProcedureParameterAspectProperties>();
  
  public Map<ProcedureParameter, ProcedureParameterAspectProcedureParameterAspectProperties> getMap() {
    return map;
  }
}
