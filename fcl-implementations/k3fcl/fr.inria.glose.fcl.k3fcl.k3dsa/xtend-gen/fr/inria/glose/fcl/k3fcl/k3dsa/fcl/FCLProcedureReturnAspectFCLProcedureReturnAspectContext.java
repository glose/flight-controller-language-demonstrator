package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureReturnAspectFCLProcedureReturnAspectProperties;
import fr.inria.glose.fcl.model.fcl.FCLProcedureReturn;
import java.util.Map;

@SuppressWarnings("all")
public class FCLProcedureReturnAspectFCLProcedureReturnAspectContext {
  public final static FCLProcedureReturnAspectFCLProcedureReturnAspectContext INSTANCE = new FCLProcedureReturnAspectFCLProcedureReturnAspectContext();
  
  public static FCLProcedureReturnAspectFCLProcedureReturnAspectProperties getSelf(final FCLProcedureReturn _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureReturnAspectFCLProcedureReturnAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<FCLProcedureReturn, FCLProcedureReturnAspectFCLProcedureReturnAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.FCLProcedureReturn, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureReturnAspectFCLProcedureReturnAspectProperties>();
  
  public Map<FCLProcedureReturn, FCLProcedureReturnAspectFCLProcedureReturnAspectProperties> getMap() {
    return map;
  }
}
