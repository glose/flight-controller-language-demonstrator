package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueTypeAspectBooleanValueTypeAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueTypeAspect;
import fr.inria.glose.fcl.model.fcl.BooleanValue;
import fr.inria.glose.fcl.model.fcl.BooleanValueType;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.Value;

@Aspect(className = BooleanValueType.class)
@SuppressWarnings("all")
public class BooleanValueTypeAspect extends ValueTypeAspect {
  public static Value createValue(final BooleanValueType _self, final String internalValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueTypeAspectBooleanValueTypeAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueTypeAspectBooleanValueTypeAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value createValue(String)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.BooleanValueType){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueTypeAspect._privk3_createValue(_self_, (fr.inria.glose.fcl.model.fcl.BooleanValueType)_self,internalValue);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  protected static Value _privk3_createValue(final BooleanValueTypeAspectBooleanValueTypeAspectProperties _self_, final BooleanValueType _self, final String internalValue) {
    final BooleanValue aValue = FclFactory.eINSTANCE.createBooleanValue();
    aValue.setBooleanValue(Boolean.parseBoolean(internalValue));
    return aValue;
  }
}
