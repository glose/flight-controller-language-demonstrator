package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ControlStructureActionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IfActionAspectIfActionAspectProperties;
import fr.inria.glose.fcl.model.fcl.Action;
import fr.inria.glose.fcl.model.fcl.IfAction;

@Aspect(className = IfAction.class)
@SuppressWarnings("all")
public class IfActionAspect extends ControlStructureActionAspect {
  public static void doAction(final IfAction _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IfActionAspectIfActionAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IfActionAspectIfActionAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void doAction()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.IfAction){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IfActionAspect._privk3_doAction(_self_, (fr.inria.glose.fcl.model.fcl.IfAction)_self);
    };
  }
  
  protected static void _privk3_doAction(final IfActionAspectIfActionAspectProperties _self_, final IfAction _self) {
    boolean _evaluateAsBoolean = ExpressionAspect.evaluateAsBoolean(_self.getCondition());
    if (_evaluateAsBoolean) {
      Action _thenAction = _self.getThenAction();
      boolean _tripleNotEquals = (_thenAction != null);
      if (_tripleNotEquals) {
        ActionAspect.doAction(_self.getThenAction());
      }
    } else {
      Action _elseAction = _self.getElseAction();
      boolean _tripleNotEquals_1 = (_elseAction != null);
      if (_tripleNotEquals_1) {
        ActionAspect.doAction(_self.getElseAction());
      }
    }
  }
}
