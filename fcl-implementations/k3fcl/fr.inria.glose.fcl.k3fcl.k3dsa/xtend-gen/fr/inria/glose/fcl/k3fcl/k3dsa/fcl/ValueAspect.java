package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NotImplementedException;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties;
import fr.inria.glose.fcl.model.fcl.BooleanValue;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.FloatValue;
import fr.inria.glose.fcl.model.fcl.IntegerValue;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.xtext.xbase.lib.Exceptions;

@Aspect(className = Value.class)
@SuppressWarnings("all")
public abstract class ValueAspect {
  public static Value evaluate(final Value _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.BooleanValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect.evaluate((fr.inria.glose.fcl.model.fcl.BooleanValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.evaluate((fr.inria.glose.fcl.model.fcl.FloatValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.StructValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect.evaluate((fr.inria.glose.fcl.model.fcl.StructValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.EnumerationValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect.evaluate((fr.inria.glose.fcl.model.fcl.EnumerationValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.StringValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect.evaluate((fr.inria.glose.fcl.model.fcl.StringValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.evaluate((fr.inria.glose.fcl.model.fcl.IntegerValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value evaluate() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value copy(final Value _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value copy() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.StructValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect.copy((fr.inria.glose.fcl.model.fcl.StructValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value copy() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value copy() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.EnumerationValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect.copy((fr.inria.glose.fcl.model.fcl.EnumerationValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value copy() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value copy() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.copy((fr.inria.glose.fcl.model.fcl.IntegerValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value copy() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value copy() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.StringValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect.copy((fr.inria.glose.fcl.model.fcl.StringValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value copy() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value copy() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.copy((fr.inria.glose.fcl.model.fcl.FloatValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value copy() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value copy() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.BooleanValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect.copy((fr.inria.glose.fcl.model.fcl.BooleanValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value copy() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect
    // #DispatchPointCut_before# Value copy()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_copy(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static BooleanValue bEquals(final Value _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue bEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.StringValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect.bEquals((fr.inria.glose.fcl.model.fcl.StringValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue bEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue bEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.bEquals((fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue bEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue bEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.EnumerationValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect.bEquals((fr.inria.glose.fcl.model.fcl.EnumerationValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue bEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue bEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.bEquals((fr.inria.glose.fcl.model.fcl.IntegerValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue bEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue bEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.BooleanValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect.bEquals((fr.inria.glose.fcl.model.fcl.BooleanValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue bEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect
    // #DispatchPointCut_before# BooleanValue bEquals(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_bEquals(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.BooleanValue)result;
  }
  
  public static Value plus(final Value _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value plus(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.plus((fr.inria.glose.fcl.model.fcl.IntegerValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value plus(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value plus(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.plus((fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value plus(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    // #DispatchPointCut_before# Value plus(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_plus(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value minus(final Value _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value minus(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.minus((fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value minus(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value minus(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.minus((fr.inria.glose.fcl.model.fcl.IntegerValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value minus(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    // #DispatchPointCut_before# Value minus(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_minus(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value mult(final Value _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value mult(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.mult((fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value mult(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value mult(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.mult((fr.inria.glose.fcl.model.fcl.IntegerValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value mult(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    // #DispatchPointCut_before# Value mult(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_mult(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value div(final Value _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value div(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.div((fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value div(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value div(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.div((fr.inria.glose.fcl.model.fcl.IntegerValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value div(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    // #DispatchPointCut_before# Value div(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_div(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value modulo(final Value _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value modulo(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.modulo((fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value modulo(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value modulo(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.modulo((fr.inria.glose.fcl.model.fcl.IntegerValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value modulo(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    // #DispatchPointCut_before# Value modulo(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_modulo(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static BooleanValue greater(final Value _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue greater(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.greater((fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue greater(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue greater(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.greater((fr.inria.glose.fcl.model.fcl.IntegerValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue greater(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    // #DispatchPointCut_before# BooleanValue greater(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_greater(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.BooleanValue)result;
  }
  
  public static BooleanValue lower(final Value _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue lower(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.lower((fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue lower(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue lower(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.lower((fr.inria.glose.fcl.model.fcl.IntegerValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue lower(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    // #DispatchPointCut_before# BooleanValue lower(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_lower(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.BooleanValue)result;
  }
  
  public static BooleanValue greaterOrEquals(final Value _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue greaterOrEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.greaterOrEquals((fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue greaterOrEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue greaterOrEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.greaterOrEquals((fr.inria.glose.fcl.model.fcl.IntegerValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue greaterOrEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    // #DispatchPointCut_before# BooleanValue greaterOrEquals(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_greaterOrEquals(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.BooleanValue)result;
  }
  
  public static BooleanValue lowerOrEquals(final Value _self, final Value rhs) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue lowerOrEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.lowerOrEquals((fr.inria.glose.fcl.model.fcl.IntegerValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue lowerOrEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue lowerOrEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.lowerOrEquals((fr.inria.glose.fcl.model.fcl.FloatValue)_self,rhs);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#BooleanValue lowerOrEquals(Value) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    // #DispatchPointCut_before# BooleanValue lowerOrEquals(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_lowerOrEquals(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self,rhs);
    };
    return (fr.inria.glose.fcl.model.fcl.BooleanValue)result;
  }
  
  public static Value uminus(final Value _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value uminus() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.uminus((fr.inria.glose.fcl.model.fcl.FloatValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value uminus() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value uminus() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.uminus((fr.inria.glose.fcl.model.fcl.IntegerValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Value uminus() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    // #DispatchPointCut_before# Value uminus()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_uminus(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static FloatValue toFloatValue(final Value _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#FloatValue toFloatValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.toFloatValue((fr.inria.glose.fcl.model.fcl.IntegerValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#FloatValue toFloatValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#FloatValue toFloatValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.toFloatValue((fr.inria.glose.fcl.model.fcl.FloatValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#FloatValue toFloatValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    // #DispatchPointCut_before# FloatValue toFloatValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_toFloatValue(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.FloatValue)result;
  }
  
  public static IntegerValue toIntegerValue(final Value _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#IntegerValue toIntegerValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.toIntegerValue((fr.inria.glose.fcl.model.fcl.IntegerValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#IntegerValue toIntegerValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#IntegerValue toIntegerValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.toIntegerValue((fr.inria.glose.fcl.model.fcl.FloatValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#IntegerValue toIntegerValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    // #DispatchPointCut_before# IntegerValue toIntegerValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_toIntegerValue(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.IntegerValue)result;
  }
  
  public static Boolean isKindOf(final Value _self, final DataType type) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Boolean isKindOf(DataType) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.EnumerationValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect.isKindOf((fr.inria.glose.fcl.model.fcl.EnumerationValue)_self,type);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Boolean isKindOf(DataType) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Boolean isKindOf(DataType) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.isKindOf((fr.inria.glose.fcl.model.fcl.FloatValue)_self,type);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Boolean isKindOf(DataType) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Boolean isKindOf(DataType) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.isKindOf((fr.inria.glose.fcl.model.fcl.IntegerValue)_self,type);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Boolean isKindOf(DataType) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Boolean isKindOf(DataType) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.StringValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect.isKindOf((fr.inria.glose.fcl.model.fcl.StringValue)_self,type);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Boolean isKindOf(DataType) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Boolean isKindOf(DataType) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.StructValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect.isKindOf((fr.inria.glose.fcl.model.fcl.StructValue)_self,type);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Boolean isKindOf(DataType) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Boolean isKindOf(DataType) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.BooleanValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect.isKindOf((fr.inria.glose.fcl.model.fcl.BooleanValue)_self,type);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Boolean isKindOf(DataType) from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect
    // #DispatchPointCut_before# Boolean isKindOf(DataType)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_isKindOf(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self,type);
    };
    return (java.lang.Boolean)result;
  }
  
  /**
   * a human readable version of the value
   */
  public static String valueToString(final Value _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#String valueToString() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.BooleanValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect.valueToString((fr.inria.glose.fcl.model.fcl.BooleanValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#String valueToString() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#String valueToString() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.valueToString((fr.inria.glose.fcl.model.fcl.IntegerValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#String valueToString() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#String valueToString() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.StructValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect.valueToString((fr.inria.glose.fcl.model.fcl.StructValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#String valueToString() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#String valueToString() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.StringValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect.valueToString((fr.inria.glose.fcl.model.fcl.StringValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#String valueToString() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#String valueToString() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.EnumerationValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect.valueToString((fr.inria.glose.fcl.model.fcl.EnumerationValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#String valueToString() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#String valueToString() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.valueToString((fr.inria.glose.fcl.model.fcl.FloatValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#String valueToString() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    // #DispatchPointCut_before# String valueToString()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_valueToString(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self);
    };
    return (java.lang.String)result;
  }
  
  public static Object rawValue(final Value _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspectValueAspectContext.getSelf(_self);
    Object result = null;
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Object rawValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.StructValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect.rawValue((fr.inria.glose.fcl.model.fcl.StructValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Object rawValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Object rawValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.FloatValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect.rawValue((fr.inria.glose.fcl.model.fcl.FloatValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Object rawValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Object rawValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.EnumerationValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect.rawValue((fr.inria.glose.fcl.model.fcl.EnumerationValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Object rawValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Object rawValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.StringValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect.rawValue((fr.inria.glose.fcl.model.fcl.StringValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Object rawValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Object rawValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.IntegerValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect.rawValue((fr.inria.glose.fcl.model.fcl.IntegerValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Object rawValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect
    	// BeginInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Object rawValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect
    		if (_self instanceof fr.inria.glose.fcl.model.fcl.BooleanValue){
    			result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect.rawValue((fr.inria.glose.fcl.model.fcl.BooleanValue)_self);
    		} else
    		// EndInjectInto fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect#Object rawValue() from fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect
    // #DispatchPointCut_before# Object rawValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.Value){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect._privk3_rawValue(_self_, (fr.inria.glose.fcl.model.fcl.Value)_self);
    };
    return (java.lang.Object)result;
  }
  
  protected static Value _privk3_evaluate(final ValueAspectValueAspectProperties _self_, final Value _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement evaluate() for " + _self));
      throw new NotImplementedException(("not implemented, please implement evaluate() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static Value _privk3_copy(final ValueAspectValueAspectProperties _self_, final Value _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement copy() for " + _self));
      throw new NotImplementedException(("not implemented, please implement copy() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static BooleanValue _privk3_bEquals(final ValueAspectValueAspectProperties _self_, final Value _self, final Value rhs) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement equals() for " + _self));
      throw new NotImplementedException(("not implemented, please implement equals() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static Value _privk3_plus(final ValueAspectValueAspectProperties _self_, final Value _self, final Value rhs) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement plus() for " + _self));
      throw new NotImplementedException(("not implemented, please implement plus() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static Value _privk3_minus(final ValueAspectValueAspectProperties _self_, final Value _self, final Value rhs) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement minus() for " + _self));
      throw new NotImplementedException(("not implemented, please implement minus() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static Value _privk3_mult(final ValueAspectValueAspectProperties _self_, final Value _self, final Value rhs) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement mult() for " + _self));
      throw new NotImplementedException(("not implemented, please implement mult() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static Value _privk3_div(final ValueAspectValueAspectProperties _self_, final Value _self, final Value rhs) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement div() for " + _self));
      throw new NotImplementedException(("not implemented, please implement div() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static Value _privk3_modulo(final ValueAspectValueAspectProperties _self_, final Value _self, final Value rhs) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement modulo() for " + _self));
      throw new NotImplementedException(("not implemented, please implement modulo() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static BooleanValue _privk3_greater(final ValueAspectValueAspectProperties _self_, final Value _self, final Value rhs) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement greater() for " + _self));
      throw new NotImplementedException(("not implemented, please implement greater() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static BooleanValue _privk3_lower(final ValueAspectValueAspectProperties _self_, final Value _self, final Value rhs) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement lower() for " + _self));
      throw new NotImplementedException(("not implemented, please implement lower() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static BooleanValue _privk3_greaterOrEquals(final ValueAspectValueAspectProperties _self_, final Value _self, final Value rhs) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement greaterOrEquals() for " + _self));
      throw new NotImplementedException(("not implemented, please implement greaterOrEquals() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static BooleanValue _privk3_lowerOrEquals(final ValueAspectValueAspectProperties _self_, final Value _self, final Value rhs) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement lowerOrEquals(() for " + _self));
      throw new NotImplementedException(("not implemented, please implement lowerOrEquals(() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static Value _privk3_uminus(final ValueAspectValueAspectProperties _self_, final Value _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement uminus() for " + _self));
      throw new NotImplementedException(("not implemented, please implement uminus() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static FloatValue _privk3_toFloatValue(final ValueAspectValueAspectProperties _self_, final Value _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement toFloatValue() for " + _self));
      throw new NotImplementedException(("not implemented, please implement toFloatValue() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static IntegerValue _privk3_toIntegerValue(final ValueAspectValueAspectProperties _self_, final Value _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement toIntegerValue() for " + _self));
      throw new NotImplementedException(("not implemented, please implement toIntegerValue() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static Boolean _privk3_isKindOf(final ValueAspectValueAspectProperties _self_, final Value _self, final DataType type) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement isKindOf() for " + _self));
      throw new NotImplementedException(("not implemented, please implement isKindOf() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static String _privk3_valueToString(final ValueAspectValueAspectProperties _self_, final Value _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement valueToString() for " + _self));
      throw new NotImplementedException(("not implemented, please implement valueToString() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static Object _privk3_rawValue(final ValueAspectValueAspectProperties _self_, final Value _self) {
    try {
      EObjectAspect.devError(_self, ("not implemented, please ask language designer to implement rawValue() for " + _self));
      throw new NotImplementedException(("not implemented, please implement rawValue() for " + _self));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
