package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspectAssignableAspectProperties;
import fr.inria.glose.fcl.model.fcl.Assignable;
import java.util.Map;

@SuppressWarnings("all")
public class AssignableAspectAssignableAspectContext {
  public final static AssignableAspectAssignableAspectContext INSTANCE = new AssignableAspectAssignableAspectContext();
  
  public static AssignableAspectAssignableAspectProperties getSelf(final Assignable _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspectAssignableAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<Assignable, AssignableAspectAssignableAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.Assignable, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspectAssignableAspectProperties>();
  
  public Map<Assignable, AssignableAspectAssignableAspectProperties> getMap() {
    return map;
  }
}
