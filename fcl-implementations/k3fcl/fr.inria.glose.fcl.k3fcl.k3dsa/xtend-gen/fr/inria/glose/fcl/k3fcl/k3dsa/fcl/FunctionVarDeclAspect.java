package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import com.google.common.base.Objects;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.FunctionVarDecl;
import fr.inria.glose.fcl.model.fcl.Value;

@Aspect(className = FunctionVarDecl.class, with = { CallableDeclarationAspect.class })
@SuppressWarnings("all")
public class FunctionVarDeclAspect extends AssignableAspect {
  public static void assign(final FunctionVarDecl _self, final Value newValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void assign(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionVarDecl){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect._privk3_assign(_self_, (fr.inria.glose.fcl.model.fcl.FunctionVarDecl)_self,newValue);
    };
  }
  
  public static Value call(final FunctionVarDecl _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value call()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionVarDecl){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect._privk3_call(_self_, (fr.inria.glose.fcl.model.fcl.FunctionVarDecl)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value callPrevious(final FunctionVarDecl _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value callPrevious()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionVarDecl){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect._privk3_callPrevious(_self_, (fr.inria.glose.fcl.model.fcl.FunctionVarDecl)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static DataType getResolvedType(final FunctionVarDecl _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# DataType getResolvedType()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionVarDecl){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect._privk3_getResolvedType(_self_, (fr.inria.glose.fcl.model.fcl.FunctionVarDecl)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.DataType)result;
  }
  
  /**
   * returns a label with smart display of the label + value on several lines
   * display the previous value only if different from current value
   */
  public static String smartLabelWithValue(final FunctionVarDecl _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# String smartLabelWithValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionVarDecl){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect._privk3_smartLabelWithValue(_self_, (fr.inria.glose.fcl.model.fcl.FunctionVarDecl)_self);
    };
    return (java.lang.String)result;
  }
  
  public static Value previousValue(final FunctionVarDecl _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value previousValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionVarDecl){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect._privk3_previousValue(_self_, (fr.inria.glose.fcl.model.fcl.FunctionVarDecl)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static void previousValue(final FunctionVarDecl _self, final Value previousValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void previousValue(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionVarDecl){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect._privk3_previousValue(_self_, (fr.inria.glose.fcl.model.fcl.FunctionVarDecl)_self,previousValue);
    };
  }
  
  public static Value currentValue(final FunctionVarDecl _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value currentValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionVarDecl){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect._privk3_currentValue(_self_, (fr.inria.glose.fcl.model.fcl.FunctionVarDecl)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static void currentValue(final FunctionVarDecl _self, final Value currentValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspectFunctionVarDeclAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void currentValue(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionVarDecl){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect._privk3_currentValue(_self_, (fr.inria.glose.fcl.model.fcl.FunctionVarDecl)_self,currentValue);
    };
  }
  
  protected static void _privk3_assign(final FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_, final FunctionVarDecl _self, final Value newValue) {
    if (((_self.getCurrentValue() == null) || (!Objects.equal(ValueAspect.rawValue(_self.getCurrentValue()), ValueAspect.rawValue(newValue))))) {
      _self.setCurrentValue(newValue);
    }
    if (((!(FunctionVarDeclAspect.previousValue(_self) != null)) || (!Boolean.valueOf(FunctionVarDeclAspect.previousValue(_self).equals(newValue)).booleanValue()))) {
      String _qualifiedName = EObjectAspect.getQualifiedName(_self);
      String _plus = ("" + _qualifiedName);
      String _plus_1 = (_plus + " := ");
      String _valueToString = ValueAspect.valueToString(newValue);
      String _plus_2 = (_plus_1 + _valueToString);
      EObjectAspect.important(_self, _plus_2);
    }
  }
  
  protected static Value _privk3_call(final FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_, final FunctionVarDecl _self) {
    return _self.getCurrentValue();
  }
  
  protected static Value _privk3_callPrevious(final FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_, final FunctionVarDecl _self) {
    return FunctionVarDeclAspect.previousValue(_self);
  }
  
  protected static DataType _privk3_getResolvedType(final FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_, final FunctionVarDecl _self) {
    return _self.getType();
  }
  
  protected static String _privk3_smartLabelWithValue(final FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_, final FunctionVarDecl _self) {
    final String prevValueString = ValueAspect.valueToString(FunctionVarDeclAspect.previousValue(_self));
    final String currentValueString = ValueAspect.valueToString(_self.getCurrentValue());
    boolean _equals = Objects.equal(prevValueString, currentValueString);
    if (_equals) {
      String _name = _self.getName();
      String _plus = (_name + "\n[");
      String _plus_1 = (_plus + currentValueString);
      return (_plus_1 + "]");
    } else {
      String _name_1 = _self.getName();
      String _plus_2 = (_name_1 + "\n[");
      String _plus_3 = (_plus_2 + prevValueString);
      String _plus_4 = (_plus_3 + "](-1)\n[");
      String _plus_5 = (_plus_4 + currentValueString);
      return (_plus_5 + "]");
    }
  }
  
  protected static Value _privk3_previousValue(final FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_, final FunctionVarDecl _self) {
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("getPreviousValue") &&
    			m.getParameterTypes().length == 0) {
    				Object ret = m.invoke(_self);
    				if (ret != null) {
    					return (fr.inria.glose.fcl.model.fcl.Value) ret;
    				} else {
    					return null;
    				}
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    return _self_.previousValue;
  }
  
  protected static void _privk3_previousValue(final FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_, final FunctionVarDecl _self, final Value previousValue) {
    boolean setterCalled = false;
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("setPreviousValue")
    				&& m.getParameterTypes().length == 1) {
    			m.invoke(_self, previousValue);
    			setterCalled = true;
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    if (!setterCalled) {
    	_self_.previousValue = previousValue;
    }
  }
  
  protected static Value _privk3_currentValue(final FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_, final FunctionVarDecl _self) {
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("getCurrentValue") &&
    			m.getParameterTypes().length == 0) {
    				Object ret = m.invoke(_self);
    				if (ret != null) {
    					return (fr.inria.glose.fcl.model.fcl.Value) ret;
    				} else {
    					return null;
    				}
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    return _self_.currentValue;
  }
  
  protected static void _privk3_currentValue(final FunctionVarDeclAspectFunctionVarDeclAspectProperties _self_, final FunctionVarDecl _self, final Value currentValue) {
    boolean setterCalled = false;
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("setCurrentValue")
    				&& m.getParameterTypes().length == 1) {
    			m.invoke(_self, currentValue);
    			setterCalled = true;
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    if (!setterCalled) {
    	_self_.currentValue = currentValue;
    }
  }
  
  public static String getSimpleName(final FunctionVarDecl _self) {
    return CallableDeclarationAspect.getSimpleName(_self);
  }
}
