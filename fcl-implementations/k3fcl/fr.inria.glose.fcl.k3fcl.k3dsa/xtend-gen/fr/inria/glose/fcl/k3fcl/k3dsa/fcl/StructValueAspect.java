package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import com.google.common.base.Objects;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructPropertyValueAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.DataStructType;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.StructPropertyValue;
import fr.inria.glose.fcl.model.fcl.StructValue;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@Aspect(className = StructValue.class)
@SuppressWarnings("all")
public class StructValueAspect extends ValueAspect {
  public static Value evaluate(final StructValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value evaluate()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.StructValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect._privk3_evaluate(_self_, (fr.inria.glose.fcl.model.fcl.StructValue)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value getValueInProperty(final StructValue _self, final String propertyName) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value getValueInProperty(String)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.StructValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect._privk3_getValueInProperty(_self_, (fr.inria.glose.fcl.model.fcl.StructValue)_self,propertyName);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value copy(final StructValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value copy()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.StructValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect._privk3_copy(_self_, (fr.inria.glose.fcl.model.fcl.StructValue)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  /**
   * Set value in property with the given name,
   * warning, no type control we suppose that the caller knows if the given property is allowed in the given DataStruct
   */
  public static void setValueInProperty(final StructValue _self, final String propertyName, final Value newPropValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void setValueInProperty(String,Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.StructValue){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect._privk3_setValueInProperty(_self_, (fr.inria.glose.fcl.model.fcl.StructValue)_self,propertyName,newPropValue);
    };
  }
  
  public static String valueToString(final StructValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# String valueToString()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.StructValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect._privk3_valueToString(_self_, (fr.inria.glose.fcl.model.fcl.StructValue)_self);
    };
    return (java.lang.String)result;
  }
  
  public static Boolean isKindOf(final StructValue _self, final DataType type) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Boolean isKindOf(DataType)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.StructValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect._privk3_isKindOf(_self_, (fr.inria.glose.fcl.model.fcl.StructValue)_self,type);
    };
    return (java.lang.Boolean)result;
  }
  
  public static Object rawValue(final StructValue _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspectStructValueAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Object rawValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.StructValue){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect._privk3_rawValue(_self_, (fr.inria.glose.fcl.model.fcl.StructValue)_self);
    };
    return (java.lang.Object)result;
  }
  
  protected static Value _privk3_evaluate(final StructValueAspectStructValueAspectProperties _self_, final StructValue _self) {
    return _self;
  }
  
  protected static Value _privk3_getValueInProperty(final StructValueAspectStructValueAspectProperties _self_, final StructValue _self, final String propertyName) {
    Object _xblockexpression = null;
    {
      final Function1<StructPropertyValue, Boolean> _function = (StructPropertyValue p) -> {
        String _name = p.getName();
        return Boolean.valueOf(Objects.equal(_name, propertyName));
      };
      final StructPropertyValue propValue = IterableExtensions.<StructPropertyValue>findFirst(_self.getStructProperties(), _function);
      Object _xifexpression = null;
      if ((propValue != null)) {
        return propValue.getValue();
      } else {
        _xifexpression = null;
      }
      _xblockexpression = _xifexpression;
    }
    return ((Value)_xblockexpression);
  }
  
  protected static Value _privk3_copy(final StructValueAspectStructValueAspectProperties _self_, final StructValue _self) {
    final StructValue aValue = FclFactory.eINSTANCE.createStructValue();
    EList<StructPropertyValue> _structProperties = _self.getStructProperties();
    for (final StructPropertyValue structPropValue : _structProperties) {
      EList<StructPropertyValue> _structProperties_1 = aValue.getStructProperties();
      StructPropertyValue _copy = StructPropertyValueAspect.copy(structPropValue);
      _structProperties_1.add(_copy);
    }
    return aValue;
  }
  
  protected static void _privk3_setValueInProperty(final StructValueAspectStructValueAspectProperties _self_, final StructValue _self, final String propertyName, final Value newPropValue) {
    final Function1<StructPropertyValue, Boolean> _function = (StructPropertyValue p) -> {
      String _name = p.getName();
      return Boolean.valueOf(Objects.equal(_name, propertyName));
    };
    final StructPropertyValue propValue = IterableExtensions.<StructPropertyValue>findFirst(_self.getStructProperties(), _function);
    if ((propValue != null)) {
      propValue.setValue(newPropValue);
    } else {
      final StructPropertyValue newStructPropValue = FclFactory.eINSTANCE.createStructPropertyValue();
      newStructPropValue.setName(propertyName);
      newStructPropValue.setValue(newPropValue);
      EList<StructPropertyValue> _structProperties = _self.getStructProperties();
      _structProperties.add(newStructPropValue);
    }
  }
  
  protected static String _privk3_valueToString(final StructValueAspectStructValueAspectProperties _self_, final StructValue _self) {
    final Function1<StructPropertyValue, String> _function = (StructPropertyValue p) -> {
      String _name = p.getName();
      String _plus = (_name + "=");
      String _valueToString = ValueAspect.valueToString(p.getValue());
      return (_plus + _valueToString);
    };
    String _join = IterableExtensions.join(ListExtensions.<StructPropertyValue, String>map(_self.getStructProperties(), _function), ", ");
    String _plus = ("{" + _join);
    return (_plus + 
      "}");
  }
  
  protected static Boolean _privk3_isKindOf(final StructValueAspectStructValueAspectProperties _self_, final StructValue _self, final DataType type) {
    return Boolean.valueOf((type instanceof DataStructType));
  }
  
  protected static Object _privk3_rawValue(final StructValueAspectStructValueAspectProperties _self_, final StructValue _self) {
    return _self;
  }
}
