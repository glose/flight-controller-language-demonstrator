package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspectFunctionPortAspectProperties;
import fr.inria.glose.fcl.model.fcl.FunctionPort;
import java.util.Map;

@SuppressWarnings("all")
public class FunctionPortAspectFunctionPortAspectContext {
  public final static FunctionPortAspectFunctionPortAspectContext INSTANCE = new FunctionPortAspectFunctionPortAspectContext();
  
  public static FunctionPortAspectFunctionPortAspectProperties getSelf(final FunctionPort _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspectFunctionPortAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<FunctionPort, FunctionPortAspectFunctionPortAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.FunctionPort, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspectFunctionPortAspectProperties>();
  
  public Map<FunctionPort, FunctionPortAspectFunctionPortAspectProperties> getMap() {
    return map;
  }
}
