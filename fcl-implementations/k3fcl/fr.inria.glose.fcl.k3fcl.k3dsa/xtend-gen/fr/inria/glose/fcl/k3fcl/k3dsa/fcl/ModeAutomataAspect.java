package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.diverse.k3.al.annotationprocessor.Step;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspectModeAutomataAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.TransitionAspect;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.Mode;
import fr.inria.glose.fcl.model.fcl.ModeAutomata;
import fr.inria.glose.fcl.model.fcl.Transition;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.gemoc.commons.eclipse.emf.EObjectUtil;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@Aspect(className = ModeAutomata.class)
@SuppressWarnings("all")
public class ModeAutomataAspect extends ModeAspect {
  public static List<Transition> fireableModeTransitions(final ModeAutomata _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspectModeAutomataAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspectModeAutomataAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# List<Transition> fireableModeTransitions()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.ModeAutomata){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspect._privk3_fireableModeTransitions(_self_, (fr.inria.glose.fcl.model.fcl.ModeAutomata)_self);
    };
    return (java.util.List<fr.inria.glose.fcl.model.fcl.Transition>)result;
  }
  
  public static void switchMode(final ModeAutomata _self, final Transition transition) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspectModeAutomataAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspectModeAutomataAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void switchMode(Transition)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.ModeAutomata){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspect._privk3_switchMode(_self_, (fr.inria.glose.fcl.model.fcl.ModeAutomata)_self,transition);
    };
  }
  
  @Step
  public static void evalModeStateMachine(final ModeAutomata _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspectModeAutomataAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspectModeAutomataAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void evalModeStateMachine()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.ModeAutomata){
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand command = new fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand() {
    		@Override
    		public void execute() {
    			fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspect._privk3_evalModeStateMachine(_self_, (fr.inria.glose.fcl.model.fcl.ModeAutomata)_self);
    		}
    	};
    	fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager stepManager = fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepManagerRegistry.getInstance().findStepManager(_self);
    	if (stepManager != null) {
    		stepManager.executeStep(_self, new Object[] {_self}, command, "ModeAutomata", "evalModeStateMachine");
    	} else {
    		command.execute();
    	}
    	;
    };
  }
  
  public static Mode currentMode(final ModeAutomata _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspectModeAutomataAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspectModeAutomataAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Mode currentMode()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.ModeAutomata){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspect._privk3_currentMode(_self_, (fr.inria.glose.fcl.model.fcl.ModeAutomata)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Mode)result;
  }
  
  public static void currentMode(final ModeAutomata _self, final Mode currentMode) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspectModeAutomataAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspectModeAutomataAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void currentMode(Mode)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.ModeAutomata){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspect._privk3_currentMode(_self_, (fr.inria.glose.fcl.model.fcl.ModeAutomata)_self,currentMode);
    };
  }
  
  protected static List<Transition> _privk3_fireableModeTransitions(final ModeAutomataAspectModeAutomataAspectProperties _self_, final ModeAutomata _self) {
    final FCLModel fclModel = EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class);
    int _size = fclModel.getReceivedEvents().size();
    boolean _greaterThan = (_size > 0);
    if (_greaterThan) {
      String _pushIndent = FCLModelAspect.pushIndent(fclModel);
      String _plus = (_pushIndent + 
        "-> Evaluating possible mode switch considering event occurrence ");
      String _name = fclModel.getReceivedEvents().get(0).getEvent().getName();
      String _plus_1 = (_plus + _name);
      EObjectAspect.devInfo(_self, _plus_1);
    } else {
      String _pushIndent_1 = FCLModelAspect.pushIndent(fclModel);
      String _plus_2 = (_pushIndent_1 + 
        "-> Evaluating possible mode switch (without events)");
      EObjectAspect.devInfo(_self, _plus_2);
    }
    final Function1<Transition, Boolean> _function = (Transition t) -> {
      return Boolean.valueOf((TransitionAspect.evalEvent(t) && TransitionAspect.evalGuard(t)));
    };
    final List<Transition> result = IterableExtensions.<Transition>toList(IterableExtensions.<Transition>filter(_self.getCurrentMode().getOutgoingTransition(), _function));
    FCLModelAspect.popIndent(fclModel);
    return result;
  }
  
  protected static void _privk3_switchMode(final ModeAutomataAspectModeAutomataAspectProperties _self_, final ModeAutomata _self, final Transition transition) {
    TransitionAspect.fire(transition);
  }
  
  protected static void _privk3_evalModeStateMachine(final ModeAutomataAspectModeAutomataAspectProperties _self_, final ModeAutomata _self) {
    final FCLModel fclModel = EObjectUtil.<FCLModel>eContainerOfType(_self, FCLModel.class);
    List<Transition> fireableModeTransitions = ModeAutomataAspect.fireableModeTransitions(_self);
    while (((fireableModeTransitions.size() > 0) || (fclModel.getReceivedEvents().size() > 0))) {
      {
        String _indentation = fclModel.getIndentation();
        String _plus = (_indentation + 
          "\t fireableTransitions=");
        String _plus_1 = (_plus + fireableModeTransitions);
        String _plus_2 = (_plus_1 + " (out of ");
        EList<Transition> _outgoingTransition = _self.getCurrentMode().getOutgoingTransition();
        String _plus_3 = (_plus_2 + _outgoingTransition);
        String _plus_4 = (_plus_3 + ")");
        EObjectAspect.devInfo(_self, _plus_4);
        int _size = fireableModeTransitions.size();
        boolean _greaterThan = (_size > 0);
        if (_greaterThan) {
          ModeAutomataAspect.switchMode(_self, fireableModeTransitions.get(0));
          fireableModeTransitions = ModeAutomataAspect.fireableModeTransitions(_self);
        }
        while (((fireableModeTransitions.size() == 0) && (fclModel.getReceivedEvents().size() > 0))) {
          {
            EObjectAspect.devInfo(_self, "no more transition can be fired, but some event remains: consume one");
            FCLModelAspect.consumeEventOccurence(fclModel);
            fireableModeTransitions = ModeAutomataAspect.fireableModeTransitions(_self);
          }
        }
      }
    }
    EObjectAspect.devInfo(_self, "no more mode switch are possible and all events have been consumed");
    String _name = _self.getCurrentMode().getName();
    String _plus = ("current mode is " + _name);
    EObjectAspect.devInfo(_self, _plus);
  }
  
  protected static Mode _privk3_currentMode(final ModeAutomataAspectModeAutomataAspectProperties _self_, final ModeAutomata _self) {
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("getCurrentMode") &&
    			m.getParameterTypes().length == 0) {
    				Object ret = m.invoke(_self);
    				if (ret != null) {
    					return (fr.inria.glose.fcl.model.fcl.Mode) ret;
    				} else {
    					return null;
    				}
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    return _self_.currentMode;
  }
  
  protected static void _privk3_currentMode(final ModeAutomataAspectModeAutomataAspectProperties _self_, final ModeAutomata _self, final Mode currentMode) {
    boolean setterCalled = false;
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("setCurrentMode")
    				&& m.getParameterTypes().length == 1) {
    			m.invoke(_self, currentMode);
    			setterCalled = true;
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    if (!setterCalled) {
    	_self_.currentMode = currentMode;
    }
  }
}
