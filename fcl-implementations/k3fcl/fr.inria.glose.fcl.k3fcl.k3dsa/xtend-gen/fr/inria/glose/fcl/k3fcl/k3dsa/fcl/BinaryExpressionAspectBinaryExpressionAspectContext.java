package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BinaryExpressionAspectBinaryExpressionAspectProperties;
import fr.inria.glose.fcl.model.fcl.BinaryExpression;
import java.util.Map;

@SuppressWarnings("all")
public class BinaryExpressionAspectBinaryExpressionAspectContext {
  public final static BinaryExpressionAspectBinaryExpressionAspectContext INSTANCE = new BinaryExpressionAspectBinaryExpressionAspectContext();
  
  public static BinaryExpressionAspectBinaryExpressionAspectProperties getSelf(final BinaryExpression _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BinaryExpressionAspectBinaryExpressionAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<BinaryExpression, BinaryExpressionAspectBinaryExpressionAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.BinaryExpression, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BinaryExpressionAspectBinaryExpressionAspectProperties>();
  
  public Map<BinaryExpression, BinaryExpressionAspectBinaryExpressionAspectProperties> getMap() {
    return map;
  }
}
