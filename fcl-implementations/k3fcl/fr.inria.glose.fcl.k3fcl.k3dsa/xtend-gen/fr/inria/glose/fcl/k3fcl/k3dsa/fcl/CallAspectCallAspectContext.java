package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspectCallAspectProperties;
import fr.inria.glose.fcl.model.fcl.Call;
import java.util.Map;

@SuppressWarnings("all")
public class CallAspectCallAspectContext {
  public final static CallAspectCallAspectContext INSTANCE = new CallAspectCallAspectContext();
  
  public static CallAspectCallAspectProperties getSelf(final Call _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspectCallAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<Call, CallAspectCallAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.Call, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspectCallAspectProperties>();
  
  public Map<Call, CallAspectCallAspectProperties> getMap() {
    return map;
  }
}
