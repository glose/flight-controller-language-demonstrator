package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.model.fcl.interpreter_vm.EventOccurrence;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext;
import java.io.BufferedReader;
import java.util.List;

@SuppressWarnings("all")
public class FCLModelAspectFCLModelAspectProperties {
  public List<EventOccurrence> receivedEvents;
  
  public List<ProcedureContext> procedureStack;
  
  public String indentation;
  
  public BufferedReader inputScenarioReader = null;
}
