package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExternalProcedureAspectExternalProcedureAspectProperties;
import fr.inria.glose.fcl.model.fcl.ExternalProcedure;
import java.util.Map;

@SuppressWarnings("all")
public class ExternalProcedureAspectExternalProcedureAspectContext {
  public final static ExternalProcedureAspectExternalProcedureAspectContext INSTANCE = new ExternalProcedureAspectExternalProcedureAspectContext();
  
  public static ExternalProcedureAspectExternalProcedureAspectProperties getSelf(final ExternalProcedure _self) {
    		if (!INSTANCE.map.containsKey(_self))
    			INSTANCE.map.put(_self, new fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExternalProcedureAspectExternalProcedureAspectProperties());
    		return INSTANCE.map.get(_self);
  }
  
  private Map<ExternalProcedure, ExternalProcedureAspectExternalProcedureAspectProperties> map = new java.util.WeakHashMap<fr.inria.glose.fcl.model.fcl.ExternalProcedure, fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExternalProcedureAspectExternalProcedureAspectProperties>();
  
  public Map<ExternalProcedure, ExternalProcedureAspectExternalProcedureAspectProperties> getMap() {
    return map;
  }
}
