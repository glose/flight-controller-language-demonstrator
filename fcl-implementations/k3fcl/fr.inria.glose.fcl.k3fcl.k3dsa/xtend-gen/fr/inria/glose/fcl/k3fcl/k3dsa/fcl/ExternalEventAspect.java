package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EventAspect;
import fr.inria.glose.fcl.model.fcl.ExternalEvent;

@Aspect(className = ExternalEvent.class)
@SuppressWarnings("all")
public class ExternalEventAspect extends EventAspect {
}
