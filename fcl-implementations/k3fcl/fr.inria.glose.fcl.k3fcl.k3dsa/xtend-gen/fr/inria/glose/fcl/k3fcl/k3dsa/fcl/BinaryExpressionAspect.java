package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect;
import fr.inria.glose.fcl.model.fcl.BinaryExpression;

@Aspect(className = BinaryExpression.class)
@SuppressWarnings("all")
public abstract class BinaryExpressionAspect extends ExpressionAspect {
}
