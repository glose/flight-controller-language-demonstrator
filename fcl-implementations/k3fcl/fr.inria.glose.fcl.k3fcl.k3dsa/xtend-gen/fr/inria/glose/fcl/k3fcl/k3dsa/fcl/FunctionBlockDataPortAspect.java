package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import com.google.common.base.Objects;
import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.ecore.EObjectAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.InvalidData;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.FloatValue;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.FunctionVarDecl;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.xtext.xbase.lib.Exceptions;

@Aspect(className = FunctionBlockDataPort.class)
@SuppressWarnings("all")
public class FunctionBlockDataPortAspect extends FunctionPortAspect {
  public static void setFloat(final FunctionBlockDataPort _self, final double newFloatVal) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void setFloat(double)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect._privk3_setFloat(_self_, (fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self,newFloatVal);
    };
  }
  
  public static double getFloat(final FunctionBlockDataPort _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# double getFloat()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect._privk3_getFloat(_self_, (fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self);
    };
    return (double)result;
  }
  
  /**
   * Method allowing to set the value from a String
   * first tries to create the value according to the port type, then convert the string into the primitive datatype
   */
  public static void setValueFromString(final FunctionBlockDataPort _self, final String newValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void setValueFromString(String)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect._privk3_setValueFromString(_self_, (fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self,newValue);
    };
  }
  
  public static Value call(final FunctionBlockDataPort _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value call()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect._privk3_call(_self_, (fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static Value callPrevious(final FunctionBlockDataPort _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value callPrevious()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect._privk3_callPrevious(_self_, (fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static void assign(final FunctionBlockDataPort _self, final Value newValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void assign(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect._privk3_assign(_self_, (fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self,newValue);
    };
  }
  
  public static DataType getResolvedType(final FunctionBlockDataPort _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# DataType getResolvedType()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect._privk3_getResolvedType(_self_, (fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.DataType)result;
  }
  
  public static FunctionBlockDataPort getTargetDataPort(final FunctionBlockDataPort _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# FunctionBlockDataPort getTargetDataPort()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect._privk3_getTargetDataPort(_self_, (fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)result;
  }
  
  /**
   * returns a label with smart display of the label + value on several lines
   * display the previous value only if different from current value
   */
  public static String smartLabelWithValue(final FunctionBlockDataPort _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# String smartLabelWithValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect._privk3_smartLabelWithValue(_self_, (fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self);
    };
    return (java.lang.String)result;
  }
  
  public static Value previousValue(final FunctionBlockDataPort _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value previousValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect._privk3_previousValue(_self_, (fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static void previousValue(final FunctionBlockDataPort _self, final Value previousValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void previousValue(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect._privk3_previousValue(_self_, (fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self,previousValue);
    };
  }
  
  public static Value currentValue(final FunctionBlockDataPort _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectContext.getSelf(_self);
    Object result = null;
    // #DispatchPointCut_before# Value currentValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    	result = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect._privk3_currentValue(_self_, (fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self);
    };
    return (fr.inria.glose.fcl.model.fcl.Value)result;
  }
  
  public static void currentValue(final FunctionBlockDataPort _self, final Value currentValue) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspectFunctionBlockDataPortAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void currentValue(Value)
    if (_self instanceof fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect._privk3_currentValue(_self_, (fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort)_self,currentValue);
    };
  }
  
  protected static void _privk3_setFloat(final FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_, final FunctionBlockDataPort _self, final double newFloatVal) {
    final FloatValue newVal = FclFactory.eINSTANCE.createFloatValue();
    newVal.setFloatValue(newFloatVal);
    FunctionVarDecl _functionVarStore = _self.getFunctionVarStore();
    boolean _tripleNotEquals = (_functionVarStore != null);
    if (_tripleNotEquals) {
      FunctionVarDecl _functionVarStore_1 = _self.getFunctionVarStore();
      _functionVarStore_1.setCurrentValue(newVal);
    } else {
      _self.setCurrentValue(newVal);
    }
  }
  
  protected static double _privk3_getFloat(final FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_, final FunctionBlockDataPort _self) {
    try {
      Value cur = _self.getCurrentValue();
      FunctionVarDecl _functionVarStore = _self.getFunctionVarStore();
      boolean _tripleNotEquals = (_functionVarStore != null);
      if (_tripleNotEquals) {
        cur = _self.getFunctionVarStore().getCurrentValue();
      } else {
        cur = _self.getCurrentValue();
      }
      if ((cur instanceof FloatValue)) {
        return ((FloatValue) cur).getFloatValue();
      } else {
        throw new InvalidData(("Not a Float: " + cur));
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected static void _privk3_setValueFromString(final FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_, final FunctionBlockDataPort _self, final String newValue) {
    final Value aValue = DataTypeAspect.createValue(_self.getDataType(), newValue);
    if ((aValue != null)) {
      FunctionBlockDataPortAspect.assign(_self, aValue);
    } else {
      String _qualifiedName = EObjectAspect.getQualifiedName(_self);
      String _plus = ((("failed to assign " + newValue) + " to ") + _qualifiedName);
      EObjectAspect.error(_self, _plus);
    }
  }
  
  protected static Value _privk3_call(final FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_, final FunctionBlockDataPort _self) {
    FunctionVarDecl _functionVarStore = _self.getFunctionVarStore();
    boolean _tripleNotEquals = (_functionVarStore != null);
    if (_tripleNotEquals) {
      return _self.getFunctionVarStore().getCurrentValue();
    } else {
      return _self.getCurrentValue();
    }
  }
  
  protected static Value _privk3_callPrevious(final FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_, final FunctionBlockDataPort _self) {
    FunctionVarDecl _functionVarStore = _self.getFunctionVarStore();
    boolean _tripleNotEquals = (_functionVarStore != null);
    if (_tripleNotEquals) {
      return FunctionVarDeclAspect.previousValue(_self.getFunctionVarStore());
    } else {
      return FunctionBlockDataPortAspect.previousValue(_self);
    }
  }
  
  protected static void _privk3_assign(final FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_, final FunctionBlockDataPort _self, final Value newValue) {
    Value prevValue = _self.getCurrentValue();
    FunctionVarDecl _functionVarStore = _self.getFunctionVarStore();
    boolean _tripleNotEquals = (_functionVarStore != null);
    if (_tripleNotEquals) {
      prevValue = _self.getFunctionVarStore().getCurrentValue();
      FunctionVarDecl _functionVarStore_1 = _self.getFunctionVarStore();
      _functionVarStore_1.setCurrentValue(newValue);
      if (((_self.getFunctionVarStore().getCurrentValue() == null) || (!Objects.equal(ValueAspect.rawValue(_self.getFunctionVarStore().getCurrentValue()), ValueAspect.rawValue(newValue))))) {
        FunctionVarDecl _functionVarStore_2 = _self.getFunctionVarStore();
        _functionVarStore_2.setCurrentValue(newValue);
      }
    } else {
      prevValue = _self.getCurrentValue();
      if (((_self.getCurrentValue() == null) || (!Objects.equal(ValueAspect.rawValue(_self.getCurrentValue()), ValueAspect.rawValue(newValue))))) {
        _self.setCurrentValue(newValue);
      }
    }
    if (((!(prevValue != null)) || (!Boolean.valueOf(prevValue.equals(newValue)).booleanValue()))) {
      String _qualifiedName = EObjectAspect.getQualifiedName(_self);
      String _plus = ("" + _qualifiedName);
      String _plus_1 = (_plus + " := ");
      String _valueToString = ValueAspect.valueToString(newValue);
      String _plus_2 = (_plus_1 + _valueToString);
      EObjectAspect.important(_self, _plus_2);
    }
  }
  
  protected static DataType _privk3_getResolvedType(final FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_, final FunctionBlockDataPort _self) {
    return _self.getDataType();
  }
  
  protected static FunctionBlockDataPort _privk3_getTargetDataPort(final FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_, final FunctionBlockDataPort _self) {
    return _self;
  }
  
  protected static String _privk3_smartLabelWithValue(final FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_, final FunctionBlockDataPort _self) {
    final String prevValueString = ValueAspect.valueToString(FunctionBlockDataPortAspect.previousValue(_self));
    final String currentValueString = ValueAspect.valueToString(_self.getCurrentValue());
    boolean _equals = Objects.equal(prevValueString, currentValueString);
    if (_equals) {
      String _name = _self.getName();
      String _plus = (_name + "\n[");
      String _plus_1 = (_plus + currentValueString);
      return (_plus_1 + "]");
    } else {
      String _name_1 = _self.getName();
      String _plus_2 = (_name_1 + "\n[");
      String _plus_3 = (_plus_2 + prevValueString);
      String _plus_4 = (_plus_3 + "](-1)\n[");
      String _plus_5 = (_plus_4 + currentValueString);
      return (_plus_5 + "]");
    }
  }
  
  protected static Value _privk3_previousValue(final FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_, final FunctionBlockDataPort _self) {
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("getPreviousValue") &&
    			m.getParameterTypes().length == 0) {
    				Object ret = m.invoke(_self);
    				if (ret != null) {
    					return (fr.inria.glose.fcl.model.fcl.Value) ret;
    				} else {
    					return null;
    				}
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    return _self_.previousValue;
  }
  
  protected static void _privk3_previousValue(final FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_, final FunctionBlockDataPort _self, final Value previousValue) {
    boolean setterCalled = false;
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("setPreviousValue")
    				&& m.getParameterTypes().length == 1) {
    			m.invoke(_self, previousValue);
    			setterCalled = true;
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    if (!setterCalled) {
    	_self_.previousValue = previousValue;
    }
  }
  
  protected static Value _privk3_currentValue(final FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_, final FunctionBlockDataPort _self) {
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("getCurrentValue") &&
    			m.getParameterTypes().length == 0) {
    				Object ret = m.invoke(_self);
    				if (ret != null) {
    					return (fr.inria.glose.fcl.model.fcl.Value) ret;
    				} else {
    					return null;
    				}
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    return _self_.currentValue;
  }
  
  protected static void _privk3_currentValue(final FunctionBlockDataPortAspectFunctionBlockDataPortAspectProperties _self_, final FunctionBlockDataPort _self, final Value currentValue) {
    boolean setterCalled = false;
    try {
    	for (java.lang.reflect.Method m : _self.getClass().getMethods()) {
    		if (m.getName().equals("setCurrentValue")
    				&& m.getParameterTypes().length == 1) {
    			m.invoke(_self, currentValue);
    			setterCalled = true;
    		}
    	}
    } catch (Exception e) {
    	// Chut !
    }
    if (!setterCalled) {
    	_self_.currentValue = currentValue;
    }
  }
}
