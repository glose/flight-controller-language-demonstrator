package fr.inria.glose.fcl.k3fcl.k3dsa.fcl;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect;
import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionDataPortAspectUnityFunctionDataPortAspectProperties;
import fr.inria.glose.fcl.model.fcl.UnityFunction;
import fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper;
import org.eclipse.gemoc.commons.eclipse.emf.EObjectUtil;
import org.eclipse.swt.widgets.Display;

@Aspect(className = UnityFunctionDataPort.class)
@SuppressWarnings("all")
public class UnityFunctionDataPortAspect extends FunctionBlockDataPortAspect {
  public static void setUnityVariableFromPortValue(final UnityFunctionDataPort _self) {
    final fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionDataPortAspectUnityFunctionDataPortAspectProperties _self_ = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionDataPortAspectUnityFunctionDataPortAspectContext.getSelf(_self);
    // #DispatchPointCut_before# void setUnityVariableFromPortValue()
    if (_self instanceof fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort){
    	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionDataPortAspect._privk3_setUnityVariableFromPortValue(_self_, (fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort)_self);
    };
  }
  
  protected static void _privk3_setUnityVariableFromPortValue(final UnityFunctionDataPortAspectUnityFunctionDataPortAspectProperties _self_, final UnityFunctionDataPort _self) {
    Display _default = Display.getDefault();
    _default.asyncExec(new Runnable() {
      @Override
      public void run() {
        final UnityFunction unityFunction = EObjectUtil.<UnityFunction>eContainerOfType(_self, UnityFunction.class);
        final UnityWrapper unityWrapper = unityFunction.getUnityWrapper();
        final String nodeName = _self.getUnityNodeName();
        final String varName = _self.getUnityVariableName();
        final double newValue = FunctionBlockDataPortAspect.getFloat(_self);
        unityWrapper.getUnityInstance().setVar(nodeName, varName, newValue);
      }
    });
  }
}
