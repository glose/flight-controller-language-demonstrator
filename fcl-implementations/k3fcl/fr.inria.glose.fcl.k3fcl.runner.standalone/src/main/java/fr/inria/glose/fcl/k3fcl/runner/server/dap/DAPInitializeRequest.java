package fr.inria.glose.fcl.k3fcl.runner.server.dap;

public class DAPInitializeRequest extends DAPRequest {

	DAPInitializeRequestArguments arguments;

	public DAPInitializeRequest() {
		this.setCommand("initialize");
	}

	public DAPInitializeRequestArguments getArguments() {
		return arguments;
	}

	public void setArguments(DAPInitializeRequestArguments arguments) {
		this.arguments = arguments;
	}
	
	
}
