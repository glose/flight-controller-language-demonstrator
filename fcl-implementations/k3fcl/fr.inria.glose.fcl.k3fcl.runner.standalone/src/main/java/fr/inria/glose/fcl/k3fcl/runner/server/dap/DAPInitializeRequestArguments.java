package fr.inria.glose.fcl.k3fcl.runner.server.dap;

import java.util.Optional;

public class DAPInitializeRequestArguments {

	  /**
	   * The ID of the (frontend) client using this adapter.
	   * Optional
	   */
	  String clientID;

	  /**
	   * The human readable name of the (frontend) client using this adapter.
	   * Optional
	   */
	  String clientName;

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}


}
