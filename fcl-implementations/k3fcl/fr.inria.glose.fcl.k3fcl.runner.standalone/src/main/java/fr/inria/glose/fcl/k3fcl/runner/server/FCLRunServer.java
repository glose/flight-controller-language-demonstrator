package fr.inria.glose.fcl.k3fcl.runner.server;

import static spark.Service.*;

import spark.Service;

public class FCLRunServer {

	
	private String fclFile;
	private int port;
	private boolean showDebugUI = false;
	private boolean showInOutUI = false;
	
	
	

	public FCLRunServer(String fclFile, int port, boolean showDebugUI, boolean showInOutUI) {
		super();
		this.fclFile = fclFile;
		this.port = port;
		this.showDebugUI = showDebugUI;
		this.showInOutUI = showInOutUI;
	}
	
	public void setupServer() {
		
		
		// Instantiate your dependencies
        //bookDao = new BookDao();
        //userDao = new UserDao();
		// TODO connect GEMOC engine
		// gemocEngine = declared and access as static, only one engine per server (or use  manager of engines) 
		
		GEMOCEngineController controller = new GEMOCEngineController();

        // Configure Spark Service
		Service service = Service.ignite();
		service.port(port);
        //staticFiles.location("/public");
        //staticFiles.expireTime(600L);

        // Set up before-filters (called before each get/post)
//        before("*",                  Filters.addTrailingSlashes);
//        before("*",                  Filters.handleLocaleChange);

        // Set up routes
		service.get("/hello", (req, res) -> "Hello DiverSE");
		service.post("/",  GEMOCEngineController.handleRequestPost);
        
        
//        get(Path.Web.INDEX,          IndexController.serveIndexPage);
//        get(Path.Web.BOOKS,          BookController.fetchAllBooks);
//        get(Path.Web.ONE_BOOK,       BookController.fetchOneBook);
//        get(Path.Web.LOGIN,          LoginController.serveLoginPage);
//        post(Path.Web.LOGIN,         LoginController.handleLoginPost);
//        post(Path.Web.LOGOUT,        LoginController.handleLogoutPost);
//        get("*",                     ViewUtil.notFound);
        
        // requests
        // response
        // notifications

        //Set up after-filters (called after each get/post)
//        after("*",                   Filters.addGzipHeader);
	}
	
}
