package fr.inria.glose.fcl.k3fcl.runner.server;

import static spark.Spark.*;

import com.google.gson.Gson;

import fr.inria.glose.fcl.k3fcl.runner.server.dap.DAPInitializeRequest;
import fr.inria.glose.fcl.k3fcl.runner.server.dap.DAPInitializeRequestArguments;
import fr.inria.glose.fcl.k3fcl.runner.server.dap.DAPRequest;
import fr.inria.glose.fcl.k3fcl.runner.server.dap.DAPResponse;
import spark.Request;
import spark.Response;
import spark.Route;

public class GEMOCEngineController {

	// engine
	public static Route handleRequestPost = (Request request, Response response) -> {
		DAPResponse dapResponse;
		DAPRequest dapRequest = new Gson().fromJson(request.body(), DAPRequest.class);
		switch (dapRequest.getCommand()) {
		case "initialize":
			DAPInitializeRequest initRequest = new Gson().fromJson(request.body(), DAPInitializeRequest.class);
			System.out.println(" received: "+new Gson()
				      .toJson(initRequest).toString());
			// do the initialization
			// TODO
			
			dapResponse =  new DAPResponse();
			dapResponse.setSeq(initRequest.getSeq());
			dapResponse.setSuccess(false);
			
			break;

		default:
			System.out.println(" received: "+new Gson()
				      .toJson(dapRequest).toString());
			// TODO error response
			dapResponse = new DAPResponse();
			break;
		}
		//System.out.print(gemocRequest);
		
		System.out.println(" response: "+new Gson()
			      .toJson(dapRequest).toString());
		return new Gson()
			      .toJson(dapResponse).toString();
	};
}
