package fr.inria.glose.fcl.k3fcl.runner.server.dap;

public class DAPProtocolMessage {

	  /**
	   * Sequence number (also known as message ID). For protocol messages of type 'request' this ID can be used to cancel the request.
	   */
	private  int seq;

	  /**
	   * Message type.
	   * Values: 'request', 'response', 'event', etc.
	   */
	private String type;

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
