package fr.inria.glose.fcl.k3fcl.runner.server;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class FCLRunServerCLI {

	private static Options options = null; // Command line options
	private CommandLine cmd = null; // Command Line arguments
	
	private static final String PORT_OPTION = "p";
	private static final String DEBUG_UI_OPTION = "debugUI";
	private static final String INOUT_UI_OPTION = "ioUI";
	
	// analyzed options
	private String fclFile;
	private int port = 8080;
	private boolean showDebugUI = false;
	private boolean showInOutUI = false;



	public static void main(String[] args) {
		FCLRunServerCLI cliProg = new FCLRunServerCLI();
		cliProg.loadArgs(args);
		cliProg.run();
	}
	
	public void run() {
		new FCLRunServer(fclFile, port, showDebugUI, showInOutUI).setupServer();;
	}
	
	/**
	 * Constructor
	 */
	public FCLRunServerCLI() {
		prepareOptions();
	}

	protected void prepareOptions() {
		options = new Options();
		options.addOption( new Option( "help", "print this message" ) );
		options.addOption(PORT_OPTION, true, "Port used by the server (default=8080)");
		options.addOption(INOUT_UI_OPTION, false, "show input/output user interface");
		options.addOption(DEBUG_UI_OPTION, false, "show debug user interface");
		options.addOption(PORT_OPTION, true, "Port used by the server (default=8080)");
	}

	/**
	 * Validate and set command line arguments.
	 * Exit after printing usage if anything is astray
	 * @param args String[] args as featured in public static void main()
	 */
	public void loadArgs(String[] args){
		CommandLineParser parser = new DefaultParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}
		
		
		if (cmd.hasOption(PORT_OPTION)){
			String portString = cmd.getOptionValue(PORT_OPTION);
			try {
				this.port = Integer.parseInt(portString);
			} catch (NumberFormatException e) {
				System.out.println("Invalid port (not an integer)");
				System.exit(1);
			}			
		}

		if (cmd.hasOption(INOUT_UI_OPTION)){
			this.showInOutUI = true;			
		}
		if (cmd.hasOption(DEBUG_UI_OPTION)){
			this.showDebugUI = true;			
		}
		
		if(cmd.getArgList().size() == 0){
			System.out.println("Missing fcl file.");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("java -jar <this_jar.jar> [OPTIONS] <your_model.fcl>", options);
			System.exit(1);
		}
		else{
			if(cmd.getArgList().size() > 1){
				System.out.println("Invalid number of arguments.");
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("java -jar <this_jar.jar> [OPTIONS] <your_model.fcl>", options);
				System.exit(1);
			}
			else {
				fclFile = cmd.getArgList().get(0).toString();
			}
		}
	}

}
