package fr.inria.glose.fcl.k3fcl.runner.server.dap;

public class DAPRequest extends DAPProtocolMessage {

	
	/**
	   * The command to execute.
	   */
	private String command;

   /**
	* Object containing arguments for the command.
	* optionnal
	*/
	//private Object arguments;
	
	public DAPRequest () {
		this.setType("request");
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	//public Object getArguments() {
	//	return arguments;
	//}

	//public void setArguments(Object arguments) {
	//	this.arguments = arguments;
	//}
	
}
