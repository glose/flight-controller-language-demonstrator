
package fr.inria.glose.fcl.k3fcl.mep.server

import com.google.inject.Guice
import fr.inria.glose.fcl.FCLRuntimeModule
import fr.inria.glose.fcl.FCLStandaloneSetup
import org.eclipse.xtext.util.Modules2
import com.google.inject.Injector
import org.eclipse.emf.ecore.EPackage
import org.eclipse.xtext.resource.IResourceServiceProvider
import fr.inria.glose.fcl.model.fcl.FclPackage
import fr.inria.glose.fcl.ide.FCLIdeModule

/**
 * Initialization support for running FCL language as Execution server using xtext.
 */
class FCLMEPModule extends FCLStandaloneSetup {

/* 	override createInjector() {
		Guice.createInjector(Modules2.mixin(new FCLRuntimeModule, new FCLIdeModule))
	}*/
	
	override createInjector() {
		Guice.createInjector(Modules2.mixin(new FCLRuntimeModule, new FCLIdeModule, new CustomServerModule,
			[bind(IResourceServiceProvider.Registry).toProvider(IResourceServiceProvider.Registry.RegistryProvider)]
		))
	}
	
	override register(Injector injector) {
		super.register(injector)
		if (!EPackage.Registry.INSTANCE.containsKey("http://www.inria.fr/glose/fcl")) {
			EPackage.Registry.INSTANCE.put("http://www.inria.fr/glose/fcl", FclPackage.eINSTANCE);
		}
	}
	
}
