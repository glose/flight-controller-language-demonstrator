package fr.inria.glose.fcl.k3fcl.xdsml.design.services;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gemoc.commons.eclipse.emf.EObjectUtil;
import org.eclipse.gemoc.executionframework.extensions.sirius.services.AbstractGemocAnimatorServices;

import fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.Function;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.FunctionVarDecl;
import fr.inria.glose.fcl.model.fcl.Mode;
import fr.inria.glose.fcl.model.fcl.Value;
import k3fcl.xdsml.api.impl.K3FCLRTDAccessor;

public class K3FCLAnimatorServices extends AbstractGemocAnimatorServices {

	@Override
	protected List<StringCouple> getRepresentationRefreshList() {
		final List<StringCouple> res = new ArrayList<StringCouple>();
		// Add in res the list of layers that should be activated and refreshed while debugging the model
		//	in case of a single odesign with all layers in a single viewpoint:
        //		- the first String is the id of the Diagram Description
        //		- the second String is the id of the Layer
		//  
		//	in case of a diagram extension:
		//		- the first String is the Representation Name of the Diagram Extension (do not confuse with the Name !!)
		//		- the second String is the id of the Layer
				
		res.add(new StringCouple("FunctionDiagramK3FCLExtension", "Animation"));
		res.add(new StringCouple("FCLFunctionDiagram", "Animation"));

		res.add(new StringCouple("ModeDiagramK3FCLExtension", "Animation"));
		res.add(new StringCouple("FCLModeDiagram", "Animation"));
		return res;
	}
	
	public String smartLabelWithValue(FunctionBlockDataPort self) {
		if(self.getFunctionVarStore() != null) {
			// value is already shown by the corresponding variable
			return self.getName();
		}
		Value prevValue = K3FCLRTDAccessor.getPreviousValue(self); 
		String prevValueString = prevValue != null ? ValueAspect.valueToString(prevValue) : "null";
		Value curValue = K3FCLRTDAccessor.getCurrentValue(self); 
		String currentValueString = curValue != null ? ValueAspect.valueToString(curValue) : "null";
		if(prevValueString.equals(currentValueString)) {
			return  self.getName() + "\n[" + currentValueString+"]";
		} else {
			return  self.getName() + "\n["+prevValueString+"](-1)\n["+currentValueString+"]";
		}	
	}
	public String smartLabelWithValue(FunctionVarDecl self) {
		Value prevValue = K3FCLRTDAccessor.getPreviousValue(self); 
		String prevValueString = prevValue != null ? ValueAspect.valueToString(prevValue) : "null";
		Value curValue = K3FCLRTDAccessor.getCurrentValue(self); 
		String currentValueString = curValue != null ? ValueAspect.valueToString(curValue) : "null";
		if(prevValueString.equals(currentValueString)) {
			return  self.getName() + "\n[" + currentValueString+"]";
		} else {
			return  self.getName() + "\n["+prevValueString+"](-1)\n["+currentValueString+"]";
		}	
	}
	
	public boolean isFunctionDisabledInCurrentMode(Function self) {
		// self.eContainer(fcl::FCLModel).modeStateMachine.currentMode.enabledFunctions->includes(self)
		Mode currentMode = K3FCLRTDAccessor.getCurrentMode(EObjectUtil.eContainerOfType(self, FCLModel.class).getModeAutomata());
		if(currentMode == null) {
			// RTD probably not initialized yet
			return false;
		}
		return ! currentMode.getEnabledFunctions().contains(self);
	}
	
	public boolean isCurrentMode(Mode self) {
		Mode currentMode = K3FCLRTDAccessor.getCurrentMode(EObjectUtil.eContainerOfType(self, FCLModel.class).getModeAutomata());
		return self.equals(currentMode);
	}

}
