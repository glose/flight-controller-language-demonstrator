name = fr.inria.glose.fcl.k3fcl.K3FCL
ecore = platform:/resource/fr.inria.glose.fcl.model/model/fcl.ecore
k3 = fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ActionBlockAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignableAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.AssignmentAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorBinaryExpressionAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BasicOperatorUnaryExpressionAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BinaryExpressionAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.BooleanValueTypeAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallConstantAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDataStructPropertyAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallDeclarationReferenceAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallPreviousAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CallableDeclarationAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.CastAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ControlStructureActionAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyAssignmentAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructPropertyReferenceAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataStructTypeAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.DataTypeAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationLiteralAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EnumerationValueTypeAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.EventAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExpressionAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExternalEventAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ExternalProcedureAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLModelAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FCLProcedureReturnAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FMUFunctionBlockDataPortAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FloatValueTypeAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionBlockDataPortAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionConnectorAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionPortReferenceAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionTimeReferenceAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.FunctionVarDeclAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IfActionAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.IntegerValueTypeAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.JavaProcedureAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.LogAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ModeAutomataAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.NewDataStructAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.PrimitiveActionAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureCallAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ProcedureParameterAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StringValueTypeAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructPropertyValueAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.StructValueAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.TransitionAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnaryExpressionAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.UnityFunctionDataPortAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.ValueTypeAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.VarDeclAspect,\
	fr.inria.glose.fcl.k3fcl.k3dsa.fcl.WhileActionAspect
xtext = /fr.inria.glose.fcl.xtext/bin/fr/inria/glose/fcl/FCL.xtext

sirius = /fr.inria.glose.fcl.design/description/fcl.odesign
sirius.animation = /fr.inria.glose.fcl.k3fcl.xdsml.design/description/fr.inria.glose.fcl.k3fcl.K3fcl.odesign
	
