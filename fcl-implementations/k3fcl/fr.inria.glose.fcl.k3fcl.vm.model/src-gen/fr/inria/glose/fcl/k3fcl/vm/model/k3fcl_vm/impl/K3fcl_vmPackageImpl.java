/**
 */
package fr.inria.glose.fcl.k3fcl.vm.model.k3fcl_vm.impl;

import fr.inria.glose.fcl.k3fcl.vm.model.k3fcl_vm.EventOccurrence;
import fr.inria.glose.fcl.k3fcl.vm.model.k3fcl_vm.K3fcl_vmFactory;
import fr.inria.glose.fcl.k3fcl.vm.model.k3fcl_vm.K3fcl_vmPackage;

import fr.inria.glose.fcl.model.fcl.FclPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class K3fcl_vmPackageImpl extends EPackageImpl implements K3fcl_vmPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventOccurrenceEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.inria.glose.fcl.k3fcl.vm.model.k3fcl_vm.K3fcl_vmPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private K3fcl_vmPackageImpl() {
		super(eNS_URI, K3fcl_vmFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link K3fcl_vmPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static K3fcl_vmPackage init() {
		if (isInited)
			return (K3fcl_vmPackage) EPackage.Registry.INSTANCE.getEPackage(K3fcl_vmPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredK3fcl_vmPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		K3fcl_vmPackageImpl theK3fcl_vmPackage = registeredK3fcl_vmPackage instanceof K3fcl_vmPackageImpl
				? (K3fcl_vmPackageImpl) registeredK3fcl_vmPackage
				: new K3fcl_vmPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		FclPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theK3fcl_vmPackage.createPackageContents();

		// Initialize created meta-data
		theK3fcl_vmPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theK3fcl_vmPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(K3fcl_vmPackage.eNS_URI, theK3fcl_vmPackage);
		return theK3fcl_vmPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventOccurrence() {
		return eventOccurrenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventOccurrence_Event() {
		return (EReference) eventOccurrenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public K3fcl_vmFactory getK3fcl_vmFactory() {
		return (K3fcl_vmFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		eventOccurrenceEClass = createEClass(EVENT_OCCURRENCE);
		createEReference(eventOccurrenceEClass, EVENT_OCCURRENCE__EVENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		FclPackage theFclPackage = (FclPackage) EPackage.Registry.INSTANCE.getEPackage(FclPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(eventOccurrenceEClass, EventOccurrence.class, "EventOccurrence", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEventOccurrence_Event(), theFclPackage.getEvent(), null, "event", null, 0, 1,
				EventOccurrence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //K3fcl_vmPackageImpl
