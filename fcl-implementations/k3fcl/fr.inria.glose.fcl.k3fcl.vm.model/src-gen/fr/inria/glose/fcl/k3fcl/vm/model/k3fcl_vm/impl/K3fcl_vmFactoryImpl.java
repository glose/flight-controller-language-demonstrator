/**
 */
package fr.inria.glose.fcl.k3fcl.vm.model.k3fcl_vm.impl;

import fr.inria.glose.fcl.k3fcl.vm.model.k3fcl_vm.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class K3fcl_vmFactoryImpl extends EFactoryImpl implements K3fcl_vmFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static K3fcl_vmFactory init() {
		try {
			K3fcl_vmFactory theK3fcl_vmFactory = (K3fcl_vmFactory) EPackage.Registry.INSTANCE
					.getEFactory(K3fcl_vmPackage.eNS_URI);
			if (theK3fcl_vmFactory != null) {
				return theK3fcl_vmFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new K3fcl_vmFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public K3fcl_vmFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case K3fcl_vmPackage.EVENT_OCCURRENCE:
			return createEventOccurrence();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventOccurrence createEventOccurrence() {
		EventOccurrenceImpl eventOccurrence = new EventOccurrenceImpl();
		return eventOccurrence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public K3fcl_vmPackage getK3fcl_vmPackage() {
		return (K3fcl_vmPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static K3fcl_vmPackage getPackage() {
		return K3fcl_vmPackage.eINSTANCE;
	}

} //K3fcl_vmFactoryImpl
