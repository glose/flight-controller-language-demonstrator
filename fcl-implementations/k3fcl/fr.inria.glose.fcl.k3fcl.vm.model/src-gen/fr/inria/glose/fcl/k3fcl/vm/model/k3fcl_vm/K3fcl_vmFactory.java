/**
 */
package fr.inria.glose.fcl.k3fcl.vm.model.k3fcl_vm;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.inria.glose.fcl.k3fcl.vm.model.k3fcl_vm.K3fcl_vmPackage
 * @generated
 */
public interface K3fcl_vmFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	K3fcl_vmFactory eINSTANCE = fr.inria.glose.fcl.k3fcl.vm.model.k3fcl_vm.impl.K3fcl_vmFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Event Occurrence</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Occurrence</em>'.
	 * @generated
	 */
	EventOccurrence createEventOccurrence();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	K3fcl_vmPackage getK3fcl_vmPackage();

} //K3fcl_vmFactory
