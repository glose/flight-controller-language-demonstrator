package fr.inria.glose.fcl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.inria.glose.fcl.services.FCLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalFCLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'FlightControllerModel'", "'{'", "'events'", "'}'", "'procedures'", "'dataTypes'", "'modeAutomata'", "'initialMode'", "'onEntry'", "'onExit'", "'final'", "'mode'", "'enabledFunctions'", "'('", "','", "')'", "'transition'", "'->'", "'when'", "'do'", "'guard'", "'as'", "'dataPort'", "':'", "'functionVarStore'", "'defaultValue'", "'fmuVarName'", "'nodeName'", "'varName'", "'portRef'", "'<->'", "'function'", "'ports'", "'variables'", "'action'", "'timeReference'", "'dataFlow'", "'internalDataFlow'", "'fmu'", "'runnableFmuPath'", "'unityFunction'", "'unityWebPagePath'", "'observableVar'", "'increment'", "'connect'", "'with'", "'delay'", "'var'", "':='", "'if'", "'then'", "'else'", "'while'", "'external'", "'FCLProcedure'", "'JavaProcedure'", "'static'", "'methodFullQualifiedName'", "'Cast'", "'resultType'", "'operand'", "'new'", "'log'", "'prev'", "'return'", "'DataStructType'", "'properties'", "'ValueType'", "'unit'", "'description'", "'IntegerValueType'", "'min'", "'max'", "'FloatValueType'", "'BooleanValueType'", "'StringValueType'", "'enumeration'", "'.'", "'true'", "'false'", "'E'", "'e'", "'-'", "'in'", "'out'", "'inout'", "'not'", "'+'", "'*'", "'/'", "'%'", "'='", "'>'", "'<'", "'>='", "'<='", "'and'", "'or'"
    };
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=4;
    public static final int RULE_INT=6;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__100=100;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__102=102;
    public static final int T__94=94;
    public static final int T__101=101;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__99=99;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__88=88;
    public static final int T__108=108;
    public static final int T__89=89;
    public static final int T__107=107;
    public static final int T__84=84;
    public static final int T__104=104;
    public static final int T__85=85;
    public static final int T__103=103;
    public static final int T__86=86;
    public static final int T__106=106;
    public static final int T__87=87;
    public static final int T__105=105;

    // delegates
    // delegators


        public InternalFCLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFCLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFCLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalFCL.g"; }



     	private FCLGrammarAccess grammarAccess;

        public InternalFCLParser(TokenStream input, FCLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "FCLModel";
       	}

       	@Override
       	protected FCLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleFCLModel"
    // InternalFCL.g:65:1: entryRuleFCLModel returns [EObject current=null] : iv_ruleFCLModel= ruleFCLModel EOF ;
    public final EObject entryRuleFCLModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFCLModel = null;


        try {
            // InternalFCL.g:65:49: (iv_ruleFCLModel= ruleFCLModel EOF )
            // InternalFCL.g:66:2: iv_ruleFCLModel= ruleFCLModel EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFCLModelRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFCLModel=ruleFCLModel();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFCLModel; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFCLModel"


    // $ANTLR start "ruleFCLModel"
    // InternalFCL.g:72:1: ruleFCLModel returns [EObject current=null] : (otherlv_0= 'FlightControllerModel' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_modeAutomata_3_0= ruleMainAutomata ) ) ( (lv_dataflow_4_0= ruleMainFunction ) ) (otherlv_5= 'events' otherlv_6= '{' ( (lv_events_7_0= ruleEvent ) ) ( (lv_events_8_0= ruleEvent ) )* otherlv_9= '}' )? (otherlv_10= 'procedures' otherlv_11= '{' ( (lv_procedures_12_0= ruleProcedure ) ) ( (lv_procedures_13_0= ruleProcedure ) )* otherlv_14= '}' )? (otherlv_15= 'dataTypes' otherlv_16= '{' ( (lv_dataTypes_17_0= ruleDataType ) ) ( (lv_dataTypes_18_0= ruleDataType ) )* otherlv_19= '}' )? otherlv_20= '}' ) ;
    public final EObject ruleFCLModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        EObject lv_modeAutomata_3_0 = null;

        EObject lv_dataflow_4_0 = null;

        EObject lv_events_7_0 = null;

        EObject lv_events_8_0 = null;

        EObject lv_procedures_12_0 = null;

        EObject lv_procedures_13_0 = null;

        EObject lv_dataTypes_17_0 = null;

        EObject lv_dataTypes_18_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:78:2: ( (otherlv_0= 'FlightControllerModel' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_modeAutomata_3_0= ruleMainAutomata ) ) ( (lv_dataflow_4_0= ruleMainFunction ) ) (otherlv_5= 'events' otherlv_6= '{' ( (lv_events_7_0= ruleEvent ) ) ( (lv_events_8_0= ruleEvent ) )* otherlv_9= '}' )? (otherlv_10= 'procedures' otherlv_11= '{' ( (lv_procedures_12_0= ruleProcedure ) ) ( (lv_procedures_13_0= ruleProcedure ) )* otherlv_14= '}' )? (otherlv_15= 'dataTypes' otherlv_16= '{' ( (lv_dataTypes_17_0= ruleDataType ) ) ( (lv_dataTypes_18_0= ruleDataType ) )* otherlv_19= '}' )? otherlv_20= '}' ) )
            // InternalFCL.g:79:2: (otherlv_0= 'FlightControllerModel' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_modeAutomata_3_0= ruleMainAutomata ) ) ( (lv_dataflow_4_0= ruleMainFunction ) ) (otherlv_5= 'events' otherlv_6= '{' ( (lv_events_7_0= ruleEvent ) ) ( (lv_events_8_0= ruleEvent ) )* otherlv_9= '}' )? (otherlv_10= 'procedures' otherlv_11= '{' ( (lv_procedures_12_0= ruleProcedure ) ) ( (lv_procedures_13_0= ruleProcedure ) )* otherlv_14= '}' )? (otherlv_15= 'dataTypes' otherlv_16= '{' ( (lv_dataTypes_17_0= ruleDataType ) ) ( (lv_dataTypes_18_0= ruleDataType ) )* otherlv_19= '}' )? otherlv_20= '}' )
            {
            // InternalFCL.g:79:2: (otherlv_0= 'FlightControllerModel' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_modeAutomata_3_0= ruleMainAutomata ) ) ( (lv_dataflow_4_0= ruleMainFunction ) ) (otherlv_5= 'events' otherlv_6= '{' ( (lv_events_7_0= ruleEvent ) ) ( (lv_events_8_0= ruleEvent ) )* otherlv_9= '}' )? (otherlv_10= 'procedures' otherlv_11= '{' ( (lv_procedures_12_0= ruleProcedure ) ) ( (lv_procedures_13_0= ruleProcedure ) )* otherlv_14= '}' )? (otherlv_15= 'dataTypes' otherlv_16= '{' ( (lv_dataTypes_17_0= ruleDataType ) ) ( (lv_dataTypes_18_0= ruleDataType ) )* otherlv_19= '}' )? otherlv_20= '}' )
            // InternalFCL.g:80:3: otherlv_0= 'FlightControllerModel' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_modeAutomata_3_0= ruleMainAutomata ) ) ( (lv_dataflow_4_0= ruleMainFunction ) ) (otherlv_5= 'events' otherlv_6= '{' ( (lv_events_7_0= ruleEvent ) ) ( (lv_events_8_0= ruleEvent ) )* otherlv_9= '}' )? (otherlv_10= 'procedures' otherlv_11= '{' ( (lv_procedures_12_0= ruleProcedure ) ) ( (lv_procedures_13_0= ruleProcedure ) )* otherlv_14= '}' )? (otherlv_15= 'dataTypes' otherlv_16= '{' ( (lv_dataTypes_17_0= ruleDataType ) ) ( (lv_dataTypes_18_0= ruleDataType ) )* otherlv_19= '}' )? otherlv_20= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getFCLModelAccess().getFlightControllerModelKeyword_0());
              		
            }
            // InternalFCL.g:84:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalFCL.g:85:4: (lv_name_1_0= RULE_ID )
            {
            // InternalFCL.g:85:4: (lv_name_1_0= RULE_ID )
            // InternalFCL.g:86:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getFCLModelAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getFCLModelRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getFCLModelAccess().getLeftCurlyBracketKeyword_2());
              		
            }
            // InternalFCL.g:106:3: ( (lv_modeAutomata_3_0= ruleMainAutomata ) )
            // InternalFCL.g:107:4: (lv_modeAutomata_3_0= ruleMainAutomata )
            {
            // InternalFCL.g:107:4: (lv_modeAutomata_3_0= ruleMainAutomata )
            // InternalFCL.g:108:5: lv_modeAutomata_3_0= ruleMainAutomata
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getFCLModelAccess().getModeAutomataMainAutomataParserRuleCall_3_0());
              				
            }
            pushFollow(FOLLOW_6);
            lv_modeAutomata_3_0=ruleMainAutomata();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getFCLModelRule());
              					}
              					set(
              						current,
              						"modeAutomata",
              						lv_modeAutomata_3_0,
              						"fr.inria.glose.fcl.FCL.MainAutomata");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalFCL.g:125:3: ( (lv_dataflow_4_0= ruleMainFunction ) )
            // InternalFCL.g:126:4: (lv_dataflow_4_0= ruleMainFunction )
            {
            // InternalFCL.g:126:4: (lv_dataflow_4_0= ruleMainFunction )
            // InternalFCL.g:127:5: lv_dataflow_4_0= ruleMainFunction
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getFCLModelAccess().getDataflowMainFunctionParserRuleCall_4_0());
              				
            }
            pushFollow(FOLLOW_7);
            lv_dataflow_4_0=ruleMainFunction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getFCLModelRule());
              					}
              					set(
              						current,
              						"dataflow",
              						lv_dataflow_4_0,
              						"fr.inria.glose.fcl.FCL.MainFunction");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalFCL.g:144:3: (otherlv_5= 'events' otherlv_6= '{' ( (lv_events_7_0= ruleEvent ) ) ( (lv_events_8_0= ruleEvent ) )* otherlv_9= '}' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==13) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalFCL.g:145:4: otherlv_5= 'events' otherlv_6= '{' ( (lv_events_7_0= ruleEvent ) ) ( (lv_events_8_0= ruleEvent ) )* otherlv_9= '}'
                    {
                    otherlv_5=(Token)match(input,13,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_5, grammarAccess.getFCLModelAccess().getEventsKeyword_5_0());
                      			
                    }
                    otherlv_6=(Token)match(input,12,FOLLOW_8); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getFCLModelAccess().getLeftCurlyBracketKeyword_5_1());
                      			
                    }
                    // InternalFCL.g:153:4: ( (lv_events_7_0= ruleEvent ) )
                    // InternalFCL.g:154:5: (lv_events_7_0= ruleEvent )
                    {
                    // InternalFCL.g:154:5: (lv_events_7_0= ruleEvent )
                    // InternalFCL.g:155:6: lv_events_7_0= ruleEvent
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getFCLModelAccess().getEventsEventParserRuleCall_5_2_0());
                      					
                    }
                    pushFollow(FOLLOW_9);
                    lv_events_7_0=ruleEvent();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getFCLModelRule());
                      						}
                      						add(
                      							current,
                      							"events",
                      							lv_events_7_0,
                      							"fr.inria.glose.fcl.FCL.Event");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalFCL.g:172:4: ( (lv_events_8_0= ruleEvent ) )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( (LA1_0==64) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // InternalFCL.g:173:5: (lv_events_8_0= ruleEvent )
                    	    {
                    	    // InternalFCL.g:173:5: (lv_events_8_0= ruleEvent )
                    	    // InternalFCL.g:174:6: lv_events_8_0= ruleEvent
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getFCLModelAccess().getEventsEventParserRuleCall_5_3_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_9);
                    	    lv_events_8_0=ruleEvent();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getFCLModelRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"events",
                    	      							lv_events_8_0,
                    	      							"fr.inria.glose.fcl.FCL.Event");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,14,FOLLOW_10); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_9, grammarAccess.getFCLModelAccess().getRightCurlyBracketKeyword_5_4());
                      			
                    }

                    }
                    break;

            }

            // InternalFCL.g:196:3: (otherlv_10= 'procedures' otherlv_11= '{' ( (lv_procedures_12_0= ruleProcedure ) ) ( (lv_procedures_13_0= ruleProcedure ) )* otherlv_14= '}' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==15) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalFCL.g:197:4: otherlv_10= 'procedures' otherlv_11= '{' ( (lv_procedures_12_0= ruleProcedure ) ) ( (lv_procedures_13_0= ruleProcedure ) )* otherlv_14= '}'
                    {
                    otherlv_10=(Token)match(input,15,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_10, grammarAccess.getFCLModelAccess().getProceduresKeyword_6_0());
                      			
                    }
                    otherlv_11=(Token)match(input,12,FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_11, grammarAccess.getFCLModelAccess().getLeftCurlyBracketKeyword_6_1());
                      			
                    }
                    // InternalFCL.g:205:4: ( (lv_procedures_12_0= ruleProcedure ) )
                    // InternalFCL.g:206:5: (lv_procedures_12_0= ruleProcedure )
                    {
                    // InternalFCL.g:206:5: (lv_procedures_12_0= ruleProcedure )
                    // InternalFCL.g:207:6: lv_procedures_12_0= ruleProcedure
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getFCLModelAccess().getProceduresProcedureParserRuleCall_6_2_0());
                      					
                    }
                    pushFollow(FOLLOW_12);
                    lv_procedures_12_0=ruleProcedure();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getFCLModelRule());
                      						}
                      						add(
                      							current,
                      							"procedures",
                      							lv_procedures_12_0,
                      							"fr.inria.glose.fcl.FCL.Procedure");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalFCL.g:224:4: ( (lv_procedures_13_0= ruleProcedure ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( ((LA3_0>=65 && LA3_0<=66)) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // InternalFCL.g:225:5: (lv_procedures_13_0= ruleProcedure )
                    	    {
                    	    // InternalFCL.g:225:5: (lv_procedures_13_0= ruleProcedure )
                    	    // InternalFCL.g:226:6: lv_procedures_13_0= ruleProcedure
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getFCLModelAccess().getProceduresProcedureParserRuleCall_6_3_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_12);
                    	    lv_procedures_13_0=ruleProcedure();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getFCLModelRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"procedures",
                    	      							lv_procedures_13_0,
                    	      							"fr.inria.glose.fcl.FCL.Procedure");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,14,FOLLOW_13); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_14, grammarAccess.getFCLModelAccess().getRightCurlyBracketKeyword_6_4());
                      			
                    }

                    }
                    break;

            }

            // InternalFCL.g:248:3: (otherlv_15= 'dataTypes' otherlv_16= '{' ( (lv_dataTypes_17_0= ruleDataType ) ) ( (lv_dataTypes_18_0= ruleDataType ) )* otherlv_19= '}' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==16) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalFCL.g:249:4: otherlv_15= 'dataTypes' otherlv_16= '{' ( (lv_dataTypes_17_0= ruleDataType ) ) ( (lv_dataTypes_18_0= ruleDataType ) )* otherlv_19= '}'
                    {
                    otherlv_15=(Token)match(input,16,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_15, grammarAccess.getFCLModelAccess().getDataTypesKeyword_7_0());
                      			
                    }
                    otherlv_16=(Token)match(input,12,FOLLOW_14); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_16, grammarAccess.getFCLModelAccess().getLeftCurlyBracketKeyword_7_1());
                      			
                    }
                    // InternalFCL.g:257:4: ( (lv_dataTypes_17_0= ruleDataType ) )
                    // InternalFCL.g:258:5: (lv_dataTypes_17_0= ruleDataType )
                    {
                    // InternalFCL.g:258:5: (lv_dataTypes_17_0= ruleDataType )
                    // InternalFCL.g:259:6: lv_dataTypes_17_0= ruleDataType
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getFCLModelAccess().getDataTypesDataTypeParserRuleCall_7_2_0());
                      					
                    }
                    pushFollow(FOLLOW_15);
                    lv_dataTypes_17_0=ruleDataType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getFCLModelRule());
                      						}
                      						add(
                      							current,
                      							"dataTypes",
                      							lv_dataTypes_17_0,
                      							"fr.inria.glose.fcl.FCL.DataType");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalFCL.g:276:4: ( (lv_dataTypes_18_0= ruleDataType ) )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==76||LA5_0==78||LA5_0==81||(LA5_0>=84 && LA5_0<=87)) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // InternalFCL.g:277:5: (lv_dataTypes_18_0= ruleDataType )
                    	    {
                    	    // InternalFCL.g:277:5: (lv_dataTypes_18_0= ruleDataType )
                    	    // InternalFCL.g:278:6: lv_dataTypes_18_0= ruleDataType
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getFCLModelAccess().getDataTypesDataTypeParserRuleCall_7_3_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_15);
                    	    lv_dataTypes_18_0=ruleDataType();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getFCLModelRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"dataTypes",
                    	      							lv_dataTypes_18_0,
                    	      							"fr.inria.glose.fcl.FCL.DataType");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    otherlv_19=(Token)match(input,14,FOLLOW_16); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_19, grammarAccess.getFCLModelAccess().getRightCurlyBracketKeyword_7_4());
                      			
                    }

                    }
                    break;

            }

            otherlv_20=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_20, grammarAccess.getFCLModelAccess().getRightCurlyBracketKeyword_8());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFCLModel"


    // $ANTLR start "entryRuleProcedure"
    // InternalFCL.g:308:1: entryRuleProcedure returns [EObject current=null] : iv_ruleProcedure= ruleProcedure EOF ;
    public final EObject entryRuleProcedure() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProcedure = null;


        try {
            // InternalFCL.g:308:50: (iv_ruleProcedure= ruleProcedure EOF )
            // InternalFCL.g:309:2: iv_ruleProcedure= ruleProcedure EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getProcedureRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleProcedure=ruleProcedure();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleProcedure; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProcedure"


    // $ANTLR start "ruleProcedure"
    // InternalFCL.g:315:1: ruleProcedure returns [EObject current=null] : (this_FCLProcedure_0= ruleFCLProcedure | this_JavaProcedure_1= ruleJavaProcedure ) ;
    public final EObject ruleProcedure() throws RecognitionException {
        EObject current = null;

        EObject this_FCLProcedure_0 = null;

        EObject this_JavaProcedure_1 = null;



        	enterRule();

        try {
            // InternalFCL.g:321:2: ( (this_FCLProcedure_0= ruleFCLProcedure | this_JavaProcedure_1= ruleJavaProcedure ) )
            // InternalFCL.g:322:2: (this_FCLProcedure_0= ruleFCLProcedure | this_JavaProcedure_1= ruleJavaProcedure )
            {
            // InternalFCL.g:322:2: (this_FCLProcedure_0= ruleFCLProcedure | this_JavaProcedure_1= ruleJavaProcedure )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==65) ) {
                alt7=1;
            }
            else if ( (LA7_0==66) ) {
                alt7=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalFCL.g:323:3: this_FCLProcedure_0= ruleFCLProcedure
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getProcedureAccess().getFCLProcedureParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_FCLProcedure_0=ruleFCLProcedure();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_FCLProcedure_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalFCL.g:332:3: this_JavaProcedure_1= ruleJavaProcedure
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getProcedureAccess().getJavaProcedureParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_JavaProcedure_1=ruleJavaProcedure();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_JavaProcedure_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProcedure"


    // $ANTLR start "entryRuleDataType"
    // InternalFCL.g:344:1: entryRuleDataType returns [EObject current=null] : iv_ruleDataType= ruleDataType EOF ;
    public final EObject entryRuleDataType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataType = null;


        try {
            // InternalFCL.g:344:49: (iv_ruleDataType= ruleDataType EOF )
            // InternalFCL.g:345:2: iv_ruleDataType= ruleDataType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDataTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDataType=ruleDataType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDataType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataType"


    // $ANTLR start "ruleDataType"
    // InternalFCL.g:351:1: ruleDataType returns [EObject current=null] : (this_DataStructType_0= ruleDataStructType | this_ValueType_Impl_1= ruleValueType_Impl | this_IntegerValueType_2= ruleIntegerValueType | this_FloatValueType_3= ruleFloatValueType | this_BooleanValueType_4= ruleBooleanValueType | this_StringValueType_5= ruleStringValueType | this_EnumerationValueType_6= ruleEnumerationValueType ) ;
    public final EObject ruleDataType() throws RecognitionException {
        EObject current = null;

        EObject this_DataStructType_0 = null;

        EObject this_ValueType_Impl_1 = null;

        EObject this_IntegerValueType_2 = null;

        EObject this_FloatValueType_3 = null;

        EObject this_BooleanValueType_4 = null;

        EObject this_StringValueType_5 = null;

        EObject this_EnumerationValueType_6 = null;



        	enterRule();

        try {
            // InternalFCL.g:357:2: ( (this_DataStructType_0= ruleDataStructType | this_ValueType_Impl_1= ruleValueType_Impl | this_IntegerValueType_2= ruleIntegerValueType | this_FloatValueType_3= ruleFloatValueType | this_BooleanValueType_4= ruleBooleanValueType | this_StringValueType_5= ruleStringValueType | this_EnumerationValueType_6= ruleEnumerationValueType ) )
            // InternalFCL.g:358:2: (this_DataStructType_0= ruleDataStructType | this_ValueType_Impl_1= ruleValueType_Impl | this_IntegerValueType_2= ruleIntegerValueType | this_FloatValueType_3= ruleFloatValueType | this_BooleanValueType_4= ruleBooleanValueType | this_StringValueType_5= ruleStringValueType | this_EnumerationValueType_6= ruleEnumerationValueType )
            {
            // InternalFCL.g:358:2: (this_DataStructType_0= ruleDataStructType | this_ValueType_Impl_1= ruleValueType_Impl | this_IntegerValueType_2= ruleIntegerValueType | this_FloatValueType_3= ruleFloatValueType | this_BooleanValueType_4= ruleBooleanValueType | this_StringValueType_5= ruleStringValueType | this_EnumerationValueType_6= ruleEnumerationValueType )
            int alt8=7;
            switch ( input.LA(1) ) {
            case 76:
                {
                alt8=1;
                }
                break;
            case 78:
                {
                alt8=2;
                }
                break;
            case 81:
                {
                alt8=3;
                }
                break;
            case 84:
                {
                alt8=4;
                }
                break;
            case 85:
                {
                alt8=5;
                }
                break;
            case 86:
                {
                alt8=6;
                }
                break;
            case 87:
                {
                alt8=7;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalFCL.g:359:3: this_DataStructType_0= ruleDataStructType
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getDataTypeAccess().getDataStructTypeParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_DataStructType_0=ruleDataStructType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_DataStructType_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalFCL.g:368:3: this_ValueType_Impl_1= ruleValueType_Impl
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getDataTypeAccess().getValueType_ImplParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ValueType_Impl_1=ruleValueType_Impl();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ValueType_Impl_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalFCL.g:377:3: this_IntegerValueType_2= ruleIntegerValueType
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getDataTypeAccess().getIntegerValueTypeParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_IntegerValueType_2=ruleIntegerValueType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IntegerValueType_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalFCL.g:386:3: this_FloatValueType_3= ruleFloatValueType
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getDataTypeAccess().getFloatValueTypeParserRuleCall_3());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_FloatValueType_3=ruleFloatValueType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_FloatValueType_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalFCL.g:395:3: this_BooleanValueType_4= ruleBooleanValueType
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getDataTypeAccess().getBooleanValueTypeParserRuleCall_4());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_BooleanValueType_4=ruleBooleanValueType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_BooleanValueType_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 6 :
                    // InternalFCL.g:404:3: this_StringValueType_5= ruleStringValueType
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getDataTypeAccess().getStringValueTypeParserRuleCall_5());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_StringValueType_5=ruleStringValueType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_StringValueType_5;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 7 :
                    // InternalFCL.g:413:3: this_EnumerationValueType_6= ruleEnumerationValueType
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getDataTypeAccess().getEnumerationValueTypeParserRuleCall_6());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_EnumerationValueType_6=ruleEnumerationValueType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_EnumerationValueType_6;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataType"


    // $ANTLR start "entryRuleAction"
    // InternalFCL.g:425:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // InternalFCL.g:425:47: (iv_ruleAction= ruleAction EOF )
            // InternalFCL.g:426:2: iv_ruleAction= ruleAction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getActionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAction=ruleAction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAction; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalFCL.g:432:1: ruleAction returns [EObject current=null] : (this_ActionBlock_0= ruleActionBlock | this_Assignment_1= ruleAssignment | this_DataStructPropertyAssignment_2= ruleDataStructPropertyAssignment | this_IfAction_3= ruleIfAction | this_WhileAction_4= ruleWhileAction | this_ProcedureCall_5= ruleProcedureCall | this_VarDecl_Impl_6= ruleVarDecl_Impl | this_FCLProcedureReturn_7= ruleFCLProcedureReturn | this_Log_8= ruleLog ) ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        EObject this_ActionBlock_0 = null;

        EObject this_Assignment_1 = null;

        EObject this_DataStructPropertyAssignment_2 = null;

        EObject this_IfAction_3 = null;

        EObject this_WhileAction_4 = null;

        EObject this_ProcedureCall_5 = null;

        EObject this_VarDecl_Impl_6 = null;

        EObject this_FCLProcedureReturn_7 = null;

        EObject this_Log_8 = null;



        	enterRule();

        try {
            // InternalFCL.g:438:2: ( (this_ActionBlock_0= ruleActionBlock | this_Assignment_1= ruleAssignment | this_DataStructPropertyAssignment_2= ruleDataStructPropertyAssignment | this_IfAction_3= ruleIfAction | this_WhileAction_4= ruleWhileAction | this_ProcedureCall_5= ruleProcedureCall | this_VarDecl_Impl_6= ruleVarDecl_Impl | this_FCLProcedureReturn_7= ruleFCLProcedureReturn | this_Log_8= ruleLog ) )
            // InternalFCL.g:439:2: (this_ActionBlock_0= ruleActionBlock | this_Assignment_1= ruleAssignment | this_DataStructPropertyAssignment_2= ruleDataStructPropertyAssignment | this_IfAction_3= ruleIfAction | this_WhileAction_4= ruleWhileAction | this_ProcedureCall_5= ruleProcedureCall | this_VarDecl_Impl_6= ruleVarDecl_Impl | this_FCLProcedureReturn_7= ruleFCLProcedureReturn | this_Log_8= ruleLog )
            {
            // InternalFCL.g:439:2: (this_ActionBlock_0= ruleActionBlock | this_Assignment_1= ruleAssignment | this_DataStructPropertyAssignment_2= ruleDataStructPropertyAssignment | this_IfAction_3= ruleIfAction | this_WhileAction_4= ruleWhileAction | this_ProcedureCall_5= ruleProcedureCall | this_VarDecl_Impl_6= ruleVarDecl_Impl | this_FCLProcedureReturn_7= ruleFCLProcedureReturn | this_Log_8= ruleLog )
            int alt9=9;
            alt9 = dfa9.predict(input);
            switch (alt9) {
                case 1 :
                    // InternalFCL.g:440:3: this_ActionBlock_0= ruleActionBlock
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getActionAccess().getActionBlockParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ActionBlock_0=ruleActionBlock();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ActionBlock_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalFCL.g:449:3: this_Assignment_1= ruleAssignment
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getActionAccess().getAssignmentParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Assignment_1=ruleAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Assignment_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalFCL.g:458:3: this_DataStructPropertyAssignment_2= ruleDataStructPropertyAssignment
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getActionAccess().getDataStructPropertyAssignmentParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_DataStructPropertyAssignment_2=ruleDataStructPropertyAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_DataStructPropertyAssignment_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalFCL.g:467:3: this_IfAction_3= ruleIfAction
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getActionAccess().getIfActionParserRuleCall_3());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_IfAction_3=ruleIfAction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_IfAction_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalFCL.g:476:3: this_WhileAction_4= ruleWhileAction
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getActionAccess().getWhileActionParserRuleCall_4());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_WhileAction_4=ruleWhileAction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_WhileAction_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 6 :
                    // InternalFCL.g:485:3: this_ProcedureCall_5= ruleProcedureCall
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getActionAccess().getProcedureCallParserRuleCall_5());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ProcedureCall_5=ruleProcedureCall();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ProcedureCall_5;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 7 :
                    // InternalFCL.g:494:3: this_VarDecl_Impl_6= ruleVarDecl_Impl
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getActionAccess().getVarDecl_ImplParserRuleCall_6());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_VarDecl_Impl_6=ruleVarDecl_Impl();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_VarDecl_Impl_6;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 8 :
                    // InternalFCL.g:503:3: this_FCLProcedureReturn_7= ruleFCLProcedureReturn
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getActionAccess().getFCLProcedureReturnParserRuleCall_7());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_FCLProcedureReturn_7=ruleFCLProcedureReturn();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_FCLProcedureReturn_7;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 9 :
                    // InternalFCL.g:512:3: this_Log_8= ruleLog
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getActionAccess().getLogParserRuleCall_8());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Log_8=ruleLog();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Log_8;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleMode"
    // InternalFCL.g:524:1: entryRuleMode returns [EObject current=null] : iv_ruleMode= ruleMode EOF ;
    public final EObject entryRuleMode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMode = null;


        try {
            // InternalFCL.g:524:45: (iv_ruleMode= ruleMode EOF )
            // InternalFCL.g:525:2: iv_ruleMode= ruleMode EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getModeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMode=ruleMode();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMode; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMode"


    // $ANTLR start "ruleMode"
    // InternalFCL.g:531:1: ruleMode returns [EObject current=null] : (this_Mode_Impl_0= ruleMode_Impl | this_ModeAutomata_Impl_1= ruleModeAutomata_Impl ) ;
    public final EObject ruleMode() throws RecognitionException {
        EObject current = null;

        EObject this_Mode_Impl_0 = null;

        EObject this_ModeAutomata_Impl_1 = null;



        	enterRule();

        try {
            // InternalFCL.g:537:2: ( (this_Mode_Impl_0= ruleMode_Impl | this_ModeAutomata_Impl_1= ruleModeAutomata_Impl ) )
            // InternalFCL.g:538:2: (this_Mode_Impl_0= ruleMode_Impl | this_ModeAutomata_Impl_1= ruleModeAutomata_Impl )
            {
            // InternalFCL.g:538:2: (this_Mode_Impl_0= ruleMode_Impl | this_ModeAutomata_Impl_1= ruleModeAutomata_Impl )
            int alt10=2;
            switch ( input.LA(1) ) {
            case 21:
                {
                int LA10_1 = input.LA(2);

                if ( (LA10_1==22) ) {
                    alt10=1;
                }
                else if ( (LA10_1==17) ) {
                    alt10=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 10, 1, input);

                    throw nvae;
                }
                }
                break;
            case 22:
                {
                alt10=1;
                }
                break;
            case 17:
                {
                alt10=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalFCL.g:539:3: this_Mode_Impl_0= ruleMode_Impl
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getModeAccess().getMode_ImplParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Mode_Impl_0=ruleMode_Impl();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Mode_Impl_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalFCL.g:548:3: this_ModeAutomata_Impl_1= ruleModeAutomata_Impl
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getModeAccess().getModeAutomata_ImplParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ModeAutomata_Impl_1=ruleModeAutomata_Impl();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ModeAutomata_Impl_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMode"


    // $ANTLR start "entryRuleFunctionPort"
    // InternalFCL.g:560:1: entryRuleFunctionPort returns [EObject current=null] : iv_ruleFunctionPort= ruleFunctionPort EOF ;
    public final EObject entryRuleFunctionPort() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionPort = null;


        try {
            // InternalFCL.g:560:53: (iv_ruleFunctionPort= ruleFunctionPort EOF )
            // InternalFCL.g:561:2: iv_ruleFunctionPort= ruleFunctionPort EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFunctionPortRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFunctionPort=ruleFunctionPort();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFunctionPort; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionPort"


    // $ANTLR start "ruleFunctionPort"
    // InternalFCL.g:567:1: ruleFunctionPort returns [EObject current=null] : (this_FunctionBlockDataPort_0= ruleFunctionBlockDataPort | this_FunctionPortReference_1= ruleFunctionPortReference ) ;
    public final EObject ruleFunctionPort() throws RecognitionException {
        EObject current = null;

        EObject this_FunctionBlockDataPort_0 = null;

        EObject this_FunctionPortReference_1 = null;



        	enterRule();

        try {
            // InternalFCL.g:573:2: ( (this_FunctionBlockDataPort_0= ruleFunctionBlockDataPort | this_FunctionPortReference_1= ruleFunctionPortReference ) )
            // InternalFCL.g:574:2: (this_FunctionBlockDataPort_0= ruleFunctionBlockDataPort | this_FunctionPortReference_1= ruleFunctionPortReference )
            {
            // InternalFCL.g:574:2: (this_FunctionBlockDataPort_0= ruleFunctionBlockDataPort | this_FunctionPortReference_1= ruleFunctionPortReference )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==33) ) {
                alt11=1;
            }
            else if ( (LA11_0==40) ) {
                alt11=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalFCL.g:575:3: this_FunctionBlockDataPort_0= ruleFunctionBlockDataPort
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getFunctionPortAccess().getFunctionBlockDataPortParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_FunctionBlockDataPort_0=ruleFunctionBlockDataPort();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_FunctionBlockDataPort_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalFCL.g:584:3: this_FunctionPortReference_1= ruleFunctionPortReference
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getFunctionPortAccess().getFunctionPortReferenceParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_FunctionPortReference_1=ruleFunctionPortReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_FunctionPortReference_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionPort"


    // $ANTLR start "entryRuleExpression"
    // InternalFCL.g:596:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalFCL.g:596:51: (iv_ruleExpression= ruleExpression EOF )
            // InternalFCL.g:597:2: iv_ruleExpression= ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalFCL.g:603:1: ruleExpression returns [EObject current=null] : this_UnaryOrPrimaryExpression_0= ruleUnaryOrPrimaryExpression ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_UnaryOrPrimaryExpression_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:609:2: (this_UnaryOrPrimaryExpression_0= ruleUnaryOrPrimaryExpression )
            // InternalFCL.g:610:2: this_UnaryOrPrimaryExpression_0= ruleUnaryOrPrimaryExpression
            {
            if ( state.backtracking==0 ) {

              		newCompositeNode(grammarAccess.getExpressionAccess().getUnaryOrPrimaryExpressionParserRuleCall());
              	
            }
            pushFollow(FOLLOW_2);
            this_UnaryOrPrimaryExpression_0=ruleUnaryOrPrimaryExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current = this_UnaryOrPrimaryExpression_0;
              		afterParserOrEnumRuleCall();
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleMainAutomata"
    // InternalFCL.g:621:1: entryRuleMainAutomata returns [EObject current=null] : iv_ruleMainAutomata= ruleMainAutomata EOF ;
    public final EObject entryRuleMainAutomata() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMainAutomata = null;


        try {
            // InternalFCL.g:621:53: (iv_ruleMainAutomata= ruleMainAutomata EOF )
            // InternalFCL.g:622:2: iv_ruleMainAutomata= ruleMainAutomata EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMainAutomataRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMainAutomata=ruleMainAutomata();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMainAutomata; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMainAutomata"


    // $ANTLR start "ruleMainAutomata"
    // InternalFCL.g:628:1: ruleMainAutomata returns [EObject current=null] : ( () otherlv_1= 'modeAutomata' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* ) ) ) ( ( (lv_modes_9_0= ruleMode ) ) | ( (lv_transitions_10_0= ruleTransition ) ) )* ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* ) ) ) otherlv_20= '}' ) ;
    public final EObject ruleMainAutomata() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_7=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        EObject lv_modes_5_0 = null;

        EObject lv_transitions_6_0 = null;

        EObject lv_modes_9_0 = null;

        EObject lv_transitions_10_0 = null;

        EObject lv_entry_14_0 = null;

        EObject lv_exit_18_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:634:2: ( ( () otherlv_1= 'modeAutomata' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* ) ) ) ( ( (lv_modes_9_0= ruleMode ) ) | ( (lv_transitions_10_0= ruleTransition ) ) )* ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* ) ) ) otherlv_20= '}' ) )
            // InternalFCL.g:635:2: ( () otherlv_1= 'modeAutomata' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* ) ) ) ( ( (lv_modes_9_0= ruleMode ) ) | ( (lv_transitions_10_0= ruleTransition ) ) )* ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* ) ) ) otherlv_20= '}' )
            {
            // InternalFCL.g:635:2: ( () otherlv_1= 'modeAutomata' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* ) ) ) ( ( (lv_modes_9_0= ruleMode ) ) | ( (lv_transitions_10_0= ruleTransition ) ) )* ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* ) ) ) otherlv_20= '}' )
            // InternalFCL.g:636:3: () otherlv_1= 'modeAutomata' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* ) ) ) ( ( (lv_modes_9_0= ruleMode ) ) | ( (lv_transitions_10_0= ruleTransition ) ) )* ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* ) ) ) otherlv_20= '}'
            {
            // InternalFCL.g:636:3: ()
            // InternalFCL.g:637:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getMainAutomataAccess().getModeAutomataAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,17,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getMainAutomataAccess().getModeAutomataKeyword_1());
              		
            }
            // InternalFCL.g:647:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalFCL.g:648:4: (lv_name_2_0= RULE_ID )
            {
            // InternalFCL.g:648:4: (lv_name_2_0= RULE_ID )
            // InternalFCL.g:649:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getMainAutomataAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getMainAutomataRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_17); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getMainAutomataAccess().getLeftCurlyBracketKeyword_3());
              		
            }
            // InternalFCL.g:669:3: ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* ) ) )
            // InternalFCL.g:670:4: ( ( ( ({...}? => ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* ) )
            {
            // InternalFCL.g:670:4: ( ( ( ({...}? => ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* ) )
            // InternalFCL.g:671:5: ( ( ({...}? => ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* )
            {
            getUnorderedGroupHelper().enter(grammarAccess.getMainAutomataAccess().getUnorderedGroup_4());
            // InternalFCL.g:674:5: ( ( ({...}? => ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* )
            // InternalFCL.g:675:6: ( ({...}? => ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )*
            {
            // InternalFCL.g:675:6: ( ({...}? => ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )*
            loop14:
            do {
                int alt14=3;
                alt14 = dfa14.predict(input);
                switch (alt14) {
            	case 1 :
            	    // InternalFCL.g:676:4: ({...}? => ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ ) )
            	    {
            	    // InternalFCL.g:676:4: ({...}? => ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ ) )
            	    // InternalFCL.g:677:5: {...}? => ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMainAutomataAccess().getUnorderedGroup_4(), 0) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleMainAutomata", "getUnorderedGroupHelper().canSelect(grammarAccess.getMainAutomataAccess().getUnorderedGroup_4(), 0)");
            	    }
            	    // InternalFCL.g:677:109: ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ )
            	    // InternalFCL.g:678:6: ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getMainAutomataAccess().getUnorderedGroup_4(), 0);
            	    // InternalFCL.g:681:9: ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+
            	    int cnt13=0;
            	    loop13:
            	    do {
            	        int alt13=2;
            	        switch ( input.LA(1) ) {
            	        case 21:
            	            {
            	            int LA13_1 = input.LA(2);

            	            if ( ((true)) ) {
            	                alt13=1;
            	            }


            	            }
            	            break;
            	        case 22:
            	            {
            	            int LA13_2 = input.LA(2);

            	            if ( ((true)) ) {
            	                alt13=1;
            	            }


            	            }
            	            break;
            	        case 17:
            	            {
            	            int LA13_3 = input.LA(2);

            	            if ( ((true)) ) {
            	                alt13=1;
            	            }


            	            }
            	            break;
            	        case 27:
            	            {
            	            int LA13_4 = input.LA(2);

            	            if ( ((true)) ) {
            	                alt13=1;
            	            }


            	            }
            	            break;

            	        }

            	        switch (alt13) {
            	    	case 1 :
            	    	    // InternalFCL.g:681:10: {...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) )
            	    	    {
            	    	    if ( !((true)) ) {
            	    	        if (state.backtracking>0) {state.failed=true; return current;}
            	    	        throw new FailedPredicateException(input, "ruleMainAutomata", "true");
            	    	    }
            	    	    // InternalFCL.g:681:19: ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) )
            	    	    int alt12=2;
            	    	    int LA12_0 = input.LA(1);

            	    	    if ( (LA12_0==17||(LA12_0>=21 && LA12_0<=22)) ) {
            	    	        alt12=1;
            	    	    }
            	    	    else if ( (LA12_0==27) ) {
            	    	        alt12=2;
            	    	    }
            	    	    else {
            	    	        if (state.backtracking>0) {state.failed=true; return current;}
            	    	        NoViableAltException nvae =
            	    	            new NoViableAltException("", 12, 0, input);

            	    	        throw nvae;
            	    	    }
            	    	    switch (alt12) {
            	    	        case 1 :
            	    	            // InternalFCL.g:681:20: ( (lv_modes_5_0= ruleMode ) )
            	    	            {
            	    	            // InternalFCL.g:681:20: ( (lv_modes_5_0= ruleMode ) )
            	    	            // InternalFCL.g:682:10: (lv_modes_5_0= ruleMode )
            	    	            {
            	    	            // InternalFCL.g:682:10: (lv_modes_5_0= ruleMode )
            	    	            // InternalFCL.g:683:11: lv_modes_5_0= ruleMode
            	    	            {
            	    	            if ( state.backtracking==0 ) {

            	    	              											newCompositeNode(grammarAccess.getMainAutomataAccess().getModesModeParserRuleCall_4_0_0_0());
            	    	              										
            	    	            }
            	    	            pushFollow(FOLLOW_17);
            	    	            lv_modes_5_0=ruleMode();

            	    	            state._fsp--;
            	    	            if (state.failed) return current;
            	    	            if ( state.backtracking==0 ) {

            	    	              											if (current==null) {
            	    	              												current = createModelElementForParent(grammarAccess.getMainAutomataRule());
            	    	              											}
            	    	              											add(
            	    	              												current,
            	    	              												"modes",
            	    	              												lv_modes_5_0,
            	    	              												"fr.inria.glose.fcl.FCL.Mode");
            	    	              											afterParserOrEnumRuleCall();
            	    	              										
            	    	            }

            	    	            }


            	    	            }


            	    	            }
            	    	            break;
            	    	        case 2 :
            	    	            // InternalFCL.g:701:9: ( (lv_transitions_6_0= ruleTransition ) )
            	    	            {
            	    	            // InternalFCL.g:701:9: ( (lv_transitions_6_0= ruleTransition ) )
            	    	            // InternalFCL.g:702:10: (lv_transitions_6_0= ruleTransition )
            	    	            {
            	    	            // InternalFCL.g:702:10: (lv_transitions_6_0= ruleTransition )
            	    	            // InternalFCL.g:703:11: lv_transitions_6_0= ruleTransition
            	    	            {
            	    	            if ( state.backtracking==0 ) {

            	    	              											newCompositeNode(grammarAccess.getMainAutomataAccess().getTransitionsTransitionParserRuleCall_4_0_1_0());
            	    	              										
            	    	            }
            	    	            pushFollow(FOLLOW_17);
            	    	            lv_transitions_6_0=ruleTransition();

            	    	            state._fsp--;
            	    	            if (state.failed) return current;
            	    	            if ( state.backtracking==0 ) {

            	    	              											if (current==null) {
            	    	              												current = createModelElementForParent(grammarAccess.getMainAutomataRule());
            	    	              											}
            	    	              											add(
            	    	              												current,
            	    	              												"transitions",
            	    	              												lv_transitions_6_0,
            	    	              												"fr.inria.glose.fcl.FCL.Transition");
            	    	              											afterParserOrEnumRuleCall();
            	    	              										
            	    	            }

            	    	            }


            	    	            }


            	    	            }
            	    	            break;

            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    if ( cnt13 >= 1 ) break loop13;
            	    	    if (state.backtracking>0) {state.failed=true; return current;}
            	                EarlyExitException eee =
            	                    new EarlyExitException(13, input);
            	                throw eee;
            	        }
            	        cnt13++;
            	    } while (true);

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getMainAutomataAccess().getUnorderedGroup_4());

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalFCL.g:726:4: ({...}? => ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) )
            	    {
            	    // InternalFCL.g:726:4: ({...}? => ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) )
            	    // InternalFCL.g:727:5: {...}? => ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMainAutomataAccess().getUnorderedGroup_4(), 1) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleMainAutomata", "getUnorderedGroupHelper().canSelect(grammarAccess.getMainAutomataAccess().getUnorderedGroup_4(), 1)");
            	    }
            	    // InternalFCL.g:727:109: ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) )
            	    // InternalFCL.g:728:6: ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getMainAutomataAccess().getUnorderedGroup_4(), 1);
            	    // InternalFCL.g:731:9: ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) )
            	    // InternalFCL.g:731:10: {...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleMainAutomata", "true");
            	    }
            	    // InternalFCL.g:731:19: (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) )
            	    // InternalFCL.g:731:20: otherlv_7= 'initialMode' ( ( ruleQualifiedName ) )
            	    {
            	    otherlv_7=(Token)match(input,18,FOLLOW_3); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_7, grammarAccess.getMainAutomataAccess().getInitialModeKeyword_4_1_0());
            	      								
            	    }
            	    // InternalFCL.g:735:9: ( ( ruleQualifiedName ) )
            	    // InternalFCL.g:736:10: ( ruleQualifiedName )
            	    {
            	    // InternalFCL.g:736:10: ( ruleQualifiedName )
            	    // InternalFCL.g:737:11: ruleQualifiedName
            	    {
            	    if ( state.backtracking==0 ) {

            	      											if (current==null) {
            	      												current = createModelElement(grammarAccess.getMainAutomataRule());
            	      											}
            	      										
            	    }
            	    if ( state.backtracking==0 ) {

            	      											newCompositeNode(grammarAccess.getMainAutomataAccess().getInitialModeModeCrossReference_4_1_1_0());
            	      										
            	    }
            	    pushFollow(FOLLOW_17);
            	    ruleQualifiedName();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      											afterParserOrEnumRuleCall();
            	      										
            	    }

            	    }


            	    }


            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getMainAutomataAccess().getUnorderedGroup_4());

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);


            }


            }

            getUnorderedGroupHelper().leave(grammarAccess.getMainAutomataAccess().getUnorderedGroup_4());

            }

            // InternalFCL.g:764:3: ( ( (lv_modes_9_0= ruleMode ) ) | ( (lv_transitions_10_0= ruleTransition ) ) )*
            loop15:
            do {
                int alt15=3;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==17||(LA15_0>=21 && LA15_0<=22)) ) {
                    alt15=1;
                }
                else if ( (LA15_0==27) ) {
                    alt15=2;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalFCL.g:765:4: ( (lv_modes_9_0= ruleMode ) )
            	    {
            	    // InternalFCL.g:765:4: ( (lv_modes_9_0= ruleMode ) )
            	    // InternalFCL.g:766:5: (lv_modes_9_0= ruleMode )
            	    {
            	    // InternalFCL.g:766:5: (lv_modes_9_0= ruleMode )
            	    // InternalFCL.g:767:6: lv_modes_9_0= ruleMode
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getMainAutomataAccess().getModesModeParserRuleCall_5_0_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_18);
            	    lv_modes_9_0=ruleMode();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getMainAutomataRule());
            	      						}
            	      						add(
            	      							current,
            	      							"modes",
            	      							lv_modes_9_0,
            	      							"fr.inria.glose.fcl.FCL.Mode");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalFCL.g:785:4: ( (lv_transitions_10_0= ruleTransition ) )
            	    {
            	    // InternalFCL.g:785:4: ( (lv_transitions_10_0= ruleTransition ) )
            	    // InternalFCL.g:786:5: (lv_transitions_10_0= ruleTransition )
            	    {
            	    // InternalFCL.g:786:5: (lv_transitions_10_0= ruleTransition )
            	    // InternalFCL.g:787:6: lv_transitions_10_0= ruleTransition
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getMainAutomataAccess().getTransitionsTransitionParserRuleCall_5_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_18);
            	    lv_transitions_10_0=ruleTransition();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getMainAutomataRule());
            	      						}
            	      						add(
            	      							current,
            	      							"transitions",
            	      							lv_transitions_10_0,
            	      							"fr.inria.glose.fcl.FCL.Transition");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            // InternalFCL.g:805:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* ) ) )
            // InternalFCL.g:806:4: ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* ) )
            {
            // InternalFCL.g:806:4: ( ( ( ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* ) )
            // InternalFCL.g:807:5: ( ( ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* )
            {
            getUnorderedGroupHelper().enter(grammarAccess.getMainAutomataAccess().getUnorderedGroup_6());
            // InternalFCL.g:810:5: ( ( ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* )
            // InternalFCL.g:811:6: ( ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )*
            {
            // InternalFCL.g:811:6: ( ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )*
            loop16:
            do {
                int alt16=3;
                int LA16_0 = input.LA(1);

                if ( LA16_0 == 19 && getUnorderedGroupHelper().canSelect(grammarAccess.getMainAutomataAccess().getUnorderedGroup_6(), 0) ) {
                    alt16=1;
                }
                else if ( LA16_0 == 20 && getUnorderedGroupHelper().canSelect(grammarAccess.getMainAutomataAccess().getUnorderedGroup_6(), 1) ) {
                    alt16=2;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalFCL.g:812:4: ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) )
            	    {
            	    // InternalFCL.g:812:4: ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) )
            	    // InternalFCL.g:813:5: {...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMainAutomataAccess().getUnorderedGroup_6(), 0) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleMainAutomata", "getUnorderedGroupHelper().canSelect(grammarAccess.getMainAutomataAccess().getUnorderedGroup_6(), 0)");
            	    }
            	    // InternalFCL.g:813:109: ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) )
            	    // InternalFCL.g:814:6: ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getMainAutomataAccess().getUnorderedGroup_6(), 0);
            	    // InternalFCL.g:817:9: ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) )
            	    // InternalFCL.g:817:10: {...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleMainAutomata", "true");
            	    }
            	    // InternalFCL.g:817:19: (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' )
            	    // InternalFCL.g:817:20: otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}'
            	    {
            	    otherlv_12=(Token)match(input,19,FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_12, grammarAccess.getMainAutomataAccess().getOnEntryKeyword_6_0_0());
            	      								
            	    }
            	    otherlv_13=(Token)match(input,12,FOLLOW_19); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_13, grammarAccess.getMainAutomataAccess().getLeftCurlyBracketKeyword_6_0_1());
            	      								
            	    }
            	    // InternalFCL.g:825:9: ( (lv_entry_14_0= ruleAction ) )
            	    // InternalFCL.g:826:10: (lv_entry_14_0= ruleAction )
            	    {
            	    // InternalFCL.g:826:10: (lv_entry_14_0= ruleAction )
            	    // InternalFCL.g:827:11: lv_entry_14_0= ruleAction
            	    {
            	    if ( state.backtracking==0 ) {

            	      											newCompositeNode(grammarAccess.getMainAutomataAccess().getEntryActionParserRuleCall_6_0_2_0());
            	      										
            	    }
            	    pushFollow(FOLLOW_16);
            	    lv_entry_14_0=ruleAction();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      											if (current==null) {
            	      												current = createModelElementForParent(grammarAccess.getMainAutomataRule());
            	      											}
            	      											set(
            	      												current,
            	      												"entry",
            	      												lv_entry_14_0,
            	      												"fr.inria.glose.fcl.FCL.Action");
            	      											afterParserOrEnumRuleCall();
            	      										
            	    }

            	    }


            	    }

            	    otherlv_15=(Token)match(input,14,FOLLOW_20); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_15, grammarAccess.getMainAutomataAccess().getRightCurlyBracketKeyword_6_0_3());
            	      								
            	    }

            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getMainAutomataAccess().getUnorderedGroup_6());

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalFCL.g:854:4: ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) )
            	    {
            	    // InternalFCL.g:854:4: ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) )
            	    // InternalFCL.g:855:5: {...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMainAutomataAccess().getUnorderedGroup_6(), 1) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleMainAutomata", "getUnorderedGroupHelper().canSelect(grammarAccess.getMainAutomataAccess().getUnorderedGroup_6(), 1)");
            	    }
            	    // InternalFCL.g:855:109: ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) )
            	    // InternalFCL.g:856:6: ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getMainAutomataAccess().getUnorderedGroup_6(), 1);
            	    // InternalFCL.g:859:9: ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) )
            	    // InternalFCL.g:859:10: {...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleMainAutomata", "true");
            	    }
            	    // InternalFCL.g:859:19: (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' )
            	    // InternalFCL.g:859:20: otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}'
            	    {
            	    otherlv_16=(Token)match(input,20,FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_16, grammarAccess.getMainAutomataAccess().getOnExitKeyword_6_1_0());
            	      								
            	    }
            	    otherlv_17=(Token)match(input,12,FOLLOW_19); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_17, grammarAccess.getMainAutomataAccess().getLeftCurlyBracketKeyword_6_1_1());
            	      								
            	    }
            	    // InternalFCL.g:867:9: ( (lv_exit_18_0= ruleAction ) )
            	    // InternalFCL.g:868:10: (lv_exit_18_0= ruleAction )
            	    {
            	    // InternalFCL.g:868:10: (lv_exit_18_0= ruleAction )
            	    // InternalFCL.g:869:11: lv_exit_18_0= ruleAction
            	    {
            	    if ( state.backtracking==0 ) {

            	      											newCompositeNode(grammarAccess.getMainAutomataAccess().getExitActionParserRuleCall_6_1_2_0());
            	      										
            	    }
            	    pushFollow(FOLLOW_16);
            	    lv_exit_18_0=ruleAction();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      											if (current==null) {
            	      												current = createModelElementForParent(grammarAccess.getMainAutomataRule());
            	      											}
            	      											set(
            	      												current,
            	      												"exit",
            	      												lv_exit_18_0,
            	      												"fr.inria.glose.fcl.FCL.Action");
            	      											afterParserOrEnumRuleCall();
            	      										
            	    }

            	    }


            	    }

            	    otherlv_19=(Token)match(input,14,FOLLOW_20); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_19, grammarAccess.getMainAutomataAccess().getRightCurlyBracketKeyword_6_1_3());
            	      								
            	    }

            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getMainAutomataAccess().getUnorderedGroup_6());

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            }


            }

            getUnorderedGroupHelper().leave(grammarAccess.getMainAutomataAccess().getUnorderedGroup_6());

            }

            otherlv_20=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_20, grammarAccess.getMainAutomataAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMainAutomata"


    // $ANTLR start "entryRuleModeAutomata_Impl"
    // InternalFCL.g:911:1: entryRuleModeAutomata_Impl returns [EObject current=null] : iv_ruleModeAutomata_Impl= ruleModeAutomata_Impl EOF ;
    public final EObject entryRuleModeAutomata_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModeAutomata_Impl = null;


        try {
            // InternalFCL.g:911:58: (iv_ruleModeAutomata_Impl= ruleModeAutomata_Impl EOF )
            // InternalFCL.g:912:2: iv_ruleModeAutomata_Impl= ruleModeAutomata_Impl EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getModeAutomata_ImplRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleModeAutomata_Impl=ruleModeAutomata_Impl();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleModeAutomata_Impl; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModeAutomata_Impl"


    // $ANTLR start "ruleModeAutomata_Impl"
    // InternalFCL.g:918:1: ruleModeAutomata_Impl returns [EObject current=null] : ( () ( (lv_final_1_0= 'final' ) )? otherlv_2= 'modeAutomata' ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '{' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* ) ) ) ( ( (lv_modes_10_0= ruleMode ) ) | ( (lv_transitions_11_0= ruleTransition ) ) )* ( ( ( ( ({...}? => ( ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) ) ) ) )* ) ) ) otherlv_21= '}' ) ;
    public final EObject ruleModeAutomata_Impl() throws RecognitionException {
        EObject current = null;

        Token lv_final_1_0=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_8=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        EObject lv_modes_6_0 = null;

        EObject lv_transitions_7_0 = null;

        EObject lv_modes_10_0 = null;

        EObject lv_transitions_11_0 = null;

        EObject lv_entry_15_0 = null;

        EObject lv_exit_19_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:924:2: ( ( () ( (lv_final_1_0= 'final' ) )? otherlv_2= 'modeAutomata' ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '{' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* ) ) ) ( ( (lv_modes_10_0= ruleMode ) ) | ( (lv_transitions_11_0= ruleTransition ) ) )* ( ( ( ( ({...}? => ( ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) ) ) ) )* ) ) ) otherlv_21= '}' ) )
            // InternalFCL.g:925:2: ( () ( (lv_final_1_0= 'final' ) )? otherlv_2= 'modeAutomata' ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '{' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* ) ) ) ( ( (lv_modes_10_0= ruleMode ) ) | ( (lv_transitions_11_0= ruleTransition ) ) )* ( ( ( ( ({...}? => ( ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) ) ) ) )* ) ) ) otherlv_21= '}' )
            {
            // InternalFCL.g:925:2: ( () ( (lv_final_1_0= 'final' ) )? otherlv_2= 'modeAutomata' ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '{' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* ) ) ) ( ( (lv_modes_10_0= ruleMode ) ) | ( (lv_transitions_11_0= ruleTransition ) ) )* ( ( ( ( ({...}? => ( ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) ) ) ) )* ) ) ) otherlv_21= '}' )
            // InternalFCL.g:926:3: () ( (lv_final_1_0= 'final' ) )? otherlv_2= 'modeAutomata' ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '{' ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* ) ) ) ( ( (lv_modes_10_0= ruleMode ) ) | ( (lv_transitions_11_0= ruleTransition ) ) )* ( ( ( ( ({...}? => ( ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) ) ) ) )* ) ) ) otherlv_21= '}'
            {
            // InternalFCL.g:926:3: ()
            // InternalFCL.g:927:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getModeAutomata_ImplAccess().getModeAutomataAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:933:3: ( (lv_final_1_0= 'final' ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==21) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalFCL.g:934:4: (lv_final_1_0= 'final' )
                    {
                    // InternalFCL.g:934:4: (lv_final_1_0= 'final' )
                    // InternalFCL.g:935:5: lv_final_1_0= 'final'
                    {
                    lv_final_1_0=(Token)match(input,21,FOLLOW_5); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_final_1_0, grammarAccess.getModeAutomata_ImplAccess().getFinalFinalKeyword_1_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getModeAutomata_ImplRule());
                      					}
                      					setWithLastConsumed(current, "final", true, "final");
                      				
                    }

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,17,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getModeAutomata_ImplAccess().getModeAutomataKeyword_2());
              		
            }
            // InternalFCL.g:951:3: ( (lv_name_3_0= RULE_ID ) )
            // InternalFCL.g:952:4: (lv_name_3_0= RULE_ID )
            {
            // InternalFCL.g:952:4: (lv_name_3_0= RULE_ID )
            // InternalFCL.g:953:5: lv_name_3_0= RULE_ID
            {
            lv_name_3_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_3_0, grammarAccess.getModeAutomata_ImplAccess().getNameIDTerminalRuleCall_3_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getModeAutomata_ImplRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_3_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_4=(Token)match(input,12,FOLLOW_17); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getModeAutomata_ImplAccess().getLeftCurlyBracketKeyword_4());
              		
            }
            // InternalFCL.g:973:3: ( ( ( ( ({...}? => ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* ) ) )
            // InternalFCL.g:974:4: ( ( ( ({...}? => ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* ) )
            {
            // InternalFCL.g:974:4: ( ( ( ({...}? => ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* ) )
            // InternalFCL.g:975:5: ( ( ({...}? => ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* )
            {
            getUnorderedGroupHelper().enter(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_5());
            // InternalFCL.g:978:5: ( ( ({...}? => ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )* )
            // InternalFCL.g:979:6: ( ({...}? => ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )*
            {
            // InternalFCL.g:979:6: ( ({...}? => ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )*
            loop20:
            do {
                int alt20=3;
                alt20 = dfa20.predict(input);
                switch (alt20) {
            	case 1 :
            	    // InternalFCL.g:980:4: ({...}? => ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ ) )
            	    {
            	    // InternalFCL.g:980:4: ({...}? => ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ ) )
            	    // InternalFCL.g:981:5: {...}? => ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_5(), 0) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleModeAutomata_Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_5(), 0)");
            	    }
            	    // InternalFCL.g:981:114: ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ )
            	    // InternalFCL.g:982:6: ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_5(), 0);
            	    // InternalFCL.g:985:9: ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+
            	    int cnt19=0;
            	    loop19:
            	    do {
            	        int alt19=2;
            	        switch ( input.LA(1) ) {
            	        case 21:
            	            {
            	            int LA19_1 = input.LA(2);

            	            if ( ((true)) ) {
            	                alt19=1;
            	            }


            	            }
            	            break;
            	        case 22:
            	            {
            	            int LA19_2 = input.LA(2);

            	            if ( ((true)) ) {
            	                alt19=1;
            	            }


            	            }
            	            break;
            	        case 17:
            	            {
            	            int LA19_3 = input.LA(2);

            	            if ( ((true)) ) {
            	                alt19=1;
            	            }


            	            }
            	            break;
            	        case 27:
            	            {
            	            int LA19_4 = input.LA(2);

            	            if ( ((true)) ) {
            	                alt19=1;
            	            }


            	            }
            	            break;

            	        }

            	        switch (alt19) {
            	    	case 1 :
            	    	    // InternalFCL.g:985:10: {...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) )
            	    	    {
            	    	    if ( !((true)) ) {
            	    	        if (state.backtracking>0) {state.failed=true; return current;}
            	    	        throw new FailedPredicateException(input, "ruleModeAutomata_Impl", "true");
            	    	    }
            	    	    // InternalFCL.g:985:19: ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) )
            	    	    int alt18=2;
            	    	    int LA18_0 = input.LA(1);

            	    	    if ( (LA18_0==17||(LA18_0>=21 && LA18_0<=22)) ) {
            	    	        alt18=1;
            	    	    }
            	    	    else if ( (LA18_0==27) ) {
            	    	        alt18=2;
            	    	    }
            	    	    else {
            	    	        if (state.backtracking>0) {state.failed=true; return current;}
            	    	        NoViableAltException nvae =
            	    	            new NoViableAltException("", 18, 0, input);

            	    	        throw nvae;
            	    	    }
            	    	    switch (alt18) {
            	    	        case 1 :
            	    	            // InternalFCL.g:985:20: ( (lv_modes_6_0= ruleMode ) )
            	    	            {
            	    	            // InternalFCL.g:985:20: ( (lv_modes_6_0= ruleMode ) )
            	    	            // InternalFCL.g:986:10: (lv_modes_6_0= ruleMode )
            	    	            {
            	    	            // InternalFCL.g:986:10: (lv_modes_6_0= ruleMode )
            	    	            // InternalFCL.g:987:11: lv_modes_6_0= ruleMode
            	    	            {
            	    	            if ( state.backtracking==0 ) {

            	    	              											newCompositeNode(grammarAccess.getModeAutomata_ImplAccess().getModesModeParserRuleCall_5_0_0_0());
            	    	              										
            	    	            }
            	    	            pushFollow(FOLLOW_17);
            	    	            lv_modes_6_0=ruleMode();

            	    	            state._fsp--;
            	    	            if (state.failed) return current;
            	    	            if ( state.backtracking==0 ) {

            	    	              											if (current==null) {
            	    	              												current = createModelElementForParent(grammarAccess.getModeAutomata_ImplRule());
            	    	              											}
            	    	              											add(
            	    	              												current,
            	    	              												"modes",
            	    	              												lv_modes_6_0,
            	    	              												"fr.inria.glose.fcl.FCL.Mode");
            	    	              											afterParserOrEnumRuleCall();
            	    	              										
            	    	            }

            	    	            }


            	    	            }


            	    	            }
            	    	            break;
            	    	        case 2 :
            	    	            // InternalFCL.g:1005:9: ( (lv_transitions_7_0= ruleTransition ) )
            	    	            {
            	    	            // InternalFCL.g:1005:9: ( (lv_transitions_7_0= ruleTransition ) )
            	    	            // InternalFCL.g:1006:10: (lv_transitions_7_0= ruleTransition )
            	    	            {
            	    	            // InternalFCL.g:1006:10: (lv_transitions_7_0= ruleTransition )
            	    	            // InternalFCL.g:1007:11: lv_transitions_7_0= ruleTransition
            	    	            {
            	    	            if ( state.backtracking==0 ) {

            	    	              											newCompositeNode(grammarAccess.getModeAutomata_ImplAccess().getTransitionsTransitionParserRuleCall_5_0_1_0());
            	    	              										
            	    	            }
            	    	            pushFollow(FOLLOW_17);
            	    	            lv_transitions_7_0=ruleTransition();

            	    	            state._fsp--;
            	    	            if (state.failed) return current;
            	    	            if ( state.backtracking==0 ) {

            	    	              											if (current==null) {
            	    	              												current = createModelElementForParent(grammarAccess.getModeAutomata_ImplRule());
            	    	              											}
            	    	              											add(
            	    	              												current,
            	    	              												"transitions",
            	    	              												lv_transitions_7_0,
            	    	              												"fr.inria.glose.fcl.FCL.Transition");
            	    	              											afterParserOrEnumRuleCall();
            	    	              										
            	    	            }

            	    	            }


            	    	            }


            	    	            }
            	    	            break;

            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    if ( cnt19 >= 1 ) break loop19;
            	    	    if (state.backtracking>0) {state.failed=true; return current;}
            	                EarlyExitException eee =
            	                    new EarlyExitException(19, input);
            	                throw eee;
            	        }
            	        cnt19++;
            	    } while (true);

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_5());

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalFCL.g:1030:4: ({...}? => ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) )
            	    {
            	    // InternalFCL.g:1030:4: ({...}? => ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) )
            	    // InternalFCL.g:1031:5: {...}? => ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_5(), 1) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleModeAutomata_Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_5(), 1)");
            	    }
            	    // InternalFCL.g:1031:114: ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) )
            	    // InternalFCL.g:1032:6: ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_5(), 1);
            	    // InternalFCL.g:1035:9: ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) )
            	    // InternalFCL.g:1035:10: {...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleModeAutomata_Impl", "true");
            	    }
            	    // InternalFCL.g:1035:19: (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) )
            	    // InternalFCL.g:1035:20: otherlv_8= 'initialMode' ( ( ruleQualifiedName ) )
            	    {
            	    otherlv_8=(Token)match(input,18,FOLLOW_3); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_8, grammarAccess.getModeAutomata_ImplAccess().getInitialModeKeyword_5_1_0());
            	      								
            	    }
            	    // InternalFCL.g:1039:9: ( ( ruleQualifiedName ) )
            	    // InternalFCL.g:1040:10: ( ruleQualifiedName )
            	    {
            	    // InternalFCL.g:1040:10: ( ruleQualifiedName )
            	    // InternalFCL.g:1041:11: ruleQualifiedName
            	    {
            	    if ( state.backtracking==0 ) {

            	      											if (current==null) {
            	      												current = createModelElement(grammarAccess.getModeAutomata_ImplRule());
            	      											}
            	      										
            	    }
            	    if ( state.backtracking==0 ) {

            	      											newCompositeNode(grammarAccess.getModeAutomata_ImplAccess().getInitialModeModeCrossReference_5_1_1_0());
            	      										
            	    }
            	    pushFollow(FOLLOW_17);
            	    ruleQualifiedName();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      											afterParserOrEnumRuleCall();
            	      										
            	    }

            	    }


            	    }


            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_5());

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            }


            }

            getUnorderedGroupHelper().leave(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_5());

            }

            // InternalFCL.g:1068:3: ( ( (lv_modes_10_0= ruleMode ) ) | ( (lv_transitions_11_0= ruleTransition ) ) )*
            loop21:
            do {
                int alt21=3;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==17||(LA21_0>=21 && LA21_0<=22)) ) {
                    alt21=1;
                }
                else if ( (LA21_0==27) ) {
                    alt21=2;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalFCL.g:1069:4: ( (lv_modes_10_0= ruleMode ) )
            	    {
            	    // InternalFCL.g:1069:4: ( (lv_modes_10_0= ruleMode ) )
            	    // InternalFCL.g:1070:5: (lv_modes_10_0= ruleMode )
            	    {
            	    // InternalFCL.g:1070:5: (lv_modes_10_0= ruleMode )
            	    // InternalFCL.g:1071:6: lv_modes_10_0= ruleMode
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getModeAutomata_ImplAccess().getModesModeParserRuleCall_6_0_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_18);
            	    lv_modes_10_0=ruleMode();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getModeAutomata_ImplRule());
            	      						}
            	      						add(
            	      							current,
            	      							"modes",
            	      							lv_modes_10_0,
            	      							"fr.inria.glose.fcl.FCL.Mode");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalFCL.g:1089:4: ( (lv_transitions_11_0= ruleTransition ) )
            	    {
            	    // InternalFCL.g:1089:4: ( (lv_transitions_11_0= ruleTransition ) )
            	    // InternalFCL.g:1090:5: (lv_transitions_11_0= ruleTransition )
            	    {
            	    // InternalFCL.g:1090:5: (lv_transitions_11_0= ruleTransition )
            	    // InternalFCL.g:1091:6: lv_transitions_11_0= ruleTransition
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getModeAutomata_ImplAccess().getTransitionsTransitionParserRuleCall_6_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_18);
            	    lv_transitions_11_0=ruleTransition();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getModeAutomata_ImplRule());
            	      						}
            	      						add(
            	      							current,
            	      							"transitions",
            	      							lv_transitions_11_0,
            	      							"fr.inria.glose.fcl.FCL.Transition");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

            // InternalFCL.g:1109:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) ) ) ) )* ) ) )
            // InternalFCL.g:1110:4: ( ( ( ({...}? => ( ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) ) ) ) )* ) )
            {
            // InternalFCL.g:1110:4: ( ( ( ({...}? => ( ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) ) ) ) )* ) )
            // InternalFCL.g:1111:5: ( ( ({...}? => ( ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) ) ) ) )* )
            {
            getUnorderedGroupHelper().enter(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_7());
            // InternalFCL.g:1114:5: ( ( ({...}? => ( ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) ) ) ) )* )
            // InternalFCL.g:1115:6: ( ({...}? => ( ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) ) ) ) )*
            {
            // InternalFCL.g:1115:6: ( ({...}? => ( ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) ) ) ) )*
            loop22:
            do {
                int alt22=3;
                int LA22_0 = input.LA(1);

                if ( LA22_0 == 19 && getUnorderedGroupHelper().canSelect(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_7(), 0) ) {
                    alt22=1;
                }
                else if ( LA22_0 == 20 && getUnorderedGroupHelper().canSelect(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_7(), 1) ) {
                    alt22=2;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalFCL.g:1116:4: ({...}? => ( ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) ) ) )
            	    {
            	    // InternalFCL.g:1116:4: ({...}? => ( ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) ) ) )
            	    // InternalFCL.g:1117:5: {...}? => ( ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_7(), 0) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleModeAutomata_Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_7(), 0)");
            	    }
            	    // InternalFCL.g:1117:114: ( ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) ) )
            	    // InternalFCL.g:1118:6: ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_7(), 0);
            	    // InternalFCL.g:1121:9: ({...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' ) )
            	    // InternalFCL.g:1121:10: {...}? => (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleModeAutomata_Impl", "true");
            	    }
            	    // InternalFCL.g:1121:19: (otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}' )
            	    // InternalFCL.g:1121:20: otherlv_13= 'onEntry' otherlv_14= '{' ( (lv_entry_15_0= ruleAction ) ) otherlv_16= '}'
            	    {
            	    otherlv_13=(Token)match(input,19,FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_13, grammarAccess.getModeAutomata_ImplAccess().getOnEntryKeyword_7_0_0());
            	      								
            	    }
            	    otherlv_14=(Token)match(input,12,FOLLOW_19); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_14, grammarAccess.getModeAutomata_ImplAccess().getLeftCurlyBracketKeyword_7_0_1());
            	      								
            	    }
            	    // InternalFCL.g:1129:9: ( (lv_entry_15_0= ruleAction ) )
            	    // InternalFCL.g:1130:10: (lv_entry_15_0= ruleAction )
            	    {
            	    // InternalFCL.g:1130:10: (lv_entry_15_0= ruleAction )
            	    // InternalFCL.g:1131:11: lv_entry_15_0= ruleAction
            	    {
            	    if ( state.backtracking==0 ) {

            	      											newCompositeNode(grammarAccess.getModeAutomata_ImplAccess().getEntryActionParserRuleCall_7_0_2_0());
            	      										
            	    }
            	    pushFollow(FOLLOW_16);
            	    lv_entry_15_0=ruleAction();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      											if (current==null) {
            	      												current = createModelElementForParent(grammarAccess.getModeAutomata_ImplRule());
            	      											}
            	      											set(
            	      												current,
            	      												"entry",
            	      												lv_entry_15_0,
            	      												"fr.inria.glose.fcl.FCL.Action");
            	      											afterParserOrEnumRuleCall();
            	      										
            	    }

            	    }


            	    }

            	    otherlv_16=(Token)match(input,14,FOLLOW_20); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_16, grammarAccess.getModeAutomata_ImplAccess().getRightCurlyBracketKeyword_7_0_3());
            	      								
            	    }

            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_7());

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalFCL.g:1158:4: ({...}? => ( ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) ) ) )
            	    {
            	    // InternalFCL.g:1158:4: ({...}? => ( ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) ) ) )
            	    // InternalFCL.g:1159:5: {...}? => ( ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_7(), 1) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleModeAutomata_Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_7(), 1)");
            	    }
            	    // InternalFCL.g:1159:114: ( ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) ) )
            	    // InternalFCL.g:1160:6: ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_7(), 1);
            	    // InternalFCL.g:1163:9: ({...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' ) )
            	    // InternalFCL.g:1163:10: {...}? => (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleModeAutomata_Impl", "true");
            	    }
            	    // InternalFCL.g:1163:19: (otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}' )
            	    // InternalFCL.g:1163:20: otherlv_17= 'onExit' otherlv_18= '{' ( (lv_exit_19_0= ruleAction ) ) otherlv_20= '}'
            	    {
            	    otherlv_17=(Token)match(input,20,FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_17, grammarAccess.getModeAutomata_ImplAccess().getOnExitKeyword_7_1_0());
            	      								
            	    }
            	    otherlv_18=(Token)match(input,12,FOLLOW_19); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_18, grammarAccess.getModeAutomata_ImplAccess().getLeftCurlyBracketKeyword_7_1_1());
            	      								
            	    }
            	    // InternalFCL.g:1171:9: ( (lv_exit_19_0= ruleAction ) )
            	    // InternalFCL.g:1172:10: (lv_exit_19_0= ruleAction )
            	    {
            	    // InternalFCL.g:1172:10: (lv_exit_19_0= ruleAction )
            	    // InternalFCL.g:1173:11: lv_exit_19_0= ruleAction
            	    {
            	    if ( state.backtracking==0 ) {

            	      											newCompositeNode(grammarAccess.getModeAutomata_ImplAccess().getExitActionParserRuleCall_7_1_2_0());
            	      										
            	    }
            	    pushFollow(FOLLOW_16);
            	    lv_exit_19_0=ruleAction();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      											if (current==null) {
            	      												current = createModelElementForParent(grammarAccess.getModeAutomata_ImplRule());
            	      											}
            	      											set(
            	      												current,
            	      												"exit",
            	      												lv_exit_19_0,
            	      												"fr.inria.glose.fcl.FCL.Action");
            	      											afterParserOrEnumRuleCall();
            	      										
            	    }

            	    }


            	    }

            	    otherlv_20=(Token)match(input,14,FOLLOW_20); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_20, grammarAccess.getModeAutomata_ImplAccess().getRightCurlyBracketKeyword_7_1_3());
            	      								
            	    }

            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_7());

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);


            }


            }

            getUnorderedGroupHelper().leave(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_7());

            }

            otherlv_21=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_21, grammarAccess.getModeAutomata_ImplAccess().getRightCurlyBracketKeyword_8());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModeAutomata_Impl"


    // $ANTLR start "entryRuleMode_Impl"
    // InternalFCL.g:1215:1: entryRuleMode_Impl returns [EObject current=null] : iv_ruleMode_Impl= ruleMode_Impl EOF ;
    public final EObject entryRuleMode_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMode_Impl = null;


        try {
            // InternalFCL.g:1215:50: (iv_ruleMode_Impl= ruleMode_Impl EOF )
            // InternalFCL.g:1216:2: iv_ruleMode_Impl= ruleMode_Impl EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMode_ImplRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMode_Impl=ruleMode_Impl();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMode_Impl; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMode_Impl"


    // $ANTLR start "ruleMode_Impl"
    // InternalFCL.g:1222:1: ruleMode_Impl returns [EObject current=null] : ( () ( (lv_final_1_0= 'final' ) )? otherlv_2= 'mode' ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* ) ) ) otherlv_20= '}' ) ;
    public final EObject ruleMode_Impl() throws RecognitionException {
        EObject current = null;

        Token lv_final_1_0=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        EObject lv_entry_14_0 = null;

        EObject lv_exit_18_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:1228:2: ( ( () ( (lv_final_1_0= 'final' ) )? otherlv_2= 'mode' ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* ) ) ) otherlv_20= '}' ) )
            // InternalFCL.g:1229:2: ( () ( (lv_final_1_0= 'final' ) )? otherlv_2= 'mode' ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* ) ) ) otherlv_20= '}' )
            {
            // InternalFCL.g:1229:2: ( () ( (lv_final_1_0= 'final' ) )? otherlv_2= 'mode' ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* ) ) ) otherlv_20= '}' )
            // InternalFCL.g:1230:3: () ( (lv_final_1_0= 'final' ) )? otherlv_2= 'mode' ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* ) ) ) otherlv_20= '}'
            {
            // InternalFCL.g:1230:3: ()
            // InternalFCL.g:1231:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getMode_ImplAccess().getModeAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:1237:3: ( (lv_final_1_0= 'final' ) )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==21) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalFCL.g:1238:4: (lv_final_1_0= 'final' )
                    {
                    // InternalFCL.g:1238:4: (lv_final_1_0= 'final' )
                    // InternalFCL.g:1239:5: lv_final_1_0= 'final'
                    {
                    lv_final_1_0=(Token)match(input,21,FOLLOW_21); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_final_1_0, grammarAccess.getMode_ImplAccess().getFinalFinalKeyword_1_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getMode_ImplRule());
                      					}
                      					setWithLastConsumed(current, "final", true, "final");
                      				
                    }

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,22,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getMode_ImplAccess().getModeKeyword_2());
              		
            }
            // InternalFCL.g:1255:3: ( (lv_name_3_0= RULE_ID ) )
            // InternalFCL.g:1256:4: (lv_name_3_0= RULE_ID )
            {
            // InternalFCL.g:1256:4: (lv_name_3_0= RULE_ID )
            // InternalFCL.g:1257:5: lv_name_3_0= RULE_ID
            {
            lv_name_3_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_3_0, grammarAccess.getMode_ImplAccess().getNameIDTerminalRuleCall_3_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getMode_ImplRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_3_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_4=(Token)match(input,12,FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getMode_ImplAccess().getLeftCurlyBracketKeyword_4());
              		
            }
            // InternalFCL.g:1277:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* ) ) )
            // InternalFCL.g:1278:4: ( ( ( ({...}? => ( ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* ) )
            {
            // InternalFCL.g:1278:4: ( ( ( ({...}? => ( ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* ) )
            // InternalFCL.g:1279:5: ( ( ({...}? => ( ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* )
            {
            getUnorderedGroupHelper().enter(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5());
            // InternalFCL.g:1282:5: ( ( ({...}? => ( ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )* )
            // InternalFCL.g:1283:6: ( ({...}? => ( ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )*
            {
            // InternalFCL.g:1283:6: ( ({...}? => ( ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) ) )*
            loop25:
            do {
                int alt25=4;
                int LA25_0 = input.LA(1);

                if ( LA25_0 == 23 && getUnorderedGroupHelper().canSelect(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5(), 0) ) {
                    alt25=1;
                }
                else if ( LA25_0 == 19 && getUnorderedGroupHelper().canSelect(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5(), 1) ) {
                    alt25=2;
                }
                else if ( LA25_0 == 20 && getUnorderedGroupHelper().canSelect(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5(), 2) ) {
                    alt25=3;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalFCL.g:1284:4: ({...}? => ( ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) ) ) )
            	    {
            	    // InternalFCL.g:1284:4: ({...}? => ( ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) ) ) )
            	    // InternalFCL.g:1285:5: {...}? => ( ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5(), 0) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleMode_Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5(), 0)");
            	    }
            	    // InternalFCL.g:1285:106: ( ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) ) )
            	    // InternalFCL.g:1286:6: ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5(), 0);
            	    // InternalFCL.g:1289:9: ({...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' ) )
            	    // InternalFCL.g:1289:10: {...}? => (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleMode_Impl", "true");
            	    }
            	    // InternalFCL.g:1289:19: (otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')' )
            	    // InternalFCL.g:1289:20: otherlv_6= 'enabledFunctions' otherlv_7= '(' ( ( ruleQualifiedName ) ) (otherlv_9= ',' ( ( ruleQualifiedName ) ) )* otherlv_11= ')'
            	    {
            	    otherlv_6=(Token)match(input,23,FOLLOW_23); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_6, grammarAccess.getMode_ImplAccess().getEnabledFunctionsKeyword_5_0_0());
            	      								
            	    }
            	    otherlv_7=(Token)match(input,24,FOLLOW_3); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_7, grammarAccess.getMode_ImplAccess().getLeftParenthesisKeyword_5_0_1());
            	      								
            	    }
            	    // InternalFCL.g:1297:9: ( ( ruleQualifiedName ) )
            	    // InternalFCL.g:1298:10: ( ruleQualifiedName )
            	    {
            	    // InternalFCL.g:1298:10: ( ruleQualifiedName )
            	    // InternalFCL.g:1299:11: ruleQualifiedName
            	    {
            	    if ( state.backtracking==0 ) {

            	      											if (current==null) {
            	      												current = createModelElement(grammarAccess.getMode_ImplRule());
            	      											}
            	      										
            	    }
            	    if ( state.backtracking==0 ) {

            	      											newCompositeNode(grammarAccess.getMode_ImplAccess().getEnabledFunctionsFunctionCrossReference_5_0_2_0());
            	      										
            	    }
            	    pushFollow(FOLLOW_24);
            	    ruleQualifiedName();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      											afterParserOrEnumRuleCall();
            	      										
            	    }

            	    }


            	    }

            	    // InternalFCL.g:1313:9: (otherlv_9= ',' ( ( ruleQualifiedName ) ) )*
            	    loop24:
            	    do {
            	        int alt24=2;
            	        int LA24_0 = input.LA(1);

            	        if ( (LA24_0==25) ) {
            	            alt24=1;
            	        }


            	        switch (alt24) {
            	    	case 1 :
            	    	    // InternalFCL.g:1314:10: otherlv_9= ',' ( ( ruleQualifiedName ) )
            	    	    {
            	    	    otherlv_9=(Token)match(input,25,FOLLOW_3); if (state.failed) return current;
            	    	    if ( state.backtracking==0 ) {

            	    	      										newLeafNode(otherlv_9, grammarAccess.getMode_ImplAccess().getCommaKeyword_5_0_3_0());
            	    	      									
            	    	    }
            	    	    // InternalFCL.g:1318:10: ( ( ruleQualifiedName ) )
            	    	    // InternalFCL.g:1319:11: ( ruleQualifiedName )
            	    	    {
            	    	    // InternalFCL.g:1319:11: ( ruleQualifiedName )
            	    	    // InternalFCL.g:1320:12: ruleQualifiedName
            	    	    {
            	    	    if ( state.backtracking==0 ) {

            	    	      												if (current==null) {
            	    	      													current = createModelElement(grammarAccess.getMode_ImplRule());
            	    	      												}
            	    	      											
            	    	    }
            	    	    if ( state.backtracking==0 ) {

            	    	      												newCompositeNode(grammarAccess.getMode_ImplAccess().getEnabledFunctionsFunctionCrossReference_5_0_3_1_0());
            	    	      											
            	    	    }
            	    	    pushFollow(FOLLOW_24);
            	    	    ruleQualifiedName();

            	    	    state._fsp--;
            	    	    if (state.failed) return current;
            	    	    if ( state.backtracking==0 ) {

            	    	      												afterParserOrEnumRuleCall();
            	    	      											
            	    	    }

            	    	    }


            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop24;
            	        }
            	    } while (true);

            	    otherlv_11=(Token)match(input,26,FOLLOW_22); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_11, grammarAccess.getMode_ImplAccess().getRightParenthesisKeyword_5_0_4());
            	      								
            	    }

            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5());

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalFCL.g:1345:4: ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) )
            	    {
            	    // InternalFCL.g:1345:4: ({...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) ) )
            	    // InternalFCL.g:1346:5: {...}? => ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5(), 1) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleMode_Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5(), 1)");
            	    }
            	    // InternalFCL.g:1346:106: ( ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) ) )
            	    // InternalFCL.g:1347:6: ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5(), 1);
            	    // InternalFCL.g:1350:9: ({...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' ) )
            	    // InternalFCL.g:1350:10: {...}? => (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleMode_Impl", "true");
            	    }
            	    // InternalFCL.g:1350:19: (otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}' )
            	    // InternalFCL.g:1350:20: otherlv_12= 'onEntry' otherlv_13= '{' ( (lv_entry_14_0= ruleAction ) ) otherlv_15= '}'
            	    {
            	    otherlv_12=(Token)match(input,19,FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_12, grammarAccess.getMode_ImplAccess().getOnEntryKeyword_5_1_0());
            	      								
            	    }
            	    otherlv_13=(Token)match(input,12,FOLLOW_19); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_13, grammarAccess.getMode_ImplAccess().getLeftCurlyBracketKeyword_5_1_1());
            	      								
            	    }
            	    // InternalFCL.g:1358:9: ( (lv_entry_14_0= ruleAction ) )
            	    // InternalFCL.g:1359:10: (lv_entry_14_0= ruleAction )
            	    {
            	    // InternalFCL.g:1359:10: (lv_entry_14_0= ruleAction )
            	    // InternalFCL.g:1360:11: lv_entry_14_0= ruleAction
            	    {
            	    if ( state.backtracking==0 ) {

            	      											newCompositeNode(grammarAccess.getMode_ImplAccess().getEntryActionParserRuleCall_5_1_2_0());
            	      										
            	    }
            	    pushFollow(FOLLOW_16);
            	    lv_entry_14_0=ruleAction();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      											if (current==null) {
            	      												current = createModelElementForParent(grammarAccess.getMode_ImplRule());
            	      											}
            	      											set(
            	      												current,
            	      												"entry",
            	      												lv_entry_14_0,
            	      												"fr.inria.glose.fcl.FCL.Action");
            	      											afterParserOrEnumRuleCall();
            	      										
            	    }

            	    }


            	    }

            	    otherlv_15=(Token)match(input,14,FOLLOW_22); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_15, grammarAccess.getMode_ImplAccess().getRightCurlyBracketKeyword_5_1_3());
            	      								
            	    }

            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5());

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalFCL.g:1387:4: ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) )
            	    {
            	    // InternalFCL.g:1387:4: ({...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) ) )
            	    // InternalFCL.g:1388:5: {...}? => ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5(), 2) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleMode_Impl", "getUnorderedGroupHelper().canSelect(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5(), 2)");
            	    }
            	    // InternalFCL.g:1388:106: ( ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) ) )
            	    // InternalFCL.g:1389:6: ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5(), 2);
            	    // InternalFCL.g:1392:9: ({...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' ) )
            	    // InternalFCL.g:1392:10: {...}? => (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleMode_Impl", "true");
            	    }
            	    // InternalFCL.g:1392:19: (otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}' )
            	    // InternalFCL.g:1392:20: otherlv_16= 'onExit' otherlv_17= '{' ( (lv_exit_18_0= ruleAction ) ) otherlv_19= '}'
            	    {
            	    otherlv_16=(Token)match(input,20,FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_16, grammarAccess.getMode_ImplAccess().getOnExitKeyword_5_2_0());
            	      								
            	    }
            	    otherlv_17=(Token)match(input,12,FOLLOW_19); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_17, grammarAccess.getMode_ImplAccess().getLeftCurlyBracketKeyword_5_2_1());
            	      								
            	    }
            	    // InternalFCL.g:1400:9: ( (lv_exit_18_0= ruleAction ) )
            	    // InternalFCL.g:1401:10: (lv_exit_18_0= ruleAction )
            	    {
            	    // InternalFCL.g:1401:10: (lv_exit_18_0= ruleAction )
            	    // InternalFCL.g:1402:11: lv_exit_18_0= ruleAction
            	    {
            	    if ( state.backtracking==0 ) {

            	      											newCompositeNode(grammarAccess.getMode_ImplAccess().getExitActionParserRuleCall_5_2_2_0());
            	      										
            	    }
            	    pushFollow(FOLLOW_16);
            	    lv_exit_18_0=ruleAction();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      											if (current==null) {
            	      												current = createModelElementForParent(grammarAccess.getMode_ImplRule());
            	      											}
            	      											set(
            	      												current,
            	      												"exit",
            	      												lv_exit_18_0,
            	      												"fr.inria.glose.fcl.FCL.Action");
            	      											afterParserOrEnumRuleCall();
            	      										
            	    }

            	    }


            	    }

            	    otherlv_19=(Token)match(input,14,FOLLOW_22); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_19, grammarAccess.getMode_ImplAccess().getRightCurlyBracketKeyword_5_2_3());
            	      								
            	    }

            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5());

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);


            }


            }

            getUnorderedGroupHelper().leave(grammarAccess.getMode_ImplAccess().getUnorderedGroup_5());

            }

            otherlv_20=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_20, grammarAccess.getMode_ImplAccess().getRightCurlyBracketKeyword_6());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMode_Impl"


    // $ANTLR start "entryRuleTransition"
    // InternalFCL.g:1444:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // InternalFCL.g:1444:51: (iv_ruleTransition= ruleTransition EOF )
            // InternalFCL.g:1445:2: iv_ruleTransition= ruleTransition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTransitionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTransition=ruleTransition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTransition; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalFCL.g:1451:1: ruleTransition returns [EObject current=null] : (otherlv_0= 'transition' ( ( ruleQualifiedName ) ) otherlv_2= '->' ( ( ruleQualifiedName ) ) otherlv_4= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) ) ) ) )* ) ) ) otherlv_14= '}' (otherlv_15= 'as' ( (lv_name_16_0= ruleEString ) ) )? ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        EObject lv_action_10_0 = null;

        EObject lv_guard_13_0 = null;

        AntlrDatatypeRuleToken lv_name_16_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:1457:2: ( (otherlv_0= 'transition' ( ( ruleQualifiedName ) ) otherlv_2= '->' ( ( ruleQualifiedName ) ) otherlv_4= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) ) ) ) )* ) ) ) otherlv_14= '}' (otherlv_15= 'as' ( (lv_name_16_0= ruleEString ) ) )? ) )
            // InternalFCL.g:1458:2: (otherlv_0= 'transition' ( ( ruleQualifiedName ) ) otherlv_2= '->' ( ( ruleQualifiedName ) ) otherlv_4= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) ) ) ) )* ) ) ) otherlv_14= '}' (otherlv_15= 'as' ( (lv_name_16_0= ruleEString ) ) )? )
            {
            // InternalFCL.g:1458:2: (otherlv_0= 'transition' ( ( ruleQualifiedName ) ) otherlv_2= '->' ( ( ruleQualifiedName ) ) otherlv_4= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) ) ) ) )* ) ) ) otherlv_14= '}' (otherlv_15= 'as' ( (lv_name_16_0= ruleEString ) ) )? )
            // InternalFCL.g:1459:3: otherlv_0= 'transition' ( ( ruleQualifiedName ) ) otherlv_2= '->' ( ( ruleQualifiedName ) ) otherlv_4= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) ) ) ) )* ) ) ) otherlv_14= '}' (otherlv_15= 'as' ( (lv_name_16_0= ruleEString ) ) )?
            {
            otherlv_0=(Token)match(input,27,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getTransitionAccess().getTransitionKeyword_0());
              		
            }
            // InternalFCL.g:1463:3: ( ( ruleQualifiedName ) )
            // InternalFCL.g:1464:4: ( ruleQualifiedName )
            {
            // InternalFCL.g:1464:4: ( ruleQualifiedName )
            // InternalFCL.g:1465:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getTransitionRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getTransitionAccess().getSourceModeCrossReference_1_0());
              				
            }
            pushFollow(FOLLOW_25);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,28,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getTransitionAccess().getHyphenMinusGreaterThanSignKeyword_2());
              		
            }
            // InternalFCL.g:1483:3: ( ( ruleQualifiedName ) )
            // InternalFCL.g:1484:4: ( ruleQualifiedName )
            {
            // InternalFCL.g:1484:4: ( ruleQualifiedName )
            // InternalFCL.g:1485:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getTransitionRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getTransitionAccess().getTargetModeCrossReference_3_0());
              				
            }
            pushFollow(FOLLOW_4);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_4=(Token)match(input,12,FOLLOW_26); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getTransitionAccess().getLeftCurlyBracketKeyword_4());
              		
            }
            // InternalFCL.g:1503:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) ) ) ) )* ) ) )
            // InternalFCL.g:1504:4: ( ( ( ({...}? => ( ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) ) ) ) )* ) )
            {
            // InternalFCL.g:1504:4: ( ( ( ({...}? => ( ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) ) ) ) )* ) )
            // InternalFCL.g:1505:5: ( ( ({...}? => ( ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) ) ) ) )* )
            {
            getUnorderedGroupHelper().enter(grammarAccess.getTransitionAccess().getUnorderedGroup_5());
            // InternalFCL.g:1508:5: ( ( ({...}? => ( ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) ) ) ) )* )
            // InternalFCL.g:1509:6: ( ({...}? => ( ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) ) ) ) )*
            {
            // InternalFCL.g:1509:6: ( ({...}? => ( ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) ) ) ) )*
            loop26:
            do {
                int alt26=4;
                int LA26_0 = input.LA(1);

                if ( LA26_0 == 29 && getUnorderedGroupHelper().canSelect(grammarAccess.getTransitionAccess().getUnorderedGroup_5(), 0) ) {
                    alt26=1;
                }
                else if ( LA26_0 == 30 && getUnorderedGroupHelper().canSelect(grammarAccess.getTransitionAccess().getUnorderedGroup_5(), 1) ) {
                    alt26=2;
                }
                else if ( LA26_0 == 31 && getUnorderedGroupHelper().canSelect(grammarAccess.getTransitionAccess().getUnorderedGroup_5(), 2) ) {
                    alt26=3;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalFCL.g:1510:4: ({...}? => ( ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) ) ) )
            	    {
            	    // InternalFCL.g:1510:4: ({...}? => ( ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) ) ) )
            	    // InternalFCL.g:1511:5: {...}? => ( ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getTransitionAccess().getUnorderedGroup_5(), 0) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleTransition", "getUnorderedGroupHelper().canSelect(grammarAccess.getTransitionAccess().getUnorderedGroup_5(), 0)");
            	    }
            	    // InternalFCL.g:1511:107: ( ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) ) )
            	    // InternalFCL.g:1512:6: ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getTransitionAccess().getUnorderedGroup_5(), 0);
            	    // InternalFCL.g:1515:9: ({...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) ) )
            	    // InternalFCL.g:1515:10: {...}? => (otherlv_6= 'when' ( ( ruleQualifiedName ) ) )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleTransition", "true");
            	    }
            	    // InternalFCL.g:1515:19: (otherlv_6= 'when' ( ( ruleQualifiedName ) ) )
            	    // InternalFCL.g:1515:20: otherlv_6= 'when' ( ( ruleQualifiedName ) )
            	    {
            	    otherlv_6=(Token)match(input,29,FOLLOW_3); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_6, grammarAccess.getTransitionAccess().getWhenKeyword_5_0_0());
            	      								
            	    }
            	    // InternalFCL.g:1519:9: ( ( ruleQualifiedName ) )
            	    // InternalFCL.g:1520:10: ( ruleQualifiedName )
            	    {
            	    // InternalFCL.g:1520:10: ( ruleQualifiedName )
            	    // InternalFCL.g:1521:11: ruleQualifiedName
            	    {
            	    if ( state.backtracking==0 ) {

            	      											if (current==null) {
            	      												current = createModelElement(grammarAccess.getTransitionRule());
            	      											}
            	      										
            	    }
            	    if ( state.backtracking==0 ) {

            	      											newCompositeNode(grammarAccess.getTransitionAccess().getEventEventCrossReference_5_0_1_0());
            	      										
            	    }
            	    pushFollow(FOLLOW_26);
            	    ruleQualifiedName();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      											afterParserOrEnumRuleCall();
            	      										
            	    }

            	    }


            	    }


            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getTransitionAccess().getUnorderedGroup_5());

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalFCL.g:1541:4: ({...}? => ( ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) ) ) )
            	    {
            	    // InternalFCL.g:1541:4: ({...}? => ( ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) ) ) )
            	    // InternalFCL.g:1542:5: {...}? => ( ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getTransitionAccess().getUnorderedGroup_5(), 1) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleTransition", "getUnorderedGroupHelper().canSelect(grammarAccess.getTransitionAccess().getUnorderedGroup_5(), 1)");
            	    }
            	    // InternalFCL.g:1542:107: ( ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) ) )
            	    // InternalFCL.g:1543:6: ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getTransitionAccess().getUnorderedGroup_5(), 1);
            	    // InternalFCL.g:1546:9: ({...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' ) )
            	    // InternalFCL.g:1546:10: {...}? => (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleTransition", "true");
            	    }
            	    // InternalFCL.g:1546:19: (otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}' )
            	    // InternalFCL.g:1546:20: otherlv_8= 'do' otherlv_9= '{' ( (lv_action_10_0= ruleAction ) ) otherlv_11= '}'
            	    {
            	    otherlv_8=(Token)match(input,30,FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_8, grammarAccess.getTransitionAccess().getDoKeyword_5_1_0());
            	      								
            	    }
            	    otherlv_9=(Token)match(input,12,FOLLOW_19); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_9, grammarAccess.getTransitionAccess().getLeftCurlyBracketKeyword_5_1_1());
            	      								
            	    }
            	    // InternalFCL.g:1554:9: ( (lv_action_10_0= ruleAction ) )
            	    // InternalFCL.g:1555:10: (lv_action_10_0= ruleAction )
            	    {
            	    // InternalFCL.g:1555:10: (lv_action_10_0= ruleAction )
            	    // InternalFCL.g:1556:11: lv_action_10_0= ruleAction
            	    {
            	    if ( state.backtracking==0 ) {

            	      											newCompositeNode(grammarAccess.getTransitionAccess().getActionActionParserRuleCall_5_1_2_0());
            	      										
            	    }
            	    pushFollow(FOLLOW_16);
            	    lv_action_10_0=ruleAction();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      											if (current==null) {
            	      												current = createModelElementForParent(grammarAccess.getTransitionRule());
            	      											}
            	      											set(
            	      												current,
            	      												"action",
            	      												lv_action_10_0,
            	      												"fr.inria.glose.fcl.FCL.Action");
            	      											afterParserOrEnumRuleCall();
            	      										
            	    }

            	    }


            	    }

            	    otherlv_11=(Token)match(input,14,FOLLOW_26); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_11, grammarAccess.getTransitionAccess().getRightCurlyBracketKeyword_5_1_3());
            	      								
            	    }

            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getTransitionAccess().getUnorderedGroup_5());

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalFCL.g:1583:4: ({...}? => ( ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) ) ) )
            	    {
            	    // InternalFCL.g:1583:4: ({...}? => ( ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) ) ) )
            	    // InternalFCL.g:1584:5: {...}? => ( ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getTransitionAccess().getUnorderedGroup_5(), 2) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleTransition", "getUnorderedGroupHelper().canSelect(grammarAccess.getTransitionAccess().getUnorderedGroup_5(), 2)");
            	    }
            	    // InternalFCL.g:1584:107: ( ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) ) )
            	    // InternalFCL.g:1585:6: ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getTransitionAccess().getUnorderedGroup_5(), 2);
            	    // InternalFCL.g:1588:9: ({...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) ) )
            	    // InternalFCL.g:1588:10: {...}? => (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleTransition", "true");
            	    }
            	    // InternalFCL.g:1588:19: (otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) ) )
            	    // InternalFCL.g:1588:20: otherlv_12= 'guard' ( (lv_guard_13_0= ruleExpression ) )
            	    {
            	    otherlv_12=(Token)match(input,31,FOLLOW_27); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      									newLeafNode(otherlv_12, grammarAccess.getTransitionAccess().getGuardKeyword_5_2_0());
            	      								
            	    }
            	    // InternalFCL.g:1592:9: ( (lv_guard_13_0= ruleExpression ) )
            	    // InternalFCL.g:1593:10: (lv_guard_13_0= ruleExpression )
            	    {
            	    // InternalFCL.g:1593:10: (lv_guard_13_0= ruleExpression )
            	    // InternalFCL.g:1594:11: lv_guard_13_0= ruleExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      											newCompositeNode(grammarAccess.getTransitionAccess().getGuardExpressionParserRuleCall_5_2_1_0());
            	      										
            	    }
            	    pushFollow(FOLLOW_26);
            	    lv_guard_13_0=ruleExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      											if (current==null) {
            	      												current = createModelElementForParent(grammarAccess.getTransitionRule());
            	      											}
            	      											set(
            	      												current,
            	      												"guard",
            	      												lv_guard_13_0,
            	      												"fr.inria.glose.fcl.FCL.Expression");
            	      											afterParserOrEnumRuleCall();
            	      										
            	    }

            	    }


            	    }


            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getTransitionAccess().getUnorderedGroup_5());

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);


            }


            }

            getUnorderedGroupHelper().leave(grammarAccess.getTransitionAccess().getUnorderedGroup_5());

            }

            otherlv_14=(Token)match(input,14,FOLLOW_28); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_14, grammarAccess.getTransitionAccess().getRightCurlyBracketKeyword_6());
              		
            }
            // InternalFCL.g:1628:3: (otherlv_15= 'as' ( (lv_name_16_0= ruleEString ) ) )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==32) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalFCL.g:1629:4: otherlv_15= 'as' ( (lv_name_16_0= ruleEString ) )
                    {
                    otherlv_15=(Token)match(input,32,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_15, grammarAccess.getTransitionAccess().getAsKeyword_7_0());
                      			
                    }
                    // InternalFCL.g:1633:4: ( (lv_name_16_0= ruleEString ) )
                    // InternalFCL.g:1634:5: (lv_name_16_0= ruleEString )
                    {
                    // InternalFCL.g:1634:5: (lv_name_16_0= ruleEString )
                    // InternalFCL.g:1635:6: lv_name_16_0= ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getTransitionAccess().getNameEStringParserRuleCall_7_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_name_16_0=ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getTransitionRule());
                      						}
                      						set(
                      							current,
                      							"name",
                      							lv_name_16_0,
                      							"fr.inria.glose.fcl.FCL.EString");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "entryRuleFunctionBlockDataPort"
    // InternalFCL.g:1657:1: entryRuleFunctionBlockDataPort returns [EObject current=null] : iv_ruleFunctionBlockDataPort= ruleFunctionBlockDataPort EOF ;
    public final EObject entryRuleFunctionBlockDataPort() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionBlockDataPort = null;


        try {
            // InternalFCL.g:1657:62: (iv_ruleFunctionBlockDataPort= ruleFunctionBlockDataPort EOF )
            // InternalFCL.g:1658:2: iv_ruleFunctionBlockDataPort= ruleFunctionBlockDataPort EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFunctionBlockDataPortRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFunctionBlockDataPort=ruleFunctionBlockDataPort();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFunctionBlockDataPort; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionBlockDataPort"


    // $ANTLR start "ruleFunctionBlockDataPort"
    // InternalFCL.g:1664:1: ruleFunctionBlockDataPort returns [EObject current=null] : (otherlv_0= 'dataPort' ( (lv_direction_1_0= ruleDirectionKind ) )? ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= ':' ( ( ruleQualifiedName ) ) )? (otherlv_5= 'functionVarStore' ( ( ruleQualifiedName ) ) )? (otherlv_7= 'defaultValue' ( (lv_defaultInputValue_8_0= ruleExpression ) ) )? ) ;
    public final EObject ruleFunctionBlockDataPort() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Enumerator lv_direction_1_0 = null;

        EObject lv_defaultInputValue_8_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:1670:2: ( (otherlv_0= 'dataPort' ( (lv_direction_1_0= ruleDirectionKind ) )? ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= ':' ( ( ruleQualifiedName ) ) )? (otherlv_5= 'functionVarStore' ( ( ruleQualifiedName ) ) )? (otherlv_7= 'defaultValue' ( (lv_defaultInputValue_8_0= ruleExpression ) ) )? ) )
            // InternalFCL.g:1671:2: (otherlv_0= 'dataPort' ( (lv_direction_1_0= ruleDirectionKind ) )? ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= ':' ( ( ruleQualifiedName ) ) )? (otherlv_5= 'functionVarStore' ( ( ruleQualifiedName ) ) )? (otherlv_7= 'defaultValue' ( (lv_defaultInputValue_8_0= ruleExpression ) ) )? )
            {
            // InternalFCL.g:1671:2: (otherlv_0= 'dataPort' ( (lv_direction_1_0= ruleDirectionKind ) )? ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= ':' ( ( ruleQualifiedName ) ) )? (otherlv_5= 'functionVarStore' ( ( ruleQualifiedName ) ) )? (otherlv_7= 'defaultValue' ( (lv_defaultInputValue_8_0= ruleExpression ) ) )? )
            // InternalFCL.g:1672:3: otherlv_0= 'dataPort' ( (lv_direction_1_0= ruleDirectionKind ) )? ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= ':' ( ( ruleQualifiedName ) ) )? (otherlv_5= 'functionVarStore' ( ( ruleQualifiedName ) ) )? (otherlv_7= 'defaultValue' ( (lv_defaultInputValue_8_0= ruleExpression ) ) )?
            {
            otherlv_0=(Token)match(input,33,FOLLOW_30); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getFunctionBlockDataPortAccess().getDataPortKeyword_0());
              		
            }
            // InternalFCL.g:1676:3: ( (lv_direction_1_0= ruleDirectionKind ) )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( ((LA28_0>=94 && LA28_0<=96)) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalFCL.g:1677:4: (lv_direction_1_0= ruleDirectionKind )
                    {
                    // InternalFCL.g:1677:4: (lv_direction_1_0= ruleDirectionKind )
                    // InternalFCL.g:1678:5: lv_direction_1_0= ruleDirectionKind
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getFunctionBlockDataPortAccess().getDirectionDirectionKindEnumRuleCall_1_0());
                      				
                    }
                    pushFollow(FOLLOW_3);
                    lv_direction_1_0=ruleDirectionKind();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getFunctionBlockDataPortRule());
                      					}
                      					set(
                      						current,
                      						"direction",
                      						lv_direction_1_0,
                      						"fr.inria.glose.fcl.FCL.DirectionKind");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }
                    break;

            }

            // InternalFCL.g:1695:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalFCL.g:1696:4: (lv_name_2_0= RULE_ID )
            {
            // InternalFCL.g:1696:4: (lv_name_2_0= RULE_ID )
            // InternalFCL.g:1697:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_31); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getFunctionBlockDataPortAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getFunctionBlockDataPortRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            // InternalFCL.g:1713:3: (otherlv_3= ':' ( ( ruleQualifiedName ) ) )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==34) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalFCL.g:1714:4: otherlv_3= ':' ( ( ruleQualifiedName ) )
                    {
                    otherlv_3=(Token)match(input,34,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_3, grammarAccess.getFunctionBlockDataPortAccess().getColonKeyword_3_0());
                      			
                    }
                    // InternalFCL.g:1718:4: ( ( ruleQualifiedName ) )
                    // InternalFCL.g:1719:5: ( ruleQualifiedName )
                    {
                    // InternalFCL.g:1719:5: ( ruleQualifiedName )
                    // InternalFCL.g:1720:6: ruleQualifiedName
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getFunctionBlockDataPortRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getFunctionBlockDataPortAccess().getDataTypeDataTypeCrossReference_3_1_0());
                      					
                    }
                    pushFollow(FOLLOW_32);
                    ruleQualifiedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:1735:3: (otherlv_5= 'functionVarStore' ( ( ruleQualifiedName ) ) )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==35) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalFCL.g:1736:4: otherlv_5= 'functionVarStore' ( ( ruleQualifiedName ) )
                    {
                    otherlv_5=(Token)match(input,35,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_5, grammarAccess.getFunctionBlockDataPortAccess().getFunctionVarStoreKeyword_4_0());
                      			
                    }
                    // InternalFCL.g:1740:4: ( ( ruleQualifiedName ) )
                    // InternalFCL.g:1741:5: ( ruleQualifiedName )
                    {
                    // InternalFCL.g:1741:5: ( ruleQualifiedName )
                    // InternalFCL.g:1742:6: ruleQualifiedName
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getFunctionBlockDataPortRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getFunctionBlockDataPortAccess().getFunctionVarStoreFunctionVarDeclCrossReference_4_1_0());
                      					
                    }
                    pushFollow(FOLLOW_33);
                    ruleQualifiedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:1757:3: (otherlv_7= 'defaultValue' ( (lv_defaultInputValue_8_0= ruleExpression ) ) )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==36) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalFCL.g:1758:4: otherlv_7= 'defaultValue' ( (lv_defaultInputValue_8_0= ruleExpression ) )
                    {
                    otherlv_7=(Token)match(input,36,FOLLOW_27); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_7, grammarAccess.getFunctionBlockDataPortAccess().getDefaultValueKeyword_5_0());
                      			
                    }
                    // InternalFCL.g:1762:4: ( (lv_defaultInputValue_8_0= ruleExpression ) )
                    // InternalFCL.g:1763:5: (lv_defaultInputValue_8_0= ruleExpression )
                    {
                    // InternalFCL.g:1763:5: (lv_defaultInputValue_8_0= ruleExpression )
                    // InternalFCL.g:1764:6: lv_defaultInputValue_8_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getFunctionBlockDataPortAccess().getDefaultInputValueExpressionParserRuleCall_5_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_defaultInputValue_8_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getFunctionBlockDataPortRule());
                      						}
                      						set(
                      							current,
                      							"defaultInputValue",
                      							lv_defaultInputValue_8_0,
                      							"fr.inria.glose.fcl.FCL.Expression");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionBlockDataPort"


    // $ANTLR start "entryRuleFMUFunctionBlockDataPort"
    // InternalFCL.g:1786:1: entryRuleFMUFunctionBlockDataPort returns [EObject current=null] : iv_ruleFMUFunctionBlockDataPort= ruleFMUFunctionBlockDataPort EOF ;
    public final EObject entryRuleFMUFunctionBlockDataPort() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFMUFunctionBlockDataPort = null;


        try {
            // InternalFCL.g:1786:65: (iv_ruleFMUFunctionBlockDataPort= ruleFMUFunctionBlockDataPort EOF )
            // InternalFCL.g:1787:2: iv_ruleFMUFunctionBlockDataPort= ruleFMUFunctionBlockDataPort EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFMUFunctionBlockDataPortRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFMUFunctionBlockDataPort=ruleFMUFunctionBlockDataPort();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFMUFunctionBlockDataPort; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFMUFunctionBlockDataPort"


    // $ANTLR start "ruleFMUFunctionBlockDataPort"
    // InternalFCL.g:1793:1: ruleFMUFunctionBlockDataPort returns [EObject current=null] : (otherlv_0= 'dataPort' ( (lv_direction_1_0= ruleDirectionKind ) )? ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= ':' ( ( ruleQualifiedName ) ) )? (otherlv_5= 'fmuVarName' ( (lv_runnableFmuVariableName_6_0= RULE_STRING ) ) )? (otherlv_7= 'functionVarStore' ( ( ruleQualifiedName ) ) )? (otherlv_9= 'defaultValue' ( (lv_defaultInputValue_10_0= ruleExpression ) ) )? ) ;
    public final EObject ruleFMUFunctionBlockDataPort() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token lv_runnableFmuVariableName_6_0=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Enumerator lv_direction_1_0 = null;

        EObject lv_defaultInputValue_10_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:1799:2: ( (otherlv_0= 'dataPort' ( (lv_direction_1_0= ruleDirectionKind ) )? ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= ':' ( ( ruleQualifiedName ) ) )? (otherlv_5= 'fmuVarName' ( (lv_runnableFmuVariableName_6_0= RULE_STRING ) ) )? (otherlv_7= 'functionVarStore' ( ( ruleQualifiedName ) ) )? (otherlv_9= 'defaultValue' ( (lv_defaultInputValue_10_0= ruleExpression ) ) )? ) )
            // InternalFCL.g:1800:2: (otherlv_0= 'dataPort' ( (lv_direction_1_0= ruleDirectionKind ) )? ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= ':' ( ( ruleQualifiedName ) ) )? (otherlv_5= 'fmuVarName' ( (lv_runnableFmuVariableName_6_0= RULE_STRING ) ) )? (otherlv_7= 'functionVarStore' ( ( ruleQualifiedName ) ) )? (otherlv_9= 'defaultValue' ( (lv_defaultInputValue_10_0= ruleExpression ) ) )? )
            {
            // InternalFCL.g:1800:2: (otherlv_0= 'dataPort' ( (lv_direction_1_0= ruleDirectionKind ) )? ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= ':' ( ( ruleQualifiedName ) ) )? (otherlv_5= 'fmuVarName' ( (lv_runnableFmuVariableName_6_0= RULE_STRING ) ) )? (otherlv_7= 'functionVarStore' ( ( ruleQualifiedName ) ) )? (otherlv_9= 'defaultValue' ( (lv_defaultInputValue_10_0= ruleExpression ) ) )? )
            // InternalFCL.g:1801:3: otherlv_0= 'dataPort' ( (lv_direction_1_0= ruleDirectionKind ) )? ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= ':' ( ( ruleQualifiedName ) ) )? (otherlv_5= 'fmuVarName' ( (lv_runnableFmuVariableName_6_0= RULE_STRING ) ) )? (otherlv_7= 'functionVarStore' ( ( ruleQualifiedName ) ) )? (otherlv_9= 'defaultValue' ( (lv_defaultInputValue_10_0= ruleExpression ) ) )?
            {
            otherlv_0=(Token)match(input,33,FOLLOW_30); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getFMUFunctionBlockDataPortAccess().getDataPortKeyword_0());
              		
            }
            // InternalFCL.g:1805:3: ( (lv_direction_1_0= ruleDirectionKind ) )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( ((LA32_0>=94 && LA32_0<=96)) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalFCL.g:1806:4: (lv_direction_1_0= ruleDirectionKind )
                    {
                    // InternalFCL.g:1806:4: (lv_direction_1_0= ruleDirectionKind )
                    // InternalFCL.g:1807:5: lv_direction_1_0= ruleDirectionKind
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getFMUFunctionBlockDataPortAccess().getDirectionDirectionKindEnumRuleCall_1_0());
                      				
                    }
                    pushFollow(FOLLOW_3);
                    lv_direction_1_0=ruleDirectionKind();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getFMUFunctionBlockDataPortRule());
                      					}
                      					set(
                      						current,
                      						"direction",
                      						lv_direction_1_0,
                      						"fr.inria.glose.fcl.FCL.DirectionKind");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }
                    break;

            }

            // InternalFCL.g:1824:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalFCL.g:1825:4: (lv_name_2_0= RULE_ID )
            {
            // InternalFCL.g:1825:4: (lv_name_2_0= RULE_ID )
            // InternalFCL.g:1826:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_34); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getFMUFunctionBlockDataPortAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getFMUFunctionBlockDataPortRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            // InternalFCL.g:1842:3: (otherlv_3= ':' ( ( ruleQualifiedName ) ) )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==34) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalFCL.g:1843:4: otherlv_3= ':' ( ( ruleQualifiedName ) )
                    {
                    otherlv_3=(Token)match(input,34,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_3, grammarAccess.getFMUFunctionBlockDataPortAccess().getColonKeyword_3_0());
                      			
                    }
                    // InternalFCL.g:1847:4: ( ( ruleQualifiedName ) )
                    // InternalFCL.g:1848:5: ( ruleQualifiedName )
                    {
                    // InternalFCL.g:1848:5: ( ruleQualifiedName )
                    // InternalFCL.g:1849:6: ruleQualifiedName
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getFMUFunctionBlockDataPortRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getFMUFunctionBlockDataPortAccess().getDataTypeDataTypeCrossReference_3_1_0());
                      					
                    }
                    pushFollow(FOLLOW_35);
                    ruleQualifiedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:1864:3: (otherlv_5= 'fmuVarName' ( (lv_runnableFmuVariableName_6_0= RULE_STRING ) ) )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==37) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalFCL.g:1865:4: otherlv_5= 'fmuVarName' ( (lv_runnableFmuVariableName_6_0= RULE_STRING ) )
                    {
                    otherlv_5=(Token)match(input,37,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_5, grammarAccess.getFMUFunctionBlockDataPortAccess().getFmuVarNameKeyword_4_0());
                      			
                    }
                    // InternalFCL.g:1869:4: ( (lv_runnableFmuVariableName_6_0= RULE_STRING ) )
                    // InternalFCL.g:1870:5: (lv_runnableFmuVariableName_6_0= RULE_STRING )
                    {
                    // InternalFCL.g:1870:5: (lv_runnableFmuVariableName_6_0= RULE_STRING )
                    // InternalFCL.g:1871:6: lv_runnableFmuVariableName_6_0= RULE_STRING
                    {
                    lv_runnableFmuVariableName_6_0=(Token)match(input,RULE_STRING,FOLLOW_32); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_runnableFmuVariableName_6_0, grammarAccess.getFMUFunctionBlockDataPortAccess().getRunnableFmuVariableNameSTRINGTerminalRuleCall_4_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getFMUFunctionBlockDataPortRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"runnableFmuVariableName",
                      							lv_runnableFmuVariableName_6_0,
                      							"org.eclipse.xtext.common.Terminals.STRING");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:1888:3: (otherlv_7= 'functionVarStore' ( ( ruleQualifiedName ) ) )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==35) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalFCL.g:1889:4: otherlv_7= 'functionVarStore' ( ( ruleQualifiedName ) )
                    {
                    otherlv_7=(Token)match(input,35,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_7, grammarAccess.getFMUFunctionBlockDataPortAccess().getFunctionVarStoreKeyword_5_0());
                      			
                    }
                    // InternalFCL.g:1893:4: ( ( ruleQualifiedName ) )
                    // InternalFCL.g:1894:5: ( ruleQualifiedName )
                    {
                    // InternalFCL.g:1894:5: ( ruleQualifiedName )
                    // InternalFCL.g:1895:6: ruleQualifiedName
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getFMUFunctionBlockDataPortRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getFMUFunctionBlockDataPortAccess().getFunctionVarStoreFunctionVarDeclCrossReference_5_1_0());
                      					
                    }
                    pushFollow(FOLLOW_33);
                    ruleQualifiedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:1910:3: (otherlv_9= 'defaultValue' ( (lv_defaultInputValue_10_0= ruleExpression ) ) )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==36) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalFCL.g:1911:4: otherlv_9= 'defaultValue' ( (lv_defaultInputValue_10_0= ruleExpression ) )
                    {
                    otherlv_9=(Token)match(input,36,FOLLOW_27); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_9, grammarAccess.getFMUFunctionBlockDataPortAccess().getDefaultValueKeyword_6_0());
                      			
                    }
                    // InternalFCL.g:1915:4: ( (lv_defaultInputValue_10_0= ruleExpression ) )
                    // InternalFCL.g:1916:5: (lv_defaultInputValue_10_0= ruleExpression )
                    {
                    // InternalFCL.g:1916:5: (lv_defaultInputValue_10_0= ruleExpression )
                    // InternalFCL.g:1917:6: lv_defaultInputValue_10_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getFMUFunctionBlockDataPortAccess().getDefaultInputValueExpressionParserRuleCall_6_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_defaultInputValue_10_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getFMUFunctionBlockDataPortRule());
                      						}
                      						set(
                      							current,
                      							"defaultInputValue",
                      							lv_defaultInputValue_10_0,
                      							"fr.inria.glose.fcl.FCL.Expression");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFMUFunctionBlockDataPort"


    // $ANTLR start "entryRuleUnityFunctionDataPort"
    // InternalFCL.g:1939:1: entryRuleUnityFunctionDataPort returns [EObject current=null] : iv_ruleUnityFunctionDataPort= ruleUnityFunctionDataPort EOF ;
    public final EObject entryRuleUnityFunctionDataPort() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnityFunctionDataPort = null;


        try {
            // InternalFCL.g:1939:62: (iv_ruleUnityFunctionDataPort= ruleUnityFunctionDataPort EOF )
            // InternalFCL.g:1940:2: iv_ruleUnityFunctionDataPort= ruleUnityFunctionDataPort EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnityFunctionDataPortRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleUnityFunctionDataPort=ruleUnityFunctionDataPort();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnityFunctionDataPort; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnityFunctionDataPort"


    // $ANTLR start "ruleUnityFunctionDataPort"
    // InternalFCL.g:1946:1: ruleUnityFunctionDataPort returns [EObject current=null] : (otherlv_0= 'dataPort' ( (lv_direction_1_0= ruleDirectionKind ) )? ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= ':' ( ( ruleQualifiedName ) ) )? (otherlv_5= 'nodeName' ( (lv_unityNodeName_6_0= RULE_STRING ) ) )? (otherlv_7= 'varName' ( (lv_unityVariableName_8_0= RULE_STRING ) ) )? (otherlv_9= 'functionVarStore' ( ( ruleQualifiedName ) ) )? (otherlv_11= 'defaultValue' ( (lv_defaultInputValue_12_0= ruleExpression ) ) )? ) ;
    public final EObject ruleUnityFunctionDataPort() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token lv_unityNodeName_6_0=null;
        Token otherlv_7=null;
        Token lv_unityVariableName_8_0=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Enumerator lv_direction_1_0 = null;

        EObject lv_defaultInputValue_12_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:1952:2: ( (otherlv_0= 'dataPort' ( (lv_direction_1_0= ruleDirectionKind ) )? ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= ':' ( ( ruleQualifiedName ) ) )? (otherlv_5= 'nodeName' ( (lv_unityNodeName_6_0= RULE_STRING ) ) )? (otherlv_7= 'varName' ( (lv_unityVariableName_8_0= RULE_STRING ) ) )? (otherlv_9= 'functionVarStore' ( ( ruleQualifiedName ) ) )? (otherlv_11= 'defaultValue' ( (lv_defaultInputValue_12_0= ruleExpression ) ) )? ) )
            // InternalFCL.g:1953:2: (otherlv_0= 'dataPort' ( (lv_direction_1_0= ruleDirectionKind ) )? ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= ':' ( ( ruleQualifiedName ) ) )? (otherlv_5= 'nodeName' ( (lv_unityNodeName_6_0= RULE_STRING ) ) )? (otherlv_7= 'varName' ( (lv_unityVariableName_8_0= RULE_STRING ) ) )? (otherlv_9= 'functionVarStore' ( ( ruleQualifiedName ) ) )? (otherlv_11= 'defaultValue' ( (lv_defaultInputValue_12_0= ruleExpression ) ) )? )
            {
            // InternalFCL.g:1953:2: (otherlv_0= 'dataPort' ( (lv_direction_1_0= ruleDirectionKind ) )? ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= ':' ( ( ruleQualifiedName ) ) )? (otherlv_5= 'nodeName' ( (lv_unityNodeName_6_0= RULE_STRING ) ) )? (otherlv_7= 'varName' ( (lv_unityVariableName_8_0= RULE_STRING ) ) )? (otherlv_9= 'functionVarStore' ( ( ruleQualifiedName ) ) )? (otherlv_11= 'defaultValue' ( (lv_defaultInputValue_12_0= ruleExpression ) ) )? )
            // InternalFCL.g:1954:3: otherlv_0= 'dataPort' ( (lv_direction_1_0= ruleDirectionKind ) )? ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= ':' ( ( ruleQualifiedName ) ) )? (otherlv_5= 'nodeName' ( (lv_unityNodeName_6_0= RULE_STRING ) ) )? (otherlv_7= 'varName' ( (lv_unityVariableName_8_0= RULE_STRING ) ) )? (otherlv_9= 'functionVarStore' ( ( ruleQualifiedName ) ) )? (otherlv_11= 'defaultValue' ( (lv_defaultInputValue_12_0= ruleExpression ) ) )?
            {
            otherlv_0=(Token)match(input,33,FOLLOW_30); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getUnityFunctionDataPortAccess().getDataPortKeyword_0());
              		
            }
            // InternalFCL.g:1958:3: ( (lv_direction_1_0= ruleDirectionKind ) )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( ((LA37_0>=94 && LA37_0<=96)) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalFCL.g:1959:4: (lv_direction_1_0= ruleDirectionKind )
                    {
                    // InternalFCL.g:1959:4: (lv_direction_1_0= ruleDirectionKind )
                    // InternalFCL.g:1960:5: lv_direction_1_0= ruleDirectionKind
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getUnityFunctionDataPortAccess().getDirectionDirectionKindEnumRuleCall_1_0());
                      				
                    }
                    pushFollow(FOLLOW_3);
                    lv_direction_1_0=ruleDirectionKind();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getUnityFunctionDataPortRule());
                      					}
                      					set(
                      						current,
                      						"direction",
                      						lv_direction_1_0,
                      						"fr.inria.glose.fcl.FCL.DirectionKind");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }
                    break;

            }

            // InternalFCL.g:1977:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalFCL.g:1978:4: (lv_name_2_0= RULE_ID )
            {
            // InternalFCL.g:1978:4: (lv_name_2_0= RULE_ID )
            // InternalFCL.g:1979:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_36); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getUnityFunctionDataPortAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getUnityFunctionDataPortRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            // InternalFCL.g:1995:3: (otherlv_3= ':' ( ( ruleQualifiedName ) ) )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==34) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalFCL.g:1996:4: otherlv_3= ':' ( ( ruleQualifiedName ) )
                    {
                    otherlv_3=(Token)match(input,34,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_3, grammarAccess.getUnityFunctionDataPortAccess().getColonKeyword_3_0());
                      			
                    }
                    // InternalFCL.g:2000:4: ( ( ruleQualifiedName ) )
                    // InternalFCL.g:2001:5: ( ruleQualifiedName )
                    {
                    // InternalFCL.g:2001:5: ( ruleQualifiedName )
                    // InternalFCL.g:2002:6: ruleQualifiedName
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getUnityFunctionDataPortRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getUnityFunctionDataPortAccess().getDataTypeDataTypeCrossReference_3_1_0());
                      					
                    }
                    pushFollow(FOLLOW_37);
                    ruleQualifiedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:2017:3: (otherlv_5= 'nodeName' ( (lv_unityNodeName_6_0= RULE_STRING ) ) )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==38) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalFCL.g:2018:4: otherlv_5= 'nodeName' ( (lv_unityNodeName_6_0= RULE_STRING ) )
                    {
                    otherlv_5=(Token)match(input,38,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_5, grammarAccess.getUnityFunctionDataPortAccess().getNodeNameKeyword_4_0());
                      			
                    }
                    // InternalFCL.g:2022:4: ( (lv_unityNodeName_6_0= RULE_STRING ) )
                    // InternalFCL.g:2023:5: (lv_unityNodeName_6_0= RULE_STRING )
                    {
                    // InternalFCL.g:2023:5: (lv_unityNodeName_6_0= RULE_STRING )
                    // InternalFCL.g:2024:6: lv_unityNodeName_6_0= RULE_STRING
                    {
                    lv_unityNodeName_6_0=(Token)match(input,RULE_STRING,FOLLOW_38); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_unityNodeName_6_0, grammarAccess.getUnityFunctionDataPortAccess().getUnityNodeNameSTRINGTerminalRuleCall_4_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getUnityFunctionDataPortRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"unityNodeName",
                      							lv_unityNodeName_6_0,
                      							"org.eclipse.xtext.common.Terminals.STRING");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:2041:3: (otherlv_7= 'varName' ( (lv_unityVariableName_8_0= RULE_STRING ) ) )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==39) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalFCL.g:2042:4: otherlv_7= 'varName' ( (lv_unityVariableName_8_0= RULE_STRING ) )
                    {
                    otherlv_7=(Token)match(input,39,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_7, grammarAccess.getUnityFunctionDataPortAccess().getVarNameKeyword_5_0());
                      			
                    }
                    // InternalFCL.g:2046:4: ( (lv_unityVariableName_8_0= RULE_STRING ) )
                    // InternalFCL.g:2047:5: (lv_unityVariableName_8_0= RULE_STRING )
                    {
                    // InternalFCL.g:2047:5: (lv_unityVariableName_8_0= RULE_STRING )
                    // InternalFCL.g:2048:6: lv_unityVariableName_8_0= RULE_STRING
                    {
                    lv_unityVariableName_8_0=(Token)match(input,RULE_STRING,FOLLOW_32); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_unityVariableName_8_0, grammarAccess.getUnityFunctionDataPortAccess().getUnityVariableNameSTRINGTerminalRuleCall_5_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getUnityFunctionDataPortRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"unityVariableName",
                      							lv_unityVariableName_8_0,
                      							"org.eclipse.xtext.common.Terminals.STRING");
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:2065:3: (otherlv_9= 'functionVarStore' ( ( ruleQualifiedName ) ) )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==35) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalFCL.g:2066:4: otherlv_9= 'functionVarStore' ( ( ruleQualifiedName ) )
                    {
                    otherlv_9=(Token)match(input,35,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_9, grammarAccess.getUnityFunctionDataPortAccess().getFunctionVarStoreKeyword_6_0());
                      			
                    }
                    // InternalFCL.g:2070:4: ( ( ruleQualifiedName ) )
                    // InternalFCL.g:2071:5: ( ruleQualifiedName )
                    {
                    // InternalFCL.g:2071:5: ( ruleQualifiedName )
                    // InternalFCL.g:2072:6: ruleQualifiedName
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getUnityFunctionDataPortRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getUnityFunctionDataPortAccess().getFunctionVarStoreFunctionVarDeclCrossReference_6_1_0());
                      					
                    }
                    pushFollow(FOLLOW_33);
                    ruleQualifiedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:2087:3: (otherlv_11= 'defaultValue' ( (lv_defaultInputValue_12_0= ruleExpression ) ) )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==36) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // InternalFCL.g:2088:4: otherlv_11= 'defaultValue' ( (lv_defaultInputValue_12_0= ruleExpression ) )
                    {
                    otherlv_11=(Token)match(input,36,FOLLOW_27); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_11, grammarAccess.getUnityFunctionDataPortAccess().getDefaultValueKeyword_7_0());
                      			
                    }
                    // InternalFCL.g:2092:4: ( (lv_defaultInputValue_12_0= ruleExpression ) )
                    // InternalFCL.g:2093:5: (lv_defaultInputValue_12_0= ruleExpression )
                    {
                    // InternalFCL.g:2093:5: (lv_defaultInputValue_12_0= ruleExpression )
                    // InternalFCL.g:2094:6: lv_defaultInputValue_12_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getUnityFunctionDataPortAccess().getDefaultInputValueExpressionParserRuleCall_7_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_defaultInputValue_12_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getUnityFunctionDataPortRule());
                      						}
                      						set(
                      							current,
                      							"defaultInputValue",
                      							lv_defaultInputValue_12_0,
                      							"fr.inria.glose.fcl.FCL.Expression");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnityFunctionDataPort"


    // $ANTLR start "entryRuleFunctionPortReference"
    // InternalFCL.g:2116:1: entryRuleFunctionPortReference returns [EObject current=null] : iv_ruleFunctionPortReference= ruleFunctionPortReference EOF ;
    public final EObject entryRuleFunctionPortReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionPortReference = null;


        try {
            // InternalFCL.g:2116:62: (iv_ruleFunctionPortReference= ruleFunctionPortReference EOF )
            // InternalFCL.g:2117:2: iv_ruleFunctionPortReference= ruleFunctionPortReference EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFunctionPortReferenceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFunctionPortReference=ruleFunctionPortReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFunctionPortReference; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionPortReference"


    // $ANTLR start "ruleFunctionPortReference"
    // InternalFCL.g:2123:1: ruleFunctionPortReference returns [EObject current=null] : (otherlv_0= 'portRef' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '<->' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleFunctionPortReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalFCL.g:2129:2: ( (otherlv_0= 'portRef' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '<->' ( ( ruleQualifiedName ) ) ) )
            // InternalFCL.g:2130:2: (otherlv_0= 'portRef' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '<->' ( ( ruleQualifiedName ) ) )
            {
            // InternalFCL.g:2130:2: (otherlv_0= 'portRef' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '<->' ( ( ruleQualifiedName ) ) )
            // InternalFCL.g:2131:3: otherlv_0= 'portRef' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '<->' ( ( ruleQualifiedName ) )
            {
            otherlv_0=(Token)match(input,40,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getFunctionPortReferenceAccess().getPortRefKeyword_0());
              		
            }
            // InternalFCL.g:2135:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalFCL.g:2136:4: (lv_name_1_0= RULE_ID )
            {
            // InternalFCL.g:2136:4: (lv_name_1_0= RULE_ID )
            // InternalFCL.g:2137:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_39); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getFunctionPortReferenceAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getFunctionPortReferenceRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,41,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getFunctionPortReferenceAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_2());
              		
            }
            // InternalFCL.g:2157:3: ( ( ruleQualifiedName ) )
            // InternalFCL.g:2158:4: ( ruleQualifiedName )
            {
            // InternalFCL.g:2158:4: ( ruleQualifiedName )
            // InternalFCL.g:2159:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getFunctionPortReferenceRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getFunctionPortReferenceAccess().getFunctionPortFunctionPortCrossReference_3_0());
              				
            }
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionPortReference"


    // $ANTLR start "entryRuleMainFunction"
    // InternalFCL.g:2177:1: entryRuleMainFunction returns [EObject current=null] : iv_ruleMainFunction= ruleMainFunction EOF ;
    public final EObject entryRuleMainFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMainFunction = null;


        try {
            // InternalFCL.g:2177:53: (iv_ruleMainFunction= ruleMainFunction EOF )
            // InternalFCL.g:2178:2: iv_ruleMainFunction= ruleMainFunction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMainFunctionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMainFunction=ruleMainFunction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMainFunction; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMainFunction"


    // $ANTLR start "ruleMainFunction"
    // InternalFCL.g:2184:1: ruleMainFunction returns [EObject current=null] : ( () otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}' )? (otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}' )? (otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) ) )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? (otherlv_20= 'dataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}' )? ) otherlv_25= '}' ) ;
    public final EObject ruleMainFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        EObject lv_functionPorts_6_0 = null;

        EObject lv_functionPorts_7_0 = null;

        EObject lv_functionVarDecls_11_0 = null;

        EObject lv_functionVarDecls_12_0 = null;

        EObject lv_action_15_0 = null;

        EObject lv_timeReference_18_0 = null;

        EObject lv_subFunctions_22_0 = null;

        EObject lv_functionConnectors_23_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:2190:2: ( ( () otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}' )? (otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}' )? (otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) ) )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? (otherlv_20= 'dataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}' )? ) otherlv_25= '}' ) )
            // InternalFCL.g:2191:2: ( () otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}' )? (otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}' )? (otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) ) )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? (otherlv_20= 'dataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}' )? ) otherlv_25= '}' )
            {
            // InternalFCL.g:2191:2: ( () otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}' )? (otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}' )? (otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) ) )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? (otherlv_20= 'dataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}' )? ) otherlv_25= '}' )
            // InternalFCL.g:2192:3: () otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}' )? (otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}' )? (otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) ) )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? (otherlv_20= 'dataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}' )? ) otherlv_25= '}'
            {
            // InternalFCL.g:2192:3: ()
            // InternalFCL.g:2193:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getMainFunctionAccess().getFunctionAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,42,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getMainFunctionAccess().getFunctionKeyword_1());
              		
            }
            // InternalFCL.g:2203:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalFCL.g:2204:4: (lv_name_2_0= RULE_ID )
            {
            // InternalFCL.g:2204:4: (lv_name_2_0= RULE_ID )
            // InternalFCL.g:2205:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getMainFunctionAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getMainFunctionRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_40); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getMainFunctionAccess().getLeftCurlyBracketKeyword_3());
              		
            }
            // InternalFCL.g:2225:3: ( (otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}' )? (otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}' )? (otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) ) )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? (otherlv_20= 'dataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}' )? )
            // InternalFCL.g:2226:4: (otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}' )? (otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}' )? (otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) ) )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? (otherlv_20= 'dataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}' )?
            {
            // InternalFCL.g:2226:4: (otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}' )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==43) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // InternalFCL.g:2227:5: otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}'
                    {
                    otherlv_4=(Token)match(input,43,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_4, grammarAccess.getMainFunctionAccess().getPortsKeyword_4_0_0());
                      				
                    }
                    otherlv_5=(Token)match(input,12,FOLLOW_41); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_5, grammarAccess.getMainFunctionAccess().getLeftCurlyBracketKeyword_4_0_1());
                      				
                    }
                    // InternalFCL.g:2235:5: ( (lv_functionPorts_6_0= ruleFunctionPort ) )
                    // InternalFCL.g:2236:6: (lv_functionPorts_6_0= ruleFunctionPort )
                    {
                    // InternalFCL.g:2236:6: (lv_functionPorts_6_0= ruleFunctionPort )
                    // InternalFCL.g:2237:7: lv_functionPorts_6_0= ruleFunctionPort
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getMainFunctionAccess().getFunctionPortsFunctionPortParserRuleCall_4_0_2_0());
                      						
                    }
                    pushFollow(FOLLOW_42);
                    lv_functionPorts_6_0=ruleFunctionPort();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getMainFunctionRule());
                      							}
                      							add(
                      								current,
                      								"functionPorts",
                      								lv_functionPorts_6_0,
                      								"fr.inria.glose.fcl.FCL.FunctionPort");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }

                    // InternalFCL.g:2254:5: ( (lv_functionPorts_7_0= ruleFunctionPort ) )*
                    loop43:
                    do {
                        int alt43=2;
                        int LA43_0 = input.LA(1);

                        if ( (LA43_0==33||LA43_0==40) ) {
                            alt43=1;
                        }


                        switch (alt43) {
                    	case 1 :
                    	    // InternalFCL.g:2255:6: (lv_functionPorts_7_0= ruleFunctionPort )
                    	    {
                    	    // InternalFCL.g:2255:6: (lv_functionPorts_7_0= ruleFunctionPort )
                    	    // InternalFCL.g:2256:7: lv_functionPorts_7_0= ruleFunctionPort
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getMainFunctionAccess().getFunctionPortsFunctionPortParserRuleCall_4_0_3_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_42);
                    	    lv_functionPorts_7_0=ruleFunctionPort();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getMainFunctionRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"functionPorts",
                    	      								lv_functionPorts_7_0,
                    	      								"fr.inria.glose.fcl.FCL.FunctionPort");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop43;
                        }
                    } while (true);

                    otherlv_8=(Token)match(input,14,FOLLOW_43); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_8, grammarAccess.getMainFunctionAccess().getRightCurlyBracketKeyword_4_0_4());
                      				
                    }

                    }
                    break;

            }

            // InternalFCL.g:2278:4: (otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}' )?
            int alt46=2;
            int LA46_0 = input.LA(1);

            if ( (LA46_0==44) ) {
                alt46=1;
            }
            switch (alt46) {
                case 1 :
                    // InternalFCL.g:2279:5: otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}'
                    {
                    otherlv_9=(Token)match(input,44,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_9, grammarAccess.getMainFunctionAccess().getVariablesKeyword_4_1_0());
                      				
                    }
                    otherlv_10=(Token)match(input,12,FOLLOW_44); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_10, grammarAccess.getMainFunctionAccess().getLeftCurlyBracketKeyword_4_1_1());
                      				
                    }
                    // InternalFCL.g:2287:5: ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) )
                    // InternalFCL.g:2288:6: (lv_functionVarDecls_11_0= ruleFunctionVarDecl )
                    {
                    // InternalFCL.g:2288:6: (lv_functionVarDecls_11_0= ruleFunctionVarDecl )
                    // InternalFCL.g:2289:7: lv_functionVarDecls_11_0= ruleFunctionVarDecl
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getMainFunctionAccess().getFunctionVarDeclsFunctionVarDeclParserRuleCall_4_1_2_0());
                      						
                    }
                    pushFollow(FOLLOW_45);
                    lv_functionVarDecls_11_0=ruleFunctionVarDecl();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getMainFunctionRule());
                      							}
                      							add(
                      								current,
                      								"functionVarDecls",
                      								lv_functionVarDecls_11_0,
                      								"fr.inria.glose.fcl.FCL.FunctionVarDecl");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }

                    // InternalFCL.g:2306:5: ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )*
                    loop45:
                    do {
                        int alt45=2;
                        int LA45_0 = input.LA(1);

                        if ( (LA45_0==58) ) {
                            alt45=1;
                        }


                        switch (alt45) {
                    	case 1 :
                    	    // InternalFCL.g:2307:6: (lv_functionVarDecls_12_0= ruleFunctionVarDecl )
                    	    {
                    	    // InternalFCL.g:2307:6: (lv_functionVarDecls_12_0= ruleFunctionVarDecl )
                    	    // InternalFCL.g:2308:7: lv_functionVarDecls_12_0= ruleFunctionVarDecl
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getMainFunctionAccess().getFunctionVarDeclsFunctionVarDeclParserRuleCall_4_1_3_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_45);
                    	    lv_functionVarDecls_12_0=ruleFunctionVarDecl();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getMainFunctionRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"functionVarDecls",
                    	      								lv_functionVarDecls_12_0,
                    	      								"fr.inria.glose.fcl.FCL.FunctionVarDecl");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop45;
                        }
                    } while (true);

                    otherlv_13=(Token)match(input,14,FOLLOW_46); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_13, grammarAccess.getMainFunctionAccess().getRightCurlyBracketKeyword_4_1_4());
                      				
                    }

                    }
                    break;

            }

            // InternalFCL.g:2330:4: (otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) ) )?
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==45) ) {
                alt47=1;
            }
            switch (alt47) {
                case 1 :
                    // InternalFCL.g:2331:5: otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) )
                    {
                    otherlv_14=(Token)match(input,45,FOLLOW_19); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_14, grammarAccess.getMainFunctionAccess().getActionKeyword_4_2_0());
                      				
                    }
                    // InternalFCL.g:2335:5: ( (lv_action_15_0= ruleAction ) )
                    // InternalFCL.g:2336:6: (lv_action_15_0= ruleAction )
                    {
                    // InternalFCL.g:2336:6: (lv_action_15_0= ruleAction )
                    // InternalFCL.g:2337:7: lv_action_15_0= ruleAction
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getMainFunctionAccess().getActionActionParserRuleCall_4_2_1_0());
                      						
                    }
                    pushFollow(FOLLOW_47);
                    lv_action_15_0=ruleAction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getMainFunctionRule());
                      							}
                      							set(
                      								current,
                      								"action",
                      								lv_action_15_0,
                      								"fr.inria.glose.fcl.FCL.Action");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:2355:4: (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )?
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==46) ) {
                alt48=1;
            }
            switch (alt48) {
                case 1 :
                    // InternalFCL.g:2356:5: otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}'
                    {
                    otherlv_16=(Token)match(input,46,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_16, grammarAccess.getMainFunctionAccess().getTimeReferenceKeyword_4_3_0());
                      				
                    }
                    otherlv_17=(Token)match(input,12,FOLLOW_48); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_17, grammarAccess.getMainFunctionAccess().getLeftCurlyBracketKeyword_4_3_1());
                      				
                    }
                    // InternalFCL.g:2364:5: ( (lv_timeReference_18_0= ruleTimeReference ) )
                    // InternalFCL.g:2365:6: (lv_timeReference_18_0= ruleTimeReference )
                    {
                    // InternalFCL.g:2365:6: (lv_timeReference_18_0= ruleTimeReference )
                    // InternalFCL.g:2366:7: lv_timeReference_18_0= ruleTimeReference
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getMainFunctionAccess().getTimeReferenceTimeReferenceParserRuleCall_4_3_2_0());
                      						
                    }
                    pushFollow(FOLLOW_16);
                    lv_timeReference_18_0=ruleTimeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getMainFunctionRule());
                      							}
                      							set(
                      								current,
                      								"timeReference",
                      								lv_timeReference_18_0,
                      								"fr.inria.glose.fcl.FCL.TimeReference");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }

                    otherlv_19=(Token)match(input,14,FOLLOW_49); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_19, grammarAccess.getMainFunctionAccess().getRightCurlyBracketKeyword_4_3_3());
                      				
                    }

                    }
                    break;

            }

            // InternalFCL.g:2388:4: (otherlv_20= 'dataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}' )?
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0==47) ) {
                alt50=1;
            }
            switch (alt50) {
                case 1 :
                    // InternalFCL.g:2389:5: otherlv_20= 'dataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}'
                    {
                    otherlv_20=(Token)match(input,47,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_20, grammarAccess.getMainFunctionAccess().getDataFlowKeyword_4_4_0());
                      				
                    }
                    otherlv_21=(Token)match(input,12,FOLLOW_50); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_21, grammarAccess.getMainFunctionAccess().getLeftCurlyBracketKeyword_4_4_1());
                      				
                    }
                    // InternalFCL.g:2397:5: ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )*
                    loop49:
                    do {
                        int alt49=3;
                        int LA49_0 = input.LA(1);

                        if ( (LA49_0==42||LA49_0==49||LA49_0==51) ) {
                            alt49=1;
                        }
                        else if ( (LA49_0==55) ) {
                            alt49=2;
                        }


                        switch (alt49) {
                    	case 1 :
                    	    // InternalFCL.g:2398:6: ( (lv_subFunctions_22_0= ruleFunction ) )
                    	    {
                    	    // InternalFCL.g:2398:6: ( (lv_subFunctions_22_0= ruleFunction ) )
                    	    // InternalFCL.g:2399:7: (lv_subFunctions_22_0= ruleFunction )
                    	    {
                    	    // InternalFCL.g:2399:7: (lv_subFunctions_22_0= ruleFunction )
                    	    // InternalFCL.g:2400:8: lv_subFunctions_22_0= ruleFunction
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      								newCompositeNode(grammarAccess.getMainFunctionAccess().getSubFunctionsFunctionParserRuleCall_4_4_2_0_0());
                    	      							
                    	    }
                    	    pushFollow(FOLLOW_50);
                    	    lv_subFunctions_22_0=ruleFunction();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      								if (current==null) {
                    	      									current = createModelElementForParent(grammarAccess.getMainFunctionRule());
                    	      								}
                    	      								add(
                    	      									current,
                    	      									"subFunctions",
                    	      									lv_subFunctions_22_0,
                    	      									"fr.inria.glose.fcl.FCL.Function");
                    	      								afterParserOrEnumRuleCall();
                    	      							
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalFCL.g:2418:6: ( (lv_functionConnectors_23_0= ruleFunctionConnector ) )
                    	    {
                    	    // InternalFCL.g:2418:6: ( (lv_functionConnectors_23_0= ruleFunctionConnector ) )
                    	    // InternalFCL.g:2419:7: (lv_functionConnectors_23_0= ruleFunctionConnector )
                    	    {
                    	    // InternalFCL.g:2419:7: (lv_functionConnectors_23_0= ruleFunctionConnector )
                    	    // InternalFCL.g:2420:8: lv_functionConnectors_23_0= ruleFunctionConnector
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      								newCompositeNode(grammarAccess.getMainFunctionAccess().getFunctionConnectorsFunctionConnectorParserRuleCall_4_4_2_1_0());
                    	      							
                    	    }
                    	    pushFollow(FOLLOW_50);
                    	    lv_functionConnectors_23_0=ruleFunctionConnector();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      								if (current==null) {
                    	      									current = createModelElementForParent(grammarAccess.getMainFunctionRule());
                    	      								}
                    	      								add(
                    	      									current,
                    	      									"functionConnectors",
                    	      									lv_functionConnectors_23_0,
                    	      									"fr.inria.glose.fcl.FCL.FunctionConnector");
                    	      								afterParserOrEnumRuleCall();
                    	      							
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop49;
                        }
                    } while (true);

                    otherlv_24=(Token)match(input,14,FOLLOW_16); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_24, grammarAccess.getMainFunctionAccess().getRightCurlyBracketKeyword_4_4_3());
                      				
                    }

                    }
                    break;

            }


            }

            otherlv_25=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_25, grammarAccess.getMainFunctionAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMainFunction"


    // $ANTLR start "entryRuleFunction"
    // InternalFCL.g:2452:1: entryRuleFunction returns [EObject current=null] : iv_ruleFunction= ruleFunction EOF ;
    public final EObject entryRuleFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunction = null;


        try {
            // InternalFCL.g:2452:49: (iv_ruleFunction= ruleFunction EOF )
            // InternalFCL.g:2453:2: iv_ruleFunction= ruleFunction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFunctionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFunction=ruleFunction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFunction; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // InternalFCL.g:2459:1: ruleFunction returns [EObject current=null] : (this_NamedFunction_0= ruleNamedFunction | this_FMUFunction_1= ruleFMUFunction | this_UnityFunction_2= ruleUnityFunction ) ;
    public final EObject ruleFunction() throws RecognitionException {
        EObject current = null;

        EObject this_NamedFunction_0 = null;

        EObject this_FMUFunction_1 = null;

        EObject this_UnityFunction_2 = null;



        	enterRule();

        try {
            // InternalFCL.g:2465:2: ( (this_NamedFunction_0= ruleNamedFunction | this_FMUFunction_1= ruleFMUFunction | this_UnityFunction_2= ruleUnityFunction ) )
            // InternalFCL.g:2466:2: (this_NamedFunction_0= ruleNamedFunction | this_FMUFunction_1= ruleFMUFunction | this_UnityFunction_2= ruleUnityFunction )
            {
            // InternalFCL.g:2466:2: (this_NamedFunction_0= ruleNamedFunction | this_FMUFunction_1= ruleFMUFunction | this_UnityFunction_2= ruleUnityFunction )
            int alt51=3;
            switch ( input.LA(1) ) {
            case 42:
                {
                alt51=1;
                }
                break;
            case 49:
                {
                alt51=2;
                }
                break;
            case 51:
                {
                alt51=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 51, 0, input);

                throw nvae;
            }

            switch (alt51) {
                case 1 :
                    // InternalFCL.g:2467:3: this_NamedFunction_0= ruleNamedFunction
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getFunctionAccess().getNamedFunctionParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_NamedFunction_0=ruleNamedFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_NamedFunction_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalFCL.g:2476:3: this_FMUFunction_1= ruleFMUFunction
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getFunctionAccess().getFMUFunctionParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_FMUFunction_1=ruleFMUFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_FMUFunction_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalFCL.g:2485:3: this_UnityFunction_2= ruleUnityFunction
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getFunctionAccess().getUnityFunctionParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_UnityFunction_2=ruleUnityFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_UnityFunction_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunction"


    // $ANTLR start "entryRuleNamedFunction"
    // InternalFCL.g:2497:1: entryRuleNamedFunction returns [EObject current=null] : iv_ruleNamedFunction= ruleNamedFunction EOF ;
    public final EObject entryRuleNamedFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNamedFunction = null;


        try {
            // InternalFCL.g:2497:54: (iv_ruleNamedFunction= ruleNamedFunction EOF )
            // InternalFCL.g:2498:2: iv_ruleNamedFunction= ruleNamedFunction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNamedFunctionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleNamedFunction=ruleNamedFunction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNamedFunction; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNamedFunction"


    // $ANTLR start "ruleNamedFunction"
    // InternalFCL.g:2504:1: ruleNamedFunction returns [EObject current=null] : ( () otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}' )? (otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}' )? (otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) ) )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? (otherlv_20= 'internalDataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}' )? ) otherlv_25= '}' ) ;
    public final EObject ruleNamedFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        EObject lv_functionPorts_6_0 = null;

        EObject lv_functionPorts_7_0 = null;

        EObject lv_functionVarDecls_11_0 = null;

        EObject lv_functionVarDecls_12_0 = null;

        EObject lv_action_15_0 = null;

        EObject lv_timeReference_18_0 = null;

        EObject lv_subFunctions_22_0 = null;

        EObject lv_functionConnectors_23_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:2510:2: ( ( () otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}' )? (otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}' )? (otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) ) )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? (otherlv_20= 'internalDataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}' )? ) otherlv_25= '}' ) )
            // InternalFCL.g:2511:2: ( () otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}' )? (otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}' )? (otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) ) )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? (otherlv_20= 'internalDataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}' )? ) otherlv_25= '}' )
            {
            // InternalFCL.g:2511:2: ( () otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}' )? (otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}' )? (otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) ) )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? (otherlv_20= 'internalDataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}' )? ) otherlv_25= '}' )
            // InternalFCL.g:2512:3: () otherlv_1= 'function' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}' )? (otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}' )? (otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) ) )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? (otherlv_20= 'internalDataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}' )? ) otherlv_25= '}'
            {
            // InternalFCL.g:2512:3: ()
            // InternalFCL.g:2513:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getNamedFunctionAccess().getFunctionAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,42,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getNamedFunctionAccess().getFunctionKeyword_1());
              		
            }
            // InternalFCL.g:2523:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalFCL.g:2524:4: (lv_name_2_0= RULE_ID )
            {
            // InternalFCL.g:2524:4: (lv_name_2_0= RULE_ID )
            // InternalFCL.g:2525:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getNamedFunctionAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getNamedFunctionRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_51); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getNamedFunctionAccess().getLeftCurlyBracketKeyword_3());
              		
            }
            // InternalFCL.g:2545:3: ( (otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}' )? (otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}' )? (otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) ) )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? (otherlv_20= 'internalDataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}' )? )
            // InternalFCL.g:2546:4: (otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}' )? (otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}' )? (otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) ) )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? (otherlv_20= 'internalDataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}' )?
            {
            // InternalFCL.g:2546:4: (otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}' )?
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==43) ) {
                alt53=1;
            }
            switch (alt53) {
                case 1 :
                    // InternalFCL.g:2547:5: otherlv_4= 'ports' otherlv_5= '{' ( (lv_functionPorts_6_0= ruleFunctionPort ) ) ( (lv_functionPorts_7_0= ruleFunctionPort ) )* otherlv_8= '}'
                    {
                    otherlv_4=(Token)match(input,43,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_4, grammarAccess.getNamedFunctionAccess().getPortsKeyword_4_0_0());
                      				
                    }
                    otherlv_5=(Token)match(input,12,FOLLOW_41); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_5, grammarAccess.getNamedFunctionAccess().getLeftCurlyBracketKeyword_4_0_1());
                      				
                    }
                    // InternalFCL.g:2555:5: ( (lv_functionPorts_6_0= ruleFunctionPort ) )
                    // InternalFCL.g:2556:6: (lv_functionPorts_6_0= ruleFunctionPort )
                    {
                    // InternalFCL.g:2556:6: (lv_functionPorts_6_0= ruleFunctionPort )
                    // InternalFCL.g:2557:7: lv_functionPorts_6_0= ruleFunctionPort
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getNamedFunctionAccess().getFunctionPortsFunctionPortParserRuleCall_4_0_2_0());
                      						
                    }
                    pushFollow(FOLLOW_42);
                    lv_functionPorts_6_0=ruleFunctionPort();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getNamedFunctionRule());
                      							}
                      							add(
                      								current,
                      								"functionPorts",
                      								lv_functionPorts_6_0,
                      								"fr.inria.glose.fcl.FCL.FunctionPort");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }

                    // InternalFCL.g:2574:5: ( (lv_functionPorts_7_0= ruleFunctionPort ) )*
                    loop52:
                    do {
                        int alt52=2;
                        int LA52_0 = input.LA(1);

                        if ( (LA52_0==33||LA52_0==40) ) {
                            alt52=1;
                        }


                        switch (alt52) {
                    	case 1 :
                    	    // InternalFCL.g:2575:6: (lv_functionPorts_7_0= ruleFunctionPort )
                    	    {
                    	    // InternalFCL.g:2575:6: (lv_functionPorts_7_0= ruleFunctionPort )
                    	    // InternalFCL.g:2576:7: lv_functionPorts_7_0= ruleFunctionPort
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getNamedFunctionAccess().getFunctionPortsFunctionPortParserRuleCall_4_0_3_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_42);
                    	    lv_functionPorts_7_0=ruleFunctionPort();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getNamedFunctionRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"functionPorts",
                    	      								lv_functionPorts_7_0,
                    	      								"fr.inria.glose.fcl.FCL.FunctionPort");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop52;
                        }
                    } while (true);

                    otherlv_8=(Token)match(input,14,FOLLOW_52); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_8, grammarAccess.getNamedFunctionAccess().getRightCurlyBracketKeyword_4_0_4());
                      				
                    }

                    }
                    break;

            }

            // InternalFCL.g:2598:4: (otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}' )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==44) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // InternalFCL.g:2599:5: otherlv_9= 'variables' otherlv_10= '{' ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )* otherlv_13= '}'
                    {
                    otherlv_9=(Token)match(input,44,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_9, grammarAccess.getNamedFunctionAccess().getVariablesKeyword_4_1_0());
                      				
                    }
                    otherlv_10=(Token)match(input,12,FOLLOW_44); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_10, grammarAccess.getNamedFunctionAccess().getLeftCurlyBracketKeyword_4_1_1());
                      				
                    }
                    // InternalFCL.g:2607:5: ( (lv_functionVarDecls_11_0= ruleFunctionVarDecl ) )
                    // InternalFCL.g:2608:6: (lv_functionVarDecls_11_0= ruleFunctionVarDecl )
                    {
                    // InternalFCL.g:2608:6: (lv_functionVarDecls_11_0= ruleFunctionVarDecl )
                    // InternalFCL.g:2609:7: lv_functionVarDecls_11_0= ruleFunctionVarDecl
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getNamedFunctionAccess().getFunctionVarDeclsFunctionVarDeclParserRuleCall_4_1_2_0());
                      						
                    }
                    pushFollow(FOLLOW_45);
                    lv_functionVarDecls_11_0=ruleFunctionVarDecl();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getNamedFunctionRule());
                      							}
                      							add(
                      								current,
                      								"functionVarDecls",
                      								lv_functionVarDecls_11_0,
                      								"fr.inria.glose.fcl.FCL.FunctionVarDecl");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }

                    // InternalFCL.g:2626:5: ( (lv_functionVarDecls_12_0= ruleFunctionVarDecl ) )*
                    loop54:
                    do {
                        int alt54=2;
                        int LA54_0 = input.LA(1);

                        if ( (LA54_0==58) ) {
                            alt54=1;
                        }


                        switch (alt54) {
                    	case 1 :
                    	    // InternalFCL.g:2627:6: (lv_functionVarDecls_12_0= ruleFunctionVarDecl )
                    	    {
                    	    // InternalFCL.g:2627:6: (lv_functionVarDecls_12_0= ruleFunctionVarDecl )
                    	    // InternalFCL.g:2628:7: lv_functionVarDecls_12_0= ruleFunctionVarDecl
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getNamedFunctionAccess().getFunctionVarDeclsFunctionVarDeclParserRuleCall_4_1_3_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_45);
                    	    lv_functionVarDecls_12_0=ruleFunctionVarDecl();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getNamedFunctionRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"functionVarDecls",
                    	      								lv_functionVarDecls_12_0,
                    	      								"fr.inria.glose.fcl.FCL.FunctionVarDecl");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop54;
                        }
                    } while (true);

                    otherlv_13=(Token)match(input,14,FOLLOW_53); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_13, grammarAccess.getNamedFunctionAccess().getRightCurlyBracketKeyword_4_1_4());
                      				
                    }

                    }
                    break;

            }

            // InternalFCL.g:2650:4: (otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) ) )?
            int alt56=2;
            int LA56_0 = input.LA(1);

            if ( (LA56_0==45) ) {
                alt56=1;
            }
            switch (alt56) {
                case 1 :
                    // InternalFCL.g:2651:5: otherlv_14= 'action' ( (lv_action_15_0= ruleAction ) )
                    {
                    otherlv_14=(Token)match(input,45,FOLLOW_19); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_14, grammarAccess.getNamedFunctionAccess().getActionKeyword_4_2_0());
                      				
                    }
                    // InternalFCL.g:2655:5: ( (lv_action_15_0= ruleAction ) )
                    // InternalFCL.g:2656:6: (lv_action_15_0= ruleAction )
                    {
                    // InternalFCL.g:2656:6: (lv_action_15_0= ruleAction )
                    // InternalFCL.g:2657:7: lv_action_15_0= ruleAction
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getNamedFunctionAccess().getActionActionParserRuleCall_4_2_1_0());
                      						
                    }
                    pushFollow(FOLLOW_54);
                    lv_action_15_0=ruleAction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getNamedFunctionRule());
                      							}
                      							set(
                      								current,
                      								"action",
                      								lv_action_15_0,
                      								"fr.inria.glose.fcl.FCL.Action");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:2675:4: (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==46) ) {
                alt57=1;
            }
            switch (alt57) {
                case 1 :
                    // InternalFCL.g:2676:5: otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}'
                    {
                    otherlv_16=(Token)match(input,46,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_16, grammarAccess.getNamedFunctionAccess().getTimeReferenceKeyword_4_3_0());
                      				
                    }
                    otherlv_17=(Token)match(input,12,FOLLOW_48); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_17, grammarAccess.getNamedFunctionAccess().getLeftCurlyBracketKeyword_4_3_1());
                      				
                    }
                    // InternalFCL.g:2684:5: ( (lv_timeReference_18_0= ruleTimeReference ) )
                    // InternalFCL.g:2685:6: (lv_timeReference_18_0= ruleTimeReference )
                    {
                    // InternalFCL.g:2685:6: (lv_timeReference_18_0= ruleTimeReference )
                    // InternalFCL.g:2686:7: lv_timeReference_18_0= ruleTimeReference
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getNamedFunctionAccess().getTimeReferenceTimeReferenceParserRuleCall_4_3_2_0());
                      						
                    }
                    pushFollow(FOLLOW_16);
                    lv_timeReference_18_0=ruleTimeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getNamedFunctionRule());
                      							}
                      							set(
                      								current,
                      								"timeReference",
                      								lv_timeReference_18_0,
                      								"fr.inria.glose.fcl.FCL.TimeReference");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }

                    otherlv_19=(Token)match(input,14,FOLLOW_55); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_19, grammarAccess.getNamedFunctionAccess().getRightCurlyBracketKeyword_4_3_3());
                      				
                    }

                    }
                    break;

            }

            // InternalFCL.g:2708:4: (otherlv_20= 'internalDataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}' )?
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==48) ) {
                alt59=1;
            }
            switch (alt59) {
                case 1 :
                    // InternalFCL.g:2709:5: otherlv_20= 'internalDataFlow' otherlv_21= '{' ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )* otherlv_24= '}'
                    {
                    otherlv_20=(Token)match(input,48,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_20, grammarAccess.getNamedFunctionAccess().getInternalDataFlowKeyword_4_4_0());
                      				
                    }
                    otherlv_21=(Token)match(input,12,FOLLOW_50); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_21, grammarAccess.getNamedFunctionAccess().getLeftCurlyBracketKeyword_4_4_1());
                      				
                    }
                    // InternalFCL.g:2717:5: ( ( (lv_subFunctions_22_0= ruleFunction ) ) | ( (lv_functionConnectors_23_0= ruleFunctionConnector ) ) )*
                    loop58:
                    do {
                        int alt58=3;
                        int LA58_0 = input.LA(1);

                        if ( (LA58_0==42||LA58_0==49||LA58_0==51) ) {
                            alt58=1;
                        }
                        else if ( (LA58_0==55) ) {
                            alt58=2;
                        }


                        switch (alt58) {
                    	case 1 :
                    	    // InternalFCL.g:2718:6: ( (lv_subFunctions_22_0= ruleFunction ) )
                    	    {
                    	    // InternalFCL.g:2718:6: ( (lv_subFunctions_22_0= ruleFunction ) )
                    	    // InternalFCL.g:2719:7: (lv_subFunctions_22_0= ruleFunction )
                    	    {
                    	    // InternalFCL.g:2719:7: (lv_subFunctions_22_0= ruleFunction )
                    	    // InternalFCL.g:2720:8: lv_subFunctions_22_0= ruleFunction
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      								newCompositeNode(grammarAccess.getNamedFunctionAccess().getSubFunctionsFunctionParserRuleCall_4_4_2_0_0());
                    	      							
                    	    }
                    	    pushFollow(FOLLOW_50);
                    	    lv_subFunctions_22_0=ruleFunction();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      								if (current==null) {
                    	      									current = createModelElementForParent(grammarAccess.getNamedFunctionRule());
                    	      								}
                    	      								add(
                    	      									current,
                    	      									"subFunctions",
                    	      									lv_subFunctions_22_0,
                    	      									"fr.inria.glose.fcl.FCL.Function");
                    	      								afterParserOrEnumRuleCall();
                    	      							
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalFCL.g:2738:6: ( (lv_functionConnectors_23_0= ruleFunctionConnector ) )
                    	    {
                    	    // InternalFCL.g:2738:6: ( (lv_functionConnectors_23_0= ruleFunctionConnector ) )
                    	    // InternalFCL.g:2739:7: (lv_functionConnectors_23_0= ruleFunctionConnector )
                    	    {
                    	    // InternalFCL.g:2739:7: (lv_functionConnectors_23_0= ruleFunctionConnector )
                    	    // InternalFCL.g:2740:8: lv_functionConnectors_23_0= ruleFunctionConnector
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      								newCompositeNode(grammarAccess.getNamedFunctionAccess().getFunctionConnectorsFunctionConnectorParserRuleCall_4_4_2_1_0());
                    	      							
                    	    }
                    	    pushFollow(FOLLOW_50);
                    	    lv_functionConnectors_23_0=ruleFunctionConnector();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      								if (current==null) {
                    	      									current = createModelElementForParent(grammarAccess.getNamedFunctionRule());
                    	      								}
                    	      								add(
                    	      									current,
                    	      									"functionConnectors",
                    	      									lv_functionConnectors_23_0,
                    	      									"fr.inria.glose.fcl.FCL.FunctionConnector");
                    	      								afterParserOrEnumRuleCall();
                    	      							
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop58;
                        }
                    } while (true);

                    otherlv_24=(Token)match(input,14,FOLLOW_16); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_24, grammarAccess.getNamedFunctionAccess().getRightCurlyBracketKeyword_4_4_3());
                      				
                    }

                    }
                    break;

            }


            }

            otherlv_25=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_25, grammarAccess.getNamedFunctionAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNamedFunction"


    // $ANTLR start "entryRuleFMUFunction"
    // InternalFCL.g:2772:1: entryRuleFMUFunction returns [EObject current=null] : iv_ruleFMUFunction= ruleFMUFunction EOF ;
    public final EObject entryRuleFMUFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFMUFunction = null;


        try {
            // InternalFCL.g:2772:52: (iv_ruleFMUFunction= ruleFMUFunction EOF )
            // InternalFCL.g:2773:2: iv_ruleFMUFunction= ruleFMUFunction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFMUFunctionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFMUFunction=ruleFMUFunction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFMUFunction; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFMUFunction"


    // $ANTLR start "ruleFMUFunction"
    // InternalFCL.g:2779:1: ruleFMUFunction returns [EObject current=null] : ( () (otherlv_1= 'fmu' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'runnableFmuPath' ( (lv_runnableFmuPath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleFMUFunctionBlockDataPort ) ) ( (lv_functionPorts_9_0= ruleFMUFunctionBlockDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? ) otherlv_20= '}' ) ) ;
    public final EObject ruleFMUFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_runnableFmuPath_5_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        EObject lv_functionPorts_8_0 = null;

        EObject lv_functionPorts_9_0 = null;

        EObject lv_functionVarDecls_13_0 = null;

        EObject lv_functionVarDecls_14_0 = null;

        EObject lv_timeReference_18_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:2785:2: ( ( () (otherlv_1= 'fmu' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'runnableFmuPath' ( (lv_runnableFmuPath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleFMUFunctionBlockDataPort ) ) ( (lv_functionPorts_9_0= ruleFMUFunctionBlockDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? ) otherlv_20= '}' ) ) )
            // InternalFCL.g:2786:2: ( () (otherlv_1= 'fmu' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'runnableFmuPath' ( (lv_runnableFmuPath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleFMUFunctionBlockDataPort ) ) ( (lv_functionPorts_9_0= ruleFMUFunctionBlockDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? ) otherlv_20= '}' ) )
            {
            // InternalFCL.g:2786:2: ( () (otherlv_1= 'fmu' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'runnableFmuPath' ( (lv_runnableFmuPath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleFMUFunctionBlockDataPort ) ) ( (lv_functionPorts_9_0= ruleFMUFunctionBlockDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? ) otherlv_20= '}' ) )
            // InternalFCL.g:2787:3: () (otherlv_1= 'fmu' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'runnableFmuPath' ( (lv_runnableFmuPath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleFMUFunctionBlockDataPort ) ) ( (lv_functionPorts_9_0= ruleFMUFunctionBlockDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? ) otherlv_20= '}' )
            {
            // InternalFCL.g:2787:3: ()
            // InternalFCL.g:2788:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getFMUFunctionAccess().getFMUFunctionAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:2794:3: (otherlv_1= 'fmu' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'runnableFmuPath' ( (lv_runnableFmuPath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleFMUFunctionBlockDataPort ) ) ( (lv_functionPorts_9_0= ruleFMUFunctionBlockDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? ) otherlv_20= '}' )
            // InternalFCL.g:2795:4: otherlv_1= 'fmu' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'runnableFmuPath' ( (lv_runnableFmuPath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleFMUFunctionBlockDataPort ) ) ( (lv_functionPorts_9_0= ruleFMUFunctionBlockDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? ) otherlv_20= '}'
            {
            otherlv_1=(Token)match(input,49,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(otherlv_1, grammarAccess.getFMUFunctionAccess().getFmuKeyword_1_0());
              			
            }
            // InternalFCL.g:2799:4: ( (lv_name_2_0= RULE_ID ) )
            // InternalFCL.g:2800:5: (lv_name_2_0= RULE_ID )
            {
            // InternalFCL.g:2800:5: (lv_name_2_0= RULE_ID )
            // InternalFCL.g:2801:6: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              						newLeafNode(lv_name_2_0, grammarAccess.getFMUFunctionAccess().getNameIDTerminalRuleCall_1_1_0());
              					
            }
            if ( state.backtracking==0 ) {

              						if (current==null) {
              							current = createModelElement(grammarAccess.getFMUFunctionRule());
              						}
              						setWithLastConsumed(
              							current,
              							"name",
              							lv_name_2_0,
              							"org.eclipse.xtext.common.Terminals.ID");
              					
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_56); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(otherlv_3, grammarAccess.getFMUFunctionAccess().getLeftCurlyBracketKeyword_1_2());
              			
            }
            // InternalFCL.g:2821:4: ( (otherlv_4= 'runnableFmuPath' ( (lv_runnableFmuPath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleFMUFunctionBlockDataPort ) ) ( (lv_functionPorts_9_0= ruleFMUFunctionBlockDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? )
            // InternalFCL.g:2822:5: (otherlv_4= 'runnableFmuPath' ( (lv_runnableFmuPath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleFMUFunctionBlockDataPort ) ) ( (lv_functionPorts_9_0= ruleFMUFunctionBlockDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )?
            {
            // InternalFCL.g:2822:5: (otherlv_4= 'runnableFmuPath' ( (lv_runnableFmuPath_5_0= RULE_STRING ) ) )?
            int alt60=2;
            int LA60_0 = input.LA(1);

            if ( (LA60_0==50) ) {
                alt60=1;
            }
            switch (alt60) {
                case 1 :
                    // InternalFCL.g:2823:6: otherlv_4= 'runnableFmuPath' ( (lv_runnableFmuPath_5_0= RULE_STRING ) )
                    {
                    otherlv_4=(Token)match(input,50,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_4, grammarAccess.getFMUFunctionAccess().getRunnableFmuPathKeyword_1_3_0_0());
                      					
                    }
                    // InternalFCL.g:2827:6: ( (lv_runnableFmuPath_5_0= RULE_STRING ) )
                    // InternalFCL.g:2828:7: (lv_runnableFmuPath_5_0= RULE_STRING )
                    {
                    // InternalFCL.g:2828:7: (lv_runnableFmuPath_5_0= RULE_STRING )
                    // InternalFCL.g:2829:8: lv_runnableFmuPath_5_0= RULE_STRING
                    {
                    lv_runnableFmuPath_5_0=(Token)match(input,RULE_STRING,FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      								newLeafNode(lv_runnableFmuPath_5_0, grammarAccess.getFMUFunctionAccess().getRunnableFmuPathSTRINGTerminalRuleCall_1_3_0_1_0());
                      							
                    }
                    if ( state.backtracking==0 ) {

                      								if (current==null) {
                      									current = createModelElement(grammarAccess.getFMUFunctionRule());
                      								}
                      								setWithLastConsumed(
                      									current,
                      									"runnableFmuPath",
                      									lv_runnableFmuPath_5_0,
                      									"org.eclipse.xtext.common.Terminals.STRING");
                      							
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:2846:5: (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleFMUFunctionBlockDataPort ) ) ( (lv_functionPorts_9_0= ruleFMUFunctionBlockDataPort ) )* otherlv_10= '}' )?
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==43) ) {
                alt62=1;
            }
            switch (alt62) {
                case 1 :
                    // InternalFCL.g:2847:6: otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleFMUFunctionBlockDataPort ) ) ( (lv_functionPorts_9_0= ruleFMUFunctionBlockDataPort ) )* otherlv_10= '}'
                    {
                    otherlv_6=(Token)match(input,43,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_6, grammarAccess.getFMUFunctionAccess().getPortsKeyword_1_3_1_0());
                      					
                    }
                    otherlv_7=(Token)match(input,12,FOLLOW_58); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_7, grammarAccess.getFMUFunctionAccess().getLeftCurlyBracketKeyword_1_3_1_1());
                      					
                    }
                    // InternalFCL.g:2855:6: ( (lv_functionPorts_8_0= ruleFMUFunctionBlockDataPort ) )
                    // InternalFCL.g:2856:7: (lv_functionPorts_8_0= ruleFMUFunctionBlockDataPort )
                    {
                    // InternalFCL.g:2856:7: (lv_functionPorts_8_0= ruleFMUFunctionBlockDataPort )
                    // InternalFCL.g:2857:8: lv_functionPorts_8_0= ruleFMUFunctionBlockDataPort
                    {
                    if ( state.backtracking==0 ) {

                      								newCompositeNode(grammarAccess.getFMUFunctionAccess().getFunctionPortsFMUFunctionBlockDataPortParserRuleCall_1_3_1_2_0());
                      							
                    }
                    pushFollow(FOLLOW_59);
                    lv_functionPorts_8_0=ruleFMUFunctionBlockDataPort();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      								if (current==null) {
                      									current = createModelElementForParent(grammarAccess.getFMUFunctionRule());
                      								}
                      								add(
                      									current,
                      									"functionPorts",
                      									lv_functionPorts_8_0,
                      									"fr.inria.glose.fcl.FCL.FMUFunctionBlockDataPort");
                      								afterParserOrEnumRuleCall();
                      							
                    }

                    }


                    }

                    // InternalFCL.g:2874:6: ( (lv_functionPorts_9_0= ruleFMUFunctionBlockDataPort ) )*
                    loop61:
                    do {
                        int alt61=2;
                        int LA61_0 = input.LA(1);

                        if ( (LA61_0==33) ) {
                            alt61=1;
                        }


                        switch (alt61) {
                    	case 1 :
                    	    // InternalFCL.g:2875:7: (lv_functionPorts_9_0= ruleFMUFunctionBlockDataPort )
                    	    {
                    	    // InternalFCL.g:2875:7: (lv_functionPorts_9_0= ruleFMUFunctionBlockDataPort )
                    	    // InternalFCL.g:2876:8: lv_functionPorts_9_0= ruleFMUFunctionBlockDataPort
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      								newCompositeNode(grammarAccess.getFMUFunctionAccess().getFunctionPortsFMUFunctionBlockDataPortParserRuleCall_1_3_1_3_0());
                    	      							
                    	    }
                    	    pushFollow(FOLLOW_59);
                    	    lv_functionPorts_9_0=ruleFMUFunctionBlockDataPort();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      								if (current==null) {
                    	      									current = createModelElementForParent(grammarAccess.getFMUFunctionRule());
                    	      								}
                    	      								add(
                    	      									current,
                    	      									"functionPorts",
                    	      									lv_functionPorts_9_0,
                    	      									"fr.inria.glose.fcl.FCL.FMUFunctionBlockDataPort");
                    	      								afterParserOrEnumRuleCall();
                    	      							
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop61;
                        }
                    } while (true);

                    otherlv_10=(Token)match(input,14,FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_10, grammarAccess.getFMUFunctionAccess().getRightCurlyBracketKeyword_1_3_1_4());
                      					
                    }

                    }
                    break;

            }

            // InternalFCL.g:2898:5: (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )?
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( (LA64_0==44) ) {
                alt64=1;
            }
            switch (alt64) {
                case 1 :
                    // InternalFCL.g:2899:6: otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}'
                    {
                    otherlv_11=(Token)match(input,44,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_11, grammarAccess.getFMUFunctionAccess().getVariablesKeyword_1_3_2_0());
                      					
                    }
                    otherlv_12=(Token)match(input,12,FOLLOW_44); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_12, grammarAccess.getFMUFunctionAccess().getLeftCurlyBracketKeyword_1_3_2_1());
                      					
                    }
                    // InternalFCL.g:2907:6: ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) )
                    // InternalFCL.g:2908:7: (lv_functionVarDecls_13_0= ruleFunctionVarDecl )
                    {
                    // InternalFCL.g:2908:7: (lv_functionVarDecls_13_0= ruleFunctionVarDecl )
                    // InternalFCL.g:2909:8: lv_functionVarDecls_13_0= ruleFunctionVarDecl
                    {
                    if ( state.backtracking==0 ) {

                      								newCompositeNode(grammarAccess.getFMUFunctionAccess().getFunctionVarDeclsFunctionVarDeclParserRuleCall_1_3_2_2_0());
                      							
                    }
                    pushFollow(FOLLOW_45);
                    lv_functionVarDecls_13_0=ruleFunctionVarDecl();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      								if (current==null) {
                      									current = createModelElementForParent(grammarAccess.getFMUFunctionRule());
                      								}
                      								add(
                      									current,
                      									"functionVarDecls",
                      									lv_functionVarDecls_13_0,
                      									"fr.inria.glose.fcl.FCL.FunctionVarDecl");
                      								afterParserOrEnumRuleCall();
                      							
                    }

                    }


                    }

                    // InternalFCL.g:2926:6: ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )*
                    loop63:
                    do {
                        int alt63=2;
                        int LA63_0 = input.LA(1);

                        if ( (LA63_0==58) ) {
                            alt63=1;
                        }


                        switch (alt63) {
                    	case 1 :
                    	    // InternalFCL.g:2927:7: (lv_functionVarDecls_14_0= ruleFunctionVarDecl )
                    	    {
                    	    // InternalFCL.g:2927:7: (lv_functionVarDecls_14_0= ruleFunctionVarDecl )
                    	    // InternalFCL.g:2928:8: lv_functionVarDecls_14_0= ruleFunctionVarDecl
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      								newCompositeNode(grammarAccess.getFMUFunctionAccess().getFunctionVarDeclsFunctionVarDeclParserRuleCall_1_3_2_3_0());
                    	      							
                    	    }
                    	    pushFollow(FOLLOW_45);
                    	    lv_functionVarDecls_14_0=ruleFunctionVarDecl();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      								if (current==null) {
                    	      									current = createModelElementForParent(grammarAccess.getFMUFunctionRule());
                    	      								}
                    	      								add(
                    	      									current,
                    	      									"functionVarDecls",
                    	      									lv_functionVarDecls_14_0,
                    	      									"fr.inria.glose.fcl.FCL.FunctionVarDecl");
                    	      								afterParserOrEnumRuleCall();
                    	      							
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop63;
                        }
                    } while (true);

                    otherlv_15=(Token)match(input,14,FOLLOW_61); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_15, grammarAccess.getFMUFunctionAccess().getRightCurlyBracketKeyword_1_3_2_4());
                      					
                    }

                    }
                    break;

            }

            // InternalFCL.g:2950:5: (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )?
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( (LA65_0==46) ) {
                alt65=1;
            }
            switch (alt65) {
                case 1 :
                    // InternalFCL.g:2951:6: otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}'
                    {
                    otherlv_16=(Token)match(input,46,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_16, grammarAccess.getFMUFunctionAccess().getTimeReferenceKeyword_1_3_3_0());
                      					
                    }
                    otherlv_17=(Token)match(input,12,FOLLOW_48); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_17, grammarAccess.getFMUFunctionAccess().getLeftCurlyBracketKeyword_1_3_3_1());
                      					
                    }
                    // InternalFCL.g:2959:6: ( (lv_timeReference_18_0= ruleTimeReference ) )
                    // InternalFCL.g:2960:7: (lv_timeReference_18_0= ruleTimeReference )
                    {
                    // InternalFCL.g:2960:7: (lv_timeReference_18_0= ruleTimeReference )
                    // InternalFCL.g:2961:8: lv_timeReference_18_0= ruleTimeReference
                    {
                    if ( state.backtracking==0 ) {

                      								newCompositeNode(grammarAccess.getFMUFunctionAccess().getTimeReferenceTimeReferenceParserRuleCall_1_3_3_2_0());
                      							
                    }
                    pushFollow(FOLLOW_16);
                    lv_timeReference_18_0=ruleTimeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      								if (current==null) {
                      									current = createModelElementForParent(grammarAccess.getFMUFunctionRule());
                      								}
                      								set(
                      									current,
                      									"timeReference",
                      									lv_timeReference_18_0,
                      									"fr.inria.glose.fcl.FCL.TimeReference");
                      								afterParserOrEnumRuleCall();
                      							
                    }

                    }


                    }

                    otherlv_19=(Token)match(input,14,FOLLOW_16); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_19, grammarAccess.getFMUFunctionAccess().getRightCurlyBracketKeyword_1_3_3_3());
                      					
                    }

                    }
                    break;

            }


            }

            otherlv_20=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(otherlv_20, grammarAccess.getFMUFunctionAccess().getRightCurlyBracketKeyword_1_4());
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFMUFunction"


    // $ANTLR start "entryRuleUnityFunction"
    // InternalFCL.g:2993:1: entryRuleUnityFunction returns [EObject current=null] : iv_ruleUnityFunction= ruleUnityFunction EOF ;
    public final EObject entryRuleUnityFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnityFunction = null;


        try {
            // InternalFCL.g:2993:54: (iv_ruleUnityFunction= ruleUnityFunction EOF )
            // InternalFCL.g:2994:2: iv_ruleUnityFunction= ruleUnityFunction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnityFunctionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleUnityFunction=ruleUnityFunction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnityFunction; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnityFunction"


    // $ANTLR start "ruleUnityFunction"
    // InternalFCL.g:3000:1: ruleUnityFunction returns [EObject current=null] : ( () (otherlv_1= 'unityFunction' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'unityWebPagePath' ( (lv_unityWebPagePath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleUnityFunctionDataPort ) ) ( (lv_functionPorts_9_0= ruleUnityFunctionDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? ) otherlv_20= '}' ) ) ;
    public final EObject ruleUnityFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_unityWebPagePath_5_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        EObject lv_functionPorts_8_0 = null;

        EObject lv_functionPorts_9_0 = null;

        EObject lv_functionVarDecls_13_0 = null;

        EObject lv_functionVarDecls_14_0 = null;

        EObject lv_timeReference_18_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:3006:2: ( ( () (otherlv_1= 'unityFunction' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'unityWebPagePath' ( (lv_unityWebPagePath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleUnityFunctionDataPort ) ) ( (lv_functionPorts_9_0= ruleUnityFunctionDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? ) otherlv_20= '}' ) ) )
            // InternalFCL.g:3007:2: ( () (otherlv_1= 'unityFunction' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'unityWebPagePath' ( (lv_unityWebPagePath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleUnityFunctionDataPort ) ) ( (lv_functionPorts_9_0= ruleUnityFunctionDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? ) otherlv_20= '}' ) )
            {
            // InternalFCL.g:3007:2: ( () (otherlv_1= 'unityFunction' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'unityWebPagePath' ( (lv_unityWebPagePath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleUnityFunctionDataPort ) ) ( (lv_functionPorts_9_0= ruleUnityFunctionDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? ) otherlv_20= '}' ) )
            // InternalFCL.g:3008:3: () (otherlv_1= 'unityFunction' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'unityWebPagePath' ( (lv_unityWebPagePath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleUnityFunctionDataPort ) ) ( (lv_functionPorts_9_0= ruleUnityFunctionDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? ) otherlv_20= '}' )
            {
            // InternalFCL.g:3008:3: ()
            // InternalFCL.g:3009:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getUnityFunctionAccess().getUnityFunctionAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:3015:3: (otherlv_1= 'unityFunction' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'unityWebPagePath' ( (lv_unityWebPagePath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleUnityFunctionDataPort ) ) ( (lv_functionPorts_9_0= ruleUnityFunctionDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? ) otherlv_20= '}' )
            // InternalFCL.g:3016:4: otherlv_1= 'unityFunction' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (otherlv_4= 'unityWebPagePath' ( (lv_unityWebPagePath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleUnityFunctionDataPort ) ) ( (lv_functionPorts_9_0= ruleUnityFunctionDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? ) otherlv_20= '}'
            {
            otherlv_1=(Token)match(input,51,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(otherlv_1, grammarAccess.getUnityFunctionAccess().getUnityFunctionKeyword_1_0());
              			
            }
            // InternalFCL.g:3020:4: ( (lv_name_2_0= RULE_ID ) )
            // InternalFCL.g:3021:5: (lv_name_2_0= RULE_ID )
            {
            // InternalFCL.g:3021:5: (lv_name_2_0= RULE_ID )
            // InternalFCL.g:3022:6: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              						newLeafNode(lv_name_2_0, grammarAccess.getUnityFunctionAccess().getNameIDTerminalRuleCall_1_1_0());
              					
            }
            if ( state.backtracking==0 ) {

              						if (current==null) {
              							current = createModelElement(grammarAccess.getUnityFunctionRule());
              						}
              						setWithLastConsumed(
              							current,
              							"name",
              							lv_name_2_0,
              							"org.eclipse.xtext.common.Terminals.ID");
              					
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_62); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(otherlv_3, grammarAccess.getUnityFunctionAccess().getLeftCurlyBracketKeyword_1_2());
              			
            }
            // InternalFCL.g:3042:4: ( (otherlv_4= 'unityWebPagePath' ( (lv_unityWebPagePath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleUnityFunctionDataPort ) ) ( (lv_functionPorts_9_0= ruleUnityFunctionDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )? )
            // InternalFCL.g:3043:5: (otherlv_4= 'unityWebPagePath' ( (lv_unityWebPagePath_5_0= RULE_STRING ) ) )? (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleUnityFunctionDataPort ) ) ( (lv_functionPorts_9_0= ruleUnityFunctionDataPort ) )* otherlv_10= '}' )? (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )? (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )?
            {
            // InternalFCL.g:3043:5: (otherlv_4= 'unityWebPagePath' ( (lv_unityWebPagePath_5_0= RULE_STRING ) ) )?
            int alt66=2;
            int LA66_0 = input.LA(1);

            if ( (LA66_0==52) ) {
                alt66=1;
            }
            switch (alt66) {
                case 1 :
                    // InternalFCL.g:3044:6: otherlv_4= 'unityWebPagePath' ( (lv_unityWebPagePath_5_0= RULE_STRING ) )
                    {
                    otherlv_4=(Token)match(input,52,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_4, grammarAccess.getUnityFunctionAccess().getUnityWebPagePathKeyword_1_3_0_0());
                      					
                    }
                    // InternalFCL.g:3048:6: ( (lv_unityWebPagePath_5_0= RULE_STRING ) )
                    // InternalFCL.g:3049:7: (lv_unityWebPagePath_5_0= RULE_STRING )
                    {
                    // InternalFCL.g:3049:7: (lv_unityWebPagePath_5_0= RULE_STRING )
                    // InternalFCL.g:3050:8: lv_unityWebPagePath_5_0= RULE_STRING
                    {
                    lv_unityWebPagePath_5_0=(Token)match(input,RULE_STRING,FOLLOW_57); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      								newLeafNode(lv_unityWebPagePath_5_0, grammarAccess.getUnityFunctionAccess().getUnityWebPagePathSTRINGTerminalRuleCall_1_3_0_1_0());
                      							
                    }
                    if ( state.backtracking==0 ) {

                      								if (current==null) {
                      									current = createModelElement(grammarAccess.getUnityFunctionRule());
                      								}
                      								setWithLastConsumed(
                      									current,
                      									"unityWebPagePath",
                      									lv_unityWebPagePath_5_0,
                      									"org.eclipse.xtext.common.Terminals.STRING");
                      							
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:3067:5: (otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleUnityFunctionDataPort ) ) ( (lv_functionPorts_9_0= ruleUnityFunctionDataPort ) )* otherlv_10= '}' )?
            int alt68=2;
            int LA68_0 = input.LA(1);

            if ( (LA68_0==43) ) {
                alt68=1;
            }
            switch (alt68) {
                case 1 :
                    // InternalFCL.g:3068:6: otherlv_6= 'ports' otherlv_7= '{' ( (lv_functionPorts_8_0= ruleUnityFunctionDataPort ) ) ( (lv_functionPorts_9_0= ruleUnityFunctionDataPort ) )* otherlv_10= '}'
                    {
                    otherlv_6=(Token)match(input,43,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_6, grammarAccess.getUnityFunctionAccess().getPortsKeyword_1_3_1_0());
                      					
                    }
                    otherlv_7=(Token)match(input,12,FOLLOW_58); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_7, grammarAccess.getUnityFunctionAccess().getLeftCurlyBracketKeyword_1_3_1_1());
                      					
                    }
                    // InternalFCL.g:3076:6: ( (lv_functionPorts_8_0= ruleUnityFunctionDataPort ) )
                    // InternalFCL.g:3077:7: (lv_functionPorts_8_0= ruleUnityFunctionDataPort )
                    {
                    // InternalFCL.g:3077:7: (lv_functionPorts_8_0= ruleUnityFunctionDataPort )
                    // InternalFCL.g:3078:8: lv_functionPorts_8_0= ruleUnityFunctionDataPort
                    {
                    if ( state.backtracking==0 ) {

                      								newCompositeNode(grammarAccess.getUnityFunctionAccess().getFunctionPortsUnityFunctionDataPortParserRuleCall_1_3_1_2_0());
                      							
                    }
                    pushFollow(FOLLOW_59);
                    lv_functionPorts_8_0=ruleUnityFunctionDataPort();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      								if (current==null) {
                      									current = createModelElementForParent(grammarAccess.getUnityFunctionRule());
                      								}
                      								add(
                      									current,
                      									"functionPorts",
                      									lv_functionPorts_8_0,
                      									"fr.inria.glose.fcl.FCL.UnityFunctionDataPort");
                      								afterParserOrEnumRuleCall();
                      							
                    }

                    }


                    }

                    // InternalFCL.g:3095:6: ( (lv_functionPorts_9_0= ruleUnityFunctionDataPort ) )*
                    loop67:
                    do {
                        int alt67=2;
                        int LA67_0 = input.LA(1);

                        if ( (LA67_0==33) ) {
                            alt67=1;
                        }


                        switch (alt67) {
                    	case 1 :
                    	    // InternalFCL.g:3096:7: (lv_functionPorts_9_0= ruleUnityFunctionDataPort )
                    	    {
                    	    // InternalFCL.g:3096:7: (lv_functionPorts_9_0= ruleUnityFunctionDataPort )
                    	    // InternalFCL.g:3097:8: lv_functionPorts_9_0= ruleUnityFunctionDataPort
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      								newCompositeNode(grammarAccess.getUnityFunctionAccess().getFunctionPortsUnityFunctionDataPortParserRuleCall_1_3_1_3_0());
                    	      							
                    	    }
                    	    pushFollow(FOLLOW_59);
                    	    lv_functionPorts_9_0=ruleUnityFunctionDataPort();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      								if (current==null) {
                    	      									current = createModelElementForParent(grammarAccess.getUnityFunctionRule());
                    	      								}
                    	      								add(
                    	      									current,
                    	      									"functionPorts",
                    	      									lv_functionPorts_9_0,
                    	      									"fr.inria.glose.fcl.FCL.UnityFunctionDataPort");
                    	      								afterParserOrEnumRuleCall();
                    	      							
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop67;
                        }
                    } while (true);

                    otherlv_10=(Token)match(input,14,FOLLOW_60); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_10, grammarAccess.getUnityFunctionAccess().getRightCurlyBracketKeyword_1_3_1_4());
                      					
                    }

                    }
                    break;

            }

            // InternalFCL.g:3119:5: (otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}' )?
            int alt70=2;
            int LA70_0 = input.LA(1);

            if ( (LA70_0==44) ) {
                alt70=1;
            }
            switch (alt70) {
                case 1 :
                    // InternalFCL.g:3120:6: otherlv_11= 'variables' otherlv_12= '{' ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) ) ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )* otherlv_15= '}'
                    {
                    otherlv_11=(Token)match(input,44,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_11, grammarAccess.getUnityFunctionAccess().getVariablesKeyword_1_3_2_0());
                      					
                    }
                    otherlv_12=(Token)match(input,12,FOLLOW_44); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_12, grammarAccess.getUnityFunctionAccess().getLeftCurlyBracketKeyword_1_3_2_1());
                      					
                    }
                    // InternalFCL.g:3128:6: ( (lv_functionVarDecls_13_0= ruleFunctionVarDecl ) )
                    // InternalFCL.g:3129:7: (lv_functionVarDecls_13_0= ruleFunctionVarDecl )
                    {
                    // InternalFCL.g:3129:7: (lv_functionVarDecls_13_0= ruleFunctionVarDecl )
                    // InternalFCL.g:3130:8: lv_functionVarDecls_13_0= ruleFunctionVarDecl
                    {
                    if ( state.backtracking==0 ) {

                      								newCompositeNode(grammarAccess.getUnityFunctionAccess().getFunctionVarDeclsFunctionVarDeclParserRuleCall_1_3_2_2_0());
                      							
                    }
                    pushFollow(FOLLOW_45);
                    lv_functionVarDecls_13_0=ruleFunctionVarDecl();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      								if (current==null) {
                      									current = createModelElementForParent(grammarAccess.getUnityFunctionRule());
                      								}
                      								add(
                      									current,
                      									"functionVarDecls",
                      									lv_functionVarDecls_13_0,
                      									"fr.inria.glose.fcl.FCL.FunctionVarDecl");
                      								afterParserOrEnumRuleCall();
                      							
                    }

                    }


                    }

                    // InternalFCL.g:3147:6: ( (lv_functionVarDecls_14_0= ruleFunctionVarDecl ) )*
                    loop69:
                    do {
                        int alt69=2;
                        int LA69_0 = input.LA(1);

                        if ( (LA69_0==58) ) {
                            alt69=1;
                        }


                        switch (alt69) {
                    	case 1 :
                    	    // InternalFCL.g:3148:7: (lv_functionVarDecls_14_0= ruleFunctionVarDecl )
                    	    {
                    	    // InternalFCL.g:3148:7: (lv_functionVarDecls_14_0= ruleFunctionVarDecl )
                    	    // InternalFCL.g:3149:8: lv_functionVarDecls_14_0= ruleFunctionVarDecl
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      								newCompositeNode(grammarAccess.getUnityFunctionAccess().getFunctionVarDeclsFunctionVarDeclParserRuleCall_1_3_2_3_0());
                    	      							
                    	    }
                    	    pushFollow(FOLLOW_45);
                    	    lv_functionVarDecls_14_0=ruleFunctionVarDecl();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      								if (current==null) {
                    	      									current = createModelElementForParent(grammarAccess.getUnityFunctionRule());
                    	      								}
                    	      								add(
                    	      									current,
                    	      									"functionVarDecls",
                    	      									lv_functionVarDecls_14_0,
                    	      									"fr.inria.glose.fcl.FCL.FunctionVarDecl");
                    	      								afterParserOrEnumRuleCall();
                    	      							
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop69;
                        }
                    } while (true);

                    otherlv_15=(Token)match(input,14,FOLLOW_61); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_15, grammarAccess.getUnityFunctionAccess().getRightCurlyBracketKeyword_1_3_2_4());
                      					
                    }

                    }
                    break;

            }

            // InternalFCL.g:3171:5: (otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}' )?
            int alt71=2;
            int LA71_0 = input.LA(1);

            if ( (LA71_0==46) ) {
                alt71=1;
            }
            switch (alt71) {
                case 1 :
                    // InternalFCL.g:3172:6: otherlv_16= 'timeReference' otherlv_17= '{' ( (lv_timeReference_18_0= ruleTimeReference ) ) otherlv_19= '}'
                    {
                    otherlv_16=(Token)match(input,46,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_16, grammarAccess.getUnityFunctionAccess().getTimeReferenceKeyword_1_3_3_0());
                      					
                    }
                    otherlv_17=(Token)match(input,12,FOLLOW_48); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_17, grammarAccess.getUnityFunctionAccess().getLeftCurlyBracketKeyword_1_3_3_1());
                      					
                    }
                    // InternalFCL.g:3180:6: ( (lv_timeReference_18_0= ruleTimeReference ) )
                    // InternalFCL.g:3181:7: (lv_timeReference_18_0= ruleTimeReference )
                    {
                    // InternalFCL.g:3181:7: (lv_timeReference_18_0= ruleTimeReference )
                    // InternalFCL.g:3182:8: lv_timeReference_18_0= ruleTimeReference
                    {
                    if ( state.backtracking==0 ) {

                      								newCompositeNode(grammarAccess.getUnityFunctionAccess().getTimeReferenceTimeReferenceParserRuleCall_1_3_3_2_0());
                      							
                    }
                    pushFollow(FOLLOW_16);
                    lv_timeReference_18_0=ruleTimeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      								if (current==null) {
                      									current = createModelElementForParent(grammarAccess.getUnityFunctionRule());
                      								}
                      								set(
                      									current,
                      									"timeReference",
                      									lv_timeReference_18_0,
                      									"fr.inria.glose.fcl.FCL.TimeReference");
                      								afterParserOrEnumRuleCall();
                      							
                    }

                    }


                    }

                    otherlv_19=(Token)match(input,14,FOLLOW_16); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_19, grammarAccess.getUnityFunctionAccess().getRightCurlyBracketKeyword_1_3_3_3());
                      					
                    }

                    }
                    break;

            }


            }

            otherlv_20=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(otherlv_20, grammarAccess.getUnityFunctionAccess().getRightCurlyBracketKeyword_1_4());
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnityFunction"


    // $ANTLR start "entryRuleTimeReference"
    // InternalFCL.g:3214:1: entryRuleTimeReference returns [EObject current=null] : iv_ruleTimeReference= ruleTimeReference EOF ;
    public final EObject entryRuleTimeReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimeReference = null;


        try {
            // InternalFCL.g:3214:54: (iv_ruleTimeReference= ruleTimeReference EOF )
            // InternalFCL.g:3215:2: iv_ruleTimeReference= ruleTimeReference EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimeReferenceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTimeReference=ruleTimeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimeReference; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimeReference"


    // $ANTLR start "ruleTimeReference"
    // InternalFCL.g:3221:1: ruleTimeReference returns [EObject current=null] : ( ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) ) ) ) ) )+ {...}?) ) ) ;
    public final EObject ruleTimeReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_increment_4_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:3227:2: ( ( ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) ) ) ) ) )+ {...}?) ) ) )
            // InternalFCL.g:3228:2: ( ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) ) ) ) ) )+ {...}?) ) )
            {
            // InternalFCL.g:3228:2: ( ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) ) ) ) ) )+ {...}?) ) )
            // InternalFCL.g:3229:3: ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) ) ) ) ) )+ {...}?) )
            {
            // InternalFCL.g:3229:3: ( ( ( ({...}? => ( ({...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) ) ) ) ) )+ {...}?) )
            // InternalFCL.g:3230:4: ( ( ({...}? => ( ({...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) ) ) ) ) )+ {...}?)
            {
            getUnorderedGroupHelper().enter(grammarAccess.getTimeReferenceAccess().getUnorderedGroup());
            // InternalFCL.g:3233:4: ( ( ({...}? => ( ({...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) ) ) ) ) )+ {...}?)
            // InternalFCL.g:3234:5: ( ({...}? => ( ({...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) ) ) ) ) )+ {...}?
            {
            // InternalFCL.g:3234:5: ( ({...}? => ( ({...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) ) ) ) ) )+
            int cnt72=0;
            loop72:
            do {
                int alt72=3;
                int LA72_0 = input.LA(1);

                if ( LA72_0 == 53 && getUnorderedGroupHelper().canSelect(grammarAccess.getTimeReferenceAccess().getUnorderedGroup(), 0) ) {
                    alt72=1;
                }
                else if ( LA72_0 == 54 && getUnorderedGroupHelper().canSelect(grammarAccess.getTimeReferenceAccess().getUnorderedGroup(), 1) ) {
                    alt72=2;
                }


                switch (alt72) {
            	case 1 :
            	    // InternalFCL.g:3235:3: ({...}? => ( ({...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) ) ) ) )
            	    {
            	    // InternalFCL.g:3235:3: ({...}? => ( ({...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) ) ) ) )
            	    // InternalFCL.g:3236:4: {...}? => ( ({...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getTimeReferenceAccess().getUnorderedGroup(), 0) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleTimeReference", "getUnorderedGroupHelper().canSelect(grammarAccess.getTimeReferenceAccess().getUnorderedGroup(), 0)");
            	    }
            	    // InternalFCL.g:3236:107: ( ({...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) ) ) )
            	    // InternalFCL.g:3237:5: ({...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getTimeReferenceAccess().getUnorderedGroup(), 0);
            	    // InternalFCL.g:3240:8: ({...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) ) )
            	    // InternalFCL.g:3240:9: {...}? => (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleTimeReference", "true");
            	    }
            	    // InternalFCL.g:3240:18: (otherlv_1= 'observableVar' ( ( ruleQualifiedName ) ) )
            	    // InternalFCL.g:3240:19: otherlv_1= 'observableVar' ( ( ruleQualifiedName ) )
            	    {
            	    otherlv_1=(Token)match(input,53,FOLLOW_3); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      								newLeafNode(otherlv_1, grammarAccess.getTimeReferenceAccess().getObservableVarKeyword_0_0());
            	      							
            	    }
            	    // InternalFCL.g:3244:8: ( ( ruleQualifiedName ) )
            	    // InternalFCL.g:3245:9: ( ruleQualifiedName )
            	    {
            	    // InternalFCL.g:3245:9: ( ruleQualifiedName )
            	    // InternalFCL.g:3246:10: ruleQualifiedName
            	    {
            	    if ( state.backtracking==0 ) {

            	      										if (current==null) {
            	      											current = createModelElement(grammarAccess.getTimeReferenceRule());
            	      										}
            	      									
            	    }
            	    if ( state.backtracking==0 ) {

            	      										newCompositeNode(grammarAccess.getTimeReferenceAccess().getObservableVarFunctionVarDeclCrossReference_0_1_0());
            	      									
            	    }
            	    pushFollow(FOLLOW_63);
            	    ruleQualifiedName();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      										afterParserOrEnumRuleCall();
            	      									
            	    }

            	    }


            	    }


            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getTimeReferenceAccess().getUnorderedGroup());

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalFCL.g:3266:3: ({...}? => ( ({...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) ) ) ) )
            	    {
            	    // InternalFCL.g:3266:3: ({...}? => ( ({...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) ) ) ) )
            	    // InternalFCL.g:3267:4: {...}? => ( ({...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getTimeReferenceAccess().getUnorderedGroup(), 1) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleTimeReference", "getUnorderedGroupHelper().canSelect(grammarAccess.getTimeReferenceAccess().getUnorderedGroup(), 1)");
            	    }
            	    // InternalFCL.g:3267:107: ( ({...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) ) ) )
            	    // InternalFCL.g:3268:5: ({...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) ) )
            	    {
            	    getUnorderedGroupHelper().select(grammarAccess.getTimeReferenceAccess().getUnorderedGroup(), 1);
            	    // InternalFCL.g:3271:8: ({...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) ) )
            	    // InternalFCL.g:3271:9: {...}? => (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) )
            	    {
            	    if ( !((true)) ) {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        throw new FailedPredicateException(input, "ruleTimeReference", "true");
            	    }
            	    // InternalFCL.g:3271:18: (otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) ) )
            	    // InternalFCL.g:3271:19: otherlv_3= 'increment' ( (lv_increment_4_0= ruleExpression ) )
            	    {
            	    otherlv_3=(Token)match(input,54,FOLLOW_27); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      								newLeafNode(otherlv_3, grammarAccess.getTimeReferenceAccess().getIncrementKeyword_1_0());
            	      							
            	    }
            	    // InternalFCL.g:3275:8: ( (lv_increment_4_0= ruleExpression ) )
            	    // InternalFCL.g:3276:9: (lv_increment_4_0= ruleExpression )
            	    {
            	    // InternalFCL.g:3276:9: (lv_increment_4_0= ruleExpression )
            	    // InternalFCL.g:3277:10: lv_increment_4_0= ruleExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      										newCompositeNode(grammarAccess.getTimeReferenceAccess().getIncrementExpressionParserRuleCall_1_1_0());
            	      									
            	    }
            	    pushFollow(FOLLOW_63);
            	    lv_increment_4_0=ruleExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      										if (current==null) {
            	      											current = createModelElementForParent(grammarAccess.getTimeReferenceRule());
            	      										}
            	      										set(
            	      											current,
            	      											"increment",
            	      											lv_increment_4_0,
            	      											"fr.inria.glose.fcl.FCL.Expression");
            	      										afterParserOrEnumRuleCall();
            	      									
            	    }

            	    }


            	    }


            	    }


            	    }

            	    getUnorderedGroupHelper().returnFromSelection(grammarAccess.getTimeReferenceAccess().getUnorderedGroup());

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt72 >= 1 ) break loop72;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(72, input);
                        throw eee;
                }
                cnt72++;
            } while (true);

            if ( ! getUnorderedGroupHelper().canLeave(grammarAccess.getTimeReferenceAccess().getUnorderedGroup()) ) {
                if (state.backtracking>0) {state.failed=true; return current;}
                throw new FailedPredicateException(input, "ruleTimeReference", "getUnorderedGroupHelper().canLeave(grammarAccess.getTimeReferenceAccess().getUnorderedGroup())");
            }

            }


            }

            getUnorderedGroupHelper().leave(grammarAccess.getTimeReferenceAccess().getUnorderedGroup());

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimeReference"


    // $ANTLR start "entryRuleFunctionConnector"
    // InternalFCL.g:3311:1: entryRuleFunctionConnector returns [EObject current=null] : iv_ruleFunctionConnector= ruleFunctionConnector EOF ;
    public final EObject entryRuleFunctionConnector() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionConnector = null;


        try {
            // InternalFCL.g:3311:58: (iv_ruleFunctionConnector= ruleFunctionConnector EOF )
            // InternalFCL.g:3312:2: iv_ruleFunctionConnector= ruleFunctionConnector EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFunctionConnectorRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFunctionConnector=ruleFunctionConnector();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFunctionConnector; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionConnector"


    // $ANTLR start "ruleFunctionConnector"
    // InternalFCL.g:3318:1: ruleFunctionConnector returns [EObject current=null] : ( () otherlv_1= 'connect' ( ( ruleQualifiedName ) ) otherlv_3= '<->' ( ( ruleQualifiedName ) ) ( ( (lv_delayed_5_0= 'with' ) ) otherlv_6= 'delay' )? ) ;
    public final EObject ruleFunctionConnector() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_delayed_5_0=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalFCL.g:3324:2: ( ( () otherlv_1= 'connect' ( ( ruleQualifiedName ) ) otherlv_3= '<->' ( ( ruleQualifiedName ) ) ( ( (lv_delayed_5_0= 'with' ) ) otherlv_6= 'delay' )? ) )
            // InternalFCL.g:3325:2: ( () otherlv_1= 'connect' ( ( ruleQualifiedName ) ) otherlv_3= '<->' ( ( ruleQualifiedName ) ) ( ( (lv_delayed_5_0= 'with' ) ) otherlv_6= 'delay' )? )
            {
            // InternalFCL.g:3325:2: ( () otherlv_1= 'connect' ( ( ruleQualifiedName ) ) otherlv_3= '<->' ( ( ruleQualifiedName ) ) ( ( (lv_delayed_5_0= 'with' ) ) otherlv_6= 'delay' )? )
            // InternalFCL.g:3326:3: () otherlv_1= 'connect' ( ( ruleQualifiedName ) ) otherlv_3= '<->' ( ( ruleQualifiedName ) ) ( ( (lv_delayed_5_0= 'with' ) ) otherlv_6= 'delay' )?
            {
            // InternalFCL.g:3326:3: ()
            // InternalFCL.g:3327:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getFunctionConnectorAccess().getFunctionConnectorAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,55,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getFunctionConnectorAccess().getConnectKeyword_1());
              		
            }
            // InternalFCL.g:3337:3: ( ( ruleQualifiedName ) )
            // InternalFCL.g:3338:4: ( ruleQualifiedName )
            {
            // InternalFCL.g:3338:4: ( ruleQualifiedName )
            // InternalFCL.g:3339:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getFunctionConnectorRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getFunctionConnectorAccess().getEmitterFunctionPortCrossReference_2_0());
              				
            }
            pushFollow(FOLLOW_39);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,41,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getFunctionConnectorAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_3());
              		
            }
            // InternalFCL.g:3357:3: ( ( ruleQualifiedName ) )
            // InternalFCL.g:3358:4: ( ruleQualifiedName )
            {
            // InternalFCL.g:3358:4: ( ruleQualifiedName )
            // InternalFCL.g:3359:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getFunctionConnectorRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getFunctionConnectorAccess().getReceiverFunctionPortCrossReference_4_0());
              				
            }
            pushFollow(FOLLOW_64);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalFCL.g:3373:3: ( ( (lv_delayed_5_0= 'with' ) ) otherlv_6= 'delay' )?
            int alt73=2;
            int LA73_0 = input.LA(1);

            if ( (LA73_0==56) ) {
                alt73=1;
            }
            switch (alt73) {
                case 1 :
                    // InternalFCL.g:3374:4: ( (lv_delayed_5_0= 'with' ) ) otherlv_6= 'delay'
                    {
                    // InternalFCL.g:3374:4: ( (lv_delayed_5_0= 'with' ) )
                    // InternalFCL.g:3375:5: (lv_delayed_5_0= 'with' )
                    {
                    // InternalFCL.g:3375:5: (lv_delayed_5_0= 'with' )
                    // InternalFCL.g:3376:6: lv_delayed_5_0= 'with'
                    {
                    lv_delayed_5_0=(Token)match(input,56,FOLLOW_65); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_delayed_5_0, grammarAccess.getFunctionConnectorAccess().getDelayedWithKeyword_5_0_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getFunctionConnectorRule());
                      						}
                      						setWithLastConsumed(current, "delayed", true, "with");
                      					
                    }

                    }


                    }

                    otherlv_6=(Token)match(input,57,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getFunctionConnectorAccess().getDelayKeyword_5_1());
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionConnector"


    // $ANTLR start "entryRuleFunctionVarDecl"
    // InternalFCL.g:3397:1: entryRuleFunctionVarDecl returns [EObject current=null] : iv_ruleFunctionVarDecl= ruleFunctionVarDecl EOF ;
    public final EObject entryRuleFunctionVarDecl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionVarDecl = null;


        try {
            // InternalFCL.g:3397:56: (iv_ruleFunctionVarDecl= ruleFunctionVarDecl EOF )
            // InternalFCL.g:3398:2: iv_ruleFunctionVarDecl= ruleFunctionVarDecl EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFunctionVarDeclRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFunctionVarDecl=ruleFunctionVarDecl();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFunctionVarDecl; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionVarDecl"


    // $ANTLR start "ruleFunctionVarDecl"
    // InternalFCL.g:3404:1: ruleFunctionVarDecl returns [EObject current=null] : ( () otherlv_1= 'var' ( ( ruleQualifiedName ) ) ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= ':=' ( (lv_initialValue_5_0= ruleExpression ) ) )? ) ;
    public final EObject ruleFunctionVarDecl() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        EObject lv_initialValue_5_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:3410:2: ( ( () otherlv_1= 'var' ( ( ruleQualifiedName ) ) ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= ':=' ( (lv_initialValue_5_0= ruleExpression ) ) )? ) )
            // InternalFCL.g:3411:2: ( () otherlv_1= 'var' ( ( ruleQualifiedName ) ) ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= ':=' ( (lv_initialValue_5_0= ruleExpression ) ) )? )
            {
            // InternalFCL.g:3411:2: ( () otherlv_1= 'var' ( ( ruleQualifiedName ) ) ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= ':=' ( (lv_initialValue_5_0= ruleExpression ) ) )? )
            // InternalFCL.g:3412:3: () otherlv_1= 'var' ( ( ruleQualifiedName ) ) ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= ':=' ( (lv_initialValue_5_0= ruleExpression ) ) )?
            {
            // InternalFCL.g:3412:3: ()
            // InternalFCL.g:3413:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getFunctionVarDeclAccess().getFunctionVarDeclAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,58,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getFunctionVarDeclAccess().getVarKeyword_1());
              		
            }
            // InternalFCL.g:3423:3: ( ( ruleQualifiedName ) )
            // InternalFCL.g:3424:4: ( ruleQualifiedName )
            {
            // InternalFCL.g:3424:4: ( ruleQualifiedName )
            // InternalFCL.g:3425:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getFunctionVarDeclRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getFunctionVarDeclAccess().getTypeDataTypeCrossReference_2_0());
              				
            }
            pushFollow(FOLLOW_3);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalFCL.g:3439:3: ( (lv_name_3_0= RULE_ID ) )
            // InternalFCL.g:3440:4: (lv_name_3_0= RULE_ID )
            {
            // InternalFCL.g:3440:4: (lv_name_3_0= RULE_ID )
            // InternalFCL.g:3441:5: lv_name_3_0= RULE_ID
            {
            lv_name_3_0=(Token)match(input,RULE_ID,FOLLOW_66); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_3_0, grammarAccess.getFunctionVarDeclAccess().getNameIDTerminalRuleCall_3_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getFunctionVarDeclRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_3_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            // InternalFCL.g:3457:3: (otherlv_4= ':=' ( (lv_initialValue_5_0= ruleExpression ) ) )?
            int alt74=2;
            int LA74_0 = input.LA(1);

            if ( (LA74_0==59) ) {
                alt74=1;
            }
            switch (alt74) {
                case 1 :
                    // InternalFCL.g:3458:4: otherlv_4= ':=' ( (lv_initialValue_5_0= ruleExpression ) )
                    {
                    otherlv_4=(Token)match(input,59,FOLLOW_27); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getFunctionVarDeclAccess().getColonEqualsSignKeyword_4_0());
                      			
                    }
                    // InternalFCL.g:3462:4: ( (lv_initialValue_5_0= ruleExpression ) )
                    // InternalFCL.g:3463:5: (lv_initialValue_5_0= ruleExpression )
                    {
                    // InternalFCL.g:3463:5: (lv_initialValue_5_0= ruleExpression )
                    // InternalFCL.g:3464:6: lv_initialValue_5_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getFunctionVarDeclAccess().getInitialValueExpressionParserRuleCall_4_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_initialValue_5_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getFunctionVarDeclRule());
                      						}
                      						set(
                      							current,
                      							"initialValue",
                      							lv_initialValue_5_0,
                      							"fr.inria.glose.fcl.FCL.Expression");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionVarDecl"


    // $ANTLR start "entryRuleActionBlock"
    // InternalFCL.g:3486:1: entryRuleActionBlock returns [EObject current=null] : iv_ruleActionBlock= ruleActionBlock EOF ;
    public final EObject entryRuleActionBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleActionBlock = null;


        try {
            // InternalFCL.g:3486:52: (iv_ruleActionBlock= ruleActionBlock EOF )
            // InternalFCL.g:3487:2: iv_ruleActionBlock= ruleActionBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getActionBlockRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleActionBlock=ruleActionBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleActionBlock; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActionBlock"


    // $ANTLR start "ruleActionBlock"
    // InternalFCL.g:3493:1: ruleActionBlock returns [EObject current=null] : ( () otherlv_1= '{' ( ( (lv_actions_2_0= ruleAction ) ) ( (lv_actions_3_0= ruleAction ) )* )? otherlv_4= '}' ) ;
    public final EObject ruleActionBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_4=null;
        EObject lv_actions_2_0 = null;

        EObject lv_actions_3_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:3499:2: ( ( () otherlv_1= '{' ( ( (lv_actions_2_0= ruleAction ) ) ( (lv_actions_3_0= ruleAction ) )* )? otherlv_4= '}' ) )
            // InternalFCL.g:3500:2: ( () otherlv_1= '{' ( ( (lv_actions_2_0= ruleAction ) ) ( (lv_actions_3_0= ruleAction ) )* )? otherlv_4= '}' )
            {
            // InternalFCL.g:3500:2: ( () otherlv_1= '{' ( ( (lv_actions_2_0= ruleAction ) ) ( (lv_actions_3_0= ruleAction ) )* )? otherlv_4= '}' )
            // InternalFCL.g:3501:3: () otherlv_1= '{' ( ( (lv_actions_2_0= ruleAction ) ) ( (lv_actions_3_0= ruleAction ) )* )? otherlv_4= '}'
            {
            // InternalFCL.g:3501:3: ()
            // InternalFCL.g:3502:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getActionBlockAccess().getActionBlockAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,12,FOLLOW_67); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getActionBlockAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalFCL.g:3512:3: ( ( (lv_actions_2_0= ruleAction ) ) ( (lv_actions_3_0= ruleAction ) )* )?
            int alt76=2;
            int LA76_0 = input.LA(1);

            if ( (LA76_0==RULE_ID||LA76_0==12||LA76_0==58||LA76_0==60||LA76_0==63||LA76_0==73||LA76_0==75) ) {
                alt76=1;
            }
            switch (alt76) {
                case 1 :
                    // InternalFCL.g:3513:4: ( (lv_actions_2_0= ruleAction ) ) ( (lv_actions_3_0= ruleAction ) )*
                    {
                    // InternalFCL.g:3513:4: ( (lv_actions_2_0= ruleAction ) )
                    // InternalFCL.g:3514:5: (lv_actions_2_0= ruleAction )
                    {
                    // InternalFCL.g:3514:5: (lv_actions_2_0= ruleAction )
                    // InternalFCL.g:3515:6: lv_actions_2_0= ruleAction
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getActionBlockAccess().getActionsActionParserRuleCall_2_0_0());
                      					
                    }
                    pushFollow(FOLLOW_67);
                    lv_actions_2_0=ruleAction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getActionBlockRule());
                      						}
                      						add(
                      							current,
                      							"actions",
                      							lv_actions_2_0,
                      							"fr.inria.glose.fcl.FCL.Action");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalFCL.g:3532:4: ( (lv_actions_3_0= ruleAction ) )*
                    loop75:
                    do {
                        int alt75=2;
                        int LA75_0 = input.LA(1);

                        if ( (LA75_0==RULE_ID||LA75_0==12||LA75_0==58||LA75_0==60||LA75_0==63||LA75_0==73||LA75_0==75) ) {
                            alt75=1;
                        }


                        switch (alt75) {
                    	case 1 :
                    	    // InternalFCL.g:3533:5: (lv_actions_3_0= ruleAction )
                    	    {
                    	    // InternalFCL.g:3533:5: (lv_actions_3_0= ruleAction )
                    	    // InternalFCL.g:3534:6: lv_actions_3_0= ruleAction
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getActionBlockAccess().getActionsActionParserRuleCall_2_1_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_67);
                    	    lv_actions_3_0=ruleAction();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getActionBlockRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"actions",
                    	      							lv_actions_3_0,
                    	      							"fr.inria.glose.fcl.FCL.Action");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop75;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_4=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getActionBlockAccess().getRightCurlyBracketKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActionBlock"


    // $ANTLR start "entryRuleAssignment"
    // InternalFCL.g:3560:1: entryRuleAssignment returns [EObject current=null] : iv_ruleAssignment= ruleAssignment EOF ;
    public final EObject entryRuleAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignment = null;


        try {
            // InternalFCL.g:3560:51: (iv_ruleAssignment= ruleAssignment EOF )
            // InternalFCL.g:3561:2: iv_ruleAssignment= ruleAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAssignment=ruleAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignment; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignment"


    // $ANTLR start "ruleAssignment"
    // InternalFCL.g:3567:1: ruleAssignment returns [EObject current=null] : ( ( ( ruleQualifiedName ) ) otherlv_1= ':=' ( (lv_expression_2_0= ruleExpression ) ) ) ;
    public final EObject ruleAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_expression_2_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:3573:2: ( ( ( ( ruleQualifiedName ) ) otherlv_1= ':=' ( (lv_expression_2_0= ruleExpression ) ) ) )
            // InternalFCL.g:3574:2: ( ( ( ruleQualifiedName ) ) otherlv_1= ':=' ( (lv_expression_2_0= ruleExpression ) ) )
            {
            // InternalFCL.g:3574:2: ( ( ( ruleQualifiedName ) ) otherlv_1= ':=' ( (lv_expression_2_0= ruleExpression ) ) )
            // InternalFCL.g:3575:3: ( ( ruleQualifiedName ) ) otherlv_1= ':=' ( (lv_expression_2_0= ruleExpression ) )
            {
            // InternalFCL.g:3575:3: ( ( ruleQualifiedName ) )
            // InternalFCL.g:3576:4: ( ruleQualifiedName )
            {
            // InternalFCL.g:3576:4: ( ruleQualifiedName )
            // InternalFCL.g:3577:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getAssignmentRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getAssignmentAccess().getAssignableAssignableCrossReference_0_0());
              				
            }
            pushFollow(FOLLOW_68);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_1=(Token)match(input,59,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getAssignmentAccess().getColonEqualsSignKeyword_1());
              		
            }
            // InternalFCL.g:3595:3: ( (lv_expression_2_0= ruleExpression ) )
            // InternalFCL.g:3596:4: (lv_expression_2_0= ruleExpression )
            {
            // InternalFCL.g:3596:4: (lv_expression_2_0= ruleExpression )
            // InternalFCL.g:3597:5: lv_expression_2_0= ruleExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getAssignmentAccess().getExpressionExpressionParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_expression_2_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getAssignmentRule());
              					}
              					set(
              						current,
              						"expression",
              						lv_expression_2_0,
              						"fr.inria.glose.fcl.FCL.Expression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignment"


    // $ANTLR start "entryRuleDataStructPropertyAssignment"
    // InternalFCL.g:3618:1: entryRuleDataStructPropertyAssignment returns [EObject current=null] : iv_ruleDataStructPropertyAssignment= ruleDataStructPropertyAssignment EOF ;
    public final EObject entryRuleDataStructPropertyAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataStructPropertyAssignment = null;


        try {
            // InternalFCL.g:3618:69: (iv_ruleDataStructPropertyAssignment= ruleDataStructPropertyAssignment EOF )
            // InternalFCL.g:3619:2: iv_ruleDataStructPropertyAssignment= ruleDataStructPropertyAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDataStructPropertyAssignmentRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDataStructPropertyAssignment=ruleDataStructPropertyAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDataStructPropertyAssignment; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataStructPropertyAssignment"


    // $ANTLR start "ruleDataStructPropertyAssignment"
    // InternalFCL.g:3625:1: ruleDataStructPropertyAssignment returns [EObject current=null] : ( () ( ( ruleQualifiedName ) ) otherlv_2= '->' ( (lv_propertyName_3_0= RULE_ID ) ) (otherlv_4= '->' ( (lv_propertyName_5_0= RULE_ID ) ) )* otherlv_6= ':=' ( (lv_expression_7_0= ruleExpression ) ) ) ;
    public final EObject ruleDataStructPropertyAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_propertyName_3_0=null;
        Token otherlv_4=null;
        Token lv_propertyName_5_0=null;
        Token otherlv_6=null;
        EObject lv_expression_7_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:3631:2: ( ( () ( ( ruleQualifiedName ) ) otherlv_2= '->' ( (lv_propertyName_3_0= RULE_ID ) ) (otherlv_4= '->' ( (lv_propertyName_5_0= RULE_ID ) ) )* otherlv_6= ':=' ( (lv_expression_7_0= ruleExpression ) ) ) )
            // InternalFCL.g:3632:2: ( () ( ( ruleQualifiedName ) ) otherlv_2= '->' ( (lv_propertyName_3_0= RULE_ID ) ) (otherlv_4= '->' ( (lv_propertyName_5_0= RULE_ID ) ) )* otherlv_6= ':=' ( (lv_expression_7_0= ruleExpression ) ) )
            {
            // InternalFCL.g:3632:2: ( () ( ( ruleQualifiedName ) ) otherlv_2= '->' ( (lv_propertyName_3_0= RULE_ID ) ) (otherlv_4= '->' ( (lv_propertyName_5_0= RULE_ID ) ) )* otherlv_6= ':=' ( (lv_expression_7_0= ruleExpression ) ) )
            // InternalFCL.g:3633:3: () ( ( ruleQualifiedName ) ) otherlv_2= '->' ( (lv_propertyName_3_0= RULE_ID ) ) (otherlv_4= '->' ( (lv_propertyName_5_0= RULE_ID ) ) )* otherlv_6= ':=' ( (lv_expression_7_0= ruleExpression ) )
            {
            // InternalFCL.g:3633:3: ()
            // InternalFCL.g:3634:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getDataStructPropertyAssignmentAccess().getDataStructPropertyAssignmentAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:3640:3: ( ( ruleQualifiedName ) )
            // InternalFCL.g:3641:4: ( ruleQualifiedName )
            {
            // InternalFCL.g:3641:4: ( ruleQualifiedName )
            // InternalFCL.g:3642:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getDataStructPropertyAssignmentRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getDataStructPropertyAssignmentAccess().getAssignableAssignableCrossReference_1_0());
              				
            }
            pushFollow(FOLLOW_25);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,28,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getDataStructPropertyAssignmentAccess().getHyphenMinusGreaterThanSignKeyword_2());
              		
            }
            // InternalFCL.g:3660:3: ( (lv_propertyName_3_0= RULE_ID ) )
            // InternalFCL.g:3661:4: (lv_propertyName_3_0= RULE_ID )
            {
            // InternalFCL.g:3661:4: (lv_propertyName_3_0= RULE_ID )
            // InternalFCL.g:3662:5: lv_propertyName_3_0= RULE_ID
            {
            lv_propertyName_3_0=(Token)match(input,RULE_ID,FOLLOW_69); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_propertyName_3_0, grammarAccess.getDataStructPropertyAssignmentAccess().getPropertyNameIDTerminalRuleCall_3_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getDataStructPropertyAssignmentRule());
              					}
              					addWithLastConsumed(
              						current,
              						"propertyName",
              						lv_propertyName_3_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            // InternalFCL.g:3678:3: (otherlv_4= '->' ( (lv_propertyName_5_0= RULE_ID ) ) )*
            loop77:
            do {
                int alt77=2;
                int LA77_0 = input.LA(1);

                if ( (LA77_0==28) ) {
                    alt77=1;
                }


                switch (alt77) {
            	case 1 :
            	    // InternalFCL.g:3679:4: otherlv_4= '->' ( (lv_propertyName_5_0= RULE_ID ) )
            	    {
            	    otherlv_4=(Token)match(input,28,FOLLOW_3); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_4, grammarAccess.getDataStructPropertyAssignmentAccess().getHyphenMinusGreaterThanSignKeyword_4_0());
            	      			
            	    }
            	    // InternalFCL.g:3683:4: ( (lv_propertyName_5_0= RULE_ID ) )
            	    // InternalFCL.g:3684:5: (lv_propertyName_5_0= RULE_ID )
            	    {
            	    // InternalFCL.g:3684:5: (lv_propertyName_5_0= RULE_ID )
            	    // InternalFCL.g:3685:6: lv_propertyName_5_0= RULE_ID
            	    {
            	    lv_propertyName_5_0=(Token)match(input,RULE_ID,FOLLOW_69); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						newLeafNode(lv_propertyName_5_0, grammarAccess.getDataStructPropertyAssignmentAccess().getPropertyNameIDTerminalRuleCall_4_1_0());
            	      					
            	    }
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElement(grammarAccess.getDataStructPropertyAssignmentRule());
            	      						}
            	      						addWithLastConsumed(
            	      							current,
            	      							"propertyName",
            	      							lv_propertyName_5_0,
            	      							"org.eclipse.xtext.common.Terminals.ID");
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop77;
                }
            } while (true);

            otherlv_6=(Token)match(input,59,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getDataStructPropertyAssignmentAccess().getColonEqualsSignKeyword_5());
              		
            }
            // InternalFCL.g:3706:3: ( (lv_expression_7_0= ruleExpression ) )
            // InternalFCL.g:3707:4: (lv_expression_7_0= ruleExpression )
            {
            // InternalFCL.g:3707:4: (lv_expression_7_0= ruleExpression )
            // InternalFCL.g:3708:5: lv_expression_7_0= ruleExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getDataStructPropertyAssignmentAccess().getExpressionExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_expression_7_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getDataStructPropertyAssignmentRule());
              					}
              					set(
              						current,
              						"expression",
              						lv_expression_7_0,
              						"fr.inria.glose.fcl.FCL.Expression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataStructPropertyAssignment"


    // $ANTLR start "entryRuleDataStructPropertyInternalAssignment"
    // InternalFCL.g:3729:1: entryRuleDataStructPropertyInternalAssignment returns [EObject current=null] : iv_ruleDataStructPropertyInternalAssignment= ruleDataStructPropertyInternalAssignment EOF ;
    public final EObject entryRuleDataStructPropertyInternalAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataStructPropertyInternalAssignment = null;


        try {
            // InternalFCL.g:3729:77: (iv_ruleDataStructPropertyInternalAssignment= ruleDataStructPropertyInternalAssignment EOF )
            // InternalFCL.g:3730:2: iv_ruleDataStructPropertyInternalAssignment= ruleDataStructPropertyInternalAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDataStructPropertyInternalAssignmentRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDataStructPropertyInternalAssignment=ruleDataStructPropertyInternalAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDataStructPropertyInternalAssignment; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataStructPropertyInternalAssignment"


    // $ANTLR start "ruleDataStructPropertyInternalAssignment"
    // InternalFCL.g:3736:1: ruleDataStructPropertyInternalAssignment returns [EObject current=null] : ( () ( (lv_propertyName_1_0= RULE_ID ) ) otherlv_2= ':=' ( (lv_expression_3_0= ruleExpression ) ) ) ;
    public final EObject ruleDataStructPropertyInternalAssignment() throws RecognitionException {
        EObject current = null;

        Token lv_propertyName_1_0=null;
        Token otherlv_2=null;
        EObject lv_expression_3_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:3742:2: ( ( () ( (lv_propertyName_1_0= RULE_ID ) ) otherlv_2= ':=' ( (lv_expression_3_0= ruleExpression ) ) ) )
            // InternalFCL.g:3743:2: ( () ( (lv_propertyName_1_0= RULE_ID ) ) otherlv_2= ':=' ( (lv_expression_3_0= ruleExpression ) ) )
            {
            // InternalFCL.g:3743:2: ( () ( (lv_propertyName_1_0= RULE_ID ) ) otherlv_2= ':=' ( (lv_expression_3_0= ruleExpression ) ) )
            // InternalFCL.g:3744:3: () ( (lv_propertyName_1_0= RULE_ID ) ) otherlv_2= ':=' ( (lv_expression_3_0= ruleExpression ) )
            {
            // InternalFCL.g:3744:3: ()
            // InternalFCL.g:3745:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getDataStructPropertyInternalAssignmentAccess().getDataStructPropertyAssignmentAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:3751:3: ( (lv_propertyName_1_0= RULE_ID ) )
            // InternalFCL.g:3752:4: (lv_propertyName_1_0= RULE_ID )
            {
            // InternalFCL.g:3752:4: (lv_propertyName_1_0= RULE_ID )
            // InternalFCL.g:3753:5: lv_propertyName_1_0= RULE_ID
            {
            lv_propertyName_1_0=(Token)match(input,RULE_ID,FOLLOW_68); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_propertyName_1_0, grammarAccess.getDataStructPropertyInternalAssignmentAccess().getPropertyNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getDataStructPropertyInternalAssignmentRule());
              					}
              					addWithLastConsumed(
              						current,
              						"propertyName",
              						lv_propertyName_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,59,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getDataStructPropertyInternalAssignmentAccess().getColonEqualsSignKeyword_2());
              		
            }
            // InternalFCL.g:3773:3: ( (lv_expression_3_0= ruleExpression ) )
            // InternalFCL.g:3774:4: (lv_expression_3_0= ruleExpression )
            {
            // InternalFCL.g:3774:4: (lv_expression_3_0= ruleExpression )
            // InternalFCL.g:3775:5: lv_expression_3_0= ruleExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getDataStructPropertyInternalAssignmentAccess().getExpressionExpressionParserRuleCall_3_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_expression_3_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getDataStructPropertyInternalAssignmentRule());
              					}
              					set(
              						current,
              						"expression",
              						lv_expression_3_0,
              						"fr.inria.glose.fcl.FCL.Expression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataStructPropertyInternalAssignment"


    // $ANTLR start "entryRuleIfAction"
    // InternalFCL.g:3796:1: entryRuleIfAction returns [EObject current=null] : iv_ruleIfAction= ruleIfAction EOF ;
    public final EObject entryRuleIfAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIfAction = null;


        try {
            // InternalFCL.g:3796:49: (iv_ruleIfAction= ruleIfAction EOF )
            // InternalFCL.g:3797:2: iv_ruleIfAction= ruleIfAction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIfActionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleIfAction=ruleIfAction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIfAction; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIfAction"


    // $ANTLR start "ruleIfAction"
    // InternalFCL.g:3803:1: ruleIfAction returns [EObject current=null] : (otherlv_0= 'if' ( (lv_condition_1_0= ruleExpression ) ) otherlv_2= 'then' ( (lv_thenAction_3_0= ruleAction ) ) ( ( ( 'else' )=>otherlv_4= 'else' ) ( (lv_elseAction_5_0= ruleAction ) ) )? ) ;
    public final EObject ruleIfAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_condition_1_0 = null;

        EObject lv_thenAction_3_0 = null;

        EObject lv_elseAction_5_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:3809:2: ( (otherlv_0= 'if' ( (lv_condition_1_0= ruleExpression ) ) otherlv_2= 'then' ( (lv_thenAction_3_0= ruleAction ) ) ( ( ( 'else' )=>otherlv_4= 'else' ) ( (lv_elseAction_5_0= ruleAction ) ) )? ) )
            // InternalFCL.g:3810:2: (otherlv_0= 'if' ( (lv_condition_1_0= ruleExpression ) ) otherlv_2= 'then' ( (lv_thenAction_3_0= ruleAction ) ) ( ( ( 'else' )=>otherlv_4= 'else' ) ( (lv_elseAction_5_0= ruleAction ) ) )? )
            {
            // InternalFCL.g:3810:2: (otherlv_0= 'if' ( (lv_condition_1_0= ruleExpression ) ) otherlv_2= 'then' ( (lv_thenAction_3_0= ruleAction ) ) ( ( ( 'else' )=>otherlv_4= 'else' ) ( (lv_elseAction_5_0= ruleAction ) ) )? )
            // InternalFCL.g:3811:3: otherlv_0= 'if' ( (lv_condition_1_0= ruleExpression ) ) otherlv_2= 'then' ( (lv_thenAction_3_0= ruleAction ) ) ( ( ( 'else' )=>otherlv_4= 'else' ) ( (lv_elseAction_5_0= ruleAction ) ) )?
            {
            otherlv_0=(Token)match(input,60,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getIfActionAccess().getIfKeyword_0());
              		
            }
            // InternalFCL.g:3815:3: ( (lv_condition_1_0= ruleExpression ) )
            // InternalFCL.g:3816:4: (lv_condition_1_0= ruleExpression )
            {
            // InternalFCL.g:3816:4: (lv_condition_1_0= ruleExpression )
            // InternalFCL.g:3817:5: lv_condition_1_0= ruleExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIfActionAccess().getConditionExpressionParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_70);
            lv_condition_1_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIfActionRule());
              					}
              					set(
              						current,
              						"condition",
              						lv_condition_1_0,
              						"fr.inria.glose.fcl.FCL.Expression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,61,FOLLOW_19); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getIfActionAccess().getThenKeyword_2());
              		
            }
            // InternalFCL.g:3838:3: ( (lv_thenAction_3_0= ruleAction ) )
            // InternalFCL.g:3839:4: (lv_thenAction_3_0= ruleAction )
            {
            // InternalFCL.g:3839:4: (lv_thenAction_3_0= ruleAction )
            // InternalFCL.g:3840:5: lv_thenAction_3_0= ruleAction
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIfActionAccess().getThenActionActionParserRuleCall_3_0());
              				
            }
            pushFollow(FOLLOW_71);
            lv_thenAction_3_0=ruleAction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIfActionRule());
              					}
              					set(
              						current,
              						"thenAction",
              						lv_thenAction_3_0,
              						"fr.inria.glose.fcl.FCL.Action");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalFCL.g:3857:3: ( ( ( 'else' )=>otherlv_4= 'else' ) ( (lv_elseAction_5_0= ruleAction ) ) )?
            int alt78=2;
            int LA78_0 = input.LA(1);

            if ( (LA78_0==62) ) {
                int LA78_1 = input.LA(2);

                if ( (synpred1_InternalFCL()) ) {
                    alt78=1;
                }
            }
            switch (alt78) {
                case 1 :
                    // InternalFCL.g:3858:4: ( ( 'else' )=>otherlv_4= 'else' ) ( (lv_elseAction_5_0= ruleAction ) )
                    {
                    // InternalFCL.g:3858:4: ( ( 'else' )=>otherlv_4= 'else' )
                    // InternalFCL.g:3859:5: ( 'else' )=>otherlv_4= 'else'
                    {
                    otherlv_4=(Token)match(input,62,FOLLOW_19); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_4, grammarAccess.getIfActionAccess().getElseKeyword_4_0());
                      				
                    }

                    }

                    // InternalFCL.g:3865:4: ( (lv_elseAction_5_0= ruleAction ) )
                    // InternalFCL.g:3866:5: (lv_elseAction_5_0= ruleAction )
                    {
                    // InternalFCL.g:3866:5: (lv_elseAction_5_0= ruleAction )
                    // InternalFCL.g:3867:6: lv_elseAction_5_0= ruleAction
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getIfActionAccess().getElseActionActionParserRuleCall_4_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_elseAction_5_0=ruleAction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getIfActionRule());
                      						}
                      						set(
                      							current,
                      							"elseAction",
                      							lv_elseAction_5_0,
                      							"fr.inria.glose.fcl.FCL.Action");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIfAction"


    // $ANTLR start "entryRuleWhileAction"
    // InternalFCL.g:3889:1: entryRuleWhileAction returns [EObject current=null] : iv_ruleWhileAction= ruleWhileAction EOF ;
    public final EObject entryRuleWhileAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWhileAction = null;


        try {
            // InternalFCL.g:3889:52: (iv_ruleWhileAction= ruleWhileAction EOF )
            // InternalFCL.g:3890:2: iv_ruleWhileAction= ruleWhileAction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getWhileActionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleWhileAction=ruleWhileAction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleWhileAction; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWhileAction"


    // $ANTLR start "ruleWhileAction"
    // InternalFCL.g:3896:1: ruleWhileAction returns [EObject current=null] : (otherlv_0= 'while' ( (lv_condition_1_0= ruleExpression ) ) ( (lv_action_2_0= ruleAction ) ) ) ;
    public final EObject ruleWhileAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_condition_1_0 = null;

        EObject lv_action_2_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:3902:2: ( (otherlv_0= 'while' ( (lv_condition_1_0= ruleExpression ) ) ( (lv_action_2_0= ruleAction ) ) ) )
            // InternalFCL.g:3903:2: (otherlv_0= 'while' ( (lv_condition_1_0= ruleExpression ) ) ( (lv_action_2_0= ruleAction ) ) )
            {
            // InternalFCL.g:3903:2: (otherlv_0= 'while' ( (lv_condition_1_0= ruleExpression ) ) ( (lv_action_2_0= ruleAction ) ) )
            // InternalFCL.g:3904:3: otherlv_0= 'while' ( (lv_condition_1_0= ruleExpression ) ) ( (lv_action_2_0= ruleAction ) )
            {
            otherlv_0=(Token)match(input,63,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getWhileActionAccess().getWhileKeyword_0());
              		
            }
            // InternalFCL.g:3908:3: ( (lv_condition_1_0= ruleExpression ) )
            // InternalFCL.g:3909:4: (lv_condition_1_0= ruleExpression )
            {
            // InternalFCL.g:3909:4: (lv_condition_1_0= ruleExpression )
            // InternalFCL.g:3910:5: lv_condition_1_0= ruleExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getWhileActionAccess().getConditionExpressionParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_19);
            lv_condition_1_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getWhileActionRule());
              					}
              					set(
              						current,
              						"condition",
              						lv_condition_1_0,
              						"fr.inria.glose.fcl.FCL.Expression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalFCL.g:3927:3: ( (lv_action_2_0= ruleAction ) )
            // InternalFCL.g:3928:4: (lv_action_2_0= ruleAction )
            {
            // InternalFCL.g:3928:4: (lv_action_2_0= ruleAction )
            // InternalFCL.g:3929:5: lv_action_2_0= ruleAction
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getWhileActionAccess().getActionActionParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_action_2_0=ruleAction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getWhileActionRule());
              					}
              					set(
              						current,
              						"action",
              						lv_action_2_0,
              						"fr.inria.glose.fcl.FCL.Action");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWhileAction"


    // $ANTLR start "entryRuleProcedureCall"
    // InternalFCL.g:3950:1: entryRuleProcedureCall returns [EObject current=null] : iv_ruleProcedureCall= ruleProcedureCall EOF ;
    public final EObject entryRuleProcedureCall() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProcedureCall = null;


        try {
            // InternalFCL.g:3950:54: (iv_ruleProcedureCall= ruleProcedureCall EOF )
            // InternalFCL.g:3951:2: iv_ruleProcedureCall= ruleProcedureCall EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getProcedureCallRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleProcedureCall=ruleProcedureCall();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleProcedureCall; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProcedureCall"


    // $ANTLR start "ruleProcedureCall"
    // InternalFCL.g:3957:1: ruleProcedureCall returns [EObject current=null] : ( () ( ( ruleQualifiedName ) ) otherlv_2= '(' ( ( (lv_procedureCallArguments_3_0= ruleExpression ) ) (otherlv_4= ',' ( (lv_procedureCallArguments_5_0= ruleExpression ) ) )* )? otherlv_6= ')' ) ;
    public final EObject ruleProcedureCall() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_procedureCallArguments_3_0 = null;

        EObject lv_procedureCallArguments_5_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:3963:2: ( ( () ( ( ruleQualifiedName ) ) otherlv_2= '(' ( ( (lv_procedureCallArguments_3_0= ruleExpression ) ) (otherlv_4= ',' ( (lv_procedureCallArguments_5_0= ruleExpression ) ) )* )? otherlv_6= ')' ) )
            // InternalFCL.g:3964:2: ( () ( ( ruleQualifiedName ) ) otherlv_2= '(' ( ( (lv_procedureCallArguments_3_0= ruleExpression ) ) (otherlv_4= ',' ( (lv_procedureCallArguments_5_0= ruleExpression ) ) )* )? otherlv_6= ')' )
            {
            // InternalFCL.g:3964:2: ( () ( ( ruleQualifiedName ) ) otherlv_2= '(' ( ( (lv_procedureCallArguments_3_0= ruleExpression ) ) (otherlv_4= ',' ( (lv_procedureCallArguments_5_0= ruleExpression ) ) )* )? otherlv_6= ')' )
            // InternalFCL.g:3965:3: () ( ( ruleQualifiedName ) ) otherlv_2= '(' ( ( (lv_procedureCallArguments_3_0= ruleExpression ) ) (otherlv_4= ',' ( (lv_procedureCallArguments_5_0= ruleExpression ) ) )* )? otherlv_6= ')'
            {
            // InternalFCL.g:3965:3: ()
            // InternalFCL.g:3966:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getProcedureCallAccess().getProcedureCallAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:3972:3: ( ( ruleQualifiedName ) )
            // InternalFCL.g:3973:4: ( ruleQualifiedName )
            {
            // InternalFCL.g:3973:4: ( ruleQualifiedName )
            // InternalFCL.g:3974:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getProcedureCallRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getProcedureCallAccess().getProcedureProcedureCrossReference_1_0());
              				
            }
            pushFollow(FOLLOW_23);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,24,FOLLOW_72); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getProcedureCallAccess().getLeftParenthesisKeyword_2());
              		
            }
            // InternalFCL.g:3992:3: ( ( (lv_procedureCallArguments_3_0= ruleExpression ) ) (otherlv_4= ',' ( (lv_procedureCallArguments_5_0= ruleExpression ) ) )* )?
            int alt80=2;
            int LA80_0 = input.LA(1);

            if ( ((LA80_0>=RULE_ID && LA80_0<=RULE_INT)||LA80_0==24||LA80_0==69||LA80_0==72||LA80_0==74||(LA80_0>=88 && LA80_0<=90)||LA80_0==93||LA80_0==97) ) {
                alt80=1;
            }
            switch (alt80) {
                case 1 :
                    // InternalFCL.g:3993:4: ( (lv_procedureCallArguments_3_0= ruleExpression ) ) (otherlv_4= ',' ( (lv_procedureCallArguments_5_0= ruleExpression ) ) )*
                    {
                    // InternalFCL.g:3993:4: ( (lv_procedureCallArguments_3_0= ruleExpression ) )
                    // InternalFCL.g:3994:5: (lv_procedureCallArguments_3_0= ruleExpression )
                    {
                    // InternalFCL.g:3994:5: (lv_procedureCallArguments_3_0= ruleExpression )
                    // InternalFCL.g:3995:6: lv_procedureCallArguments_3_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getProcedureCallAccess().getProcedureCallArgumentsExpressionParserRuleCall_3_0_0());
                      					
                    }
                    pushFollow(FOLLOW_24);
                    lv_procedureCallArguments_3_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getProcedureCallRule());
                      						}
                      						add(
                      							current,
                      							"procedureCallArguments",
                      							lv_procedureCallArguments_3_0,
                      							"fr.inria.glose.fcl.FCL.Expression");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalFCL.g:4012:4: (otherlv_4= ',' ( (lv_procedureCallArguments_5_0= ruleExpression ) ) )*
                    loop79:
                    do {
                        int alt79=2;
                        int LA79_0 = input.LA(1);

                        if ( (LA79_0==25) ) {
                            alt79=1;
                        }


                        switch (alt79) {
                    	case 1 :
                    	    // InternalFCL.g:4013:5: otherlv_4= ',' ( (lv_procedureCallArguments_5_0= ruleExpression ) )
                    	    {
                    	    otherlv_4=(Token)match(input,25,FOLLOW_27); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_4, grammarAccess.getProcedureCallAccess().getCommaKeyword_3_1_0());
                    	      				
                    	    }
                    	    // InternalFCL.g:4017:5: ( (lv_procedureCallArguments_5_0= ruleExpression ) )
                    	    // InternalFCL.g:4018:6: (lv_procedureCallArguments_5_0= ruleExpression )
                    	    {
                    	    // InternalFCL.g:4018:6: (lv_procedureCallArguments_5_0= ruleExpression )
                    	    // InternalFCL.g:4019:7: lv_procedureCallArguments_5_0= ruleExpression
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getProcedureCallAccess().getProcedureCallArgumentsExpressionParserRuleCall_3_1_1_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_24);
                    	    lv_procedureCallArguments_5_0=ruleExpression();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getProcedureCallRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"procedureCallArguments",
                    	      								lv_procedureCallArguments_5_0,
                    	      								"fr.inria.glose.fcl.FCL.Expression");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop79;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_6=(Token)match(input,26,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getProcedureCallAccess().getRightParenthesisKeyword_4());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProcedureCall"


    // $ANTLR start "entryRuleProcedureParameter"
    // InternalFCL.g:4046:1: entryRuleProcedureParameter returns [EObject current=null] : iv_ruleProcedureParameter= ruleProcedureParameter EOF ;
    public final EObject entryRuleProcedureParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProcedureParameter = null;


        try {
            // InternalFCL.g:4046:59: (iv_ruleProcedureParameter= ruleProcedureParameter EOF )
            // InternalFCL.g:4047:2: iv_ruleProcedureParameter= ruleProcedureParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getProcedureParameterRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleProcedureParameter=ruleProcedureParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleProcedureParameter; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProcedureParameter"


    // $ANTLR start "ruleProcedureParameter"
    // InternalFCL.g:4053:1: ruleProcedureParameter returns [EObject current=null] : ( ( ( ruleQualifiedName ) ) ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleProcedureParameter() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalFCL.g:4059:2: ( ( ( ( ruleQualifiedName ) ) ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalFCL.g:4060:2: ( ( ( ruleQualifiedName ) ) ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalFCL.g:4060:2: ( ( ( ruleQualifiedName ) ) ( (lv_name_1_0= RULE_ID ) ) )
            // InternalFCL.g:4061:3: ( ( ruleQualifiedName ) ) ( (lv_name_1_0= RULE_ID ) )
            {
            // InternalFCL.g:4061:3: ( ( ruleQualifiedName ) )
            // InternalFCL.g:4062:4: ( ruleQualifiedName )
            {
            // InternalFCL.g:4062:4: ( ruleQualifiedName )
            // InternalFCL.g:4063:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getProcedureParameterRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getProcedureParameterAccess().getTypeDataTypeCrossReference_0_0());
              				
            }
            pushFollow(FOLLOW_3);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalFCL.g:4077:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalFCL.g:4078:4: (lv_name_1_0= RULE_ID )
            {
            // InternalFCL.g:4078:4: (lv_name_1_0= RULE_ID )
            // InternalFCL.g:4079:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getProcedureParameterAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getProcedureParameterRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProcedureParameter"


    // $ANTLR start "entryRuleVarDecl_Impl"
    // InternalFCL.g:4099:1: entryRuleVarDecl_Impl returns [EObject current=null] : iv_ruleVarDecl_Impl= ruleVarDecl_Impl EOF ;
    public final EObject entryRuleVarDecl_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVarDecl_Impl = null;


        try {
            // InternalFCL.g:4099:53: (iv_ruleVarDecl_Impl= ruleVarDecl_Impl EOF )
            // InternalFCL.g:4100:2: iv_ruleVarDecl_Impl= ruleVarDecl_Impl EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVarDecl_ImplRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleVarDecl_Impl=ruleVarDecl_Impl();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVarDecl_Impl; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVarDecl_Impl"


    // $ANTLR start "ruleVarDecl_Impl"
    // InternalFCL.g:4106:1: ruleVarDecl_Impl returns [EObject current=null] : ( () otherlv_1= 'var' ( ( ruleQualifiedName ) ) ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= ':=' ( (lv_initialValue_5_0= ruleExpression ) ) )? ) ;
    public final EObject ruleVarDecl_Impl() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        EObject lv_initialValue_5_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:4112:2: ( ( () otherlv_1= 'var' ( ( ruleQualifiedName ) ) ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= ':=' ( (lv_initialValue_5_0= ruleExpression ) ) )? ) )
            // InternalFCL.g:4113:2: ( () otherlv_1= 'var' ( ( ruleQualifiedName ) ) ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= ':=' ( (lv_initialValue_5_0= ruleExpression ) ) )? )
            {
            // InternalFCL.g:4113:2: ( () otherlv_1= 'var' ( ( ruleQualifiedName ) ) ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= ':=' ( (lv_initialValue_5_0= ruleExpression ) ) )? )
            // InternalFCL.g:4114:3: () otherlv_1= 'var' ( ( ruleQualifiedName ) ) ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= ':=' ( (lv_initialValue_5_0= ruleExpression ) ) )?
            {
            // InternalFCL.g:4114:3: ()
            // InternalFCL.g:4115:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getVarDecl_ImplAccess().getVarDeclAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,58,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getVarDecl_ImplAccess().getVarKeyword_1());
              		
            }
            // InternalFCL.g:4125:3: ( ( ruleQualifiedName ) )
            // InternalFCL.g:4126:4: ( ruleQualifiedName )
            {
            // InternalFCL.g:4126:4: ( ruleQualifiedName )
            // InternalFCL.g:4127:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getVarDecl_ImplRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getVarDecl_ImplAccess().getTypeDataTypeCrossReference_2_0());
              				
            }
            pushFollow(FOLLOW_3);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalFCL.g:4141:3: ( (lv_name_3_0= RULE_ID ) )
            // InternalFCL.g:4142:4: (lv_name_3_0= RULE_ID )
            {
            // InternalFCL.g:4142:4: (lv_name_3_0= RULE_ID )
            // InternalFCL.g:4143:5: lv_name_3_0= RULE_ID
            {
            lv_name_3_0=(Token)match(input,RULE_ID,FOLLOW_66); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_3_0, grammarAccess.getVarDecl_ImplAccess().getNameIDTerminalRuleCall_3_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getVarDecl_ImplRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_3_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            // InternalFCL.g:4159:3: (otherlv_4= ':=' ( (lv_initialValue_5_0= ruleExpression ) ) )?
            int alt81=2;
            int LA81_0 = input.LA(1);

            if ( (LA81_0==59) ) {
                alt81=1;
            }
            switch (alt81) {
                case 1 :
                    // InternalFCL.g:4160:4: otherlv_4= ':=' ( (lv_initialValue_5_0= ruleExpression ) )
                    {
                    otherlv_4=(Token)match(input,59,FOLLOW_27); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getVarDecl_ImplAccess().getColonEqualsSignKeyword_4_0());
                      			
                    }
                    // InternalFCL.g:4164:4: ( (lv_initialValue_5_0= ruleExpression ) )
                    // InternalFCL.g:4165:5: (lv_initialValue_5_0= ruleExpression )
                    {
                    // InternalFCL.g:4165:5: (lv_initialValue_5_0= ruleExpression )
                    // InternalFCL.g:4166:6: lv_initialValue_5_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getVarDecl_ImplAccess().getInitialValueExpressionParserRuleCall_4_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_initialValue_5_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getVarDecl_ImplRule());
                      						}
                      						set(
                      							current,
                      							"initialValue",
                      							lv_initialValue_5_0,
                      							"fr.inria.glose.fcl.FCL.Expression");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVarDecl_Impl"


    // $ANTLR start "entryRuleEvent"
    // InternalFCL.g:4188:1: entryRuleEvent returns [EObject current=null] : iv_ruleEvent= ruleEvent EOF ;
    public final EObject entryRuleEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEvent = null;


        try {
            // InternalFCL.g:4188:46: (iv_ruleEvent= ruleEvent EOF )
            // InternalFCL.g:4189:2: iv_ruleEvent= ruleEvent EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEventRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEvent=ruleEvent();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEvent; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEvent"


    // $ANTLR start "ruleEvent"
    // InternalFCL.g:4195:1: ruleEvent returns [EObject current=null] : this_ExternalEvent_0= ruleExternalEvent ;
    public final EObject ruleEvent() throws RecognitionException {
        EObject current = null;

        EObject this_ExternalEvent_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:4201:2: (this_ExternalEvent_0= ruleExternalEvent )
            // InternalFCL.g:4202:2: this_ExternalEvent_0= ruleExternalEvent
            {
            if ( state.backtracking==0 ) {

              		newCompositeNode(grammarAccess.getEventAccess().getExternalEventParserRuleCall());
              	
            }
            pushFollow(FOLLOW_2);
            this_ExternalEvent_0=ruleExternalEvent();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current = this_ExternalEvent_0;
              		afterParserOrEnumRuleCall();
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEvent"


    // $ANTLR start "entryRuleExternalEvent"
    // InternalFCL.g:4213:1: entryRuleExternalEvent returns [EObject current=null] : iv_ruleExternalEvent= ruleExternalEvent EOF ;
    public final EObject entryRuleExternalEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExternalEvent = null;


        try {
            // InternalFCL.g:4213:54: (iv_ruleExternalEvent= ruleExternalEvent EOF )
            // InternalFCL.g:4214:2: iv_ruleExternalEvent= ruleExternalEvent EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExternalEventRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleExternalEvent=ruleExternalEvent();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExternalEvent; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExternalEvent"


    // $ANTLR start "ruleExternalEvent"
    // InternalFCL.g:4220:1: ruleExternalEvent returns [EObject current=null] : ( () otherlv_1= 'external' ( (lv_name_2_0= RULE_ID ) ) ) ;
    public final EObject ruleExternalEvent() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;


        	enterRule();

        try {
            // InternalFCL.g:4226:2: ( ( () otherlv_1= 'external' ( (lv_name_2_0= RULE_ID ) ) ) )
            // InternalFCL.g:4227:2: ( () otherlv_1= 'external' ( (lv_name_2_0= RULE_ID ) ) )
            {
            // InternalFCL.g:4227:2: ( () otherlv_1= 'external' ( (lv_name_2_0= RULE_ID ) ) )
            // InternalFCL.g:4228:3: () otherlv_1= 'external' ( (lv_name_2_0= RULE_ID ) )
            {
            // InternalFCL.g:4228:3: ()
            // InternalFCL.g:4229:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getExternalEventAccess().getExternalEventAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,64,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getExternalEventAccess().getExternalKeyword_1());
              		
            }
            // InternalFCL.g:4239:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalFCL.g:4240:4: (lv_name_2_0= RULE_ID )
            {
            // InternalFCL.g:4240:4: (lv_name_2_0= RULE_ID )
            // InternalFCL.g:4241:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getExternalEventAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getExternalEventRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExternalEvent"


    // $ANTLR start "entryRuleFCLProcedure"
    // InternalFCL.g:4261:1: entryRuleFCLProcedure returns [EObject current=null] : iv_ruleFCLProcedure= ruleFCLProcedure EOF ;
    public final EObject entryRuleFCLProcedure() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFCLProcedure = null;


        try {
            // InternalFCL.g:4261:53: (iv_ruleFCLProcedure= ruleFCLProcedure EOF )
            // InternalFCL.g:4262:2: iv_ruleFCLProcedure= ruleFCLProcedure EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFCLProcedureRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFCLProcedure=ruleFCLProcedure();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFCLProcedure; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFCLProcedure"


    // $ANTLR start "ruleFCLProcedure"
    // InternalFCL.g:4268:1: ruleFCLProcedure returns [EObject current=null] : ( () otherlv_1= 'FCLProcedure' ( ( ruleQualifiedName ) )? ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( ( (lv_procedureParameters_5_0= ruleProcedureParameter ) ) (otherlv_6= ',' ( (lv_procedureParameters_7_0= ruleProcedureParameter ) ) )* )? otherlv_8= ')' ( (lv_action_9_0= ruleActionBlock ) ) ) ;
    public final EObject ruleFCLProcedure() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_procedureParameters_5_0 = null;

        EObject lv_procedureParameters_7_0 = null;

        EObject lv_action_9_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:4274:2: ( ( () otherlv_1= 'FCLProcedure' ( ( ruleQualifiedName ) )? ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( ( (lv_procedureParameters_5_0= ruleProcedureParameter ) ) (otherlv_6= ',' ( (lv_procedureParameters_7_0= ruleProcedureParameter ) ) )* )? otherlv_8= ')' ( (lv_action_9_0= ruleActionBlock ) ) ) )
            // InternalFCL.g:4275:2: ( () otherlv_1= 'FCLProcedure' ( ( ruleQualifiedName ) )? ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( ( (lv_procedureParameters_5_0= ruleProcedureParameter ) ) (otherlv_6= ',' ( (lv_procedureParameters_7_0= ruleProcedureParameter ) ) )* )? otherlv_8= ')' ( (lv_action_9_0= ruleActionBlock ) ) )
            {
            // InternalFCL.g:4275:2: ( () otherlv_1= 'FCLProcedure' ( ( ruleQualifiedName ) )? ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( ( (lv_procedureParameters_5_0= ruleProcedureParameter ) ) (otherlv_6= ',' ( (lv_procedureParameters_7_0= ruleProcedureParameter ) ) )* )? otherlv_8= ')' ( (lv_action_9_0= ruleActionBlock ) ) )
            // InternalFCL.g:4276:3: () otherlv_1= 'FCLProcedure' ( ( ruleQualifiedName ) )? ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( ( (lv_procedureParameters_5_0= ruleProcedureParameter ) ) (otherlv_6= ',' ( (lv_procedureParameters_7_0= ruleProcedureParameter ) ) )* )? otherlv_8= ')' ( (lv_action_9_0= ruleActionBlock ) )
            {
            // InternalFCL.g:4276:3: ()
            // InternalFCL.g:4277:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getFCLProcedureAccess().getFCLProcedureAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,65,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getFCLProcedureAccess().getFCLProcedureKeyword_1());
              		
            }
            // InternalFCL.g:4287:3: ( ( ruleQualifiedName ) )?
            int alt82=2;
            int LA82_0 = input.LA(1);

            if ( (LA82_0==RULE_ID) ) {
                int LA82_1 = input.LA(2);

                if ( (LA82_1==RULE_ID||LA82_1==88) ) {
                    alt82=1;
                }
            }
            switch (alt82) {
                case 1 :
                    // InternalFCL.g:4288:4: ( ruleQualifiedName )
                    {
                    // InternalFCL.g:4288:4: ( ruleQualifiedName )
                    // InternalFCL.g:4289:5: ruleQualifiedName
                    {
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getFCLProcedureRule());
                      					}
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getFCLProcedureAccess().getReturnTypeDataTypeCrossReference_2_0());
                      				
                    }
                    pushFollow(FOLLOW_3);
                    ruleQualifiedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }
                    break;

            }

            // InternalFCL.g:4303:3: ( (lv_name_3_0= RULE_ID ) )
            // InternalFCL.g:4304:4: (lv_name_3_0= RULE_ID )
            {
            // InternalFCL.g:4304:4: (lv_name_3_0= RULE_ID )
            // InternalFCL.g:4305:5: lv_name_3_0= RULE_ID
            {
            lv_name_3_0=(Token)match(input,RULE_ID,FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_3_0, grammarAccess.getFCLProcedureAccess().getNameIDTerminalRuleCall_3_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getFCLProcedureRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_3_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_4=(Token)match(input,24,FOLLOW_73); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getFCLProcedureAccess().getLeftParenthesisKeyword_4());
              		
            }
            // InternalFCL.g:4325:3: ( ( (lv_procedureParameters_5_0= ruleProcedureParameter ) ) (otherlv_6= ',' ( (lv_procedureParameters_7_0= ruleProcedureParameter ) ) )* )?
            int alt84=2;
            int LA84_0 = input.LA(1);

            if ( (LA84_0==RULE_ID) ) {
                alt84=1;
            }
            switch (alt84) {
                case 1 :
                    // InternalFCL.g:4326:4: ( (lv_procedureParameters_5_0= ruleProcedureParameter ) ) (otherlv_6= ',' ( (lv_procedureParameters_7_0= ruleProcedureParameter ) ) )*
                    {
                    // InternalFCL.g:4326:4: ( (lv_procedureParameters_5_0= ruleProcedureParameter ) )
                    // InternalFCL.g:4327:5: (lv_procedureParameters_5_0= ruleProcedureParameter )
                    {
                    // InternalFCL.g:4327:5: (lv_procedureParameters_5_0= ruleProcedureParameter )
                    // InternalFCL.g:4328:6: lv_procedureParameters_5_0= ruleProcedureParameter
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getFCLProcedureAccess().getProcedureParametersProcedureParameterParserRuleCall_5_0_0());
                      					
                    }
                    pushFollow(FOLLOW_24);
                    lv_procedureParameters_5_0=ruleProcedureParameter();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getFCLProcedureRule());
                      						}
                      						add(
                      							current,
                      							"procedureParameters",
                      							lv_procedureParameters_5_0,
                      							"fr.inria.glose.fcl.FCL.ProcedureParameter");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalFCL.g:4345:4: (otherlv_6= ',' ( (lv_procedureParameters_7_0= ruleProcedureParameter ) ) )*
                    loop83:
                    do {
                        int alt83=2;
                        int LA83_0 = input.LA(1);

                        if ( (LA83_0==25) ) {
                            alt83=1;
                        }


                        switch (alt83) {
                    	case 1 :
                    	    // InternalFCL.g:4346:5: otherlv_6= ',' ( (lv_procedureParameters_7_0= ruleProcedureParameter ) )
                    	    {
                    	    otherlv_6=(Token)match(input,25,FOLLOW_3); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_6, grammarAccess.getFCLProcedureAccess().getCommaKeyword_5_1_0());
                    	      				
                    	    }
                    	    // InternalFCL.g:4350:5: ( (lv_procedureParameters_7_0= ruleProcedureParameter ) )
                    	    // InternalFCL.g:4351:6: (lv_procedureParameters_7_0= ruleProcedureParameter )
                    	    {
                    	    // InternalFCL.g:4351:6: (lv_procedureParameters_7_0= ruleProcedureParameter )
                    	    // InternalFCL.g:4352:7: lv_procedureParameters_7_0= ruleProcedureParameter
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getFCLProcedureAccess().getProcedureParametersProcedureParameterParserRuleCall_5_1_1_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_24);
                    	    lv_procedureParameters_7_0=ruleProcedureParameter();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getFCLProcedureRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"procedureParameters",
                    	      								lv_procedureParameters_7_0,
                    	      								"fr.inria.glose.fcl.FCL.ProcedureParameter");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop83;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_8=(Token)match(input,26,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getFCLProcedureAccess().getRightParenthesisKeyword_6());
              		
            }
            // InternalFCL.g:4375:3: ( (lv_action_9_0= ruleActionBlock ) )
            // InternalFCL.g:4376:4: (lv_action_9_0= ruleActionBlock )
            {
            // InternalFCL.g:4376:4: (lv_action_9_0= ruleActionBlock )
            // InternalFCL.g:4377:5: lv_action_9_0= ruleActionBlock
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getFCLProcedureAccess().getActionActionBlockParserRuleCall_7_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_action_9_0=ruleActionBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getFCLProcedureRule());
              					}
              					set(
              						current,
              						"action",
              						lv_action_9_0,
              						"fr.inria.glose.fcl.FCL.ActionBlock");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFCLProcedure"


    // $ANTLR start "entryRuleJavaProcedure"
    // InternalFCL.g:4398:1: entryRuleJavaProcedure returns [EObject current=null] : iv_ruleJavaProcedure= ruleJavaProcedure EOF ;
    public final EObject entryRuleJavaProcedure() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJavaProcedure = null;


        try {
            // InternalFCL.g:4398:54: (iv_ruleJavaProcedure= ruleJavaProcedure EOF )
            // InternalFCL.g:4399:2: iv_ruleJavaProcedure= ruleJavaProcedure EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJavaProcedureRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleJavaProcedure=ruleJavaProcedure();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJavaProcedure; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJavaProcedure"


    // $ANTLR start "ruleJavaProcedure"
    // InternalFCL.g:4405:1: ruleJavaProcedure returns [EObject current=null] : ( () otherlv_1= 'JavaProcedure' ( (lv_static_2_0= 'static' ) )? ( ( ruleQualifiedName ) )? ( (lv_name_4_0= RULE_ID ) ) otherlv_5= '(' ( ( (lv_procedureParameters_6_0= ruleProcedureParameter ) ) (otherlv_7= ',' ( (lv_procedureParameters_8_0= ruleProcedureParameter ) ) )* )? otherlv_9= ')' otherlv_10= '{' (otherlv_11= 'methodFullQualifiedName' ( (lv_methodFullQualifiedName_12_0= ruleEString ) ) )? otherlv_13= '}' ) ;
    public final EObject ruleJavaProcedure() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_static_2_0=null;
        Token lv_name_4_0=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        EObject lv_procedureParameters_6_0 = null;

        EObject lv_procedureParameters_8_0 = null;

        AntlrDatatypeRuleToken lv_methodFullQualifiedName_12_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:4411:2: ( ( () otherlv_1= 'JavaProcedure' ( (lv_static_2_0= 'static' ) )? ( ( ruleQualifiedName ) )? ( (lv_name_4_0= RULE_ID ) ) otherlv_5= '(' ( ( (lv_procedureParameters_6_0= ruleProcedureParameter ) ) (otherlv_7= ',' ( (lv_procedureParameters_8_0= ruleProcedureParameter ) ) )* )? otherlv_9= ')' otherlv_10= '{' (otherlv_11= 'methodFullQualifiedName' ( (lv_methodFullQualifiedName_12_0= ruleEString ) ) )? otherlv_13= '}' ) )
            // InternalFCL.g:4412:2: ( () otherlv_1= 'JavaProcedure' ( (lv_static_2_0= 'static' ) )? ( ( ruleQualifiedName ) )? ( (lv_name_4_0= RULE_ID ) ) otherlv_5= '(' ( ( (lv_procedureParameters_6_0= ruleProcedureParameter ) ) (otherlv_7= ',' ( (lv_procedureParameters_8_0= ruleProcedureParameter ) ) )* )? otherlv_9= ')' otherlv_10= '{' (otherlv_11= 'methodFullQualifiedName' ( (lv_methodFullQualifiedName_12_0= ruleEString ) ) )? otherlv_13= '}' )
            {
            // InternalFCL.g:4412:2: ( () otherlv_1= 'JavaProcedure' ( (lv_static_2_0= 'static' ) )? ( ( ruleQualifiedName ) )? ( (lv_name_4_0= RULE_ID ) ) otherlv_5= '(' ( ( (lv_procedureParameters_6_0= ruleProcedureParameter ) ) (otherlv_7= ',' ( (lv_procedureParameters_8_0= ruleProcedureParameter ) ) )* )? otherlv_9= ')' otherlv_10= '{' (otherlv_11= 'methodFullQualifiedName' ( (lv_methodFullQualifiedName_12_0= ruleEString ) ) )? otherlv_13= '}' )
            // InternalFCL.g:4413:3: () otherlv_1= 'JavaProcedure' ( (lv_static_2_0= 'static' ) )? ( ( ruleQualifiedName ) )? ( (lv_name_4_0= RULE_ID ) ) otherlv_5= '(' ( ( (lv_procedureParameters_6_0= ruleProcedureParameter ) ) (otherlv_7= ',' ( (lv_procedureParameters_8_0= ruleProcedureParameter ) ) )* )? otherlv_9= ')' otherlv_10= '{' (otherlv_11= 'methodFullQualifiedName' ( (lv_methodFullQualifiedName_12_0= ruleEString ) ) )? otherlv_13= '}'
            {
            // InternalFCL.g:4413:3: ()
            // InternalFCL.g:4414:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getJavaProcedureAccess().getJavaProcedureAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,66,FOLLOW_74); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getJavaProcedureAccess().getJavaProcedureKeyword_1());
              		
            }
            // InternalFCL.g:4424:3: ( (lv_static_2_0= 'static' ) )?
            int alt85=2;
            int LA85_0 = input.LA(1);

            if ( (LA85_0==67) ) {
                alt85=1;
            }
            switch (alt85) {
                case 1 :
                    // InternalFCL.g:4425:4: (lv_static_2_0= 'static' )
                    {
                    // InternalFCL.g:4425:4: (lv_static_2_0= 'static' )
                    // InternalFCL.g:4426:5: lv_static_2_0= 'static'
                    {
                    lv_static_2_0=(Token)match(input,67,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_static_2_0, grammarAccess.getJavaProcedureAccess().getStaticStaticKeyword_2_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getJavaProcedureRule());
                      					}
                      					setWithLastConsumed(current, "static", true, "static");
                      				
                    }

                    }


                    }
                    break;

            }

            // InternalFCL.g:4438:3: ( ( ruleQualifiedName ) )?
            int alt86=2;
            int LA86_0 = input.LA(1);

            if ( (LA86_0==RULE_ID) ) {
                int LA86_1 = input.LA(2);

                if ( (LA86_1==RULE_ID||LA86_1==88) ) {
                    alt86=1;
                }
            }
            switch (alt86) {
                case 1 :
                    // InternalFCL.g:4439:4: ( ruleQualifiedName )
                    {
                    // InternalFCL.g:4439:4: ( ruleQualifiedName )
                    // InternalFCL.g:4440:5: ruleQualifiedName
                    {
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getJavaProcedureRule());
                      					}
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getJavaProcedureAccess().getReturnTypeDataTypeCrossReference_3_0());
                      				
                    }
                    pushFollow(FOLLOW_3);
                    ruleQualifiedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }
                    break;

            }

            // InternalFCL.g:4454:3: ( (lv_name_4_0= RULE_ID ) )
            // InternalFCL.g:4455:4: (lv_name_4_0= RULE_ID )
            {
            // InternalFCL.g:4455:4: (lv_name_4_0= RULE_ID )
            // InternalFCL.g:4456:5: lv_name_4_0= RULE_ID
            {
            lv_name_4_0=(Token)match(input,RULE_ID,FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_4_0, grammarAccess.getJavaProcedureAccess().getNameIDTerminalRuleCall_4_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getJavaProcedureRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_4_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_5=(Token)match(input,24,FOLLOW_73); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_5, grammarAccess.getJavaProcedureAccess().getLeftParenthesisKeyword_5());
              		
            }
            // InternalFCL.g:4476:3: ( ( (lv_procedureParameters_6_0= ruleProcedureParameter ) ) (otherlv_7= ',' ( (lv_procedureParameters_8_0= ruleProcedureParameter ) ) )* )?
            int alt88=2;
            int LA88_0 = input.LA(1);

            if ( (LA88_0==RULE_ID) ) {
                alt88=1;
            }
            switch (alt88) {
                case 1 :
                    // InternalFCL.g:4477:4: ( (lv_procedureParameters_6_0= ruleProcedureParameter ) ) (otherlv_7= ',' ( (lv_procedureParameters_8_0= ruleProcedureParameter ) ) )*
                    {
                    // InternalFCL.g:4477:4: ( (lv_procedureParameters_6_0= ruleProcedureParameter ) )
                    // InternalFCL.g:4478:5: (lv_procedureParameters_6_0= ruleProcedureParameter )
                    {
                    // InternalFCL.g:4478:5: (lv_procedureParameters_6_0= ruleProcedureParameter )
                    // InternalFCL.g:4479:6: lv_procedureParameters_6_0= ruleProcedureParameter
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getJavaProcedureAccess().getProcedureParametersProcedureParameterParserRuleCall_6_0_0());
                      					
                    }
                    pushFollow(FOLLOW_24);
                    lv_procedureParameters_6_0=ruleProcedureParameter();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getJavaProcedureRule());
                      						}
                      						add(
                      							current,
                      							"procedureParameters",
                      							lv_procedureParameters_6_0,
                      							"fr.inria.glose.fcl.FCL.ProcedureParameter");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalFCL.g:4496:4: (otherlv_7= ',' ( (lv_procedureParameters_8_0= ruleProcedureParameter ) ) )*
                    loop87:
                    do {
                        int alt87=2;
                        int LA87_0 = input.LA(1);

                        if ( (LA87_0==25) ) {
                            alt87=1;
                        }


                        switch (alt87) {
                    	case 1 :
                    	    // InternalFCL.g:4497:5: otherlv_7= ',' ( (lv_procedureParameters_8_0= ruleProcedureParameter ) )
                    	    {
                    	    otherlv_7=(Token)match(input,25,FOLLOW_3); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_7, grammarAccess.getJavaProcedureAccess().getCommaKeyword_6_1_0());
                    	      				
                    	    }
                    	    // InternalFCL.g:4501:5: ( (lv_procedureParameters_8_0= ruleProcedureParameter ) )
                    	    // InternalFCL.g:4502:6: (lv_procedureParameters_8_0= ruleProcedureParameter )
                    	    {
                    	    // InternalFCL.g:4502:6: (lv_procedureParameters_8_0= ruleProcedureParameter )
                    	    // InternalFCL.g:4503:7: lv_procedureParameters_8_0= ruleProcedureParameter
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getJavaProcedureAccess().getProcedureParametersProcedureParameterParserRuleCall_6_1_1_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_24);
                    	    lv_procedureParameters_8_0=ruleProcedureParameter();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getJavaProcedureRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"procedureParameters",
                    	      								lv_procedureParameters_8_0,
                    	      								"fr.inria.glose.fcl.FCL.ProcedureParameter");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop87;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_9=(Token)match(input,26,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_9, grammarAccess.getJavaProcedureAccess().getRightParenthesisKeyword_7());
              		
            }
            otherlv_10=(Token)match(input,12,FOLLOW_75); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_10, grammarAccess.getJavaProcedureAccess().getLeftCurlyBracketKeyword_8());
              		
            }
            // InternalFCL.g:4530:3: (otherlv_11= 'methodFullQualifiedName' ( (lv_methodFullQualifiedName_12_0= ruleEString ) ) )?
            int alt89=2;
            int LA89_0 = input.LA(1);

            if ( (LA89_0==68) ) {
                alt89=1;
            }
            switch (alt89) {
                case 1 :
                    // InternalFCL.g:4531:4: otherlv_11= 'methodFullQualifiedName' ( (lv_methodFullQualifiedName_12_0= ruleEString ) )
                    {
                    otherlv_11=(Token)match(input,68,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_11, grammarAccess.getJavaProcedureAccess().getMethodFullQualifiedNameKeyword_9_0());
                      			
                    }
                    // InternalFCL.g:4535:4: ( (lv_methodFullQualifiedName_12_0= ruleEString ) )
                    // InternalFCL.g:4536:5: (lv_methodFullQualifiedName_12_0= ruleEString )
                    {
                    // InternalFCL.g:4536:5: (lv_methodFullQualifiedName_12_0= ruleEString )
                    // InternalFCL.g:4537:6: lv_methodFullQualifiedName_12_0= ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getJavaProcedureAccess().getMethodFullQualifiedNameEStringParserRuleCall_9_1_0());
                      					
                    }
                    pushFollow(FOLLOW_16);
                    lv_methodFullQualifiedName_12_0=ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getJavaProcedureRule());
                      						}
                      						set(
                      							current,
                      							"methodFullQualifiedName",
                      							lv_methodFullQualifiedName_12_0,
                      							"fr.inria.glose.fcl.FCL.EString");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_13=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_13, grammarAccess.getJavaProcedureAccess().getRightCurlyBracketKeyword_10());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJavaProcedure"


    // $ANTLR start "entryRuleCast"
    // InternalFCL.g:4563:1: entryRuleCast returns [EObject current=null] : iv_ruleCast= ruleCast EOF ;
    public final EObject entryRuleCast() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCast = null;


        try {
            // InternalFCL.g:4563:45: (iv_ruleCast= ruleCast EOF )
            // InternalFCL.g:4564:2: iv_ruleCast= ruleCast EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCastRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleCast=ruleCast();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCast; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCast"


    // $ANTLR start "ruleCast"
    // InternalFCL.g:4570:1: ruleCast returns [EObject current=null] : (otherlv_0= 'Cast' otherlv_1= '{' (otherlv_2= 'resultType' ( ( ruleQualifiedName ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleExpression ) ) otherlv_6= '}' ) ;
    public final EObject ruleCast() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_operand_5_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:4576:2: ( (otherlv_0= 'Cast' otherlv_1= '{' (otherlv_2= 'resultType' ( ( ruleQualifiedName ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleExpression ) ) otherlv_6= '}' ) )
            // InternalFCL.g:4577:2: (otherlv_0= 'Cast' otherlv_1= '{' (otherlv_2= 'resultType' ( ( ruleQualifiedName ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleExpression ) ) otherlv_6= '}' )
            {
            // InternalFCL.g:4577:2: (otherlv_0= 'Cast' otherlv_1= '{' (otherlv_2= 'resultType' ( ( ruleQualifiedName ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleExpression ) ) otherlv_6= '}' )
            // InternalFCL.g:4578:3: otherlv_0= 'Cast' otherlv_1= '{' (otherlv_2= 'resultType' ( ( ruleQualifiedName ) ) )? otherlv_4= 'operand' ( (lv_operand_5_0= ruleExpression ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,69,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getCastAccess().getCastKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,12,FOLLOW_76); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getCastAccess().getLeftCurlyBracketKeyword_1());
              		
            }
            // InternalFCL.g:4586:3: (otherlv_2= 'resultType' ( ( ruleQualifiedName ) ) )?
            int alt90=2;
            int LA90_0 = input.LA(1);

            if ( (LA90_0==70) ) {
                alt90=1;
            }
            switch (alt90) {
                case 1 :
                    // InternalFCL.g:4587:4: otherlv_2= 'resultType' ( ( ruleQualifiedName ) )
                    {
                    otherlv_2=(Token)match(input,70,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getCastAccess().getResultTypeKeyword_2_0());
                      			
                    }
                    // InternalFCL.g:4591:4: ( ( ruleQualifiedName ) )
                    // InternalFCL.g:4592:5: ( ruleQualifiedName )
                    {
                    // InternalFCL.g:4592:5: ( ruleQualifiedName )
                    // InternalFCL.g:4593:6: ruleQualifiedName
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getCastRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getCastAccess().getResultTypeDataTypeCrossReference_2_1_0());
                      					
                    }
                    pushFollow(FOLLOW_77);
                    ruleQualifiedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,71,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getCastAccess().getOperandKeyword_3());
              		
            }
            // InternalFCL.g:4612:3: ( (lv_operand_5_0= ruleExpression ) )
            // InternalFCL.g:4613:4: (lv_operand_5_0= ruleExpression )
            {
            // InternalFCL.g:4613:4: (lv_operand_5_0= ruleExpression )
            // InternalFCL.g:4614:5: lv_operand_5_0= ruleExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getCastAccess().getOperandExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FOLLOW_16);
            lv_operand_5_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getCastRule());
              					}
              					set(
              						current,
              						"operand",
              						lv_operand_5_0,
              						"fr.inria.glose.fcl.FCL.Expression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getCastAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCast"


    // $ANTLR start "entryRuleNewDataStruct"
    // InternalFCL.g:4639:1: entryRuleNewDataStruct returns [EObject current=null] : iv_ruleNewDataStruct= ruleNewDataStruct EOF ;
    public final EObject entryRuleNewDataStruct() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNewDataStruct = null;


        try {
            // InternalFCL.g:4639:54: (iv_ruleNewDataStruct= ruleNewDataStruct EOF )
            // InternalFCL.g:4640:2: iv_ruleNewDataStruct= ruleNewDataStruct EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNewDataStructRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleNewDataStruct=ruleNewDataStruct();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNewDataStruct; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNewDataStruct"


    // $ANTLR start "ruleNewDataStruct"
    // InternalFCL.g:4646:1: ruleNewDataStruct returns [EObject current=null] : ( () otherlv_1= 'new' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( ( (lv_propertyAssignments_4_0= ruleDataStructPropertyInternalAssignment ) ) (otherlv_5= ',' ( (lv_propertyAssignments_6_0= ruleDataStructPropertyInternalAssignment ) ) )* )? otherlv_7= '}' ) ;
    public final EObject ruleNewDataStruct() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_propertyAssignments_4_0 = null;

        EObject lv_propertyAssignments_6_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:4652:2: ( ( () otherlv_1= 'new' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( ( (lv_propertyAssignments_4_0= ruleDataStructPropertyInternalAssignment ) ) (otherlv_5= ',' ( (lv_propertyAssignments_6_0= ruleDataStructPropertyInternalAssignment ) ) )* )? otherlv_7= '}' ) )
            // InternalFCL.g:4653:2: ( () otherlv_1= 'new' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( ( (lv_propertyAssignments_4_0= ruleDataStructPropertyInternalAssignment ) ) (otherlv_5= ',' ( (lv_propertyAssignments_6_0= ruleDataStructPropertyInternalAssignment ) ) )* )? otherlv_7= '}' )
            {
            // InternalFCL.g:4653:2: ( () otherlv_1= 'new' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( ( (lv_propertyAssignments_4_0= ruleDataStructPropertyInternalAssignment ) ) (otherlv_5= ',' ( (lv_propertyAssignments_6_0= ruleDataStructPropertyInternalAssignment ) ) )* )? otherlv_7= '}' )
            // InternalFCL.g:4654:3: () otherlv_1= 'new' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( ( (lv_propertyAssignments_4_0= ruleDataStructPropertyInternalAssignment ) ) (otherlv_5= ',' ( (lv_propertyAssignments_6_0= ruleDataStructPropertyInternalAssignment ) ) )* )? otherlv_7= '}'
            {
            // InternalFCL.g:4654:3: ()
            // InternalFCL.g:4655:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getNewDataStructAccess().getNewDataStructAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,72,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getNewDataStructAccess().getNewKeyword_1());
              		
            }
            // InternalFCL.g:4665:3: ( ( ruleQualifiedName ) )
            // InternalFCL.g:4666:4: ( ruleQualifiedName )
            {
            // InternalFCL.g:4666:4: ( ruleQualifiedName )
            // InternalFCL.g:4667:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getNewDataStructRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getNewDataStructAccess().getResultTypeDataStructTypeCrossReference_2_0());
              				
            }
            pushFollow(FOLLOW_4);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_78); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getNewDataStructAccess().getLeftCurlyBracketKeyword_3());
              		
            }
            // InternalFCL.g:4685:3: ( ( (lv_propertyAssignments_4_0= ruleDataStructPropertyInternalAssignment ) ) (otherlv_5= ',' ( (lv_propertyAssignments_6_0= ruleDataStructPropertyInternalAssignment ) ) )* )?
            int alt92=2;
            int LA92_0 = input.LA(1);

            if ( (LA92_0==RULE_ID) ) {
                alt92=1;
            }
            switch (alt92) {
                case 1 :
                    // InternalFCL.g:4686:4: ( (lv_propertyAssignments_4_0= ruleDataStructPropertyInternalAssignment ) ) (otherlv_5= ',' ( (lv_propertyAssignments_6_0= ruleDataStructPropertyInternalAssignment ) ) )*
                    {
                    // InternalFCL.g:4686:4: ( (lv_propertyAssignments_4_0= ruleDataStructPropertyInternalAssignment ) )
                    // InternalFCL.g:4687:5: (lv_propertyAssignments_4_0= ruleDataStructPropertyInternalAssignment )
                    {
                    // InternalFCL.g:4687:5: (lv_propertyAssignments_4_0= ruleDataStructPropertyInternalAssignment )
                    // InternalFCL.g:4688:6: lv_propertyAssignments_4_0= ruleDataStructPropertyInternalAssignment
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getNewDataStructAccess().getPropertyAssignmentsDataStructPropertyInternalAssignmentParserRuleCall_4_0_0());
                      					
                    }
                    pushFollow(FOLLOW_79);
                    lv_propertyAssignments_4_0=ruleDataStructPropertyInternalAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getNewDataStructRule());
                      						}
                      						add(
                      							current,
                      							"propertyAssignments",
                      							lv_propertyAssignments_4_0,
                      							"fr.inria.glose.fcl.FCL.DataStructPropertyInternalAssignment");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalFCL.g:4705:4: (otherlv_5= ',' ( (lv_propertyAssignments_6_0= ruleDataStructPropertyInternalAssignment ) ) )*
                    loop91:
                    do {
                        int alt91=2;
                        int LA91_0 = input.LA(1);

                        if ( (LA91_0==25) ) {
                            alt91=1;
                        }


                        switch (alt91) {
                    	case 1 :
                    	    // InternalFCL.g:4706:5: otherlv_5= ',' ( (lv_propertyAssignments_6_0= ruleDataStructPropertyInternalAssignment ) )
                    	    {
                    	    otherlv_5=(Token)match(input,25,FOLLOW_3); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_5, grammarAccess.getNewDataStructAccess().getCommaKeyword_4_1_0());
                    	      				
                    	    }
                    	    // InternalFCL.g:4710:5: ( (lv_propertyAssignments_6_0= ruleDataStructPropertyInternalAssignment ) )
                    	    // InternalFCL.g:4711:6: (lv_propertyAssignments_6_0= ruleDataStructPropertyInternalAssignment )
                    	    {
                    	    // InternalFCL.g:4711:6: (lv_propertyAssignments_6_0= ruleDataStructPropertyInternalAssignment )
                    	    // InternalFCL.g:4712:7: lv_propertyAssignments_6_0= ruleDataStructPropertyInternalAssignment
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getNewDataStructAccess().getPropertyAssignmentsDataStructPropertyInternalAssignmentParserRuleCall_4_1_1_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_79);
                    	    lv_propertyAssignments_6_0=ruleDataStructPropertyInternalAssignment();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getNewDataStructRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"propertyAssignments",
                    	      								lv_propertyAssignments_6_0,
                    	      								"fr.inria.glose.fcl.FCL.DataStructPropertyInternalAssignment");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop91;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_7=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_7, grammarAccess.getNewDataStructAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNewDataStruct"


    // $ANTLR start "entryRuleLog"
    // InternalFCL.g:4739:1: entryRuleLog returns [EObject current=null] : iv_ruleLog= ruleLog EOF ;
    public final EObject entryRuleLog() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLog = null;


        try {
            // InternalFCL.g:4739:44: (iv_ruleLog= ruleLog EOF )
            // InternalFCL.g:4740:2: iv_ruleLog= ruleLog EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLogRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleLog=ruleLog();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLog; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLog"


    // $ANTLR start "ruleLog"
    // InternalFCL.g:4746:1: ruleLog returns [EObject current=null] : (otherlv_0= 'log' otherlv_1= '(' ( (lv_expression_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_expression_4_0= ruleExpression ) ) )* otherlv_5= ')' ) ;
    public final EObject ruleLog() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_expression_2_0 = null;

        EObject lv_expression_4_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:4752:2: ( (otherlv_0= 'log' otherlv_1= '(' ( (lv_expression_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_expression_4_0= ruleExpression ) ) )* otherlv_5= ')' ) )
            // InternalFCL.g:4753:2: (otherlv_0= 'log' otherlv_1= '(' ( (lv_expression_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_expression_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            {
            // InternalFCL.g:4753:2: (otherlv_0= 'log' otherlv_1= '(' ( (lv_expression_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_expression_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            // InternalFCL.g:4754:3: otherlv_0= 'log' otherlv_1= '(' ( (lv_expression_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_expression_4_0= ruleExpression ) ) )* otherlv_5= ')'
            {
            otherlv_0=(Token)match(input,73,FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getLogAccess().getLogKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,24,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getLogAccess().getLeftParenthesisKeyword_1());
              		
            }
            // InternalFCL.g:4762:3: ( (lv_expression_2_0= ruleExpression ) )
            // InternalFCL.g:4763:4: (lv_expression_2_0= ruleExpression )
            {
            // InternalFCL.g:4763:4: (lv_expression_2_0= ruleExpression )
            // InternalFCL.g:4764:5: lv_expression_2_0= ruleExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getLogAccess().getExpressionExpressionParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_24);
            lv_expression_2_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getLogRule());
              					}
              					add(
              						current,
              						"expression",
              						lv_expression_2_0,
              						"fr.inria.glose.fcl.FCL.Expression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalFCL.g:4781:3: (otherlv_3= ',' ( (lv_expression_4_0= ruleExpression ) ) )*
            loop93:
            do {
                int alt93=2;
                int LA93_0 = input.LA(1);

                if ( (LA93_0==25) ) {
                    alt93=1;
                }


                switch (alt93) {
            	case 1 :
            	    // InternalFCL.g:4782:4: otherlv_3= ',' ( (lv_expression_4_0= ruleExpression ) )
            	    {
            	    otherlv_3=(Token)match(input,25,FOLLOW_27); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_3, grammarAccess.getLogAccess().getCommaKeyword_3_0());
            	      			
            	    }
            	    // InternalFCL.g:4786:4: ( (lv_expression_4_0= ruleExpression ) )
            	    // InternalFCL.g:4787:5: (lv_expression_4_0= ruleExpression )
            	    {
            	    // InternalFCL.g:4787:5: (lv_expression_4_0= ruleExpression )
            	    // InternalFCL.g:4788:6: lv_expression_4_0= ruleExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getLogAccess().getExpressionExpressionParserRuleCall_3_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_24);
            	    lv_expression_4_0=ruleExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getLogRule());
            	      						}
            	      						add(
            	      							current,
            	      							"expression",
            	      							lv_expression_4_0,
            	      							"fr.inria.glose.fcl.FCL.Expression");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop93;
                }
            } while (true);

            otherlv_5=(Token)match(input,26,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_5, grammarAccess.getLogAccess().getRightParenthesisKeyword_4());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLog"


    // $ANTLR start "entryRuleCall"
    // InternalFCL.g:4814:1: entryRuleCall returns [EObject current=null] : iv_ruleCall= ruleCall EOF ;
    public final EObject entryRuleCall() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCall = null;


        try {
            // InternalFCL.g:4814:45: (iv_ruleCall= ruleCall EOF )
            // InternalFCL.g:4815:2: iv_ruleCall= ruleCall EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCallRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleCall=ruleCall();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCall; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCall"


    // $ANTLR start "ruleCall"
    // InternalFCL.g:4821:1: ruleCall returns [EObject current=null] : (this_CallConstant_0= ruleCallConstant | this_CallDeclarationReference_1= ruleCallDeclarationReference | this_CallPrevious_2= ruleCallPrevious | this_CallDataStructProperty_3= ruleCallDataStructProperty ) ;
    public final EObject ruleCall() throws RecognitionException {
        EObject current = null;

        EObject this_CallConstant_0 = null;

        EObject this_CallDeclarationReference_1 = null;

        EObject this_CallPrevious_2 = null;

        EObject this_CallDataStructProperty_3 = null;



        	enterRule();

        try {
            // InternalFCL.g:4827:2: ( (this_CallConstant_0= ruleCallConstant | this_CallDeclarationReference_1= ruleCallDeclarationReference | this_CallPrevious_2= ruleCallPrevious | this_CallDataStructProperty_3= ruleCallDataStructProperty ) )
            // InternalFCL.g:4828:2: (this_CallConstant_0= ruleCallConstant | this_CallDeclarationReference_1= ruleCallDeclarationReference | this_CallPrevious_2= ruleCallPrevious | this_CallDataStructProperty_3= ruleCallDataStructProperty )
            {
            // InternalFCL.g:4828:2: (this_CallConstant_0= ruleCallConstant | this_CallDeclarationReference_1= ruleCallDeclarationReference | this_CallPrevious_2= ruleCallPrevious | this_CallDataStructProperty_3= ruleCallDataStructProperty )
            int alt94=4;
            alt94 = dfa94.predict(input);
            switch (alt94) {
                case 1 :
                    // InternalFCL.g:4829:3: this_CallConstant_0= ruleCallConstant
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getCallAccess().getCallConstantParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_CallConstant_0=ruleCallConstant();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_CallConstant_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalFCL.g:4838:3: this_CallDeclarationReference_1= ruleCallDeclarationReference
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getCallAccess().getCallDeclarationReferenceParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_CallDeclarationReference_1=ruleCallDeclarationReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_CallDeclarationReference_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalFCL.g:4847:3: this_CallPrevious_2= ruleCallPrevious
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getCallAccess().getCallPreviousParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_CallPrevious_2=ruleCallPrevious();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_CallPrevious_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalFCL.g:4856:3: this_CallDataStructProperty_3= ruleCallDataStructProperty
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getCallAccess().getCallDataStructPropertyParserRuleCall_3());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_CallDataStructProperty_3=ruleCallDataStructProperty();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_CallDataStructProperty_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCall"


    // $ANTLR start "entryRuleCallConstant"
    // InternalFCL.g:4868:1: entryRuleCallConstant returns [EObject current=null] : iv_ruleCallConstant= ruleCallConstant EOF ;
    public final EObject entryRuleCallConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCallConstant = null;


        try {
            // InternalFCL.g:4868:53: (iv_ruleCallConstant= ruleCallConstant EOF )
            // InternalFCL.g:4869:2: iv_ruleCallConstant= ruleCallConstant EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCallConstantRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleCallConstant=ruleCallConstant();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCallConstant; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCallConstant"


    // $ANTLR start "ruleCallConstant"
    // InternalFCL.g:4875:1: ruleCallConstant returns [EObject current=null] : ( () ( ( (lv_value_1_1= ruleStringValue | lv_value_1_2= ruleIntegerValue | lv_value_1_3= ruleBooleanValue | lv_value_1_4= ruleFloatValue ) ) ) ) ;
    public final EObject ruleCallConstant() throws RecognitionException {
        EObject current = null;

        EObject lv_value_1_1 = null;

        EObject lv_value_1_2 = null;

        EObject lv_value_1_3 = null;

        EObject lv_value_1_4 = null;



        	enterRule();

        try {
            // InternalFCL.g:4881:2: ( ( () ( ( (lv_value_1_1= ruleStringValue | lv_value_1_2= ruleIntegerValue | lv_value_1_3= ruleBooleanValue | lv_value_1_4= ruleFloatValue ) ) ) ) )
            // InternalFCL.g:4882:2: ( () ( ( (lv_value_1_1= ruleStringValue | lv_value_1_2= ruleIntegerValue | lv_value_1_3= ruleBooleanValue | lv_value_1_4= ruleFloatValue ) ) ) )
            {
            // InternalFCL.g:4882:2: ( () ( ( (lv_value_1_1= ruleStringValue | lv_value_1_2= ruleIntegerValue | lv_value_1_3= ruleBooleanValue | lv_value_1_4= ruleFloatValue ) ) ) )
            // InternalFCL.g:4883:3: () ( ( (lv_value_1_1= ruleStringValue | lv_value_1_2= ruleIntegerValue | lv_value_1_3= ruleBooleanValue | lv_value_1_4= ruleFloatValue ) ) )
            {
            // InternalFCL.g:4883:3: ()
            // InternalFCL.g:4884:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getCallConstantAccess().getCallConstantAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:4890:3: ( ( (lv_value_1_1= ruleStringValue | lv_value_1_2= ruleIntegerValue | lv_value_1_3= ruleBooleanValue | lv_value_1_4= ruleFloatValue ) ) )
            // InternalFCL.g:4891:4: ( (lv_value_1_1= ruleStringValue | lv_value_1_2= ruleIntegerValue | lv_value_1_3= ruleBooleanValue | lv_value_1_4= ruleFloatValue ) )
            {
            // InternalFCL.g:4891:4: ( (lv_value_1_1= ruleStringValue | lv_value_1_2= ruleIntegerValue | lv_value_1_3= ruleBooleanValue | lv_value_1_4= ruleFloatValue ) )
            // InternalFCL.g:4892:5: (lv_value_1_1= ruleStringValue | lv_value_1_2= ruleIntegerValue | lv_value_1_3= ruleBooleanValue | lv_value_1_4= ruleFloatValue )
            {
            // InternalFCL.g:4892:5: (lv_value_1_1= ruleStringValue | lv_value_1_2= ruleIntegerValue | lv_value_1_3= ruleBooleanValue | lv_value_1_4= ruleFloatValue )
            int alt95=4;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt95=1;
                }
                break;
            case RULE_INT:
                {
                int LA95_2 = input.LA(2);

                if ( (LA95_2==EOF||LA95_2==RULE_ID||LA95_2==12||LA95_2==14||(LA95_2>=25 && LA95_2<=26)||(LA95_2>=29 && LA95_2<=31)||LA95_2==33||LA95_2==40||(LA95_2>=46 && LA95_2<=48)||(LA95_2>=53 && LA95_2<=54)||LA95_2==58||(LA95_2>=60 && LA95_2<=63)||LA95_2==73||LA95_2==75||LA95_2==93||(LA95_2>=98 && LA95_2<=108)) ) {
                    alt95=2;
                }
                else if ( (LA95_2==88) ) {
                    alt95=4;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 95, 2, input);

                    throw nvae;
                }
                }
                break;
            case 89:
            case 90:
                {
                alt95=3;
                }
                break;
            case 88:
                {
                alt95=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 95, 0, input);

                throw nvae;
            }

            switch (alt95) {
                case 1 :
                    // InternalFCL.g:4893:6: lv_value_1_1= ruleStringValue
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getCallConstantAccess().getValueStringValueParserRuleCall_1_0_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_value_1_1=ruleStringValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getCallConstantRule());
                      						}
                      						set(
                      							current,
                      							"value",
                      							lv_value_1_1,
                      							"fr.inria.glose.fcl.FCL.StringValue");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }
                    break;
                case 2 :
                    // InternalFCL.g:4909:6: lv_value_1_2= ruleIntegerValue
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getCallConstantAccess().getValueIntegerValueParserRuleCall_1_0_1());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_value_1_2=ruleIntegerValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getCallConstantRule());
                      						}
                      						set(
                      							current,
                      							"value",
                      							lv_value_1_2,
                      							"fr.inria.glose.fcl.FCL.IntegerValue");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }
                    break;
                case 3 :
                    // InternalFCL.g:4925:6: lv_value_1_3= ruleBooleanValue
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getCallConstantAccess().getValueBooleanValueParserRuleCall_1_0_2());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_value_1_3=ruleBooleanValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getCallConstantRule());
                      						}
                      						set(
                      							current,
                      							"value",
                      							lv_value_1_3,
                      							"fr.inria.glose.fcl.FCL.BooleanValue");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }
                    break;
                case 4 :
                    // InternalFCL.g:4941:6: lv_value_1_4= ruleFloatValue
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getCallConstantAccess().getValueFloatValueParserRuleCall_1_0_3());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_value_1_4=ruleFloatValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getCallConstantRule());
                      						}
                      						set(
                      							current,
                      							"value",
                      							lv_value_1_4,
                      							"fr.inria.glose.fcl.FCL.FloatValue");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }
                    break;

            }


            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCallConstant"


    // $ANTLR start "entryRuleStringValue"
    // InternalFCL.g:4963:1: entryRuleStringValue returns [EObject current=null] : iv_ruleStringValue= ruleStringValue EOF ;
    public final EObject entryRuleStringValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringValue = null;


        try {
            // InternalFCL.g:4963:52: (iv_ruleStringValue= ruleStringValue EOF )
            // InternalFCL.g:4964:2: iv_ruleStringValue= ruleStringValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringValueRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleStringValue=ruleStringValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringValue; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // InternalFCL.g:4970:1: ruleStringValue returns [EObject current=null] : ( () ( (lv_stringValue_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleStringValue() throws RecognitionException {
        EObject current = null;

        Token lv_stringValue_1_0=null;


        	enterRule();

        try {
            // InternalFCL.g:4976:2: ( ( () ( (lv_stringValue_1_0= RULE_STRING ) ) ) )
            // InternalFCL.g:4977:2: ( () ( (lv_stringValue_1_0= RULE_STRING ) ) )
            {
            // InternalFCL.g:4977:2: ( () ( (lv_stringValue_1_0= RULE_STRING ) ) )
            // InternalFCL.g:4978:3: () ( (lv_stringValue_1_0= RULE_STRING ) )
            {
            // InternalFCL.g:4978:3: ()
            // InternalFCL.g:4979:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getStringValueAccess().getStringValueAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:4985:3: ( (lv_stringValue_1_0= RULE_STRING ) )
            // InternalFCL.g:4986:4: (lv_stringValue_1_0= RULE_STRING )
            {
            // InternalFCL.g:4986:4: (lv_stringValue_1_0= RULE_STRING )
            // InternalFCL.g:4987:5: lv_stringValue_1_0= RULE_STRING
            {
            lv_stringValue_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_stringValue_1_0, grammarAccess.getStringValueAccess().getStringValueSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getStringValueRule());
              					}
              					setWithLastConsumed(
              						current,
              						"stringValue",
              						lv_stringValue_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleIntegerValue"
    // InternalFCL.g:5007:1: entryRuleIntegerValue returns [EObject current=null] : iv_ruleIntegerValue= ruleIntegerValue EOF ;
    public final EObject entryRuleIntegerValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerValue = null;


        try {
            // InternalFCL.g:5007:53: (iv_ruleIntegerValue= ruleIntegerValue EOF )
            // InternalFCL.g:5008:2: iv_ruleIntegerValue= ruleIntegerValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerValueRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleIntegerValue=ruleIntegerValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerValue; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerValue"


    // $ANTLR start "ruleIntegerValue"
    // InternalFCL.g:5014:1: ruleIntegerValue returns [EObject current=null] : ( () ( (lv_intValue_1_0= ruleEInt ) ) ) ;
    public final EObject ruleIntegerValue() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_intValue_1_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:5020:2: ( ( () ( (lv_intValue_1_0= ruleEInt ) ) ) )
            // InternalFCL.g:5021:2: ( () ( (lv_intValue_1_0= ruleEInt ) ) )
            {
            // InternalFCL.g:5021:2: ( () ( (lv_intValue_1_0= ruleEInt ) ) )
            // InternalFCL.g:5022:3: () ( (lv_intValue_1_0= ruleEInt ) )
            {
            // InternalFCL.g:5022:3: ()
            // InternalFCL.g:5023:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getIntegerValueAccess().getIntegerValueAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:5029:3: ( (lv_intValue_1_0= ruleEInt ) )
            // InternalFCL.g:5030:4: (lv_intValue_1_0= ruleEInt )
            {
            // InternalFCL.g:5030:4: (lv_intValue_1_0= ruleEInt )
            // InternalFCL.g:5031:5: lv_intValue_1_0= ruleEInt
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getIntegerValueAccess().getIntValueEIntParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_intValue_1_0=ruleEInt();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getIntegerValueRule());
              					}
              					set(
              						current,
              						"intValue",
              						lv_intValue_1_0,
              						"fr.inria.glose.fcl.FCL.EInt");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerValue"


    // $ANTLR start "entryRuleFloatValue"
    // InternalFCL.g:5052:1: entryRuleFloatValue returns [EObject current=null] : iv_ruleFloatValue= ruleFloatValue EOF ;
    public final EObject entryRuleFloatValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFloatValue = null;


        try {
            // InternalFCL.g:5052:51: (iv_ruleFloatValue= ruleFloatValue EOF )
            // InternalFCL.g:5053:2: iv_ruleFloatValue= ruleFloatValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFloatValueRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFloatValue=ruleFloatValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFloatValue; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFloatValue"


    // $ANTLR start "ruleFloatValue"
    // InternalFCL.g:5059:1: ruleFloatValue returns [EObject current=null] : ( () ( (lv_floatValue_1_0= ruleEDouble ) ) ) ;
    public final EObject ruleFloatValue() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_floatValue_1_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:5065:2: ( ( () ( (lv_floatValue_1_0= ruleEDouble ) ) ) )
            // InternalFCL.g:5066:2: ( () ( (lv_floatValue_1_0= ruleEDouble ) ) )
            {
            // InternalFCL.g:5066:2: ( () ( (lv_floatValue_1_0= ruleEDouble ) ) )
            // InternalFCL.g:5067:3: () ( (lv_floatValue_1_0= ruleEDouble ) )
            {
            // InternalFCL.g:5067:3: ()
            // InternalFCL.g:5068:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getFloatValueAccess().getFloatValueAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:5074:3: ( (lv_floatValue_1_0= ruleEDouble ) )
            // InternalFCL.g:5075:4: (lv_floatValue_1_0= ruleEDouble )
            {
            // InternalFCL.g:5075:4: (lv_floatValue_1_0= ruleEDouble )
            // InternalFCL.g:5076:5: lv_floatValue_1_0= ruleEDouble
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getFloatValueAccess().getFloatValueEDoubleParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_floatValue_1_0=ruleEDouble();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getFloatValueRule());
              					}
              					set(
              						current,
              						"floatValue",
              						lv_floatValue_1_0,
              						"fr.inria.glose.fcl.FCL.EDouble");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFloatValue"


    // $ANTLR start "entryRuleBooleanValue"
    // InternalFCL.g:5097:1: entryRuleBooleanValue returns [EObject current=null] : iv_ruleBooleanValue= ruleBooleanValue EOF ;
    public final EObject entryRuleBooleanValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanValue = null;


        try {
            // InternalFCL.g:5097:53: (iv_ruleBooleanValue= ruleBooleanValue EOF )
            // InternalFCL.g:5098:2: iv_ruleBooleanValue= ruleBooleanValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanValueRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBooleanValue=ruleBooleanValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanValue; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanValue"


    // $ANTLR start "ruleBooleanValue"
    // InternalFCL.g:5104:1: ruleBooleanValue returns [EObject current=null] : ( () ( (lv_booleanValue_1_0= ruleEBoolean ) ) ) ;
    public final EObject ruleBooleanValue() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_booleanValue_1_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:5110:2: ( ( () ( (lv_booleanValue_1_0= ruleEBoolean ) ) ) )
            // InternalFCL.g:5111:2: ( () ( (lv_booleanValue_1_0= ruleEBoolean ) ) )
            {
            // InternalFCL.g:5111:2: ( () ( (lv_booleanValue_1_0= ruleEBoolean ) ) )
            // InternalFCL.g:5112:3: () ( (lv_booleanValue_1_0= ruleEBoolean ) )
            {
            // InternalFCL.g:5112:3: ()
            // InternalFCL.g:5113:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getBooleanValueAccess().getBooleanValueAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:5119:3: ( (lv_booleanValue_1_0= ruleEBoolean ) )
            // InternalFCL.g:5120:4: (lv_booleanValue_1_0= ruleEBoolean )
            {
            // InternalFCL.g:5120:4: (lv_booleanValue_1_0= ruleEBoolean )
            // InternalFCL.g:5121:5: lv_booleanValue_1_0= ruleEBoolean
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBooleanValueAccess().getBooleanValueEBooleanParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_booleanValue_1_0=ruleEBoolean();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getBooleanValueRule());
              					}
              					set(
              						current,
              						"booleanValue",
              						lv_booleanValue_1_0,
              						"fr.inria.glose.fcl.FCL.EBoolean");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanValue"


    // $ANTLR start "entryRuleCallDeclarationReference"
    // InternalFCL.g:5142:1: entryRuleCallDeclarationReference returns [EObject current=null] : iv_ruleCallDeclarationReference= ruleCallDeclarationReference EOF ;
    public final EObject entryRuleCallDeclarationReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCallDeclarationReference = null;


        try {
            // InternalFCL.g:5142:65: (iv_ruleCallDeclarationReference= ruleCallDeclarationReference EOF )
            // InternalFCL.g:5143:2: iv_ruleCallDeclarationReference= ruleCallDeclarationReference EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCallDeclarationReferenceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleCallDeclarationReference=ruleCallDeclarationReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCallDeclarationReference; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCallDeclarationReference"


    // $ANTLR start "ruleCallDeclarationReference"
    // InternalFCL.g:5149:1: ruleCallDeclarationReference returns [EObject current=null] : ( () ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleCallDeclarationReference() throws RecognitionException {
        EObject current = null;


        	enterRule();

        try {
            // InternalFCL.g:5155:2: ( ( () ( ( ruleQualifiedName ) ) ) )
            // InternalFCL.g:5156:2: ( () ( ( ruleQualifiedName ) ) )
            {
            // InternalFCL.g:5156:2: ( () ( ( ruleQualifiedName ) ) )
            // InternalFCL.g:5157:3: () ( ( ruleQualifiedName ) )
            {
            // InternalFCL.g:5157:3: ()
            // InternalFCL.g:5158:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getCallDeclarationReferenceAccess().getCallDeclarationReferenceAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:5164:3: ( ( ruleQualifiedName ) )
            // InternalFCL.g:5165:4: ( ruleQualifiedName )
            {
            // InternalFCL.g:5165:4: ( ruleQualifiedName )
            // InternalFCL.g:5166:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getCallDeclarationReferenceRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getCallDeclarationReferenceAccess().getCallableCallableDeclarationCrossReference_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCallDeclarationReference"


    // $ANTLR start "entryRuleCallPrevious"
    // InternalFCL.g:5184:1: entryRuleCallPrevious returns [EObject current=null] : iv_ruleCallPrevious= ruleCallPrevious EOF ;
    public final EObject entryRuleCallPrevious() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCallPrevious = null;


        try {
            // InternalFCL.g:5184:53: (iv_ruleCallPrevious= ruleCallPrevious EOF )
            // InternalFCL.g:5185:2: iv_ruleCallPrevious= ruleCallPrevious EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCallPreviousRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleCallPrevious=ruleCallPrevious();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCallPrevious; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCallPrevious"


    // $ANTLR start "ruleCallPrevious"
    // InternalFCL.g:5191:1: ruleCallPrevious returns [EObject current=null] : ( () otherlv_1= 'prev' otherlv_2= '(' ( ( ruleQualifiedName ) ) otherlv_4= ')' ) ;
    public final EObject ruleCallPrevious() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;


        	enterRule();

        try {
            // InternalFCL.g:5197:2: ( ( () otherlv_1= 'prev' otherlv_2= '(' ( ( ruleQualifiedName ) ) otherlv_4= ')' ) )
            // InternalFCL.g:5198:2: ( () otherlv_1= 'prev' otherlv_2= '(' ( ( ruleQualifiedName ) ) otherlv_4= ')' )
            {
            // InternalFCL.g:5198:2: ( () otherlv_1= 'prev' otherlv_2= '(' ( ( ruleQualifiedName ) ) otherlv_4= ')' )
            // InternalFCL.g:5199:3: () otherlv_1= 'prev' otherlv_2= '(' ( ( ruleQualifiedName ) ) otherlv_4= ')'
            {
            // InternalFCL.g:5199:3: ()
            // InternalFCL.g:5200:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getCallPreviousAccess().getCallPreviousAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,74,FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getCallPreviousAccess().getPrevKeyword_1());
              		
            }
            otherlv_2=(Token)match(input,24,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getCallPreviousAccess().getLeftParenthesisKeyword_2());
              		
            }
            // InternalFCL.g:5214:3: ( ( ruleQualifiedName ) )
            // InternalFCL.g:5215:4: ( ruleQualifiedName )
            {
            // InternalFCL.g:5215:4: ( ruleQualifiedName )
            // InternalFCL.g:5216:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getCallPreviousRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getCallPreviousAccess().getCallableCallableDeclarationCrossReference_3_0());
              				
            }
            pushFollow(FOLLOW_80);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_4=(Token)match(input,26,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getCallPreviousAccess().getRightParenthesisKeyword_4());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCallPrevious"


    // $ANTLR start "entryRuleCallDataStructProperty"
    // InternalFCL.g:5238:1: entryRuleCallDataStructProperty returns [EObject current=null] : iv_ruleCallDataStructProperty= ruleCallDataStructProperty EOF ;
    public final EObject entryRuleCallDataStructProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCallDataStructProperty = null;


        try {
            // InternalFCL.g:5238:63: (iv_ruleCallDataStructProperty= ruleCallDataStructProperty EOF )
            // InternalFCL.g:5239:2: iv_ruleCallDataStructProperty= ruleCallDataStructProperty EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCallDataStructPropertyRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleCallDataStructProperty=ruleCallDataStructProperty();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCallDataStructProperty; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCallDataStructProperty"


    // $ANTLR start "ruleCallDataStructProperty"
    // InternalFCL.g:5245:1: ruleCallDataStructProperty returns [EObject current=null] : ( () ( (lv_rootTargetDataStruct_1_0= ruleCallDeclarationReference ) ) otherlv_2= '->' ( (lv_propertyName_3_0= RULE_ID ) ) (otherlv_4= '->' ( (lv_propertyName_5_0= RULE_ID ) ) )* ) ;
    public final EObject ruleCallDataStructProperty() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_propertyName_3_0=null;
        Token otherlv_4=null;
        Token lv_propertyName_5_0=null;
        EObject lv_rootTargetDataStruct_1_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:5251:2: ( ( () ( (lv_rootTargetDataStruct_1_0= ruleCallDeclarationReference ) ) otherlv_2= '->' ( (lv_propertyName_3_0= RULE_ID ) ) (otherlv_4= '->' ( (lv_propertyName_5_0= RULE_ID ) ) )* ) )
            // InternalFCL.g:5252:2: ( () ( (lv_rootTargetDataStruct_1_0= ruleCallDeclarationReference ) ) otherlv_2= '->' ( (lv_propertyName_3_0= RULE_ID ) ) (otherlv_4= '->' ( (lv_propertyName_5_0= RULE_ID ) ) )* )
            {
            // InternalFCL.g:5252:2: ( () ( (lv_rootTargetDataStruct_1_0= ruleCallDeclarationReference ) ) otherlv_2= '->' ( (lv_propertyName_3_0= RULE_ID ) ) (otherlv_4= '->' ( (lv_propertyName_5_0= RULE_ID ) ) )* )
            // InternalFCL.g:5253:3: () ( (lv_rootTargetDataStruct_1_0= ruleCallDeclarationReference ) ) otherlv_2= '->' ( (lv_propertyName_3_0= RULE_ID ) ) (otherlv_4= '->' ( (lv_propertyName_5_0= RULE_ID ) ) )*
            {
            // InternalFCL.g:5253:3: ()
            // InternalFCL.g:5254:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getCallDataStructPropertyAccess().getCallDataStructPropertyAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:5260:3: ( (lv_rootTargetDataStruct_1_0= ruleCallDeclarationReference ) )
            // InternalFCL.g:5261:4: (lv_rootTargetDataStruct_1_0= ruleCallDeclarationReference )
            {
            // InternalFCL.g:5261:4: (lv_rootTargetDataStruct_1_0= ruleCallDeclarationReference )
            // InternalFCL.g:5262:5: lv_rootTargetDataStruct_1_0= ruleCallDeclarationReference
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getCallDataStructPropertyAccess().getRootTargetDataStructCallDeclarationReferenceParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_25);
            lv_rootTargetDataStruct_1_0=ruleCallDeclarationReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getCallDataStructPropertyRule());
              					}
              					set(
              						current,
              						"rootTargetDataStruct",
              						lv_rootTargetDataStruct_1_0,
              						"fr.inria.glose.fcl.FCL.CallDeclarationReference");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,28,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getCallDataStructPropertyAccess().getHyphenMinusGreaterThanSignKeyword_2());
              		
            }
            // InternalFCL.g:5283:3: ( (lv_propertyName_3_0= RULE_ID ) )
            // InternalFCL.g:5284:4: (lv_propertyName_3_0= RULE_ID )
            {
            // InternalFCL.g:5284:4: (lv_propertyName_3_0= RULE_ID )
            // InternalFCL.g:5285:5: lv_propertyName_3_0= RULE_ID
            {
            lv_propertyName_3_0=(Token)match(input,RULE_ID,FOLLOW_81); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_propertyName_3_0, grammarAccess.getCallDataStructPropertyAccess().getPropertyNameIDTerminalRuleCall_3_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getCallDataStructPropertyRule());
              					}
              					addWithLastConsumed(
              						current,
              						"propertyName",
              						lv_propertyName_3_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            // InternalFCL.g:5301:3: (otherlv_4= '->' ( (lv_propertyName_5_0= RULE_ID ) ) )*
            loop96:
            do {
                int alt96=2;
                int LA96_0 = input.LA(1);

                if ( (LA96_0==28) ) {
                    alt96=1;
                }


                switch (alt96) {
            	case 1 :
            	    // InternalFCL.g:5302:4: otherlv_4= '->' ( (lv_propertyName_5_0= RULE_ID ) )
            	    {
            	    otherlv_4=(Token)match(input,28,FOLLOW_3); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_4, grammarAccess.getCallDataStructPropertyAccess().getHyphenMinusGreaterThanSignKeyword_4_0());
            	      			
            	    }
            	    // InternalFCL.g:5306:4: ( (lv_propertyName_5_0= RULE_ID ) )
            	    // InternalFCL.g:5307:5: (lv_propertyName_5_0= RULE_ID )
            	    {
            	    // InternalFCL.g:5307:5: (lv_propertyName_5_0= RULE_ID )
            	    // InternalFCL.g:5308:6: lv_propertyName_5_0= RULE_ID
            	    {
            	    lv_propertyName_5_0=(Token)match(input,RULE_ID,FOLLOW_81); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						newLeafNode(lv_propertyName_5_0, grammarAccess.getCallDataStructPropertyAccess().getPropertyNameIDTerminalRuleCall_4_1_0());
            	      					
            	    }
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElement(grammarAccess.getCallDataStructPropertyRule());
            	      						}
            	      						addWithLastConsumed(
            	      							current,
            	      							"propertyName",
            	      							lv_propertyName_5_0,
            	      							"org.eclipse.xtext.common.Terminals.ID");
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop96;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCallDataStructProperty"


    // $ANTLR start "entryRuleOrExpression"
    // InternalFCL.g:5329:1: entryRuleOrExpression returns [EObject current=null] : iv_ruleOrExpression= ruleOrExpression EOF ;
    public final EObject entryRuleOrExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrExpression = null;


        try {
            // InternalFCL.g:5329:53: (iv_ruleOrExpression= ruleOrExpression EOF )
            // InternalFCL.g:5330:2: iv_ruleOrExpression= ruleOrExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOrExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOrExpression=ruleOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOrExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrExpression"


    // $ANTLR start "ruleOrExpression"
    // InternalFCL.g:5336:1: ruleOrExpression returns [EObject current=null] : (this_AndExpression_0= ruleAndExpression ( () ( (lv_operator_2_0= ruleOrOperator ) ) ( (lv_rhsOperand_3_0= ruleAndExpression ) ) )* ) ;
    public final EObject ruleOrExpression() throws RecognitionException {
        EObject current = null;

        EObject this_AndExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rhsOperand_3_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:5342:2: ( (this_AndExpression_0= ruleAndExpression ( () ( (lv_operator_2_0= ruleOrOperator ) ) ( (lv_rhsOperand_3_0= ruleAndExpression ) ) )* ) )
            // InternalFCL.g:5343:2: (this_AndExpression_0= ruleAndExpression ( () ( (lv_operator_2_0= ruleOrOperator ) ) ( (lv_rhsOperand_3_0= ruleAndExpression ) ) )* )
            {
            // InternalFCL.g:5343:2: (this_AndExpression_0= ruleAndExpression ( () ( (lv_operator_2_0= ruleOrOperator ) ) ( (lv_rhsOperand_3_0= ruleAndExpression ) ) )* )
            // InternalFCL.g:5344:3: this_AndExpression_0= ruleAndExpression ( () ( (lv_operator_2_0= ruleOrOperator ) ) ( (lv_rhsOperand_3_0= ruleAndExpression ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getOrExpressionAccess().getAndExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_82);
            this_AndExpression_0=ruleAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_AndExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalFCL.g:5352:3: ( () ( (lv_operator_2_0= ruleOrOperator ) ) ( (lv_rhsOperand_3_0= ruleAndExpression ) ) )*
            loop97:
            do {
                int alt97=2;
                int LA97_0 = input.LA(1);

                if ( (LA97_0==108) ) {
                    alt97=1;
                }


                switch (alt97) {
            	case 1 :
            	    // InternalFCL.g:5353:4: () ( (lv_operator_2_0= ruleOrOperator ) ) ( (lv_rhsOperand_3_0= ruleAndExpression ) )
            	    {
            	    // InternalFCL.g:5353:4: ()
            	    // InternalFCL.g:5354:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getOrExpressionAccess().getBasicOperatorBinaryExpressionLhsOperandAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    // InternalFCL.g:5360:4: ( (lv_operator_2_0= ruleOrOperator ) )
            	    // InternalFCL.g:5361:5: (lv_operator_2_0= ruleOrOperator )
            	    {
            	    // InternalFCL.g:5361:5: (lv_operator_2_0= ruleOrOperator )
            	    // InternalFCL.g:5362:6: lv_operator_2_0= ruleOrOperator
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getOrExpressionAccess().getOperatorOrOperatorEnumRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_83);
            	    lv_operator_2_0=ruleOrOperator();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getOrExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"operator",
            	      							lv_operator_2_0,
            	      							"fr.inria.glose.fcl.FCL.OrOperator");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    // InternalFCL.g:5379:4: ( (lv_rhsOperand_3_0= ruleAndExpression ) )
            	    // InternalFCL.g:5380:5: (lv_rhsOperand_3_0= ruleAndExpression )
            	    {
            	    // InternalFCL.g:5380:5: (lv_rhsOperand_3_0= ruleAndExpression )
            	    // InternalFCL.g:5381:6: lv_rhsOperand_3_0= ruleAndExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getOrExpressionAccess().getRhsOperandAndExpressionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_82);
            	    lv_rhsOperand_3_0=ruleAndExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getOrExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"rhsOperand",
            	      							lv_rhsOperand_3_0,
            	      							"fr.inria.glose.fcl.FCL.AndExpression");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop97;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrExpression"


    // $ANTLR start "entryRuleAndExpression"
    // InternalFCL.g:5403:1: entryRuleAndExpression returns [EObject current=null] : iv_ruleAndExpression= ruleAndExpression EOF ;
    public final EObject entryRuleAndExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAndExpression = null;


        try {
            // InternalFCL.g:5403:54: (iv_ruleAndExpression= ruleAndExpression EOF )
            // InternalFCL.g:5404:2: iv_ruleAndExpression= ruleAndExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAndExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAndExpression=ruleAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAndExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAndExpression"


    // $ANTLR start "ruleAndExpression"
    // InternalFCL.g:5410:1: ruleAndExpression returns [EObject current=null] : (this_EqualExpression_0= ruleEqualExpression ( () ( (lv_operator_2_0= ruleAndOperator ) ) ( (lv_rhsOperand_3_0= ruleEqualExpression ) ) )* ) ;
    public final EObject ruleAndExpression() throws RecognitionException {
        EObject current = null;

        EObject this_EqualExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rhsOperand_3_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:5416:2: ( (this_EqualExpression_0= ruleEqualExpression ( () ( (lv_operator_2_0= ruleAndOperator ) ) ( (lv_rhsOperand_3_0= ruleEqualExpression ) ) )* ) )
            // InternalFCL.g:5417:2: (this_EqualExpression_0= ruleEqualExpression ( () ( (lv_operator_2_0= ruleAndOperator ) ) ( (lv_rhsOperand_3_0= ruleEqualExpression ) ) )* )
            {
            // InternalFCL.g:5417:2: (this_EqualExpression_0= ruleEqualExpression ( () ( (lv_operator_2_0= ruleAndOperator ) ) ( (lv_rhsOperand_3_0= ruleEqualExpression ) ) )* )
            // InternalFCL.g:5418:3: this_EqualExpression_0= ruleEqualExpression ( () ( (lv_operator_2_0= ruleAndOperator ) ) ( (lv_rhsOperand_3_0= ruleEqualExpression ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getAndExpressionAccess().getEqualExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_84);
            this_EqualExpression_0=ruleEqualExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_EqualExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalFCL.g:5426:3: ( () ( (lv_operator_2_0= ruleAndOperator ) ) ( (lv_rhsOperand_3_0= ruleEqualExpression ) ) )*
            loop98:
            do {
                int alt98=2;
                int LA98_0 = input.LA(1);

                if ( (LA98_0==107) ) {
                    alt98=1;
                }


                switch (alt98) {
            	case 1 :
            	    // InternalFCL.g:5427:4: () ( (lv_operator_2_0= ruleAndOperator ) ) ( (lv_rhsOperand_3_0= ruleEqualExpression ) )
            	    {
            	    // InternalFCL.g:5427:4: ()
            	    // InternalFCL.g:5428:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getAndExpressionAccess().getBasicOperatorBinaryExpressionLhsOperandAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    // InternalFCL.g:5434:4: ( (lv_operator_2_0= ruleAndOperator ) )
            	    // InternalFCL.g:5435:5: (lv_operator_2_0= ruleAndOperator )
            	    {
            	    // InternalFCL.g:5435:5: (lv_operator_2_0= ruleAndOperator )
            	    // InternalFCL.g:5436:6: lv_operator_2_0= ruleAndOperator
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getAndExpressionAccess().getOperatorAndOperatorEnumRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_83);
            	    lv_operator_2_0=ruleAndOperator();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getAndExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"operator",
            	      							lv_operator_2_0,
            	      							"fr.inria.glose.fcl.FCL.AndOperator");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    // InternalFCL.g:5453:4: ( (lv_rhsOperand_3_0= ruleEqualExpression ) )
            	    // InternalFCL.g:5454:5: (lv_rhsOperand_3_0= ruleEqualExpression )
            	    {
            	    // InternalFCL.g:5454:5: (lv_rhsOperand_3_0= ruleEqualExpression )
            	    // InternalFCL.g:5455:6: lv_rhsOperand_3_0= ruleEqualExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getAndExpressionAccess().getRhsOperandEqualExpressionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_84);
            	    lv_rhsOperand_3_0=ruleEqualExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getAndExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"rhsOperand",
            	      							lv_rhsOperand_3_0,
            	      							"fr.inria.glose.fcl.FCL.EqualExpression");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop98;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndExpression"


    // $ANTLR start "entryRuleEqualExpression"
    // InternalFCL.g:5477:1: entryRuleEqualExpression returns [EObject current=null] : iv_ruleEqualExpression= ruleEqualExpression EOF ;
    public final EObject entryRuleEqualExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEqualExpression = null;


        try {
            // InternalFCL.g:5477:56: (iv_ruleEqualExpression= ruleEqualExpression EOF )
            // InternalFCL.g:5478:2: iv_ruleEqualExpression= ruleEqualExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEqualExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEqualExpression=ruleEqualExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEqualExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEqualExpression"


    // $ANTLR start "ruleEqualExpression"
    // InternalFCL.g:5484:1: ruleEqualExpression returns [EObject current=null] : (this_ComparisionExpression_0= ruleComparisionExpression ( () ( (lv_operator_2_0= ruleEqualOperator ) ) ( (lv_rhsOperand_3_0= ruleComparisionExpression ) ) )* ) ;
    public final EObject ruleEqualExpression() throws RecognitionException {
        EObject current = null;

        EObject this_ComparisionExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rhsOperand_3_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:5490:2: ( (this_ComparisionExpression_0= ruleComparisionExpression ( () ( (lv_operator_2_0= ruleEqualOperator ) ) ( (lv_rhsOperand_3_0= ruleComparisionExpression ) ) )* ) )
            // InternalFCL.g:5491:2: (this_ComparisionExpression_0= ruleComparisionExpression ( () ( (lv_operator_2_0= ruleEqualOperator ) ) ( (lv_rhsOperand_3_0= ruleComparisionExpression ) ) )* )
            {
            // InternalFCL.g:5491:2: (this_ComparisionExpression_0= ruleComparisionExpression ( () ( (lv_operator_2_0= ruleEqualOperator ) ) ( (lv_rhsOperand_3_0= ruleComparisionExpression ) ) )* )
            // InternalFCL.g:5492:3: this_ComparisionExpression_0= ruleComparisionExpression ( () ( (lv_operator_2_0= ruleEqualOperator ) ) ( (lv_rhsOperand_3_0= ruleComparisionExpression ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getEqualExpressionAccess().getComparisionExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_85);
            this_ComparisionExpression_0=ruleComparisionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_ComparisionExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalFCL.g:5500:3: ( () ( (lv_operator_2_0= ruleEqualOperator ) ) ( (lv_rhsOperand_3_0= ruleComparisionExpression ) ) )*
            loop99:
            do {
                int alt99=2;
                int LA99_0 = input.LA(1);

                if ( (LA99_0==102) ) {
                    alt99=1;
                }


                switch (alt99) {
            	case 1 :
            	    // InternalFCL.g:5501:4: () ( (lv_operator_2_0= ruleEqualOperator ) ) ( (lv_rhsOperand_3_0= ruleComparisionExpression ) )
            	    {
            	    // InternalFCL.g:5501:4: ()
            	    // InternalFCL.g:5502:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getEqualExpressionAccess().getBasicOperatorBinaryExpressionLhsOperandAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    // InternalFCL.g:5508:4: ( (lv_operator_2_0= ruleEqualOperator ) )
            	    // InternalFCL.g:5509:5: (lv_operator_2_0= ruleEqualOperator )
            	    {
            	    // InternalFCL.g:5509:5: (lv_operator_2_0= ruleEqualOperator )
            	    // InternalFCL.g:5510:6: lv_operator_2_0= ruleEqualOperator
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getEqualExpressionAccess().getOperatorEqualOperatorEnumRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_83);
            	    lv_operator_2_0=ruleEqualOperator();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getEqualExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"operator",
            	      							lv_operator_2_0,
            	      							"fr.inria.glose.fcl.FCL.EqualOperator");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    // InternalFCL.g:5527:4: ( (lv_rhsOperand_3_0= ruleComparisionExpression ) )
            	    // InternalFCL.g:5528:5: (lv_rhsOperand_3_0= ruleComparisionExpression )
            	    {
            	    // InternalFCL.g:5528:5: (lv_rhsOperand_3_0= ruleComparisionExpression )
            	    // InternalFCL.g:5529:6: lv_rhsOperand_3_0= ruleComparisionExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getEqualExpressionAccess().getRhsOperandComparisionExpressionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_85);
            	    lv_rhsOperand_3_0=ruleComparisionExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getEqualExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"rhsOperand",
            	      							lv_rhsOperand_3_0,
            	      							"fr.inria.glose.fcl.FCL.ComparisionExpression");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop99;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEqualExpression"


    // $ANTLR start "entryRuleComparisionExpression"
    // InternalFCL.g:5551:1: entryRuleComparisionExpression returns [EObject current=null] : iv_ruleComparisionExpression= ruleComparisionExpression EOF ;
    public final EObject entryRuleComparisionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComparisionExpression = null;


        try {
            // InternalFCL.g:5551:62: (iv_ruleComparisionExpression= ruleComparisionExpression EOF )
            // InternalFCL.g:5552:2: iv_ruleComparisionExpression= ruleComparisionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getComparisionExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleComparisionExpression=ruleComparisionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleComparisionExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComparisionExpression"


    // $ANTLR start "ruleComparisionExpression"
    // InternalFCL.g:5558:1: ruleComparisionExpression returns [EObject current=null] : (this_AdditionExpression_0= ruleAdditionExpression ( () ( (lv_operator_2_0= ruleComparisionOperator ) ) ( (lv_rhsOperand_3_0= ruleAdditionExpression ) ) )* ) ;
    public final EObject ruleComparisionExpression() throws RecognitionException {
        EObject current = null;

        EObject this_AdditionExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rhsOperand_3_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:5564:2: ( (this_AdditionExpression_0= ruleAdditionExpression ( () ( (lv_operator_2_0= ruleComparisionOperator ) ) ( (lv_rhsOperand_3_0= ruleAdditionExpression ) ) )* ) )
            // InternalFCL.g:5565:2: (this_AdditionExpression_0= ruleAdditionExpression ( () ( (lv_operator_2_0= ruleComparisionOperator ) ) ( (lv_rhsOperand_3_0= ruleAdditionExpression ) ) )* )
            {
            // InternalFCL.g:5565:2: (this_AdditionExpression_0= ruleAdditionExpression ( () ( (lv_operator_2_0= ruleComparisionOperator ) ) ( (lv_rhsOperand_3_0= ruleAdditionExpression ) ) )* )
            // InternalFCL.g:5566:3: this_AdditionExpression_0= ruleAdditionExpression ( () ( (lv_operator_2_0= ruleComparisionOperator ) ) ( (lv_rhsOperand_3_0= ruleAdditionExpression ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getComparisionExpressionAccess().getAdditionExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_86);
            this_AdditionExpression_0=ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_AdditionExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalFCL.g:5574:3: ( () ( (lv_operator_2_0= ruleComparisionOperator ) ) ( (lv_rhsOperand_3_0= ruleAdditionExpression ) ) )*
            loop100:
            do {
                int alt100=2;
                int LA100_0 = input.LA(1);

                if ( ((LA100_0>=103 && LA100_0<=106)) ) {
                    alt100=1;
                }


                switch (alt100) {
            	case 1 :
            	    // InternalFCL.g:5575:4: () ( (lv_operator_2_0= ruleComparisionOperator ) ) ( (lv_rhsOperand_3_0= ruleAdditionExpression ) )
            	    {
            	    // InternalFCL.g:5575:4: ()
            	    // InternalFCL.g:5576:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getComparisionExpressionAccess().getBasicOperatorBinaryExpressionLhsOperandAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    // InternalFCL.g:5582:4: ( (lv_operator_2_0= ruleComparisionOperator ) )
            	    // InternalFCL.g:5583:5: (lv_operator_2_0= ruleComparisionOperator )
            	    {
            	    // InternalFCL.g:5583:5: (lv_operator_2_0= ruleComparisionOperator )
            	    // InternalFCL.g:5584:6: lv_operator_2_0= ruleComparisionOperator
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getComparisionExpressionAccess().getOperatorComparisionOperatorEnumRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_83);
            	    lv_operator_2_0=ruleComparisionOperator();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getComparisionExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"operator",
            	      							lv_operator_2_0,
            	      							"fr.inria.glose.fcl.FCL.ComparisionOperator");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    // InternalFCL.g:5601:4: ( (lv_rhsOperand_3_0= ruleAdditionExpression ) )
            	    // InternalFCL.g:5602:5: (lv_rhsOperand_3_0= ruleAdditionExpression )
            	    {
            	    // InternalFCL.g:5602:5: (lv_rhsOperand_3_0= ruleAdditionExpression )
            	    // InternalFCL.g:5603:6: lv_rhsOperand_3_0= ruleAdditionExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getComparisionExpressionAccess().getRhsOperandAdditionExpressionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_86);
            	    lv_rhsOperand_3_0=ruleAdditionExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getComparisionExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"rhsOperand",
            	      							lv_rhsOperand_3_0,
            	      							"fr.inria.glose.fcl.FCL.AdditionExpression");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop100;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComparisionExpression"


    // $ANTLR start "entryRuleAdditionExpression"
    // InternalFCL.g:5625:1: entryRuleAdditionExpression returns [EObject current=null] : iv_ruleAdditionExpression= ruleAdditionExpression EOF ;
    public final EObject entryRuleAdditionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdditionExpression = null;


        try {
            // InternalFCL.g:5625:59: (iv_ruleAdditionExpression= ruleAdditionExpression EOF )
            // InternalFCL.g:5626:2: iv_ruleAdditionExpression= ruleAdditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAdditionExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAdditionExpression=ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAdditionExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdditionExpression"


    // $ANTLR start "ruleAdditionExpression"
    // InternalFCL.g:5632:1: ruleAdditionExpression returns [EObject current=null] : (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( (lv_operator_2_0= ruleAdditionOperator ) ) ( (lv_rhsOperand_3_0= ruleMultiplicationExpression ) ) )* ) ;
    public final EObject ruleAdditionExpression() throws RecognitionException {
        EObject current = null;

        EObject this_MultiplicationExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rhsOperand_3_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:5638:2: ( (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( (lv_operator_2_0= ruleAdditionOperator ) ) ( (lv_rhsOperand_3_0= ruleMultiplicationExpression ) ) )* ) )
            // InternalFCL.g:5639:2: (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( (lv_operator_2_0= ruleAdditionOperator ) ) ( (lv_rhsOperand_3_0= ruleMultiplicationExpression ) ) )* )
            {
            // InternalFCL.g:5639:2: (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( (lv_operator_2_0= ruleAdditionOperator ) ) ( (lv_rhsOperand_3_0= ruleMultiplicationExpression ) ) )* )
            // InternalFCL.g:5640:3: this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( (lv_operator_2_0= ruleAdditionOperator ) ) ( (lv_rhsOperand_3_0= ruleMultiplicationExpression ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_87);
            this_MultiplicationExpression_0=ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_MultiplicationExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalFCL.g:5648:3: ( () ( (lv_operator_2_0= ruleAdditionOperator ) ) ( (lv_rhsOperand_3_0= ruleMultiplicationExpression ) ) )*
            loop101:
            do {
                int alt101=2;
                int LA101_0 = input.LA(1);

                if ( (LA101_0==93||LA101_0==98) ) {
                    alt101=1;
                }


                switch (alt101) {
            	case 1 :
            	    // InternalFCL.g:5649:4: () ( (lv_operator_2_0= ruleAdditionOperator ) ) ( (lv_rhsOperand_3_0= ruleMultiplicationExpression ) )
            	    {
            	    // InternalFCL.g:5649:4: ()
            	    // InternalFCL.g:5650:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getAdditionExpressionAccess().getBasicOperatorBinaryExpressionLhsOperandAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    // InternalFCL.g:5656:4: ( (lv_operator_2_0= ruleAdditionOperator ) )
            	    // InternalFCL.g:5657:5: (lv_operator_2_0= ruleAdditionOperator )
            	    {
            	    // InternalFCL.g:5657:5: (lv_operator_2_0= ruleAdditionOperator )
            	    // InternalFCL.g:5658:6: lv_operator_2_0= ruleAdditionOperator
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getAdditionExpressionAccess().getOperatorAdditionOperatorEnumRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_83);
            	    lv_operator_2_0=ruleAdditionOperator();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getAdditionExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"operator",
            	      							lv_operator_2_0,
            	      							"fr.inria.glose.fcl.FCL.AdditionOperator");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    // InternalFCL.g:5675:4: ( (lv_rhsOperand_3_0= ruleMultiplicationExpression ) )
            	    // InternalFCL.g:5676:5: (lv_rhsOperand_3_0= ruleMultiplicationExpression )
            	    {
            	    // InternalFCL.g:5676:5: (lv_rhsOperand_3_0= ruleMultiplicationExpression )
            	    // InternalFCL.g:5677:6: lv_rhsOperand_3_0= ruleMultiplicationExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getAdditionExpressionAccess().getRhsOperandMultiplicationExpressionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_87);
            	    lv_rhsOperand_3_0=ruleMultiplicationExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getAdditionExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"rhsOperand",
            	      							lv_rhsOperand_3_0,
            	      							"fr.inria.glose.fcl.FCL.MultiplicationExpression");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop101;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdditionExpression"


    // $ANTLR start "entryRuleMultiplicationExpression"
    // InternalFCL.g:5699:1: entryRuleMultiplicationExpression returns [EObject current=null] : iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF ;
    public final EObject entryRuleMultiplicationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiplicationExpression = null;


        try {
            // InternalFCL.g:5699:65: (iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF )
            // InternalFCL.g:5700:2: iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMultiplicationExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMultiplicationExpression=ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMultiplicationExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiplicationExpression"


    // $ANTLR start "ruleMultiplicationExpression"
    // InternalFCL.g:5706:1: ruleMultiplicationExpression returns [EObject current=null] : (this_PrimaryExpression_0= rulePrimaryExpression ( () ( (lv_operator_2_0= ruleMultiplicationOperator ) ) ( (lv_rhsOperand_3_0= ruleUnaryOrPrimaryExpression ) ) )* ) ;
    public final EObject ruleMultiplicationExpression() throws RecognitionException {
        EObject current = null;

        EObject this_PrimaryExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rhsOperand_3_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:5712:2: ( (this_PrimaryExpression_0= rulePrimaryExpression ( () ( (lv_operator_2_0= ruleMultiplicationOperator ) ) ( (lv_rhsOperand_3_0= ruleUnaryOrPrimaryExpression ) ) )* ) )
            // InternalFCL.g:5713:2: (this_PrimaryExpression_0= rulePrimaryExpression ( () ( (lv_operator_2_0= ruleMultiplicationOperator ) ) ( (lv_rhsOperand_3_0= ruleUnaryOrPrimaryExpression ) ) )* )
            {
            // InternalFCL.g:5713:2: (this_PrimaryExpression_0= rulePrimaryExpression ( () ( (lv_operator_2_0= ruleMultiplicationOperator ) ) ( (lv_rhsOperand_3_0= ruleUnaryOrPrimaryExpression ) ) )* )
            // InternalFCL.g:5714:3: this_PrimaryExpression_0= rulePrimaryExpression ( () ( (lv_operator_2_0= ruleMultiplicationOperator ) ) ( (lv_rhsOperand_3_0= ruleUnaryOrPrimaryExpression ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getPrimaryExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_88);
            this_PrimaryExpression_0=rulePrimaryExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_PrimaryExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalFCL.g:5722:3: ( () ( (lv_operator_2_0= ruleMultiplicationOperator ) ) ( (lv_rhsOperand_3_0= ruleUnaryOrPrimaryExpression ) ) )*
            loop102:
            do {
                int alt102=2;
                int LA102_0 = input.LA(1);

                if ( ((LA102_0>=99 && LA102_0<=101)) ) {
                    alt102=1;
                }


                switch (alt102) {
            	case 1 :
            	    // InternalFCL.g:5723:4: () ( (lv_operator_2_0= ruleMultiplicationOperator ) ) ( (lv_rhsOperand_3_0= ruleUnaryOrPrimaryExpression ) )
            	    {
            	    // InternalFCL.g:5723:4: ()
            	    // InternalFCL.g:5724:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getMultiplicationExpressionAccess().getBasicOperatorBinaryExpressionLhsOperandAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    // InternalFCL.g:5730:4: ( (lv_operator_2_0= ruleMultiplicationOperator ) )
            	    // InternalFCL.g:5731:5: (lv_operator_2_0= ruleMultiplicationOperator )
            	    {
            	    // InternalFCL.g:5731:5: (lv_operator_2_0= ruleMultiplicationOperator )
            	    // InternalFCL.g:5732:6: lv_operator_2_0= ruleMultiplicationOperator
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getOperatorMultiplicationOperatorEnumRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_27);
            	    lv_operator_2_0=ruleMultiplicationOperator();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getMultiplicationExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"operator",
            	      							lv_operator_2_0,
            	      							"fr.inria.glose.fcl.FCL.MultiplicationOperator");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    // InternalFCL.g:5749:4: ( (lv_rhsOperand_3_0= ruleUnaryOrPrimaryExpression ) )
            	    // InternalFCL.g:5750:5: (lv_rhsOperand_3_0= ruleUnaryOrPrimaryExpression )
            	    {
            	    // InternalFCL.g:5750:5: (lv_rhsOperand_3_0= ruleUnaryOrPrimaryExpression )
            	    // InternalFCL.g:5751:6: lv_rhsOperand_3_0= ruleUnaryOrPrimaryExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getRhsOperandUnaryOrPrimaryExpressionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_88);
            	    lv_rhsOperand_3_0=ruleUnaryOrPrimaryExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getMultiplicationExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"rhsOperand",
            	      							lv_rhsOperand_3_0,
            	      							"fr.inria.glose.fcl.FCL.UnaryOrPrimaryExpression");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop102;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicationExpression"


    // $ANTLR start "entryRuleUnaryOrPrimaryExpression"
    // InternalFCL.g:5773:1: entryRuleUnaryOrPrimaryExpression returns [EObject current=null] : iv_ruleUnaryOrPrimaryExpression= ruleUnaryOrPrimaryExpression EOF ;
    public final EObject entryRuleUnaryOrPrimaryExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnaryOrPrimaryExpression = null;


        try {
            // InternalFCL.g:5773:65: (iv_ruleUnaryOrPrimaryExpression= ruleUnaryOrPrimaryExpression EOF )
            // InternalFCL.g:5774:2: iv_ruleUnaryOrPrimaryExpression= ruleUnaryOrPrimaryExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnaryOrPrimaryExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleUnaryOrPrimaryExpression=ruleUnaryOrPrimaryExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnaryOrPrimaryExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryOrPrimaryExpression"


    // $ANTLR start "ruleUnaryOrPrimaryExpression"
    // InternalFCL.g:5780:1: ruleUnaryOrPrimaryExpression returns [EObject current=null] : (this_PrimaryExpression_0= rulePrimaryExpression | this_UnaryExpression_1= ruleUnaryExpression ) ;
    public final EObject ruleUnaryOrPrimaryExpression() throws RecognitionException {
        EObject current = null;

        EObject this_PrimaryExpression_0 = null;

        EObject this_UnaryExpression_1 = null;



        	enterRule();

        try {
            // InternalFCL.g:5786:2: ( (this_PrimaryExpression_0= rulePrimaryExpression | this_UnaryExpression_1= ruleUnaryExpression ) )
            // InternalFCL.g:5787:2: (this_PrimaryExpression_0= rulePrimaryExpression | this_UnaryExpression_1= ruleUnaryExpression )
            {
            // InternalFCL.g:5787:2: (this_PrimaryExpression_0= rulePrimaryExpression | this_UnaryExpression_1= ruleUnaryExpression )
            int alt103=2;
            int LA103_0 = input.LA(1);

            if ( ((LA103_0>=RULE_ID && LA103_0<=RULE_INT)||LA103_0==24||LA103_0==69||LA103_0==72||LA103_0==74||(LA103_0>=88 && LA103_0<=90)) ) {
                alt103=1;
            }
            else if ( (LA103_0==93||LA103_0==97) ) {
                alt103=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 103, 0, input);

                throw nvae;
            }
            switch (alt103) {
                case 1 :
                    // InternalFCL.g:5788:3: this_PrimaryExpression_0= rulePrimaryExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getUnaryOrPrimaryExpressionAccess().getPrimaryExpressionParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_PrimaryExpression_0=rulePrimaryExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_PrimaryExpression_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalFCL.g:5797:3: this_UnaryExpression_1= ruleUnaryExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getUnaryOrPrimaryExpressionAccess().getUnaryExpressionParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_UnaryExpression_1=ruleUnaryExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_UnaryExpression_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryOrPrimaryExpression"


    // $ANTLR start "entryRuleUnaryExpression"
    // InternalFCL.g:5809:1: entryRuleUnaryExpression returns [EObject current=null] : iv_ruleUnaryExpression= ruleUnaryExpression EOF ;
    public final EObject entryRuleUnaryExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnaryExpression = null;


        try {
            // InternalFCL.g:5809:56: (iv_ruleUnaryExpression= ruleUnaryExpression EOF )
            // InternalFCL.g:5810:2: iv_ruleUnaryExpression= ruleUnaryExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnaryExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleUnaryExpression=ruleUnaryExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnaryExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryExpression"


    // $ANTLR start "ruleUnaryExpression"
    // InternalFCL.g:5816:1: ruleUnaryExpression returns [EObject current=null] : ( () ( (lv_operator_1_0= ruleUnaryOperator ) ) ( (lv_operand_2_0= ruleUnaryOrPrimaryExpression ) ) ) ;
    public final EObject ruleUnaryExpression() throws RecognitionException {
        EObject current = null;

        Enumerator lv_operator_1_0 = null;

        EObject lv_operand_2_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:5822:2: ( ( () ( (lv_operator_1_0= ruleUnaryOperator ) ) ( (lv_operand_2_0= ruleUnaryOrPrimaryExpression ) ) ) )
            // InternalFCL.g:5823:2: ( () ( (lv_operator_1_0= ruleUnaryOperator ) ) ( (lv_operand_2_0= ruleUnaryOrPrimaryExpression ) ) )
            {
            // InternalFCL.g:5823:2: ( () ( (lv_operator_1_0= ruleUnaryOperator ) ) ( (lv_operand_2_0= ruleUnaryOrPrimaryExpression ) ) )
            // InternalFCL.g:5824:3: () ( (lv_operator_1_0= ruleUnaryOperator ) ) ( (lv_operand_2_0= ruleUnaryOrPrimaryExpression ) )
            {
            // InternalFCL.g:5824:3: ()
            // InternalFCL.g:5825:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getUnaryExpressionAccess().getBasicOperatorUnaryExpressionAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:5831:3: ( (lv_operator_1_0= ruleUnaryOperator ) )
            // InternalFCL.g:5832:4: (lv_operator_1_0= ruleUnaryOperator )
            {
            // InternalFCL.g:5832:4: (lv_operator_1_0= ruleUnaryOperator )
            // InternalFCL.g:5833:5: lv_operator_1_0= ruleUnaryOperator
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getUnaryExpressionAccess().getOperatorUnaryOperatorEnumRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_27);
            lv_operator_1_0=ruleUnaryOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
              					}
              					set(
              						current,
              						"operator",
              						lv_operator_1_0,
              						"fr.inria.glose.fcl.FCL.UnaryOperator");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalFCL.g:5850:3: ( (lv_operand_2_0= ruleUnaryOrPrimaryExpression ) )
            // InternalFCL.g:5851:4: (lv_operand_2_0= ruleUnaryOrPrimaryExpression )
            {
            // InternalFCL.g:5851:4: (lv_operand_2_0= ruleUnaryOrPrimaryExpression )
            // InternalFCL.g:5852:5: lv_operand_2_0= ruleUnaryOrPrimaryExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getUnaryExpressionAccess().getOperandUnaryOrPrimaryExpressionParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_operand_2_0=ruleUnaryOrPrimaryExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
              					}
              					set(
              						current,
              						"operand",
              						lv_operand_2_0,
              						"fr.inria.glose.fcl.FCL.UnaryOrPrimaryExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryExpression"


    // $ANTLR start "entryRulePrimaryExpression"
    // InternalFCL.g:5873:1: entryRulePrimaryExpression returns [EObject current=null] : iv_rulePrimaryExpression= rulePrimaryExpression EOF ;
    public final EObject entryRulePrimaryExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimaryExpression = null;


        try {
            // InternalFCL.g:5873:58: (iv_rulePrimaryExpression= rulePrimaryExpression EOF )
            // InternalFCL.g:5874:2: iv_rulePrimaryExpression= rulePrimaryExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPrimaryExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_rulePrimaryExpression=rulePrimaryExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePrimaryExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimaryExpression"


    // $ANTLR start "rulePrimaryExpression"
    // InternalFCL.g:5880:1: rulePrimaryExpression returns [EObject current=null] : (this_Cast_0= ruleCast | this_ProcedureCall_1= ruleProcedureCall | this_Call_2= ruleCall | this_NewDataStruct_3= ruleNewDataStruct | (otherlv_4= '(' (this_OrExpression_5= ruleOrExpression | this_UnaryExpression_6= ruleUnaryExpression ) otherlv_7= ')' ) ) ;
    public final EObject rulePrimaryExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_4=null;
        Token otherlv_7=null;
        EObject this_Cast_0 = null;

        EObject this_ProcedureCall_1 = null;

        EObject this_Call_2 = null;

        EObject this_NewDataStruct_3 = null;

        EObject this_OrExpression_5 = null;

        EObject this_UnaryExpression_6 = null;



        	enterRule();

        try {
            // InternalFCL.g:5886:2: ( (this_Cast_0= ruleCast | this_ProcedureCall_1= ruleProcedureCall | this_Call_2= ruleCall | this_NewDataStruct_3= ruleNewDataStruct | (otherlv_4= '(' (this_OrExpression_5= ruleOrExpression | this_UnaryExpression_6= ruleUnaryExpression ) otherlv_7= ')' ) ) )
            // InternalFCL.g:5887:2: (this_Cast_0= ruleCast | this_ProcedureCall_1= ruleProcedureCall | this_Call_2= ruleCall | this_NewDataStruct_3= ruleNewDataStruct | (otherlv_4= '(' (this_OrExpression_5= ruleOrExpression | this_UnaryExpression_6= ruleUnaryExpression ) otherlv_7= ')' ) )
            {
            // InternalFCL.g:5887:2: (this_Cast_0= ruleCast | this_ProcedureCall_1= ruleProcedureCall | this_Call_2= ruleCall | this_NewDataStruct_3= ruleNewDataStruct | (otherlv_4= '(' (this_OrExpression_5= ruleOrExpression | this_UnaryExpression_6= ruleUnaryExpression ) otherlv_7= ')' ) )
            int alt105=5;
            alt105 = dfa105.predict(input);
            switch (alt105) {
                case 1 :
                    // InternalFCL.g:5888:3: this_Cast_0= ruleCast
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getPrimaryExpressionAccess().getCastParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Cast_0=ruleCast();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Cast_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalFCL.g:5897:3: this_ProcedureCall_1= ruleProcedureCall
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getPrimaryExpressionAccess().getProcedureCallParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ProcedureCall_1=ruleProcedureCall();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ProcedureCall_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalFCL.g:5906:3: this_Call_2= ruleCall
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getPrimaryExpressionAccess().getCallParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Call_2=ruleCall();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Call_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalFCL.g:5915:3: this_NewDataStruct_3= ruleNewDataStruct
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getPrimaryExpressionAccess().getNewDataStructParserRuleCall_3());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_NewDataStruct_3=ruleNewDataStruct();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_NewDataStruct_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalFCL.g:5924:3: (otherlv_4= '(' (this_OrExpression_5= ruleOrExpression | this_UnaryExpression_6= ruleUnaryExpression ) otherlv_7= ')' )
                    {
                    // InternalFCL.g:5924:3: (otherlv_4= '(' (this_OrExpression_5= ruleOrExpression | this_UnaryExpression_6= ruleUnaryExpression ) otherlv_7= ')' )
                    // InternalFCL.g:5925:4: otherlv_4= '(' (this_OrExpression_5= ruleOrExpression | this_UnaryExpression_6= ruleUnaryExpression ) otherlv_7= ')'
                    {
                    otherlv_4=(Token)match(input,24,FOLLOW_27); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getPrimaryExpressionAccess().getLeftParenthesisKeyword_4_0());
                      			
                    }
                    // InternalFCL.g:5929:4: (this_OrExpression_5= ruleOrExpression | this_UnaryExpression_6= ruleUnaryExpression )
                    int alt104=2;
                    int LA104_0 = input.LA(1);

                    if ( ((LA104_0>=RULE_ID && LA104_0<=RULE_INT)||LA104_0==24||LA104_0==69||LA104_0==72||LA104_0==74||(LA104_0>=88 && LA104_0<=90)) ) {
                        alt104=1;
                    }
                    else if ( (LA104_0==93||LA104_0==97) ) {
                        alt104=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 104, 0, input);

                        throw nvae;
                    }
                    switch (alt104) {
                        case 1 :
                            // InternalFCL.g:5930:5: this_OrExpression_5= ruleOrExpression
                            {
                            if ( state.backtracking==0 ) {

                              					newCompositeNode(grammarAccess.getPrimaryExpressionAccess().getOrExpressionParserRuleCall_4_1_0());
                              				
                            }
                            pushFollow(FOLLOW_80);
                            this_OrExpression_5=ruleOrExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					current = this_OrExpression_5;
                              					afterParserOrEnumRuleCall();
                              				
                            }

                            }
                            break;
                        case 2 :
                            // InternalFCL.g:5939:5: this_UnaryExpression_6= ruleUnaryExpression
                            {
                            if ( state.backtracking==0 ) {

                              					newCompositeNode(grammarAccess.getPrimaryExpressionAccess().getUnaryExpressionParserRuleCall_4_1_1());
                              				
                            }
                            pushFollow(FOLLOW_80);
                            this_UnaryExpression_6=ruleUnaryExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					current = this_UnaryExpression_6;
                              					afterParserOrEnumRuleCall();
                              				
                            }

                            }
                            break;

                    }

                    otherlv_7=(Token)match(input,26,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_7, grammarAccess.getPrimaryExpressionAccess().getRightParenthesisKeyword_4_2());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimaryExpression"


    // $ANTLR start "entryRuleFCLProcedureReturn"
    // InternalFCL.g:5957:1: entryRuleFCLProcedureReturn returns [EObject current=null] : iv_ruleFCLProcedureReturn= ruleFCLProcedureReturn EOF ;
    public final EObject entryRuleFCLProcedureReturn() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFCLProcedureReturn = null;


        try {
            // InternalFCL.g:5957:59: (iv_ruleFCLProcedureReturn= ruleFCLProcedureReturn EOF )
            // InternalFCL.g:5958:2: iv_ruleFCLProcedureReturn= ruleFCLProcedureReturn EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFCLProcedureReturnRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFCLProcedureReturn=ruleFCLProcedureReturn();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFCLProcedureReturn; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFCLProcedureReturn"


    // $ANTLR start "ruleFCLProcedureReturn"
    // InternalFCL.g:5964:1: ruleFCLProcedureReturn returns [EObject current=null] : ( () otherlv_1= 'return' ( (lv_expression_2_0= ruleExpression ) ) ) ;
    public final EObject ruleFCLProcedureReturn() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_expression_2_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:5970:2: ( ( () otherlv_1= 'return' ( (lv_expression_2_0= ruleExpression ) ) ) )
            // InternalFCL.g:5971:2: ( () otherlv_1= 'return' ( (lv_expression_2_0= ruleExpression ) ) )
            {
            // InternalFCL.g:5971:2: ( () otherlv_1= 'return' ( (lv_expression_2_0= ruleExpression ) ) )
            // InternalFCL.g:5972:3: () otherlv_1= 'return' ( (lv_expression_2_0= ruleExpression ) )
            {
            // InternalFCL.g:5972:3: ()
            // InternalFCL.g:5973:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getFCLProcedureReturnAccess().getFCLProcedureReturnAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,75,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getFCLProcedureReturnAccess().getReturnKeyword_1());
              		
            }
            // InternalFCL.g:5983:3: ( (lv_expression_2_0= ruleExpression ) )
            // InternalFCL.g:5984:4: (lv_expression_2_0= ruleExpression )
            {
            // InternalFCL.g:5984:4: (lv_expression_2_0= ruleExpression )
            // InternalFCL.g:5985:5: lv_expression_2_0= ruleExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getFCLProcedureReturnAccess().getExpressionExpressionParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_expression_2_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getFCLProcedureReturnRule());
              					}
              					set(
              						current,
              						"expression",
              						lv_expression_2_0,
              						"fr.inria.glose.fcl.FCL.Expression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFCLProcedureReturn"


    // $ANTLR start "entryRuleDataStructType"
    // InternalFCL.g:6006:1: entryRuleDataStructType returns [EObject current=null] : iv_ruleDataStructType= ruleDataStructType EOF ;
    public final EObject entryRuleDataStructType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataStructType = null;


        try {
            // InternalFCL.g:6006:55: (iv_ruleDataStructType= ruleDataStructType EOF )
            // InternalFCL.g:6007:2: iv_ruleDataStructType= ruleDataStructType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDataStructTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDataStructType=ruleDataStructType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDataStructType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataStructType"


    // $ANTLR start "ruleDataStructType"
    // InternalFCL.g:6013:1: ruleDataStructType returns [EObject current=null] : ( () otherlv_1= 'DataStructType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'properties' otherlv_5= '{' ( (lv_properties_6_0= ruleDataStructProperty ) ) (otherlv_7= ',' ( (lv_properties_8_0= ruleDataStructProperty ) ) )* otherlv_9= '}' )? otherlv_10= '}' ) ;
    public final EObject ruleDataStructType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        EObject lv_properties_6_0 = null;

        EObject lv_properties_8_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:6019:2: ( ( () otherlv_1= 'DataStructType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'properties' otherlv_5= '{' ( (lv_properties_6_0= ruleDataStructProperty ) ) (otherlv_7= ',' ( (lv_properties_8_0= ruleDataStructProperty ) ) )* otherlv_9= '}' )? otherlv_10= '}' ) )
            // InternalFCL.g:6020:2: ( () otherlv_1= 'DataStructType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'properties' otherlv_5= '{' ( (lv_properties_6_0= ruleDataStructProperty ) ) (otherlv_7= ',' ( (lv_properties_8_0= ruleDataStructProperty ) ) )* otherlv_9= '}' )? otherlv_10= '}' )
            {
            // InternalFCL.g:6020:2: ( () otherlv_1= 'DataStructType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'properties' otherlv_5= '{' ( (lv_properties_6_0= ruleDataStructProperty ) ) (otherlv_7= ',' ( (lv_properties_8_0= ruleDataStructProperty ) ) )* otherlv_9= '}' )? otherlv_10= '}' )
            // InternalFCL.g:6021:3: () otherlv_1= 'DataStructType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'properties' otherlv_5= '{' ( (lv_properties_6_0= ruleDataStructProperty ) ) (otherlv_7= ',' ( (lv_properties_8_0= ruleDataStructProperty ) ) )* otherlv_9= '}' )? otherlv_10= '}'
            {
            // InternalFCL.g:6021:3: ()
            // InternalFCL.g:6022:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getDataStructTypeAccess().getDataStructTypeAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,76,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getDataStructTypeAccess().getDataStructTypeKeyword_1());
              		
            }
            // InternalFCL.g:6032:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalFCL.g:6033:4: (lv_name_2_0= RULE_ID )
            {
            // InternalFCL.g:6033:4: (lv_name_2_0= RULE_ID )
            // InternalFCL.g:6034:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getDataStructTypeAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getDataStructTypeRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_89); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getDataStructTypeAccess().getLeftCurlyBracketKeyword_3());
              		
            }
            // InternalFCL.g:6054:3: (otherlv_4= 'properties' otherlv_5= '{' ( (lv_properties_6_0= ruleDataStructProperty ) ) (otherlv_7= ',' ( (lv_properties_8_0= ruleDataStructProperty ) ) )* otherlv_9= '}' )?
            int alt107=2;
            int LA107_0 = input.LA(1);

            if ( (LA107_0==77) ) {
                alt107=1;
            }
            switch (alt107) {
                case 1 :
                    // InternalFCL.g:6055:4: otherlv_4= 'properties' otherlv_5= '{' ( (lv_properties_6_0= ruleDataStructProperty ) ) (otherlv_7= ',' ( (lv_properties_8_0= ruleDataStructProperty ) ) )* otherlv_9= '}'
                    {
                    otherlv_4=(Token)match(input,77,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getDataStructTypeAccess().getPropertiesKeyword_4_0());
                      			
                    }
                    otherlv_5=(Token)match(input,12,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_5, grammarAccess.getDataStructTypeAccess().getLeftCurlyBracketKeyword_4_1());
                      			
                    }
                    // InternalFCL.g:6063:4: ( (lv_properties_6_0= ruleDataStructProperty ) )
                    // InternalFCL.g:6064:5: (lv_properties_6_0= ruleDataStructProperty )
                    {
                    // InternalFCL.g:6064:5: (lv_properties_6_0= ruleDataStructProperty )
                    // InternalFCL.g:6065:6: lv_properties_6_0= ruleDataStructProperty
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getDataStructTypeAccess().getPropertiesDataStructPropertyParserRuleCall_4_2_0());
                      					
                    }
                    pushFollow(FOLLOW_79);
                    lv_properties_6_0=ruleDataStructProperty();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getDataStructTypeRule());
                      						}
                      						add(
                      							current,
                      							"properties",
                      							lv_properties_6_0,
                      							"fr.inria.glose.fcl.FCL.DataStructProperty");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalFCL.g:6082:4: (otherlv_7= ',' ( (lv_properties_8_0= ruleDataStructProperty ) ) )*
                    loop106:
                    do {
                        int alt106=2;
                        int LA106_0 = input.LA(1);

                        if ( (LA106_0==25) ) {
                            alt106=1;
                        }


                        switch (alt106) {
                    	case 1 :
                    	    // InternalFCL.g:6083:5: otherlv_7= ',' ( (lv_properties_8_0= ruleDataStructProperty ) )
                    	    {
                    	    otherlv_7=(Token)match(input,25,FOLLOW_3); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_7, grammarAccess.getDataStructTypeAccess().getCommaKeyword_4_3_0());
                    	      				
                    	    }
                    	    // InternalFCL.g:6087:5: ( (lv_properties_8_0= ruleDataStructProperty ) )
                    	    // InternalFCL.g:6088:6: (lv_properties_8_0= ruleDataStructProperty )
                    	    {
                    	    // InternalFCL.g:6088:6: (lv_properties_8_0= ruleDataStructProperty )
                    	    // InternalFCL.g:6089:7: lv_properties_8_0= ruleDataStructProperty
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getDataStructTypeAccess().getPropertiesDataStructPropertyParserRuleCall_4_3_1_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_79);
                    	    lv_properties_8_0=ruleDataStructProperty();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getDataStructTypeRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"properties",
                    	      								lv_properties_8_0,
                    	      								"fr.inria.glose.fcl.FCL.DataStructProperty");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop106;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,14,FOLLOW_16); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_9, grammarAccess.getDataStructTypeAccess().getRightCurlyBracketKeyword_4_4());
                      			
                    }

                    }
                    break;

            }

            otherlv_10=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_10, grammarAccess.getDataStructTypeAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataStructType"


    // $ANTLR start "entryRuleDataStructProperty"
    // InternalFCL.g:6120:1: entryRuleDataStructProperty returns [EObject current=null] : iv_ruleDataStructProperty= ruleDataStructProperty EOF ;
    public final EObject entryRuleDataStructProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataStructProperty = null;


        try {
            // InternalFCL.g:6120:59: (iv_ruleDataStructProperty= ruleDataStructProperty EOF )
            // InternalFCL.g:6121:2: iv_ruleDataStructProperty= ruleDataStructProperty EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDataStructPropertyRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDataStructProperty=ruleDataStructProperty();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDataStructProperty; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataStructProperty"


    // $ANTLR start "ruleDataStructProperty"
    // InternalFCL.g:6127:1: ruleDataStructProperty returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleDataStructProperty() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalFCL.g:6133:2: ( ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) ) )
            // InternalFCL.g:6134:2: ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) )
            {
            // InternalFCL.g:6134:2: ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( ( ruleQualifiedName ) ) )
            // InternalFCL.g:6135:3: () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( ( ruleQualifiedName ) )
            {
            // InternalFCL.g:6135:3: ()
            // InternalFCL.g:6136:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getDataStructPropertyAccess().getDataStructPropertyAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:6142:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalFCL.g:6143:4: (lv_name_1_0= RULE_ID )
            {
            // InternalFCL.g:6143:4: (lv_name_1_0= RULE_ID )
            // InternalFCL.g:6144:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_90); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getDataStructPropertyAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getDataStructPropertyRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,34,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getDataStructPropertyAccess().getColonKeyword_2());
              		
            }
            // InternalFCL.g:6164:3: ( ( ruleQualifiedName ) )
            // InternalFCL.g:6165:4: ( ruleQualifiedName )
            {
            // InternalFCL.g:6165:4: ( ruleQualifiedName )
            // InternalFCL.g:6166:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getDataStructPropertyRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getDataStructPropertyAccess().getTypeDataTypeCrossReference_3_0());
              				
            }
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataStructProperty"


    // $ANTLR start "entryRuleValueType_Impl"
    // InternalFCL.g:6184:1: entryRuleValueType_Impl returns [EObject current=null] : iv_ruleValueType_Impl= ruleValueType_Impl EOF ;
    public final EObject entryRuleValueType_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValueType_Impl = null;


        try {
            // InternalFCL.g:6184:55: (iv_ruleValueType_Impl= ruleValueType_Impl EOF )
            // InternalFCL.g:6185:2: iv_ruleValueType_Impl= ruleValueType_Impl EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getValueType_ImplRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleValueType_Impl=ruleValueType_Impl();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleValueType_Impl; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValueType_Impl"


    // $ANTLR start "ruleValueType_Impl"
    // InternalFCL.g:6191:1: ruleValueType_Impl returns [EObject current=null] : ( () otherlv_1= 'ValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? otherlv_8= '}' ) ;
    public final EObject ruleValueType_Impl() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_unit_5_0 = null;

        AntlrDatatypeRuleToken lv_description_7_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:6197:2: ( ( () otherlv_1= 'ValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? otherlv_8= '}' ) )
            // InternalFCL.g:6198:2: ( () otherlv_1= 'ValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? otherlv_8= '}' )
            {
            // InternalFCL.g:6198:2: ( () otherlv_1= 'ValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? otherlv_8= '}' )
            // InternalFCL.g:6199:3: () otherlv_1= 'ValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? otherlv_8= '}'
            {
            // InternalFCL.g:6199:3: ()
            // InternalFCL.g:6200:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getValueType_ImplAccess().getValueTypeAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,78,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getValueType_ImplAccess().getValueTypeKeyword_1());
              		
            }
            // InternalFCL.g:6210:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalFCL.g:6211:4: (lv_name_2_0= RULE_ID )
            {
            // InternalFCL.g:6211:4: (lv_name_2_0= RULE_ID )
            // InternalFCL.g:6212:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getValueType_ImplAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getValueType_ImplRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_91); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getValueType_ImplAccess().getLeftCurlyBracketKeyword_3());
              		
            }
            // InternalFCL.g:6232:3: (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )?
            int alt108=2;
            int LA108_0 = input.LA(1);

            if ( (LA108_0==79) ) {
                alt108=1;
            }
            switch (alt108) {
                case 1 :
                    // InternalFCL.g:6233:4: otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,79,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getValueType_ImplAccess().getUnitKeyword_4_0());
                      			
                    }
                    // InternalFCL.g:6237:4: ( (lv_unit_5_0= ruleEString ) )
                    // InternalFCL.g:6238:5: (lv_unit_5_0= ruleEString )
                    {
                    // InternalFCL.g:6238:5: (lv_unit_5_0= ruleEString )
                    // InternalFCL.g:6239:6: lv_unit_5_0= ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getValueType_ImplAccess().getUnitEStringParserRuleCall_4_1_0());
                      					
                    }
                    pushFollow(FOLLOW_92);
                    lv_unit_5_0=ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getValueType_ImplRule());
                      						}
                      						set(
                      							current,
                      							"unit",
                      							lv_unit_5_0,
                      							"fr.inria.glose.fcl.FCL.EString");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:6257:3: (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )?
            int alt109=2;
            int LA109_0 = input.LA(1);

            if ( (LA109_0==80) ) {
                alt109=1;
            }
            switch (alt109) {
                case 1 :
                    // InternalFCL.g:6258:4: otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,80,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getValueType_ImplAccess().getDescriptionKeyword_5_0());
                      			
                    }
                    // InternalFCL.g:6262:4: ( (lv_description_7_0= ruleEString ) )
                    // InternalFCL.g:6263:5: (lv_description_7_0= ruleEString )
                    {
                    // InternalFCL.g:6263:5: (lv_description_7_0= ruleEString )
                    // InternalFCL.g:6264:6: lv_description_7_0= ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getValueType_ImplAccess().getDescriptionEStringParserRuleCall_5_1_0());
                      					
                    }
                    pushFollow(FOLLOW_16);
                    lv_description_7_0=ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getValueType_ImplRule());
                      						}
                      						set(
                      							current,
                      							"description",
                      							lv_description_7_0,
                      							"fr.inria.glose.fcl.FCL.EString");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getValueType_ImplAccess().getRightCurlyBracketKeyword_6());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValueType_Impl"


    // $ANTLR start "entryRuleIntegerValueType"
    // InternalFCL.g:6290:1: entryRuleIntegerValueType returns [EObject current=null] : iv_ruleIntegerValueType= ruleIntegerValueType EOF ;
    public final EObject entryRuleIntegerValueType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerValueType = null;


        try {
            // InternalFCL.g:6290:57: (iv_ruleIntegerValueType= ruleIntegerValueType EOF )
            // InternalFCL.g:6291:2: iv_ruleIntegerValueType= ruleIntegerValueType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerValueTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleIntegerValueType=ruleIntegerValueType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerValueType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerValueType"


    // $ANTLR start "ruleIntegerValueType"
    // InternalFCL.g:6297:1: ruleIntegerValueType returns [EObject current=null] : ( () otherlv_1= 'IntegerValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= 'min' ( (lv_min_9_0= ruleEInt ) ) )? (otherlv_10= 'max' ( (lv_max_11_0= ruleEInt ) ) )? otherlv_12= '}' ) ;
    public final EObject ruleIntegerValueType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        AntlrDatatypeRuleToken lv_unit_5_0 = null;

        AntlrDatatypeRuleToken lv_description_7_0 = null;

        AntlrDatatypeRuleToken lv_min_9_0 = null;

        AntlrDatatypeRuleToken lv_max_11_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:6303:2: ( ( () otherlv_1= 'IntegerValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= 'min' ( (lv_min_9_0= ruleEInt ) ) )? (otherlv_10= 'max' ( (lv_max_11_0= ruleEInt ) ) )? otherlv_12= '}' ) )
            // InternalFCL.g:6304:2: ( () otherlv_1= 'IntegerValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= 'min' ( (lv_min_9_0= ruleEInt ) ) )? (otherlv_10= 'max' ( (lv_max_11_0= ruleEInt ) ) )? otherlv_12= '}' )
            {
            // InternalFCL.g:6304:2: ( () otherlv_1= 'IntegerValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= 'min' ( (lv_min_9_0= ruleEInt ) ) )? (otherlv_10= 'max' ( (lv_max_11_0= ruleEInt ) ) )? otherlv_12= '}' )
            // InternalFCL.g:6305:3: () otherlv_1= 'IntegerValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= 'min' ( (lv_min_9_0= ruleEInt ) ) )? (otherlv_10= 'max' ( (lv_max_11_0= ruleEInt ) ) )? otherlv_12= '}'
            {
            // InternalFCL.g:6305:3: ()
            // InternalFCL.g:6306:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getIntegerValueTypeAccess().getIntegerValueTypeAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,81,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getIntegerValueTypeAccess().getIntegerValueTypeKeyword_1());
              		
            }
            // InternalFCL.g:6316:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalFCL.g:6317:4: (lv_name_2_0= RULE_ID )
            {
            // InternalFCL.g:6317:4: (lv_name_2_0= RULE_ID )
            // InternalFCL.g:6318:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getIntegerValueTypeAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getIntegerValueTypeRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_93); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getIntegerValueTypeAccess().getLeftCurlyBracketKeyword_3());
              		
            }
            // InternalFCL.g:6338:3: (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )?
            int alt110=2;
            int LA110_0 = input.LA(1);

            if ( (LA110_0==79) ) {
                alt110=1;
            }
            switch (alt110) {
                case 1 :
                    // InternalFCL.g:6339:4: otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,79,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getIntegerValueTypeAccess().getUnitKeyword_4_0());
                      			
                    }
                    // InternalFCL.g:6343:4: ( (lv_unit_5_0= ruleEString ) )
                    // InternalFCL.g:6344:5: (lv_unit_5_0= ruleEString )
                    {
                    // InternalFCL.g:6344:5: (lv_unit_5_0= ruleEString )
                    // InternalFCL.g:6345:6: lv_unit_5_0= ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getIntegerValueTypeAccess().getUnitEStringParserRuleCall_4_1_0());
                      					
                    }
                    pushFollow(FOLLOW_94);
                    lv_unit_5_0=ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getIntegerValueTypeRule());
                      						}
                      						set(
                      							current,
                      							"unit",
                      							lv_unit_5_0,
                      							"fr.inria.glose.fcl.FCL.EString");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:6363:3: (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )?
            int alt111=2;
            int LA111_0 = input.LA(1);

            if ( (LA111_0==80) ) {
                alt111=1;
            }
            switch (alt111) {
                case 1 :
                    // InternalFCL.g:6364:4: otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,80,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getIntegerValueTypeAccess().getDescriptionKeyword_5_0());
                      			
                    }
                    // InternalFCL.g:6368:4: ( (lv_description_7_0= ruleEString ) )
                    // InternalFCL.g:6369:5: (lv_description_7_0= ruleEString )
                    {
                    // InternalFCL.g:6369:5: (lv_description_7_0= ruleEString )
                    // InternalFCL.g:6370:6: lv_description_7_0= ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getIntegerValueTypeAccess().getDescriptionEStringParserRuleCall_5_1_0());
                      					
                    }
                    pushFollow(FOLLOW_95);
                    lv_description_7_0=ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getIntegerValueTypeRule());
                      						}
                      						set(
                      							current,
                      							"description",
                      							lv_description_7_0,
                      							"fr.inria.glose.fcl.FCL.EString");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:6388:3: (otherlv_8= 'min' ( (lv_min_9_0= ruleEInt ) ) )?
            int alt112=2;
            int LA112_0 = input.LA(1);

            if ( (LA112_0==82) ) {
                alt112=1;
            }
            switch (alt112) {
                case 1 :
                    // InternalFCL.g:6389:4: otherlv_8= 'min' ( (lv_min_9_0= ruleEInt ) )
                    {
                    otherlv_8=(Token)match(input,82,FOLLOW_96); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_8, grammarAccess.getIntegerValueTypeAccess().getMinKeyword_6_0());
                      			
                    }
                    // InternalFCL.g:6393:4: ( (lv_min_9_0= ruleEInt ) )
                    // InternalFCL.g:6394:5: (lv_min_9_0= ruleEInt )
                    {
                    // InternalFCL.g:6394:5: (lv_min_9_0= ruleEInt )
                    // InternalFCL.g:6395:6: lv_min_9_0= ruleEInt
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getIntegerValueTypeAccess().getMinEIntParserRuleCall_6_1_0());
                      					
                    }
                    pushFollow(FOLLOW_97);
                    lv_min_9_0=ruleEInt();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getIntegerValueTypeRule());
                      						}
                      						set(
                      							current,
                      							"min",
                      							lv_min_9_0,
                      							"fr.inria.glose.fcl.FCL.EInt");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:6413:3: (otherlv_10= 'max' ( (lv_max_11_0= ruleEInt ) ) )?
            int alt113=2;
            int LA113_0 = input.LA(1);

            if ( (LA113_0==83) ) {
                alt113=1;
            }
            switch (alt113) {
                case 1 :
                    // InternalFCL.g:6414:4: otherlv_10= 'max' ( (lv_max_11_0= ruleEInt ) )
                    {
                    otherlv_10=(Token)match(input,83,FOLLOW_96); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_10, grammarAccess.getIntegerValueTypeAccess().getMaxKeyword_7_0());
                      			
                    }
                    // InternalFCL.g:6418:4: ( (lv_max_11_0= ruleEInt ) )
                    // InternalFCL.g:6419:5: (lv_max_11_0= ruleEInt )
                    {
                    // InternalFCL.g:6419:5: (lv_max_11_0= ruleEInt )
                    // InternalFCL.g:6420:6: lv_max_11_0= ruleEInt
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getIntegerValueTypeAccess().getMaxEIntParserRuleCall_7_1_0());
                      					
                    }
                    pushFollow(FOLLOW_16);
                    lv_max_11_0=ruleEInt();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getIntegerValueTypeRule());
                      						}
                      						set(
                      							current,
                      							"max",
                      							lv_max_11_0,
                      							"fr.inria.glose.fcl.FCL.EInt");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_12=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_12, grammarAccess.getIntegerValueTypeAccess().getRightCurlyBracketKeyword_8());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerValueType"


    // $ANTLR start "entryRuleFloatValueType"
    // InternalFCL.g:6446:1: entryRuleFloatValueType returns [EObject current=null] : iv_ruleFloatValueType= ruleFloatValueType EOF ;
    public final EObject entryRuleFloatValueType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFloatValueType = null;


        try {
            // InternalFCL.g:6446:55: (iv_ruleFloatValueType= ruleFloatValueType EOF )
            // InternalFCL.g:6447:2: iv_ruleFloatValueType= ruleFloatValueType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFloatValueTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFloatValueType=ruleFloatValueType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFloatValueType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFloatValueType"


    // $ANTLR start "ruleFloatValueType"
    // InternalFCL.g:6453:1: ruleFloatValueType returns [EObject current=null] : ( () otherlv_1= 'FloatValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= 'min' ( (lv_min_9_0= ruleEDouble ) ) )? (otherlv_10= 'max' ( (lv_max_11_0= ruleEDouble ) ) )? otherlv_12= '}' ) ;
    public final EObject ruleFloatValueType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        AntlrDatatypeRuleToken lv_unit_5_0 = null;

        AntlrDatatypeRuleToken lv_description_7_0 = null;

        AntlrDatatypeRuleToken lv_min_9_0 = null;

        AntlrDatatypeRuleToken lv_max_11_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:6459:2: ( ( () otherlv_1= 'FloatValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= 'min' ( (lv_min_9_0= ruleEDouble ) ) )? (otherlv_10= 'max' ( (lv_max_11_0= ruleEDouble ) ) )? otherlv_12= '}' ) )
            // InternalFCL.g:6460:2: ( () otherlv_1= 'FloatValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= 'min' ( (lv_min_9_0= ruleEDouble ) ) )? (otherlv_10= 'max' ( (lv_max_11_0= ruleEDouble ) ) )? otherlv_12= '}' )
            {
            // InternalFCL.g:6460:2: ( () otherlv_1= 'FloatValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= 'min' ( (lv_min_9_0= ruleEDouble ) ) )? (otherlv_10= 'max' ( (lv_max_11_0= ruleEDouble ) ) )? otherlv_12= '}' )
            // InternalFCL.g:6461:3: () otherlv_1= 'FloatValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= 'min' ( (lv_min_9_0= ruleEDouble ) ) )? (otherlv_10= 'max' ( (lv_max_11_0= ruleEDouble ) ) )? otherlv_12= '}'
            {
            // InternalFCL.g:6461:3: ()
            // InternalFCL.g:6462:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getFloatValueTypeAccess().getFloatValueTypeAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,84,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getFloatValueTypeAccess().getFloatValueTypeKeyword_1());
              		
            }
            // InternalFCL.g:6472:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalFCL.g:6473:4: (lv_name_2_0= RULE_ID )
            {
            // InternalFCL.g:6473:4: (lv_name_2_0= RULE_ID )
            // InternalFCL.g:6474:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getFloatValueTypeAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getFloatValueTypeRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_93); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getFloatValueTypeAccess().getLeftCurlyBracketKeyword_3());
              		
            }
            // InternalFCL.g:6494:3: (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )?
            int alt114=2;
            int LA114_0 = input.LA(1);

            if ( (LA114_0==79) ) {
                alt114=1;
            }
            switch (alt114) {
                case 1 :
                    // InternalFCL.g:6495:4: otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,79,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getFloatValueTypeAccess().getUnitKeyword_4_0());
                      			
                    }
                    // InternalFCL.g:6499:4: ( (lv_unit_5_0= ruleEString ) )
                    // InternalFCL.g:6500:5: (lv_unit_5_0= ruleEString )
                    {
                    // InternalFCL.g:6500:5: (lv_unit_5_0= ruleEString )
                    // InternalFCL.g:6501:6: lv_unit_5_0= ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getFloatValueTypeAccess().getUnitEStringParserRuleCall_4_1_0());
                      					
                    }
                    pushFollow(FOLLOW_94);
                    lv_unit_5_0=ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getFloatValueTypeRule());
                      						}
                      						set(
                      							current,
                      							"unit",
                      							lv_unit_5_0,
                      							"fr.inria.glose.fcl.FCL.EString");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:6519:3: (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )?
            int alt115=2;
            int LA115_0 = input.LA(1);

            if ( (LA115_0==80) ) {
                alt115=1;
            }
            switch (alt115) {
                case 1 :
                    // InternalFCL.g:6520:4: otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,80,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getFloatValueTypeAccess().getDescriptionKeyword_5_0());
                      			
                    }
                    // InternalFCL.g:6524:4: ( (lv_description_7_0= ruleEString ) )
                    // InternalFCL.g:6525:5: (lv_description_7_0= ruleEString )
                    {
                    // InternalFCL.g:6525:5: (lv_description_7_0= ruleEString )
                    // InternalFCL.g:6526:6: lv_description_7_0= ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getFloatValueTypeAccess().getDescriptionEStringParserRuleCall_5_1_0());
                      					
                    }
                    pushFollow(FOLLOW_95);
                    lv_description_7_0=ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getFloatValueTypeRule());
                      						}
                      						set(
                      							current,
                      							"description",
                      							lv_description_7_0,
                      							"fr.inria.glose.fcl.FCL.EString");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:6544:3: (otherlv_8= 'min' ( (lv_min_9_0= ruleEDouble ) ) )?
            int alt116=2;
            int LA116_0 = input.LA(1);

            if ( (LA116_0==82) ) {
                alt116=1;
            }
            switch (alt116) {
                case 1 :
                    // InternalFCL.g:6545:4: otherlv_8= 'min' ( (lv_min_9_0= ruleEDouble ) )
                    {
                    otherlv_8=(Token)match(input,82,FOLLOW_98); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_8, grammarAccess.getFloatValueTypeAccess().getMinKeyword_6_0());
                      			
                    }
                    // InternalFCL.g:6549:4: ( (lv_min_9_0= ruleEDouble ) )
                    // InternalFCL.g:6550:5: (lv_min_9_0= ruleEDouble )
                    {
                    // InternalFCL.g:6550:5: (lv_min_9_0= ruleEDouble )
                    // InternalFCL.g:6551:6: lv_min_9_0= ruleEDouble
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getFloatValueTypeAccess().getMinEDoubleParserRuleCall_6_1_0());
                      					
                    }
                    pushFollow(FOLLOW_97);
                    lv_min_9_0=ruleEDouble();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getFloatValueTypeRule());
                      						}
                      						set(
                      							current,
                      							"min",
                      							lv_min_9_0,
                      							"fr.inria.glose.fcl.FCL.EDouble");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:6569:3: (otherlv_10= 'max' ( (lv_max_11_0= ruleEDouble ) ) )?
            int alt117=2;
            int LA117_0 = input.LA(1);

            if ( (LA117_0==83) ) {
                alt117=1;
            }
            switch (alt117) {
                case 1 :
                    // InternalFCL.g:6570:4: otherlv_10= 'max' ( (lv_max_11_0= ruleEDouble ) )
                    {
                    otherlv_10=(Token)match(input,83,FOLLOW_98); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_10, grammarAccess.getFloatValueTypeAccess().getMaxKeyword_7_0());
                      			
                    }
                    // InternalFCL.g:6574:4: ( (lv_max_11_0= ruleEDouble ) )
                    // InternalFCL.g:6575:5: (lv_max_11_0= ruleEDouble )
                    {
                    // InternalFCL.g:6575:5: (lv_max_11_0= ruleEDouble )
                    // InternalFCL.g:6576:6: lv_max_11_0= ruleEDouble
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getFloatValueTypeAccess().getMaxEDoubleParserRuleCall_7_1_0());
                      					
                    }
                    pushFollow(FOLLOW_16);
                    lv_max_11_0=ruleEDouble();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getFloatValueTypeRule());
                      						}
                      						set(
                      							current,
                      							"max",
                      							lv_max_11_0,
                      							"fr.inria.glose.fcl.FCL.EDouble");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_12=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_12, grammarAccess.getFloatValueTypeAccess().getRightCurlyBracketKeyword_8());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFloatValueType"


    // $ANTLR start "entryRuleBooleanValueType"
    // InternalFCL.g:6602:1: entryRuleBooleanValueType returns [EObject current=null] : iv_ruleBooleanValueType= ruleBooleanValueType EOF ;
    public final EObject entryRuleBooleanValueType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanValueType = null;


        try {
            // InternalFCL.g:6602:57: (iv_ruleBooleanValueType= ruleBooleanValueType EOF )
            // InternalFCL.g:6603:2: iv_ruleBooleanValueType= ruleBooleanValueType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanValueTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBooleanValueType=ruleBooleanValueType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanValueType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanValueType"


    // $ANTLR start "ruleBooleanValueType"
    // InternalFCL.g:6609:1: ruleBooleanValueType returns [EObject current=null] : ( () otherlv_1= 'BooleanValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? otherlv_8= '}' ) ;
    public final EObject ruleBooleanValueType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_unit_5_0 = null;

        AntlrDatatypeRuleToken lv_description_7_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:6615:2: ( ( () otherlv_1= 'BooleanValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? otherlv_8= '}' ) )
            // InternalFCL.g:6616:2: ( () otherlv_1= 'BooleanValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? otherlv_8= '}' )
            {
            // InternalFCL.g:6616:2: ( () otherlv_1= 'BooleanValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? otherlv_8= '}' )
            // InternalFCL.g:6617:3: () otherlv_1= 'BooleanValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? otherlv_8= '}'
            {
            // InternalFCL.g:6617:3: ()
            // InternalFCL.g:6618:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getBooleanValueTypeAccess().getBooleanValueTypeAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,85,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getBooleanValueTypeAccess().getBooleanValueTypeKeyword_1());
              		
            }
            // InternalFCL.g:6628:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalFCL.g:6629:4: (lv_name_2_0= RULE_ID )
            {
            // InternalFCL.g:6629:4: (lv_name_2_0= RULE_ID )
            // InternalFCL.g:6630:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getBooleanValueTypeAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getBooleanValueTypeRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_91); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getBooleanValueTypeAccess().getLeftCurlyBracketKeyword_3());
              		
            }
            // InternalFCL.g:6650:3: (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )?
            int alt118=2;
            int LA118_0 = input.LA(1);

            if ( (LA118_0==79) ) {
                alt118=1;
            }
            switch (alt118) {
                case 1 :
                    // InternalFCL.g:6651:4: otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,79,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getBooleanValueTypeAccess().getUnitKeyword_4_0());
                      			
                    }
                    // InternalFCL.g:6655:4: ( (lv_unit_5_0= ruleEString ) )
                    // InternalFCL.g:6656:5: (lv_unit_5_0= ruleEString )
                    {
                    // InternalFCL.g:6656:5: (lv_unit_5_0= ruleEString )
                    // InternalFCL.g:6657:6: lv_unit_5_0= ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getBooleanValueTypeAccess().getUnitEStringParserRuleCall_4_1_0());
                      					
                    }
                    pushFollow(FOLLOW_92);
                    lv_unit_5_0=ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getBooleanValueTypeRule());
                      						}
                      						set(
                      							current,
                      							"unit",
                      							lv_unit_5_0,
                      							"fr.inria.glose.fcl.FCL.EString");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:6675:3: (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )?
            int alt119=2;
            int LA119_0 = input.LA(1);

            if ( (LA119_0==80) ) {
                alt119=1;
            }
            switch (alt119) {
                case 1 :
                    // InternalFCL.g:6676:4: otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,80,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getBooleanValueTypeAccess().getDescriptionKeyword_5_0());
                      			
                    }
                    // InternalFCL.g:6680:4: ( (lv_description_7_0= ruleEString ) )
                    // InternalFCL.g:6681:5: (lv_description_7_0= ruleEString )
                    {
                    // InternalFCL.g:6681:5: (lv_description_7_0= ruleEString )
                    // InternalFCL.g:6682:6: lv_description_7_0= ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getBooleanValueTypeAccess().getDescriptionEStringParserRuleCall_5_1_0());
                      					
                    }
                    pushFollow(FOLLOW_16);
                    lv_description_7_0=ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getBooleanValueTypeRule());
                      						}
                      						set(
                      							current,
                      							"description",
                      							lv_description_7_0,
                      							"fr.inria.glose.fcl.FCL.EString");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getBooleanValueTypeAccess().getRightCurlyBracketKeyword_6());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanValueType"


    // $ANTLR start "entryRuleStringValueType"
    // InternalFCL.g:6708:1: entryRuleStringValueType returns [EObject current=null] : iv_ruleStringValueType= ruleStringValueType EOF ;
    public final EObject entryRuleStringValueType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringValueType = null;


        try {
            // InternalFCL.g:6708:56: (iv_ruleStringValueType= ruleStringValueType EOF )
            // InternalFCL.g:6709:2: iv_ruleStringValueType= ruleStringValueType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringValueTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleStringValueType=ruleStringValueType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringValueType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringValueType"


    // $ANTLR start "ruleStringValueType"
    // InternalFCL.g:6715:1: ruleStringValueType returns [EObject current=null] : ( () otherlv_1= 'StringValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? otherlv_8= '}' ) ;
    public final EObject ruleStringValueType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_unit_5_0 = null;

        AntlrDatatypeRuleToken lv_description_7_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:6721:2: ( ( () otherlv_1= 'StringValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? otherlv_8= '}' ) )
            // InternalFCL.g:6722:2: ( () otherlv_1= 'StringValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? otherlv_8= '}' )
            {
            // InternalFCL.g:6722:2: ( () otherlv_1= 'StringValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? otherlv_8= '}' )
            // InternalFCL.g:6723:3: () otherlv_1= 'StringValueType' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? otherlv_8= '}'
            {
            // InternalFCL.g:6723:3: ()
            // InternalFCL.g:6724:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getStringValueTypeAccess().getStringValueTypeAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,86,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getStringValueTypeAccess().getStringValueTypeKeyword_1());
              		
            }
            // InternalFCL.g:6734:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalFCL.g:6735:4: (lv_name_2_0= RULE_ID )
            {
            // InternalFCL.g:6735:4: (lv_name_2_0= RULE_ID )
            // InternalFCL.g:6736:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getStringValueTypeAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getStringValueTypeRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_91); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getStringValueTypeAccess().getLeftCurlyBracketKeyword_3());
              		
            }
            // InternalFCL.g:6756:3: (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )?
            int alt120=2;
            int LA120_0 = input.LA(1);

            if ( (LA120_0==79) ) {
                alt120=1;
            }
            switch (alt120) {
                case 1 :
                    // InternalFCL.g:6757:4: otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,79,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getStringValueTypeAccess().getUnitKeyword_4_0());
                      			
                    }
                    // InternalFCL.g:6761:4: ( (lv_unit_5_0= ruleEString ) )
                    // InternalFCL.g:6762:5: (lv_unit_5_0= ruleEString )
                    {
                    // InternalFCL.g:6762:5: (lv_unit_5_0= ruleEString )
                    // InternalFCL.g:6763:6: lv_unit_5_0= ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getStringValueTypeAccess().getUnitEStringParserRuleCall_4_1_0());
                      					
                    }
                    pushFollow(FOLLOW_92);
                    lv_unit_5_0=ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getStringValueTypeRule());
                      						}
                      						set(
                      							current,
                      							"unit",
                      							lv_unit_5_0,
                      							"fr.inria.glose.fcl.FCL.EString");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:6781:3: (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )?
            int alt121=2;
            int LA121_0 = input.LA(1);

            if ( (LA121_0==80) ) {
                alt121=1;
            }
            switch (alt121) {
                case 1 :
                    // InternalFCL.g:6782:4: otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,80,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getStringValueTypeAccess().getDescriptionKeyword_5_0());
                      			
                    }
                    // InternalFCL.g:6786:4: ( (lv_description_7_0= ruleEString ) )
                    // InternalFCL.g:6787:5: (lv_description_7_0= ruleEString )
                    {
                    // InternalFCL.g:6787:5: (lv_description_7_0= ruleEString )
                    // InternalFCL.g:6788:6: lv_description_7_0= ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getStringValueTypeAccess().getDescriptionEStringParserRuleCall_5_1_0());
                      					
                    }
                    pushFollow(FOLLOW_16);
                    lv_description_7_0=ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getStringValueTypeRule());
                      						}
                      						set(
                      							current,
                      							"description",
                      							lv_description_7_0,
                      							"fr.inria.glose.fcl.FCL.EString");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getStringValueTypeAccess().getRightCurlyBracketKeyword_6());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringValueType"


    // $ANTLR start "entryRuleEnumerationValueType"
    // InternalFCL.g:6814:1: entryRuleEnumerationValueType returns [EObject current=null] : iv_ruleEnumerationValueType= ruleEnumerationValueType EOF ;
    public final EObject entryRuleEnumerationValueType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumerationValueType = null;


        try {
            // InternalFCL.g:6814:61: (iv_ruleEnumerationValueType= ruleEnumerationValueType EOF )
            // InternalFCL.g:6815:2: iv_ruleEnumerationValueType= ruleEnumerationValueType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumerationValueTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEnumerationValueType=ruleEnumerationValueType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumerationValueType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumerationValueType"


    // $ANTLR start "ruleEnumerationValueType"
    // InternalFCL.g:6821:1: ruleEnumerationValueType returns [EObject current=null] : ( () otherlv_1= 'enumeration' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= '(' ( (lv_enumerationLiteral_9_0= ruleEnumerationLiteral ) ) (otherlv_10= ',' ( (lv_enumerationLiteral_11_0= ruleEnumerationLiteral ) ) )* otherlv_12= ')' )? otherlv_13= '}' ) ;
    public final EObject ruleEnumerationValueType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        AntlrDatatypeRuleToken lv_unit_5_0 = null;

        AntlrDatatypeRuleToken lv_description_7_0 = null;

        EObject lv_enumerationLiteral_9_0 = null;

        EObject lv_enumerationLiteral_11_0 = null;



        	enterRule();

        try {
            // InternalFCL.g:6827:2: ( ( () otherlv_1= 'enumeration' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= '(' ( (lv_enumerationLiteral_9_0= ruleEnumerationLiteral ) ) (otherlv_10= ',' ( (lv_enumerationLiteral_11_0= ruleEnumerationLiteral ) ) )* otherlv_12= ')' )? otherlv_13= '}' ) )
            // InternalFCL.g:6828:2: ( () otherlv_1= 'enumeration' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= '(' ( (lv_enumerationLiteral_9_0= ruleEnumerationLiteral ) ) (otherlv_10= ',' ( (lv_enumerationLiteral_11_0= ruleEnumerationLiteral ) ) )* otherlv_12= ')' )? otherlv_13= '}' )
            {
            // InternalFCL.g:6828:2: ( () otherlv_1= 'enumeration' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= '(' ( (lv_enumerationLiteral_9_0= ruleEnumerationLiteral ) ) (otherlv_10= ',' ( (lv_enumerationLiteral_11_0= ruleEnumerationLiteral ) ) )* otherlv_12= ')' )? otherlv_13= '}' )
            // InternalFCL.g:6829:3: () otherlv_1= 'enumeration' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= '(' ( (lv_enumerationLiteral_9_0= ruleEnumerationLiteral ) ) (otherlv_10= ',' ( (lv_enumerationLiteral_11_0= ruleEnumerationLiteral ) ) )* otherlv_12= ')' )? otherlv_13= '}'
            {
            // InternalFCL.g:6829:3: ()
            // InternalFCL.g:6830:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getEnumerationValueTypeAccess().getEnumerationValueTypeAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,87,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getEnumerationValueTypeAccess().getEnumerationKeyword_1());
              		
            }
            // InternalFCL.g:6840:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalFCL.g:6841:4: (lv_name_2_0= RULE_ID )
            {
            // InternalFCL.g:6841:4: (lv_name_2_0= RULE_ID )
            // InternalFCL.g:6842:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_2_0, grammarAccess.getEnumerationValueTypeAccess().getNameIDTerminalRuleCall_2_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getEnumerationValueTypeRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_2_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_99); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getEnumerationValueTypeAccess().getLeftCurlyBracketKeyword_3());
              		
            }
            // InternalFCL.g:6862:3: (otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) ) )?
            int alt122=2;
            int LA122_0 = input.LA(1);

            if ( (LA122_0==79) ) {
                alt122=1;
            }
            switch (alt122) {
                case 1 :
                    // InternalFCL.g:6863:4: otherlv_4= 'unit' ( (lv_unit_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,79,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getEnumerationValueTypeAccess().getUnitKeyword_4_0());
                      			
                    }
                    // InternalFCL.g:6867:4: ( (lv_unit_5_0= ruleEString ) )
                    // InternalFCL.g:6868:5: (lv_unit_5_0= ruleEString )
                    {
                    // InternalFCL.g:6868:5: (lv_unit_5_0= ruleEString )
                    // InternalFCL.g:6869:6: lv_unit_5_0= ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getEnumerationValueTypeAccess().getUnitEStringParserRuleCall_4_1_0());
                      					
                    }
                    pushFollow(FOLLOW_100);
                    lv_unit_5_0=ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getEnumerationValueTypeRule());
                      						}
                      						set(
                      							current,
                      							"unit",
                      							lv_unit_5_0,
                      							"fr.inria.glose.fcl.FCL.EString");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:6887:3: (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )?
            int alt123=2;
            int LA123_0 = input.LA(1);

            if ( (LA123_0==80) ) {
                alt123=1;
            }
            switch (alt123) {
                case 1 :
                    // InternalFCL.g:6888:4: otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,80,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getEnumerationValueTypeAccess().getDescriptionKeyword_5_0());
                      			
                    }
                    // InternalFCL.g:6892:4: ( (lv_description_7_0= ruleEString ) )
                    // InternalFCL.g:6893:5: (lv_description_7_0= ruleEString )
                    {
                    // InternalFCL.g:6893:5: (lv_description_7_0= ruleEString )
                    // InternalFCL.g:6894:6: lv_description_7_0= ruleEString
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getEnumerationValueTypeAccess().getDescriptionEStringParserRuleCall_5_1_0());
                      					
                    }
                    pushFollow(FOLLOW_101);
                    lv_description_7_0=ruleEString();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getEnumerationValueTypeRule());
                      						}
                      						set(
                      							current,
                      							"description",
                      							lv_description_7_0,
                      							"fr.inria.glose.fcl.FCL.EString");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalFCL.g:6912:3: (otherlv_8= '(' ( (lv_enumerationLiteral_9_0= ruleEnumerationLiteral ) ) (otherlv_10= ',' ( (lv_enumerationLiteral_11_0= ruleEnumerationLiteral ) ) )* otherlv_12= ')' )?
            int alt125=2;
            int LA125_0 = input.LA(1);

            if ( (LA125_0==24) ) {
                alt125=1;
            }
            switch (alt125) {
                case 1 :
                    // InternalFCL.g:6913:4: otherlv_8= '(' ( (lv_enumerationLiteral_9_0= ruleEnumerationLiteral ) ) (otherlv_10= ',' ( (lv_enumerationLiteral_11_0= ruleEnumerationLiteral ) ) )* otherlv_12= ')'
                    {
                    otherlv_8=(Token)match(input,24,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_8, grammarAccess.getEnumerationValueTypeAccess().getLeftParenthesisKeyword_6_0());
                      			
                    }
                    // InternalFCL.g:6917:4: ( (lv_enumerationLiteral_9_0= ruleEnumerationLiteral ) )
                    // InternalFCL.g:6918:5: (lv_enumerationLiteral_9_0= ruleEnumerationLiteral )
                    {
                    // InternalFCL.g:6918:5: (lv_enumerationLiteral_9_0= ruleEnumerationLiteral )
                    // InternalFCL.g:6919:6: lv_enumerationLiteral_9_0= ruleEnumerationLiteral
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getEnumerationValueTypeAccess().getEnumerationLiteralEnumerationLiteralParserRuleCall_6_1_0());
                      					
                    }
                    pushFollow(FOLLOW_24);
                    lv_enumerationLiteral_9_0=ruleEnumerationLiteral();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getEnumerationValueTypeRule());
                      						}
                      						add(
                      							current,
                      							"enumerationLiteral",
                      							lv_enumerationLiteral_9_0,
                      							"fr.inria.glose.fcl.FCL.EnumerationLiteral");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalFCL.g:6936:4: (otherlv_10= ',' ( (lv_enumerationLiteral_11_0= ruleEnumerationLiteral ) ) )*
                    loop124:
                    do {
                        int alt124=2;
                        int LA124_0 = input.LA(1);

                        if ( (LA124_0==25) ) {
                            alt124=1;
                        }


                        switch (alt124) {
                    	case 1 :
                    	    // InternalFCL.g:6937:5: otherlv_10= ',' ( (lv_enumerationLiteral_11_0= ruleEnumerationLiteral ) )
                    	    {
                    	    otherlv_10=(Token)match(input,25,FOLLOW_3); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_10, grammarAccess.getEnumerationValueTypeAccess().getCommaKeyword_6_2_0());
                    	      				
                    	    }
                    	    // InternalFCL.g:6941:5: ( (lv_enumerationLiteral_11_0= ruleEnumerationLiteral ) )
                    	    // InternalFCL.g:6942:6: (lv_enumerationLiteral_11_0= ruleEnumerationLiteral )
                    	    {
                    	    // InternalFCL.g:6942:6: (lv_enumerationLiteral_11_0= ruleEnumerationLiteral )
                    	    // InternalFCL.g:6943:7: lv_enumerationLiteral_11_0= ruleEnumerationLiteral
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getEnumerationValueTypeAccess().getEnumerationLiteralEnumerationLiteralParserRuleCall_6_2_1_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_24);
                    	    lv_enumerationLiteral_11_0=ruleEnumerationLiteral();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getEnumerationValueTypeRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"enumerationLiteral",
                    	      								lv_enumerationLiteral_11_0,
                    	      								"fr.inria.glose.fcl.FCL.EnumerationLiteral");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop124;
                        }
                    } while (true);

                    otherlv_12=(Token)match(input,26,FOLLOW_16); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_12, grammarAccess.getEnumerationValueTypeAccess().getRightParenthesisKeyword_6_3());
                      			
                    }

                    }
                    break;

            }

            otherlv_13=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_13, grammarAccess.getEnumerationValueTypeAccess().getRightCurlyBracketKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumerationValueType"


    // $ANTLR start "entryRuleEnumerationLiteral"
    // InternalFCL.g:6974:1: entryRuleEnumerationLiteral returns [EObject current=null] : iv_ruleEnumerationLiteral= ruleEnumerationLiteral EOF ;
    public final EObject entryRuleEnumerationLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumerationLiteral = null;


        try {
            // InternalFCL.g:6974:59: (iv_ruleEnumerationLiteral= ruleEnumerationLiteral EOF )
            // InternalFCL.g:6975:2: iv_ruleEnumerationLiteral= ruleEnumerationLiteral EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumerationLiteralRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEnumerationLiteral=ruleEnumerationLiteral();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumerationLiteral; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumerationLiteral"


    // $ANTLR start "ruleEnumerationLiteral"
    // InternalFCL.g:6981:1: ruleEnumerationLiteral returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleEnumerationLiteral() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalFCL.g:6987:2: ( ( () ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalFCL.g:6988:2: ( () ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalFCL.g:6988:2: ( () ( (lv_name_1_0= RULE_ID ) ) )
            // InternalFCL.g:6989:3: () ( (lv_name_1_0= RULE_ID ) )
            {
            // InternalFCL.g:6989:3: ()
            // InternalFCL.g:6990:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getEnumerationLiteralAccess().getEnumerationLiteralAction_0(),
              					current);
              			
            }

            }

            // InternalFCL.g:6996:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalFCL.g:6997:4: (lv_name_1_0= RULE_ID )
            {
            // InternalFCL.g:6997:4: (lv_name_1_0= RULE_ID )
            // InternalFCL.g:6998:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getEnumerationLiteralAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getEnumerationLiteralRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumerationLiteral"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalFCL.g:7018:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalFCL.g:7018:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalFCL.g:7019:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleQualifiedName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalFCL.g:7025:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalFCL.g:7031:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalFCL.g:7032:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalFCL.g:7032:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalFCL.g:7033:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_102); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_ID_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
              		
            }
            // InternalFCL.g:7040:3: (kw= '.' this_ID_2= RULE_ID )*
            loop126:
            do {
                int alt126=2;
                int LA126_0 = input.LA(1);

                if ( (LA126_0==88) ) {
                    alt126=1;
                }


                switch (alt126) {
            	case 1 :
            	    // InternalFCL.g:7041:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,88,FOLLOW_3); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(kw);
            	      				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	      			
            	    }
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_102); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(this_ID_2);
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	      			
            	    }

            	    }
            	    break;

            	default :
            	    break loop126;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleEString"
    // InternalFCL.g:7058:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalFCL.g:7058:47: (iv_ruleEString= ruleEString EOF )
            // InternalFCL.g:7059:2: iv_ruleEString= ruleEString EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEStringRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEString.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalFCL.g:7065:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_STRING_0= RULE_STRING ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;


        	enterRule();

        try {
            // InternalFCL.g:7071:2: (this_STRING_0= RULE_STRING )
            // InternalFCL.g:7072:2: this_STRING_0= RULE_STRING
            {
            this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current.merge(this_STRING_0);
              	
            }
            if ( state.backtracking==0 ) {

              		newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall());
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleEBoolean"
    // InternalFCL.g:7082:1: entryRuleEBoolean returns [String current=null] : iv_ruleEBoolean= ruleEBoolean EOF ;
    public final String entryRuleEBoolean() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEBoolean = null;


        try {
            // InternalFCL.g:7082:48: (iv_ruleEBoolean= ruleEBoolean EOF )
            // InternalFCL.g:7083:2: iv_ruleEBoolean= ruleEBoolean EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEBooleanRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEBoolean=ruleEBoolean();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEBoolean.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEBoolean"


    // $ANTLR start "ruleEBoolean"
    // InternalFCL.g:7089:1: ruleEBoolean returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'true' | kw= 'false' ) ;
    public final AntlrDatatypeRuleToken ruleEBoolean() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalFCL.g:7095:2: ( (kw= 'true' | kw= 'false' ) )
            // InternalFCL.g:7096:2: (kw= 'true' | kw= 'false' )
            {
            // InternalFCL.g:7096:2: (kw= 'true' | kw= 'false' )
            int alt127=2;
            int LA127_0 = input.LA(1);

            if ( (LA127_0==89) ) {
                alt127=1;
            }
            else if ( (LA127_0==90) ) {
                alt127=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 127, 0, input);

                throw nvae;
            }
            switch (alt127) {
                case 1 :
                    // InternalFCL.g:7097:3: kw= 'true'
                    {
                    kw=(Token)match(input,89,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getEBooleanAccess().getTrueKeyword_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalFCL.g:7103:3: kw= 'false'
                    {
                    kw=(Token)match(input,90,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getEBooleanAccess().getFalseKeyword_1());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEBoolean"


    // $ANTLR start "entryRuleEInt"
    // InternalFCL.g:7112:1: entryRuleEInt returns [String current=null] : iv_ruleEInt= ruleEInt EOF ;
    public final String entryRuleEInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEInt = null;


        try {
            // InternalFCL.g:7112:44: (iv_ruleEInt= ruleEInt EOF )
            // InternalFCL.g:7113:2: iv_ruleEInt= ruleEInt EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEIntRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEInt=ruleEInt();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEInt.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalFCL.g:7119:1: ruleEInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_INT_0= RULE_INT ;
    public final AntlrDatatypeRuleToken ruleEInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;


        	enterRule();

        try {
            // InternalFCL.g:7125:2: (this_INT_0= RULE_INT )
            // InternalFCL.g:7126:2: this_INT_0= RULE_INT
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current.merge(this_INT_0);
              	
            }
            if ( state.backtracking==0 ) {

              		newLeafNode(this_INT_0, grammarAccess.getEIntAccess().getINTTerminalRuleCall());
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "entryRuleEDouble"
    // InternalFCL.g:7136:1: entryRuleEDouble returns [String current=null] : iv_ruleEDouble= ruleEDouble EOF ;
    public final String entryRuleEDouble() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEDouble = null;


        try {
            // InternalFCL.g:7136:47: (iv_ruleEDouble= ruleEDouble EOF )
            // InternalFCL.g:7137:2: iv_ruleEDouble= ruleEDouble EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEDoubleRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEDouble=ruleEDouble();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEDouble.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEDouble"


    // $ANTLR start "ruleEDouble"
    // InternalFCL.g:7143:1: ruleEDouble returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (this_INT_0= RULE_INT )? kw= '.' this_INT_2= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_6= RULE_INT )? ) ;
    public final AntlrDatatypeRuleToken ruleEDouble() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;
        Token kw=null;
        Token this_INT_2=null;
        Token this_INT_6=null;


        	enterRule();

        try {
            // InternalFCL.g:7149:2: ( ( (this_INT_0= RULE_INT )? kw= '.' this_INT_2= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_6= RULE_INT )? ) )
            // InternalFCL.g:7150:2: ( (this_INT_0= RULE_INT )? kw= '.' this_INT_2= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_6= RULE_INT )? )
            {
            // InternalFCL.g:7150:2: ( (this_INT_0= RULE_INT )? kw= '.' this_INT_2= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_6= RULE_INT )? )
            // InternalFCL.g:7151:3: (this_INT_0= RULE_INT )? kw= '.' this_INT_2= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_6= RULE_INT )?
            {
            // InternalFCL.g:7151:3: (this_INT_0= RULE_INT )?
            int alt128=2;
            int LA128_0 = input.LA(1);

            if ( (LA128_0==RULE_INT) ) {
                alt128=1;
            }
            switch (alt128) {
                case 1 :
                    // InternalFCL.g:7152:4: this_INT_0= RULE_INT
                    {
                    this_INT_0=(Token)match(input,RULE_INT,FOLLOW_103); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(this_INT_0);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newLeafNode(this_INT_0, grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_0());
                      			
                    }

                    }
                    break;

            }

            kw=(Token)match(input,88,FOLLOW_96); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getEDoubleAccess().getFullStopKeyword_1());
              		
            }
            this_INT_2=(Token)match(input,RULE_INT,FOLLOW_104); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_INT_2);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_INT_2, grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_2());
              		
            }
            // InternalFCL.g:7172:3: ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_6= RULE_INT )?
            int alt131=2;
            int LA131_0 = input.LA(1);

            if ( ((LA131_0>=91 && LA131_0<=92)) ) {
                alt131=1;
            }
            switch (alt131) {
                case 1 :
                    // InternalFCL.g:7173:4: (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_6= RULE_INT
                    {
                    // InternalFCL.g:7173:4: (kw= 'E' | kw= 'e' )
                    int alt129=2;
                    int LA129_0 = input.LA(1);

                    if ( (LA129_0==91) ) {
                        alt129=1;
                    }
                    else if ( (LA129_0==92) ) {
                        alt129=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 129, 0, input);

                        throw nvae;
                    }
                    switch (alt129) {
                        case 1 :
                            // InternalFCL.g:7174:5: kw= 'E'
                            {
                            kw=(Token)match(input,91,FOLLOW_105); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					current.merge(kw);
                              					newLeafNode(kw, grammarAccess.getEDoubleAccess().getEKeyword_3_0_0());
                              				
                            }

                            }
                            break;
                        case 2 :
                            // InternalFCL.g:7180:5: kw= 'e'
                            {
                            kw=(Token)match(input,92,FOLLOW_105); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					current.merge(kw);
                              					newLeafNode(kw, grammarAccess.getEDoubleAccess().getEKeyword_3_0_1());
                              				
                            }

                            }
                            break;

                    }

                    // InternalFCL.g:7186:4: (kw= '-' )?
                    int alt130=2;
                    int LA130_0 = input.LA(1);

                    if ( (LA130_0==93) ) {
                        alt130=1;
                    }
                    switch (alt130) {
                        case 1 :
                            // InternalFCL.g:7187:5: kw= '-'
                            {
                            kw=(Token)match(input,93,FOLLOW_96); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					current.merge(kw);
                              					newLeafNode(kw, grammarAccess.getEDoubleAccess().getHyphenMinusKeyword_3_1());
                              				
                            }

                            }
                            break;

                    }

                    this_INT_6=(Token)match(input,RULE_INT,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(this_INT_6);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newLeafNode(this_INT_6, grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_3_2());
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEDouble"


    // $ANTLR start "ruleDirectionKind"
    // InternalFCL.g:7205:1: ruleDirectionKind returns [Enumerator current=null] : ( (enumLiteral_0= 'in' ) | (enumLiteral_1= 'out' ) | (enumLiteral_2= 'inout' ) ) ;
    public final Enumerator ruleDirectionKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalFCL.g:7211:2: ( ( (enumLiteral_0= 'in' ) | (enumLiteral_1= 'out' ) | (enumLiteral_2= 'inout' ) ) )
            // InternalFCL.g:7212:2: ( (enumLiteral_0= 'in' ) | (enumLiteral_1= 'out' ) | (enumLiteral_2= 'inout' ) )
            {
            // InternalFCL.g:7212:2: ( (enumLiteral_0= 'in' ) | (enumLiteral_1= 'out' ) | (enumLiteral_2= 'inout' ) )
            int alt132=3;
            switch ( input.LA(1) ) {
            case 94:
                {
                alt132=1;
                }
                break;
            case 95:
                {
                alt132=2;
                }
                break;
            case 96:
                {
                alt132=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 132, 0, input);

                throw nvae;
            }

            switch (alt132) {
                case 1 :
                    // InternalFCL.g:7213:3: (enumLiteral_0= 'in' )
                    {
                    // InternalFCL.g:7213:3: (enumLiteral_0= 'in' )
                    // InternalFCL.g:7214:4: enumLiteral_0= 'in'
                    {
                    enumLiteral_0=(Token)match(input,94,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getDirectionKindAccess().getInEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getDirectionKindAccess().getInEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalFCL.g:7221:3: (enumLiteral_1= 'out' )
                    {
                    // InternalFCL.g:7221:3: (enumLiteral_1= 'out' )
                    // InternalFCL.g:7222:4: enumLiteral_1= 'out'
                    {
                    enumLiteral_1=(Token)match(input,95,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getDirectionKindAccess().getOutEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getDirectionKindAccess().getOutEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalFCL.g:7229:3: (enumLiteral_2= 'inout' )
                    {
                    // InternalFCL.g:7229:3: (enumLiteral_2= 'inout' )
                    // InternalFCL.g:7230:4: enumLiteral_2= 'inout'
                    {
                    enumLiteral_2=(Token)match(input,96,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getDirectionKindAccess().getInOutEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_2, grammarAccess.getDirectionKindAccess().getInOutEnumLiteralDeclaration_2());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDirectionKind"


    // $ANTLR start "ruleUnaryOperator"
    // InternalFCL.g:7240:1: ruleUnaryOperator returns [Enumerator current=null] : ( (enumLiteral_0= '-' ) | (enumLiteral_1= 'not' ) ) ;
    public final Enumerator ruleUnaryOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalFCL.g:7246:2: ( ( (enumLiteral_0= '-' ) | (enumLiteral_1= 'not' ) ) )
            // InternalFCL.g:7247:2: ( (enumLiteral_0= '-' ) | (enumLiteral_1= 'not' ) )
            {
            // InternalFCL.g:7247:2: ( (enumLiteral_0= '-' ) | (enumLiteral_1= 'not' ) )
            int alt133=2;
            int LA133_0 = input.LA(1);

            if ( (LA133_0==93) ) {
                alt133=1;
            }
            else if ( (LA133_0==97) ) {
                alt133=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 133, 0, input);

                throw nvae;
            }
            switch (alt133) {
                case 1 :
                    // InternalFCL.g:7248:3: (enumLiteral_0= '-' )
                    {
                    // InternalFCL.g:7248:3: (enumLiteral_0= '-' )
                    // InternalFCL.g:7249:4: enumLiteral_0= '-'
                    {
                    enumLiteral_0=(Token)match(input,93,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getUnaryOperatorAccess().getUNARYMINUSEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getUnaryOperatorAccess().getUNARYMINUSEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalFCL.g:7256:3: (enumLiteral_1= 'not' )
                    {
                    // InternalFCL.g:7256:3: (enumLiteral_1= 'not' )
                    // InternalFCL.g:7257:4: enumLiteral_1= 'not'
                    {
                    enumLiteral_1=(Token)match(input,97,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getUnaryOperatorAccess().getNOTEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getUnaryOperatorAccess().getNOTEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryOperator"


    // $ANTLR start "ruleAdditionOperator"
    // InternalFCL.g:7267:1: ruleAdditionOperator returns [Enumerator current=null] : ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) ) ;
    public final Enumerator ruleAdditionOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalFCL.g:7273:2: ( ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) ) )
            // InternalFCL.g:7274:2: ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) )
            {
            // InternalFCL.g:7274:2: ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) )
            int alt134=2;
            int LA134_0 = input.LA(1);

            if ( (LA134_0==98) ) {
                alt134=1;
            }
            else if ( (LA134_0==93) ) {
                alt134=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 134, 0, input);

                throw nvae;
            }
            switch (alt134) {
                case 1 :
                    // InternalFCL.g:7275:3: (enumLiteral_0= '+' )
                    {
                    // InternalFCL.g:7275:3: (enumLiteral_0= '+' )
                    // InternalFCL.g:7276:4: enumLiteral_0= '+'
                    {
                    enumLiteral_0=(Token)match(input,98,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getAdditionOperatorAccess().getPLUSEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getAdditionOperatorAccess().getPLUSEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalFCL.g:7283:3: (enumLiteral_1= '-' )
                    {
                    // InternalFCL.g:7283:3: (enumLiteral_1= '-' )
                    // InternalFCL.g:7284:4: enumLiteral_1= '-'
                    {
                    enumLiteral_1=(Token)match(input,93,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getAdditionOperatorAccess().getMINUSEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getAdditionOperatorAccess().getMINUSEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdditionOperator"


    // $ANTLR start "ruleMultiplicationOperator"
    // InternalFCL.g:7294:1: ruleMultiplicationOperator returns [Enumerator current=null] : ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) | (enumLiteral_2= '%' ) ) ;
    public final Enumerator ruleMultiplicationOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalFCL.g:7300:2: ( ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) | (enumLiteral_2= '%' ) ) )
            // InternalFCL.g:7301:2: ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) | (enumLiteral_2= '%' ) )
            {
            // InternalFCL.g:7301:2: ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) | (enumLiteral_2= '%' ) )
            int alt135=3;
            switch ( input.LA(1) ) {
            case 99:
                {
                alt135=1;
                }
                break;
            case 100:
                {
                alt135=2;
                }
                break;
            case 101:
                {
                alt135=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 135, 0, input);

                throw nvae;
            }

            switch (alt135) {
                case 1 :
                    // InternalFCL.g:7302:3: (enumLiteral_0= '*' )
                    {
                    // InternalFCL.g:7302:3: (enumLiteral_0= '*' )
                    // InternalFCL.g:7303:4: enumLiteral_0= '*'
                    {
                    enumLiteral_0=(Token)match(input,99,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getMultiplicationOperatorAccess().getMULTEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getMultiplicationOperatorAccess().getMULTEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalFCL.g:7310:3: (enumLiteral_1= '/' )
                    {
                    // InternalFCL.g:7310:3: (enumLiteral_1= '/' )
                    // InternalFCL.g:7311:4: enumLiteral_1= '/'
                    {
                    enumLiteral_1=(Token)match(input,100,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getMultiplicationOperatorAccess().getDIVEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getMultiplicationOperatorAccess().getDIVEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalFCL.g:7318:3: (enumLiteral_2= '%' )
                    {
                    // InternalFCL.g:7318:3: (enumLiteral_2= '%' )
                    // InternalFCL.g:7319:4: enumLiteral_2= '%'
                    {
                    enumLiteral_2=(Token)match(input,101,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getMultiplicationOperatorAccess().getMODEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_2, grammarAccess.getMultiplicationOperatorAccess().getMODEnumLiteralDeclaration_2());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicationOperator"


    // $ANTLR start "ruleEqualOperator"
    // InternalFCL.g:7329:1: ruleEqualOperator returns [Enumerator current=null] : (enumLiteral_0= '=' ) ;
    public final Enumerator ruleEqualOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;


        	enterRule();

        try {
            // InternalFCL.g:7335:2: ( (enumLiteral_0= '=' ) )
            // InternalFCL.g:7336:2: (enumLiteral_0= '=' )
            {
            // InternalFCL.g:7336:2: (enumLiteral_0= '=' )
            // InternalFCL.g:7337:3: enumLiteral_0= '='
            {
            enumLiteral_0=(Token)match(input,102,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = grammarAccess.getEqualOperatorAccess().getEQUALEnumLiteralDeclaration().getEnumLiteral().getInstance();
              			newLeafNode(enumLiteral_0, grammarAccess.getEqualOperatorAccess().getEQUALEnumLiteralDeclaration());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEqualOperator"


    // $ANTLR start "ruleComparisionOperator"
    // InternalFCL.g:7346:1: ruleComparisionOperator returns [Enumerator current=null] : ( (enumLiteral_0= '>' ) | (enumLiteral_1= '<' ) | (enumLiteral_2= '>=' ) | (enumLiteral_3= '<=' ) ) ;
    public final Enumerator ruleComparisionOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalFCL.g:7352:2: ( ( (enumLiteral_0= '>' ) | (enumLiteral_1= '<' ) | (enumLiteral_2= '>=' ) | (enumLiteral_3= '<=' ) ) )
            // InternalFCL.g:7353:2: ( (enumLiteral_0= '>' ) | (enumLiteral_1= '<' ) | (enumLiteral_2= '>=' ) | (enumLiteral_3= '<=' ) )
            {
            // InternalFCL.g:7353:2: ( (enumLiteral_0= '>' ) | (enumLiteral_1= '<' ) | (enumLiteral_2= '>=' ) | (enumLiteral_3= '<=' ) )
            int alt136=4;
            switch ( input.LA(1) ) {
            case 103:
                {
                alt136=1;
                }
                break;
            case 104:
                {
                alt136=2;
                }
                break;
            case 105:
                {
                alt136=3;
                }
                break;
            case 106:
                {
                alt136=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 136, 0, input);

                throw nvae;
            }

            switch (alt136) {
                case 1 :
                    // InternalFCL.g:7354:3: (enumLiteral_0= '>' )
                    {
                    // InternalFCL.g:7354:3: (enumLiteral_0= '>' )
                    // InternalFCL.g:7355:4: enumLiteral_0= '>'
                    {
                    enumLiteral_0=(Token)match(input,103,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getComparisionOperatorAccess().getGREATEREnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getComparisionOperatorAccess().getGREATEREnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalFCL.g:7362:3: (enumLiteral_1= '<' )
                    {
                    // InternalFCL.g:7362:3: (enumLiteral_1= '<' )
                    // InternalFCL.g:7363:4: enumLiteral_1= '<'
                    {
                    enumLiteral_1=(Token)match(input,104,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getComparisionOperatorAccess().getLOWEREnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getComparisionOperatorAccess().getLOWEREnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalFCL.g:7370:3: (enumLiteral_2= '>=' )
                    {
                    // InternalFCL.g:7370:3: (enumLiteral_2= '>=' )
                    // InternalFCL.g:7371:4: enumLiteral_2= '>='
                    {
                    enumLiteral_2=(Token)match(input,105,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getComparisionOperatorAccess().getGREATEROREQUALEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_2, grammarAccess.getComparisionOperatorAccess().getGREATEROREQUALEnumLiteralDeclaration_2());
                      			
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalFCL.g:7378:3: (enumLiteral_3= '<=' )
                    {
                    // InternalFCL.g:7378:3: (enumLiteral_3= '<=' )
                    // InternalFCL.g:7379:4: enumLiteral_3= '<='
                    {
                    enumLiteral_3=(Token)match(input,106,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getComparisionOperatorAccess().getLOWEROREQUALEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_3, grammarAccess.getComparisionOperatorAccess().getLOWEROREQUALEnumLiteralDeclaration_3());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComparisionOperator"


    // $ANTLR start "ruleAndOperator"
    // InternalFCL.g:7389:1: ruleAndOperator returns [Enumerator current=null] : (enumLiteral_0= 'and' ) ;
    public final Enumerator ruleAndOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;


        	enterRule();

        try {
            // InternalFCL.g:7395:2: ( (enumLiteral_0= 'and' ) )
            // InternalFCL.g:7396:2: (enumLiteral_0= 'and' )
            {
            // InternalFCL.g:7396:2: (enumLiteral_0= 'and' )
            // InternalFCL.g:7397:3: enumLiteral_0= 'and'
            {
            enumLiteral_0=(Token)match(input,107,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = grammarAccess.getAndOperatorAccess().getANDEnumLiteralDeclaration().getEnumLiteral().getInstance();
              			newLeafNode(enumLiteral_0, grammarAccess.getAndOperatorAccess().getANDEnumLiteralDeclaration());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndOperator"


    // $ANTLR start "ruleOrOperator"
    // InternalFCL.g:7406:1: ruleOrOperator returns [Enumerator current=null] : (enumLiteral_0= 'or' ) ;
    public final Enumerator ruleOrOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;


        	enterRule();

        try {
            // InternalFCL.g:7412:2: ( (enumLiteral_0= 'or' ) )
            // InternalFCL.g:7413:2: (enumLiteral_0= 'or' )
            {
            // InternalFCL.g:7413:2: (enumLiteral_0= 'or' )
            // InternalFCL.g:7414:3: enumLiteral_0= 'or'
            {
            enumLiteral_0=(Token)match(input,108,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = grammarAccess.getOrOperatorAccess().getOREnumLiteralDeclaration().getEnumLiteral().getInstance();
              			newLeafNode(enumLiteral_0, grammarAccess.getOrOperatorAccess().getOREnumLiteralDeclaration());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrOperator"

    // $ANTLR start synpred1_InternalFCL
    public final void synpred1_InternalFCL_fragment() throws RecognitionException {   
        // InternalFCL.g:3859:5: ( 'else' )
        // InternalFCL.g:3859:6: 'else'
        {
        match(input,62,FOLLOW_2); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred1_InternalFCL

    // Delegated rules

    public final boolean synpred1_InternalFCL() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_InternalFCL_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA9 dfa9 = new DFA9(this);
    protected DFA14 dfa14 = new DFA14(this);
    protected DFA20 dfa20 = new DFA20(this);
    protected DFA94 dfa94 = new DFA94(this);
    protected DFA105 dfa105 = new DFA105(this);
    static final String dfa_1s = "\15\uffff";
    static final String dfa_2s = "\1\4\1\uffff\1\30\5\uffff\1\4\3\uffff\1\30";
    static final String dfa_3s = "\1\113\1\uffff\1\130\5\uffff\1\4\3\uffff\1\130";
    static final String dfa_4s = "\1\uffff\1\1\1\uffff\1\4\1\5\1\7\1\10\1\11\1\uffff\1\2\1\6\1\3\1\uffff";
    static final String dfa_5s = "\15\uffff}>";
    static final String[] dfa_6s = {
            "\1\2\7\uffff\1\1\55\uffff\1\5\1\uffff\1\3\2\uffff\1\4\11\uffff\1\7\1\uffff\1\6",
            "",
            "\1\12\3\uffff\1\13\36\uffff\1\11\34\uffff\1\10",
            "",
            "",
            "",
            "",
            "",
            "\1\14",
            "",
            "",
            "",
            "\1\12\3\uffff\1\13\36\uffff\1\11\34\uffff\1\10"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA9 extends DFA {

        public DFA9(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 9;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "439:2: (this_ActionBlock_0= ruleActionBlock | this_Assignment_1= ruleAssignment | this_DataStructPropertyAssignment_2= ruleDataStructPropertyAssignment | this_IfAction_3= ruleIfAction | this_WhileAction_4= ruleWhileAction | this_ProcedureCall_5= ruleProcedureCall | this_VarDecl_Impl_6= ruleVarDecl_Impl | this_FCLProcedureReturn_7= ruleFCLProcedureReturn | this_Log_8= ruleLog )";
        }
    }
    static final String dfa_7s = "\12\uffff";
    static final String dfa_8s = "\1\16\4\0\5\uffff";
    static final String dfa_9s = "\1\33\4\0\5\uffff";
    static final String dfa_10s = "\5\uffff\1\3\2\uffff\1\2\1\1";
    static final String dfa_11s = "\1\0\1\1\1\2\1\3\1\4\5\uffff}>";
    static final String[] dfa_12s = {
            "\1\5\2\uffff\1\3\1\10\2\5\1\1\1\2\4\uffff\1\4",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_7 = DFA.unpackEncodedString(dfa_7s);
    static final char[] dfa_8 = DFA.unpackEncodedStringToUnsignedChars(dfa_8s);
    static final char[] dfa_9 = DFA.unpackEncodedStringToUnsignedChars(dfa_9s);
    static final short[] dfa_10 = DFA.unpackEncodedString(dfa_10s);
    static final short[] dfa_11 = DFA.unpackEncodedString(dfa_11s);
    static final short[][] dfa_12 = unpackEncodedStringArray(dfa_12s);

    class DFA14 extends DFA {

        public DFA14(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 14;
            this.eot = dfa_7;
            this.eof = dfa_7;
            this.min = dfa_8;
            this.max = dfa_9;
            this.accept = dfa_10;
            this.special = dfa_11;
            this.transition = dfa_12;
        }
        public String getDescription() {
            return "()* loopback of 675:6: ( ({...}? => ( ({...}? => ( ( (lv_modes_5_0= ruleMode ) ) | ( (lv_transitions_6_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_7= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )*";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA14_0 = input.LA(1);

                         
                        int index14_0 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (LA14_0==21) ) {s = 1;}

                        else if ( (LA14_0==22) ) {s = 2;}

                        else if ( (LA14_0==17) ) {s = 3;}

                        else if ( (LA14_0==27) ) {s = 4;}

                        else if ( (LA14_0==14||(LA14_0>=19 && LA14_0<=20)) ) {s = 5;}

                        else if ( LA14_0 == 18 && getUnorderedGroupHelper().canSelect(grammarAccess.getMainAutomataAccess().getUnorderedGroup_4(), 1) ) {s = 8;}

                         
                        input.seek(index14_0);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA14_1 = input.LA(1);

                         
                        int index14_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( getUnorderedGroupHelper().canSelect(grammarAccess.getMainAutomataAccess().getUnorderedGroup_4(), 0) ) {s = 9;}

                        else if ( (true) ) {s = 5;}

                         
                        input.seek(index14_1);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA14_2 = input.LA(1);

                         
                        int index14_2 = input.index();
                        input.rewind();
                        s = -1;
                        if ( getUnorderedGroupHelper().canSelect(grammarAccess.getMainAutomataAccess().getUnorderedGroup_4(), 0) ) {s = 9;}

                        else if ( (true) ) {s = 5;}

                         
                        input.seek(index14_2);
                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA14_3 = input.LA(1);

                         
                        int index14_3 = input.index();
                        input.rewind();
                        s = -1;
                        if ( getUnorderedGroupHelper().canSelect(grammarAccess.getMainAutomataAccess().getUnorderedGroup_4(), 0) ) {s = 9;}

                        else if ( (true) ) {s = 5;}

                         
                        input.seek(index14_3);
                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA14_4 = input.LA(1);

                         
                        int index14_4 = input.index();
                        input.rewind();
                        s = -1;
                        if ( getUnorderedGroupHelper().canSelect(grammarAccess.getMainAutomataAccess().getUnorderedGroup_4(), 0) ) {s = 9;}

                        else if ( (true) ) {s = 5;}

                         
                        input.seek(index14_4);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 14, _s, input);
            error(nvae);
            throw nvae;
        }
    }

    class DFA20 extends DFA {

        public DFA20(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 20;
            this.eot = dfa_7;
            this.eof = dfa_7;
            this.min = dfa_8;
            this.max = dfa_9;
            this.accept = dfa_10;
            this.special = dfa_11;
            this.transition = dfa_12;
        }
        public String getDescription() {
            return "()* loopback of 979:6: ( ({...}? => ( ({...}? => ( ( (lv_modes_6_0= ruleMode ) ) | ( (lv_transitions_7_0= ruleTransition ) ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_8= 'initialMode' ( ( ruleQualifiedName ) ) ) ) ) ) )*";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA20_0 = input.LA(1);

                         
                        int index20_0 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (LA20_0==21) ) {s = 1;}

                        else if ( (LA20_0==22) ) {s = 2;}

                        else if ( (LA20_0==17) ) {s = 3;}

                        else if ( (LA20_0==27) ) {s = 4;}

                        else if ( (LA20_0==14||(LA20_0>=19 && LA20_0<=20)) ) {s = 5;}

                        else if ( LA20_0 == 18 && getUnorderedGroupHelper().canSelect(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_5(), 1) ) {s = 8;}

                         
                        input.seek(index20_0);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA20_1 = input.LA(1);

                         
                        int index20_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( getUnorderedGroupHelper().canSelect(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_5(), 0) ) {s = 9;}

                        else if ( (true) ) {s = 5;}

                         
                        input.seek(index20_1);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA20_2 = input.LA(1);

                         
                        int index20_2 = input.index();
                        input.rewind();
                        s = -1;
                        if ( getUnorderedGroupHelper().canSelect(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_5(), 0) ) {s = 9;}

                        else if ( (true) ) {s = 5;}

                         
                        input.seek(index20_2);
                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA20_3 = input.LA(1);

                         
                        int index20_3 = input.index();
                        input.rewind();
                        s = -1;
                        if ( getUnorderedGroupHelper().canSelect(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_5(), 0) ) {s = 9;}

                        else if ( (true) ) {s = 5;}

                         
                        input.seek(index20_3);
                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA20_4 = input.LA(1);

                         
                        int index20_4 = input.index();
                        input.rewind();
                        s = -1;
                        if ( getUnorderedGroupHelper().canSelect(grammarAccess.getModeAutomata_ImplAccess().getUnorderedGroup_5(), 0) ) {s = 9;}

                        else if ( (true) ) {s = 5;}

                         
                        input.seek(index20_4);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 20, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String dfa_13s = "\10\uffff";
    static final String dfa_14s = "\2\uffff\1\5\4\uffff\1\5";
    static final String dfa_15s = "\1\4\1\uffff\1\4\1\uffff\1\4\2\uffff\1\4";
    static final String dfa_16s = "\1\132\1\uffff\1\154\1\uffff\1\4\2\uffff\1\154";
    static final String dfa_17s = "\1\uffff\1\1\1\uffff\1\3\1\uffff\1\2\1\4\1\uffff";
    static final String dfa_18s = "\10\uffff}>";
    static final String[] dfa_19s = {
            "\1\2\2\1\103\uffff\1\3\15\uffff\3\1",
            "",
            "\1\5\7\uffff\1\5\1\uffff\1\5\12\uffff\2\5\1\uffff\1\6\3\5\1\uffff\1\5\6\uffff\1\5\5\uffff\3\5\4\uffff\2\5\3\uffff\1\5\1\uffff\4\5\11\uffff\1\5\1\uffff\1\5\14\uffff\1\4\4\uffff\1\5\4\uffff\13\5",
            "",
            "\1\7",
            "",
            "",
            "\1\5\7\uffff\1\5\1\uffff\1\5\12\uffff\2\5\1\uffff\1\6\3\5\1\uffff\1\5\6\uffff\1\5\5\uffff\3\5\4\uffff\2\5\3\uffff\1\5\1\uffff\4\5\11\uffff\1\5\1\uffff\1\5\14\uffff\1\4\4\uffff\1\5\4\uffff\13\5"
    };

    static final short[] dfa_13 = DFA.unpackEncodedString(dfa_13s);
    static final short[] dfa_14 = DFA.unpackEncodedString(dfa_14s);
    static final char[] dfa_15 = DFA.unpackEncodedStringToUnsignedChars(dfa_15s);
    static final char[] dfa_16 = DFA.unpackEncodedStringToUnsignedChars(dfa_16s);
    static final short[] dfa_17 = DFA.unpackEncodedString(dfa_17s);
    static final short[] dfa_18 = DFA.unpackEncodedString(dfa_18s);
    static final short[][] dfa_19 = unpackEncodedStringArray(dfa_19s);

    class DFA94 extends DFA {

        public DFA94(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 94;
            this.eot = dfa_13;
            this.eof = dfa_14;
            this.min = dfa_15;
            this.max = dfa_16;
            this.accept = dfa_17;
            this.special = dfa_18;
            this.transition = dfa_19;
        }
        public String getDescription() {
            return "4828:2: (this_CallConstant_0= ruleCallConstant | this_CallDeclarationReference_1= ruleCallDeclarationReference | this_CallPrevious_2= ruleCallPrevious | this_CallDataStructProperty_3= ruleCallDataStructProperty )";
        }
    }
    static final String dfa_20s = "\11\uffff";
    static final String dfa_21s = "\2\uffff\1\3\5\uffff\1\3";
    static final String dfa_22s = "\1\4\1\uffff\1\4\3\uffff\1\4\1\uffff\1\4";
    static final String dfa_23s = "\1\132\1\uffff\1\154\3\uffff\1\4\1\uffff\1\154";
    static final String dfa_24s = "\1\uffff\1\1\1\uffff\1\3\1\4\1\5\1\uffff\1\2\1\uffff";
    static final String dfa_25s = "\11\uffff}>";
    static final String[] dfa_26s = {
            "\1\2\2\3\21\uffff\1\5\54\uffff\1\1\2\uffff\1\4\1\uffff\1\3\15\uffff\3\3",
            "",
            "\1\3\7\uffff\1\3\1\uffff\1\3\11\uffff\1\7\2\3\1\uffff\4\3\1\uffff\1\3\6\uffff\1\3\5\uffff\3\3\4\uffff\2\3\3\uffff\1\3\1\uffff\4\3\11\uffff\1\3\1\uffff\1\3\14\uffff\1\6\4\uffff\1\3\4\uffff\13\3",
            "",
            "",
            "",
            "\1\10",
            "",
            "\1\3\7\uffff\1\3\1\uffff\1\3\11\uffff\1\7\2\3\1\uffff\4\3\1\uffff\1\3\6\uffff\1\3\5\uffff\3\3\4\uffff\2\3\3\uffff\1\3\1\uffff\4\3\11\uffff\1\3\1\uffff\1\3\14\uffff\1\6\4\uffff\1\3\4\uffff\13\3"
    };

    static final short[] dfa_20 = DFA.unpackEncodedString(dfa_20s);
    static final short[] dfa_21 = DFA.unpackEncodedString(dfa_21s);
    static final char[] dfa_22 = DFA.unpackEncodedStringToUnsignedChars(dfa_22s);
    static final char[] dfa_23 = DFA.unpackEncodedStringToUnsignedChars(dfa_23s);
    static final short[] dfa_24 = DFA.unpackEncodedString(dfa_24s);
    static final short[] dfa_25 = DFA.unpackEncodedString(dfa_25s);
    static final short[][] dfa_26 = unpackEncodedStringArray(dfa_26s);

    class DFA105 extends DFA {

        public DFA105(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 105;
            this.eot = dfa_20;
            this.eof = dfa_21;
            this.min = dfa_22;
            this.max = dfa_23;
            this.accept = dfa_24;
            this.special = dfa_25;
            this.transition = dfa_26;
        }
        public String getDescription() {
            return "5887:2: (this_Cast_0= ruleCast | this_ProcedureCall_1= ruleProcedureCall | this_Call_2= ruleCall | this_NewDataStruct_3= ruleNewDataStruct | (otherlv_4= '(' (this_OrExpression_5= ruleOrExpression | this_UnaryExpression_6= ruleUnaryExpression ) otherlv_7= ')' ) )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x000000000001E000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000004000L,0x0000000000000001L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x000000000001C000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000006L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000004000L,0x0000000000000006L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000014000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000000L,0x0000000000F25000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000004000L,0x0000000000F25000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x00000000087E4000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x00000000087A4000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x9400000000001010L,0x0000000000000A00L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000184000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000984000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000006000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x00000000E0004000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000001000070L,0x0000000227000520L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000000010L,0x00000001C0000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000001C00000002L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000001800000002L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000003C00000002L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000003800000002L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x000000DC00000002L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x000000D800000002L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000009800000002L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000F80000004000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000010200000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000010200004000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000F00000004000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0400000000000000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0400000000004000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000E00000004000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000C00000004000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0060000000000000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000800000004000L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x008A040000004000L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0001780000004000L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0001700000004000L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0001600000004000L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0001400000004000L});
    public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0001000000004000L});
    public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x0004580000004000L});
    public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0000580000004000L});
    public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_59 = new BitSet(new long[]{0x0000000200004000L});
    public static final BitSet FOLLOW_60 = new BitSet(new long[]{0x0000500000004000L});
    public static final BitSet FOLLOW_61 = new BitSet(new long[]{0x0000400000004000L});
    public static final BitSet FOLLOW_62 = new BitSet(new long[]{0x0010580000004000L});
    public static final BitSet FOLLOW_63 = new BitSet(new long[]{0x0060000000000002L});
    public static final BitSet FOLLOW_64 = new BitSet(new long[]{0x0100000000000002L});
    public static final BitSet FOLLOW_65 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_66 = new BitSet(new long[]{0x0800000000000002L});
    public static final BitSet FOLLOW_67 = new BitSet(new long[]{0x9400000000005010L,0x0000000000000A00L});
    public static final BitSet FOLLOW_68 = new BitSet(new long[]{0x0800000000000000L});
    public static final BitSet FOLLOW_69 = new BitSet(new long[]{0x0800000010000000L});
    public static final BitSet FOLLOW_70 = new BitSet(new long[]{0x2000000000000000L});
    public static final BitSet FOLLOW_71 = new BitSet(new long[]{0x4000000000000002L});
    public static final BitSet FOLLOW_72 = new BitSet(new long[]{0x0000000005000070L,0x0000000227000520L});
    public static final BitSet FOLLOW_73 = new BitSet(new long[]{0x0000000004000010L});
    public static final BitSet FOLLOW_74 = new BitSet(new long[]{0x0000000000000010L,0x0000000000000008L});
    public static final BitSet FOLLOW_75 = new BitSet(new long[]{0x0000000000004000L,0x0000000000000010L});
    public static final BitSet FOLLOW_76 = new BitSet(new long[]{0x0000000000000000L,0x00000000000000C0L});
    public static final BitSet FOLLOW_77 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000080L});
    public static final BitSet FOLLOW_78 = new BitSet(new long[]{0x0000000000004010L});
    public static final BitSet FOLLOW_79 = new BitSet(new long[]{0x0000000002004000L});
    public static final BitSet FOLLOW_80 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_81 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_82 = new BitSet(new long[]{0x0000000000000002L,0x0000100000000000L});
    public static final BitSet FOLLOW_83 = new BitSet(new long[]{0x0000000001000070L,0x0000000007000520L});
    public static final BitSet FOLLOW_84 = new BitSet(new long[]{0x0000000000000002L,0x0000080000000000L});
    public static final BitSet FOLLOW_85 = new BitSet(new long[]{0x0000000000000002L,0x0000004000000000L});
    public static final BitSet FOLLOW_86 = new BitSet(new long[]{0x0000000000000002L,0x0000078000000000L});
    public static final BitSet FOLLOW_87 = new BitSet(new long[]{0x0000000000000002L,0x0000000420000000L});
    public static final BitSet FOLLOW_88 = new BitSet(new long[]{0x0000000000000002L,0x0000003800000000L});
    public static final BitSet FOLLOW_89 = new BitSet(new long[]{0x0000000000004000L,0x0000000000002000L});
    public static final BitSet FOLLOW_90 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_91 = new BitSet(new long[]{0x0000000000004000L,0x0000000000018000L});
    public static final BitSet FOLLOW_92 = new BitSet(new long[]{0x0000000000004000L,0x0000000000010000L});
    public static final BitSet FOLLOW_93 = new BitSet(new long[]{0x0000000000004000L,0x00000000000D8000L});
    public static final BitSet FOLLOW_94 = new BitSet(new long[]{0x0000000000004000L,0x00000000000D0000L});
    public static final BitSet FOLLOW_95 = new BitSet(new long[]{0x0000000000004000L,0x00000000000C0000L});
    public static final BitSet FOLLOW_96 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_97 = new BitSet(new long[]{0x0000000000004000L,0x0000000000080000L});
    public static final BitSet FOLLOW_98 = new BitSet(new long[]{0x0000000000000060L,0x0000000007000000L});
    public static final BitSet FOLLOW_99 = new BitSet(new long[]{0x0000000001004000L,0x0000000000018000L});
    public static final BitSet FOLLOW_100 = new BitSet(new long[]{0x0000000001004000L,0x0000000000010000L});
    public static final BitSet FOLLOW_101 = new BitSet(new long[]{0x0000000001004000L});
    public static final BitSet FOLLOW_102 = new BitSet(new long[]{0x0000000000000002L,0x0000000001000000L});
    public static final BitSet FOLLOW_103 = new BitSet(new long[]{0x0000000000000000L,0x0000000001000000L});
    public static final BitSet FOLLOW_104 = new BitSet(new long[]{0x0000000000000002L,0x0000000018000000L});
    public static final BitSet FOLLOW_105 = new BitSet(new long[]{0x0000000000000040L,0x0000000020000000L});

}
