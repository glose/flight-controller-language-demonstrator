package fr.inria.glose.fcl.ui.outline;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.xtext.ui.editor.outline.IOutlineNode;
import org.eclipse.xtext.ui.editor.outline.impl.AbstractOutlineNode;

@SuppressWarnings("all")
public class VirtualOutlineNode extends AbstractOutlineNode {
  protected VirtualOutlineNode(final IOutlineNode parent, final Image image, final Object text, final boolean isLeaf) {
    super(parent, image, text, isLeaf);
  }
  
  protected VirtualOutlineNode(final IOutlineNode parent, final ImageDescriptor imageDesc, final Object text, final boolean isLeaf) {
    super(parent, imageDesc, text, isLeaf);
  }
}
