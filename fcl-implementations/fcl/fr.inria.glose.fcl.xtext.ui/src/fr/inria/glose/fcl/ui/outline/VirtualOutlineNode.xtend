package fr.inria.glose.fcl.ui.outline

import org.eclipse.xtext.ui.editor.outline.impl.AbstractOutlineNode
import org.eclipse.xtext.ui.editor.outline.IOutlineNode
import org.eclipse.swt.graphics.Image
import org.eclipse.jface.resource.ImageDescriptor

class VirtualOutlineNode extends AbstractOutlineNode {
	
	protected new(IOutlineNode parent, Image image, Object text, boolean isLeaf) {
		super(parent, image, text, isLeaf)
	}
	protected new(IOutlineNode parent, ImageDescriptor imageDesc, Object text, boolean isLeaf) {
		super(parent, imageDesc, text, isLeaf)
	}
	
}