package fr.inria.glose.fcl.design;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.texteditor.AbstractTextEditor;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.resource.XtextResource;

import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.FunctionConnector;
import fr.inria.glose.fcl.model.fcl.Mode;
import fr.inria.glose.fcl.model.fcl.ModeAutomata;
import fr.inria.glose.fcl.model.fcl.Transition;

/**
 * The services class used by VSM.
 */
public class Services {
   
    /**
    * See http://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.sirius.doc%2Fdoc%2Findex.html&cp=24 for documentation on how to write service methods.
    */
    public EObject myService(EObject self, String arg) {
       // TODO Auto-generated code
      return self;
    }
  
	/**
	 * Gets the label for the given {@link Transition}.
	 * 
	 * @param transition
	 *            the {@link Transition}
	 * @return the label for the given {@link Transition}
	 */
	public String getLabel(Transition transition) {
		final StringBuilder res = new StringBuilder();

		if(transition.getName() != null) {
			res.append(transition.getName());
		}
		if(transition.getEvent() != null) {
			if(transition.getName() != null) {
				res.append("\n");
			}
			res.append("when ");
			res.append(transition.getEvent().getName());
		}
		if(transition.getGuard() != null) {
			if(transition.getName() != null || (transition.getEvent() != null)) {
				res.append("\n");
			}
			res.append("guard ");
			res.append(xtextPrettyPrint(transition.getGuard()));
		}	
		return res.toString();
	}
	
	/**
	 * internal mode can be initial
	 * the main one (contained by the FCLModel is not considered as initial (as it is the only one)
	 * @param mode
	 * @return
	 */
	public Boolean isInitialMode(Mode mode) {
		if(mode.eContainer() != null && mode.eContainer() instanceof ModeAutomata) {
			ModeAutomata sm = (ModeAutomata)(mode.eContainer());
			return mode.equals(sm.getInitialMode());
		}
		return false;
	}
	
	/**
	 * checks if the port is implied in a direct loop 
	 * @param port
	 * @return
	 */
	public Boolean isInOutWithLoopConnection(FunctionBlockDataPort port) {
		// PB incomming connectors EOpposite are not set by xtext
		
		/* return port.getIncomingConnectors()
				.stream()
				.anyMatch(incomingConnector -> 
					port.getOutgoingConnectors().contains(incomingConnector));
					*/
		// workaround
		return EcoreUtil2.getAllContentsOfType(EcoreUtil.getRootContainer(port), FunctionConnector.class).stream()
				.anyMatch(connector -> 
					connector.getEmitter() != null
					&& connector.getEmitter().equals(port) 
					&& connector.getEmitter().equals(connector.getReceiver()));
	}
	
	/**
	 * checks that the connector does a direct loop (should be on an InOut port)
	 * @param connector
	 * @return
	 */
	public Boolean isLoopConnection(FunctionConnector connector) {
		return connector.getEmitter() != null && connector.getEmitter().equals(connector.getReceiver());
	}
	
    /**
     * Try to retrieve an xtext resource for the given element and then get its String representation
     * @param any EObject
     * @return the xtext representation of the EObject or an empty string
     */
    public String xtextPrettyPrint(EObject any) {
    	if (any != null && any.eResource() instanceof XtextResource && any.eResource().getURI() != null) {
			String fileURI = any.eResource().getURI().toPlatformString(true);
			IFile workspaceFile = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(fileURI));
			if (workspaceFile != null) {
				ICompositeNode node = NodeModelUtils.findActualNodeFor(any);
				if (node != null) {
					return node.getText().trim();
				}
			}
    	}
    	return "";
    }
    
    public EObject openTextEditor(EObject any) {
		if (any != null && any.eResource() instanceof XtextResource && any.eResource().getURI() != null) {

			String fileURI = any.eResource().getURI().toPlatformString(true);
			IFile workspaceFile = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(fileURI));
			if (workspaceFile != null) {
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				try {
					IEditorPart openEditor = IDE.openEditor(page, workspaceFile,
							"fr.inria.glose.fcl.FCL", true);
					if (openEditor instanceof AbstractTextEditor) {
						ICompositeNode node = NodeModelUtils.findActualNodeFor(any);
						if (node != null) {
							int offset = node.getOffset();
							int length = node.getTotalEndOffset() - offset;
							((AbstractTextEditor) openEditor).selectAndReveal(offset, length);
						}
					}
					// editorInput.
				} catch (PartInitException e) {
					Activator.error(e.getMessage(), e);
				}
			}
		}
		return any;
	}
	
	public EObject openBasicHoveringDialog(EObject any) {
		String xtextString = xtextPrettyPrint(any);
		if (xtextString != null && !xtextString.isEmpty()) {
			IEditorPart part = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();							
			InfoPopUp pop = new InfoPopUp( part.getSite().getShell() , "Textual representation of the element","press ESC to close");
			pop.setText(xtextString);
			pop.open();
		}
		return any;
	}
	
	
	public EObject firstCommonContainer(EObject obj1, EObject obj2) {
		List<EObject> hierarchy1 = getContainersHierarchy(obj1);
		List<EObject> hierarchy2 = getContainersHierarchy(obj2);
		
		hierarchy1.retainAll(hierarchy2);
		if(!hierarchy1.isEmpty()) {
			return hierarchy1.get(0);
		} else {
			return null;
		}
	}
	
	/**
	 * self and all its containers
	 * 
	 * topmost container last
	 * @param obj
	 * @return
	 */
	public List<EObject> getContainersHierarchy(EObject obj){
		List<EObject> res = new ArrayList<EObject>();
		res.add(obj);
		if(obj.eContainer() != null) {
			res.addAll(getContainersHierarchy(obj.eContainer()));
		}
		
		return res;
	}
}
