package fr.inria.glose.fcl.model.unity;

import java.util.HashMap;
import java.util.Map.Entry;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

public class UnityInstance {
	Shell shell;
	String name;
	String browserURL;
	private Browser browser;
	public UnityInstance(String name, String browserURL ) {
		this.name = name;
		this.browserURL = browserURL;
		//Thread t = new Thread() {
		Display.getDefault().syncExec(new Runnable() {
		    //public void run() {
		        // do any work that updates the screen ...
		        // remember to check if the widget
		        // still exists
		        // might happen if the part was closed
		   
			@Override
			public void run() {
				//display = new Display();
				Display display = PlatformUI.getWorkbench().getDisplay();
				shell = new Shell(display);
				shell.setText("Unity Visualization for "+name);
				shell.setSize(1024, 728);
				shell.setLayout(new FillLayout());
				
				browser = new Browser(shell, SWT.NONE);
				browser.setUrl(browserURL);
				
				shell.open();
				
				//float i = 0f;
			/*	while (!shell.isDisposed()) {
			        if (!display.readAndDispatch()) {
			            display.sleep();
			        }
			        //i = (i + 1f) % 360;
			        //this.setCoordinates(10, 10, 10, i, i, i);
				}*/
				//display.dispose();
			}
		});
		//t.start();
	}
	
	HashMap<String, Double > variablesBuffer = new HashMap<String, Double >();
	
	public void setVar(String nodeName, String varName, double value) {
		variablesBuffer.put(nodeName+","+varName, value);
		//this.browser.execute("unityInstance.SendMessage('"+nodeName+"', 'set"+varName+"', " + value + ");");
	}
	
	public void sendVariablesBufferToBrowser() {
		for(Entry<String, Double> entry : variablesBuffer.entrySet()) {
			String nodeName = entry.getKey().split(",")[0];
			String varName = entry.getKey().split(",")[1];
			if(entry.getValue() != null) {
				this.browser.execute("unityInstance.SendMessage('"+nodeName+"', 'set"+varName+"', " + entry.getValue() + ");");
			}
		}
	}
	
	public void dispose() {
		shell.dispose();
	}
}
