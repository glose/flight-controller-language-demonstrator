/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Declaration Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.CallDeclarationReference#getCallable <em>Callable</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getCallDeclarationReference()
 * @model
 * @generated
 */
public interface CallDeclarationReference extends Call {
	/**
	 * Returns the value of the '<em><b>Callable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Callable</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Callable</em>' reference.
	 * @see #setCallable(CallableDeclaration)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getCallDeclarationReference_Callable()
	 * @model required="true"
	 * @generated
	 */
	CallableDeclaration getCallable();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.CallDeclarationReference#getCallable <em>Callable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Callable</em>' reference.
	 * @see #getCallable()
	 * @generated
	 */
	void setCallable(CallableDeclaration value);

} // CallDeclarationReference
