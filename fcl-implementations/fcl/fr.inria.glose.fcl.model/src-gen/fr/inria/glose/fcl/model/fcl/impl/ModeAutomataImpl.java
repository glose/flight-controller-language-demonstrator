/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.Mode;
import fr.inria.glose.fcl.model.fcl.ModeAutomata;
import fr.inria.glose.fcl.model.fcl.Transition;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mode Automata</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.ModeAutomataImpl#getModes <em>Modes</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.ModeAutomataImpl#getInitialMode <em>Initial Mode</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.ModeAutomataImpl#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.ModeAutomataImpl#getCurrentMode <em>Current Mode</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModeAutomataImpl extends ModeImpl implements ModeAutomata {
	/**
	 * The cached value of the '{@link #getModes() <em>Modes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModes()
	 * @generated
	 * @ordered
	 */
	protected EList<Mode> modes;

	/**
	 * The cached value of the '{@link #getInitialMode() <em>Initial Mode</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialMode()
	 * @generated
	 * @ordered
	 */
	protected Mode initialMode;

	/**
	 * The cached value of the '{@link #getTransitions() <em>Transitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> transitions;

	/**
	 * The cached value of the '{@link #getCurrentMode() <em>Current Mode</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentMode()
	 * @generated
	 * @ordered
	 */
	protected Mode currentMode;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModeAutomataImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.MODE_AUTOMATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Mode> getModes() {
		if (modes == null) {
			modes = new EObjectContainmentEList<Mode>(Mode.class, this, FclPackage.MODE_AUTOMATA__MODES);
		}
		return modes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mode getInitialMode() {
		if (initialMode != null && initialMode.eIsProxy()) {
			InternalEObject oldInitialMode = (InternalEObject) initialMode;
			initialMode = (Mode) eResolveProxy(oldInitialMode);
			if (initialMode != oldInitialMode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FclPackage.MODE_AUTOMATA__INITIAL_MODE,
							oldInitialMode, initialMode));
			}
		}
		return initialMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mode basicGetInitialMode() {
		return initialMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialMode(Mode newInitialMode) {
		Mode oldInitialMode = initialMode;
		initialMode = newInitialMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.MODE_AUTOMATA__INITIAL_MODE,
					oldInitialMode, initialMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getTransitions() {
		if (transitions == null) {
			transitions = new EObjectContainmentEList<Transition>(Transition.class, this,
					FclPackage.MODE_AUTOMATA__TRANSITIONS);
		}
		return transitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mode getCurrentMode() {
		if (currentMode != null && currentMode.eIsProxy()) {
			InternalEObject oldCurrentMode = (InternalEObject) currentMode;
			currentMode = (Mode) eResolveProxy(oldCurrentMode);
			if (currentMode != oldCurrentMode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FclPackage.MODE_AUTOMATA__CURRENT_MODE,
							oldCurrentMode, currentMode));
			}
		}
		return currentMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mode basicGetCurrentMode() {
		return currentMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentMode(Mode newCurrentMode) {
		Mode oldCurrentMode = currentMode;
		currentMode = newCurrentMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.MODE_AUTOMATA__CURRENT_MODE,
					oldCurrentMode, currentMode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FclPackage.MODE_AUTOMATA__MODES:
			return ((InternalEList<?>) getModes()).basicRemove(otherEnd, msgs);
		case FclPackage.MODE_AUTOMATA__TRANSITIONS:
			return ((InternalEList<?>) getTransitions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.MODE_AUTOMATA__MODES:
			return getModes();
		case FclPackage.MODE_AUTOMATA__INITIAL_MODE:
			if (resolve)
				return getInitialMode();
			return basicGetInitialMode();
		case FclPackage.MODE_AUTOMATA__TRANSITIONS:
			return getTransitions();
		case FclPackage.MODE_AUTOMATA__CURRENT_MODE:
			if (resolve)
				return getCurrentMode();
			return basicGetCurrentMode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.MODE_AUTOMATA__MODES:
			getModes().clear();
			getModes().addAll((Collection<? extends Mode>) newValue);
			return;
		case FclPackage.MODE_AUTOMATA__INITIAL_MODE:
			setInitialMode((Mode) newValue);
			return;
		case FclPackage.MODE_AUTOMATA__TRANSITIONS:
			getTransitions().clear();
			getTransitions().addAll((Collection<? extends Transition>) newValue);
			return;
		case FclPackage.MODE_AUTOMATA__CURRENT_MODE:
			setCurrentMode((Mode) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.MODE_AUTOMATA__MODES:
			getModes().clear();
			return;
		case FclPackage.MODE_AUTOMATA__INITIAL_MODE:
			setInitialMode((Mode) null);
			return;
		case FclPackage.MODE_AUTOMATA__TRANSITIONS:
			getTransitions().clear();
			return;
		case FclPackage.MODE_AUTOMATA__CURRENT_MODE:
			setCurrentMode((Mode) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.MODE_AUTOMATA__MODES:
			return modes != null && !modes.isEmpty();
		case FclPackage.MODE_AUTOMATA__INITIAL_MODE:
			return initialMode != null;
		case FclPackage.MODE_AUTOMATA__TRANSITIONS:
			return transitions != null && !transitions.isEmpty();
		case FclPackage.MODE_AUTOMATA__CURRENT_MODE:
			return currentMode != null;
		}
		return super.eIsSet(featureID);
	}

} //ModeAutomataImpl
