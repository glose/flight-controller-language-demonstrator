/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.EnumerationLiteral;
import fr.inria.glose.fcl.model.fcl.EnumerationValue;
import fr.inria.glose.fcl.model.fcl.FclPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Enumeration Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.EnumerationValueImpl#getEnumerationValue <em>Enumeration Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnumerationValueImpl extends ValueImpl implements EnumerationValue {
	/**
	 * The cached value of the '{@link #getEnumerationValue() <em>Enumeration Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnumerationValue()
	 * @generated
	 * @ordered
	 */
	protected EnumerationLiteral enumerationValue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnumerationValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.ENUMERATION_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteral getEnumerationValue() {
		if (enumerationValue != null && enumerationValue.eIsProxy()) {
			InternalEObject oldEnumerationValue = (InternalEObject) enumerationValue;
			enumerationValue = (EnumerationLiteral) eResolveProxy(oldEnumerationValue);
			if (enumerationValue != oldEnumerationValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							FclPackage.ENUMERATION_VALUE__ENUMERATION_VALUE, oldEnumerationValue, enumerationValue));
			}
		}
		return enumerationValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteral basicGetEnumerationValue() {
		return enumerationValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnumerationValue(EnumerationLiteral newEnumerationValue) {
		EnumerationLiteral oldEnumerationValue = enumerationValue;
		enumerationValue = newEnumerationValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.ENUMERATION_VALUE__ENUMERATION_VALUE,
					oldEnumerationValue, enumerationValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.ENUMERATION_VALUE__ENUMERATION_VALUE:
			if (resolve)
				return getEnumerationValue();
			return basicGetEnumerationValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.ENUMERATION_VALUE__ENUMERATION_VALUE:
			setEnumerationValue((EnumerationLiteral) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.ENUMERATION_VALUE__ENUMERATION_VALUE:
			setEnumerationValue((EnumerationLiteral) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.ENUMERATION_VALUE__ENUMERATION_VALUE:
			return enumerationValue != null;
		}
		return super.eIsSet(featureID);
	}

} //EnumerationValueImpl
