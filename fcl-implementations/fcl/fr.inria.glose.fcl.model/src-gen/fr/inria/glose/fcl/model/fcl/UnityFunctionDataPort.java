/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unity Function Data Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort#getUnityNodeName <em>Unity Node Name</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort#getUnityVariableName <em>Unity Variable Name</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getUnityFunctionDataPort()
 * @model
 * @generated
 */
public interface UnityFunctionDataPort extends FunctionBlockDataPort {
	/**
	 * Returns the value of the '<em><b>Unity Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unity Node Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unity Node Name</em>' attribute.
	 * @see #setUnityNodeName(String)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getUnityFunctionDataPort_UnityNodeName()
	 * @model
	 * @generated
	 */
	String getUnityNodeName();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort#getUnityNodeName <em>Unity Node Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unity Node Name</em>' attribute.
	 * @see #getUnityNodeName()
	 * @generated
	 */
	void setUnityNodeName(String value);

	/**
	 * Returns the value of the '<em><b>Unity Variable Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unity Variable Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unity Variable Name</em>' attribute.
	 * @see #setUnityVariableName(String)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getUnityFunctionDataPort_UnityVariableName()
	 * @model
	 * @generated
	 */
	String getUnityVariableName();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort#getUnityVariableName <em>Unity Variable Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unity Variable Name</em>' attribute.
	 * @see #getUnityVariableName()
	 * @generated
	 */
	void setUnityVariableName(String value);

} // UnityFunctionDataPort
