/**
 */
package fr.inria.glose.fcl.model.fcl.interpreter_vm.impl;

import fr.inria.glose.fcl.model.fcl.Value;

import fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Procedure Context</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.ProcedureContextImpl#getDeclarationMapEntries <em>Declaration Map Entries</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.ProcedureContextImpl#getResultValue <em>Result Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProcedureContextImpl extends MinimalEObjectImpl.Container implements ProcedureContext {
	/**
	 * The cached value of the '{@link #getDeclarationMapEntries() <em>Declaration Map Entries</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeclarationMapEntries()
	 * @generated
	 * @ordered
	 */
	protected EList<DeclarationMapEntry> declarationMapEntries;

	/**
	 * The cached value of the '{@link #getResultValue() <em>Result Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResultValue()
	 * @generated
	 * @ordered
	 */
	protected Value resultValue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcedureContextImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Interpreter_vmPackage.Literals.PROCEDURE_CONTEXT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DeclarationMapEntry> getDeclarationMapEntries() {
		if (declarationMapEntries == null) {
			declarationMapEntries = new EObjectContainmentEList<DeclarationMapEntry>(DeclarationMapEntry.class, this,
					Interpreter_vmPackage.PROCEDURE_CONTEXT__DECLARATION_MAP_ENTRIES);
		}
		return declarationMapEntries;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getResultValue() {
		return resultValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResultValue(Value newResultValue, NotificationChain msgs) {
		Value oldResultValue = resultValue;
		resultValue = newResultValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					Interpreter_vmPackage.PROCEDURE_CONTEXT__RESULT_VALUE, oldResultValue, newResultValue);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResultValue(Value newResultValue) {
		if (newResultValue != resultValue) {
			NotificationChain msgs = null;
			if (resultValue != null)
				msgs = ((InternalEObject) resultValue).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - Interpreter_vmPackage.PROCEDURE_CONTEXT__RESULT_VALUE, null, msgs);
			if (newResultValue != null)
				msgs = ((InternalEObject) newResultValue).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - Interpreter_vmPackage.PROCEDURE_CONTEXT__RESULT_VALUE, null, msgs);
			msgs = basicSetResultValue(newResultValue, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Interpreter_vmPackage.PROCEDURE_CONTEXT__RESULT_VALUE,
					newResultValue, newResultValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Interpreter_vmPackage.PROCEDURE_CONTEXT__DECLARATION_MAP_ENTRIES:
			return ((InternalEList<?>) getDeclarationMapEntries()).basicRemove(otherEnd, msgs);
		case Interpreter_vmPackage.PROCEDURE_CONTEXT__RESULT_VALUE:
			return basicSetResultValue(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Interpreter_vmPackage.PROCEDURE_CONTEXT__DECLARATION_MAP_ENTRIES:
			return getDeclarationMapEntries();
		case Interpreter_vmPackage.PROCEDURE_CONTEXT__RESULT_VALUE:
			return getResultValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Interpreter_vmPackage.PROCEDURE_CONTEXT__DECLARATION_MAP_ENTRIES:
			getDeclarationMapEntries().clear();
			getDeclarationMapEntries().addAll((Collection<? extends DeclarationMapEntry>) newValue);
			return;
		case Interpreter_vmPackage.PROCEDURE_CONTEXT__RESULT_VALUE:
			setResultValue((Value) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Interpreter_vmPackage.PROCEDURE_CONTEXT__DECLARATION_MAP_ENTRIES:
			getDeclarationMapEntries().clear();
			return;
		case Interpreter_vmPackage.PROCEDURE_CONTEXT__RESULT_VALUE:
			setResultValue((Value) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Interpreter_vmPackage.PROCEDURE_CONTEXT__DECLARATION_MAP_ENTRIES:
			return declarationMapEntries != null && !declarationMapEntries.isEmpty();
		case Interpreter_vmPackage.PROCEDURE_CONTEXT__RESULT_VALUE:
			return resultValue != null;
		}
		return super.eIsSet(featureID);
	}

} //ProcedureContextImpl
