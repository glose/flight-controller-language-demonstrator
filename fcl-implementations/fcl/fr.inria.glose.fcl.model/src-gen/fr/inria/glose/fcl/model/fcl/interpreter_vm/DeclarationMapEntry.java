/**
 */
package fr.inria.glose.fcl.model.fcl.interpreter_vm;

import fr.inria.glose.fcl.model.fcl.CallableDeclaration;
import fr.inria.glose.fcl.model.fcl.Value;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Declaration Map Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry#getCallableDeclaration <em>Callable Declaration</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage#getDeclarationMapEntry()
 * @model
 * @generated
 */
public interface DeclarationMapEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Callable Declaration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Callable Declaration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Callable Declaration</em>' reference.
	 * @see #setCallableDeclaration(CallableDeclaration)
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage#getDeclarationMapEntry_CallableDeclaration()
	 * @model
	 * @generated
	 */
	CallableDeclaration getCallableDeclaration();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry#getCallableDeclaration <em>Callable Declaration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Callable Declaration</em>' reference.
	 * @see #getCallableDeclaration()
	 * @generated
	 */
	void setCallableDeclaration(CallableDeclaration value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(Value)
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage#getDeclarationMapEntry_Value()
	 * @model containment="true"
	 * @generated
	 */
	Value getValue();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Value value);

} // DeclarationMapEntry
