/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.IfAction#getCondition <em>Condition</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.IfAction#getThenAction <em>Then Action</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.IfAction#getElseAction <em>Else Action</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getIfAction()
 * @model
 * @generated
 */
public interface IfAction extends ControlStructureAction {
	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #setCondition(Expression)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getIfAction_Condition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getCondition();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.IfAction#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(Expression value);

	/**
	 * Returns the value of the '<em><b>Then Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Then Action</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Then Action</em>' containment reference.
	 * @see #setThenAction(Action)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getIfAction_ThenAction()
	 * @model containment="true"
	 * @generated
	 */
	Action getThenAction();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.IfAction#getThenAction <em>Then Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Then Action</em>' containment reference.
	 * @see #getThenAction()
	 * @generated
	 */
	void setThenAction(Action value);

	/**
	 * Returns the value of the '<em><b>Else Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Else Action</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Else Action</em>' containment reference.
	 * @see #setElseAction(Action)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getIfAction_ElseAction()
	 * @model containment="true"
	 * @generated
	 */
	Action getElseAction();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.IfAction#getElseAction <em>Else Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Else Action</em>' containment reference.
	 * @see #getElseAction()
	 * @generated
	 */
	void setElseAction(Action value);

} // IfAction
