/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.UnityFunction;

import fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unity Function</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.UnityFunctionImpl#getUnityWebPagePath <em>Unity Web Page Path</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.UnityFunctionImpl#getUnityWrapper <em>Unity Wrapper</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UnityFunctionImpl extends FunctionImpl implements UnityFunction {
	/**
	 * The default value of the '{@link #getUnityWebPagePath() <em>Unity Web Page Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnityWebPagePath()
	 * @generated
	 * @ordered
	 */
	protected static final String UNITY_WEB_PAGE_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnityWebPagePath() <em>Unity Web Page Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnityWebPagePath()
	 * @generated
	 * @ordered
	 */
	protected String unityWebPagePath = UNITY_WEB_PAGE_PATH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getUnityWrapper() <em>Unity Wrapper</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnityWrapper()
	 * @generated
	 * @ordered
	 */
	protected UnityWrapper unityWrapper;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnityFunctionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.UNITY_FUNCTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnityWebPagePath() {
		return unityWebPagePath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnityWebPagePath(String newUnityWebPagePath) {
		String oldUnityWebPagePath = unityWebPagePath;
		unityWebPagePath = newUnityWebPagePath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.UNITY_FUNCTION__UNITY_WEB_PAGE_PATH,
					oldUnityWebPagePath, unityWebPagePath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnityWrapper getUnityWrapper() {
		if (unityWrapper != null && unityWrapper.eIsProxy()) {
			InternalEObject oldUnityWrapper = (InternalEObject) unityWrapper;
			unityWrapper = (UnityWrapper) eResolveProxy(oldUnityWrapper);
			if (unityWrapper != oldUnityWrapper) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FclPackage.UNITY_FUNCTION__UNITY_WRAPPER,
							oldUnityWrapper, unityWrapper));
			}
		}
		return unityWrapper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnityWrapper basicGetUnityWrapper() {
		return unityWrapper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnityWrapper(UnityWrapper newUnityWrapper) {
		UnityWrapper oldUnityWrapper = unityWrapper;
		unityWrapper = newUnityWrapper;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.UNITY_FUNCTION__UNITY_WRAPPER,
					oldUnityWrapper, unityWrapper));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.UNITY_FUNCTION__UNITY_WEB_PAGE_PATH:
			return getUnityWebPagePath();
		case FclPackage.UNITY_FUNCTION__UNITY_WRAPPER:
			if (resolve)
				return getUnityWrapper();
			return basicGetUnityWrapper();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.UNITY_FUNCTION__UNITY_WEB_PAGE_PATH:
			setUnityWebPagePath((String) newValue);
			return;
		case FclPackage.UNITY_FUNCTION__UNITY_WRAPPER:
			setUnityWrapper((UnityWrapper) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.UNITY_FUNCTION__UNITY_WEB_PAGE_PATH:
			setUnityWebPagePath(UNITY_WEB_PAGE_PATH_EDEFAULT);
			return;
		case FclPackage.UNITY_FUNCTION__UNITY_WRAPPER:
			setUnityWrapper((UnityWrapper) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.UNITY_FUNCTION__UNITY_WEB_PAGE_PATH:
			return UNITY_WEB_PAGE_PATH_EDEFAULT == null ? unityWebPagePath != null
					: !UNITY_WEB_PAGE_PATH_EDEFAULT.equals(unityWebPagePath);
		case FclPackage.UNITY_FUNCTION__UNITY_WRAPPER:
			return unityWrapper != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (unityWebPagePath: ");
		result.append(unityWebPagePath);
		result.append(')');
		return result.toString();
	}

} //UnityFunctionImpl
