/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.BooleanValueType;
import fr.inria.glose.fcl.model.fcl.FclPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Boolean Value Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BooleanValueTypeImpl extends ValueTypeImpl implements BooleanValueType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BooleanValueTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.BOOLEAN_VALUE_TYPE;
	}

} //BooleanValueTypeImpl
