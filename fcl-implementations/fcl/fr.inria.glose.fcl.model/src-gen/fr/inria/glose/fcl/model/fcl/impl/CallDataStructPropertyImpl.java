/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.Call;
import fr.inria.glose.fcl.model.fcl.CallDataStructProperty;
import fr.inria.glose.fcl.model.fcl.CallDeclarationReference;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.Expression;
import fr.inria.glose.fcl.model.fcl.FclPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Call Data Struct Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.CallDataStructPropertyImpl#getResultType <em>Result Type</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.CallDataStructPropertyImpl#getRootTargetDataStruct <em>Root Target Data Struct</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CallDataStructPropertyImpl extends DataStructPropertyReferenceImpl implements CallDataStructProperty {
	/**
	 * The cached value of the '{@link #getResultType() <em>Result Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResultType()
	 * @generated
	 * @ordered
	 */
	protected DataType resultType;

	/**
	 * The cached value of the '{@link #getRootTargetDataStruct() <em>Root Target Data Struct</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootTargetDataStruct()
	 * @generated
	 * @ordered
	 */
	protected CallDeclarationReference rootTargetDataStruct;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CallDataStructPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.CALL_DATA_STRUCT_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType getResultType() {
		if (resultType != null && resultType.eIsProxy()) {
			InternalEObject oldResultType = (InternalEObject) resultType;
			resultType = (DataType) eResolveProxy(oldResultType);
			if (resultType != oldResultType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							FclPackage.CALL_DATA_STRUCT_PROPERTY__RESULT_TYPE, oldResultType, resultType));
			}
		}
		return resultType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType basicGetResultType() {
		return resultType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResultType(DataType newResultType) {
		DataType oldResultType = resultType;
		resultType = newResultType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.CALL_DATA_STRUCT_PROPERTY__RESULT_TYPE,
					oldResultType, resultType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallDeclarationReference getRootTargetDataStruct() {
		return rootTargetDataStruct;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRootTargetDataStruct(CallDeclarationReference newRootTargetDataStruct,
			NotificationChain msgs) {
		CallDeclarationReference oldRootTargetDataStruct = rootTargetDataStruct;
		rootTargetDataStruct = newRootTargetDataStruct;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					FclPackage.CALL_DATA_STRUCT_PROPERTY__ROOT_TARGET_DATA_STRUCT, oldRootTargetDataStruct,
					newRootTargetDataStruct);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRootTargetDataStruct(CallDeclarationReference newRootTargetDataStruct) {
		if (newRootTargetDataStruct != rootTargetDataStruct) {
			NotificationChain msgs = null;
			if (rootTargetDataStruct != null)
				msgs = ((InternalEObject) rootTargetDataStruct).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.CALL_DATA_STRUCT_PROPERTY__ROOT_TARGET_DATA_STRUCT, null,
						msgs);
			if (newRootTargetDataStruct != null)
				msgs = ((InternalEObject) newRootTargetDataStruct).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.CALL_DATA_STRUCT_PROPERTY__ROOT_TARGET_DATA_STRUCT, null,
						msgs);
			msgs = basicSetRootTargetDataStruct(newRootTargetDataStruct, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FclPackage.CALL_DATA_STRUCT_PROPERTY__ROOT_TARGET_DATA_STRUCT, newRootTargetDataStruct,
					newRootTargetDataStruct));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FclPackage.CALL_DATA_STRUCT_PROPERTY__ROOT_TARGET_DATA_STRUCT:
			return basicSetRootTargetDataStruct(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.CALL_DATA_STRUCT_PROPERTY__RESULT_TYPE:
			if (resolve)
				return getResultType();
			return basicGetResultType();
		case FclPackage.CALL_DATA_STRUCT_PROPERTY__ROOT_TARGET_DATA_STRUCT:
			return getRootTargetDataStruct();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.CALL_DATA_STRUCT_PROPERTY__RESULT_TYPE:
			setResultType((DataType) newValue);
			return;
		case FclPackage.CALL_DATA_STRUCT_PROPERTY__ROOT_TARGET_DATA_STRUCT:
			setRootTargetDataStruct((CallDeclarationReference) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.CALL_DATA_STRUCT_PROPERTY__RESULT_TYPE:
			setResultType((DataType) null);
			return;
		case FclPackage.CALL_DATA_STRUCT_PROPERTY__ROOT_TARGET_DATA_STRUCT:
			setRootTargetDataStruct((CallDeclarationReference) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.CALL_DATA_STRUCT_PROPERTY__RESULT_TYPE:
			return resultType != null;
		case FclPackage.CALL_DATA_STRUCT_PROPERTY__ROOT_TARGET_DATA_STRUCT:
			return rootTargetDataStruct != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Expression.class) {
			switch (derivedFeatureID) {
			case FclPackage.CALL_DATA_STRUCT_PROPERTY__RESULT_TYPE:
				return FclPackage.EXPRESSION__RESULT_TYPE;
			default:
				return -1;
			}
		}
		if (baseClass == Call.class) {
			switch (derivedFeatureID) {
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Expression.class) {
			switch (baseFeatureID) {
			case FclPackage.EXPRESSION__RESULT_TYPE:
				return FclPackage.CALL_DATA_STRUCT_PROPERTY__RESULT_TYPE;
			default:
				return -1;
			}
		}
		if (baseClass == Call.class) {
			switch (baseFeatureID) {
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //CallDataStructPropertyImpl
