/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.Cast;
import fr.inria.glose.fcl.model.fcl.FclPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cast</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CastImpl extends UnaryExpressionImpl implements Cast {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CastImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.CAST;
	}

} //CastImpl
