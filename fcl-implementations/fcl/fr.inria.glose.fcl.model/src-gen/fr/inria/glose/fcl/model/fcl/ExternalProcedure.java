/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External Procedure</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getExternalProcedure()
 * @model abstract="true"
 * @generated
 */
public interface ExternalProcedure extends Procedure {
} // ExternalProcedure
