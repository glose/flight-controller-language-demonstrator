/**
 */
package fr.inria.glose.fcl.model.fcl.interpreter_vm.impl;

import fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.javafmi.wrapper.generic.Simulation2;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FMU Simulation Wrapper</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.FMUSimulationWrapperImpl#getFmiSimulation2 <em>Fmi Simulation2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FMUSimulationWrapperImpl extends MinimalEObjectImpl.Container implements FMUSimulationWrapper {
	/**
	 * The default value of the '{@link #getFmiSimulation2() <em>Fmi Simulation2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFmiSimulation2()
	 * @generated
	 * @ordered
	 */
	protected static final Simulation2 FMI_SIMULATION2_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFmiSimulation2() <em>Fmi Simulation2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFmiSimulation2()
	 * @generated
	 * @ordered
	 */
	protected Simulation2 fmiSimulation2 = FMI_SIMULATION2_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FMUSimulationWrapperImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Interpreter_vmPackage.Literals.FMU_SIMULATION_WRAPPER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simulation2 getFmiSimulation2() {
		return fmiSimulation2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFmiSimulation2(Simulation2 newFmiSimulation2) {
		Simulation2 oldFmiSimulation2 = fmiSimulation2;
		fmiSimulation2 = newFmiSimulation2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Interpreter_vmPackage.FMU_SIMULATION_WRAPPER__FMI_SIMULATION2, oldFmiSimulation2, fmiSimulation2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Interpreter_vmPackage.FMU_SIMULATION_WRAPPER__FMI_SIMULATION2:
			return getFmiSimulation2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Interpreter_vmPackage.FMU_SIMULATION_WRAPPER__FMI_SIMULATION2:
			setFmiSimulation2((Simulation2) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Interpreter_vmPackage.FMU_SIMULATION_WRAPPER__FMI_SIMULATION2:
			setFmiSimulation2(FMI_SIMULATION2_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Interpreter_vmPackage.FMU_SIMULATION_WRAPPER__FMI_SIMULATION2:
			return FMI_SIMULATION2_EDEFAULT == null ? fmiSimulation2 != null
					: !FMI_SIMULATION2_EDEFAULT.equals(fmiSimulation2);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (fmiSimulation2: ");
		result.append(fmiSimulation2);
		result.append(')');
		return result.toString();
	}

} //FMUSimulationWrapperImpl
