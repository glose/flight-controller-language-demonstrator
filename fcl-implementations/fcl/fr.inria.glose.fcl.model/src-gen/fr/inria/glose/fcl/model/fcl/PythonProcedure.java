/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Python Procedure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.PythonProcedure#getScriptPath <em>Script Path</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.PythonProcedure#getMethodQualifiedName <em>Method Qualified Name</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getPythonProcedure()
 * @model
 * @generated
 */
public interface PythonProcedure extends ExternalProcedure {
	/**
	 * Returns the value of the '<em><b>Script Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Script Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Script Path</em>' attribute.
	 * @see #setScriptPath(String)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getPythonProcedure_ScriptPath()
	 * @model
	 * @generated
	 */
	String getScriptPath();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.PythonProcedure#getScriptPath <em>Script Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Script Path</em>' attribute.
	 * @see #getScriptPath()
	 * @generated
	 */
	void setScriptPath(String value);

	/**
	 * Returns the value of the '<em><b>Method Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Method Qualified Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method Qualified Name</em>' attribute.
	 * @see #setMethodQualifiedName(String)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getPythonProcedure_MethodQualifiedName()
	 * @model
	 * @generated
	 */
	String getMethodQualifiedName();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.PythonProcedure#getMethodQualifiedName <em>Method Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method Qualified Name</em>' attribute.
	 * @see #getMethodQualifiedName()
	 * @generated
	 */
	void setMethodQualifiedName(String value);

} // PythonProcedure
