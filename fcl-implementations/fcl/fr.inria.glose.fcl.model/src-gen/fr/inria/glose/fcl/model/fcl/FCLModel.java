/**
 */
package fr.inria.glose.fcl.model.fcl;

import fr.inria.glose.fcl.model.fcl.interpreter_vm.EventOccurrence;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FCL Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FCLModel#getModeAutomata <em>Mode Automata</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FCLModel#getEvents <em>Events</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FCLModel#getProcedures <em>Procedures</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FCLModel#getDataTypes <em>Data Types</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FCLModel#getDataflow <em>Dataflow</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FCLModel#getReceivedEvents <em>Received Events</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FCLModel#getProcedureStack <em>Procedure Stack</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FCLModel#getIndentation <em>Indentation</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFCLModel()
 * @model
 * @generated
 */
public interface FCLModel extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Mode Automata</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mode Automata</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mode Automata</em>' containment reference.
	 * @see #setModeAutomata(ModeAutomata)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFCLModel_ModeAutomata()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ModeAutomata getModeAutomata();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.FCLModel#getModeAutomata <em>Mode Automata</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mode Automata</em>' containment reference.
	 * @see #getModeAutomata()
	 * @generated
	 */
	void setModeAutomata(ModeAutomata value);

	/**
	 * Returns the value of the '<em><b>Events</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' containment reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFCLModel_Events()
	 * @model containment="true"
	 * @generated
	 */
	EList<Event> getEvents();

	/**
	 * Returns the value of the '<em><b>Procedures</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.Procedure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedures</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedures</em>' containment reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFCLModel_Procedures()
	 * @model containment="true"
	 * @generated
	 */
	EList<Procedure> getProcedures();

	/**
	 * Returns the value of the '<em><b>Data Types</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.DataType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Types</em>' containment reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFCLModel_DataTypes()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataType> getDataTypes();

	/**
	 * Returns the value of the '<em><b>Dataflow</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dataflow</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dataflow</em>' containment reference.
	 * @see #setDataflow(Function)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFCLModel_Dataflow()
	 * @model containment="true"
	 * @generated
	 */
	Function getDataflow();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.FCLModel#getDataflow <em>Dataflow</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dataflow</em>' containment reference.
	 * @see #getDataflow()
	 * @generated
	 */
	void setDataflow(Function value);

	/**
	 * Returns the value of the '<em><b>Received Events</b></em>' reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.interpreter_vm.EventOccurrence}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Received Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Received Events</em>' reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFCLModel_ReceivedEvents()
	 * @model
	 * @generated
	 */
	EList<EventOccurrence> getReceivedEvents();

	/**
	 * Returns the value of the '<em><b>Procedure Stack</b></em>' reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Stack</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Stack</em>' reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFCLModel_ProcedureStack()
	 * @model
	 * @generated
	 */
	EList<ProcedureContext> getProcedureStack();

	/**
	 * Returns the value of the '<em><b>Indentation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Indentation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Indentation</em>' attribute.
	 * @see #setIndentation(String)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFCLModel_Indentation()
	 * @model
	 * @generated
	 */
	String getIndentation();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.FCLModel#getIndentation <em>Indentation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Indentation</em>' attribute.
	 * @see #getIndentation()
	 * @generated
	 */
	void setIndentation(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<Function> allFunctions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<FunctionConnector> allConnectors();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void propagateDelayedResults();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void evaluateModeAutomata();

} // FCLModel
