/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Block Data Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort#getDefaultInputValue <em>Default Input Value</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort#getFunctionVarStore <em>Function Var Store</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort#getCurrentValue <em>Current Value</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunctionBlockDataPort()
 * @model
 * @generated
 */
public interface FunctionBlockDataPort extends FunctionPort {
	/**
	 * Returns the value of the '<em><b>Default Input Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Input Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Input Value</em>' containment reference.
	 * @see #setDefaultInputValue(Expression)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunctionBlockDataPort_DefaultInputValue()
	 * @model containment="true"
	 * @generated
	 */
	Expression getDefaultInputValue();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort#getDefaultInputValue <em>Default Input Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Input Value</em>' containment reference.
	 * @see #getDefaultInputValue()
	 * @generated
	 */
	void setDefaultInputValue(Expression value);

	/**
	 * Returns the value of the '<em><b>Function Var Store</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Var Store</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Var Store</em>' reference.
	 * @see #setFunctionVarStore(FunctionVarDecl)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunctionBlockDataPort_FunctionVarStore()
	 * @model
	 * @generated
	 */
	FunctionVarDecl getFunctionVarStore();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort#getFunctionVarStore <em>Function Var Store</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Var Store</em>' reference.
	 * @see #getFunctionVarStore()
	 * @generated
	 */
	void setFunctionVarStore(FunctionVarDecl value);

	/**
	 * Returns the value of the '<em><b>Current Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Value</em>' reference.
	 * @see #setCurrentValue(Value)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunctionBlockDataPort_CurrentValue()
	 * @model
	 * @generated
	 */
	Value getCurrentValue();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort#getCurrentValue <em>Current Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Value</em>' reference.
	 * @see #getCurrentValue()
	 * @generated
	 */
	void setCurrentValue(Value value);

} // FunctionBlockDataPort
