/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.Action;
import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.Function;
import fr.inria.glose.fcl.model.fcl.Mode;
import fr.inria.glose.fcl.model.fcl.Transition;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mode</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.ModeImpl#getEntry <em>Entry</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.ModeImpl#getExit <em>Exit</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.ModeImpl#isFinal <em>Final</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.ModeImpl#getEnabledFunctions <em>Enabled Functions</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.ModeImpl#getIncomingTransition <em>Incoming Transition</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.ModeImpl#getOutgoingTransition <em>Outgoing Transition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModeImpl extends NamedElementImpl implements Mode {
	/**
	 * The cached value of the '{@link #getEntry() <em>Entry</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntry()
	 * @generated
	 * @ordered
	 */
	protected Action entry;

	/**
	 * The cached value of the '{@link #getExit() <em>Exit</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExit()
	 * @generated
	 * @ordered
	 */
	protected Action exit;

	/**
	 * The default value of the '{@link #isFinal() <em>Final</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFinal()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FINAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isFinal() <em>Final</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFinal()
	 * @generated
	 * @ordered
	 */
	protected boolean final_ = FINAL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEnabledFunctions() <em>Enabled Functions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnabledFunctions()
	 * @generated
	 * @ordered
	 */
	protected EList<Function> enabledFunctions;

	/**
	 * The cached value of the '{@link #getIncomingTransition() <em>Incoming Transition</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncomingTransition()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> incomingTransition;

	/**
	 * The cached value of the '{@link #getOutgoingTransition() <em>Outgoing Transition</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutgoingTransition()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> outgoingTransition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.MODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getEntry() {
		return entry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEntry(Action newEntry, NotificationChain msgs) {
		Action oldEntry = entry;
		entry = newEntry;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FclPackage.MODE__ENTRY,
					oldEntry, newEntry);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntry(Action newEntry) {
		if (newEntry != entry) {
			NotificationChain msgs = null;
			if (entry != null)
				msgs = ((InternalEObject) entry).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FclPackage.MODE__ENTRY,
						null, msgs);
			if (newEntry != null)
				msgs = ((InternalEObject) newEntry).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FclPackage.MODE__ENTRY,
						null, msgs);
			msgs = basicSetEntry(newEntry, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.MODE__ENTRY, newEntry, newEntry));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getExit() {
		return exit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExit(Action newExit, NotificationChain msgs) {
		Action oldExit = exit;
		exit = newExit;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FclPackage.MODE__EXIT,
					oldExit, newExit);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExit(Action newExit) {
		if (newExit != exit) {
			NotificationChain msgs = null;
			if (exit != null)
				msgs = ((InternalEObject) exit).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FclPackage.MODE__EXIT,
						null, msgs);
			if (newExit != null)
				msgs = ((InternalEObject) newExit).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FclPackage.MODE__EXIT,
						null, msgs);
			msgs = basicSetExit(newExit, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.MODE__EXIT, newExit, newExit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isFinal() {
		return final_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinal(boolean newFinal) {
		boolean oldFinal = final_;
		final_ = newFinal;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.MODE__FINAL, oldFinal, final_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Function> getEnabledFunctions() {
		if (enabledFunctions == null) {
			enabledFunctions = new EObjectResolvingEList<Function>(Function.class, this,
					FclPackage.MODE__ENABLED_FUNCTIONS);
		}
		return enabledFunctions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getIncomingTransition() {
		if (incomingTransition == null) {
			incomingTransition = new EObjectWithInverseResolvingEList<Transition>(Transition.class, this,
					FclPackage.MODE__INCOMING_TRANSITION, FclPackage.TRANSITION__TARGET);
		}
		return incomingTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getOutgoingTransition() {
		if (outgoingTransition == null) {
			outgoingTransition = new EObjectWithInverseResolvingEList<Transition>(Transition.class, this,
					FclPackage.MODE__OUTGOING_TRANSITION, FclPackage.TRANSITION__SOURCE);
		}
		return outgoingTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FclPackage.MODE__INCOMING_TRANSITION:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getIncomingTransition()).basicAdd(otherEnd,
					msgs);
		case FclPackage.MODE__OUTGOING_TRANSITION:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getOutgoingTransition()).basicAdd(otherEnd,
					msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FclPackage.MODE__ENTRY:
			return basicSetEntry(null, msgs);
		case FclPackage.MODE__EXIT:
			return basicSetExit(null, msgs);
		case FclPackage.MODE__INCOMING_TRANSITION:
			return ((InternalEList<?>) getIncomingTransition()).basicRemove(otherEnd, msgs);
		case FclPackage.MODE__OUTGOING_TRANSITION:
			return ((InternalEList<?>) getOutgoingTransition()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.MODE__ENTRY:
			return getEntry();
		case FclPackage.MODE__EXIT:
			return getExit();
		case FclPackage.MODE__FINAL:
			return isFinal();
		case FclPackage.MODE__ENABLED_FUNCTIONS:
			return getEnabledFunctions();
		case FclPackage.MODE__INCOMING_TRANSITION:
			return getIncomingTransition();
		case FclPackage.MODE__OUTGOING_TRANSITION:
			return getOutgoingTransition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.MODE__ENTRY:
			setEntry((Action) newValue);
			return;
		case FclPackage.MODE__EXIT:
			setExit((Action) newValue);
			return;
		case FclPackage.MODE__FINAL:
			setFinal((Boolean) newValue);
			return;
		case FclPackage.MODE__ENABLED_FUNCTIONS:
			getEnabledFunctions().clear();
			getEnabledFunctions().addAll((Collection<? extends Function>) newValue);
			return;
		case FclPackage.MODE__INCOMING_TRANSITION:
			getIncomingTransition().clear();
			getIncomingTransition().addAll((Collection<? extends Transition>) newValue);
			return;
		case FclPackage.MODE__OUTGOING_TRANSITION:
			getOutgoingTransition().clear();
			getOutgoingTransition().addAll((Collection<? extends Transition>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.MODE__ENTRY:
			setEntry((Action) null);
			return;
		case FclPackage.MODE__EXIT:
			setExit((Action) null);
			return;
		case FclPackage.MODE__FINAL:
			setFinal(FINAL_EDEFAULT);
			return;
		case FclPackage.MODE__ENABLED_FUNCTIONS:
			getEnabledFunctions().clear();
			return;
		case FclPackage.MODE__INCOMING_TRANSITION:
			getIncomingTransition().clear();
			return;
		case FclPackage.MODE__OUTGOING_TRANSITION:
			getOutgoingTransition().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.MODE__ENTRY:
			return entry != null;
		case FclPackage.MODE__EXIT:
			return exit != null;
		case FclPackage.MODE__FINAL:
			return final_ != FINAL_EDEFAULT;
		case FclPackage.MODE__ENABLED_FUNCTIONS:
			return enabledFunctions != null && !enabledFunctions.isEmpty();
		case FclPackage.MODE__INCOMING_TRANSITION:
			return incomingTransition != null && !incomingTransition.isEmpty();
		case FclPackage.MODE__OUTGOING_TRANSITION:
			return outgoingTransition != null && !outgoingTransition.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (final: ");
		result.append(final_);
		result.append(')');
		return result.toString();
	}

} //ModeImpl
