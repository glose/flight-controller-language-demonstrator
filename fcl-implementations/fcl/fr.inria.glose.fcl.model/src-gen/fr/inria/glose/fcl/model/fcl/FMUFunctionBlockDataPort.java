/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FMU Function Block Data Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort#getRunnableFmuVariableName <em>Runnable Fmu Variable Name</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFMUFunctionBlockDataPort()
 * @model
 * @generated
 */
public interface FMUFunctionBlockDataPort<T> extends FunctionBlockDataPort {
	/**
	 * Returns the value of the '<em><b>Runnable Fmu Variable Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runnable Fmu Variable Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runnable Fmu Variable Name</em>' attribute.
	 * @see #setRunnableFmuVariableName(String)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFMUFunctionBlockDataPort_RunnableFmuVariableName()
	 * @model
	 * @generated
	 */
	String getRunnableFmuVariableName();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort#getRunnableFmuVariableName <em>Runnable Fmu Variable Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Runnable Fmu Variable Name</em>' attribute.
	 * @see #getRunnableFmuVariableName()
	 * @generated
	 */
	void setRunnableFmuVariableName(String value);

} // FMUFunctionBlockDataPort
