/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment;
import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.NewDataStruct;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>New Data Struct</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.NewDataStructImpl#getPropertyAssignments <em>Property Assignments</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NewDataStructImpl extends ExpressionImpl implements NewDataStruct {
	/**
	 * The cached value of the '{@link #getPropertyAssignments() <em>Property Assignments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyAssignments()
	 * @generated
	 * @ordered
	 */
	protected EList<DataStructPropertyAssignment> propertyAssignments;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NewDataStructImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.NEW_DATA_STRUCT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataStructPropertyAssignment> getPropertyAssignments() {
		if (propertyAssignments == null) {
			propertyAssignments = new EObjectContainmentEList<DataStructPropertyAssignment>(
					DataStructPropertyAssignment.class, this, FclPackage.NEW_DATA_STRUCT__PROPERTY_ASSIGNMENTS);
		}
		return propertyAssignments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FclPackage.NEW_DATA_STRUCT__PROPERTY_ASSIGNMENTS:
			return ((InternalEList<?>) getPropertyAssignments()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.NEW_DATA_STRUCT__PROPERTY_ASSIGNMENTS:
			return getPropertyAssignments();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.NEW_DATA_STRUCT__PROPERTY_ASSIGNMENTS:
			getPropertyAssignments().clear();
			getPropertyAssignments().addAll((Collection<? extends DataStructPropertyAssignment>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.NEW_DATA_STRUCT__PROPERTY_ASSIGNMENTS:
			getPropertyAssignments().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.NEW_DATA_STRUCT__PROPERTY_ASSIGNMENTS:
			return propertyAssignments != null && !propertyAssignments.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //NewDataStructImpl
