/**
 */
package fr.inria.glose.fcl.model.fcl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Procedure Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.ProcedureCall#getProcedure <em>Procedure</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.ProcedureCall#getProcedureCallArguments <em>Procedure Call Arguments</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getProcedureCall()
 * @model
 * @generated
 */
public interface ProcedureCall extends PrimitiveAction, Expression {
	/**
	 * Returns the value of the '<em><b>Procedure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure</em>' reference.
	 * @see #setProcedure(Procedure)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getProcedureCall_Procedure()
	 * @model required="true"
	 * @generated
	 */
	Procedure getProcedure();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.ProcedureCall#getProcedure <em>Procedure</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure</em>' reference.
	 * @see #getProcedure()
	 * @generated
	 */
	void setProcedure(Procedure value);

	/**
	 * Returns the value of the '<em><b>Procedure Call Arguments</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.Expression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Call Arguments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Call Arguments</em>' containment reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getProcedureCall_ProcedureCallArguments()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getProcedureCallArguments();

} // ProcedureCall
