/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FCL Procedure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FCLProcedure#getAction <em>Action</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFCLProcedure()
 * @model
 * @generated
 */
public interface FCLProcedure extends Procedure {
	/**
	 * Returns the value of the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' containment reference.
	 * @see #setAction(Action)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFCLProcedure_Action()
	 * @model containment="true"
	 * @generated
	 */
	Action getAction();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.FCLProcedure#getAction <em>Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' containment reference.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(Action value);

} // FCLProcedure
