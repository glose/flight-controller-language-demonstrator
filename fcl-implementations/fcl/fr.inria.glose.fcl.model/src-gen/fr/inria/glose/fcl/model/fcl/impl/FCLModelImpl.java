/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.Event;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.Function;
import fr.inria.glose.fcl.model.fcl.FunctionConnector;
import fr.inria.glose.fcl.model.fcl.ModeAutomata;
import fr.inria.glose.fcl.model.fcl.Procedure;

import fr.inria.glose.fcl.model.fcl.interpreter_vm.EventOccurrence;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FCL Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FCLModelImpl#getModeAutomata <em>Mode Automata</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FCLModelImpl#getEvents <em>Events</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FCLModelImpl#getProcedures <em>Procedures</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FCLModelImpl#getDataTypes <em>Data Types</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FCLModelImpl#getDataflow <em>Dataflow</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FCLModelImpl#getReceivedEvents <em>Received Events</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FCLModelImpl#getProcedureStack <em>Procedure Stack</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FCLModelImpl#getIndentation <em>Indentation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FCLModelImpl extends NamedElementImpl implements FCLModel {
	/**
	 * The cached value of the '{@link #getModeAutomata() <em>Mode Automata</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModeAutomata()
	 * @generated
	 * @ordered
	 */
	protected ModeAutomata modeAutomata;

	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> events;

	/**
	 * The cached value of the '{@link #getProcedures() <em>Procedures</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcedures()
	 * @generated
	 * @ordered
	 */
	protected EList<Procedure> procedures;

	/**
	 * The cached value of the '{@link #getDataTypes() <em>Data Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<DataType> dataTypes;

	/**
	 * The cached value of the '{@link #getDataflow() <em>Dataflow</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataflow()
	 * @generated
	 * @ordered
	 */
	protected Function dataflow;

	/**
	 * The cached value of the '{@link #getReceivedEvents() <em>Received Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReceivedEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<EventOccurrence> receivedEvents;

	/**
	 * The cached value of the '{@link #getProcedureStack() <em>Procedure Stack</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcedureStack()
	 * @generated
	 * @ordered
	 */
	protected EList<ProcedureContext> procedureStack;

	/**
	 * The default value of the '{@link #getIndentation() <em>Indentation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndentation()
	 * @generated
	 * @ordered
	 */
	protected static final String INDENTATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIndentation() <em>Indentation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndentation()
	 * @generated
	 * @ordered
	 */
	protected String indentation = INDENTATION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FCLModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.FCL_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModeAutomata getModeAutomata() {
		return modeAutomata;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModeAutomata(ModeAutomata newModeAutomata, NotificationChain msgs) {
		ModeAutomata oldModeAutomata = modeAutomata;
		modeAutomata = newModeAutomata;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					FclPackage.FCL_MODEL__MODE_AUTOMATA, oldModeAutomata, newModeAutomata);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModeAutomata(ModeAutomata newModeAutomata) {
		if (newModeAutomata != modeAutomata) {
			NotificationChain msgs = null;
			if (modeAutomata != null)
				msgs = ((InternalEObject) modeAutomata).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.FCL_MODEL__MODE_AUTOMATA, null, msgs);
			if (newModeAutomata != null)
				msgs = ((InternalEObject) newModeAutomata).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.FCL_MODEL__MODE_AUTOMATA, null, msgs);
			msgs = basicSetModeAutomata(newModeAutomata, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.FCL_MODEL__MODE_AUTOMATA, newModeAutomata,
					newModeAutomata));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEvents() {
		if (events == null) {
			events = new EObjectContainmentEList<Event>(Event.class, this, FclPackage.FCL_MODEL__EVENTS);
		}
		return events;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Procedure> getProcedures() {
		if (procedures == null) {
			procedures = new EObjectContainmentEList<Procedure>(Procedure.class, this,
					FclPackage.FCL_MODEL__PROCEDURES);
		}
		return procedures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataType> getDataTypes() {
		if (dataTypes == null) {
			dataTypes = new EObjectContainmentEList<DataType>(DataType.class, this, FclPackage.FCL_MODEL__DATA_TYPES);
		}
		return dataTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Function getDataflow() {
		return dataflow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataflow(Function newDataflow, NotificationChain msgs) {
		Function oldDataflow = dataflow;
		dataflow = newDataflow;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					FclPackage.FCL_MODEL__DATAFLOW, oldDataflow, newDataflow);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataflow(Function newDataflow) {
		if (newDataflow != dataflow) {
			NotificationChain msgs = null;
			if (dataflow != null)
				msgs = ((InternalEObject) dataflow).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.FCL_MODEL__DATAFLOW, null, msgs);
			if (newDataflow != null)
				msgs = ((InternalEObject) newDataflow).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.FCL_MODEL__DATAFLOW, null, msgs);
			msgs = basicSetDataflow(newDataflow, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.FCL_MODEL__DATAFLOW, newDataflow,
					newDataflow));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EventOccurrence> getReceivedEvents() {
		if (receivedEvents == null) {
			receivedEvents = new EObjectResolvingEList<EventOccurrence>(EventOccurrence.class, this,
					FclPackage.FCL_MODEL__RECEIVED_EVENTS);
		}
		return receivedEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcedureContext> getProcedureStack() {
		if (procedureStack == null) {
			procedureStack = new EObjectResolvingEList<ProcedureContext>(ProcedureContext.class, this,
					FclPackage.FCL_MODEL__PROCEDURE_STACK);
		}
		return procedureStack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getIndentation() {
		return indentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndentation(String newIndentation) {
		String oldIndentation = indentation;
		indentation = newIndentation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.FCL_MODEL__INDENTATION, oldIndentation,
					indentation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Function> allFunctions() {
		BasicEList<Function> l = new BasicEList<Function>();
		l.add(this.dataflow);
		l.addAll(this.dataflow.allSubFunctions());
		return l;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FunctionConnector> allConnectors() {
		BasicEList<FunctionConnector> l = new BasicEList<FunctionConnector>();
		for (Function f : this.allFunctions()) {
			l.addAll(f.getFunctionConnectors());
		}
		return l;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void propagateDelayedResults() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void evaluateModeAutomata() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FclPackage.FCL_MODEL__MODE_AUTOMATA:
			return basicSetModeAutomata(null, msgs);
		case FclPackage.FCL_MODEL__EVENTS:
			return ((InternalEList<?>) getEvents()).basicRemove(otherEnd, msgs);
		case FclPackage.FCL_MODEL__PROCEDURES:
			return ((InternalEList<?>) getProcedures()).basicRemove(otherEnd, msgs);
		case FclPackage.FCL_MODEL__DATA_TYPES:
			return ((InternalEList<?>) getDataTypes()).basicRemove(otherEnd, msgs);
		case FclPackage.FCL_MODEL__DATAFLOW:
			return basicSetDataflow(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.FCL_MODEL__MODE_AUTOMATA:
			return getModeAutomata();
		case FclPackage.FCL_MODEL__EVENTS:
			return getEvents();
		case FclPackage.FCL_MODEL__PROCEDURES:
			return getProcedures();
		case FclPackage.FCL_MODEL__DATA_TYPES:
			return getDataTypes();
		case FclPackage.FCL_MODEL__DATAFLOW:
			return getDataflow();
		case FclPackage.FCL_MODEL__RECEIVED_EVENTS:
			return getReceivedEvents();
		case FclPackage.FCL_MODEL__PROCEDURE_STACK:
			return getProcedureStack();
		case FclPackage.FCL_MODEL__INDENTATION:
			return getIndentation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.FCL_MODEL__MODE_AUTOMATA:
			setModeAutomata((ModeAutomata) newValue);
			return;
		case FclPackage.FCL_MODEL__EVENTS:
			getEvents().clear();
			getEvents().addAll((Collection<? extends Event>) newValue);
			return;
		case FclPackage.FCL_MODEL__PROCEDURES:
			getProcedures().clear();
			getProcedures().addAll((Collection<? extends Procedure>) newValue);
			return;
		case FclPackage.FCL_MODEL__DATA_TYPES:
			getDataTypes().clear();
			getDataTypes().addAll((Collection<? extends DataType>) newValue);
			return;
		case FclPackage.FCL_MODEL__DATAFLOW:
			setDataflow((Function) newValue);
			return;
		case FclPackage.FCL_MODEL__RECEIVED_EVENTS:
			getReceivedEvents().clear();
			getReceivedEvents().addAll((Collection<? extends EventOccurrence>) newValue);
			return;
		case FclPackage.FCL_MODEL__PROCEDURE_STACK:
			getProcedureStack().clear();
			getProcedureStack().addAll((Collection<? extends ProcedureContext>) newValue);
			return;
		case FclPackage.FCL_MODEL__INDENTATION:
			setIndentation((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.FCL_MODEL__MODE_AUTOMATA:
			setModeAutomata((ModeAutomata) null);
			return;
		case FclPackage.FCL_MODEL__EVENTS:
			getEvents().clear();
			return;
		case FclPackage.FCL_MODEL__PROCEDURES:
			getProcedures().clear();
			return;
		case FclPackage.FCL_MODEL__DATA_TYPES:
			getDataTypes().clear();
			return;
		case FclPackage.FCL_MODEL__DATAFLOW:
			setDataflow((Function) null);
			return;
		case FclPackage.FCL_MODEL__RECEIVED_EVENTS:
			getReceivedEvents().clear();
			return;
		case FclPackage.FCL_MODEL__PROCEDURE_STACK:
			getProcedureStack().clear();
			return;
		case FclPackage.FCL_MODEL__INDENTATION:
			setIndentation(INDENTATION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.FCL_MODEL__MODE_AUTOMATA:
			return modeAutomata != null;
		case FclPackage.FCL_MODEL__EVENTS:
			return events != null && !events.isEmpty();
		case FclPackage.FCL_MODEL__PROCEDURES:
			return procedures != null && !procedures.isEmpty();
		case FclPackage.FCL_MODEL__DATA_TYPES:
			return dataTypes != null && !dataTypes.isEmpty();
		case FclPackage.FCL_MODEL__DATAFLOW:
			return dataflow != null;
		case FclPackage.FCL_MODEL__RECEIVED_EVENTS:
			return receivedEvents != null && !receivedEvents.isEmpty();
		case FclPackage.FCL_MODEL__PROCEDURE_STACK:
			return procedureStack != null && !procedureStack.isEmpty();
		case FclPackage.FCL_MODEL__INDENTATION:
			return INDENTATION_EDEFAULT == null ? indentation != null : !INDENTATION_EDEFAULT.equals(indentation);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case FclPackage.FCL_MODEL___ALL_FUNCTIONS:
			return allFunctions();
		case FclPackage.FCL_MODEL___ALL_CONNECTORS:
			return allConnectors();
		case FclPackage.FCL_MODEL___PROPAGATE_DELAYED_RESULTS:
			propagateDelayedResults();
			return null;
		case FclPackage.FCL_MODEL___EVALUATE_MODE_AUTOMATA:
			evaluateModeAutomata();
			return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (indentation: ");
		result.append(indentation);
		result.append(')');
		return result.toString();
	}

} //FCLModelImpl
