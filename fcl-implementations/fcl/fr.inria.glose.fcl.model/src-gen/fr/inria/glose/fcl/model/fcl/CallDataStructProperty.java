/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Data Struct Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.CallDataStructProperty#getRootTargetDataStruct <em>Root Target Data Struct</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getCallDataStructProperty()
 * @model
 * @generated
 */
public interface CallDataStructProperty extends DataStructPropertyReference, Call {

	/**
	 * Returns the value of the '<em><b>Root Target Data Struct</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Target Data Struct</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Target Data Struct</em>' containment reference.
	 * @see #setRootTargetDataStruct(CallDeclarationReference)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getCallDataStructProperty_RootTargetDataStruct()
	 * @model containment="true"
	 * @generated
	 */
	CallDeclarationReference getRootTargetDataStruct();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.CallDataStructProperty#getRootTargetDataStruct <em>Root Target Data Struct</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Target Data Struct</em>' containment reference.
	 * @see #getRootTargetDataStruct()
	 * @generated
	 */
	void setRootTargetDataStruct(CallDeclarationReference value);
} // CallDataStructProperty
