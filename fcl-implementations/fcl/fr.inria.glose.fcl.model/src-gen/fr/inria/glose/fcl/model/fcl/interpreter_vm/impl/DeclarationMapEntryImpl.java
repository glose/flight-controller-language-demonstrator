/**
 */
package fr.inria.glose.fcl.model.fcl.interpreter_vm.impl;

import fr.inria.glose.fcl.model.fcl.CallableDeclaration;
import fr.inria.glose.fcl.model.fcl.Value;

import fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Declaration Map Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.DeclarationMapEntryImpl#getCallableDeclaration <em>Callable Declaration</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.DeclarationMapEntryImpl#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeclarationMapEntryImpl extends MinimalEObjectImpl.Container implements DeclarationMapEntry {
	/**
	 * The cached value of the '{@link #getCallableDeclaration() <em>Callable Declaration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCallableDeclaration()
	 * @generated
	 * @ordered
	 */
	protected CallableDeclaration callableDeclaration;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected Value value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeclarationMapEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Interpreter_vmPackage.Literals.DECLARATION_MAP_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallableDeclaration getCallableDeclaration() {
		if (callableDeclaration != null && callableDeclaration.eIsProxy()) {
			InternalEObject oldCallableDeclaration = (InternalEObject) callableDeclaration;
			callableDeclaration = (CallableDeclaration) eResolveProxy(oldCallableDeclaration);
			if (callableDeclaration != oldCallableDeclaration) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Interpreter_vmPackage.DECLARATION_MAP_ENTRY__CALLABLE_DECLARATION, oldCallableDeclaration,
							callableDeclaration));
			}
		}
		return callableDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallableDeclaration basicGetCallableDeclaration() {
		return callableDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCallableDeclaration(CallableDeclaration newCallableDeclaration) {
		CallableDeclaration oldCallableDeclaration = callableDeclaration;
		callableDeclaration = newCallableDeclaration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Interpreter_vmPackage.DECLARATION_MAP_ENTRY__CALLABLE_DECLARATION, oldCallableDeclaration,
					callableDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValue(Value newValue, NotificationChain msgs) {
		Value oldValue = value;
		value = newValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					Interpreter_vmPackage.DECLARATION_MAP_ENTRY__VALUE, oldValue, newValue);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(Value newValue) {
		if (newValue != value) {
			NotificationChain msgs = null;
			if (value != null)
				msgs = ((InternalEObject) value).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - Interpreter_vmPackage.DECLARATION_MAP_ENTRY__VALUE, null, msgs);
			if (newValue != null)
				msgs = ((InternalEObject) newValue).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - Interpreter_vmPackage.DECLARATION_MAP_ENTRY__VALUE, null, msgs);
			msgs = basicSetValue(newValue, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Interpreter_vmPackage.DECLARATION_MAP_ENTRY__VALUE,
					newValue, newValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Interpreter_vmPackage.DECLARATION_MAP_ENTRY__VALUE:
			return basicSetValue(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Interpreter_vmPackage.DECLARATION_MAP_ENTRY__CALLABLE_DECLARATION:
			if (resolve)
				return getCallableDeclaration();
			return basicGetCallableDeclaration();
		case Interpreter_vmPackage.DECLARATION_MAP_ENTRY__VALUE:
			return getValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Interpreter_vmPackage.DECLARATION_MAP_ENTRY__CALLABLE_DECLARATION:
			setCallableDeclaration((CallableDeclaration) newValue);
			return;
		case Interpreter_vmPackage.DECLARATION_MAP_ENTRY__VALUE:
			setValue((Value) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Interpreter_vmPackage.DECLARATION_MAP_ENTRY__CALLABLE_DECLARATION:
			setCallableDeclaration((CallableDeclaration) null);
			return;
		case Interpreter_vmPackage.DECLARATION_MAP_ENTRY__VALUE:
			setValue((Value) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Interpreter_vmPackage.DECLARATION_MAP_ENTRY__CALLABLE_DECLARATION:
			return callableDeclaration != null;
		case Interpreter_vmPackage.DECLARATION_MAP_ENTRY__VALUE:
			return value != null;
		}
		return super.eIsSet(featureID);
	}

} //DeclarationMapEntryImpl
