/**
 */
package fr.inria.glose.fcl.model.fcl;

import fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unity Function</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.UnityFunction#getUnityWebPagePath <em>Unity Web Page Path</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.UnityFunction#getUnityWrapper <em>Unity Wrapper</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getUnityFunction()
 * @model
 * @generated
 */
public interface UnityFunction extends Function {
	/**
	 * Returns the value of the '<em><b>Unity Web Page Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unity Web Page Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unity Web Page Path</em>' attribute.
	 * @see #setUnityWebPagePath(String)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getUnityFunction_UnityWebPagePath()
	 * @model
	 * @generated
	 */
	String getUnityWebPagePath();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.UnityFunction#getUnityWebPagePath <em>Unity Web Page Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unity Web Page Path</em>' attribute.
	 * @see #getUnityWebPagePath()
	 * @generated
	 */
	void setUnityWebPagePath(String value);

	/**
	 * Returns the value of the '<em><b>Unity Wrapper</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unity Wrapper</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unity Wrapper</em>' reference.
	 * @see #setUnityWrapper(UnityWrapper)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getUnityFunction_UnityWrapper()
	 * @model
	 * @generated
	 */
	UnityWrapper getUnityWrapper();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.UnityFunction#getUnityWrapper <em>Unity Wrapper</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unity Wrapper</em>' reference.
	 * @see #getUnityWrapper()
	 * @generated
	 */
	void setUnityWrapper(UnityWrapper value);

} // UnityFunction
