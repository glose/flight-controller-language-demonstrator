/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.Action;
import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.Function;
import fr.inria.glose.fcl.model.fcl.FunctionConnector;
import fr.inria.glose.fcl.model.fcl.FunctionPort;
import fr.inria.glose.fcl.model.fcl.FunctionTimeReference;
import fr.inria.glose.fcl.model.fcl.FunctionVarDecl;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FunctionImpl#getFunctionPorts <em>Function Ports</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FunctionImpl#getAction <em>Action</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FunctionImpl#getSubFunctions <em>Sub Functions</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FunctionImpl#getFunctionConnectors <em>Function Connectors</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FunctionImpl#getFunctionVarDecls <em>Function Var Decls</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FunctionImpl#getTimeReference <em>Time Reference</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionImpl extends NamedElementImpl implements Function {
	/**
	 * The cached value of the '{@link #getFunctionPorts() <em>Function Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<FunctionPort> functionPorts;

	/**
	 * The cached value of the '{@link #getAction() <em>Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAction()
	 * @generated
	 * @ordered
	 */
	protected Action action;

	/**
	 * The cached value of the '{@link #getSubFunctions() <em>Sub Functions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubFunctions()
	 * @generated
	 * @ordered
	 */
	protected EList<Function> subFunctions;

	/**
	 * The cached value of the '{@link #getFunctionConnectors() <em>Function Connectors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionConnectors()
	 * @generated
	 * @ordered
	 */
	protected EList<FunctionConnector> functionConnectors;

	/**
	 * The cached value of the '{@link #getFunctionVarDecls() <em>Function Var Decls</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionVarDecls()
	 * @generated
	 * @ordered
	 */
	protected EList<FunctionVarDecl> functionVarDecls;

	/**
	 * The cached value of the '{@link #getTimeReference() <em>Time Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeReference()
	 * @generated
	 * @ordered
	 */
	protected FunctionTimeReference timeReference;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.FUNCTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionPort> getFunctionPorts() {
		if (functionPorts == null) {
			functionPorts = new EObjectContainmentEList<FunctionPort>(FunctionPort.class, this,
					FclPackage.FUNCTION__FUNCTION_PORTS);
		}
		return functionPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getAction() {
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAction(Action newAction, NotificationChain msgs) {
		Action oldAction = action;
		action = newAction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FclPackage.FUNCTION__ACTION,
					oldAction, newAction);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAction(Action newAction) {
		if (newAction != action) {
			NotificationChain msgs = null;
			if (action != null)
				msgs = ((InternalEObject) action).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.FUNCTION__ACTION, null, msgs);
			if (newAction != null)
				msgs = ((InternalEObject) newAction).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.FUNCTION__ACTION, null, msgs);
			msgs = basicSetAction(newAction, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.FUNCTION__ACTION, newAction, newAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Function> getSubFunctions() {
		if (subFunctions == null) {
			subFunctions = new EObjectContainmentEList<Function>(Function.class, this,
					FclPackage.FUNCTION__SUB_FUNCTIONS);
		}
		return subFunctions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionConnector> getFunctionConnectors() {
		if (functionConnectors == null) {
			functionConnectors = new EObjectContainmentEList<FunctionConnector>(FunctionConnector.class, this,
					FclPackage.FUNCTION__FUNCTION_CONNECTORS);
		}
		return functionConnectors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionVarDecl> getFunctionVarDecls() {
		if (functionVarDecls == null) {
			functionVarDecls = new EObjectContainmentEList<FunctionVarDecl>(FunctionVarDecl.class, this,
					FclPackage.FUNCTION__FUNCTION_VAR_DECLS);
		}
		return functionVarDecls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionTimeReference getTimeReference() {
		return timeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTimeReference(FunctionTimeReference newTimeReference, NotificationChain msgs) {
		FunctionTimeReference oldTimeReference = timeReference;
		timeReference = newTimeReference;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					FclPackage.FUNCTION__TIME_REFERENCE, oldTimeReference, newTimeReference);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeReference(FunctionTimeReference newTimeReference) {
		if (newTimeReference != timeReference) {
			NotificationChain msgs = null;
			if (timeReference != null)
				msgs = ((InternalEObject) timeReference).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.FUNCTION__TIME_REFERENCE, null, msgs);
			if (newTimeReference != null)
				msgs = ((InternalEObject) newTimeReference).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.FUNCTION__TIME_REFERENCE, null, msgs);
			msgs = basicSetTimeReference(newTimeReference, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.FUNCTION__TIME_REFERENCE, newTimeReference,
					newTimeReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Function> allSubFunctions() {
		BasicEList<Function> l = new BasicEList<Function>(this.getSubFunctions());
		for (Function s : this.getSubFunctions()) {
			l.addAll(s.allSubFunctions());
		}
		return l;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void dse_startEvalFunction() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void dse_stopEvalFunction() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEnabled() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FclPackage.FUNCTION__FUNCTION_PORTS:
			return ((InternalEList<?>) getFunctionPorts()).basicRemove(otherEnd, msgs);
		case FclPackage.FUNCTION__ACTION:
			return basicSetAction(null, msgs);
		case FclPackage.FUNCTION__SUB_FUNCTIONS:
			return ((InternalEList<?>) getSubFunctions()).basicRemove(otherEnd, msgs);
		case FclPackage.FUNCTION__FUNCTION_CONNECTORS:
			return ((InternalEList<?>) getFunctionConnectors()).basicRemove(otherEnd, msgs);
		case FclPackage.FUNCTION__FUNCTION_VAR_DECLS:
			return ((InternalEList<?>) getFunctionVarDecls()).basicRemove(otherEnd, msgs);
		case FclPackage.FUNCTION__TIME_REFERENCE:
			return basicSetTimeReference(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.FUNCTION__FUNCTION_PORTS:
			return getFunctionPorts();
		case FclPackage.FUNCTION__ACTION:
			return getAction();
		case FclPackage.FUNCTION__SUB_FUNCTIONS:
			return getSubFunctions();
		case FclPackage.FUNCTION__FUNCTION_CONNECTORS:
			return getFunctionConnectors();
		case FclPackage.FUNCTION__FUNCTION_VAR_DECLS:
			return getFunctionVarDecls();
		case FclPackage.FUNCTION__TIME_REFERENCE:
			return getTimeReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.FUNCTION__FUNCTION_PORTS:
			getFunctionPorts().clear();
			getFunctionPorts().addAll((Collection<? extends FunctionPort>) newValue);
			return;
		case FclPackage.FUNCTION__ACTION:
			setAction((Action) newValue);
			return;
		case FclPackage.FUNCTION__SUB_FUNCTIONS:
			getSubFunctions().clear();
			getSubFunctions().addAll((Collection<? extends Function>) newValue);
			return;
		case FclPackage.FUNCTION__FUNCTION_CONNECTORS:
			getFunctionConnectors().clear();
			getFunctionConnectors().addAll((Collection<? extends FunctionConnector>) newValue);
			return;
		case FclPackage.FUNCTION__FUNCTION_VAR_DECLS:
			getFunctionVarDecls().clear();
			getFunctionVarDecls().addAll((Collection<? extends FunctionVarDecl>) newValue);
			return;
		case FclPackage.FUNCTION__TIME_REFERENCE:
			setTimeReference((FunctionTimeReference) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.FUNCTION__FUNCTION_PORTS:
			getFunctionPorts().clear();
			return;
		case FclPackage.FUNCTION__ACTION:
			setAction((Action) null);
			return;
		case FclPackage.FUNCTION__SUB_FUNCTIONS:
			getSubFunctions().clear();
			return;
		case FclPackage.FUNCTION__FUNCTION_CONNECTORS:
			getFunctionConnectors().clear();
			return;
		case FclPackage.FUNCTION__FUNCTION_VAR_DECLS:
			getFunctionVarDecls().clear();
			return;
		case FclPackage.FUNCTION__TIME_REFERENCE:
			setTimeReference((FunctionTimeReference) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.FUNCTION__FUNCTION_PORTS:
			return functionPorts != null && !functionPorts.isEmpty();
		case FclPackage.FUNCTION__ACTION:
			return action != null;
		case FclPackage.FUNCTION__SUB_FUNCTIONS:
			return subFunctions != null && !subFunctions.isEmpty();
		case FclPackage.FUNCTION__FUNCTION_CONNECTORS:
			return functionConnectors != null && !functionConnectors.isEmpty();
		case FclPackage.FUNCTION__FUNCTION_VAR_DECLS:
			return functionVarDecls != null && !functionVarDecls.isEmpty();
		case FclPackage.FUNCTION__TIME_REFERENCE:
			return timeReference != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case FclPackage.FUNCTION___ALL_SUB_FUNCTIONS:
			return allSubFunctions();
		case FclPackage.FUNCTION___DSE_START_EVAL_FUNCTION:
			dse_startEvalFunction();
			return null;
		case FclPackage.FUNCTION___DSE_STOP_EVAL_FUNCTION:
			dse_stopEvalFunction();
			return null;
		case FclPackage.FUNCTION___IS_ENABLED:
			return isEnabled();
		}
		return super.eInvoke(operationID, arguments);
	}

} //FunctionImpl
