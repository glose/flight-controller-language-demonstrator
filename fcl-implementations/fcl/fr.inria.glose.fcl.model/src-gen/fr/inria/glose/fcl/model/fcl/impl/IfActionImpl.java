/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.Action;
import fr.inria.glose.fcl.model.fcl.Expression;
import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.IfAction;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>If Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.IfActionImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.IfActionImpl#getThenAction <em>Then Action</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.IfActionImpl#getElseAction <em>Else Action</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IfActionImpl extends ControlStructureActionImpl implements IfAction {
	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected Expression condition;

	/**
	 * The cached value of the '{@link #getThenAction() <em>Then Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThenAction()
	 * @generated
	 * @ordered
	 */
	protected Action thenAction;

	/**
	 * The cached value of the '{@link #getElseAction() <em>Else Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElseAction()
	 * @generated
	 * @ordered
	 */
	protected Action elseAction;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IfActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.IF_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getCondition() {
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCondition(Expression newCondition, NotificationChain msgs) {
		Expression oldCondition = condition;
		condition = newCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					FclPackage.IF_ACTION__CONDITION, oldCondition, newCondition);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCondition(Expression newCondition) {
		if (newCondition != condition) {
			NotificationChain msgs = null;
			if (condition != null)
				msgs = ((InternalEObject) condition).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.IF_ACTION__CONDITION, null, msgs);
			if (newCondition != null)
				msgs = ((InternalEObject) newCondition).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.IF_ACTION__CONDITION, null, msgs);
			msgs = basicSetCondition(newCondition, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.IF_ACTION__CONDITION, newCondition,
					newCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getThenAction() {
		return thenAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetThenAction(Action newThenAction, NotificationChain msgs) {
		Action oldThenAction = thenAction;
		thenAction = newThenAction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					FclPackage.IF_ACTION__THEN_ACTION, oldThenAction, newThenAction);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThenAction(Action newThenAction) {
		if (newThenAction != thenAction) {
			NotificationChain msgs = null;
			if (thenAction != null)
				msgs = ((InternalEObject) thenAction).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.IF_ACTION__THEN_ACTION, null, msgs);
			if (newThenAction != null)
				msgs = ((InternalEObject) newThenAction).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.IF_ACTION__THEN_ACTION, null, msgs);
			msgs = basicSetThenAction(newThenAction, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.IF_ACTION__THEN_ACTION, newThenAction,
					newThenAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getElseAction() {
		return elseAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElseAction(Action newElseAction, NotificationChain msgs) {
		Action oldElseAction = elseAction;
		elseAction = newElseAction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					FclPackage.IF_ACTION__ELSE_ACTION, oldElseAction, newElseAction);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElseAction(Action newElseAction) {
		if (newElseAction != elseAction) {
			NotificationChain msgs = null;
			if (elseAction != null)
				msgs = ((InternalEObject) elseAction).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.IF_ACTION__ELSE_ACTION, null, msgs);
			if (newElseAction != null)
				msgs = ((InternalEObject) newElseAction).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.IF_ACTION__ELSE_ACTION, null, msgs);
			msgs = basicSetElseAction(newElseAction, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.IF_ACTION__ELSE_ACTION, newElseAction,
					newElseAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FclPackage.IF_ACTION__CONDITION:
			return basicSetCondition(null, msgs);
		case FclPackage.IF_ACTION__THEN_ACTION:
			return basicSetThenAction(null, msgs);
		case FclPackage.IF_ACTION__ELSE_ACTION:
			return basicSetElseAction(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.IF_ACTION__CONDITION:
			return getCondition();
		case FclPackage.IF_ACTION__THEN_ACTION:
			return getThenAction();
		case FclPackage.IF_ACTION__ELSE_ACTION:
			return getElseAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.IF_ACTION__CONDITION:
			setCondition((Expression) newValue);
			return;
		case FclPackage.IF_ACTION__THEN_ACTION:
			setThenAction((Action) newValue);
			return;
		case FclPackage.IF_ACTION__ELSE_ACTION:
			setElseAction((Action) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.IF_ACTION__CONDITION:
			setCondition((Expression) null);
			return;
		case FclPackage.IF_ACTION__THEN_ACTION:
			setThenAction((Action) null);
			return;
		case FclPackage.IF_ACTION__ELSE_ACTION:
			setElseAction((Action) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.IF_ACTION__CONDITION:
			return condition != null;
		case FclPackage.IF_ACTION__THEN_ACTION:
			return thenAction != null;
		case FclPackage.IF_ACTION__ELSE_ACTION:
			return elseAction != null;
		}
		return super.eIsSet(featureID);
	}

} //IfActionImpl
