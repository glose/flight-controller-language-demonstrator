/**
 */
package fr.inria.glose.fcl.model.fcl.interpreter_vm;

import fr.inria.glose.fcl.model.unity.UnityInstance;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unity Wrapper</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper#getUnityInstance <em>Unity Instance</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage#getUnityWrapper()
 * @model
 * @generated
 */
public interface UnityWrapper extends EObject {
	/**
	 * Returns the value of the '<em><b>Unity Instance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unity Instance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unity Instance</em>' attribute.
	 * @see #setUnityInstance(UnityInstance)
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage#getUnityWrapper_UnityInstance()
	 * @model dataType="fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityInstance"
	 * @generated
	 */
	UnityInstance getUnityInstance();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper#getUnityInstance <em>Unity Instance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unity Instance</em>' attribute.
	 * @see #getUnityInstance()
	 * @generated
	 */
	void setUnityInstance(UnityInstance value);

} // UnityWrapper
