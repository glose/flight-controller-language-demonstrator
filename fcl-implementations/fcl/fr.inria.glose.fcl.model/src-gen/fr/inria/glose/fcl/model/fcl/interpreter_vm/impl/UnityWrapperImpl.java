/**
 */
package fr.inria.glose.fcl.model.fcl.interpreter_vm.impl;

import fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper;

import fr.inria.glose.fcl.model.unity.UnityInstance;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unity Wrapper</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.UnityWrapperImpl#getUnityInstance <em>Unity Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UnityWrapperImpl extends MinimalEObjectImpl.Container implements UnityWrapper {
	/**
	 * The default value of the '{@link #getUnityInstance() <em>Unity Instance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnityInstance()
	 * @generated
	 * @ordered
	 */
	protected static final UnityInstance UNITY_INSTANCE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnityInstance() <em>Unity Instance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnityInstance()
	 * @generated
	 * @ordered
	 */
	protected UnityInstance unityInstance = UNITY_INSTANCE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnityWrapperImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Interpreter_vmPackage.Literals.UNITY_WRAPPER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnityInstance getUnityInstance() {
		return unityInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnityInstance(UnityInstance newUnityInstance) {
		UnityInstance oldUnityInstance = unityInstance;
		unityInstance = newUnityInstance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Interpreter_vmPackage.UNITY_WRAPPER__UNITY_INSTANCE,
					oldUnityInstance, unityInstance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Interpreter_vmPackage.UNITY_WRAPPER__UNITY_INSTANCE:
			return getUnityInstance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Interpreter_vmPackage.UNITY_WRAPPER__UNITY_INSTANCE:
			setUnityInstance((UnityInstance) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Interpreter_vmPackage.UNITY_WRAPPER__UNITY_INSTANCE:
			setUnityInstance(UNITY_INSTANCE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Interpreter_vmPackage.UNITY_WRAPPER__UNITY_INSTANCE:
			return UNITY_INSTANCE_EDEFAULT == null ? unityInstance != null
					: !UNITY_INSTANCE_EDEFAULT.equals(unityInstance);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (unityInstance: ");
		result.append(unityInstance);
		result.append(')');
		return result.toString();
	}

} //UnityWrapperImpl
