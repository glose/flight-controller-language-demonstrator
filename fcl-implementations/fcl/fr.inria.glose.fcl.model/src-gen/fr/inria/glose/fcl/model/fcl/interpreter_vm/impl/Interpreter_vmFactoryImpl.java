/**
 */
package fr.inria.glose.fcl.model.fcl.interpreter_vm.impl;

import fr.inria.glose.fcl.model.fcl.interpreter_vm.*;

import fr.inria.glose.fcl.model.unity.UnityInstance;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.javafmi.wrapper.generic.Simulation2;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Interpreter_vmFactoryImpl extends EFactoryImpl implements Interpreter_vmFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Interpreter_vmFactory init() {
		try {
			Interpreter_vmFactory theInterpreter_vmFactory = (Interpreter_vmFactory) EPackage.Registry.INSTANCE
					.getEFactory(Interpreter_vmPackage.eNS_URI);
			if (theInterpreter_vmFactory != null) {
				return theInterpreter_vmFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Interpreter_vmFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interpreter_vmFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case Interpreter_vmPackage.PROCEDURE_CONTEXT:
			return createProcedureContext();
		case Interpreter_vmPackage.DECLARATION_MAP_ENTRY:
			return createDeclarationMapEntry();
		case Interpreter_vmPackage.UNITY_WRAPPER:
			return createUnityWrapper();
		case Interpreter_vmPackage.FMU_SIMULATION_WRAPPER:
			return createFMUSimulationWrapper();
		case Interpreter_vmPackage.EVENT_OCCURRENCE:
			return createEventOccurrence();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case Interpreter_vmPackage.UNITY_INSTANCE:
			return createUnityInstanceFromString(eDataType, initialValue);
		case Interpreter_vmPackage.FMI_SIMULATION2:
			return createFMISimulation2FromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case Interpreter_vmPackage.UNITY_INSTANCE:
			return convertUnityInstanceToString(eDataType, instanceValue);
		case Interpreter_vmPackage.FMI_SIMULATION2:
			return convertFMISimulation2ToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureContext createProcedureContext() {
		ProcedureContextImpl procedureContext = new ProcedureContextImpl();
		return procedureContext;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarationMapEntry createDeclarationMapEntry() {
		DeclarationMapEntryImpl declarationMapEntry = new DeclarationMapEntryImpl();
		return declarationMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnityWrapper createUnityWrapper() {
		UnityWrapperImpl unityWrapper = new UnityWrapperImpl();
		return unityWrapper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FMUSimulationWrapper createFMUSimulationWrapper() {
		FMUSimulationWrapperImpl fmuSimulationWrapper = new FMUSimulationWrapperImpl();
		return fmuSimulationWrapper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventOccurrence createEventOccurrence() {
		EventOccurrenceImpl eventOccurrence = new EventOccurrenceImpl();
		return eventOccurrence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnityInstance createUnityInstanceFromString(EDataType eDataType, String initialValue) {
		return (UnityInstance) super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertUnityInstanceToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simulation2 createFMISimulation2FromString(EDataType eDataType, String initialValue) {
		return (Simulation2) super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFMISimulation2ToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interpreter_vmPackage getInterpreter_vmPackage() {
		return (Interpreter_vmPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Interpreter_vmPackage getPackage() {
		return Interpreter_vmPackage.eINSTANCE;
	}

} //Interpreter_vmFactoryImpl
