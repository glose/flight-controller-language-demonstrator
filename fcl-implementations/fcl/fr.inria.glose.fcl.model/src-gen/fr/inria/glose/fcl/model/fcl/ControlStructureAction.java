/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Control Structure Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getControlStructureAction()
 * @model abstract="true"
 * @generated
 */
public interface ControlStructureAction extends Action {
} // ControlStructureAction
