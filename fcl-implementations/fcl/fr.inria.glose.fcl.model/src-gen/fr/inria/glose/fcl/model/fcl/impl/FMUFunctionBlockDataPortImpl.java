/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.FclPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FMU Function Block Data Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FMUFunctionBlockDataPortImpl#getRunnableFmuVariableName <em>Runnable Fmu Variable Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FMUFunctionBlockDataPortImpl<T> extends FunctionBlockDataPortImpl implements FMUFunctionBlockDataPort<T> {
	/**
	 * The default value of the '{@link #getRunnableFmuVariableName() <em>Runnable Fmu Variable Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRunnableFmuVariableName()
	 * @generated
	 * @ordered
	 */
	protected static final String RUNNABLE_FMU_VARIABLE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRunnableFmuVariableName() <em>Runnable Fmu Variable Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRunnableFmuVariableName()
	 * @generated
	 * @ordered
	 */
	protected String runnableFmuVariableName = RUNNABLE_FMU_VARIABLE_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FMUFunctionBlockDataPortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.FMU_FUNCTION_BLOCK_DATA_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRunnableFmuVariableName() {
		return runnableFmuVariableName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRunnableFmuVariableName(String newRunnableFmuVariableName) {
		String oldRunnableFmuVariableName = runnableFmuVariableName;
		runnableFmuVariableName = newRunnableFmuVariableName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FclPackage.FMU_FUNCTION_BLOCK_DATA_PORT__RUNNABLE_FMU_VARIABLE_NAME, oldRunnableFmuVariableName,
					runnableFmuVariableName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.FMU_FUNCTION_BLOCK_DATA_PORT__RUNNABLE_FMU_VARIABLE_NAME:
			return getRunnableFmuVariableName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.FMU_FUNCTION_BLOCK_DATA_PORT__RUNNABLE_FMU_VARIABLE_NAME:
			setRunnableFmuVariableName((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.FMU_FUNCTION_BLOCK_DATA_PORT__RUNNABLE_FMU_VARIABLE_NAME:
			setRunnableFmuVariableName(RUNNABLE_FMU_VARIABLE_NAME_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.FMU_FUNCTION_BLOCK_DATA_PORT__RUNNABLE_FMU_VARIABLE_NAME:
			return RUNNABLE_FMU_VARIABLE_NAME_EDEFAULT == null ? runnableFmuVariableName != null
					: !RUNNABLE_FMU_VARIABLE_NAME_EDEFAULT.equals(runnableFmuVariableName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (runnableFmuVariableName: ");
		result.append(runnableFmuVariableName);
		result.append(')');
		return result.toString();
	}

} //FMUFunctionBlockDataPortImpl
