/**
 */
package fr.inria.glose.fcl.model.fcl.util;

import fr.inria.glose.fcl.model.fcl.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.inria.glose.fcl.model.fcl.FclPackage
 * @generated
 */
public class FclSwitch<T1> extends Switch<T1> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FclPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FclSwitch() {
		if (modelPackage == null) {
			modelPackage = FclPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T1 doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case FclPackage.FCL_MODEL: {
			FCLModel fclModel = (FCLModel) theEObject;
			T1 result = caseFCLModel(fclModel);
			if (result == null)
				result = caseNamedElement(fclModel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.MODE_AUTOMATA: {
			ModeAutomata modeAutomata = (ModeAutomata) theEObject;
			T1 result = caseModeAutomata(modeAutomata);
			if (result == null)
				result = caseMode(modeAutomata);
			if (result == null)
				result = caseNamedElement(modeAutomata);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.TRANSITION: {
			Transition transition = (Transition) theEObject;
			T1 result = caseTransition(transition);
			if (result == null)
				result = caseNamedElement(transition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.MODE: {
			Mode mode = (Mode) theEObject;
			T1 result = caseMode(mode);
			if (result == null)
				result = caseNamedElement(mode);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.ACTION: {
			Action action = (Action) theEObject;
			T1 result = caseAction(action);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.EVENT: {
			Event event = (Event) theEObject;
			T1 result = caseEvent(event);
			if (result == null)
				result = caseNamedElement(event);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.EXPRESSION: {
			Expression expression = (Expression) theEObject;
			T1 result = caseExpression(expression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.PROCEDURE: {
			Procedure procedure = (Procedure) theEObject;
			T1 result = caseProcedure(procedure);
			if (result == null)
				result = caseNamedElement(procedure);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.ACTION_BLOCK: {
			ActionBlock actionBlock = (ActionBlock) theEObject;
			T1 result = caseActionBlock(actionBlock);
			if (result == null)
				result = caseControlStructureAction(actionBlock);
			if (result == null)
				result = caseAction(actionBlock);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.DATA_STRUCT_TYPE: {
			DataStructType dataStructType = (DataStructType) theEObject;
			T1 result = caseDataStructType(dataStructType);
			if (result == null)
				result = caseDataType(dataStructType);
			if (result == null)
				result = caseNamedElement(dataStructType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.NAMED_ELEMENT: {
			NamedElement namedElement = (NamedElement) theEObject;
			T1 result = caseNamedElement(namedElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.FUNCTION_BLOCK_DATA_PORT: {
			FunctionBlockDataPort functionBlockDataPort = (FunctionBlockDataPort) theEObject;
			T1 result = caseFunctionBlockDataPort(functionBlockDataPort);
			if (result == null)
				result = caseFunctionPort(functionBlockDataPort);
			if (result == null)
				result = caseAssignable(functionBlockDataPort);
			if (result == null)
				result = caseCallableDeclaration(functionBlockDataPort);
			if (result == null)
				result = caseNamedElement(functionBlockDataPort);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.DATA_TYPE: {
			DataType dataType = (DataType) theEObject;
			T1 result = caseDataType(dataType);
			if (result == null)
				result = caseNamedElement(dataType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.PROCEDURE_PARAMETER: {
			ProcedureParameter procedureParameter = (ProcedureParameter) theEObject;
			T1 result = caseProcedureParameter(procedureParameter);
			if (result == null)
				result = caseNamedElement(procedureParameter);
			if (result == null)
				result = caseAssignable(procedureParameter);
			if (result == null)
				result = caseCallableDeclaration(procedureParameter);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.FUNCTION_PORT: {
			FunctionPort functionPort = (FunctionPort) theEObject;
			T1 result = caseFunctionPort(functionPort);
			if (result == null)
				result = caseAssignable(functionPort);
			if (result == null)
				result = caseCallableDeclaration(functionPort);
			if (result == null)
				result = caseNamedElement(functionPort);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.FUNCTION_CONNECTOR: {
			FunctionConnector functionConnector = (FunctionConnector) theEObject;
			T1 result = caseFunctionConnector(functionConnector);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.EXTERNAL_EVENT: {
			ExternalEvent externalEvent = (ExternalEvent) theEObject;
			T1 result = caseExternalEvent(externalEvent);
			if (result == null)
				result = caseEvent(externalEvent);
			if (result == null)
				result = caseNamedElement(externalEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.BINARY_EXPRESSION: {
			BinaryExpression binaryExpression = (BinaryExpression) theEObject;
			T1 result = caseBinaryExpression(binaryExpression);
			if (result == null)
				result = caseExpression(binaryExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.UNARY_EXPRESSION: {
			UnaryExpression unaryExpression = (UnaryExpression) theEObject;
			T1 result = caseUnaryExpression(unaryExpression);
			if (result == null)
				result = caseExpression(unaryExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.CAST: {
			Cast cast = (Cast) theEObject;
			T1 result = caseCast(cast);
			if (result == null)
				result = caseUnaryExpression(cast);
			if (result == null)
				result = caseExpression(cast);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.CALL: {
			Call call = (Call) theEObject;
			T1 result = caseCall(call);
			if (result == null)
				result = caseExpression(call);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.PRIMITIVE_ACTION: {
			PrimitiveAction primitiveAction = (PrimitiveAction) theEObject;
			T1 result = casePrimitiveAction(primitiveAction);
			if (result == null)
				result = caseAction(primitiveAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.ASSIGNMENT: {
			Assignment assignment = (Assignment) theEObject;
			T1 result = caseAssignment(assignment);
			if (result == null)
				result = casePrimitiveAction(assignment);
			if (result == null)
				result = caseAction(assignment);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.CONTROL_STRUCTURE_ACTION: {
			ControlStructureAction controlStructureAction = (ControlStructureAction) theEObject;
			T1 result = caseControlStructureAction(controlStructureAction);
			if (result == null)
				result = caseAction(controlStructureAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.IF_ACTION: {
			IfAction ifAction = (IfAction) theEObject;
			T1 result = caseIfAction(ifAction);
			if (result == null)
				result = caseControlStructureAction(ifAction);
			if (result == null)
				result = caseAction(ifAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.WHILE_ACTION: {
			WhileAction whileAction = (WhileAction) theEObject;
			T1 result = caseWhileAction(whileAction);
			if (result == null)
				result = caseControlStructureAction(whileAction);
			if (result == null)
				result = caseAction(whileAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.PROCEDURE_CALL: {
			ProcedureCall procedureCall = (ProcedureCall) theEObject;
			T1 result = caseProcedureCall(procedureCall);
			if (result == null)
				result = casePrimitiveAction(procedureCall);
			if (result == null)
				result = caseExpression(procedureCall);
			if (result == null)
				result = caseAction(procedureCall);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.VAR_DECL: {
			VarDecl varDecl = (VarDecl) theEObject;
			T1 result = caseVarDecl(varDecl);
			if (result == null)
				result = casePrimitiveAction(varDecl);
			if (result == null)
				result = caseAssignable(varDecl);
			if (result == null)
				result = caseCallableDeclaration(varDecl);
			if (result == null)
				result = caseNamedElement(varDecl);
			if (result == null)
				result = caseAction(varDecl);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.CALL_DECLARATION_REFERENCE: {
			CallDeclarationReference callDeclarationReference = (CallDeclarationReference) theEObject;
			T1 result = caseCallDeclarationReference(callDeclarationReference);
			if (result == null)
				result = caseCall(callDeclarationReference);
			if (result == null)
				result = caseExpression(callDeclarationReference);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.FUNCTION_PORT_REFERENCE: {
			FunctionPortReference functionPortReference = (FunctionPortReference) theEObject;
			T1 result = caseFunctionPortReference(functionPortReference);
			if (result == null)
				result = caseFunctionPort(functionPortReference);
			if (result == null)
				result = caseAssignable(functionPortReference);
			if (result == null)
				result = caseCallableDeclaration(functionPortReference);
			if (result == null)
				result = caseNamedElement(functionPortReference);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.BASIC_OPERATOR_UNARY_EXPRESSION: {
			BasicOperatorUnaryExpression basicOperatorUnaryExpression = (BasicOperatorUnaryExpression) theEObject;
			T1 result = caseBasicOperatorUnaryExpression(basicOperatorUnaryExpression);
			if (result == null)
				result = caseUnaryExpression(basicOperatorUnaryExpression);
			if (result == null)
				result = caseExpression(basicOperatorUnaryExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.BASIC_OPERATOR_BINARY_EXPRESSION: {
			BasicOperatorBinaryExpression basicOperatorBinaryExpression = (BasicOperatorBinaryExpression) theEObject;
			T1 result = caseBasicOperatorBinaryExpression(basicOperatorBinaryExpression);
			if (result == null)
				result = caseBinaryExpression(basicOperatorBinaryExpression);
			if (result == null)
				result = caseExpression(basicOperatorBinaryExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.VALUE_TYPE: {
			ValueType valueType = (ValueType) theEObject;
			T1 result = caseValueType(valueType);
			if (result == null)
				result = caseDataType(valueType);
			if (result == null)
				result = caseNamedElement(valueType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.INTEGER_VALUE_TYPE: {
			IntegerValueType integerValueType = (IntegerValueType) theEObject;
			T1 result = caseIntegerValueType(integerValueType);
			if (result == null)
				result = caseValueType(integerValueType);
			if (result == null)
				result = caseDataType(integerValueType);
			if (result == null)
				result = caseNamedElement(integerValueType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.FLOAT_VALUE_TYPE: {
			FloatValueType floatValueType = (FloatValueType) theEObject;
			T1 result = caseFloatValueType(floatValueType);
			if (result == null)
				result = caseValueType(floatValueType);
			if (result == null)
				result = caseDataType(floatValueType);
			if (result == null)
				result = caseNamedElement(floatValueType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.BOOLEAN_VALUE_TYPE: {
			BooleanValueType booleanValueType = (BooleanValueType) theEObject;
			T1 result = caseBooleanValueType(booleanValueType);
			if (result == null)
				result = caseValueType(booleanValueType);
			if (result == null)
				result = caseDataType(booleanValueType);
			if (result == null)
				result = caseNamedElement(booleanValueType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.STRING_VALUE_TYPE: {
			StringValueType stringValueType = (StringValueType) theEObject;
			T1 result = caseStringValueType(stringValueType);
			if (result == null)
				result = caseValueType(stringValueType);
			if (result == null)
				result = caseDataType(stringValueType);
			if (result == null)
				result = caseNamedElement(stringValueType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.ENUMERATION_VALUE_TYPE: {
			EnumerationValueType enumerationValueType = (EnumerationValueType) theEObject;
			T1 result = caseEnumerationValueType(enumerationValueType);
			if (result == null)
				result = caseValueType(enumerationValueType);
			if (result == null)
				result = caseDataType(enumerationValueType);
			if (result == null)
				result = caseNamedElement(enumerationValueType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.ENUMERATION_LITERAL: {
			EnumerationLiteral enumerationLiteral = (EnumerationLiteral) theEObject;
			T1 result = caseEnumerationLiteral(enumerationLiteral);
			if (result == null)
				result = caseNamedElement(enumerationLiteral);
			if (result == null)
				result = caseCallableDeclaration(enumerationLiteral);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.ASSIGNABLE: {
			Assignable assignable = (Assignable) theEObject;
			T1 result = caseAssignable(assignable);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.CALLABLE_DECLARATION: {
			CallableDeclaration callableDeclaration = (CallableDeclaration) theEObject;
			T1 result = caseCallableDeclaration(callableDeclaration);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.FUNCTION: {
			Function function = (Function) theEObject;
			T1 result = caseFunction(function);
			if (result == null)
				result = caseNamedElement(function);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.FUNCTION_TIME_REFERENCE: {
			FunctionTimeReference functionTimeReference = (FunctionTimeReference) theEObject;
			T1 result = caseFunctionTimeReference(functionTimeReference);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.EXTERNAL_PROCEDURE: {
			ExternalProcedure externalProcedure = (ExternalProcedure) theEObject;
			T1 result = caseExternalProcedure(externalProcedure);
			if (result == null)
				result = caseProcedure(externalProcedure);
			if (result == null)
				result = caseNamedElement(externalProcedure);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.FCL_PROCEDURE: {
			FCLProcedure fclProcedure = (FCLProcedure) theEObject;
			T1 result = caseFCLProcedure(fclProcedure);
			if (result == null)
				result = caseProcedure(fclProcedure);
			if (result == null)
				result = caseNamedElement(fclProcedure);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.FCL_PROCEDURE_RETURN: {
			FCLProcedureReturn fclProcedureReturn = (FCLProcedureReturn) theEObject;
			T1 result = caseFCLProcedureReturn(fclProcedureReturn);
			if (result == null)
				result = casePrimitiveAction(fclProcedureReturn);
			if (result == null)
				result = caseAction(fclProcedureReturn);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.JAVA_PROCEDURE: {
			JavaProcedure javaProcedure = (JavaProcedure) theEObject;
			T1 result = caseJavaProcedure(javaProcedure);
			if (result == null)
				result = caseExternalProcedure(javaProcedure);
			if (result == null)
				result = caseProcedure(javaProcedure);
			if (result == null)
				result = caseNamedElement(javaProcedure);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.INTEGER_VALUE: {
			IntegerValue integerValue = (IntegerValue) theEObject;
			T1 result = caseIntegerValue(integerValue);
			if (result == null)
				result = caseValue(integerValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.FLOAT_VALUE: {
			FloatValue floatValue = (FloatValue) theEObject;
			T1 result = caseFloatValue(floatValue);
			if (result == null)
				result = caseValue(floatValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.STRING_VALUE: {
			StringValue stringValue = (StringValue) theEObject;
			T1 result = caseStringValue(stringValue);
			if (result == null)
				result = caseValue(stringValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.BOOLEAN_VALUE: {
			BooleanValue booleanValue = (BooleanValue) theEObject;
			T1 result = caseBooleanValue(booleanValue);
			if (result == null)
				result = caseValue(booleanValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.FUNCTION_VAR_DECL: {
			FunctionVarDecl functionVarDecl = (FunctionVarDecl) theEObject;
			T1 result = caseFunctionVarDecl(functionVarDecl);
			if (result == null)
				result = caseAssignable(functionVarDecl);
			if (result == null)
				result = caseCallableDeclaration(functionVarDecl);
			if (result == null)
				result = caseNamedElement(functionVarDecl);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.VALUE: {
			Value value = (Value) theEObject;
			T1 result = caseValue(value);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.CALL_CONSTANT: {
			CallConstant callConstant = (CallConstant) theEObject;
			T1 result = caseCallConstant(callConstant);
			if (result == null)
				result = caseCall(callConstant);
			if (result == null)
				result = caseExpression(callConstant);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.ENUMERATION_VALUE: {
			EnumerationValue enumerationValue = (EnumerationValue) theEObject;
			T1 result = caseEnumerationValue(enumerationValue);
			if (result == null)
				result = caseValue(enumerationValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.FMU_FUNCTION: {
			FMUFunction fmuFunction = (FMUFunction) theEObject;
			T1 result = caseFMUFunction(fmuFunction);
			if (result == null)
				result = caseFunction(fmuFunction);
			if (result == null)
				result = caseNamedElement(fmuFunction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.FMU_FUNCTION_BLOCK_DATA_PORT: {
			FMUFunctionBlockDataPort<?> fmuFunctionBlockDataPort = (FMUFunctionBlockDataPort<?>) theEObject;
			T1 result = caseFMUFunctionBlockDataPort(fmuFunctionBlockDataPort);
			if (result == null)
				result = caseFunctionBlockDataPort(fmuFunctionBlockDataPort);
			if (result == null)
				result = caseFunctionPort(fmuFunctionBlockDataPort);
			if (result == null)
				result = caseAssignable(fmuFunctionBlockDataPort);
			if (result == null)
				result = caseCallableDeclaration(fmuFunctionBlockDataPort);
			if (result == null)
				result = caseNamedElement(fmuFunctionBlockDataPort);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.UNITY_FUNCTION: {
			UnityFunction unityFunction = (UnityFunction) theEObject;
			T1 result = caseUnityFunction(unityFunction);
			if (result == null)
				result = caseFunction(unityFunction);
			if (result == null)
				result = caseNamedElement(unityFunction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.UNITY_FUNCTION_DATA_PORT: {
			UnityFunctionDataPort unityFunctionDataPort = (UnityFunctionDataPort) theEObject;
			T1 result = caseUnityFunctionDataPort(unityFunctionDataPort);
			if (result == null)
				result = caseFunctionBlockDataPort(unityFunctionDataPort);
			if (result == null)
				result = caseFunctionPort(unityFunctionDataPort);
			if (result == null)
				result = caseAssignable(unityFunctionDataPort);
			if (result == null)
				result = caseCallableDeclaration(unityFunctionDataPort);
			if (result == null)
				result = caseNamedElement(unityFunctionDataPort);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.CALL_PREVIOUS: {
			CallPrevious callPrevious = (CallPrevious) theEObject;
			T1 result = caseCallPrevious(callPrevious);
			if (result == null)
				result = caseCall(callPrevious);
			if (result == null)
				result = caseExpression(callPrevious);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.DATA_STRUCT_PROPERTY: {
			DataStructProperty dataStructProperty = (DataStructProperty) theEObject;
			T1 result = caseDataStructProperty(dataStructProperty);
			if (result == null)
				result = caseNamedElement(dataStructProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.DATA_STRUCT_PROPERTY_REFERENCE: {
			DataStructPropertyReference dataStructPropertyReference = (DataStructPropertyReference) theEObject;
			T1 result = caseDataStructPropertyReference(dataStructPropertyReference);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT: {
			DataStructPropertyAssignment dataStructPropertyAssignment = (DataStructPropertyAssignment) theEObject;
			T1 result = caseDataStructPropertyAssignment(dataStructPropertyAssignment);
			if (result == null)
				result = caseDataStructPropertyReference(dataStructPropertyAssignment);
			if (result == null)
				result = casePrimitiveAction(dataStructPropertyAssignment);
			if (result == null)
				result = caseAction(dataStructPropertyAssignment);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.CALL_DATA_STRUCT_PROPERTY: {
			CallDataStructProperty callDataStructProperty = (CallDataStructProperty) theEObject;
			T1 result = caseCallDataStructProperty(callDataStructProperty);
			if (result == null)
				result = caseDataStructPropertyReference(callDataStructProperty);
			if (result == null)
				result = caseCall(callDataStructProperty);
			if (result == null)
				result = caseExpression(callDataStructProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.NEW_DATA_STRUCT: {
			NewDataStruct newDataStruct = (NewDataStruct) theEObject;
			T1 result = caseNewDataStruct(newDataStruct);
			if (result == null)
				result = caseExpression(newDataStruct);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.STRUCT_VALUE: {
			StructValue structValue = (StructValue) theEObject;
			T1 result = caseStructValue(structValue);
			if (result == null)
				result = caseValue(structValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.STRUCT_PROPERTY_VALUE: {
			StructPropertyValue structPropertyValue = (StructPropertyValue) theEObject;
			T1 result = caseStructPropertyValue(structPropertyValue);
			if (result == null)
				result = caseNamedElement(structPropertyValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FclPackage.LOG: {
			Log log = (Log) theEObject;
			T1 result = caseLog(log);
			if (result == null)
				result = casePrimitiveAction(log);
			if (result == null)
				result = caseAction(log);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FCL Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FCL Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFCLModel(FCLModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mode Automata</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mode Automata</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseModeAutomata(ModeAutomata object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTransition(Transition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mode</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mode</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseMode(Mode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseAction(Action object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseEvent(Event object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseExpression(Expression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Procedure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseProcedure(Procedure object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Action Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Action Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseActionBlock(ActionBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Struct Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Struct Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseDataStructType(DataStructType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Block Data Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Block Data Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFunctionBlockDataPort(FunctionBlockDataPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseDataType(DataType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Procedure Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Procedure Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseProcedureParameter(ProcedureParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFunctionPort(FunctionPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Connector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Connector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFunctionConnector(FunctionConnector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>External Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>External Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseExternalEvent(ExternalEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseBinaryExpression(BinaryExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseUnaryExpression(UnaryExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cast</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cast</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseCast(Cast object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Call</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Call</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseCall(Call object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Primitive Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Primitive Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 casePrimitiveAction(PrimitiveAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assignment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseAssignment(Assignment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Control Structure Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Control Structure Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseControlStructureAction(ControlStructureAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseIfAction(IfAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>While Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>While Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseWhileAction(WhileAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Procedure Call</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Procedure Call</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseProcedureCall(ProcedureCall object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Var Decl</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Var Decl</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseVarDecl(VarDecl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Call Declaration Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Call Declaration Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseCallDeclarationReference(CallDeclarationReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Port Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Port Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFunctionPortReference(FunctionPortReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Basic Operator Unary Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Basic Operator Unary Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseBasicOperatorUnaryExpression(BasicOperatorUnaryExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Basic Operator Binary Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Basic Operator Binary Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseBasicOperatorBinaryExpression(BasicOperatorBinaryExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseValueType(ValueType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Integer Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer Value Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseIntegerValueType(IntegerValueType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Float Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Float Value Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFloatValueType(FloatValueType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Value Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseBooleanValueType(BooleanValueType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String Value Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStringValueType(StringValueType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enumeration Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enumeration Value Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseEnumerationValueType(EnumerationValueType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enumeration Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enumeration Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseEnumerationLiteral(EnumerationLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assignable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assignable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseAssignable(Assignable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Callable Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Callable Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseCallableDeclaration(CallableDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFunction(Function object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Time Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Time Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFunctionTimeReference(FunctionTimeReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>External Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>External Procedure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseExternalProcedure(ExternalProcedure object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FCL Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FCL Procedure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFCLProcedure(FCLProcedure object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FCL Procedure Return</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FCL Procedure Return</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFCLProcedureReturn(FCLProcedureReturn object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Java Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Java Procedure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseJavaProcedure(JavaProcedure object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Integer Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseIntegerValue(IntegerValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Float Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Float Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFloatValue(FloatValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStringValue(StringValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseBooleanValue(BooleanValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Var Decl</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Var Decl</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFunctionVarDecl(FunctionVarDecl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FMU Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FMU Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFMUFunction(FMUFunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseValue(Value object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Call Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Call Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseCallConstant(CallConstant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enumeration Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enumeration Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseEnumerationValue(EnumerationValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FMU Function Block Data Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FMU Function Block Data Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T> T1 caseFMUFunctionBlockDataPort(FMUFunctionBlockDataPort<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unity Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unity Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseUnityFunction(UnityFunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unity Function Data Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unity Function Data Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseUnityFunctionDataPort(UnityFunctionDataPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Call Previous</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Call Previous</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseCallPrevious(CallPrevious object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Struct Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Struct Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseDataStructProperty(DataStructProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Struct Property Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Struct Property Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseDataStructPropertyReference(DataStructPropertyReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Struct Property Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Struct Property Assignment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseDataStructPropertyAssignment(DataStructPropertyAssignment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Call Data Struct Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Call Data Struct Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseCallDataStructProperty(CallDataStructProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>New Data Struct</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>New Data Struct</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseNewDataStruct(NewDataStruct object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Struct Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Struct Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStructValue(StructValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Struct Property Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Struct Property Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStructPropertyValue(StructPropertyValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Log</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Log</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseLog(Log object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T1 defaultCase(EObject object) {
		return null;
	}

} //FclSwitch
