/**
 */
package fr.inria.glose.fcl.model.fcl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mode Automata</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.ModeAutomata#getModes <em>Modes</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.ModeAutomata#getInitialMode <em>Initial Mode</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.ModeAutomata#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.ModeAutomata#getCurrentMode <em>Current Mode</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getModeAutomata()
 * @model
 * @generated
 */
public interface ModeAutomata extends Mode {
	/**
	 * Returns the value of the '<em><b>Modes</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.Mode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Modes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modes</em>' containment reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getModeAutomata_Modes()
	 * @model containment="true"
	 * @generated
	 */
	EList<Mode> getModes();

	/**
	 * Returns the value of the '<em><b>Initial Mode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Mode</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Mode</em>' reference.
	 * @see #setInitialMode(Mode)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getModeAutomata_InitialMode()
	 * @model required="true"
	 * @generated
	 */
	Mode getInitialMode();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.ModeAutomata#getInitialMode <em>Initial Mode</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Mode</em>' reference.
	 * @see #getInitialMode()
	 * @generated
	 */
	void setInitialMode(Mode value);

	/**
	 * Returns the value of the '<em><b>Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.Transition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitions</em>' containment reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getModeAutomata_Transitions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Transition> getTransitions();

	/**
	 * Returns the value of the '<em><b>Current Mode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Mode</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Mode</em>' reference.
	 * @see #setCurrentMode(Mode)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getModeAutomata_CurrentMode()
	 * @model
	 * @generated
	 */
	Mode getCurrentMode();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.ModeAutomata#getCurrentMode <em>Current Mode</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Mode</em>' reference.
	 * @see #getCurrentMode()
	 * @generated
	 */
	void setCurrentMode(Mode value);

} // ModeAutomata
