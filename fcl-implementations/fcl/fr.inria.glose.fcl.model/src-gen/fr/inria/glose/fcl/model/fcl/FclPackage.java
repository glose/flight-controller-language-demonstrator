/**
 */
package fr.inria.glose.fcl.model.fcl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * invariant: 
 * not subFunctions.isempty implies 
 * <!-- end-model-doc -->
 * @see fr.inria.glose.fcl.model.fcl.FclFactory
 * @model kind="package"
 * @generated
 */
public interface FclPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "fcl";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.inria.fr/glose/fcl";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fcl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FclPackage eINSTANCE = fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.NamedElementImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.FCLModelImpl <em>FCL Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.FCLModelImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFCLModel()
	 * @generated
	 */
	int FCL_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_MODEL__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Mode Automata</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_MODEL__MODE_AUTOMATA = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_MODEL__EVENTS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Procedures</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_MODEL__PROCEDURES = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Data Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_MODEL__DATA_TYPES = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Dataflow</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_MODEL__DATAFLOW = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Received Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_MODEL__RECEIVED_EVENTS = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Procedure Stack</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_MODEL__PROCEDURE_STACK = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Indentation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_MODEL__INDENTATION = NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>FCL Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_MODEL_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>All Functions</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_MODEL___ALL_FUNCTIONS = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>All Connectors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_MODEL___ALL_CONNECTORS = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Propagate Delayed Results</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_MODEL___PROPAGATE_DELAYED_RESULTS = NAMED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Evaluate Mode Automata</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_MODEL___EVALUATE_MODE_AUTOMATA = NAMED_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>FCL Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_MODEL_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.ModeImpl <em>Mode</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.ModeImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getMode()
	 * @generated
	 */
	int MODE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE__ENTRY = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Exit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE__EXIT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Final</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE__FINAL = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Enabled Functions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE__ENABLED_FUNCTIONS = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Incoming Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE__INCOMING_TRANSITION = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Outgoing Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE__OUTGOING_TRANSITION = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Mode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Mode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.ModeAutomataImpl <em>Mode Automata</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.ModeAutomataImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getModeAutomata()
	 * @generated
	 */
	int MODE_AUTOMATA = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_AUTOMATA__NAME = MODE__NAME;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_AUTOMATA__ENTRY = MODE__ENTRY;

	/**
	 * The feature id for the '<em><b>Exit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_AUTOMATA__EXIT = MODE__EXIT;

	/**
	 * The feature id for the '<em><b>Final</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_AUTOMATA__FINAL = MODE__FINAL;

	/**
	 * The feature id for the '<em><b>Enabled Functions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_AUTOMATA__ENABLED_FUNCTIONS = MODE__ENABLED_FUNCTIONS;

	/**
	 * The feature id for the '<em><b>Incoming Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_AUTOMATA__INCOMING_TRANSITION = MODE__INCOMING_TRANSITION;

	/**
	 * The feature id for the '<em><b>Outgoing Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_AUTOMATA__OUTGOING_TRANSITION = MODE__OUTGOING_TRANSITION;

	/**
	 * The feature id for the '<em><b>Modes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_AUTOMATA__MODES = MODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Initial Mode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_AUTOMATA__INITIAL_MODE = MODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_AUTOMATA__TRANSITIONS = MODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Current Mode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_AUTOMATA__CURRENT_MODE = MODE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Mode Automata</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_AUTOMATA_FEATURE_COUNT = MODE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Mode Automata</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODE_AUTOMATA_OPERATION_COUNT = MODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.TransitionImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ACTION = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Guard</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__GUARD = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__EVENT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TARGET = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__SOURCE = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.ActionImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 4;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.EventImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.ExpressionImpl <em>Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.ExpressionImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getExpression()
	 * @generated
	 */
	int EXPRESSION = 6;

	/**
	 * The feature id for the '<em><b>Result Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__RESULT_TYPE = 0;

	/**
	 * The number of structural features of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.ProcedureImpl <em>Procedure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.ProcedureImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getProcedure()
	 * @generated
	 */
	int PROCEDURE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Procedure Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE__PROCEDURE_PARAMETERS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE__RETURN_TYPE = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Procedure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Procedure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.ControlStructureActionImpl <em>Control Structure Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.ControlStructureActionImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getControlStructureAction()
	 * @generated
	 */
	int CONTROL_STRUCTURE_ACTION = 23;

	/**
	 * The number of structural features of the '<em>Control Structure Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_STRUCTURE_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Control Structure Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_STRUCTURE_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.ActionBlockImpl <em>Action Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.ActionBlockImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getActionBlock()
	 * @generated
	 */
	int ACTION_BLOCK = 8;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_BLOCK__ACTIONS = CONTROL_STRUCTURE_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Action Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_BLOCK_FEATURE_COUNT = CONTROL_STRUCTURE_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Action Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_BLOCK_OPERATION_COUNT = CONTROL_STRUCTURE_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.DataTypeImpl <em>Data Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.DataTypeImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getDataType()
	 * @generated
	 */
	int DATA_TYPE = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.DataStructTypeImpl <em>Data Struct Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.DataStructTypeImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getDataStructType()
	 * @generated
	 */
	int DATA_STRUCT_TYPE = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCT_TYPE__NAME = DATA_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCT_TYPE__PROPERTIES = DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Data Struct Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCT_TYPE_FEATURE_COUNT = DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Data Struct Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCT_TYPE_OPERATION_COUNT = DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.AssignableImpl <em>Assignable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.AssignableImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getAssignable()
	 * @generated
	 */
	int ASSIGNABLE = 39;

	/**
	 * The number of structural features of the '<em>Assignable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNABLE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Assignable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.FunctionPortImpl <em>Function Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.FunctionPortImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFunctionPort()
	 * @generated
	 */
	int FUNCTION_PORT = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_PORT__NAME = ASSIGNABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_PORT__DATA_TYPE = ASSIGNABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_PORT__DIRECTION = ASSIGNABLE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Function Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_PORT_FEATURE_COUNT = ASSIGNABLE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Outgoing Connectors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_PORT___OUTGOING_CONNECTORS = ASSIGNABLE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Incoming Connectors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_PORT___INCOMING_CONNECTORS = ASSIGNABLE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Function Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_PORT_OPERATION_COUNT = ASSIGNABLE_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.FunctionBlockDataPortImpl <em>Function Block Data Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.FunctionBlockDataPortImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFunctionBlockDataPort()
	 * @generated
	 */
	int FUNCTION_BLOCK_DATA_PORT = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BLOCK_DATA_PORT__NAME = FUNCTION_PORT__NAME;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BLOCK_DATA_PORT__DATA_TYPE = FUNCTION_PORT__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BLOCK_DATA_PORT__DIRECTION = FUNCTION_PORT__DIRECTION;

	/**
	 * The feature id for the '<em><b>Default Input Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE = FUNCTION_PORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Function Var Store</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BLOCK_DATA_PORT__FUNCTION_VAR_STORE = FUNCTION_PORT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Current Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BLOCK_DATA_PORT__CURRENT_VALUE = FUNCTION_PORT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Function Block Data Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BLOCK_DATA_PORT_FEATURE_COUNT = FUNCTION_PORT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Outgoing Connectors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BLOCK_DATA_PORT___OUTGOING_CONNECTORS = FUNCTION_PORT___OUTGOING_CONNECTORS;

	/**
	 * The operation id for the '<em>Incoming Connectors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BLOCK_DATA_PORT___INCOMING_CONNECTORS = FUNCTION_PORT___INCOMING_CONNECTORS;

	/**
	 * The number of operations of the '<em>Function Block Data Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_BLOCK_DATA_PORT_OPERATION_COUNT = FUNCTION_PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.ProcedureParameterImpl <em>Procedure Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.ProcedureParameterImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getProcedureParameter()
	 * @generated
	 */
	int PROCEDURE_PARAMETER = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_PARAMETER__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_PARAMETER__TYPE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Procedure Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_PARAMETER_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Procedure Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_PARAMETER_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.FunctionConnectorImpl <em>Function Connector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.FunctionConnectorImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFunctionConnector()
	 * @generated
	 */
	int FUNCTION_CONNECTOR = 15;

	/**
	 * The feature id for the '<em><b>Emitter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CONNECTOR__EMITTER = 0;

	/**
	 * The feature id for the '<em><b>Receiver</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CONNECTOR__RECEIVER = 1;

	/**
	 * The feature id for the '<em><b>Delayed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CONNECTOR__DELAYED = 2;

	/**
	 * The number of structural features of the '<em>Function Connector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CONNECTOR_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Dse send Data</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CONNECTOR___DSE_SEND_DATA = 0;

	/**
	 * The number of operations of the '<em>Function Connector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CONNECTOR_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.ExternalEventImpl <em>External Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.ExternalEventImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getExternalEvent()
	 * @generated
	 */
	int EXTERNAL_EVENT = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_EVENT__NAME = EVENT__NAME;

	/**
	 * The number of structural features of the '<em>External Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>External Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_EVENT_OPERATION_COUNT = EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.BinaryExpressionImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getBinaryExpression()
	 * @generated
	 */
	int BINARY_EXPRESSION = 17;

	/**
	 * The feature id for the '<em><b>Result Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__RESULT_TYPE = EXPRESSION__RESULT_TYPE;

	/**
	 * The feature id for the '<em><b>Lhs Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__LHS_OPERAND = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Rhs Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__RHS_OPERAND = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Binary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Binary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.UnaryExpressionImpl <em>Unary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.UnaryExpressionImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getUnaryExpression()
	 * @generated
	 */
	int UNARY_EXPRESSION = 18;

	/**
	 * The feature id for the '<em><b>Result Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__RESULT_TYPE = EXPRESSION__RESULT_TYPE;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__OPERAND = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Unary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.CastImpl <em>Cast</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.CastImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getCast()
	 * @generated
	 */
	int CAST = 19;

	/**
	 * The feature id for the '<em><b>Result Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAST__RESULT_TYPE = UNARY_EXPRESSION__RESULT_TYPE;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAST__OPERAND = UNARY_EXPRESSION__OPERAND;

	/**
	 * The number of structural features of the '<em>Cast</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAST_FEATURE_COUNT = UNARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Cast</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAST_OPERATION_COUNT = UNARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.CallImpl <em>Call</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.CallImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getCall()
	 * @generated
	 */
	int CALL = 20;

	/**
	 * The feature id for the '<em><b>Result Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL__RESULT_TYPE = EXPRESSION__RESULT_TYPE;

	/**
	 * The number of structural features of the '<em>Call</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Call</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.PrimitiveActionImpl <em>Primitive Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.PrimitiveActionImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getPrimitiveAction()
	 * @generated
	 */
	int PRIMITIVE_ACTION = 21;

	/**
	 * The number of structural features of the '<em>Primitive Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Primitive Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.AssignmentImpl <em>Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.AssignmentImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getAssignment()
	 * @generated
	 */
	int ASSIGNMENT = 22;

	/**
	 * The feature id for the '<em><b>Assignable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT__ASSIGNABLE = PRIMITIVE_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT__EXPRESSION = PRIMITIVE_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT_FEATURE_COUNT = PRIMITIVE_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT_OPERATION_COUNT = PRIMITIVE_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.IfActionImpl <em>If Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.IfActionImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getIfAction()
	 * @generated
	 */
	int IF_ACTION = 24;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACTION__CONDITION = CONTROL_STRUCTURE_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Then Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACTION__THEN_ACTION = CONTROL_STRUCTURE_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Else Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACTION__ELSE_ACTION = CONTROL_STRUCTURE_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>If Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACTION_FEATURE_COUNT = CONTROL_STRUCTURE_ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>If Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACTION_OPERATION_COUNT = CONTROL_STRUCTURE_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.WhileActionImpl <em>While Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.WhileActionImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getWhileAction()
	 * @generated
	 */
	int WHILE_ACTION = 25;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_ACTION__CONDITION = CONTROL_STRUCTURE_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_ACTION__ACTION = CONTROL_STRUCTURE_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>While Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_ACTION_FEATURE_COUNT = CONTROL_STRUCTURE_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>While Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_ACTION_OPERATION_COUNT = CONTROL_STRUCTURE_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.ProcedureCallImpl <em>Procedure Call</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.ProcedureCallImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getProcedureCall()
	 * @generated
	 */
	int PROCEDURE_CALL = 26;

	/**
	 * The feature id for the '<em><b>Result Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_CALL__RESULT_TYPE = PRIMITIVE_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Procedure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_CALL__PROCEDURE = PRIMITIVE_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Procedure Call Arguments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS = PRIMITIVE_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Procedure Call</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_CALL_FEATURE_COUNT = PRIMITIVE_ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Procedure Call</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_CALL_OPERATION_COUNT = PRIMITIVE_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.VarDeclImpl <em>Var Decl</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.VarDeclImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getVarDecl()
	 * @generated
	 */
	int VAR_DECL = 27;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_DECL__NAME = PRIMITIVE_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_DECL__TYPE = PRIMITIVE_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_DECL__INITIAL_VALUE = PRIMITIVE_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Var Decl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_DECL_FEATURE_COUNT = PRIMITIVE_ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Var Decl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_DECL_OPERATION_COUNT = PRIMITIVE_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.CallDeclarationReferenceImpl <em>Call Declaration Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.CallDeclarationReferenceImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getCallDeclarationReference()
	 * @generated
	 */
	int CALL_DECLARATION_REFERENCE = 28;

	/**
	 * The feature id for the '<em><b>Result Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_DECLARATION_REFERENCE__RESULT_TYPE = CALL__RESULT_TYPE;

	/**
	 * The feature id for the '<em><b>Callable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_DECLARATION_REFERENCE__CALLABLE = CALL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Call Declaration Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_DECLARATION_REFERENCE_FEATURE_COUNT = CALL_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Call Declaration Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_DECLARATION_REFERENCE_OPERATION_COUNT = CALL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.FunctionPortReferenceImpl <em>Function Port Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.FunctionPortReferenceImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFunctionPortReference()
	 * @generated
	 */
	int FUNCTION_PORT_REFERENCE = 29;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_PORT_REFERENCE__NAME = FUNCTION_PORT__NAME;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_PORT_REFERENCE__DATA_TYPE = FUNCTION_PORT__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_PORT_REFERENCE__DIRECTION = FUNCTION_PORT__DIRECTION;

	/**
	 * The feature id for the '<em><b>Function Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_PORT_REFERENCE__FUNCTION_PORT = FUNCTION_PORT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Function Port Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_PORT_REFERENCE_FEATURE_COUNT = FUNCTION_PORT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Outgoing Connectors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_PORT_REFERENCE___OUTGOING_CONNECTORS = FUNCTION_PORT___OUTGOING_CONNECTORS;

	/**
	 * The operation id for the '<em>Incoming Connectors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_PORT_REFERENCE___INCOMING_CONNECTORS = FUNCTION_PORT___INCOMING_CONNECTORS;

	/**
	 * The number of operations of the '<em>Function Port Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_PORT_REFERENCE_OPERATION_COUNT = FUNCTION_PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.BasicOperatorUnaryExpressionImpl <em>Basic Operator Unary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.BasicOperatorUnaryExpressionImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getBasicOperatorUnaryExpression()
	 * @generated
	 */
	int BASIC_OPERATOR_UNARY_EXPRESSION = 30;

	/**
	 * The feature id for the '<em><b>Result Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_OPERATOR_UNARY_EXPRESSION__RESULT_TYPE = UNARY_EXPRESSION__RESULT_TYPE;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_OPERATOR_UNARY_EXPRESSION__OPERAND = UNARY_EXPRESSION__OPERAND;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_OPERATOR_UNARY_EXPRESSION__OPERATOR = UNARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Basic Operator Unary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_OPERATOR_UNARY_EXPRESSION_FEATURE_COUNT = UNARY_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Basic Operator Unary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_OPERATOR_UNARY_EXPRESSION_OPERATION_COUNT = UNARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.BasicOperatorBinaryExpressionImpl <em>Basic Operator Binary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.BasicOperatorBinaryExpressionImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getBasicOperatorBinaryExpression()
	 * @generated
	 */
	int BASIC_OPERATOR_BINARY_EXPRESSION = 31;

	/**
	 * The feature id for the '<em><b>Result Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_OPERATOR_BINARY_EXPRESSION__RESULT_TYPE = BINARY_EXPRESSION__RESULT_TYPE;

	/**
	 * The feature id for the '<em><b>Lhs Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_OPERATOR_BINARY_EXPRESSION__LHS_OPERAND = BINARY_EXPRESSION__LHS_OPERAND;

	/**
	 * The feature id for the '<em><b>Rhs Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_OPERATOR_BINARY_EXPRESSION__RHS_OPERAND = BINARY_EXPRESSION__RHS_OPERAND;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_OPERATOR_BINARY_EXPRESSION__OPERATOR = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Basic Operator Binary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_OPERATOR_BINARY_EXPRESSION_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Basic Operator Binary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_OPERATOR_BINARY_EXPRESSION_OPERATION_COUNT = BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.ValueTypeImpl <em>Value Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.ValueTypeImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getValueType()
	 * @generated
	 */
	int VALUE_TYPE = 32;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_TYPE__NAME = DATA_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_TYPE__UNIT = DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_TYPE__DESCRIPTION = DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Value Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_TYPE_FEATURE_COUNT = DATA_TYPE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Value Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_TYPE_OPERATION_COUNT = DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.IntegerValueTypeImpl <em>Integer Value Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.IntegerValueTypeImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getIntegerValueType()
	 * @generated
	 */
	int INTEGER_VALUE_TYPE = 33;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_TYPE__NAME = VALUE_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_TYPE__UNIT = VALUE_TYPE__UNIT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_TYPE__DESCRIPTION = VALUE_TYPE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_TYPE__MIN = VALUE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_TYPE__MAX = VALUE_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Integer Value Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_TYPE_FEATURE_COUNT = VALUE_TYPE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Integer Value Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_TYPE_OPERATION_COUNT = VALUE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.FloatValueTypeImpl <em>Float Value Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.FloatValueTypeImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFloatValueType()
	 * @generated
	 */
	int FLOAT_VALUE_TYPE = 34;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE_TYPE__NAME = VALUE_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE_TYPE__UNIT = VALUE_TYPE__UNIT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE_TYPE__DESCRIPTION = VALUE_TYPE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE_TYPE__MIN = VALUE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE_TYPE__MAX = VALUE_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Float Value Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE_TYPE_FEATURE_COUNT = VALUE_TYPE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Float Value Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE_TYPE_OPERATION_COUNT = VALUE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.BooleanValueTypeImpl <em>Boolean Value Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.BooleanValueTypeImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getBooleanValueType()
	 * @generated
	 */
	int BOOLEAN_VALUE_TYPE = 35;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE_TYPE__NAME = VALUE_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE_TYPE__UNIT = VALUE_TYPE__UNIT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE_TYPE__DESCRIPTION = VALUE_TYPE__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>Boolean Value Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE_TYPE_FEATURE_COUNT = VALUE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Boolean Value Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE_TYPE_OPERATION_COUNT = VALUE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.StringValueTypeImpl <em>String Value Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.StringValueTypeImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getStringValueType()
	 * @generated
	 */
	int STRING_VALUE_TYPE = 36;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE_TYPE__NAME = VALUE_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE_TYPE__UNIT = VALUE_TYPE__UNIT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE_TYPE__DESCRIPTION = VALUE_TYPE__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>String Value Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE_TYPE_FEATURE_COUNT = VALUE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>String Value Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE_TYPE_OPERATION_COUNT = VALUE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.EnumerationValueTypeImpl <em>Enumeration Value Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.EnumerationValueTypeImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getEnumerationValueType()
	 * @generated
	 */
	int ENUMERATION_VALUE_TYPE = 37;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_VALUE_TYPE__NAME = VALUE_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_VALUE_TYPE__UNIT = VALUE_TYPE__UNIT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_VALUE_TYPE__DESCRIPTION = VALUE_TYPE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enumeration Literal</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_VALUE_TYPE__ENUMERATION_LITERAL = VALUE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Enumeration Value Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_VALUE_TYPE_FEATURE_COUNT = VALUE_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Enumeration Value Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_VALUE_TYPE_OPERATION_COUNT = VALUE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.EnumerationLiteralImpl <em>Enumeration Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.EnumerationLiteralImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getEnumerationLiteral()
	 * @generated
	 */
	int ENUMERATION_LITERAL = 38;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_LITERAL__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Enumeration Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_LITERAL_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Enumeration Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_LITERAL_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.CallableDeclarationImpl <em>Callable Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.CallableDeclarationImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getCallableDeclaration()
	 * @generated
	 */
	int CALLABLE_DECLARATION = 40;

	/**
	 * The number of structural features of the '<em>Callable Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALLABLE_DECLARATION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Callable Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALLABLE_DECLARATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.FunctionImpl <em>Function</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.FunctionImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFunction()
	 * @generated
	 */
	int FUNCTION = 41;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Function Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__FUNCTION_PORTS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__ACTION = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sub Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__SUB_FUNCTIONS = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Function Connectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__FUNCTION_CONNECTORS = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Function Var Decls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__FUNCTION_VAR_DECLS = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Time Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__TIME_REFERENCE = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>All Sub Functions</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION___ALL_SUB_FUNCTIONS = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Dse start Eval Function</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION___DSE_START_EVAL_FUNCTION = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Dse stop Eval Function</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION___DSE_STOP_EVAL_FUNCTION = NAMED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Enabled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION___IS_ENABLED = NAMED_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.FunctionTimeReferenceImpl <em>Function Time Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.FunctionTimeReferenceImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFunctionTimeReference()
	 * @generated
	 */
	int FUNCTION_TIME_REFERENCE = 42;

	/**
	 * The feature id for the '<em><b>Observable Var</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TIME_REFERENCE__OBSERVABLE_VAR = 0;

	/**
	 * The feature id for the '<em><b>Increment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TIME_REFERENCE__INCREMENT = 1;

	/**
	 * The number of structural features of the '<em>Function Time Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TIME_REFERENCE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Function Time Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_TIME_REFERENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.ExternalProcedureImpl <em>External Procedure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.ExternalProcedureImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getExternalProcedure()
	 * @generated
	 */
	int EXTERNAL_PROCEDURE = 43;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_PROCEDURE__NAME = PROCEDURE__NAME;

	/**
	 * The feature id for the '<em><b>Procedure Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_PROCEDURE__PROCEDURE_PARAMETERS = PROCEDURE__PROCEDURE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_PROCEDURE__RETURN_TYPE = PROCEDURE__RETURN_TYPE;

	/**
	 * The number of structural features of the '<em>External Procedure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_PROCEDURE_FEATURE_COUNT = PROCEDURE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>External Procedure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_PROCEDURE_OPERATION_COUNT = PROCEDURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.FCLProcedureImpl <em>FCL Procedure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.FCLProcedureImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFCLProcedure()
	 * @generated
	 */
	int FCL_PROCEDURE = 44;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_PROCEDURE__NAME = PROCEDURE__NAME;

	/**
	 * The feature id for the '<em><b>Procedure Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_PROCEDURE__PROCEDURE_PARAMETERS = PROCEDURE__PROCEDURE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_PROCEDURE__RETURN_TYPE = PROCEDURE__RETURN_TYPE;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_PROCEDURE__ACTION = PROCEDURE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FCL Procedure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_PROCEDURE_FEATURE_COUNT = PROCEDURE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>FCL Procedure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_PROCEDURE_OPERATION_COUNT = PROCEDURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.FCLProcedureReturnImpl <em>FCL Procedure Return</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.FCLProcedureReturnImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFCLProcedureReturn()
	 * @generated
	 */
	int FCL_PROCEDURE_RETURN = 45;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_PROCEDURE_RETURN__EXPRESSION = PRIMITIVE_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FCL Procedure Return</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_PROCEDURE_RETURN_FEATURE_COUNT = PRIMITIVE_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>FCL Procedure Return</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCL_PROCEDURE_RETURN_OPERATION_COUNT = PRIMITIVE_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.JavaProcedureImpl <em>Java Procedure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.JavaProcedureImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getJavaProcedure()
	 * @generated
	 */
	int JAVA_PROCEDURE = 46;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_PROCEDURE__NAME = EXTERNAL_PROCEDURE__NAME;

	/**
	 * The feature id for the '<em><b>Procedure Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_PROCEDURE__PROCEDURE_PARAMETERS = EXTERNAL_PROCEDURE__PROCEDURE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_PROCEDURE__RETURN_TYPE = EXTERNAL_PROCEDURE__RETURN_TYPE;

	/**
	 * The feature id for the '<em><b>Method Full Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_PROCEDURE__METHOD_FULL_QUALIFIED_NAME = EXTERNAL_PROCEDURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_PROCEDURE__STATIC = EXTERNAL_PROCEDURE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Java Procedure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_PROCEDURE_FEATURE_COUNT = EXTERNAL_PROCEDURE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Java Procedure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_PROCEDURE_OPERATION_COUNT = EXTERNAL_PROCEDURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.ValueImpl <em>Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.ValueImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getValue()
	 * @generated
	 */
	int VALUE = 52;

	/**
	 * The number of structural features of the '<em>Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.IntegerValueImpl <em>Integer Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.IntegerValueImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getIntegerValue()
	 * @generated
	 */
	int INTEGER_VALUE = 47;

	/**
	 * The feature id for the '<em><b>Int Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE__INT_VALUE = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Integer Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Integer Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.FloatValueImpl <em>Float Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.FloatValueImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFloatValue()
	 * @generated
	 */
	int FLOAT_VALUE = 48;

	/**
	 * The feature id for the '<em><b>Float Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE__FLOAT_VALUE = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Float Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Float Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.StringValueImpl <em>String Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.StringValueImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getStringValue()
	 * @generated
	 */
	int STRING_VALUE = 49;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE__STRING_VALUE = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>String Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.BooleanValueImpl <em>Boolean Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.BooleanValueImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getBooleanValue()
	 * @generated
	 */
	int BOOLEAN_VALUE = 50;

	/**
	 * The feature id for the '<em><b>Boolean Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE__BOOLEAN_VALUE = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Boolean Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.FunctionVarDeclImpl <em>Function Var Decl</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.FunctionVarDeclImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFunctionVarDecl()
	 * @generated
	 */
	int FUNCTION_VAR_DECL = 51;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_VAR_DECL__NAME = ASSIGNABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_VAR_DECL__TYPE = ASSIGNABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_VAR_DECL__INITIAL_VALUE = ASSIGNABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Current Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_VAR_DECL__CURRENT_VALUE = ASSIGNABLE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Function Var Decl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_VAR_DECL_FEATURE_COUNT = ASSIGNABLE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Function Var Decl</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_VAR_DECL_OPERATION_COUNT = ASSIGNABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.FMUFunctionImpl <em>FMU Function</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.FMUFunctionImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFMUFunction()
	 * @generated
	 */
	int FMU_FUNCTION = 55;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.CallConstantImpl <em>Call Constant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.CallConstantImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getCallConstant()
	 * @generated
	 */
	int CALL_CONSTANT = 53;

	/**
	 * The feature id for the '<em><b>Result Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_CONSTANT__RESULT_TYPE = CALL__RESULT_TYPE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_CONSTANT__VALUE = CALL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Call Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_CONSTANT_FEATURE_COUNT = CALL_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Call Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_CONSTANT_OPERATION_COUNT = CALL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.EnumerationValueImpl <em>Enumeration Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.EnumerationValueImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getEnumerationValue()
	 * @generated
	 */
	int ENUMERATION_VALUE = 54;

	/**
	 * The feature id for the '<em><b>Enumeration Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_VALUE__ENUMERATION_VALUE = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Enumeration Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Enumeration Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION__NAME = FUNCTION__NAME;

	/**
	 * The feature id for the '<em><b>Function Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION__FUNCTION_PORTS = FUNCTION__FUNCTION_PORTS;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION__ACTION = FUNCTION__ACTION;

	/**
	 * The feature id for the '<em><b>Sub Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION__SUB_FUNCTIONS = FUNCTION__SUB_FUNCTIONS;

	/**
	 * The feature id for the '<em><b>Function Connectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION__FUNCTION_CONNECTORS = FUNCTION__FUNCTION_CONNECTORS;

	/**
	 * The feature id for the '<em><b>Function Var Decls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION__FUNCTION_VAR_DECLS = FUNCTION__FUNCTION_VAR_DECLS;

	/**
	 * The feature id for the '<em><b>Time Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION__TIME_REFERENCE = FUNCTION__TIME_REFERENCE;

	/**
	 * The feature id for the '<em><b>Runnable Fmu Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION__RUNNABLE_FMU_PATH = FUNCTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Fmu Simulation Wrapper</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION__FMU_SIMULATION_WRAPPER = FUNCTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FMU Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION_FEATURE_COUNT = FUNCTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>All Sub Functions</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION___ALL_SUB_FUNCTIONS = FUNCTION___ALL_SUB_FUNCTIONS;

	/**
	 * The operation id for the '<em>Dse start Eval Function</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION___DSE_START_EVAL_FUNCTION = FUNCTION___DSE_START_EVAL_FUNCTION;

	/**
	 * The operation id for the '<em>Dse stop Eval Function</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION___DSE_STOP_EVAL_FUNCTION = FUNCTION___DSE_STOP_EVAL_FUNCTION;

	/**
	 * The operation id for the '<em>Is Enabled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION___IS_ENABLED = FUNCTION___IS_ENABLED;

	/**
	 * The number of operations of the '<em>FMU Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION_OPERATION_COUNT = FUNCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.FMUFunctionBlockDataPortImpl <em>FMU Function Block Data Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.FMUFunctionBlockDataPortImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFMUFunctionBlockDataPort()
	 * @generated
	 */
	int FMU_FUNCTION_BLOCK_DATA_PORT = 56;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION_BLOCK_DATA_PORT__NAME = FUNCTION_BLOCK_DATA_PORT__NAME;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION_BLOCK_DATA_PORT__DATA_TYPE = FUNCTION_BLOCK_DATA_PORT__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION_BLOCK_DATA_PORT__DIRECTION = FUNCTION_BLOCK_DATA_PORT__DIRECTION;

	/**
	 * The feature id for the '<em><b>Default Input Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE = FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE;

	/**
	 * The feature id for the '<em><b>Function Var Store</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION_BLOCK_DATA_PORT__FUNCTION_VAR_STORE = FUNCTION_BLOCK_DATA_PORT__FUNCTION_VAR_STORE;

	/**
	 * The feature id for the '<em><b>Current Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION_BLOCK_DATA_PORT__CURRENT_VALUE = FUNCTION_BLOCK_DATA_PORT__CURRENT_VALUE;

	/**
	 * The feature id for the '<em><b>Runnable Fmu Variable Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION_BLOCK_DATA_PORT__RUNNABLE_FMU_VARIABLE_NAME = FUNCTION_BLOCK_DATA_PORT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FMU Function Block Data Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION_BLOCK_DATA_PORT_FEATURE_COUNT = FUNCTION_BLOCK_DATA_PORT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Outgoing Connectors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION_BLOCK_DATA_PORT___OUTGOING_CONNECTORS = FUNCTION_BLOCK_DATA_PORT___OUTGOING_CONNECTORS;

	/**
	 * The operation id for the '<em>Incoming Connectors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION_BLOCK_DATA_PORT___INCOMING_CONNECTORS = FUNCTION_BLOCK_DATA_PORT___INCOMING_CONNECTORS;

	/**
	 * The number of operations of the '<em>FMU Function Block Data Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_FUNCTION_BLOCK_DATA_PORT_OPERATION_COUNT = FUNCTION_BLOCK_DATA_PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.UnityFunctionImpl <em>Unity Function</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.UnityFunctionImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getUnityFunction()
	 * @generated
	 */
	int UNITY_FUNCTION = 57;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION__NAME = FUNCTION__NAME;

	/**
	 * The feature id for the '<em><b>Function Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION__FUNCTION_PORTS = FUNCTION__FUNCTION_PORTS;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION__ACTION = FUNCTION__ACTION;

	/**
	 * The feature id for the '<em><b>Sub Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION__SUB_FUNCTIONS = FUNCTION__SUB_FUNCTIONS;

	/**
	 * The feature id for the '<em><b>Function Connectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION__FUNCTION_CONNECTORS = FUNCTION__FUNCTION_CONNECTORS;

	/**
	 * The feature id for the '<em><b>Function Var Decls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION__FUNCTION_VAR_DECLS = FUNCTION__FUNCTION_VAR_DECLS;

	/**
	 * The feature id for the '<em><b>Time Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION__TIME_REFERENCE = FUNCTION__TIME_REFERENCE;

	/**
	 * The feature id for the '<em><b>Unity Web Page Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION__UNITY_WEB_PAGE_PATH = FUNCTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Unity Wrapper</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION__UNITY_WRAPPER = FUNCTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Unity Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION_FEATURE_COUNT = FUNCTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>All Sub Functions</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION___ALL_SUB_FUNCTIONS = FUNCTION___ALL_SUB_FUNCTIONS;

	/**
	 * The operation id for the '<em>Dse start Eval Function</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION___DSE_START_EVAL_FUNCTION = FUNCTION___DSE_START_EVAL_FUNCTION;

	/**
	 * The operation id for the '<em>Dse stop Eval Function</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION___DSE_STOP_EVAL_FUNCTION = FUNCTION___DSE_STOP_EVAL_FUNCTION;

	/**
	 * The operation id for the '<em>Is Enabled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION___IS_ENABLED = FUNCTION___IS_ENABLED;

	/**
	 * The number of operations of the '<em>Unity Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION_OPERATION_COUNT = FUNCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.UnityFunctionDataPortImpl <em>Unity Function Data Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.UnityFunctionDataPortImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getUnityFunctionDataPort()
	 * @generated
	 */
	int UNITY_FUNCTION_DATA_PORT = 58;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION_DATA_PORT__NAME = FUNCTION_BLOCK_DATA_PORT__NAME;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION_DATA_PORT__DATA_TYPE = FUNCTION_BLOCK_DATA_PORT__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION_DATA_PORT__DIRECTION = FUNCTION_BLOCK_DATA_PORT__DIRECTION;

	/**
	 * The feature id for the '<em><b>Default Input Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION_DATA_PORT__DEFAULT_INPUT_VALUE = FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE;

	/**
	 * The feature id for the '<em><b>Function Var Store</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION_DATA_PORT__FUNCTION_VAR_STORE = FUNCTION_BLOCK_DATA_PORT__FUNCTION_VAR_STORE;

	/**
	 * The feature id for the '<em><b>Current Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION_DATA_PORT__CURRENT_VALUE = FUNCTION_BLOCK_DATA_PORT__CURRENT_VALUE;

	/**
	 * The feature id for the '<em><b>Unity Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION_DATA_PORT__UNITY_NODE_NAME = FUNCTION_BLOCK_DATA_PORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Unity Variable Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION_DATA_PORT__UNITY_VARIABLE_NAME = FUNCTION_BLOCK_DATA_PORT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Unity Function Data Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION_DATA_PORT_FEATURE_COUNT = FUNCTION_BLOCK_DATA_PORT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Outgoing Connectors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION_DATA_PORT___OUTGOING_CONNECTORS = FUNCTION_BLOCK_DATA_PORT___OUTGOING_CONNECTORS;

	/**
	 * The operation id for the '<em>Incoming Connectors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION_DATA_PORT___INCOMING_CONNECTORS = FUNCTION_BLOCK_DATA_PORT___INCOMING_CONNECTORS;

	/**
	 * The number of operations of the '<em>Unity Function Data Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_FUNCTION_DATA_PORT_OPERATION_COUNT = FUNCTION_BLOCK_DATA_PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.CallPreviousImpl <em>Call Previous</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.CallPreviousImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getCallPrevious()
	 * @generated
	 */
	int CALL_PREVIOUS = 59;

	/**
	 * The feature id for the '<em><b>Result Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_PREVIOUS__RESULT_TYPE = CALL__RESULT_TYPE;

	/**
	 * The feature id for the '<em><b>Callable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_PREVIOUS__CALLABLE = CALL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Call Previous</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_PREVIOUS_FEATURE_COUNT = CALL_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Call Previous</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_PREVIOUS_OPERATION_COUNT = CALL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.DataStructPropertyImpl <em>Data Struct Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.DataStructPropertyImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getDataStructProperty()
	 * @generated
	 */
	int DATA_STRUCT_PROPERTY = 60;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCT_PROPERTY__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCT_PROPERTY__TYPE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Data Struct Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCT_PROPERTY_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Data Struct Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCT_PROPERTY_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.DataStructPropertyReferenceImpl <em>Data Struct Property Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.DataStructPropertyReferenceImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getDataStructPropertyReference()
	 * @generated
	 */
	int DATA_STRUCT_PROPERTY_REFERENCE = 61;

	/**
	 * The feature id for the '<em><b>Property Name</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCT_PROPERTY_REFERENCE__PROPERTY_NAME = 0;

	/**
	 * The number of structural features of the '<em>Data Struct Property Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCT_PROPERTY_REFERENCE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Data Struct Property Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCT_PROPERTY_REFERENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.DataStructPropertyAssignmentImpl <em>Data Struct Property Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.DataStructPropertyAssignmentImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getDataStructPropertyAssignment()
	 * @generated
	 */
	int DATA_STRUCT_PROPERTY_ASSIGNMENT = 62;

	/**
	 * The feature id for the '<em><b>Property Name</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCT_PROPERTY_ASSIGNMENT__PROPERTY_NAME = DATA_STRUCT_PROPERTY_REFERENCE__PROPERTY_NAME;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCT_PROPERTY_ASSIGNMENT__EXPRESSION = DATA_STRUCT_PROPERTY_REFERENCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Assignable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCT_PROPERTY_ASSIGNMENT__ASSIGNABLE = DATA_STRUCT_PROPERTY_REFERENCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Data Struct Property Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCT_PROPERTY_ASSIGNMENT_FEATURE_COUNT = DATA_STRUCT_PROPERTY_REFERENCE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Data Struct Property Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_STRUCT_PROPERTY_ASSIGNMENT_OPERATION_COUNT = DATA_STRUCT_PROPERTY_REFERENCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.CallDataStructPropertyImpl <em>Call Data Struct Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.CallDataStructPropertyImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getCallDataStructProperty()
	 * @generated
	 */
	int CALL_DATA_STRUCT_PROPERTY = 63;

	/**
	 * The feature id for the '<em><b>Property Name</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_DATA_STRUCT_PROPERTY__PROPERTY_NAME = DATA_STRUCT_PROPERTY_REFERENCE__PROPERTY_NAME;

	/**
	 * The feature id for the '<em><b>Result Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_DATA_STRUCT_PROPERTY__RESULT_TYPE = DATA_STRUCT_PROPERTY_REFERENCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Root Target Data Struct</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_DATA_STRUCT_PROPERTY__ROOT_TARGET_DATA_STRUCT = DATA_STRUCT_PROPERTY_REFERENCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Call Data Struct Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_DATA_STRUCT_PROPERTY_FEATURE_COUNT = DATA_STRUCT_PROPERTY_REFERENCE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Call Data Struct Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_DATA_STRUCT_PROPERTY_OPERATION_COUNT = DATA_STRUCT_PROPERTY_REFERENCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.NewDataStructImpl <em>New Data Struct</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.NewDataStructImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getNewDataStruct()
	 * @generated
	 */
	int NEW_DATA_STRUCT = 64;

	/**
	 * The feature id for the '<em><b>Result Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_DATA_STRUCT__RESULT_TYPE = EXPRESSION__RESULT_TYPE;

	/**
	 * The feature id for the '<em><b>Property Assignments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_DATA_STRUCT__PROPERTY_ASSIGNMENTS = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>New Data Struct</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_DATA_STRUCT_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>New Data Struct</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_DATA_STRUCT_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.StructValueImpl <em>Struct Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.StructValueImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getStructValue()
	 * @generated
	 */
	int STRUCT_VALUE = 65;

	/**
	 * The feature id for the '<em><b>Struct Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCT_VALUE__STRUCT_PROPERTIES = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Struct Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCT_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Struct Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCT_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.StructPropertyValueImpl <em>Struct Property Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.StructPropertyValueImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getStructPropertyValue()
	 * @generated
	 */
	int STRUCT_PROPERTY_VALUE = 66;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCT_PROPERTY_VALUE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCT_PROPERTY_VALUE__VALUE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Struct Property Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCT_PROPERTY_VALUE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Struct Property Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCT_PROPERTY_VALUE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.impl.LogImpl <em>Log</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.impl.LogImpl
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getLog()
	 * @generated
	 */
	int LOG = 67;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG__EXPRESSION = PRIMITIVE_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Log</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG_FEATURE_COUNT = PRIMITIVE_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Log</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG_OPERATION_COUNT = PRIMITIVE_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.DirectionKind <em>Direction Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.DirectionKind
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getDirectionKind()
	 * @generated
	 */
	int DIRECTION_KIND = 68;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.BinaryOperator <em>Binary Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.BinaryOperator
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getBinaryOperator()
	 * @generated
	 */
	int BINARY_OPERATOR = 69;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.UnaryOperator <em>Unary Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.UnaryOperator
	 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getUnaryOperator()
	 * @generated
	 */
	int UNARY_OPERATOR = 70;

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.FCLModel <em>FCL Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FCL Model</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FCLModel
	 * @generated
	 */
	EClass getFCLModel();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.FCLModel#getModeAutomata <em>Mode Automata</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Mode Automata</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FCLModel#getModeAutomata()
	 * @see #getFCLModel()
	 * @generated
	 */
	EReference getFCLModel_ModeAutomata();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.FCLModel#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Events</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FCLModel#getEvents()
	 * @see #getFCLModel()
	 * @generated
	 */
	EReference getFCLModel_Events();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.FCLModel#getProcedures <em>Procedures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Procedures</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FCLModel#getProcedures()
	 * @see #getFCLModel()
	 * @generated
	 */
	EReference getFCLModel_Procedures();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.FCLModel#getDataTypes <em>Data Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Types</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FCLModel#getDataTypes()
	 * @see #getFCLModel()
	 * @generated
	 */
	EReference getFCLModel_DataTypes();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.FCLModel#getDataflow <em>Dataflow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dataflow</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FCLModel#getDataflow()
	 * @see #getFCLModel()
	 * @generated
	 */
	EReference getFCLModel_Dataflow();

	/**
	 * Returns the meta object for the reference list '{@link fr.inria.glose.fcl.model.fcl.FCLModel#getReceivedEvents <em>Received Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Received Events</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FCLModel#getReceivedEvents()
	 * @see #getFCLModel()
	 * @generated
	 */
	EReference getFCLModel_ReceivedEvents();

	/**
	 * Returns the meta object for the reference list '{@link fr.inria.glose.fcl.model.fcl.FCLModel#getProcedureStack <em>Procedure Stack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Procedure Stack</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FCLModel#getProcedureStack()
	 * @see #getFCLModel()
	 * @generated
	 */
	EReference getFCLModel_ProcedureStack();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.FCLModel#getIndentation <em>Indentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Indentation</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FCLModel#getIndentation()
	 * @see #getFCLModel()
	 * @generated
	 */
	EAttribute getFCLModel_Indentation();

	/**
	 * Returns the meta object for the '{@link fr.inria.glose.fcl.model.fcl.FCLModel#allFunctions() <em>All Functions</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Functions</em>' operation.
	 * @see fr.inria.glose.fcl.model.fcl.FCLModel#allFunctions()
	 * @generated
	 */
	EOperation getFCLModel__AllFunctions();

	/**
	 * Returns the meta object for the '{@link fr.inria.glose.fcl.model.fcl.FCLModel#allConnectors() <em>All Connectors</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Connectors</em>' operation.
	 * @see fr.inria.glose.fcl.model.fcl.FCLModel#allConnectors()
	 * @generated
	 */
	EOperation getFCLModel__AllConnectors();

	/**
	 * Returns the meta object for the '{@link fr.inria.glose.fcl.model.fcl.FCLModel#propagateDelayedResults() <em>Propagate Delayed Results</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Propagate Delayed Results</em>' operation.
	 * @see fr.inria.glose.fcl.model.fcl.FCLModel#propagateDelayedResults()
	 * @generated
	 */
	EOperation getFCLModel__PropagateDelayedResults();

	/**
	 * Returns the meta object for the '{@link fr.inria.glose.fcl.model.fcl.FCLModel#evaluateModeAutomata() <em>Evaluate Mode Automata</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Evaluate Mode Automata</em>' operation.
	 * @see fr.inria.glose.fcl.model.fcl.FCLModel#evaluateModeAutomata()
	 * @generated
	 */
	EOperation getFCLModel__EvaluateModeAutomata();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.ModeAutomata <em>Mode Automata</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mode Automata</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ModeAutomata
	 * @generated
	 */
	EClass getModeAutomata();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.ModeAutomata#getModes <em>Modes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Modes</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ModeAutomata#getModes()
	 * @see #getModeAutomata()
	 * @generated
	 */
	EReference getModeAutomata_Modes();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.ModeAutomata#getInitialMode <em>Initial Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Initial Mode</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ModeAutomata#getInitialMode()
	 * @see #getModeAutomata()
	 * @generated
	 */
	EReference getModeAutomata_InitialMode();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.ModeAutomata#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transitions</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ModeAutomata#getTransitions()
	 * @see #getModeAutomata()
	 * @generated
	 */
	EReference getModeAutomata_Transitions();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.ModeAutomata#getCurrentMode <em>Current Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current Mode</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ModeAutomata#getCurrentMode()
	 * @see #getModeAutomata()
	 * @generated
	 */
	EReference getModeAutomata_CurrentMode();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.Transition#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Transition#getAction()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Action();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.Transition#getGuard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Guard</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Transition#getGuard()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Guard();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.Transition#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Transition#getEvent()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Event();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.Transition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Transition#getTarget()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Target();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.Transition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Transition#getSource()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Source();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.Mode <em>Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mode</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Mode
	 * @generated
	 */
	EClass getMode();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.Mode#getEntry <em>Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Entry</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Mode#getEntry()
	 * @see #getMode()
	 * @generated
	 */
	EReference getMode_Entry();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.Mode#getExit <em>Exit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Exit</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Mode#getExit()
	 * @see #getMode()
	 * @generated
	 */
	EReference getMode_Exit();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.Mode#isFinal <em>Final</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Mode#isFinal()
	 * @see #getMode()
	 * @generated
	 */
	EAttribute getMode_Final();

	/**
	 * Returns the meta object for the reference list '{@link fr.inria.glose.fcl.model.fcl.Mode#getEnabledFunctions <em>Enabled Functions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Enabled Functions</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Mode#getEnabledFunctions()
	 * @see #getMode()
	 * @generated
	 */
	EReference getMode_EnabledFunctions();

	/**
	 * Returns the meta object for the reference list '{@link fr.inria.glose.fcl.model.fcl.Mode#getIncomingTransition <em>Incoming Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Incoming Transition</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Mode#getIncomingTransition()
	 * @see #getMode()
	 * @generated
	 */
	EReference getMode_IncomingTransition();

	/**
	 * Returns the meta object for the reference list '{@link fr.inria.glose.fcl.model.fcl.Mode#getOutgoingTransition <em>Outgoing Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Outgoing Transition</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Mode#getOutgoingTransition()
	 * @see #getMode()
	 * @generated
	 */
	EReference getMode_OutgoingTransition();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Expression
	 * @generated
	 */
	EClass getExpression();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.Expression#getResultType <em>Result Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Result Type</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Expression#getResultType()
	 * @see #getExpression()
	 * @generated
	 */
	EReference getExpression_ResultType();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.Procedure <em>Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Procedure</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Procedure
	 * @generated
	 */
	EClass getProcedure();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.Procedure#getProcedureParameters <em>Procedure Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Procedure Parameters</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Procedure#getProcedureParameters()
	 * @see #getProcedure()
	 * @generated
	 */
	EReference getProcedure_ProcedureParameters();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.Procedure#getReturnType <em>Return Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Return Type</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Procedure#getReturnType()
	 * @see #getProcedure()
	 * @generated
	 */
	EReference getProcedure_ReturnType();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.ActionBlock <em>Action Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action Block</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ActionBlock
	 * @generated
	 */
	EClass getActionBlock();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.ActionBlock#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actions</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ActionBlock#getActions()
	 * @see #getActionBlock()
	 * @generated
	 */
	EReference getActionBlock_Actions();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.DataStructType <em>Data Struct Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Struct Type</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.DataStructType
	 * @generated
	 */
	EClass getDataStructType();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.DataStructType#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.DataStructType#getProperties()
	 * @see #getDataStructType()
	 * @generated
	 */
	EReference getDataStructType_Properties();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort <em>Function Block Data Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Block Data Port</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort
	 * @generated
	 */
	EClass getFunctionBlockDataPort();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort#getDefaultInputValue <em>Default Input Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Default Input Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort#getDefaultInputValue()
	 * @see #getFunctionBlockDataPort()
	 * @generated
	 */
	EReference getFunctionBlockDataPort_DefaultInputValue();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort#getFunctionVarStore <em>Function Var Store</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Function Var Store</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort#getFunctionVarStore()
	 * @see #getFunctionBlockDataPort()
	 * @generated
	 */
	EReference getFunctionBlockDataPort_FunctionVarStore();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort#getCurrentValue <em>Current Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort#getCurrentValue()
	 * @see #getFunctionBlockDataPort()
	 * @generated
	 */
	EReference getFunctionBlockDataPort_CurrentValue();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.DataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Type</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.DataType
	 * @generated
	 */
	EClass getDataType();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.ProcedureParameter <em>Procedure Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Procedure Parameter</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ProcedureParameter
	 * @generated
	 */
	EClass getProcedureParameter();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.ProcedureParameter#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ProcedureParameter#getType()
	 * @see #getProcedureParameter()
	 * @generated
	 */
	EReference getProcedureParameter_Type();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.FunctionPort <em>Function Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Port</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionPort
	 * @generated
	 */
	EClass getFunctionPort();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.FunctionPort#getDataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Data Type</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionPort#getDataType()
	 * @see #getFunctionPort()
	 * @generated
	 */
	EReference getFunctionPort_DataType();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.FunctionPort#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionPort#getDirection()
	 * @see #getFunctionPort()
	 * @generated
	 */
	EAttribute getFunctionPort_Direction();

	/**
	 * Returns the meta object for the '{@link fr.inria.glose.fcl.model.fcl.FunctionPort#outgoingConnectors() <em>Outgoing Connectors</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Outgoing Connectors</em>' operation.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionPort#outgoingConnectors()
	 * @generated
	 */
	EOperation getFunctionPort__OutgoingConnectors();

	/**
	 * Returns the meta object for the '{@link fr.inria.glose.fcl.model.fcl.FunctionPort#incomingConnectors() <em>Incoming Connectors</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Incoming Connectors</em>' operation.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionPort#incomingConnectors()
	 * @generated
	 */
	EOperation getFunctionPort__IncomingConnectors();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.FunctionConnector <em>Function Connector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Connector</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionConnector
	 * @generated
	 */
	EClass getFunctionConnector();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.FunctionConnector#getEmitter <em>Emitter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Emitter</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionConnector#getEmitter()
	 * @see #getFunctionConnector()
	 * @generated
	 */
	EReference getFunctionConnector_Emitter();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.FunctionConnector#getReceiver <em>Receiver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Receiver</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionConnector#getReceiver()
	 * @see #getFunctionConnector()
	 * @generated
	 */
	EReference getFunctionConnector_Receiver();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.FunctionConnector#isDelayed <em>Delayed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delayed</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionConnector#isDelayed()
	 * @see #getFunctionConnector()
	 * @generated
	 */
	EAttribute getFunctionConnector_Delayed();

	/**
	 * Returns the meta object for the '{@link fr.inria.glose.fcl.model.fcl.FunctionConnector#dse_sendData() <em>Dse send Data</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Dse send Data</em>' operation.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionConnector#dse_sendData()
	 * @generated
	 */
	EOperation getFunctionConnector__Dse_sendData();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.ExternalEvent <em>External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Event</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ExternalEvent
	 * @generated
	 */
	EClass getExternalEvent();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.BinaryExpression <em>Binary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Expression</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.BinaryExpression
	 * @generated
	 */
	EClass getBinaryExpression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.BinaryExpression#getLhsOperand <em>Lhs Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Lhs Operand</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.BinaryExpression#getLhsOperand()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EReference getBinaryExpression_LhsOperand();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.BinaryExpression#getRhsOperand <em>Rhs Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Rhs Operand</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.BinaryExpression#getRhsOperand()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EReference getBinaryExpression_RhsOperand();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.UnaryExpression <em>Unary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Expression</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.UnaryExpression
	 * @generated
	 */
	EClass getUnaryExpression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.UnaryExpression#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.UnaryExpression#getOperand()
	 * @see #getUnaryExpression()
	 * @generated
	 */
	EReference getUnaryExpression_Operand();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.Cast <em>Cast</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cast</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Cast
	 * @generated
	 */
	EClass getCast();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.Call <em>Call</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Call
	 * @generated
	 */
	EClass getCall();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.PrimitiveAction <em>Primitive Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Primitive Action</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.PrimitiveAction
	 * @generated
	 */
	EClass getPrimitiveAction();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.Assignment <em>Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assignment</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Assignment
	 * @generated
	 */
	EClass getAssignment();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.Assignment#getAssignable <em>Assignable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Assignable</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Assignment#getAssignable()
	 * @see #getAssignment()
	 * @generated
	 */
	EReference getAssignment_Assignable();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.Assignment#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Assignment#getExpression()
	 * @see #getAssignment()
	 * @generated
	 */
	EReference getAssignment_Expression();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.ControlStructureAction <em>Control Structure Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control Structure Action</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ControlStructureAction
	 * @generated
	 */
	EClass getControlStructureAction();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.IfAction <em>If Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If Action</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.IfAction
	 * @generated
	 */
	EClass getIfAction();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.IfAction#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.IfAction#getCondition()
	 * @see #getIfAction()
	 * @generated
	 */
	EReference getIfAction_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.IfAction#getThenAction <em>Then Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Then Action</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.IfAction#getThenAction()
	 * @see #getIfAction()
	 * @generated
	 */
	EReference getIfAction_ThenAction();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.IfAction#getElseAction <em>Else Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Else Action</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.IfAction#getElseAction()
	 * @see #getIfAction()
	 * @generated
	 */
	EReference getIfAction_ElseAction();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.WhileAction <em>While Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>While Action</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.WhileAction
	 * @generated
	 */
	EClass getWhileAction();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.WhileAction#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.WhileAction#getCondition()
	 * @see #getWhileAction()
	 * @generated
	 */
	EReference getWhileAction_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.WhileAction#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.WhileAction#getAction()
	 * @see #getWhileAction()
	 * @generated
	 */
	EReference getWhileAction_Action();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.ProcedureCall <em>Procedure Call</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Procedure Call</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ProcedureCall
	 * @generated
	 */
	EClass getProcedureCall();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.ProcedureCall#getProcedure <em>Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Procedure</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ProcedureCall#getProcedure()
	 * @see #getProcedureCall()
	 * @generated
	 */
	EReference getProcedureCall_Procedure();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.ProcedureCall#getProcedureCallArguments <em>Procedure Call Arguments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Procedure Call Arguments</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ProcedureCall#getProcedureCallArguments()
	 * @see #getProcedureCall()
	 * @generated
	 */
	EReference getProcedureCall_ProcedureCallArguments();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.VarDecl <em>Var Decl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Var Decl</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.VarDecl
	 * @generated
	 */
	EClass getVarDecl();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.VarDecl#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.VarDecl#getType()
	 * @see #getVarDecl()
	 * @generated
	 */
	EReference getVarDecl_Type();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.VarDecl#getInitialValue <em>Initial Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Initial Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.VarDecl#getInitialValue()
	 * @see #getVarDecl()
	 * @generated
	 */
	EReference getVarDecl_InitialValue();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.CallDeclarationReference <em>Call Declaration Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Declaration Reference</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.CallDeclarationReference
	 * @generated
	 */
	EClass getCallDeclarationReference();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.CallDeclarationReference#getCallable <em>Callable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Callable</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.CallDeclarationReference#getCallable()
	 * @see #getCallDeclarationReference()
	 * @generated
	 */
	EReference getCallDeclarationReference_Callable();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.FunctionPortReference <em>Function Port Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Port Reference</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionPortReference
	 * @generated
	 */
	EClass getFunctionPortReference();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.FunctionPortReference#getFunctionPort <em>Function Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Function Port</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionPortReference#getFunctionPort()
	 * @see #getFunctionPortReference()
	 * @generated
	 */
	EReference getFunctionPortReference_FunctionPort();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.BasicOperatorUnaryExpression <em>Basic Operator Unary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Basic Operator Unary Expression</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.BasicOperatorUnaryExpression
	 * @generated
	 */
	EClass getBasicOperatorUnaryExpression();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.BasicOperatorUnaryExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.BasicOperatorUnaryExpression#getOperator()
	 * @see #getBasicOperatorUnaryExpression()
	 * @generated
	 */
	EAttribute getBasicOperatorUnaryExpression_Operator();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.BasicOperatorBinaryExpression <em>Basic Operator Binary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Basic Operator Binary Expression</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.BasicOperatorBinaryExpression
	 * @generated
	 */
	EClass getBasicOperatorBinaryExpression();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.BasicOperatorBinaryExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.BasicOperatorBinaryExpression#getOperator()
	 * @see #getBasicOperatorBinaryExpression()
	 * @generated
	 */
	EAttribute getBasicOperatorBinaryExpression_Operator();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.ValueType <em>Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Type</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ValueType
	 * @generated
	 */
	EClass getValueType();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.ValueType#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unit</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ValueType#getUnit()
	 * @see #getValueType()
	 * @generated
	 */
	EAttribute getValueType_Unit();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.ValueType#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ValueType#getDescription()
	 * @see #getValueType()
	 * @generated
	 */
	EAttribute getValueType_Description();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.IntegerValueType <em>Integer Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Value Type</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.IntegerValueType
	 * @generated
	 */
	EClass getIntegerValueType();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.IntegerValueType#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.IntegerValueType#getMin()
	 * @see #getIntegerValueType()
	 * @generated
	 */
	EAttribute getIntegerValueType_Min();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.IntegerValueType#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.IntegerValueType#getMax()
	 * @see #getIntegerValueType()
	 * @generated
	 */
	EAttribute getIntegerValueType_Max();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.FloatValueType <em>Float Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Float Value Type</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FloatValueType
	 * @generated
	 */
	EClass getFloatValueType();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.FloatValueType#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FloatValueType#getMin()
	 * @see #getFloatValueType()
	 * @generated
	 */
	EAttribute getFloatValueType_Min();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.FloatValueType#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FloatValueType#getMax()
	 * @see #getFloatValueType()
	 * @generated
	 */
	EAttribute getFloatValueType_Max();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.BooleanValueType <em>Boolean Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Value Type</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.BooleanValueType
	 * @generated
	 */
	EClass getBooleanValueType();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.StringValueType <em>String Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Value Type</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.StringValueType
	 * @generated
	 */
	EClass getStringValueType();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.EnumerationValueType <em>Enumeration Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enumeration Value Type</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.EnumerationValueType
	 * @generated
	 */
	EClass getEnumerationValueType();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.EnumerationValueType#getEnumerationLiteral <em>Enumeration Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Enumeration Literal</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.EnumerationValueType#getEnumerationLiteral()
	 * @see #getEnumerationValueType()
	 * @generated
	 */
	EReference getEnumerationValueType_EnumerationLiteral();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.EnumerationLiteral <em>Enumeration Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enumeration Literal</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.EnumerationLiteral
	 * @generated
	 */
	EClass getEnumerationLiteral();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.Assignable <em>Assignable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assignable</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Assignable
	 * @generated
	 */
	EClass getAssignable();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.CallableDeclaration <em>Callable Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Callable Declaration</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.CallableDeclaration
	 * @generated
	 */
	EClass getCallableDeclaration();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.Function <em>Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Function
	 * @generated
	 */
	EClass getFunction();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.Function#getFunctionPorts <em>Function Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Function Ports</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Function#getFunctionPorts()
	 * @see #getFunction()
	 * @generated
	 */
	EReference getFunction_FunctionPorts();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.Function#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Function#getAction()
	 * @see #getFunction()
	 * @generated
	 */
	EReference getFunction_Action();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.Function#getSubFunctions <em>Sub Functions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Functions</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Function#getSubFunctions()
	 * @see #getFunction()
	 * @generated
	 */
	EReference getFunction_SubFunctions();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.Function#getFunctionConnectors <em>Function Connectors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Function Connectors</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Function#getFunctionConnectors()
	 * @see #getFunction()
	 * @generated
	 */
	EReference getFunction_FunctionConnectors();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.Function#getFunctionVarDecls <em>Function Var Decls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Function Var Decls</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Function#getFunctionVarDecls()
	 * @see #getFunction()
	 * @generated
	 */
	EReference getFunction_FunctionVarDecls();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.Function#getTimeReference <em>Time Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Time Reference</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Function#getTimeReference()
	 * @see #getFunction()
	 * @generated
	 */
	EReference getFunction_TimeReference();

	/**
	 * Returns the meta object for the '{@link fr.inria.glose.fcl.model.fcl.Function#allSubFunctions() <em>All Sub Functions</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Sub Functions</em>' operation.
	 * @see fr.inria.glose.fcl.model.fcl.Function#allSubFunctions()
	 * @generated
	 */
	EOperation getFunction__AllSubFunctions();

	/**
	 * Returns the meta object for the '{@link fr.inria.glose.fcl.model.fcl.Function#dse_startEvalFunction() <em>Dse start Eval Function</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Dse start Eval Function</em>' operation.
	 * @see fr.inria.glose.fcl.model.fcl.Function#dse_startEvalFunction()
	 * @generated
	 */
	EOperation getFunction__Dse_startEvalFunction();

	/**
	 * Returns the meta object for the '{@link fr.inria.glose.fcl.model.fcl.Function#dse_stopEvalFunction() <em>Dse stop Eval Function</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Dse stop Eval Function</em>' operation.
	 * @see fr.inria.glose.fcl.model.fcl.Function#dse_stopEvalFunction()
	 * @generated
	 */
	EOperation getFunction__Dse_stopEvalFunction();

	/**
	 * Returns the meta object for the '{@link fr.inria.glose.fcl.model.fcl.Function#isEnabled() <em>Is Enabled</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Enabled</em>' operation.
	 * @see fr.inria.glose.fcl.model.fcl.Function#isEnabled()
	 * @generated
	 */
	EOperation getFunction__IsEnabled();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.FunctionTimeReference <em>Function Time Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Time Reference</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionTimeReference
	 * @generated
	 */
	EClass getFunctionTimeReference();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.FunctionTimeReference#getObservableVar <em>Observable Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Observable Var</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionTimeReference#getObservableVar()
	 * @see #getFunctionTimeReference()
	 * @generated
	 */
	EReference getFunctionTimeReference_ObservableVar();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.FunctionTimeReference#getIncrement <em>Increment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Increment</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionTimeReference#getIncrement()
	 * @see #getFunctionTimeReference()
	 * @generated
	 */
	EReference getFunctionTimeReference_Increment();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.ExternalProcedure <em>External Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Procedure</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.ExternalProcedure
	 * @generated
	 */
	EClass getExternalProcedure();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.FCLProcedure <em>FCL Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FCL Procedure</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FCLProcedure
	 * @generated
	 */
	EClass getFCLProcedure();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.FCLProcedure#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FCLProcedure#getAction()
	 * @see #getFCLProcedure()
	 * @generated
	 */
	EReference getFCLProcedure_Action();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.FCLProcedureReturn <em>FCL Procedure Return</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FCL Procedure Return</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FCLProcedureReturn
	 * @generated
	 */
	EClass getFCLProcedureReturn();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.FCLProcedureReturn#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FCLProcedureReturn#getExpression()
	 * @see #getFCLProcedureReturn()
	 * @generated
	 */
	EReference getFCLProcedureReturn_Expression();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.JavaProcedure <em>Java Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Procedure</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.JavaProcedure
	 * @generated
	 */
	EClass getJavaProcedure();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.JavaProcedure#getMethodFullQualifiedName <em>Method Full Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Method Full Qualified Name</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.JavaProcedure#getMethodFullQualifiedName()
	 * @see #getJavaProcedure()
	 * @generated
	 */
	EAttribute getJavaProcedure_MethodFullQualifiedName();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.JavaProcedure#isStatic <em>Static</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Static</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.JavaProcedure#isStatic()
	 * @see #getJavaProcedure()
	 * @generated
	 */
	EAttribute getJavaProcedure_Static();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.IntegerValue <em>Integer Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.IntegerValue
	 * @generated
	 */
	EClass getIntegerValue();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.IntegerValue#getIntValue <em>Int Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Int Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.IntegerValue#getIntValue()
	 * @see #getIntegerValue()
	 * @generated
	 */
	EAttribute getIntegerValue_IntValue();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.FloatValue <em>Float Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Float Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FloatValue
	 * @generated
	 */
	EClass getFloatValue();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.FloatValue#getFloatValue <em>Float Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Float Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FloatValue#getFloatValue()
	 * @see #getFloatValue()
	 * @generated
	 */
	EAttribute getFloatValue_FloatValue();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.StringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.StringValue
	 * @generated
	 */
	EClass getStringValue();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.StringValue#getStringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>String Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.StringValue#getStringValue()
	 * @see #getStringValue()
	 * @generated
	 */
	EAttribute getStringValue_StringValue();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.BooleanValue <em>Boolean Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.BooleanValue
	 * @generated
	 */
	EClass getBooleanValue();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.BooleanValue#isBooleanValue <em>Boolean Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Boolean Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.BooleanValue#isBooleanValue()
	 * @see #getBooleanValue()
	 * @generated
	 */
	EAttribute getBooleanValue_BooleanValue();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.FunctionVarDecl <em>Function Var Decl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Var Decl</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionVarDecl
	 * @generated
	 */
	EClass getFunctionVarDecl();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.FunctionVarDecl#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionVarDecl#getType()
	 * @see #getFunctionVarDecl()
	 * @generated
	 */
	EReference getFunctionVarDecl_Type();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.FunctionVarDecl#getInitialValue <em>Initial Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Initial Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionVarDecl#getInitialValue()
	 * @see #getFunctionVarDecl()
	 * @generated
	 */
	EReference getFunctionVarDecl_InitialValue();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.FunctionVarDecl#getCurrentValue <em>Current Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FunctionVarDecl#getCurrentValue()
	 * @see #getFunctionVarDecl()
	 * @generated
	 */
	EReference getFunctionVarDecl_CurrentValue();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.FMUFunction <em>FMU Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FMU Function</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FMUFunction
	 * @generated
	 */
	EClass getFMUFunction();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.FMUFunction#getRunnableFmuPath <em>Runnable Fmu Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Runnable Fmu Path</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FMUFunction#getRunnableFmuPath()
	 * @see #getFMUFunction()
	 * @generated
	 */
	EAttribute getFMUFunction_RunnableFmuPath();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.FMUFunction#getFmuSimulationWrapper <em>Fmu Simulation Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Fmu Simulation Wrapper</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FMUFunction#getFmuSimulationWrapper()
	 * @see #getFMUFunction()
	 * @generated
	 */
	EReference getFMUFunction_FmuSimulationWrapper();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.Value <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Value
	 * @generated
	 */
	EClass getValue();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.CallConstant <em>Call Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Constant</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.CallConstant
	 * @generated
	 */
	EClass getCallConstant();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.CallConstant#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.CallConstant#getValue()
	 * @see #getCallConstant()
	 * @generated
	 */
	EReference getCallConstant_Value();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.EnumerationValue <em>Enumeration Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enumeration Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.EnumerationValue
	 * @generated
	 */
	EClass getEnumerationValue();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.EnumerationValue#getEnumerationValue <em>Enumeration Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Enumeration Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.EnumerationValue#getEnumerationValue()
	 * @see #getEnumerationValue()
	 * @generated
	 */
	EReference getEnumerationValue_EnumerationValue();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort <em>FMU Function Block Data Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FMU Function Block Data Port</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort
	 * @generated
	 */
	EClass getFMUFunctionBlockDataPort();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort#getRunnableFmuVariableName <em>Runnable Fmu Variable Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Runnable Fmu Variable Name</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort#getRunnableFmuVariableName()
	 * @see #getFMUFunctionBlockDataPort()
	 * @generated
	 */
	EAttribute getFMUFunctionBlockDataPort_RunnableFmuVariableName();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.UnityFunction <em>Unity Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unity Function</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.UnityFunction
	 * @generated
	 */
	EClass getUnityFunction();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.UnityFunction#getUnityWebPagePath <em>Unity Web Page Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unity Web Page Path</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.UnityFunction#getUnityWebPagePath()
	 * @see #getUnityFunction()
	 * @generated
	 */
	EAttribute getUnityFunction_UnityWebPagePath();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.UnityFunction#getUnityWrapper <em>Unity Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Unity Wrapper</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.UnityFunction#getUnityWrapper()
	 * @see #getUnityFunction()
	 * @generated
	 */
	EReference getUnityFunction_UnityWrapper();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort <em>Unity Function Data Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unity Function Data Port</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort
	 * @generated
	 */
	EClass getUnityFunctionDataPort();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort#getUnityNodeName <em>Unity Node Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unity Node Name</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort#getUnityNodeName()
	 * @see #getUnityFunctionDataPort()
	 * @generated
	 */
	EAttribute getUnityFunctionDataPort_UnityNodeName();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort#getUnityVariableName <em>Unity Variable Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unity Variable Name</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort#getUnityVariableName()
	 * @see #getUnityFunctionDataPort()
	 * @generated
	 */
	EAttribute getUnityFunctionDataPort_UnityVariableName();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.CallPrevious <em>Call Previous</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Previous</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.CallPrevious
	 * @generated
	 */
	EClass getCallPrevious();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.CallPrevious#getCallable <em>Callable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Callable</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.CallPrevious#getCallable()
	 * @see #getCallPrevious()
	 * @generated
	 */
	EReference getCallPrevious_Callable();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.DataStructProperty <em>Data Struct Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Struct Property</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.DataStructProperty
	 * @generated
	 */
	EClass getDataStructProperty();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.DataStructProperty#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.DataStructProperty#getType()
	 * @see #getDataStructProperty()
	 * @generated
	 */
	EReference getDataStructProperty_Type();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.DataStructPropertyReference <em>Data Struct Property Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Struct Property Reference</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.DataStructPropertyReference
	 * @generated
	 */
	EClass getDataStructPropertyReference();

	/**
	 * Returns the meta object for the attribute list '{@link fr.inria.glose.fcl.model.fcl.DataStructPropertyReference#getPropertyName <em>Property Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Property Name</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.DataStructPropertyReference#getPropertyName()
	 * @see #getDataStructPropertyReference()
	 * @generated
	 */
	EAttribute getDataStructPropertyReference_PropertyName();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment <em>Data Struct Property Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Struct Property Assignment</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment
	 * @generated
	 */
	EClass getDataStructPropertyAssignment();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment#getExpression()
	 * @see #getDataStructPropertyAssignment()
	 * @generated
	 */
	EReference getDataStructPropertyAssignment_Expression();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment#getAssignable <em>Assignable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Assignable</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment#getAssignable()
	 * @see #getDataStructPropertyAssignment()
	 * @generated
	 */
	EReference getDataStructPropertyAssignment_Assignable();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.CallDataStructProperty <em>Call Data Struct Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Data Struct Property</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.CallDataStructProperty
	 * @generated
	 */
	EClass getCallDataStructProperty();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.CallDataStructProperty#getRootTargetDataStruct <em>Root Target Data Struct</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Root Target Data Struct</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.CallDataStructProperty#getRootTargetDataStruct()
	 * @see #getCallDataStructProperty()
	 * @generated
	 */
	EReference getCallDataStructProperty_RootTargetDataStruct();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.NewDataStruct <em>New Data Struct</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>New Data Struct</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.NewDataStruct
	 * @generated
	 */
	EClass getNewDataStruct();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.NewDataStruct#getPropertyAssignments <em>Property Assignments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Property Assignments</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.NewDataStruct#getPropertyAssignments()
	 * @see #getNewDataStruct()
	 * @generated
	 */
	EReference getNewDataStruct_PropertyAssignments();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.StructValue <em>Struct Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Struct Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.StructValue
	 * @generated
	 */
	EClass getStructValue();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.StructValue#getStructProperties <em>Struct Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Struct Properties</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.StructValue#getStructProperties()
	 * @see #getStructValue()
	 * @generated
	 */
	EReference getStructValue_StructProperties();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.StructPropertyValue <em>Struct Property Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Struct Property Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.StructPropertyValue
	 * @generated
	 */
	EClass getStructPropertyValue();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.StructPropertyValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.StructPropertyValue#getValue()
	 * @see #getStructPropertyValue()
	 * @generated
	 */
	EReference getStructPropertyValue_Value();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.Log <em>Log</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Log</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Log
	 * @generated
	 */
	EClass getLog();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.Log#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Expression</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.Log#getExpression()
	 * @see #getLog()
	 * @generated
	 */
	EReference getLog_Expression();

	/**
	 * Returns the meta object for enum '{@link fr.inria.glose.fcl.model.fcl.DirectionKind <em>Direction Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Direction Kind</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.DirectionKind
	 * @generated
	 */
	EEnum getDirectionKind();

	/**
	 * Returns the meta object for enum '{@link fr.inria.glose.fcl.model.fcl.BinaryOperator <em>Binary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Binary Operator</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.BinaryOperator
	 * @generated
	 */
	EEnum getBinaryOperator();

	/**
	 * Returns the meta object for enum '{@link fr.inria.glose.fcl.model.fcl.UnaryOperator <em>Unary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Unary Operator</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.UnaryOperator
	 * @generated
	 */
	EEnum getUnaryOperator();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FclFactory getFclFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.FCLModelImpl <em>FCL Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.FCLModelImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFCLModel()
		 * @generated
		 */
		EClass FCL_MODEL = eINSTANCE.getFCLModel();

		/**
		 * The meta object literal for the '<em><b>Mode Automata</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FCL_MODEL__MODE_AUTOMATA = eINSTANCE.getFCLModel_ModeAutomata();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FCL_MODEL__EVENTS = eINSTANCE.getFCLModel_Events();

		/**
		 * The meta object literal for the '<em><b>Procedures</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FCL_MODEL__PROCEDURES = eINSTANCE.getFCLModel_Procedures();

		/**
		 * The meta object literal for the '<em><b>Data Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FCL_MODEL__DATA_TYPES = eINSTANCE.getFCLModel_DataTypes();

		/**
		 * The meta object literal for the '<em><b>Dataflow</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FCL_MODEL__DATAFLOW = eINSTANCE.getFCLModel_Dataflow();

		/**
		 * The meta object literal for the '<em><b>Received Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FCL_MODEL__RECEIVED_EVENTS = eINSTANCE.getFCLModel_ReceivedEvents();

		/**
		 * The meta object literal for the '<em><b>Procedure Stack</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FCL_MODEL__PROCEDURE_STACK = eINSTANCE.getFCLModel_ProcedureStack();

		/**
		 * The meta object literal for the '<em><b>Indentation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FCL_MODEL__INDENTATION = eINSTANCE.getFCLModel_Indentation();

		/**
		 * The meta object literal for the '<em><b>All Functions</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FCL_MODEL___ALL_FUNCTIONS = eINSTANCE.getFCLModel__AllFunctions();

		/**
		 * The meta object literal for the '<em><b>All Connectors</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FCL_MODEL___ALL_CONNECTORS = eINSTANCE.getFCLModel__AllConnectors();

		/**
		 * The meta object literal for the '<em><b>Propagate Delayed Results</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FCL_MODEL___PROPAGATE_DELAYED_RESULTS = eINSTANCE.getFCLModel__PropagateDelayedResults();

		/**
		 * The meta object literal for the '<em><b>Evaluate Mode Automata</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FCL_MODEL___EVALUATE_MODE_AUTOMATA = eINSTANCE.getFCLModel__EvaluateModeAutomata();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.ModeAutomataImpl <em>Mode Automata</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.ModeAutomataImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getModeAutomata()
		 * @generated
		 */
		EClass MODE_AUTOMATA = eINSTANCE.getModeAutomata();

		/**
		 * The meta object literal for the '<em><b>Modes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODE_AUTOMATA__MODES = eINSTANCE.getModeAutomata_Modes();

		/**
		 * The meta object literal for the '<em><b>Initial Mode</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODE_AUTOMATA__INITIAL_MODE = eINSTANCE.getModeAutomata_InitialMode();

		/**
		 * The meta object literal for the '<em><b>Transitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODE_AUTOMATA__TRANSITIONS = eINSTANCE.getModeAutomata_Transitions();

		/**
		 * The meta object literal for the '<em><b>Current Mode</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODE_AUTOMATA__CURRENT_MODE = eINSTANCE.getModeAutomata_CurrentMode();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.TransitionImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__ACTION = eINSTANCE.getTransition_Action();

		/**
		 * The meta object literal for the '<em><b>Guard</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__GUARD = eINSTANCE.getTransition_Guard();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__EVENT = eINSTANCE.getTransition_Event();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TARGET = eINSTANCE.getTransition_Target();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__SOURCE = eINSTANCE.getTransition_Source();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.ModeImpl <em>Mode</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.ModeImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getMode()
		 * @generated
		 */
		EClass MODE = eINSTANCE.getMode();

		/**
		 * The meta object literal for the '<em><b>Entry</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODE__ENTRY = eINSTANCE.getMode_Entry();

		/**
		 * The meta object literal for the '<em><b>Exit</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODE__EXIT = eINSTANCE.getMode_Exit();

		/**
		 * The meta object literal for the '<em><b>Final</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODE__FINAL = eINSTANCE.getMode_Final();

		/**
		 * The meta object literal for the '<em><b>Enabled Functions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODE__ENABLED_FUNCTIONS = eINSTANCE.getMode_EnabledFunctions();

		/**
		 * The meta object literal for the '<em><b>Incoming Transition</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODE__INCOMING_TRANSITION = eINSTANCE.getMode_IncomingTransition();

		/**
		 * The meta object literal for the '<em><b>Outgoing Transition</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODE__OUTGOING_TRANSITION = eINSTANCE.getMode_OutgoingTransition();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.ActionImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.EventImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.ExpressionImpl <em>Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.ExpressionImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getExpression()
		 * @generated
		 */
		EClass EXPRESSION = eINSTANCE.getExpression();

		/**
		 * The meta object literal for the '<em><b>Result Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXPRESSION__RESULT_TYPE = eINSTANCE.getExpression_ResultType();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.ProcedureImpl <em>Procedure</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.ProcedureImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getProcedure()
		 * @generated
		 */
		EClass PROCEDURE = eINSTANCE.getProcedure();

		/**
		 * The meta object literal for the '<em><b>Procedure Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE__PROCEDURE_PARAMETERS = eINSTANCE.getProcedure_ProcedureParameters();

		/**
		 * The meta object literal for the '<em><b>Return Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE__RETURN_TYPE = eINSTANCE.getProcedure_ReturnType();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.ActionBlockImpl <em>Action Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.ActionBlockImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getActionBlock()
		 * @generated
		 */
		EClass ACTION_BLOCK = eINSTANCE.getActionBlock();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION_BLOCK__ACTIONS = eINSTANCE.getActionBlock_Actions();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.DataStructTypeImpl <em>Data Struct Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.DataStructTypeImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getDataStructType()
		 * @generated
		 */
		EClass DATA_STRUCT_TYPE = eINSTANCE.getDataStructType();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_STRUCT_TYPE__PROPERTIES = eINSTANCE.getDataStructType_Properties();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.NamedElementImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.FunctionBlockDataPortImpl <em>Function Block Data Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.FunctionBlockDataPortImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFunctionBlockDataPort()
		 * @generated
		 */
		EClass FUNCTION_BLOCK_DATA_PORT = eINSTANCE.getFunctionBlockDataPort();

		/**
		 * The meta object literal for the '<em><b>Default Input Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE = eINSTANCE
				.getFunctionBlockDataPort_DefaultInputValue();

		/**
		 * The meta object literal for the '<em><b>Function Var Store</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_BLOCK_DATA_PORT__FUNCTION_VAR_STORE = eINSTANCE.getFunctionBlockDataPort_FunctionVarStore();

		/**
		 * The meta object literal for the '<em><b>Current Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_BLOCK_DATA_PORT__CURRENT_VALUE = eINSTANCE.getFunctionBlockDataPort_CurrentValue();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.DataTypeImpl <em>Data Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.DataTypeImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getDataType()
		 * @generated
		 */
		EClass DATA_TYPE = eINSTANCE.getDataType();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.ProcedureParameterImpl <em>Procedure Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.ProcedureParameterImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getProcedureParameter()
		 * @generated
		 */
		EClass PROCEDURE_PARAMETER = eINSTANCE.getProcedureParameter();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE_PARAMETER__TYPE = eINSTANCE.getProcedureParameter_Type();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.FunctionPortImpl <em>Function Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.FunctionPortImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFunctionPort()
		 * @generated
		 */
		EClass FUNCTION_PORT = eINSTANCE.getFunctionPort();

		/**
		 * The meta object literal for the '<em><b>Data Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_PORT__DATA_TYPE = eINSTANCE.getFunctionPort_DataType();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION_PORT__DIRECTION = eINSTANCE.getFunctionPort_Direction();

		/**
		 * The meta object literal for the '<em><b>Outgoing Connectors</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FUNCTION_PORT___OUTGOING_CONNECTORS = eINSTANCE.getFunctionPort__OutgoingConnectors();

		/**
		 * The meta object literal for the '<em><b>Incoming Connectors</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FUNCTION_PORT___INCOMING_CONNECTORS = eINSTANCE.getFunctionPort__IncomingConnectors();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.FunctionConnectorImpl <em>Function Connector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.FunctionConnectorImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFunctionConnector()
		 * @generated
		 */
		EClass FUNCTION_CONNECTOR = eINSTANCE.getFunctionConnector();

		/**
		 * The meta object literal for the '<em><b>Emitter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_CONNECTOR__EMITTER = eINSTANCE.getFunctionConnector_Emitter();

		/**
		 * The meta object literal for the '<em><b>Receiver</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_CONNECTOR__RECEIVER = eINSTANCE.getFunctionConnector_Receiver();

		/**
		 * The meta object literal for the '<em><b>Delayed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION_CONNECTOR__DELAYED = eINSTANCE.getFunctionConnector_Delayed();

		/**
		 * The meta object literal for the '<em><b>Dse send Data</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FUNCTION_CONNECTOR___DSE_SEND_DATA = eINSTANCE.getFunctionConnector__Dse_sendData();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.ExternalEventImpl <em>External Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.ExternalEventImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getExternalEvent()
		 * @generated
		 */
		EClass EXTERNAL_EVENT = eINSTANCE.getExternalEvent();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.BinaryExpressionImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getBinaryExpression()
		 * @generated
		 */
		EClass BINARY_EXPRESSION = eINSTANCE.getBinaryExpression();

		/**
		 * The meta object literal for the '<em><b>Lhs Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXPRESSION__LHS_OPERAND = eINSTANCE.getBinaryExpression_LhsOperand();

		/**
		 * The meta object literal for the '<em><b>Rhs Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXPRESSION__RHS_OPERAND = eINSTANCE.getBinaryExpression_RhsOperand();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.UnaryExpressionImpl <em>Unary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.UnaryExpressionImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getUnaryExpression()
		 * @generated
		 */
		EClass UNARY_EXPRESSION = eINSTANCE.getUnaryExpression();

		/**
		 * The meta object literal for the '<em><b>Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_EXPRESSION__OPERAND = eINSTANCE.getUnaryExpression_Operand();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.CastImpl <em>Cast</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.CastImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getCast()
		 * @generated
		 */
		EClass CAST = eINSTANCE.getCast();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.CallImpl <em>Call</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.CallImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getCall()
		 * @generated
		 */
		EClass CALL = eINSTANCE.getCall();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.PrimitiveActionImpl <em>Primitive Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.PrimitiveActionImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getPrimitiveAction()
		 * @generated
		 */
		EClass PRIMITIVE_ACTION = eINSTANCE.getPrimitiveAction();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.AssignmentImpl <em>Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.AssignmentImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getAssignment()
		 * @generated
		 */
		EClass ASSIGNMENT = eINSTANCE.getAssignment();

		/**
		 * The meta object literal for the '<em><b>Assignable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSIGNMENT__ASSIGNABLE = eINSTANCE.getAssignment_Assignable();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSIGNMENT__EXPRESSION = eINSTANCE.getAssignment_Expression();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.ControlStructureActionImpl <em>Control Structure Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.ControlStructureActionImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getControlStructureAction()
		 * @generated
		 */
		EClass CONTROL_STRUCTURE_ACTION = eINSTANCE.getControlStructureAction();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.IfActionImpl <em>If Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.IfActionImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getIfAction()
		 * @generated
		 */
		EClass IF_ACTION = eINSTANCE.getIfAction();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_ACTION__CONDITION = eINSTANCE.getIfAction_Condition();

		/**
		 * The meta object literal for the '<em><b>Then Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_ACTION__THEN_ACTION = eINSTANCE.getIfAction_ThenAction();

		/**
		 * The meta object literal for the '<em><b>Else Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_ACTION__ELSE_ACTION = eINSTANCE.getIfAction_ElseAction();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.WhileActionImpl <em>While Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.WhileActionImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getWhileAction()
		 * @generated
		 */
		EClass WHILE_ACTION = eINSTANCE.getWhileAction();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHILE_ACTION__CONDITION = eINSTANCE.getWhileAction_Condition();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHILE_ACTION__ACTION = eINSTANCE.getWhileAction_Action();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.ProcedureCallImpl <em>Procedure Call</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.ProcedureCallImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getProcedureCall()
		 * @generated
		 */
		EClass PROCEDURE_CALL = eINSTANCE.getProcedureCall();

		/**
		 * The meta object literal for the '<em><b>Procedure</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE_CALL__PROCEDURE = eINSTANCE.getProcedureCall_Procedure();

		/**
		 * The meta object literal for the '<em><b>Procedure Call Arguments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS = eINSTANCE.getProcedureCall_ProcedureCallArguments();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.VarDeclImpl <em>Var Decl</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.VarDeclImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getVarDecl()
		 * @generated
		 */
		EClass VAR_DECL = eINSTANCE.getVarDecl();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VAR_DECL__TYPE = eINSTANCE.getVarDecl_Type();

		/**
		 * The meta object literal for the '<em><b>Initial Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VAR_DECL__INITIAL_VALUE = eINSTANCE.getVarDecl_InitialValue();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.CallDeclarationReferenceImpl <em>Call Declaration Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.CallDeclarationReferenceImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getCallDeclarationReference()
		 * @generated
		 */
		EClass CALL_DECLARATION_REFERENCE = eINSTANCE.getCallDeclarationReference();

		/**
		 * The meta object literal for the '<em><b>Callable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_DECLARATION_REFERENCE__CALLABLE = eINSTANCE.getCallDeclarationReference_Callable();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.FunctionPortReferenceImpl <em>Function Port Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.FunctionPortReferenceImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFunctionPortReference()
		 * @generated
		 */
		EClass FUNCTION_PORT_REFERENCE = eINSTANCE.getFunctionPortReference();

		/**
		 * The meta object literal for the '<em><b>Function Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_PORT_REFERENCE__FUNCTION_PORT = eINSTANCE.getFunctionPortReference_FunctionPort();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.BasicOperatorUnaryExpressionImpl <em>Basic Operator Unary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.BasicOperatorUnaryExpressionImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getBasicOperatorUnaryExpression()
		 * @generated
		 */
		EClass BASIC_OPERATOR_UNARY_EXPRESSION = eINSTANCE.getBasicOperatorUnaryExpression();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASIC_OPERATOR_UNARY_EXPRESSION__OPERATOR = eINSTANCE.getBasicOperatorUnaryExpression_Operator();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.BasicOperatorBinaryExpressionImpl <em>Basic Operator Binary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.BasicOperatorBinaryExpressionImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getBasicOperatorBinaryExpression()
		 * @generated
		 */
		EClass BASIC_OPERATOR_BINARY_EXPRESSION = eINSTANCE.getBasicOperatorBinaryExpression();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASIC_OPERATOR_BINARY_EXPRESSION__OPERATOR = eINSTANCE.getBasicOperatorBinaryExpression_Operator();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.ValueTypeImpl <em>Value Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.ValueTypeImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getValueType()
		 * @generated
		 */
		EClass VALUE_TYPE = eINSTANCE.getValueType();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUE_TYPE__UNIT = eINSTANCE.getValueType_Unit();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUE_TYPE__DESCRIPTION = eINSTANCE.getValueType_Description();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.IntegerValueTypeImpl <em>Integer Value Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.IntegerValueTypeImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getIntegerValueType()
		 * @generated
		 */
		EClass INTEGER_VALUE_TYPE = eINSTANCE.getIntegerValueType();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_VALUE_TYPE__MIN = eINSTANCE.getIntegerValueType_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_VALUE_TYPE__MAX = eINSTANCE.getIntegerValueType_Max();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.FloatValueTypeImpl <em>Float Value Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.FloatValueTypeImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFloatValueType()
		 * @generated
		 */
		EClass FLOAT_VALUE_TYPE = eINSTANCE.getFloatValueType();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOAT_VALUE_TYPE__MIN = eINSTANCE.getFloatValueType_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOAT_VALUE_TYPE__MAX = eINSTANCE.getFloatValueType_Max();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.BooleanValueTypeImpl <em>Boolean Value Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.BooleanValueTypeImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getBooleanValueType()
		 * @generated
		 */
		EClass BOOLEAN_VALUE_TYPE = eINSTANCE.getBooleanValueType();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.StringValueTypeImpl <em>String Value Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.StringValueTypeImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getStringValueType()
		 * @generated
		 */
		EClass STRING_VALUE_TYPE = eINSTANCE.getStringValueType();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.EnumerationValueTypeImpl <em>Enumeration Value Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.EnumerationValueTypeImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getEnumerationValueType()
		 * @generated
		 */
		EClass ENUMERATION_VALUE_TYPE = eINSTANCE.getEnumerationValueType();

		/**
		 * The meta object literal for the '<em><b>Enumeration Literal</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUMERATION_VALUE_TYPE__ENUMERATION_LITERAL = eINSTANCE.getEnumerationValueType_EnumerationLiteral();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.EnumerationLiteralImpl <em>Enumeration Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.EnumerationLiteralImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getEnumerationLiteral()
		 * @generated
		 */
		EClass ENUMERATION_LITERAL = eINSTANCE.getEnumerationLiteral();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.AssignableImpl <em>Assignable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.AssignableImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getAssignable()
		 * @generated
		 */
		EClass ASSIGNABLE = eINSTANCE.getAssignable();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.CallableDeclarationImpl <em>Callable Declaration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.CallableDeclarationImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getCallableDeclaration()
		 * @generated
		 */
		EClass CALLABLE_DECLARATION = eINSTANCE.getCallableDeclaration();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.FunctionImpl <em>Function</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.FunctionImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFunction()
		 * @generated
		 */
		EClass FUNCTION = eINSTANCE.getFunction();

		/**
		 * The meta object literal for the '<em><b>Function Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION__FUNCTION_PORTS = eINSTANCE.getFunction_FunctionPorts();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION__ACTION = eINSTANCE.getFunction_Action();

		/**
		 * The meta object literal for the '<em><b>Sub Functions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION__SUB_FUNCTIONS = eINSTANCE.getFunction_SubFunctions();

		/**
		 * The meta object literal for the '<em><b>Function Connectors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION__FUNCTION_CONNECTORS = eINSTANCE.getFunction_FunctionConnectors();

		/**
		 * The meta object literal for the '<em><b>Function Var Decls</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION__FUNCTION_VAR_DECLS = eINSTANCE.getFunction_FunctionVarDecls();

		/**
		 * The meta object literal for the '<em><b>Time Reference</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION__TIME_REFERENCE = eINSTANCE.getFunction_TimeReference();

		/**
		 * The meta object literal for the '<em><b>All Sub Functions</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FUNCTION___ALL_SUB_FUNCTIONS = eINSTANCE.getFunction__AllSubFunctions();

		/**
		 * The meta object literal for the '<em><b>Dse start Eval Function</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FUNCTION___DSE_START_EVAL_FUNCTION = eINSTANCE.getFunction__Dse_startEvalFunction();

		/**
		 * The meta object literal for the '<em><b>Dse stop Eval Function</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FUNCTION___DSE_STOP_EVAL_FUNCTION = eINSTANCE.getFunction__Dse_stopEvalFunction();

		/**
		 * The meta object literal for the '<em><b>Is Enabled</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FUNCTION___IS_ENABLED = eINSTANCE.getFunction__IsEnabled();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.FunctionTimeReferenceImpl <em>Function Time Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.FunctionTimeReferenceImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFunctionTimeReference()
		 * @generated
		 */
		EClass FUNCTION_TIME_REFERENCE = eINSTANCE.getFunctionTimeReference();

		/**
		 * The meta object literal for the '<em><b>Observable Var</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_TIME_REFERENCE__OBSERVABLE_VAR = eINSTANCE.getFunctionTimeReference_ObservableVar();

		/**
		 * The meta object literal for the '<em><b>Increment</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_TIME_REFERENCE__INCREMENT = eINSTANCE.getFunctionTimeReference_Increment();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.ExternalProcedureImpl <em>External Procedure</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.ExternalProcedureImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getExternalProcedure()
		 * @generated
		 */
		EClass EXTERNAL_PROCEDURE = eINSTANCE.getExternalProcedure();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.FCLProcedureImpl <em>FCL Procedure</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.FCLProcedureImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFCLProcedure()
		 * @generated
		 */
		EClass FCL_PROCEDURE = eINSTANCE.getFCLProcedure();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FCL_PROCEDURE__ACTION = eINSTANCE.getFCLProcedure_Action();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.FCLProcedureReturnImpl <em>FCL Procedure Return</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.FCLProcedureReturnImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFCLProcedureReturn()
		 * @generated
		 */
		EClass FCL_PROCEDURE_RETURN = eINSTANCE.getFCLProcedureReturn();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FCL_PROCEDURE_RETURN__EXPRESSION = eINSTANCE.getFCLProcedureReturn_Expression();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.JavaProcedureImpl <em>Java Procedure</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.JavaProcedureImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getJavaProcedure()
		 * @generated
		 */
		EClass JAVA_PROCEDURE = eINSTANCE.getJavaProcedure();

		/**
		 * The meta object literal for the '<em><b>Method Full Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JAVA_PROCEDURE__METHOD_FULL_QUALIFIED_NAME = eINSTANCE.getJavaProcedure_MethodFullQualifiedName();

		/**
		 * The meta object literal for the '<em><b>Static</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JAVA_PROCEDURE__STATIC = eINSTANCE.getJavaProcedure_Static();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.IntegerValueImpl <em>Integer Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.IntegerValueImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getIntegerValue()
		 * @generated
		 */
		EClass INTEGER_VALUE = eINSTANCE.getIntegerValue();

		/**
		 * The meta object literal for the '<em><b>Int Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_VALUE__INT_VALUE = eINSTANCE.getIntegerValue_IntValue();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.FloatValueImpl <em>Float Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.FloatValueImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFloatValue()
		 * @generated
		 */
		EClass FLOAT_VALUE = eINSTANCE.getFloatValue();

		/**
		 * The meta object literal for the '<em><b>Float Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOAT_VALUE__FLOAT_VALUE = eINSTANCE.getFloatValue_FloatValue();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.StringValueImpl <em>String Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.StringValueImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getStringValue()
		 * @generated
		 */
		EClass STRING_VALUE = eINSTANCE.getStringValue();

		/**
		 * The meta object literal for the '<em><b>String Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_VALUE__STRING_VALUE = eINSTANCE.getStringValue_StringValue();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.BooleanValueImpl <em>Boolean Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.BooleanValueImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getBooleanValue()
		 * @generated
		 */
		EClass BOOLEAN_VALUE = eINSTANCE.getBooleanValue();

		/**
		 * The meta object literal for the '<em><b>Boolean Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOLEAN_VALUE__BOOLEAN_VALUE = eINSTANCE.getBooleanValue_BooleanValue();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.FunctionVarDeclImpl <em>Function Var Decl</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.FunctionVarDeclImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFunctionVarDecl()
		 * @generated
		 */
		EClass FUNCTION_VAR_DECL = eINSTANCE.getFunctionVarDecl();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_VAR_DECL__TYPE = eINSTANCE.getFunctionVarDecl_Type();

		/**
		 * The meta object literal for the '<em><b>Initial Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_VAR_DECL__INITIAL_VALUE = eINSTANCE.getFunctionVarDecl_InitialValue();

		/**
		 * The meta object literal for the '<em><b>Current Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTION_VAR_DECL__CURRENT_VALUE = eINSTANCE.getFunctionVarDecl_CurrentValue();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.FMUFunctionImpl <em>FMU Function</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.FMUFunctionImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFMUFunction()
		 * @generated
		 */
		EClass FMU_FUNCTION = eINSTANCE.getFMUFunction();

		/**
		 * The meta object literal for the '<em><b>Runnable Fmu Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FMU_FUNCTION__RUNNABLE_FMU_PATH = eINSTANCE.getFMUFunction_RunnableFmuPath();

		/**
		 * The meta object literal for the '<em><b>Fmu Simulation Wrapper</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FMU_FUNCTION__FMU_SIMULATION_WRAPPER = eINSTANCE.getFMUFunction_FmuSimulationWrapper();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.ValueImpl <em>Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.ValueImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getValue()
		 * @generated
		 */
		EClass VALUE = eINSTANCE.getValue();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.CallConstantImpl <em>Call Constant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.CallConstantImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getCallConstant()
		 * @generated
		 */
		EClass CALL_CONSTANT = eINSTANCE.getCallConstant();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_CONSTANT__VALUE = eINSTANCE.getCallConstant_Value();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.EnumerationValueImpl <em>Enumeration Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.EnumerationValueImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getEnumerationValue()
		 * @generated
		 */
		EClass ENUMERATION_VALUE = eINSTANCE.getEnumerationValue();

		/**
		 * The meta object literal for the '<em><b>Enumeration Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUMERATION_VALUE__ENUMERATION_VALUE = eINSTANCE.getEnumerationValue_EnumerationValue();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.FMUFunctionBlockDataPortImpl <em>FMU Function Block Data Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.FMUFunctionBlockDataPortImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getFMUFunctionBlockDataPort()
		 * @generated
		 */
		EClass FMU_FUNCTION_BLOCK_DATA_PORT = eINSTANCE.getFMUFunctionBlockDataPort();

		/**
		 * The meta object literal for the '<em><b>Runnable Fmu Variable Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FMU_FUNCTION_BLOCK_DATA_PORT__RUNNABLE_FMU_VARIABLE_NAME = eINSTANCE
				.getFMUFunctionBlockDataPort_RunnableFmuVariableName();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.UnityFunctionImpl <em>Unity Function</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.UnityFunctionImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getUnityFunction()
		 * @generated
		 */
		EClass UNITY_FUNCTION = eINSTANCE.getUnityFunction();

		/**
		 * The meta object literal for the '<em><b>Unity Web Page Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNITY_FUNCTION__UNITY_WEB_PAGE_PATH = eINSTANCE.getUnityFunction_UnityWebPagePath();

		/**
		 * The meta object literal for the '<em><b>Unity Wrapper</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNITY_FUNCTION__UNITY_WRAPPER = eINSTANCE.getUnityFunction_UnityWrapper();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.UnityFunctionDataPortImpl <em>Unity Function Data Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.UnityFunctionDataPortImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getUnityFunctionDataPort()
		 * @generated
		 */
		EClass UNITY_FUNCTION_DATA_PORT = eINSTANCE.getUnityFunctionDataPort();

		/**
		 * The meta object literal for the '<em><b>Unity Node Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNITY_FUNCTION_DATA_PORT__UNITY_NODE_NAME = eINSTANCE.getUnityFunctionDataPort_UnityNodeName();

		/**
		 * The meta object literal for the '<em><b>Unity Variable Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNITY_FUNCTION_DATA_PORT__UNITY_VARIABLE_NAME = eINSTANCE
				.getUnityFunctionDataPort_UnityVariableName();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.CallPreviousImpl <em>Call Previous</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.CallPreviousImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getCallPrevious()
		 * @generated
		 */
		EClass CALL_PREVIOUS = eINSTANCE.getCallPrevious();

		/**
		 * The meta object literal for the '<em><b>Callable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_PREVIOUS__CALLABLE = eINSTANCE.getCallPrevious_Callable();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.DataStructPropertyImpl <em>Data Struct Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.DataStructPropertyImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getDataStructProperty()
		 * @generated
		 */
		EClass DATA_STRUCT_PROPERTY = eINSTANCE.getDataStructProperty();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_STRUCT_PROPERTY__TYPE = eINSTANCE.getDataStructProperty_Type();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.DataStructPropertyReferenceImpl <em>Data Struct Property Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.DataStructPropertyReferenceImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getDataStructPropertyReference()
		 * @generated
		 */
		EClass DATA_STRUCT_PROPERTY_REFERENCE = eINSTANCE.getDataStructPropertyReference();

		/**
		 * The meta object literal for the '<em><b>Property Name</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_STRUCT_PROPERTY_REFERENCE__PROPERTY_NAME = eINSTANCE
				.getDataStructPropertyReference_PropertyName();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.DataStructPropertyAssignmentImpl <em>Data Struct Property Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.DataStructPropertyAssignmentImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getDataStructPropertyAssignment()
		 * @generated
		 */
		EClass DATA_STRUCT_PROPERTY_ASSIGNMENT = eINSTANCE.getDataStructPropertyAssignment();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_STRUCT_PROPERTY_ASSIGNMENT__EXPRESSION = eINSTANCE.getDataStructPropertyAssignment_Expression();

		/**
		 * The meta object literal for the '<em><b>Assignable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_STRUCT_PROPERTY_ASSIGNMENT__ASSIGNABLE = eINSTANCE.getDataStructPropertyAssignment_Assignable();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.CallDataStructPropertyImpl <em>Call Data Struct Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.CallDataStructPropertyImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getCallDataStructProperty()
		 * @generated
		 */
		EClass CALL_DATA_STRUCT_PROPERTY = eINSTANCE.getCallDataStructProperty();

		/**
		 * The meta object literal for the '<em><b>Root Target Data Struct</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_DATA_STRUCT_PROPERTY__ROOT_TARGET_DATA_STRUCT = eINSTANCE
				.getCallDataStructProperty_RootTargetDataStruct();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.NewDataStructImpl <em>New Data Struct</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.NewDataStructImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getNewDataStruct()
		 * @generated
		 */
		EClass NEW_DATA_STRUCT = eINSTANCE.getNewDataStruct();

		/**
		 * The meta object literal for the '<em><b>Property Assignments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEW_DATA_STRUCT__PROPERTY_ASSIGNMENTS = eINSTANCE.getNewDataStruct_PropertyAssignments();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.StructValueImpl <em>Struct Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.StructValueImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getStructValue()
		 * @generated
		 */
		EClass STRUCT_VALUE = eINSTANCE.getStructValue();

		/**
		 * The meta object literal for the '<em><b>Struct Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRUCT_VALUE__STRUCT_PROPERTIES = eINSTANCE.getStructValue_StructProperties();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.StructPropertyValueImpl <em>Struct Property Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.StructPropertyValueImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getStructPropertyValue()
		 * @generated
		 */
		EClass STRUCT_PROPERTY_VALUE = eINSTANCE.getStructPropertyValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRUCT_PROPERTY_VALUE__VALUE = eINSTANCE.getStructPropertyValue_Value();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.impl.LogImpl <em>Log</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.impl.LogImpl
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getLog()
		 * @generated
		 */
		EClass LOG = eINSTANCE.getLog();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOG__EXPRESSION = eINSTANCE.getLog_Expression();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.DirectionKind <em>Direction Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.DirectionKind
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getDirectionKind()
		 * @generated
		 */
		EEnum DIRECTION_KIND = eINSTANCE.getDirectionKind();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.BinaryOperator <em>Binary Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.BinaryOperator
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getBinaryOperator()
		 * @generated
		 */
		EEnum BINARY_OPERATOR = eINSTANCE.getBinaryOperator();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.UnaryOperator <em>Unary Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.UnaryOperator
		 * @see fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl#getUnaryOperator()
		 * @generated
		 */
		EEnum UNARY_OPERATOR = eINSTANCE.getUnaryOperator();

	}

} //FclPackage
