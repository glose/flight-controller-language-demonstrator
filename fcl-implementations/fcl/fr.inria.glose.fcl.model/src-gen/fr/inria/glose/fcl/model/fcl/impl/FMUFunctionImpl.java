/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.FMUFunction;
import fr.inria.glose.fcl.model.fcl.FclPackage;

import fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FMU Function</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FMUFunctionImpl#getRunnableFmuPath <em>Runnable Fmu Path</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FMUFunctionImpl#getFmuSimulationWrapper <em>Fmu Simulation Wrapper</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FMUFunctionImpl extends FunctionImpl implements FMUFunction {
	/**
	 * The default value of the '{@link #getRunnableFmuPath() <em>Runnable Fmu Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRunnableFmuPath()
	 * @generated
	 * @ordered
	 */
	protected static final String RUNNABLE_FMU_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRunnableFmuPath() <em>Runnable Fmu Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRunnableFmuPath()
	 * @generated
	 * @ordered
	 */
	protected String runnableFmuPath = RUNNABLE_FMU_PATH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFmuSimulationWrapper() <em>Fmu Simulation Wrapper</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFmuSimulationWrapper()
	 * @generated
	 * @ordered
	 */
	protected FMUSimulationWrapper fmuSimulationWrapper;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FMUFunctionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.FMU_FUNCTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRunnableFmuPath() {
		return runnableFmuPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRunnableFmuPath(String newRunnableFmuPath) {
		String oldRunnableFmuPath = runnableFmuPath;
		runnableFmuPath = newRunnableFmuPath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.FMU_FUNCTION__RUNNABLE_FMU_PATH,
					oldRunnableFmuPath, runnableFmuPath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FMUSimulationWrapper getFmuSimulationWrapper() {
		if (fmuSimulationWrapper != null && fmuSimulationWrapper.eIsProxy()) {
			InternalEObject oldFmuSimulationWrapper = (InternalEObject) fmuSimulationWrapper;
			fmuSimulationWrapper = (FMUSimulationWrapper) eResolveProxy(oldFmuSimulationWrapper);
			if (fmuSimulationWrapper != oldFmuSimulationWrapper) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							FclPackage.FMU_FUNCTION__FMU_SIMULATION_WRAPPER, oldFmuSimulationWrapper,
							fmuSimulationWrapper));
			}
		}
		return fmuSimulationWrapper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FMUSimulationWrapper basicGetFmuSimulationWrapper() {
		return fmuSimulationWrapper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFmuSimulationWrapper(FMUSimulationWrapper newFmuSimulationWrapper) {
		FMUSimulationWrapper oldFmuSimulationWrapper = fmuSimulationWrapper;
		fmuSimulationWrapper = newFmuSimulationWrapper;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.FMU_FUNCTION__FMU_SIMULATION_WRAPPER,
					oldFmuSimulationWrapper, fmuSimulationWrapper));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.FMU_FUNCTION__RUNNABLE_FMU_PATH:
			return getRunnableFmuPath();
		case FclPackage.FMU_FUNCTION__FMU_SIMULATION_WRAPPER:
			if (resolve)
				return getFmuSimulationWrapper();
			return basicGetFmuSimulationWrapper();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.FMU_FUNCTION__RUNNABLE_FMU_PATH:
			setRunnableFmuPath((String) newValue);
			return;
		case FclPackage.FMU_FUNCTION__FMU_SIMULATION_WRAPPER:
			setFmuSimulationWrapper((FMUSimulationWrapper) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.FMU_FUNCTION__RUNNABLE_FMU_PATH:
			setRunnableFmuPath(RUNNABLE_FMU_PATH_EDEFAULT);
			return;
		case FclPackage.FMU_FUNCTION__FMU_SIMULATION_WRAPPER:
			setFmuSimulationWrapper((FMUSimulationWrapper) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.FMU_FUNCTION__RUNNABLE_FMU_PATH:
			return RUNNABLE_FMU_PATH_EDEFAULT == null ? runnableFmuPath != null
					: !RUNNABLE_FMU_PATH_EDEFAULT.equals(runnableFmuPath);
		case FclPackage.FMU_FUNCTION__FMU_SIMULATION_WRAPPER:
			return fmuSimulationWrapper != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (runnableFmuPath: ");
		result.append(runnableFmuPath);
		result.append(')');
		return result.toString();
	}

} //FMUFunctionImpl
