/**
 */
package fr.inria.glose.fcl.model.fcl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Connector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FunctionConnector#getEmitter <em>Emitter</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FunctionConnector#getReceiver <em>Receiver</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FunctionConnector#isDelayed <em>Delayed</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunctionConnector()
 * @model
 * @generated
 */
public interface FunctionConnector extends EObject {
	/**
	 * Returns the value of the '<em><b>Emitter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Emitter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Emitter</em>' reference.
	 * @see #setEmitter(FunctionPort)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunctionConnector_Emitter()
	 * @model required="true"
	 * @generated
	 */
	FunctionPort getEmitter();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.FunctionConnector#getEmitter <em>Emitter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Emitter</em>' reference.
	 * @see #getEmitter()
	 * @generated
	 */
	void setEmitter(FunctionPort value);

	/**
	 * Returns the value of the '<em><b>Receiver</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Receiver</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Receiver</em>' reference.
	 * @see #setReceiver(FunctionPort)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunctionConnector_Receiver()
	 * @model required="true"
	 * @generated
	 */
	FunctionPort getReceiver();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.FunctionConnector#getReceiver <em>Receiver</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Receiver</em>' reference.
	 * @see #getReceiver()
	 * @generated
	 */
	void setReceiver(FunctionPort value);

	/**
	 * Returns the value of the '<em><b>Delayed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delayed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delayed</em>' attribute.
	 * @see #setDelayed(boolean)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunctionConnector_Delayed()
	 * @model
	 * @generated
	 */
	boolean isDelayed();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.FunctionConnector#isDelayed <em>Delayed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delayed</em>' attribute.
	 * @see #isDelayed()
	 * @generated
	 */
	void setDelayed(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void dse_sendData();

} // FunctionConnector
