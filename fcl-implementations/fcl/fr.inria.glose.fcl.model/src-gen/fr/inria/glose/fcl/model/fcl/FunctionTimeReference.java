/**
 */
package fr.inria.glose.fcl.model.fcl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Time Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FunctionTimeReference#getObservableVar <em>Observable Var</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FunctionTimeReference#getIncrement <em>Increment</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunctionTimeReference()
 * @model
 * @generated
 */
public interface FunctionTimeReference extends EObject {
	/**
	 * Returns the value of the '<em><b>Observable Var</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Observable Var</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Observable Var</em>' reference.
	 * @see #setObservableVar(FunctionVarDecl)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunctionTimeReference_ObservableVar()
	 * @model required="true"
	 * @generated
	 */
	FunctionVarDecl getObservableVar();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.FunctionTimeReference#getObservableVar <em>Observable Var</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Observable Var</em>' reference.
	 * @see #getObservableVar()
	 * @generated
	 */
	void setObservableVar(FunctionVarDecl value);

	/**
	 * Returns the value of the '<em><b>Increment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Increment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Increment</em>' containment reference.
	 * @see #setIncrement(Expression)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunctionTimeReference_Increment()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getIncrement();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.FunctionTimeReference#getIncrement <em>Increment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Increment</em>' containment reference.
	 * @see #getIncrement()
	 * @generated
	 */
	void setIncrement(Expression value);

} // FunctionTimeReference
