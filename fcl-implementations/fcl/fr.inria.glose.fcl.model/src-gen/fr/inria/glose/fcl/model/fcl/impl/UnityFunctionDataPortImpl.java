/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unity Function Data Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.UnityFunctionDataPortImpl#getUnityNodeName <em>Unity Node Name</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.UnityFunctionDataPortImpl#getUnityVariableName <em>Unity Variable Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UnityFunctionDataPortImpl extends FunctionBlockDataPortImpl implements UnityFunctionDataPort {
	/**
	 * The default value of the '{@link #getUnityNodeName() <em>Unity Node Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnityNodeName()
	 * @generated
	 * @ordered
	 */
	protected static final String UNITY_NODE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnityNodeName() <em>Unity Node Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnityNodeName()
	 * @generated
	 * @ordered
	 */
	protected String unityNodeName = UNITY_NODE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnityVariableName() <em>Unity Variable Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnityVariableName()
	 * @generated
	 * @ordered
	 */
	protected static final String UNITY_VARIABLE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnityVariableName() <em>Unity Variable Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnityVariableName()
	 * @generated
	 * @ordered
	 */
	protected String unityVariableName = UNITY_VARIABLE_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnityFunctionDataPortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.UNITY_FUNCTION_DATA_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnityNodeName() {
		return unityNodeName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnityNodeName(String newUnityNodeName) {
		String oldUnityNodeName = unityNodeName;
		unityNodeName = newUnityNodeName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.UNITY_FUNCTION_DATA_PORT__UNITY_NODE_NAME,
					oldUnityNodeName, unityNodeName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnityVariableName() {
		return unityVariableName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnityVariableName(String newUnityVariableName) {
		String oldUnityVariableName = unityVariableName;
		unityVariableName = newUnityVariableName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FclPackage.UNITY_FUNCTION_DATA_PORT__UNITY_VARIABLE_NAME, oldUnityVariableName, unityVariableName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.UNITY_FUNCTION_DATA_PORT__UNITY_NODE_NAME:
			return getUnityNodeName();
		case FclPackage.UNITY_FUNCTION_DATA_PORT__UNITY_VARIABLE_NAME:
			return getUnityVariableName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.UNITY_FUNCTION_DATA_PORT__UNITY_NODE_NAME:
			setUnityNodeName((String) newValue);
			return;
		case FclPackage.UNITY_FUNCTION_DATA_PORT__UNITY_VARIABLE_NAME:
			setUnityVariableName((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.UNITY_FUNCTION_DATA_PORT__UNITY_NODE_NAME:
			setUnityNodeName(UNITY_NODE_NAME_EDEFAULT);
			return;
		case FclPackage.UNITY_FUNCTION_DATA_PORT__UNITY_VARIABLE_NAME:
			setUnityVariableName(UNITY_VARIABLE_NAME_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.UNITY_FUNCTION_DATA_PORT__UNITY_NODE_NAME:
			return UNITY_NODE_NAME_EDEFAULT == null ? unityNodeName != null
					: !UNITY_NODE_NAME_EDEFAULT.equals(unityNodeName);
		case FclPackage.UNITY_FUNCTION_DATA_PORT__UNITY_VARIABLE_NAME:
			return UNITY_VARIABLE_NAME_EDEFAULT == null ? unityVariableName != null
					: !UNITY_VARIABLE_NAME_EDEFAULT.equals(unityVariableName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (unityNodeName: ");
		result.append(unityNodeName);
		result.append(", unityVariableName: ");
		result.append(unityVariableName);
		result.append(')');
		return result.toString();
	}

} //UnityFunctionDataPortImpl
