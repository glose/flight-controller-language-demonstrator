/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.Assignable;
import fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment;
import fr.inria.glose.fcl.model.fcl.Expression;
import fr.inria.glose.fcl.model.fcl.FclPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Struct Property Assignment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.DataStructPropertyAssignmentImpl#getExpression <em>Expression</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.DataStructPropertyAssignmentImpl#getAssignable <em>Assignable</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataStructPropertyAssignmentImpl extends DataStructPropertyReferenceImpl
		implements DataStructPropertyAssignment {
	/**
	 * The cached value of the '{@link #getExpression() <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpression()
	 * @generated
	 * @ordered
	 */
	protected Expression expression;

	/**
	 * The cached value of the '{@link #getAssignable() <em>Assignable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssignable()
	 * @generated
	 * @ordered
	 */
	protected Assignable assignable;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataStructPropertyAssignmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.DATA_STRUCT_PROPERTY_ASSIGNMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getExpression() {
		return expression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpression(Expression newExpression, NotificationChain msgs) {
		Expression oldExpression = expression;
		expression = newExpression;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT__EXPRESSION, oldExpression, newExpression);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpression(Expression newExpression) {
		if (newExpression != expression) {
			NotificationChain msgs = null;
			if (expression != null)
				msgs = ((InternalEObject) expression).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT__EXPRESSION, null, msgs);
			if (newExpression != null)
				msgs = ((InternalEObject) newExpression).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT__EXPRESSION, null, msgs);
			msgs = basicSetExpression(newExpression, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT__EXPRESSION, newExpression, newExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assignable getAssignable() {
		if (assignable != null && assignable.eIsProxy()) {
			InternalEObject oldAssignable = (InternalEObject) assignable;
			assignable = (Assignable) eResolveProxy(oldAssignable);
			if (assignable != oldAssignable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT__ASSIGNABLE, oldAssignable, assignable));
			}
		}
		return assignable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assignable basicGetAssignable() {
		return assignable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssignable(Assignable newAssignable) {
		Assignable oldAssignable = assignable;
		assignable = newAssignable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT__ASSIGNABLE, oldAssignable, assignable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT__EXPRESSION:
			return basicSetExpression(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT__EXPRESSION:
			return getExpression();
		case FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT__ASSIGNABLE:
			if (resolve)
				return getAssignable();
			return basicGetAssignable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT__EXPRESSION:
			setExpression((Expression) newValue);
			return;
		case FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT__ASSIGNABLE:
			setAssignable((Assignable) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT__EXPRESSION:
			setExpression((Expression) null);
			return;
		case FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT__ASSIGNABLE:
			setAssignable((Assignable) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT__EXPRESSION:
			return expression != null;
		case FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT__ASSIGNABLE:
			return assignable != null;
		}
		return super.eIsSet(featureID);
	}

} //DataStructPropertyAssignmentImpl
