/**
 */
package fr.inria.glose.fcl.model.fcl.interpreter_vm;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage
 * @generated
 */
public interface Interpreter_vmFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Interpreter_vmFactory eINSTANCE = fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Procedure Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Procedure Context</em>'.
	 * @generated
	 */
	ProcedureContext createProcedureContext();

	/**
	 * Returns a new object of class '<em>Declaration Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Declaration Map Entry</em>'.
	 * @generated
	 */
	DeclarationMapEntry createDeclarationMapEntry();

	/**
	 * Returns a new object of class '<em>Unity Wrapper</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unity Wrapper</em>'.
	 * @generated
	 */
	UnityWrapper createUnityWrapper();

	/**
	 * Returns a new object of class '<em>FMU Simulation Wrapper</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FMU Simulation Wrapper</em>'.
	 * @generated
	 */
	FMUSimulationWrapper createFMUSimulationWrapper();

	/**
	 * Returns a new object of class '<em>Event Occurrence</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Occurrence</em>'.
	 * @generated
	 */
	EventOccurrence createEventOccurrence();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Interpreter_vmPackage getInterpreter_vmPackage();

} //Interpreter_vmFactory
