/**
 */
package fr.inria.glose.fcl.model.fcl.interpreter_vm;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmFactory
 * @model kind="package"
 * @generated
 */
public interface Interpreter_vmPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "interpreter_vm";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.inria.fr/glose/fcl/interpreter_vm";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "interpreter_vm";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Interpreter_vmPackage eINSTANCE = fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.ProcedureContextImpl <em>Procedure Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.ProcedureContextImpl
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmPackageImpl#getProcedureContext()
	 * @generated
	 */
	int PROCEDURE_CONTEXT = 0;

	/**
	 * The feature id for the '<em><b>Declaration Map Entries</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_CONTEXT__DECLARATION_MAP_ENTRIES = 0;

	/**
	 * The feature id for the '<em><b>Result Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_CONTEXT__RESULT_VALUE = 1;

	/**
	 * The number of structural features of the '<em>Procedure Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_CONTEXT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Procedure Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_CONTEXT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.DeclarationMapEntryImpl <em>Declaration Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.DeclarationMapEntryImpl
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmPackageImpl#getDeclarationMapEntry()
	 * @generated
	 */
	int DECLARATION_MAP_ENTRY = 1;

	/**
	 * The feature id for the '<em><b>Callable Declaration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION_MAP_ENTRY__CALLABLE_DECLARATION = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Declaration Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Declaration Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.UnityWrapperImpl <em>Unity Wrapper</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.UnityWrapperImpl
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmPackageImpl#getUnityWrapper()
	 * @generated
	 */
	int UNITY_WRAPPER = 2;

	/**
	 * The feature id for the '<em><b>Unity Instance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_WRAPPER__UNITY_INSTANCE = 0;

	/**
	 * The number of structural features of the '<em>Unity Wrapper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_WRAPPER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Unity Wrapper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNITY_WRAPPER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.FMUSimulationWrapperImpl <em>FMU Simulation Wrapper</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.FMUSimulationWrapperImpl
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmPackageImpl#getFMUSimulationWrapper()
	 * @generated
	 */
	int FMU_SIMULATION_WRAPPER = 3;

	/**
	 * The feature id for the '<em><b>Fmi Simulation2</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_SIMULATION_WRAPPER__FMI_SIMULATION2 = 0;

	/**
	 * The number of structural features of the '<em>FMU Simulation Wrapper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_SIMULATION_WRAPPER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>FMU Simulation Wrapper</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMU_SIMULATION_WRAPPER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.EventOccurrenceImpl <em>Event Occurrence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.EventOccurrenceImpl
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmPackageImpl#getEventOccurrence()
	 * @generated
	 */
	int EVENT_OCCURRENCE = 4;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OCCURRENCE__EVENT = 0;

	/**
	 * The number of structural features of the '<em>Event Occurrence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OCCURRENCE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Event Occurrence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OCCURRENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '<em>Unity Instance</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.fcl.model.unity.UnityInstance
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmPackageImpl#getUnityInstance()
	 * @generated
	 */
	int UNITY_INSTANCE = 5;

	/**
	 * The meta object id for the '<em>FMI Simulation2</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.javafmi.wrapper.generic.Simulation2
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmPackageImpl#getFMISimulation2()
	 * @generated
	 */
	int FMI_SIMULATION2 = 6;

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext <em>Procedure Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Procedure Context</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext
	 * @generated
	 */
	EClass getProcedureContext();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext#getDeclarationMapEntries <em>Declaration Map Entries</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Declaration Map Entries</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext#getDeclarationMapEntries()
	 * @see #getProcedureContext()
	 * @generated
	 */
	EReference getProcedureContext_DeclarationMapEntries();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext#getResultValue <em>Result Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Result Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext#getResultValue()
	 * @see #getProcedureContext()
	 * @generated
	 */
	EReference getProcedureContext_ResultValue();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry <em>Declaration Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Declaration Map Entry</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry
	 * @generated
	 */
	EClass getDeclarationMapEntry();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry#getCallableDeclaration <em>Callable Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Callable Declaration</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry#getCallableDeclaration()
	 * @see #getDeclarationMapEntry()
	 * @generated
	 */
	EReference getDeclarationMapEntry_CallableDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry#getValue()
	 * @see #getDeclarationMapEntry()
	 * @generated
	 */
	EReference getDeclarationMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper <em>Unity Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unity Wrapper</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper
	 * @generated
	 */
	EClass getUnityWrapper();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper#getUnityInstance <em>Unity Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unity Instance</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper#getUnityInstance()
	 * @see #getUnityWrapper()
	 * @generated
	 */
	EAttribute getUnityWrapper_UnityInstance();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper <em>FMU Simulation Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FMU Simulation Wrapper</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper
	 * @generated
	 */
	EClass getFMUSimulationWrapper();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper#getFmiSimulation2 <em>Fmi Simulation2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fmi Simulation2</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper#getFmiSimulation2()
	 * @see #getFMUSimulationWrapper()
	 * @generated
	 */
	EAttribute getFMUSimulationWrapper_FmiSimulation2();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.EventOccurrence <em>Event Occurrence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Occurrence</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.EventOccurrence
	 * @generated
	 */
	EClass getEventOccurrence();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.EventOccurrence#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.EventOccurrence#getEvent()
	 * @see #getEventOccurrence()
	 * @generated
	 */
	EReference getEventOccurrence_Event();

	/**
	 * Returns the meta object for data type '{@link fr.inria.glose.fcl.model.unity.UnityInstance <em>Unity Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Unity Instance</em>'.
	 * @see fr.inria.glose.fcl.model.unity.UnityInstance
	 * @model instanceClass="fr.inria.glose.fcl.model.unity.UnityInstance"
	 * @generated
	 */
	EDataType getUnityInstance();

	/**
	 * Returns the meta object for data type '{@link org.javafmi.wrapper.generic.Simulation2 <em>FMI Simulation2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>FMI Simulation2</em>'.
	 * @see org.javafmi.wrapper.generic.Simulation2
	 * @model instanceClass="org.javafmi.wrapper.generic.Simulation2"
	 * @generated
	 */
	EDataType getFMISimulation2();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Interpreter_vmFactory getInterpreter_vmFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.ProcedureContextImpl <em>Procedure Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.ProcedureContextImpl
		 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmPackageImpl#getProcedureContext()
		 * @generated
		 */
		EClass PROCEDURE_CONTEXT = eINSTANCE.getProcedureContext();

		/**
		 * The meta object literal for the '<em><b>Declaration Map Entries</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE_CONTEXT__DECLARATION_MAP_ENTRIES = eINSTANCE.getProcedureContext_DeclarationMapEntries();

		/**
		 * The meta object literal for the '<em><b>Result Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCEDURE_CONTEXT__RESULT_VALUE = eINSTANCE.getProcedureContext_ResultValue();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.DeclarationMapEntryImpl <em>Declaration Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.DeclarationMapEntryImpl
		 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmPackageImpl#getDeclarationMapEntry()
		 * @generated
		 */
		EClass DECLARATION_MAP_ENTRY = eINSTANCE.getDeclarationMapEntry();

		/**
		 * The meta object literal for the '<em><b>Callable Declaration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECLARATION_MAP_ENTRY__CALLABLE_DECLARATION = eINSTANCE.getDeclarationMapEntry_CallableDeclaration();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECLARATION_MAP_ENTRY__VALUE = eINSTANCE.getDeclarationMapEntry_Value();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.UnityWrapperImpl <em>Unity Wrapper</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.UnityWrapperImpl
		 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmPackageImpl#getUnityWrapper()
		 * @generated
		 */
		EClass UNITY_WRAPPER = eINSTANCE.getUnityWrapper();

		/**
		 * The meta object literal for the '<em><b>Unity Instance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNITY_WRAPPER__UNITY_INSTANCE = eINSTANCE.getUnityWrapper_UnityInstance();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.FMUSimulationWrapperImpl <em>FMU Simulation Wrapper</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.FMUSimulationWrapperImpl
		 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmPackageImpl#getFMUSimulationWrapper()
		 * @generated
		 */
		EClass FMU_SIMULATION_WRAPPER = eINSTANCE.getFMUSimulationWrapper();

		/**
		 * The meta object literal for the '<em><b>Fmi Simulation2</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FMU_SIMULATION_WRAPPER__FMI_SIMULATION2 = eINSTANCE.getFMUSimulationWrapper_FmiSimulation2();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.EventOccurrenceImpl <em>Event Occurrence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.EventOccurrenceImpl
		 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmPackageImpl#getEventOccurrence()
		 * @generated
		 */
		EClass EVENT_OCCURRENCE = eINSTANCE.getEventOccurrence();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_OCCURRENCE__EVENT = eINSTANCE.getEventOccurrence_Event();

		/**
		 * The meta object literal for the '<em>Unity Instance</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.fcl.model.unity.UnityInstance
		 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmPackageImpl#getUnityInstance()
		 * @generated
		 */
		EDataType UNITY_INSTANCE = eINSTANCE.getUnityInstance();

		/**
		 * The meta object literal for the '<em>FMI Simulation2</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.javafmi.wrapper.generic.Simulation2
		 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmPackageImpl#getFMISimulation2()
		 * @generated
		 */
		EDataType FMI_SIMULATION2 = eINSTANCE.getFMISimulation2();

	}

} //Interpreter_vmPackage
