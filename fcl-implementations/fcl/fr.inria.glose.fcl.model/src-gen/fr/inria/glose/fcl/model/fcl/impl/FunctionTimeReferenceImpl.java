/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.Expression;
import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.FunctionTimeReference;
import fr.inria.glose.fcl.model.fcl.FunctionVarDecl;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Time Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FunctionTimeReferenceImpl#getObservableVar <em>Observable Var</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FunctionTimeReferenceImpl#getIncrement <em>Increment</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionTimeReferenceImpl extends MinimalEObjectImpl.Container implements FunctionTimeReference {
	/**
	 * The cached value of the '{@link #getObservableVar() <em>Observable Var</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObservableVar()
	 * @generated
	 * @ordered
	 */
	protected FunctionVarDecl observableVar;

	/**
	 * The cached value of the '{@link #getIncrement() <em>Increment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncrement()
	 * @generated
	 * @ordered
	 */
	protected Expression increment;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionTimeReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.FUNCTION_TIME_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionVarDecl getObservableVar() {
		if (observableVar != null && observableVar.eIsProxy()) {
			InternalEObject oldObservableVar = (InternalEObject) observableVar;
			observableVar = (FunctionVarDecl) eResolveProxy(oldObservableVar);
			if (observableVar != oldObservableVar) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							FclPackage.FUNCTION_TIME_REFERENCE__OBSERVABLE_VAR, oldObservableVar, observableVar));
			}
		}
		return observableVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionVarDecl basicGetObservableVar() {
		return observableVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObservableVar(FunctionVarDecl newObservableVar) {
		FunctionVarDecl oldObservableVar = observableVar;
		observableVar = newObservableVar;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.FUNCTION_TIME_REFERENCE__OBSERVABLE_VAR,
					oldObservableVar, observableVar));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getIncrement() {
		return increment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIncrement(Expression newIncrement, NotificationChain msgs) {
		Expression oldIncrement = increment;
		increment = newIncrement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					FclPackage.FUNCTION_TIME_REFERENCE__INCREMENT, oldIncrement, newIncrement);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIncrement(Expression newIncrement) {
		if (newIncrement != increment) {
			NotificationChain msgs = null;
			if (increment != null)
				msgs = ((InternalEObject) increment).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.FUNCTION_TIME_REFERENCE__INCREMENT, null, msgs);
			if (newIncrement != null)
				msgs = ((InternalEObject) newIncrement).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.FUNCTION_TIME_REFERENCE__INCREMENT, null, msgs);
			msgs = basicSetIncrement(newIncrement, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.FUNCTION_TIME_REFERENCE__INCREMENT,
					newIncrement, newIncrement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FclPackage.FUNCTION_TIME_REFERENCE__INCREMENT:
			return basicSetIncrement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.FUNCTION_TIME_REFERENCE__OBSERVABLE_VAR:
			if (resolve)
				return getObservableVar();
			return basicGetObservableVar();
		case FclPackage.FUNCTION_TIME_REFERENCE__INCREMENT:
			return getIncrement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.FUNCTION_TIME_REFERENCE__OBSERVABLE_VAR:
			setObservableVar((FunctionVarDecl) newValue);
			return;
		case FclPackage.FUNCTION_TIME_REFERENCE__INCREMENT:
			setIncrement((Expression) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.FUNCTION_TIME_REFERENCE__OBSERVABLE_VAR:
			setObservableVar((FunctionVarDecl) null);
			return;
		case FclPackage.FUNCTION_TIME_REFERENCE__INCREMENT:
			setIncrement((Expression) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.FUNCTION_TIME_REFERENCE__OBSERVABLE_VAR:
			return observableVar != null;
		case FclPackage.FUNCTION_TIME_REFERENCE__INCREMENT:
			return increment != null;
		}
		return super.eIsSet(featureID);
	}

} //FunctionTimeReferenceImpl
