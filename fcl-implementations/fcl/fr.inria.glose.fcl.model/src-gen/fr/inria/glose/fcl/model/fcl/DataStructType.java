/**
 */
package fr.inria.glose.fcl.model.fcl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Struct Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.DataStructType#getProperties <em>Properties</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getDataStructType()
 * @model
 * @generated
 */
public interface DataStructType extends DataType {
	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.DataStructProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getDataStructType_Properties()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataStructProperty> getProperties();

} // DataStructType
