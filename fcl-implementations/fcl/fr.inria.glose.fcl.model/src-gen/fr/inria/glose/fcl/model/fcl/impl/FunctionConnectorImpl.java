/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.FunctionConnector;
import fr.inria.glose.fcl.model.fcl.FunctionPort;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Connector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FunctionConnectorImpl#getEmitter <em>Emitter</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FunctionConnectorImpl#getReceiver <em>Receiver</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FunctionConnectorImpl#isDelayed <em>Delayed</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionConnectorImpl extends MinimalEObjectImpl.Container implements FunctionConnector {
	/**
	 * The cached value of the '{@link #getEmitter() <em>Emitter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEmitter()
	 * @generated
	 * @ordered
	 */
	protected FunctionPort emitter;

	/**
	 * The cached value of the '{@link #getReceiver() <em>Receiver</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReceiver()
	 * @generated
	 * @ordered
	 */
	protected FunctionPort receiver;

	/**
	 * The default value of the '{@link #isDelayed() <em>Delayed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDelayed()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DELAYED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDelayed() <em>Delayed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDelayed()
	 * @generated
	 * @ordered
	 */
	protected boolean delayed = DELAYED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionConnectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.FUNCTION_CONNECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionPort getEmitter() {
		if (emitter != null && emitter.eIsProxy()) {
			InternalEObject oldEmitter = (InternalEObject) emitter;
			emitter = (FunctionPort) eResolveProxy(oldEmitter);
			if (emitter != oldEmitter) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FclPackage.FUNCTION_CONNECTOR__EMITTER,
							oldEmitter, emitter));
			}
		}
		return emitter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionPort basicGetEmitter() {
		return emitter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEmitter(FunctionPort newEmitter) {
		FunctionPort oldEmitter = emitter;
		emitter = newEmitter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.FUNCTION_CONNECTOR__EMITTER, oldEmitter,
					emitter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionPort getReceiver() {
		if (receiver != null && receiver.eIsProxy()) {
			InternalEObject oldReceiver = (InternalEObject) receiver;
			receiver = (FunctionPort) eResolveProxy(oldReceiver);
			if (receiver != oldReceiver) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FclPackage.FUNCTION_CONNECTOR__RECEIVER,
							oldReceiver, receiver));
			}
		}
		return receiver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionPort basicGetReceiver() {
		return receiver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReceiver(FunctionPort newReceiver) {
		FunctionPort oldReceiver = receiver;
		receiver = newReceiver;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.FUNCTION_CONNECTOR__RECEIVER, oldReceiver,
					receiver));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDelayed() {
		return delayed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDelayed(boolean newDelayed) {
		boolean oldDelayed = delayed;
		delayed = newDelayed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.FUNCTION_CONNECTOR__DELAYED, oldDelayed,
					delayed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void dse_sendData() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.FUNCTION_CONNECTOR__EMITTER:
			if (resolve)
				return getEmitter();
			return basicGetEmitter();
		case FclPackage.FUNCTION_CONNECTOR__RECEIVER:
			if (resolve)
				return getReceiver();
			return basicGetReceiver();
		case FclPackage.FUNCTION_CONNECTOR__DELAYED:
			return isDelayed();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.FUNCTION_CONNECTOR__EMITTER:
			setEmitter((FunctionPort) newValue);
			return;
		case FclPackage.FUNCTION_CONNECTOR__RECEIVER:
			setReceiver((FunctionPort) newValue);
			return;
		case FclPackage.FUNCTION_CONNECTOR__DELAYED:
			setDelayed((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.FUNCTION_CONNECTOR__EMITTER:
			setEmitter((FunctionPort) null);
			return;
		case FclPackage.FUNCTION_CONNECTOR__RECEIVER:
			setReceiver((FunctionPort) null);
			return;
		case FclPackage.FUNCTION_CONNECTOR__DELAYED:
			setDelayed(DELAYED_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.FUNCTION_CONNECTOR__EMITTER:
			return emitter != null;
		case FclPackage.FUNCTION_CONNECTOR__RECEIVER:
			return receiver != null;
		case FclPackage.FUNCTION_CONNECTOR__DELAYED:
			return delayed != DELAYED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case FclPackage.FUNCTION_CONNECTOR___DSE_SEND_DATA:
			dse_sendData();
			return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (delayed: ");
		result.append(delayed);
		result.append(')');
		return result.toString();
	}

} //FunctionConnectorImpl
