/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enumeration Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.EnumerationValue#getEnumerationValue <em>Enumeration Value</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getEnumerationValue()
 * @model
 * @generated
 */
public interface EnumerationValue extends Value {
	/**
	 * Returns the value of the '<em><b>Enumeration Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Value</em>' reference.
	 * @see #setEnumerationValue(EnumerationLiteral)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getEnumerationValue_EnumerationValue()
	 * @model
	 * @generated
	 */
	EnumerationLiteral getEnumerationValue();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.EnumerationValue#getEnumerationValue <em>Enumeration Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enumeration Value</em>' reference.
	 * @see #getEnumerationValue()
	 * @generated
	 */
	void setEnumerationValue(EnumerationLiteral value);

} // EnumerationValue
