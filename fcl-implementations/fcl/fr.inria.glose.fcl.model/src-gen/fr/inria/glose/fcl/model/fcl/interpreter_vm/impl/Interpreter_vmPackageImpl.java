/**
 */
package fr.inria.glose.fcl.model.fcl.interpreter_vm.impl;

import fr.inria.glose.fcl.model.fcl.FclPackage;

import fr.inria.glose.fcl.model.fcl.impl.FclPackageImpl;

import fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.EventOccurrence;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmFactory;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext;

import fr.inria.glose.fcl.model.fcl.interpreter_vm.UnityWrapper;
import fr.inria.glose.fcl.model.unity.UnityInstance;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.javafmi.wrapper.generic.Simulation2;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Interpreter_vmPackageImpl extends EPackageImpl implements Interpreter_vmPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass procedureContextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass declarationMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unityWrapperEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fmuSimulationWrapperEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventOccurrenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType unityInstanceEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType fmiSimulation2EDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Interpreter_vmPackageImpl() {
		super(eNS_URI, Interpreter_vmFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link Interpreter_vmPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Interpreter_vmPackage init() {
		if (isInited)
			return (Interpreter_vmPackage) EPackage.Registry.INSTANCE.getEPackage(Interpreter_vmPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredInterpreter_vmPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		Interpreter_vmPackageImpl theInterpreter_vmPackage = registeredInterpreter_vmPackage instanceof Interpreter_vmPackageImpl
				? (Interpreter_vmPackageImpl) registeredInterpreter_vmPackage
				: new Interpreter_vmPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FclPackage.eNS_URI);
		FclPackageImpl theFclPackage = (FclPackageImpl) (registeredPackage instanceof FclPackageImpl ? registeredPackage
				: FclPackage.eINSTANCE);

		// Create package meta-data objects
		theInterpreter_vmPackage.createPackageContents();
		theFclPackage.createPackageContents();

		// Initialize created meta-data
		theInterpreter_vmPackage.initializePackageContents();
		theFclPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theInterpreter_vmPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Interpreter_vmPackage.eNS_URI, theInterpreter_vmPackage);
		return theInterpreter_vmPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcedureContext() {
		return procedureContextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedureContext_DeclarationMapEntries() {
		return (EReference) procedureContextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedureContext_ResultValue() {
		return (EReference) procedureContextEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeclarationMapEntry() {
		return declarationMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeclarationMapEntry_CallableDeclaration() {
		return (EReference) declarationMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeclarationMapEntry_Value() {
		return (EReference) declarationMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnityWrapper() {
		return unityWrapperEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnityWrapper_UnityInstance() {
		return (EAttribute) unityWrapperEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFMUSimulationWrapper() {
		return fmuSimulationWrapperEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFMUSimulationWrapper_FmiSimulation2() {
		return (EAttribute) fmuSimulationWrapperEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventOccurrence() {
		return eventOccurrenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventOccurrence_Event() {
		return (EReference) eventOccurrenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getUnityInstance() {
		return unityInstanceEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getFMISimulation2() {
		return fmiSimulation2EDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interpreter_vmFactory getInterpreter_vmFactory() {
		return (Interpreter_vmFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		procedureContextEClass = createEClass(PROCEDURE_CONTEXT);
		createEReference(procedureContextEClass, PROCEDURE_CONTEXT__DECLARATION_MAP_ENTRIES);
		createEReference(procedureContextEClass, PROCEDURE_CONTEXT__RESULT_VALUE);

		declarationMapEntryEClass = createEClass(DECLARATION_MAP_ENTRY);
		createEReference(declarationMapEntryEClass, DECLARATION_MAP_ENTRY__CALLABLE_DECLARATION);
		createEReference(declarationMapEntryEClass, DECLARATION_MAP_ENTRY__VALUE);

		unityWrapperEClass = createEClass(UNITY_WRAPPER);
		createEAttribute(unityWrapperEClass, UNITY_WRAPPER__UNITY_INSTANCE);

		fmuSimulationWrapperEClass = createEClass(FMU_SIMULATION_WRAPPER);
		createEAttribute(fmuSimulationWrapperEClass, FMU_SIMULATION_WRAPPER__FMI_SIMULATION2);

		eventOccurrenceEClass = createEClass(EVENT_OCCURRENCE);
		createEReference(eventOccurrenceEClass, EVENT_OCCURRENCE__EVENT);

		// Create data types
		unityInstanceEDataType = createEDataType(UNITY_INSTANCE);
		fmiSimulation2EDataType = createEDataType(FMI_SIMULATION2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		FclPackage theFclPackage = (FclPackage) EPackage.Registry.INSTANCE.getEPackage(FclPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(procedureContextEClass, ProcedureContext.class, "ProcedureContext", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProcedureContext_DeclarationMapEntries(), this.getDeclarationMapEntry(), null,
				"declarationMapEntries", null, 0, -1, ProcedureContext.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcedureContext_ResultValue(), theFclPackage.getValue(), null, "resultValue", null, 0, 1,
				ProcedureContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(declarationMapEntryEClass, DeclarationMapEntry.class, "DeclarationMapEntry", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDeclarationMapEntry_CallableDeclaration(), theFclPackage.getCallableDeclaration(), null,
				"callableDeclaration", null, 0, 1, DeclarationMapEntry.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeclarationMapEntry_Value(), theFclPackage.getValue(), null, "value", null, 0, 1,
				DeclarationMapEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unityWrapperEClass, UnityWrapper.class, "UnityWrapper", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUnityWrapper_UnityInstance(), this.getUnityInstance(), "unityInstance", null, 0, 1,
				UnityWrapper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(fmuSimulationWrapperEClass, FMUSimulationWrapper.class, "FMUSimulationWrapper", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFMUSimulationWrapper_FmiSimulation2(), this.getFMISimulation2(), "fmiSimulation2", null, 0, 1,
				FMUSimulationWrapper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventOccurrenceEClass, EventOccurrence.class, "EventOccurrence", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEventOccurrence_Event(), theFclPackage.getEvent(), null, "event", null, 0, 1,
				EventOccurrence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize data types
		initEDataType(unityInstanceEDataType, UnityInstance.class, "UnityInstance", IS_SERIALIZABLE,
				!IS_GENERATED_INSTANCE_CLASS);
		initEDataType(fmiSimulation2EDataType, Simulation2.class, "FMISimulation2", IS_SERIALIZABLE,
				!IS_GENERATED_INSTANCE_CLASS);

		// Create annotations
		// aspect
		createAspectAnnotations();
	}

	/**
	 * Initializes the annotations for <b>aspect</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createAspectAnnotations() {
		String source = "aspect";
		addAnnotation(procedureContextEClass, source, new String[] {});
		addAnnotation(eventOccurrenceEClass, source, new String[] {});
	}

} //Interpreter_vmPackageImpl
