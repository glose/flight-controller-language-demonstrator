/**
 */
package fr.inria.glose.fcl.model.fcl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Callable Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getCallableDeclaration()
 * @model abstract="true"
 * @generated
 */
public interface CallableDeclaration extends EObject {
} // CallableDeclaration
