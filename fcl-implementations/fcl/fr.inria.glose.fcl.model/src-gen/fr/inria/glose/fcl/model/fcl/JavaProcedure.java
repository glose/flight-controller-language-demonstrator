/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Procedure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.JavaProcedure#getMethodFullQualifiedName <em>Method Full Qualified Name</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.JavaProcedure#isStatic <em>Static</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getJavaProcedure()
 * @model
 * @generated
 */
public interface JavaProcedure extends ExternalProcedure {
	/**
	 * Returns the value of the '<em><b>Method Full Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Method Full Qualified Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method Full Qualified Name</em>' attribute.
	 * @see #setMethodFullQualifiedName(String)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getJavaProcedure_MethodFullQualifiedName()
	 * @model
	 * @generated
	 */
	String getMethodFullQualifiedName();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.JavaProcedure#getMethodFullQualifiedName <em>Method Full Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method Full Qualified Name</em>' attribute.
	 * @see #getMethodFullQualifiedName()
	 * @generated
	 */
	void setMethodFullQualifiedName(String value);

	/**
	 * Returns the value of the '<em><b>Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Static</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Static</em>' attribute.
	 * @see #setStatic(boolean)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getJavaProcedure_Static()
	 * @model
	 * @generated
	 */
	boolean isStatic();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.JavaProcedure#isStatic <em>Static</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Static</em>' attribute.
	 * @see #isStatic()
	 * @generated
	 */
	void setStatic(boolean value);

} // JavaProcedure
