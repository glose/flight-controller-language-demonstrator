/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.BinaryExpression#getLhsOperand <em>Lhs Operand</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.BinaryExpression#getRhsOperand <em>Rhs Operand</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getBinaryExpression()
 * @model abstract="true"
 * @generated
 */
public interface BinaryExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Lhs Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lhs Operand</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lhs Operand</em>' containment reference.
	 * @see #setLhsOperand(Expression)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getBinaryExpression_LhsOperand()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getLhsOperand();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.BinaryExpression#getLhsOperand <em>Lhs Operand</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lhs Operand</em>' containment reference.
	 * @see #getLhsOperand()
	 * @generated
	 */
	void setLhsOperand(Expression value);

	/**
	 * Returns the value of the '<em><b>Rhs Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rhs Operand</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rhs Operand</em>' containment reference.
	 * @see #setRhsOperand(Expression)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getBinaryExpression_RhsOperand()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getRhsOperand();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.BinaryExpression#getRhsOperand <em>Rhs Operand</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rhs Operand</em>' containment reference.
	 * @see #getRhsOperand()
	 * @generated
	 */
	void setRhsOperand(Expression value);

} // BinaryExpression
