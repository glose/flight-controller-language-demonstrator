/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.Action;
import fr.inria.glose.fcl.model.fcl.ActionBlock;
import fr.inria.glose.fcl.model.fcl.Assignable;
import fr.inria.glose.fcl.model.fcl.Assignment;
import fr.inria.glose.fcl.model.fcl.BasicOperatorBinaryExpression;
import fr.inria.glose.fcl.model.fcl.BasicOperatorUnaryExpression;
import fr.inria.glose.fcl.model.fcl.BinaryExpression;
import fr.inria.glose.fcl.model.fcl.BinaryOperator;
import fr.inria.glose.fcl.model.fcl.BooleanValue;
import fr.inria.glose.fcl.model.fcl.BooleanValueType;
import fr.inria.glose.fcl.model.fcl.Call;
import fr.inria.glose.fcl.model.fcl.CallConstant;
import fr.inria.glose.fcl.model.fcl.CallDataStructProperty;
import fr.inria.glose.fcl.model.fcl.CallDeclarationReference;
import fr.inria.glose.fcl.model.fcl.CallPrevious;
import fr.inria.glose.fcl.model.fcl.CallableDeclaration;
import fr.inria.glose.fcl.model.fcl.Cast;
import fr.inria.glose.fcl.model.fcl.ControlStructureAction;
import fr.inria.glose.fcl.model.fcl.DataStructProperty;
import fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment;
import fr.inria.glose.fcl.model.fcl.DataStructPropertyReference;
import fr.inria.glose.fcl.model.fcl.DataStructType;
import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.DirectionKind;
import fr.inria.glose.fcl.model.fcl.EnumerationLiteral;
import fr.inria.glose.fcl.model.fcl.EnumerationValue;
import fr.inria.glose.fcl.model.fcl.EnumerationValueType;
import fr.inria.glose.fcl.model.fcl.Event;
import fr.inria.glose.fcl.model.fcl.Expression;
import fr.inria.glose.fcl.model.fcl.ExternalEvent;
import fr.inria.glose.fcl.model.fcl.ExternalProcedure;
import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FCLProcedure;
import fr.inria.glose.fcl.model.fcl.FCLProcedureReturn;
import fr.inria.glose.fcl.model.fcl.FMUFunction;
import fr.inria.glose.fcl.model.fcl.FMUFunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.FloatValue;
import fr.inria.glose.fcl.model.fcl.FloatValueType;
import fr.inria.glose.fcl.model.fcl.Function;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.FunctionConnector;
import fr.inria.glose.fcl.model.fcl.FunctionPort;
import fr.inria.glose.fcl.model.fcl.FunctionPortReference;
import fr.inria.glose.fcl.model.fcl.FunctionTimeReference;
import fr.inria.glose.fcl.model.fcl.FunctionVarDecl;
import fr.inria.glose.fcl.model.fcl.IfAction;
import fr.inria.glose.fcl.model.fcl.IntegerValue;
import fr.inria.glose.fcl.model.fcl.IntegerValueType;
import fr.inria.glose.fcl.model.fcl.JavaProcedure;
import fr.inria.glose.fcl.model.fcl.Log;
import fr.inria.glose.fcl.model.fcl.Mode;
import fr.inria.glose.fcl.model.fcl.ModeAutomata;
import fr.inria.glose.fcl.model.fcl.NamedElement;
import fr.inria.glose.fcl.model.fcl.NewDataStruct;
import fr.inria.glose.fcl.model.fcl.PrimitiveAction;
import fr.inria.glose.fcl.model.fcl.Procedure;
import fr.inria.glose.fcl.model.fcl.ProcedureCall;
import fr.inria.glose.fcl.model.fcl.ProcedureParameter;
import fr.inria.glose.fcl.model.fcl.StringValue;
import fr.inria.glose.fcl.model.fcl.StringValueType;
import fr.inria.glose.fcl.model.fcl.StructPropertyValue;
import fr.inria.glose.fcl.model.fcl.StructValue;
import fr.inria.glose.fcl.model.fcl.Transition;
import fr.inria.glose.fcl.model.fcl.UnaryExpression;
import fr.inria.glose.fcl.model.fcl.UnaryOperator;
import fr.inria.glose.fcl.model.fcl.UnityFunction;
import fr.inria.glose.fcl.model.fcl.UnityFunctionDataPort;
import fr.inria.glose.fcl.model.fcl.Value;
import fr.inria.glose.fcl.model.fcl.ValueType;
import fr.inria.glose.fcl.model.fcl.VarDecl;
import fr.inria.glose.fcl.model.fcl.WhileAction;

import fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.impl.Interpreter_vmPackageImpl;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FclPackageImpl extends EPackageImpl implements FclPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fclModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modeAutomataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass procedureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actionBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataStructTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionBlockDataPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass procedureParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionConnectorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass externalEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass binaryExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unaryExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass castEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass callEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass primitiveActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assignmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass controlStructureActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ifActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass whileActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass procedureCallEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass varDeclEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass callDeclarationReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionPortReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass basicOperatorUnaryExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass basicOperatorBinaryExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass integerValueTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass floatValueTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanValueTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringValueTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enumerationValueTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enumerationLiteralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assignableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass callableDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionTimeReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass externalProcedureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fclProcedureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fclProcedureReturnEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass javaProcedureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass integerValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass floatValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionVarDeclEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fmuFunctionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass callConstantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enumerationValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fmuFunctionBlockDataPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unityFunctionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unityFunctionDataPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass callPreviousEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataStructPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataStructPropertyReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataStructPropertyAssignmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass callDataStructPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass newDataStructEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass structValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass structPropertyValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass logEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum directionKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum binaryOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum unaryOperatorEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FclPackageImpl() {
		super(eNS_URI, FclFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link FclPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FclPackage init() {
		if (isInited)
			return (FclPackage) EPackage.Registry.INSTANCE.getEPackage(FclPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredFclPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		FclPackageImpl theFclPackage = registeredFclPackage instanceof FclPackageImpl
				? (FclPackageImpl) registeredFclPackage
				: new FclPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(Interpreter_vmPackage.eNS_URI);
		Interpreter_vmPackageImpl theInterpreter_vmPackage = (Interpreter_vmPackageImpl) (registeredPackage instanceof Interpreter_vmPackageImpl
				? registeredPackage
				: Interpreter_vmPackage.eINSTANCE);

		// Create package meta-data objects
		theFclPackage.createPackageContents();
		theInterpreter_vmPackage.createPackageContents();

		// Initialize created meta-data
		theFclPackage.initializePackageContents();
		theInterpreter_vmPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFclPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FclPackage.eNS_URI, theFclPackage);
		return theFclPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFCLModel() {
		return fclModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFCLModel_ModeAutomata() {
		return (EReference) fclModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFCLModel_Events() {
		return (EReference) fclModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFCLModel_Procedures() {
		return (EReference) fclModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFCLModel_DataTypes() {
		return (EReference) fclModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFCLModel_Dataflow() {
		return (EReference) fclModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFCLModel_ReceivedEvents() {
		return (EReference) fclModelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFCLModel_ProcedureStack() {
		return (EReference) fclModelEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFCLModel_Indentation() {
		return (EAttribute) fclModelEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFCLModel__AllFunctions() {
		return fclModelEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFCLModel__AllConnectors() {
		return fclModelEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFCLModel__PropagateDelayedResults() {
		return fclModelEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFCLModel__EvaluateModeAutomata() {
		return fclModelEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModeAutomata() {
		return modeAutomataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModeAutomata_Modes() {
		return (EReference) modeAutomataEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModeAutomata_InitialMode() {
		return (EReference) modeAutomataEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModeAutomata_Transitions() {
		return (EReference) modeAutomataEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModeAutomata_CurrentMode() {
		return (EReference) modeAutomataEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransition() {
		return transitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_Action() {
		return (EReference) transitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_Guard() {
		return (EReference) transitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_Event() {
		return (EReference) transitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_Target() {
		return (EReference) transitionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_Source() {
		return (EReference) transitionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMode() {
		return modeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMode_Entry() {
		return (EReference) modeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMode_Exit() {
		return (EReference) modeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMode_Final() {
		return (EAttribute) modeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMode_EnabledFunctions() {
		return (EReference) modeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMode_IncomingTransition() {
		return (EReference) modeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMode_OutgoingTransition() {
		return (EReference) modeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAction() {
		return actionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEvent() {
		return eventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExpression() {
		return expressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExpression_ResultType() {
		return (EReference) expressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcedure() {
		return procedureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedure_ProcedureParameters() {
		return (EReference) procedureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedure_ReturnType() {
		return (EReference) procedureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActionBlock() {
		return actionBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActionBlock_Actions() {
		return (EReference) actionBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataStructType() {
		return dataStructTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataStructType_Properties() {
		return (EReference) dataStructTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute) namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunctionBlockDataPort() {
		return functionBlockDataPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionBlockDataPort_DefaultInputValue() {
		return (EReference) functionBlockDataPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionBlockDataPort_FunctionVarStore() {
		return (EReference) functionBlockDataPortEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionBlockDataPort_CurrentValue() {
		return (EReference) functionBlockDataPortEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataType() {
		return dataTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcedureParameter() {
		return procedureParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedureParameter_Type() {
		return (EReference) procedureParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunctionPort() {
		return functionPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionPort_DataType() {
		return (EReference) functionPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionPort_Direction() {
		return (EAttribute) functionPortEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFunctionPort__OutgoingConnectors() {
		return functionPortEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFunctionPort__IncomingConnectors() {
		return functionPortEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunctionConnector() {
		return functionConnectorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionConnector_Emitter() {
		return (EReference) functionConnectorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionConnector_Receiver() {
		return (EReference) functionConnectorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunctionConnector_Delayed() {
		return (EAttribute) functionConnectorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFunctionConnector__Dse_sendData() {
		return functionConnectorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExternalEvent() {
		return externalEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBinaryExpression() {
		return binaryExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBinaryExpression_LhsOperand() {
		return (EReference) binaryExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBinaryExpression_RhsOperand() {
		return (EReference) binaryExpressionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnaryExpression() {
		return unaryExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUnaryExpression_Operand() {
		return (EReference) unaryExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCast() {
		return castEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCall() {
		return callEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPrimitiveAction() {
		return primitiveActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssignment() {
		return assignmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssignment_Assignable() {
		return (EReference) assignmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssignment_Expression() {
		return (EReference) assignmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getControlStructureAction() {
		return controlStructureActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIfAction() {
		return ifActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIfAction_Condition() {
		return (EReference) ifActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIfAction_ThenAction() {
		return (EReference) ifActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIfAction_ElseAction() {
		return (EReference) ifActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWhileAction() {
		return whileActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhileAction_Condition() {
		return (EReference) whileActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhileAction_Action() {
		return (EReference) whileActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcedureCall() {
		return procedureCallEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedureCall_Procedure() {
		return (EReference) procedureCallEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcedureCall_ProcedureCallArguments() {
		return (EReference) procedureCallEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVarDecl() {
		return varDeclEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVarDecl_Type() {
		return (EReference) varDeclEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVarDecl_InitialValue() {
		return (EReference) varDeclEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCallDeclarationReference() {
		return callDeclarationReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallDeclarationReference_Callable() {
		return (EReference) callDeclarationReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunctionPortReference() {
		return functionPortReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionPortReference_FunctionPort() {
		return (EReference) functionPortReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBasicOperatorUnaryExpression() {
		return basicOperatorUnaryExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBasicOperatorUnaryExpression_Operator() {
		return (EAttribute) basicOperatorUnaryExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBasicOperatorBinaryExpression() {
		return basicOperatorBinaryExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBasicOperatorBinaryExpression_Operator() {
		return (EAttribute) basicOperatorBinaryExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValueType() {
		return valueTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getValueType_Unit() {
		return (EAttribute) valueTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getValueType_Description() {
		return (EAttribute) valueTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntegerValueType() {
		return integerValueTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntegerValueType_Min() {
		return (EAttribute) integerValueTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntegerValueType_Max() {
		return (EAttribute) integerValueTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFloatValueType() {
		return floatValueTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFloatValueType_Min() {
		return (EAttribute) floatValueTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFloatValueType_Max() {
		return (EAttribute) floatValueTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooleanValueType() {
		return booleanValueTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringValueType() {
		return stringValueTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnumerationValueType() {
		return enumerationValueTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnumerationValueType_EnumerationLiteral() {
		return (EReference) enumerationValueTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnumerationLiteral() {
		return enumerationLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssignable() {
		return assignableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCallableDeclaration() {
		return callableDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunction() {
		return functionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunction_FunctionPorts() {
		return (EReference) functionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunction_Action() {
		return (EReference) functionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunction_SubFunctions() {
		return (EReference) functionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunction_FunctionConnectors() {
		return (EReference) functionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunction_FunctionVarDecls() {
		return (EReference) functionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunction_TimeReference() {
		return (EReference) functionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFunction__AllSubFunctions() {
		return functionEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFunction__Dse_startEvalFunction() {
		return functionEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFunction__Dse_stopEvalFunction() {
		return functionEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFunction__IsEnabled() {
		return functionEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunctionTimeReference() {
		return functionTimeReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionTimeReference_ObservableVar() {
		return (EReference) functionTimeReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionTimeReference_Increment() {
		return (EReference) functionTimeReferenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExternalProcedure() {
		return externalProcedureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFCLProcedure() {
		return fclProcedureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFCLProcedure_Action() {
		return (EReference) fclProcedureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFCLProcedureReturn() {
		return fclProcedureReturnEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFCLProcedureReturn_Expression() {
		return (EReference) fclProcedureReturnEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJavaProcedure() {
		return javaProcedureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getJavaProcedure_MethodFullQualifiedName() {
		return (EAttribute) javaProcedureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getJavaProcedure_Static() {
		return (EAttribute) javaProcedureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntegerValue() {
		return integerValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntegerValue_IntValue() {
		return (EAttribute) integerValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFloatValue() {
		return floatValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFloatValue_FloatValue() {
		return (EAttribute) floatValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringValue() {
		return stringValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringValue_StringValue() {
		return (EAttribute) stringValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooleanValue() {
		return booleanValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooleanValue_BooleanValue() {
		return (EAttribute) booleanValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunctionVarDecl() {
		return functionVarDeclEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionVarDecl_Type() {
		return (EReference) functionVarDeclEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionVarDecl_InitialValue() {
		return (EReference) functionVarDeclEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionVarDecl_CurrentValue() {
		return (EReference) functionVarDeclEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFMUFunction() {
		return fmuFunctionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFMUFunction_RunnableFmuPath() {
		return (EAttribute) fmuFunctionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFMUFunction_FmuSimulationWrapper() {
		return (EReference) fmuFunctionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValue() {
		return valueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCallConstant() {
		return callConstantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallConstant_Value() {
		return (EReference) callConstantEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnumerationValue() {
		return enumerationValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnumerationValue_EnumerationValue() {
		return (EReference) enumerationValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFMUFunctionBlockDataPort() {
		return fmuFunctionBlockDataPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFMUFunctionBlockDataPort_RunnableFmuVariableName() {
		return (EAttribute) fmuFunctionBlockDataPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnityFunction() {
		return unityFunctionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnityFunction_UnityWebPagePath() {
		return (EAttribute) unityFunctionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUnityFunction_UnityWrapper() {
		return (EReference) unityFunctionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnityFunctionDataPort() {
		return unityFunctionDataPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnityFunctionDataPort_UnityNodeName() {
		return (EAttribute) unityFunctionDataPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnityFunctionDataPort_UnityVariableName() {
		return (EAttribute) unityFunctionDataPortEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCallPrevious() {
		return callPreviousEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallPrevious_Callable() {
		return (EReference) callPreviousEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataStructProperty() {
		return dataStructPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataStructProperty_Type() {
		return (EReference) dataStructPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataStructPropertyReference() {
		return dataStructPropertyReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataStructPropertyReference_PropertyName() {
		return (EAttribute) dataStructPropertyReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataStructPropertyAssignment() {
		return dataStructPropertyAssignmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataStructPropertyAssignment_Expression() {
		return (EReference) dataStructPropertyAssignmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataStructPropertyAssignment_Assignable() {
		return (EReference) dataStructPropertyAssignmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCallDataStructProperty() {
		return callDataStructPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallDataStructProperty_RootTargetDataStruct() {
		return (EReference) callDataStructPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNewDataStruct() {
		return newDataStructEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNewDataStruct_PropertyAssignments() {
		return (EReference) newDataStructEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStructValue() {
		return structValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStructValue_StructProperties() {
		return (EReference) structValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStructPropertyValue() {
		return structPropertyValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStructPropertyValue_Value() {
		return (EReference) structPropertyValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLog() {
		return logEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLog_Expression() {
		return (EReference) logEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDirectionKind() {
		return directionKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getBinaryOperator() {
		return binaryOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getUnaryOperator() {
		return unaryOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FclFactory getFclFactory() {
		return (FclFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		fclModelEClass = createEClass(FCL_MODEL);
		createEReference(fclModelEClass, FCL_MODEL__MODE_AUTOMATA);
		createEReference(fclModelEClass, FCL_MODEL__EVENTS);
		createEReference(fclModelEClass, FCL_MODEL__PROCEDURES);
		createEReference(fclModelEClass, FCL_MODEL__DATA_TYPES);
		createEReference(fclModelEClass, FCL_MODEL__DATAFLOW);
		createEReference(fclModelEClass, FCL_MODEL__RECEIVED_EVENTS);
		createEReference(fclModelEClass, FCL_MODEL__PROCEDURE_STACK);
		createEAttribute(fclModelEClass, FCL_MODEL__INDENTATION);
		createEOperation(fclModelEClass, FCL_MODEL___ALL_FUNCTIONS);
		createEOperation(fclModelEClass, FCL_MODEL___ALL_CONNECTORS);
		createEOperation(fclModelEClass, FCL_MODEL___PROPAGATE_DELAYED_RESULTS);
		createEOperation(fclModelEClass, FCL_MODEL___EVALUATE_MODE_AUTOMATA);

		modeAutomataEClass = createEClass(MODE_AUTOMATA);
		createEReference(modeAutomataEClass, MODE_AUTOMATA__MODES);
		createEReference(modeAutomataEClass, MODE_AUTOMATA__INITIAL_MODE);
		createEReference(modeAutomataEClass, MODE_AUTOMATA__TRANSITIONS);
		createEReference(modeAutomataEClass, MODE_AUTOMATA__CURRENT_MODE);

		transitionEClass = createEClass(TRANSITION);
		createEReference(transitionEClass, TRANSITION__ACTION);
		createEReference(transitionEClass, TRANSITION__GUARD);
		createEReference(transitionEClass, TRANSITION__EVENT);
		createEReference(transitionEClass, TRANSITION__TARGET);
		createEReference(transitionEClass, TRANSITION__SOURCE);

		modeEClass = createEClass(MODE);
		createEReference(modeEClass, MODE__ENTRY);
		createEReference(modeEClass, MODE__EXIT);
		createEAttribute(modeEClass, MODE__FINAL);
		createEReference(modeEClass, MODE__ENABLED_FUNCTIONS);
		createEReference(modeEClass, MODE__INCOMING_TRANSITION);
		createEReference(modeEClass, MODE__OUTGOING_TRANSITION);

		actionEClass = createEClass(ACTION);

		eventEClass = createEClass(EVENT);

		expressionEClass = createEClass(EXPRESSION);
		createEReference(expressionEClass, EXPRESSION__RESULT_TYPE);

		procedureEClass = createEClass(PROCEDURE);
		createEReference(procedureEClass, PROCEDURE__PROCEDURE_PARAMETERS);
		createEReference(procedureEClass, PROCEDURE__RETURN_TYPE);

		actionBlockEClass = createEClass(ACTION_BLOCK);
		createEReference(actionBlockEClass, ACTION_BLOCK__ACTIONS);

		dataStructTypeEClass = createEClass(DATA_STRUCT_TYPE);
		createEReference(dataStructTypeEClass, DATA_STRUCT_TYPE__PROPERTIES);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		functionBlockDataPortEClass = createEClass(FUNCTION_BLOCK_DATA_PORT);
		createEReference(functionBlockDataPortEClass, FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE);
		createEReference(functionBlockDataPortEClass, FUNCTION_BLOCK_DATA_PORT__FUNCTION_VAR_STORE);
		createEReference(functionBlockDataPortEClass, FUNCTION_BLOCK_DATA_PORT__CURRENT_VALUE);

		dataTypeEClass = createEClass(DATA_TYPE);

		procedureParameterEClass = createEClass(PROCEDURE_PARAMETER);
		createEReference(procedureParameterEClass, PROCEDURE_PARAMETER__TYPE);

		functionPortEClass = createEClass(FUNCTION_PORT);
		createEReference(functionPortEClass, FUNCTION_PORT__DATA_TYPE);
		createEAttribute(functionPortEClass, FUNCTION_PORT__DIRECTION);
		createEOperation(functionPortEClass, FUNCTION_PORT___OUTGOING_CONNECTORS);
		createEOperation(functionPortEClass, FUNCTION_PORT___INCOMING_CONNECTORS);

		functionConnectorEClass = createEClass(FUNCTION_CONNECTOR);
		createEReference(functionConnectorEClass, FUNCTION_CONNECTOR__EMITTER);
		createEReference(functionConnectorEClass, FUNCTION_CONNECTOR__RECEIVER);
		createEAttribute(functionConnectorEClass, FUNCTION_CONNECTOR__DELAYED);
		createEOperation(functionConnectorEClass, FUNCTION_CONNECTOR___DSE_SEND_DATA);

		externalEventEClass = createEClass(EXTERNAL_EVENT);

		binaryExpressionEClass = createEClass(BINARY_EXPRESSION);
		createEReference(binaryExpressionEClass, BINARY_EXPRESSION__LHS_OPERAND);
		createEReference(binaryExpressionEClass, BINARY_EXPRESSION__RHS_OPERAND);

		unaryExpressionEClass = createEClass(UNARY_EXPRESSION);
		createEReference(unaryExpressionEClass, UNARY_EXPRESSION__OPERAND);

		castEClass = createEClass(CAST);

		callEClass = createEClass(CALL);

		primitiveActionEClass = createEClass(PRIMITIVE_ACTION);

		assignmentEClass = createEClass(ASSIGNMENT);
		createEReference(assignmentEClass, ASSIGNMENT__ASSIGNABLE);
		createEReference(assignmentEClass, ASSIGNMENT__EXPRESSION);

		controlStructureActionEClass = createEClass(CONTROL_STRUCTURE_ACTION);

		ifActionEClass = createEClass(IF_ACTION);
		createEReference(ifActionEClass, IF_ACTION__CONDITION);
		createEReference(ifActionEClass, IF_ACTION__THEN_ACTION);
		createEReference(ifActionEClass, IF_ACTION__ELSE_ACTION);

		whileActionEClass = createEClass(WHILE_ACTION);
		createEReference(whileActionEClass, WHILE_ACTION__CONDITION);
		createEReference(whileActionEClass, WHILE_ACTION__ACTION);

		procedureCallEClass = createEClass(PROCEDURE_CALL);
		createEReference(procedureCallEClass, PROCEDURE_CALL__PROCEDURE);
		createEReference(procedureCallEClass, PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS);

		varDeclEClass = createEClass(VAR_DECL);
		createEReference(varDeclEClass, VAR_DECL__TYPE);
		createEReference(varDeclEClass, VAR_DECL__INITIAL_VALUE);

		callDeclarationReferenceEClass = createEClass(CALL_DECLARATION_REFERENCE);
		createEReference(callDeclarationReferenceEClass, CALL_DECLARATION_REFERENCE__CALLABLE);

		functionPortReferenceEClass = createEClass(FUNCTION_PORT_REFERENCE);
		createEReference(functionPortReferenceEClass, FUNCTION_PORT_REFERENCE__FUNCTION_PORT);

		basicOperatorUnaryExpressionEClass = createEClass(BASIC_OPERATOR_UNARY_EXPRESSION);
		createEAttribute(basicOperatorUnaryExpressionEClass, BASIC_OPERATOR_UNARY_EXPRESSION__OPERATOR);

		basicOperatorBinaryExpressionEClass = createEClass(BASIC_OPERATOR_BINARY_EXPRESSION);
		createEAttribute(basicOperatorBinaryExpressionEClass, BASIC_OPERATOR_BINARY_EXPRESSION__OPERATOR);

		valueTypeEClass = createEClass(VALUE_TYPE);
		createEAttribute(valueTypeEClass, VALUE_TYPE__UNIT);
		createEAttribute(valueTypeEClass, VALUE_TYPE__DESCRIPTION);

		integerValueTypeEClass = createEClass(INTEGER_VALUE_TYPE);
		createEAttribute(integerValueTypeEClass, INTEGER_VALUE_TYPE__MIN);
		createEAttribute(integerValueTypeEClass, INTEGER_VALUE_TYPE__MAX);

		floatValueTypeEClass = createEClass(FLOAT_VALUE_TYPE);
		createEAttribute(floatValueTypeEClass, FLOAT_VALUE_TYPE__MIN);
		createEAttribute(floatValueTypeEClass, FLOAT_VALUE_TYPE__MAX);

		booleanValueTypeEClass = createEClass(BOOLEAN_VALUE_TYPE);

		stringValueTypeEClass = createEClass(STRING_VALUE_TYPE);

		enumerationValueTypeEClass = createEClass(ENUMERATION_VALUE_TYPE);
		createEReference(enumerationValueTypeEClass, ENUMERATION_VALUE_TYPE__ENUMERATION_LITERAL);

		enumerationLiteralEClass = createEClass(ENUMERATION_LITERAL);

		assignableEClass = createEClass(ASSIGNABLE);

		callableDeclarationEClass = createEClass(CALLABLE_DECLARATION);

		functionEClass = createEClass(FUNCTION);
		createEReference(functionEClass, FUNCTION__FUNCTION_PORTS);
		createEReference(functionEClass, FUNCTION__ACTION);
		createEReference(functionEClass, FUNCTION__SUB_FUNCTIONS);
		createEReference(functionEClass, FUNCTION__FUNCTION_CONNECTORS);
		createEReference(functionEClass, FUNCTION__FUNCTION_VAR_DECLS);
		createEReference(functionEClass, FUNCTION__TIME_REFERENCE);
		createEOperation(functionEClass, FUNCTION___ALL_SUB_FUNCTIONS);
		createEOperation(functionEClass, FUNCTION___DSE_START_EVAL_FUNCTION);
		createEOperation(functionEClass, FUNCTION___DSE_STOP_EVAL_FUNCTION);
		createEOperation(functionEClass, FUNCTION___IS_ENABLED);

		functionTimeReferenceEClass = createEClass(FUNCTION_TIME_REFERENCE);
		createEReference(functionTimeReferenceEClass, FUNCTION_TIME_REFERENCE__OBSERVABLE_VAR);
		createEReference(functionTimeReferenceEClass, FUNCTION_TIME_REFERENCE__INCREMENT);

		externalProcedureEClass = createEClass(EXTERNAL_PROCEDURE);

		fclProcedureEClass = createEClass(FCL_PROCEDURE);
		createEReference(fclProcedureEClass, FCL_PROCEDURE__ACTION);

		fclProcedureReturnEClass = createEClass(FCL_PROCEDURE_RETURN);
		createEReference(fclProcedureReturnEClass, FCL_PROCEDURE_RETURN__EXPRESSION);

		javaProcedureEClass = createEClass(JAVA_PROCEDURE);
		createEAttribute(javaProcedureEClass, JAVA_PROCEDURE__METHOD_FULL_QUALIFIED_NAME);
		createEAttribute(javaProcedureEClass, JAVA_PROCEDURE__STATIC);

		integerValueEClass = createEClass(INTEGER_VALUE);
		createEAttribute(integerValueEClass, INTEGER_VALUE__INT_VALUE);

		floatValueEClass = createEClass(FLOAT_VALUE);
		createEAttribute(floatValueEClass, FLOAT_VALUE__FLOAT_VALUE);

		stringValueEClass = createEClass(STRING_VALUE);
		createEAttribute(stringValueEClass, STRING_VALUE__STRING_VALUE);

		booleanValueEClass = createEClass(BOOLEAN_VALUE);
		createEAttribute(booleanValueEClass, BOOLEAN_VALUE__BOOLEAN_VALUE);

		functionVarDeclEClass = createEClass(FUNCTION_VAR_DECL);
		createEReference(functionVarDeclEClass, FUNCTION_VAR_DECL__TYPE);
		createEReference(functionVarDeclEClass, FUNCTION_VAR_DECL__INITIAL_VALUE);
		createEReference(functionVarDeclEClass, FUNCTION_VAR_DECL__CURRENT_VALUE);

		valueEClass = createEClass(VALUE);

		callConstantEClass = createEClass(CALL_CONSTANT);
		createEReference(callConstantEClass, CALL_CONSTANT__VALUE);

		enumerationValueEClass = createEClass(ENUMERATION_VALUE);
		createEReference(enumerationValueEClass, ENUMERATION_VALUE__ENUMERATION_VALUE);

		fmuFunctionEClass = createEClass(FMU_FUNCTION);
		createEAttribute(fmuFunctionEClass, FMU_FUNCTION__RUNNABLE_FMU_PATH);
		createEReference(fmuFunctionEClass, FMU_FUNCTION__FMU_SIMULATION_WRAPPER);

		fmuFunctionBlockDataPortEClass = createEClass(FMU_FUNCTION_BLOCK_DATA_PORT);
		createEAttribute(fmuFunctionBlockDataPortEClass, FMU_FUNCTION_BLOCK_DATA_PORT__RUNNABLE_FMU_VARIABLE_NAME);

		unityFunctionEClass = createEClass(UNITY_FUNCTION);
		createEAttribute(unityFunctionEClass, UNITY_FUNCTION__UNITY_WEB_PAGE_PATH);
		createEReference(unityFunctionEClass, UNITY_FUNCTION__UNITY_WRAPPER);

		unityFunctionDataPortEClass = createEClass(UNITY_FUNCTION_DATA_PORT);
		createEAttribute(unityFunctionDataPortEClass, UNITY_FUNCTION_DATA_PORT__UNITY_NODE_NAME);
		createEAttribute(unityFunctionDataPortEClass, UNITY_FUNCTION_DATA_PORT__UNITY_VARIABLE_NAME);

		callPreviousEClass = createEClass(CALL_PREVIOUS);
		createEReference(callPreviousEClass, CALL_PREVIOUS__CALLABLE);

		dataStructPropertyEClass = createEClass(DATA_STRUCT_PROPERTY);
		createEReference(dataStructPropertyEClass, DATA_STRUCT_PROPERTY__TYPE);

		dataStructPropertyReferenceEClass = createEClass(DATA_STRUCT_PROPERTY_REFERENCE);
		createEAttribute(dataStructPropertyReferenceEClass, DATA_STRUCT_PROPERTY_REFERENCE__PROPERTY_NAME);

		dataStructPropertyAssignmentEClass = createEClass(DATA_STRUCT_PROPERTY_ASSIGNMENT);
		createEReference(dataStructPropertyAssignmentEClass, DATA_STRUCT_PROPERTY_ASSIGNMENT__EXPRESSION);
		createEReference(dataStructPropertyAssignmentEClass, DATA_STRUCT_PROPERTY_ASSIGNMENT__ASSIGNABLE);

		callDataStructPropertyEClass = createEClass(CALL_DATA_STRUCT_PROPERTY);
		createEReference(callDataStructPropertyEClass, CALL_DATA_STRUCT_PROPERTY__ROOT_TARGET_DATA_STRUCT);

		newDataStructEClass = createEClass(NEW_DATA_STRUCT);
		createEReference(newDataStructEClass, NEW_DATA_STRUCT__PROPERTY_ASSIGNMENTS);

		structValueEClass = createEClass(STRUCT_VALUE);
		createEReference(structValueEClass, STRUCT_VALUE__STRUCT_PROPERTIES);

		structPropertyValueEClass = createEClass(STRUCT_PROPERTY_VALUE);
		createEReference(structPropertyValueEClass, STRUCT_PROPERTY_VALUE__VALUE);

		logEClass = createEClass(LOG);
		createEReference(logEClass, LOG__EXPRESSION);

		// Create enums
		directionKindEEnum = createEEnum(DIRECTION_KIND);
		binaryOperatorEEnum = createEEnum(BINARY_OPERATOR);
		unaryOperatorEEnum = createEEnum(UNARY_OPERATOR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		Interpreter_vmPackage theInterpreter_vmPackage = (Interpreter_vmPackage) EPackage.Registry.INSTANCE
				.getEPackage(Interpreter_vmPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theInterpreter_vmPackage);

		// Create type parameters
		addETypeParameter(fmuFunctionBlockDataPortEClass, "T");

		// Set bounds for type parameters

		// Add supertypes to classes
		fclModelEClass.getESuperTypes().add(this.getNamedElement());
		modeAutomataEClass.getESuperTypes().add(this.getMode());
		transitionEClass.getESuperTypes().add(this.getNamedElement());
		modeEClass.getESuperTypes().add(this.getNamedElement());
		eventEClass.getESuperTypes().add(this.getNamedElement());
		procedureEClass.getESuperTypes().add(this.getNamedElement());
		actionBlockEClass.getESuperTypes().add(this.getControlStructureAction());
		dataStructTypeEClass.getESuperTypes().add(this.getDataType());
		functionBlockDataPortEClass.getESuperTypes().add(this.getFunctionPort());
		dataTypeEClass.getESuperTypes().add(this.getNamedElement());
		procedureParameterEClass.getESuperTypes().add(this.getNamedElement());
		procedureParameterEClass.getESuperTypes().add(this.getAssignable());
		procedureParameterEClass.getESuperTypes().add(this.getCallableDeclaration());
		functionPortEClass.getESuperTypes().add(this.getAssignable());
		functionPortEClass.getESuperTypes().add(this.getCallableDeclaration());
		functionPortEClass.getESuperTypes().add(this.getNamedElement());
		externalEventEClass.getESuperTypes().add(this.getEvent());
		binaryExpressionEClass.getESuperTypes().add(this.getExpression());
		unaryExpressionEClass.getESuperTypes().add(this.getExpression());
		castEClass.getESuperTypes().add(this.getUnaryExpression());
		callEClass.getESuperTypes().add(this.getExpression());
		primitiveActionEClass.getESuperTypes().add(this.getAction());
		assignmentEClass.getESuperTypes().add(this.getPrimitiveAction());
		controlStructureActionEClass.getESuperTypes().add(this.getAction());
		ifActionEClass.getESuperTypes().add(this.getControlStructureAction());
		whileActionEClass.getESuperTypes().add(this.getControlStructureAction());
		procedureCallEClass.getESuperTypes().add(this.getPrimitiveAction());
		procedureCallEClass.getESuperTypes().add(this.getExpression());
		varDeclEClass.getESuperTypes().add(this.getPrimitiveAction());
		varDeclEClass.getESuperTypes().add(this.getAssignable());
		varDeclEClass.getESuperTypes().add(this.getCallableDeclaration());
		varDeclEClass.getESuperTypes().add(this.getNamedElement());
		callDeclarationReferenceEClass.getESuperTypes().add(this.getCall());
		functionPortReferenceEClass.getESuperTypes().add(this.getFunctionPort());
		basicOperatorUnaryExpressionEClass.getESuperTypes().add(this.getUnaryExpression());
		basicOperatorBinaryExpressionEClass.getESuperTypes().add(this.getBinaryExpression());
		valueTypeEClass.getESuperTypes().add(this.getDataType());
		integerValueTypeEClass.getESuperTypes().add(this.getValueType());
		floatValueTypeEClass.getESuperTypes().add(this.getValueType());
		booleanValueTypeEClass.getESuperTypes().add(this.getValueType());
		stringValueTypeEClass.getESuperTypes().add(this.getValueType());
		enumerationValueTypeEClass.getESuperTypes().add(this.getValueType());
		enumerationLiteralEClass.getESuperTypes().add(this.getNamedElement());
		enumerationLiteralEClass.getESuperTypes().add(this.getCallableDeclaration());
		functionEClass.getESuperTypes().add(this.getNamedElement());
		externalProcedureEClass.getESuperTypes().add(this.getProcedure());
		fclProcedureEClass.getESuperTypes().add(this.getProcedure());
		fclProcedureReturnEClass.getESuperTypes().add(this.getPrimitiveAction());
		javaProcedureEClass.getESuperTypes().add(this.getExternalProcedure());
		integerValueEClass.getESuperTypes().add(this.getValue());
		floatValueEClass.getESuperTypes().add(this.getValue());
		stringValueEClass.getESuperTypes().add(this.getValue());
		booleanValueEClass.getESuperTypes().add(this.getValue());
		functionVarDeclEClass.getESuperTypes().add(this.getAssignable());
		functionVarDeclEClass.getESuperTypes().add(this.getCallableDeclaration());
		functionVarDeclEClass.getESuperTypes().add(this.getNamedElement());
		callConstantEClass.getESuperTypes().add(this.getCall());
		enumerationValueEClass.getESuperTypes().add(this.getValue());
		fmuFunctionEClass.getESuperTypes().add(this.getFunction());
		fmuFunctionBlockDataPortEClass.getESuperTypes().add(this.getFunctionBlockDataPort());
		unityFunctionEClass.getESuperTypes().add(this.getFunction());
		unityFunctionDataPortEClass.getESuperTypes().add(this.getFunctionBlockDataPort());
		callPreviousEClass.getESuperTypes().add(this.getCall());
		dataStructPropertyEClass.getESuperTypes().add(this.getNamedElement());
		dataStructPropertyAssignmentEClass.getESuperTypes().add(this.getDataStructPropertyReference());
		dataStructPropertyAssignmentEClass.getESuperTypes().add(this.getPrimitiveAction());
		callDataStructPropertyEClass.getESuperTypes().add(this.getDataStructPropertyReference());
		callDataStructPropertyEClass.getESuperTypes().add(this.getCall());
		newDataStructEClass.getESuperTypes().add(this.getExpression());
		structValueEClass.getESuperTypes().add(this.getValue());
		structPropertyValueEClass.getESuperTypes().add(this.getNamedElement());
		logEClass.getESuperTypes().add(this.getPrimitiveAction());

		// Initialize classes, features, and operations; add parameters
		initEClass(fclModelEClass, FCLModel.class, "FCLModel", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFCLModel_ModeAutomata(), this.getModeAutomata(), null, "modeAutomata", null, 1, 1,
				FCLModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFCLModel_Events(), this.getEvent(), null, "events", null, 0, -1, FCLModel.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFCLModel_Procedures(), this.getProcedure(), null, "procedures", null, 0, -1, FCLModel.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFCLModel_DataTypes(), this.getDataType(), null, "dataTypes", null, 0, -1, FCLModel.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFCLModel_Dataflow(), this.getFunction(), null, "dataflow", null, 0, 1, FCLModel.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFCLModel_ReceivedEvents(), theInterpreter_vmPackage.getEventOccurrence(), null,
				"receivedEvents", null, 0, -1, FCLModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFCLModel_ProcedureStack(), theInterpreter_vmPackage.getProcedureContext(), null,
				"procedureStack", null, 0, -1, FCLModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFCLModel_Indentation(), ecorePackage.getEString(), "indentation", null, 0, 1, FCLModel.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getFCLModel__AllFunctions(), this.getFunction(), "allFunctions", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getFCLModel__AllConnectors(), this.getFunctionConnector(), "allConnectors", 0, -1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getFCLModel__PropagateDelayedResults(), null, "propagateDelayedResults", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getFCLModel__EvaluateModeAutomata(), null, "evaluateModeAutomata", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(modeAutomataEClass, ModeAutomata.class, "ModeAutomata", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getModeAutomata_Modes(), this.getMode(), null, "modes", null, 0, -1, ModeAutomata.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModeAutomata_InitialMode(), this.getMode(), null, "initialMode", null, 1, 1,
				ModeAutomata.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModeAutomata_Transitions(), this.getTransition(), null, "transitions", null, 0, -1,
				ModeAutomata.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModeAutomata_CurrentMode(), this.getMode(), null, "currentMode", null, 0, 1,
				ModeAutomata.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(transitionEClass, Transition.class, "Transition", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTransition_Action(), this.getAction(), null, "action", null, 0, 1, Transition.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransition_Guard(), this.getExpression(), null, "guard", null, 0, 1, Transition.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransition_Event(), this.getEvent(), null, "event", null, 0, 1, Transition.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransition_Target(), this.getMode(), this.getMode_IncomingTransition(), "target", null, 1, 1,
				Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransition_Source(), this.getMode(), this.getMode_OutgoingTransition(), "source", null, 1, 1,
				Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(modeEClass, Mode.class, "Mode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMode_Entry(), this.getAction(), null, "entry", null, 0, 1, Mode.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMode_Exit(), this.getAction(), null, "exit", null, 0, 1, Mode.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMode_Final(), ecorePackage.getEBoolean(), "final", null, 0, 1, Mode.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMode_EnabledFunctions(), this.getFunction(), null, "enabledFunctions", null, 0, -1,
				Mode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMode_IncomingTransition(), this.getTransition(), this.getTransition_Target(),
				"incomingTransition", null, 0, -1, Mode.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMode_OutgoingTransition(), this.getTransition(), this.getTransition_Source(),
				"outgoingTransition", null, 0, -1, Mode.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(actionEClass, Action.class, "Action", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(eventEClass, Event.class, "Event", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(expressionEClass, Expression.class, "Expression", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExpression_ResultType(), this.getDataType(), null, "resultType", null, 0, 1, Expression.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(procedureEClass, Procedure.class, "Procedure", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProcedure_ProcedureParameters(), this.getProcedureParameter(), null, "procedureParameters",
				null, 0, -1, Procedure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcedure_ReturnType(), this.getDataType(), null, "returnType", null, 0, 1, Procedure.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(actionBlockEClass, ActionBlock.class, "ActionBlock", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActionBlock_Actions(), this.getAction(), null, "actions", null, 0, -1, ActionBlock.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataStructTypeEClass, DataStructType.class, "DataStructType", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataStructType_Properties(), this.getDataStructProperty(), null, "properties", null, 0, -1,
				DataStructType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, NamedElement.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(functionBlockDataPortEClass, FunctionBlockDataPort.class, "FunctionBlockDataPort", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFunctionBlockDataPort_DefaultInputValue(), this.getExpression(), null, "defaultInputValue",
				null, 0, 1, FunctionBlockDataPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunctionBlockDataPort_FunctionVarStore(), this.getFunctionVarDecl(), null, "functionVarStore",
				null, 0, 1, FunctionBlockDataPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunctionBlockDataPort_CurrentValue(), this.getValue(), null, "currentValue", null, 0, 1,
				FunctionBlockDataPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataTypeEClass, DataType.class, "DataType", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(procedureParameterEClass, ProcedureParameter.class, "ProcedureParameter", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProcedureParameter_Type(), this.getDataType(), null, "type", null, 1, 1,
				ProcedureParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(functionPortEClass, FunctionPort.class, "FunctionPort", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFunctionPort_DataType(), this.getDataType(), null, "dataType", null, 0, 1, FunctionPort.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFunctionPort_Direction(), this.getDirectionKind(), "direction", "InOut", 0, 1,
				FunctionPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEOperation(getFunctionPort__OutgoingConnectors(), this.getFunctionConnector(), "outgoingConnectors", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getFunctionPort__IncomingConnectors(), this.getFunctionConnector(), "incomingConnectors", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(functionConnectorEClass, FunctionConnector.class, "FunctionConnector", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFunctionConnector_Emitter(), this.getFunctionPort(), null, "emitter", null, 1, 1,
				FunctionConnector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunctionConnector_Receiver(), this.getFunctionPort(), null, "receiver", null, 1, 1,
				FunctionConnector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFunctionConnector_Delayed(), ecorePackage.getEBoolean(), "delayed", null, 0, 1,
				FunctionConnector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEOperation(getFunctionConnector__Dse_sendData(), null, "dse_sendData", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(externalEventEClass, ExternalEvent.class, "ExternalEvent", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(binaryExpressionEClass, BinaryExpression.class, "BinaryExpression", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBinaryExpression_LhsOperand(), this.getExpression(), null, "lhsOperand", null, 1, 1,
				BinaryExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBinaryExpression_RhsOperand(), this.getExpression(), null, "rhsOperand", null, 1, 1,
				BinaryExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unaryExpressionEClass, UnaryExpression.class, "UnaryExpression", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUnaryExpression_Operand(), this.getExpression(), null, "operand", null, 1, 1,
				UnaryExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(castEClass, Cast.class, "Cast", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(callEClass, Call.class, "Call", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(primitiveActionEClass, PrimitiveAction.class, "PrimitiveAction", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(assignmentEClass, Assignment.class, "Assignment", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssignment_Assignable(), this.getAssignable(), null, "assignable", null, 1, 1,
				Assignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssignment_Expression(), this.getExpression(), null, "expression", null, 1, 1,
				Assignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(controlStructureActionEClass, ControlStructureAction.class, "ControlStructureAction", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ifActionEClass, IfAction.class, "IfAction", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIfAction_Condition(), this.getExpression(), null, "condition", null, 1, 1, IfAction.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIfAction_ThenAction(), this.getAction(), null, "thenAction", null, 0, 1, IfAction.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIfAction_ElseAction(), this.getAction(), null, "elseAction", null, 0, 1, IfAction.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(whileActionEClass, WhileAction.class, "WhileAction", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getWhileAction_Condition(), this.getExpression(), null, "condition", null, 1, 1,
				WhileAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWhileAction_Action(), this.getAction(), null, "action", null, 0, 1, WhileAction.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(procedureCallEClass, ProcedureCall.class, "ProcedureCall", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProcedureCall_Procedure(), this.getProcedure(), null, "procedure", null, 1, 1,
				ProcedureCall.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcedureCall_ProcedureCallArguments(), this.getExpression(), null, "procedureCallArguments",
				null, 0, -1, ProcedureCall.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(varDeclEClass, VarDecl.class, "VarDecl", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVarDecl_Type(), this.getDataType(), null, "type", null, 1, 1, VarDecl.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getVarDecl_InitialValue(), this.getExpression(), null, "initialValue", null, 0, 1, VarDecl.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(callDeclarationReferenceEClass, CallDeclarationReference.class, "CallDeclarationReference",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCallDeclarationReference_Callable(), this.getCallableDeclaration(), null, "callable", null, 1,
				1, CallDeclarationReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(functionPortReferenceEClass, FunctionPortReference.class, "FunctionPortReference", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFunctionPortReference_FunctionPort(), this.getFunctionPort(), null, "functionPort", null, 1,
				1, FunctionPortReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(basicOperatorUnaryExpressionEClass, BasicOperatorUnaryExpression.class,
				"BasicOperatorUnaryExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBasicOperatorUnaryExpression_Operator(), this.getUnaryOperator(), "operator", null, 0, 1,
				BasicOperatorUnaryExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(basicOperatorBinaryExpressionEClass, BasicOperatorBinaryExpression.class,
				"BasicOperatorBinaryExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBasicOperatorBinaryExpression_Operator(), this.getBinaryOperator(), "operator", null, 0, 1,
				BasicOperatorBinaryExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(valueTypeEClass, ValueType.class, "ValueType", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getValueType_Unit(), ecorePackage.getEString(), "unit", null, 0, 1, ValueType.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getValueType_Description(), ecorePackage.getEString(), "description", null, 0, 1,
				ValueType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(integerValueTypeEClass, IntegerValueType.class, "IntegerValueType", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntegerValueType_Min(), ecorePackage.getEInt(), "min", null, 0, 1, IntegerValueType.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIntegerValueType_Max(), ecorePackage.getEInt(), "max", null, 0, 1, IntegerValueType.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(floatValueTypeEClass, FloatValueType.class, "FloatValueType", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFloatValueType_Min(), ecorePackage.getEDouble(), "min", null, 0, 1, FloatValueType.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFloatValueType_Max(), ecorePackage.getEDouble(), "max", null, 0, 1, FloatValueType.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(booleanValueTypeEClass, BooleanValueType.class, "BooleanValueType", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(stringValueTypeEClass, StringValueType.class, "StringValueType", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(enumerationValueTypeEClass, EnumerationValueType.class, "EnumerationValueType", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEnumerationValueType_EnumerationLiteral(), this.getEnumerationLiteral(), null,
				"enumerationLiteral", null, 0, -1, EnumerationValueType.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(enumerationLiteralEClass, EnumerationLiteral.class, "EnumerationLiteral", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(assignableEClass, Assignable.class, "Assignable", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(callableDeclarationEClass, CallableDeclaration.class, "CallableDeclaration", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(functionEClass, Function.class, "Function", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFunction_FunctionPorts(), this.getFunctionPort(), null, "functionPorts", null, 0, -1,
				Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunction_Action(), this.getAction(), null, "action", null, 0, 1, Function.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunction_SubFunctions(), this.getFunction(), null, "subFunctions", null, 0, -1,
				Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunction_FunctionConnectors(), this.getFunctionConnector(), null, "functionConnectors", null,
				0, -1, Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunction_FunctionVarDecls(), this.getFunctionVarDecl(), null, "functionVarDecls", null, 0, -1,
				Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunction_TimeReference(), this.getFunctionTimeReference(), null, "timeReference", null, 0, 1,
				Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getFunction__AllSubFunctions(), this.getFunction(), "allSubFunctions", 0, -1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getFunction__Dse_startEvalFunction(), null, "dse_startEvalFunction", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getFunction__Dse_stopEvalFunction(), null, "dse_stopEvalFunction", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getFunction__IsEnabled(), ecorePackage.getEBoolean(), "isEnabled", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(functionTimeReferenceEClass, FunctionTimeReference.class, "FunctionTimeReference", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFunctionTimeReference_ObservableVar(), this.getFunctionVarDecl(), null, "observableVar", null,
				1, 1, FunctionTimeReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunctionTimeReference_Increment(), this.getExpression(), null, "increment", null, 1, 1,
				FunctionTimeReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(externalProcedureEClass, ExternalProcedure.class, "ExternalProcedure", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(fclProcedureEClass, FCLProcedure.class, "FCLProcedure", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFCLProcedure_Action(), this.getAction(), null, "action", null, 0, 1, FCLProcedure.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fclProcedureReturnEClass, FCLProcedureReturn.class, "FCLProcedureReturn", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFCLProcedureReturn_Expression(), this.getExpression(), null, "expression", null, 1, 1,
				FCLProcedureReturn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(javaProcedureEClass, JavaProcedure.class, "JavaProcedure", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getJavaProcedure_MethodFullQualifiedName(), ecorePackage.getEString(), "methodFullQualifiedName",
				null, 0, 1, JavaProcedure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getJavaProcedure_Static(), ecorePackage.getEBoolean(), "static", null, 0, 1, JavaProcedure.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(integerValueEClass, IntegerValue.class, "IntegerValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntegerValue_IntValue(), ecorePackage.getEInt(), "intValue", null, 0, 1, IntegerValue.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(floatValueEClass, FloatValue.class, "FloatValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFloatValue_FloatValue(), ecorePackage.getEDouble(), "floatValue", null, 0, 1,
				FloatValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(stringValueEClass, StringValue.class, "StringValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringValue_StringValue(), ecorePackage.getEString(), "stringValue", null, 0, 1,
				StringValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(booleanValueEClass, BooleanValue.class, "BooleanValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBooleanValue_BooleanValue(), ecorePackage.getEBoolean(), "booleanValue", null, 0, 1,
				BooleanValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(functionVarDeclEClass, FunctionVarDecl.class, "FunctionVarDecl", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFunctionVarDecl_Type(), this.getDataType(), null, "type", null, 1, 1, FunctionVarDecl.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunctionVarDecl_InitialValue(), this.getExpression(), null, "initialValue", null, 0, 1,
				FunctionVarDecl.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunctionVarDecl_CurrentValue(), this.getValue(), null, "currentValue", null, 0, 1,
				FunctionVarDecl.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(valueEClass, Value.class, "Value", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(callConstantEClass, CallConstant.class, "CallConstant", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCallConstant_Value(), this.getValue(), null, "value", null, 0, 1, CallConstant.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(enumerationValueEClass, EnumerationValue.class, "EnumerationValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEnumerationValue_EnumerationValue(), this.getEnumerationLiteral(), null, "enumerationValue",
				null, 0, 1, EnumerationValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fmuFunctionEClass, FMUFunction.class, "FMUFunction", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFMUFunction_RunnableFmuPath(), ecorePackage.getEString(), "runnableFmuPath", null, 0, 1,
				FMUFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getFMUFunction_FmuSimulationWrapper(), theInterpreter_vmPackage.getFMUSimulationWrapper(), null,
				"fmuSimulationWrapper", null, 0, 1, FMUFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fmuFunctionBlockDataPortEClass, FMUFunctionBlockDataPort.class, "FMUFunctionBlockDataPort",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFMUFunctionBlockDataPort_RunnableFmuVariableName(), ecorePackage.getEString(),
				"runnableFmuVariableName", null, 0, 1, FMUFunctionBlockDataPort.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unityFunctionEClass, UnityFunction.class, "UnityFunction", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUnityFunction_UnityWebPagePath(), ecorePackage.getEString(), "unityWebPagePath", null, 0, 1,
				UnityFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getUnityFunction_UnityWrapper(), theInterpreter_vmPackage.getUnityWrapper(), null,
				"unityWrapper", null, 0, 1, UnityFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unityFunctionDataPortEClass, UnityFunctionDataPort.class, "UnityFunctionDataPort", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUnityFunctionDataPort_UnityNodeName(), ecorePackage.getEString(), "unityNodeName", null, 0, 1,
				UnityFunctionDataPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUnityFunctionDataPort_UnityVariableName(), ecorePackage.getEString(), "unityVariableName",
				null, 0, 1, UnityFunctionDataPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(callPreviousEClass, CallPrevious.class, "CallPrevious", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCallPrevious_Callable(), this.getCallableDeclaration(), null, "callable", null, 1, 1,
				CallPrevious.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataStructPropertyEClass, DataStructProperty.class, "DataStructProperty", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataStructProperty_Type(), this.getDataType(), null, "type", null, 0, 1,
				DataStructProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataStructPropertyReferenceEClass, DataStructPropertyReference.class, "DataStructPropertyReference",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDataStructPropertyReference_PropertyName(), ecorePackage.getEString(), "propertyName", null,
				1, -1, DataStructPropertyReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataStructPropertyAssignmentEClass, DataStructPropertyAssignment.class,
				"DataStructPropertyAssignment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataStructPropertyAssignment_Expression(), this.getExpression(), null, "expression", null, 0,
				1, DataStructPropertyAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataStructPropertyAssignment_Assignable(), this.getAssignable(), null, "assignable", null, 0,
				1, DataStructPropertyAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(callDataStructPropertyEClass, CallDataStructProperty.class, "CallDataStructProperty", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCallDataStructProperty_RootTargetDataStruct(), this.getCallDeclarationReference(), null,
				"rootTargetDataStruct", null, 0, 1, CallDataStructProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(newDataStructEClass, NewDataStruct.class, "NewDataStruct", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNewDataStruct_PropertyAssignments(), this.getDataStructPropertyAssignment(), null,
				"propertyAssignments", null, 0, -1, NewDataStruct.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(structValueEClass, StructValue.class, "StructValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStructValue_StructProperties(), this.getStructPropertyValue(), null, "structProperties", null,
				0, -1, StructValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(structPropertyValueEClass, StructPropertyValue.class, "StructPropertyValue", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStructPropertyValue_Value(), this.getValue(), null, "value", null, 0, 1,
				StructPropertyValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(logEClass, Log.class, "Log", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLog_Expression(), this.getExpression(), null, "expression", null, 0, -1, Log.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(directionKindEEnum, DirectionKind.class, "DirectionKind");
		addEEnumLiteral(directionKindEEnum, DirectionKind.IN_OUT);
		addEEnumLiteral(directionKindEEnum, DirectionKind.IN);
		addEEnumLiteral(directionKindEEnum, DirectionKind.OUT);

		initEEnum(binaryOperatorEEnum, BinaryOperator.class, "BinaryOperator");
		addEEnumLiteral(binaryOperatorEEnum, BinaryOperator.PLUS);
		addEEnumLiteral(binaryOperatorEEnum, BinaryOperator.MINUS);
		addEEnumLiteral(binaryOperatorEEnum, BinaryOperator.MULT);
		addEEnumLiteral(binaryOperatorEEnum, BinaryOperator.DIV);
		addEEnumLiteral(binaryOperatorEEnum, BinaryOperator.EQUAL);
		addEEnumLiteral(binaryOperatorEEnum, BinaryOperator.GREATER);
		addEEnumLiteral(binaryOperatorEEnum, BinaryOperator.LOWER);
		addEEnumLiteral(binaryOperatorEEnum, BinaryOperator.GREATEROREQUAL);
		addEEnumLiteral(binaryOperatorEEnum, BinaryOperator.LOWEROREQUAL);
		addEEnumLiteral(binaryOperatorEEnum, BinaryOperator.AND);
		addEEnumLiteral(binaryOperatorEEnum, BinaryOperator.OR);
		addEEnumLiteral(binaryOperatorEEnum, BinaryOperator.MOD);

		initEEnum(unaryOperatorEEnum, UnaryOperator.class, "UnaryOperator");
		addEEnumLiteral(unaryOperatorEEnum, UnaryOperator.UNARYMINUS);
		addEEnumLiteral(unaryOperatorEEnum, UnaryOperator.NOT);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// dse
		createDseAnnotations();
		// aspect
		createAspectAnnotations();
	}

	/**
	 * Initializes the annotations for <b>dse</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createDseAnnotations() {
		String source = "dse";
		addAnnotation(getFCLModel__PropagateDelayedResults(), source, new String[] {});
		addAnnotation(getFCLModel__EvaluateModeAutomata(), source, new String[] {});
		addAnnotation(getFunction__Dse_startEvalFunction(), source, new String[] {});
		addAnnotation(getFunction__Dse_stopEvalFunction(), source, new String[] {});
		addAnnotation(getFunction__IsEnabled(), source, new String[] {});
	}

	/**
	 * Initializes the annotations for <b>aspect</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createAspectAnnotations() {
		String source = "aspect";
		addAnnotation(getFCLModel_ReceivedEvents(), source, new String[] {});
		addAnnotation(getFCLModel_ProcedureStack(), source, new String[] {});
		addAnnotation(getFCLModel_Indentation(), source, new String[] {});
		addAnnotation(getModeAutomata_CurrentMode(), source, new String[] {});
		addAnnotation(getFunctionBlockDataPort_CurrentValue(), source, new String[] {});
		addAnnotation(getFunctionVarDecl_CurrentValue(), source, new String[] {});
		addAnnotation(getFMUFunction_FmuSimulationWrapper(), source, new String[] {});
		addAnnotation(getUnityFunction_UnityWrapper(), source, new String[] {});
	}

} //FclPackageImpl
