/**
 */
package fr.inria.glose.fcl.model.fcl.interpreter_vm;

import org.eclipse.emf.ecore.EObject;

import org.javafmi.wrapper.generic.Simulation2;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FMU Simulation Wrapper</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper#getFmiSimulation2 <em>Fmi Simulation2</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage#getFMUSimulationWrapper()
 * @model
 * @generated
 */
public interface FMUSimulationWrapper extends EObject {
	/**
	 * Returns the value of the '<em><b>Fmi Simulation2</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fmi Simulation2</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fmi Simulation2</em>' attribute.
	 * @see #setFmiSimulation2(Simulation2)
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage#getFMUSimulationWrapper_FmiSimulation2()
	 * @model dataType="fr.inria.glose.fcl.model.fcl.interpreter_vm.FMISimulation2"
	 * @generated
	 */
	Simulation2 getFmiSimulation2();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper#getFmiSimulation2 <em>Fmi Simulation2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fmi Simulation2</em>' attribute.
	 * @see #getFmiSimulation2()
	 * @generated
	 */
	void setFmiSimulation2(Simulation2 value);

} // FMUSimulationWrapper
