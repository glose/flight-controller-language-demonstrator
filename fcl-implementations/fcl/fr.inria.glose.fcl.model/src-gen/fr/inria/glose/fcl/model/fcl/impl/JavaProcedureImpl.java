/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.JavaProcedure;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Procedure</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.JavaProcedureImpl#getMethodFullQualifiedName <em>Method Full Qualified Name</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.JavaProcedureImpl#isStatic <em>Static</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JavaProcedureImpl extends ExternalProcedureImpl implements JavaProcedure {
	/**
	 * The default value of the '{@link #getMethodFullQualifiedName() <em>Method Full Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethodFullQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected static final String METHOD_FULL_QUALIFIED_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMethodFullQualifiedName() <em>Method Full Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethodFullQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected String methodFullQualifiedName = METHOD_FULL_QUALIFIED_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #isStatic() <em>Static</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStatic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean STATIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isStatic() <em>Static</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStatic()
	 * @generated
	 * @ordered
	 */
	protected boolean static_ = STATIC_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JavaProcedureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.JAVA_PROCEDURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMethodFullQualifiedName() {
		return methodFullQualifiedName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMethodFullQualifiedName(String newMethodFullQualifiedName) {
		String oldMethodFullQualifiedName = methodFullQualifiedName;
		methodFullQualifiedName = newMethodFullQualifiedName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.JAVA_PROCEDURE__METHOD_FULL_QUALIFIED_NAME,
					oldMethodFullQualifiedName, methodFullQualifiedName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isStatic() {
		return static_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatic(boolean newStatic) {
		boolean oldStatic = static_;
		static_ = newStatic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.JAVA_PROCEDURE__STATIC, oldStatic,
					static_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.JAVA_PROCEDURE__METHOD_FULL_QUALIFIED_NAME:
			return getMethodFullQualifiedName();
		case FclPackage.JAVA_PROCEDURE__STATIC:
			return isStatic();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.JAVA_PROCEDURE__METHOD_FULL_QUALIFIED_NAME:
			setMethodFullQualifiedName((String) newValue);
			return;
		case FclPackage.JAVA_PROCEDURE__STATIC:
			setStatic((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.JAVA_PROCEDURE__METHOD_FULL_QUALIFIED_NAME:
			setMethodFullQualifiedName(METHOD_FULL_QUALIFIED_NAME_EDEFAULT);
			return;
		case FclPackage.JAVA_PROCEDURE__STATIC:
			setStatic(STATIC_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.JAVA_PROCEDURE__METHOD_FULL_QUALIFIED_NAME:
			return METHOD_FULL_QUALIFIED_NAME_EDEFAULT == null ? methodFullQualifiedName != null
					: !METHOD_FULL_QUALIFIED_NAME_EDEFAULT.equals(methodFullQualifiedName);
		case FclPackage.JAVA_PROCEDURE__STATIC:
			return static_ != STATIC_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (methodFullQualifiedName: ");
		result.append(methodFullQualifiedName);
		result.append(", static: ");
		result.append(static_);
		result.append(')');
		return result.toString();
	}

} //JavaProcedureImpl
