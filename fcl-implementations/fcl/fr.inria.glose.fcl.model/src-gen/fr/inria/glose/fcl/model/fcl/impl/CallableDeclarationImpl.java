/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.CallableDeclaration;
import fr.inria.glose.fcl.model.fcl.FclPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Callable Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class CallableDeclarationImpl extends MinimalEObjectImpl.Container implements CallableDeclaration {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CallableDeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.CALLABLE_DECLARATION;
	}

} //CallableDeclarationImpl
