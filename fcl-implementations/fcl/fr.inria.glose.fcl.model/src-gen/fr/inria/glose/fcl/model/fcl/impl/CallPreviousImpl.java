/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.CallPrevious;
import fr.inria.glose.fcl.model.fcl.CallableDeclaration;
import fr.inria.glose.fcl.model.fcl.FclPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Call Previous</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.CallPreviousImpl#getCallable <em>Callable</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CallPreviousImpl extends CallImpl implements CallPrevious {
	/**
	 * The cached value of the '{@link #getCallable() <em>Callable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCallable()
	 * @generated
	 * @ordered
	 */
	protected CallableDeclaration callable;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CallPreviousImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.CALL_PREVIOUS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallableDeclaration getCallable() {
		if (callable != null && callable.eIsProxy()) {
			InternalEObject oldCallable = (InternalEObject) callable;
			callable = (CallableDeclaration) eResolveProxy(oldCallable);
			if (callable != oldCallable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FclPackage.CALL_PREVIOUS__CALLABLE,
							oldCallable, callable));
			}
		}
		return callable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallableDeclaration basicGetCallable() {
		return callable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCallable(CallableDeclaration newCallable) {
		CallableDeclaration oldCallable = callable;
		callable = newCallable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.CALL_PREVIOUS__CALLABLE, oldCallable,
					callable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.CALL_PREVIOUS__CALLABLE:
			if (resolve)
				return getCallable();
			return basicGetCallable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.CALL_PREVIOUS__CALLABLE:
			setCallable((CallableDeclaration) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.CALL_PREVIOUS__CALLABLE:
			setCallable((CallableDeclaration) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.CALL_PREVIOUS__CALLABLE:
			return callable != null;
		}
		return super.eIsSet(featureID);
	}

} //CallPreviousImpl
