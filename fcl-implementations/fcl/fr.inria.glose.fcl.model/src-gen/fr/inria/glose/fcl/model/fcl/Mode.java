/**
 */
package fr.inria.glose.fcl.model.fcl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mode</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.Mode#getEntry <em>Entry</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.Mode#getExit <em>Exit</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.Mode#isFinal <em>Final</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.Mode#getEnabledFunctions <em>Enabled Functions</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.Mode#getIncomingTransition <em>Incoming Transition</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.Mode#getOutgoingTransition <em>Outgoing Transition</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getMode()
 * @model
 * @generated
 */
public interface Mode extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry</em>' containment reference.
	 * @see #setEntry(Action)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getMode_Entry()
	 * @model containment="true"
	 * @generated
	 */
	Action getEntry();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.Mode#getEntry <em>Entry</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entry</em>' containment reference.
	 * @see #getEntry()
	 * @generated
	 */
	void setEntry(Action value);

	/**
	 * Returns the value of the '<em><b>Exit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exit</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exit</em>' containment reference.
	 * @see #setExit(Action)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getMode_Exit()
	 * @model containment="true"
	 * @generated
	 */
	Action getExit();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.Mode#getExit <em>Exit</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exit</em>' containment reference.
	 * @see #getExit()
	 * @generated
	 */
	void setExit(Action value);

	/**
	 * Returns the value of the '<em><b>Final</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final</em>' attribute.
	 * @see #setFinal(boolean)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getMode_Final()
	 * @model
	 * @generated
	 */
	boolean isFinal();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.Mode#isFinal <em>Final</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final</em>' attribute.
	 * @see #isFinal()
	 * @generated
	 */
	void setFinal(boolean value);

	/**
	 * Returns the value of the '<em><b>Enabled Functions</b></em>' reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.Function}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enabled Functions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enabled Functions</em>' reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getMode_EnabledFunctions()
	 * @model
	 * @generated
	 */
	EList<Function> getEnabledFunctions();

	/**
	 * Returns the value of the '<em><b>Incoming Transition</b></em>' reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.Transition}.
	 * It is bidirectional and its opposite is '{@link fr.inria.glose.fcl.model.fcl.Transition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming Transition</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming Transition</em>' reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getMode_IncomingTransition()
	 * @see fr.inria.glose.fcl.model.fcl.Transition#getTarget
	 * @model opposite="target" transient="true"
	 * @generated
	 */
	EList<Transition> getIncomingTransition();

	/**
	 * Returns the value of the '<em><b>Outgoing Transition</b></em>' reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.Transition}.
	 * It is bidirectional and its opposite is '{@link fr.inria.glose.fcl.model.fcl.Transition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Transition</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing Transition</em>' reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getMode_OutgoingTransition()
	 * @see fr.inria.glose.fcl.model.fcl.Transition#getSource
	 * @model opposite="source" transient="true"
	 * @generated
	 */
	EList<Transition> getOutgoingTransition();

} // Mode
