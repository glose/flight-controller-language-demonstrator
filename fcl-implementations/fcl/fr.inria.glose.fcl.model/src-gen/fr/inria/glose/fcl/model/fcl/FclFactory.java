/**
 */
package fr.inria.glose.fcl.model.fcl;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.inria.glose.fcl.model.fcl.FclPackage
 * @generated
 */
public interface FclFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FclFactory eINSTANCE = fr.inria.glose.fcl.model.fcl.impl.FclFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>FCL Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FCL Model</em>'.
	 * @generated
	 */
	FCLModel createFCLModel();

	/**
	 * Returns a new object of class '<em>Mode Automata</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mode Automata</em>'.
	 * @generated
	 */
	ModeAutomata createModeAutomata();

	/**
	 * Returns a new object of class '<em>Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transition</em>'.
	 * @generated
	 */
	Transition createTransition();

	/**
	 * Returns a new object of class '<em>Mode</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mode</em>'.
	 * @generated
	 */
	Mode createMode();

	/**
	 * Returns a new object of class '<em>Action Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Action Block</em>'.
	 * @generated
	 */
	ActionBlock createActionBlock();

	/**
	 * Returns a new object of class '<em>Data Struct Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Struct Type</em>'.
	 * @generated
	 */
	DataStructType createDataStructType();

	/**
	 * Returns a new object of class '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Named Element</em>'.
	 * @generated
	 */
	NamedElement createNamedElement();

	/**
	 * Returns a new object of class '<em>Function Block Data Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Block Data Port</em>'.
	 * @generated
	 */
	FunctionBlockDataPort createFunctionBlockDataPort();

	/**
	 * Returns a new object of class '<em>Procedure Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Procedure Parameter</em>'.
	 * @generated
	 */
	ProcedureParameter createProcedureParameter();

	/**
	 * Returns a new object of class '<em>Function Connector</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Connector</em>'.
	 * @generated
	 */
	FunctionConnector createFunctionConnector();

	/**
	 * Returns a new object of class '<em>External Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>External Event</em>'.
	 * @generated
	 */
	ExternalEvent createExternalEvent();

	/**
	 * Returns a new object of class '<em>Cast</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cast</em>'.
	 * @generated
	 */
	Cast createCast();

	/**
	 * Returns a new object of class '<em>Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assignment</em>'.
	 * @generated
	 */
	Assignment createAssignment();

	/**
	 * Returns a new object of class '<em>If Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If Action</em>'.
	 * @generated
	 */
	IfAction createIfAction();

	/**
	 * Returns a new object of class '<em>While Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>While Action</em>'.
	 * @generated
	 */
	WhileAction createWhileAction();

	/**
	 * Returns a new object of class '<em>Procedure Call</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Procedure Call</em>'.
	 * @generated
	 */
	ProcedureCall createProcedureCall();

	/**
	 * Returns a new object of class '<em>Var Decl</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Var Decl</em>'.
	 * @generated
	 */
	VarDecl createVarDecl();

	/**
	 * Returns a new object of class '<em>Call Declaration Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Call Declaration Reference</em>'.
	 * @generated
	 */
	CallDeclarationReference createCallDeclarationReference();

	/**
	 * Returns a new object of class '<em>Function Port Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Port Reference</em>'.
	 * @generated
	 */
	FunctionPortReference createFunctionPortReference();

	/**
	 * Returns a new object of class '<em>Basic Operator Unary Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Basic Operator Unary Expression</em>'.
	 * @generated
	 */
	BasicOperatorUnaryExpression createBasicOperatorUnaryExpression();

	/**
	 * Returns a new object of class '<em>Basic Operator Binary Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Basic Operator Binary Expression</em>'.
	 * @generated
	 */
	BasicOperatorBinaryExpression createBasicOperatorBinaryExpression();

	/**
	 * Returns a new object of class '<em>Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value Type</em>'.
	 * @generated
	 */
	ValueType createValueType();

	/**
	 * Returns a new object of class '<em>Integer Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Integer Value Type</em>'.
	 * @generated
	 */
	IntegerValueType createIntegerValueType();

	/**
	 * Returns a new object of class '<em>Float Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Float Value Type</em>'.
	 * @generated
	 */
	FloatValueType createFloatValueType();

	/**
	 * Returns a new object of class '<em>Boolean Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean Value Type</em>'.
	 * @generated
	 */
	BooleanValueType createBooleanValueType();

	/**
	 * Returns a new object of class '<em>String Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Value Type</em>'.
	 * @generated
	 */
	StringValueType createStringValueType();

	/**
	 * Returns a new object of class '<em>Enumeration Value Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Enumeration Value Type</em>'.
	 * @generated
	 */
	EnumerationValueType createEnumerationValueType();

	/**
	 * Returns a new object of class '<em>Enumeration Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Enumeration Literal</em>'.
	 * @generated
	 */
	EnumerationLiteral createEnumerationLiteral();

	/**
	 * Returns a new object of class '<em>Function</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function</em>'.
	 * @generated
	 */
	Function createFunction();

	/**
	 * Returns a new object of class '<em>Function Time Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Time Reference</em>'.
	 * @generated
	 */
	FunctionTimeReference createFunctionTimeReference();

	/**
	 * Returns a new object of class '<em>FCL Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FCL Procedure</em>'.
	 * @generated
	 */
	FCLProcedure createFCLProcedure();

	/**
	 * Returns a new object of class '<em>FCL Procedure Return</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FCL Procedure Return</em>'.
	 * @generated
	 */
	FCLProcedureReturn createFCLProcedureReturn();

	/**
	 * Returns a new object of class '<em>Java Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Java Procedure</em>'.
	 * @generated
	 */
	JavaProcedure createJavaProcedure();

	/**
	 * Returns a new object of class '<em>Integer Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Integer Value</em>'.
	 * @generated
	 */
	IntegerValue createIntegerValue();

	/**
	 * Returns a new object of class '<em>Float Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Float Value</em>'.
	 * @generated
	 */
	FloatValue createFloatValue();

	/**
	 * Returns a new object of class '<em>String Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Value</em>'.
	 * @generated
	 */
	StringValue createStringValue();

	/**
	 * Returns a new object of class '<em>Boolean Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean Value</em>'.
	 * @generated
	 */
	BooleanValue createBooleanValue();

	/**
	 * Returns a new object of class '<em>Function Var Decl</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Var Decl</em>'.
	 * @generated
	 */
	FunctionVarDecl createFunctionVarDecl();

	/**
	 * Returns a new object of class '<em>FMU Function</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FMU Function</em>'.
	 * @generated
	 */
	FMUFunction createFMUFunction();

	/**
	 * Returns a new object of class '<em>Call Constant</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Call Constant</em>'.
	 * @generated
	 */
	CallConstant createCallConstant();

	/**
	 * Returns a new object of class '<em>Enumeration Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Enumeration Value</em>'.
	 * @generated
	 */
	EnumerationValue createEnumerationValue();

	/**
	 * Returns a new object of class '<em>FMU Function Block Data Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FMU Function Block Data Port</em>'.
	 * @generated
	 */
	<T> FMUFunctionBlockDataPort<T> createFMUFunctionBlockDataPort();

	/**
	 * Returns a new object of class '<em>Unity Function</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unity Function</em>'.
	 * @generated
	 */
	UnityFunction createUnityFunction();

	/**
	 * Returns a new object of class '<em>Unity Function Data Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unity Function Data Port</em>'.
	 * @generated
	 */
	UnityFunctionDataPort createUnityFunctionDataPort();

	/**
	 * Returns a new object of class '<em>Call Previous</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Call Previous</em>'.
	 * @generated
	 */
	CallPrevious createCallPrevious();

	/**
	 * Returns a new object of class '<em>Data Struct Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Struct Property</em>'.
	 * @generated
	 */
	DataStructProperty createDataStructProperty();

	/**
	 * Returns a new object of class '<em>Data Struct Property Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Struct Property Assignment</em>'.
	 * @generated
	 */
	DataStructPropertyAssignment createDataStructPropertyAssignment();

	/**
	 * Returns a new object of class '<em>Call Data Struct Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Call Data Struct Property</em>'.
	 * @generated
	 */
	CallDataStructProperty createCallDataStructProperty();

	/**
	 * Returns a new object of class '<em>New Data Struct</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>New Data Struct</em>'.
	 * @generated
	 */
	NewDataStruct createNewDataStruct();

	/**
	 * Returns a new object of class '<em>Struct Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Struct Value</em>'.
	 * @generated
	 */
	StructValue createStructValue();

	/**
	 * Returns a new object of class '<em>Struct Property Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Struct Property Value</em>'.
	 * @generated
	 */
	StructPropertyValue createStructPropertyValue();

	/**
	 * Returns a new object of class '<em>Log</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Log</em>'.
	 * @generated
	 */
	Log createLog();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	FclPackage getFclPackage();

} //FclFactory
