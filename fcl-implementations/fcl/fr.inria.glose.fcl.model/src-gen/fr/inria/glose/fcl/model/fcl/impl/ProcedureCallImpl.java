/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.DataType;
import fr.inria.glose.fcl.model.fcl.Expression;
import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.Procedure;
import fr.inria.glose.fcl.model.fcl.ProcedureCall;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Procedure Call</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.ProcedureCallImpl#getResultType <em>Result Type</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.ProcedureCallImpl#getProcedure <em>Procedure</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.ProcedureCallImpl#getProcedureCallArguments <em>Procedure Call Arguments</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProcedureCallImpl extends PrimitiveActionImpl implements ProcedureCall {
	/**
	 * The cached value of the '{@link #getResultType() <em>Result Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResultType()
	 * @generated
	 * @ordered
	 */
	protected DataType resultType;

	/**
	 * The cached value of the '{@link #getProcedure() <em>Procedure</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcedure()
	 * @generated
	 * @ordered
	 */
	protected Procedure procedure;

	/**
	 * The cached value of the '{@link #getProcedureCallArguments() <em>Procedure Call Arguments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcedureCallArguments()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> procedureCallArguments;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcedureCallImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.PROCEDURE_CALL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType getResultType() {
		if (resultType != null && resultType.eIsProxy()) {
			InternalEObject oldResultType = (InternalEObject) resultType;
			resultType = (DataType) eResolveProxy(oldResultType);
			if (resultType != oldResultType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FclPackage.PROCEDURE_CALL__RESULT_TYPE,
							oldResultType, resultType));
			}
		}
		return resultType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType basicGetResultType() {
		return resultType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResultType(DataType newResultType) {
		DataType oldResultType = resultType;
		resultType = newResultType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.PROCEDURE_CALL__RESULT_TYPE, oldResultType,
					resultType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure getProcedure() {
		if (procedure != null && procedure.eIsProxy()) {
			InternalEObject oldProcedure = (InternalEObject) procedure;
			procedure = (Procedure) eResolveProxy(oldProcedure);
			if (procedure != oldProcedure) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FclPackage.PROCEDURE_CALL__PROCEDURE,
							oldProcedure, procedure));
			}
		}
		return procedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure basicGetProcedure() {
		return procedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcedure(Procedure newProcedure) {
		Procedure oldProcedure = procedure;
		procedure = newProcedure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.PROCEDURE_CALL__PROCEDURE, oldProcedure,
					procedure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expression> getProcedureCallArguments() {
		if (procedureCallArguments == null) {
			procedureCallArguments = new EObjectContainmentEList<Expression>(Expression.class, this,
					FclPackage.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS);
		}
		return procedureCallArguments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FclPackage.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS:
			return ((InternalEList<?>) getProcedureCallArguments()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.PROCEDURE_CALL__RESULT_TYPE:
			if (resolve)
				return getResultType();
			return basicGetResultType();
		case FclPackage.PROCEDURE_CALL__PROCEDURE:
			if (resolve)
				return getProcedure();
			return basicGetProcedure();
		case FclPackage.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS:
			return getProcedureCallArguments();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.PROCEDURE_CALL__RESULT_TYPE:
			setResultType((DataType) newValue);
			return;
		case FclPackage.PROCEDURE_CALL__PROCEDURE:
			setProcedure((Procedure) newValue);
			return;
		case FclPackage.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS:
			getProcedureCallArguments().clear();
			getProcedureCallArguments().addAll((Collection<? extends Expression>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.PROCEDURE_CALL__RESULT_TYPE:
			setResultType((DataType) null);
			return;
		case FclPackage.PROCEDURE_CALL__PROCEDURE:
			setProcedure((Procedure) null);
			return;
		case FclPackage.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS:
			getProcedureCallArguments().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.PROCEDURE_CALL__RESULT_TYPE:
			return resultType != null;
		case FclPackage.PROCEDURE_CALL__PROCEDURE:
			return procedure != null;
		case FclPackage.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS:
			return procedureCallArguments != null && !procedureCallArguments.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Expression.class) {
			switch (derivedFeatureID) {
			case FclPackage.PROCEDURE_CALL__RESULT_TYPE:
				return FclPackage.EXPRESSION__RESULT_TYPE;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Expression.class) {
			switch (baseFeatureID) {
			case FclPackage.EXPRESSION__RESULT_TYPE:
				return FclPackage.PROCEDURE_CALL__RESULT_TYPE;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ProcedureCallImpl
