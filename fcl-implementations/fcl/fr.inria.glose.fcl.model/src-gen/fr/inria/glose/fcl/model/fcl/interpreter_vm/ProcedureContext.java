/**
 */
package fr.inria.glose.fcl.model.fcl.interpreter_vm;

import fr.inria.glose.fcl.model.fcl.Value;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Procedure Context</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext#getDeclarationMapEntries <em>Declaration Map Entries</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext#getResultValue <em>Result Value</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage#getProcedureContext()
 * @model
 * @generated
 */
public interface ProcedureContext extends EObject {
	/**
	 * Returns the value of the '<em><b>Declaration Map Entries</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.interpreter_vm.DeclarationMapEntry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Declaration Map Entries</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Declaration Map Entries</em>' containment reference list.
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage#getProcedureContext_DeclarationMapEntries()
	 * @model containment="true"
	 * @generated
	 */
	EList<DeclarationMapEntry> getDeclarationMapEntries();

	/**
	 * Returns the value of the '<em><b>Result Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result Value</em>' containment reference.
	 * @see #setResultValue(Value)
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage#getProcedureContext_ResultValue()
	 * @model containment="true"
	 * @generated
	 */
	Value getResultValue();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext#getResultValue <em>Result Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result Value</em>' containment reference.
	 * @see #getResultValue()
	 * @generated
	 */
	void setResultValue(Value value);

} // ProcedureContext
