/**
 */
package fr.inria.glose.fcl.model.fcl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * invariant:
 * ((not subFunctions.isEmpty or not functionConnectors.isempty) implies action == null)
 * and
 * ( action != null implies  (subFunctions.isEmpty and functionConnectors.isempty) )
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.Function#getFunctionPorts <em>Function Ports</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.Function#getAction <em>Action</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.Function#getSubFunctions <em>Sub Functions</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.Function#getFunctionConnectors <em>Function Connectors</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.Function#getFunctionVarDecls <em>Function Var Decls</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.Function#getTimeReference <em>Time Reference</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunction()
 * @model
 * @generated
 */
public interface Function extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Function Ports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.FunctionPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Ports</em>' containment reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunction_FunctionPorts()
	 * @model containment="true"
	 * @generated
	 */
	EList<FunctionPort> getFunctionPorts();

	/**
	 * Returns the value of the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' containment reference.
	 * @see #setAction(Action)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunction_Action()
	 * @model containment="true"
	 * @generated
	 */
	Action getAction();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.Function#getAction <em>Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' containment reference.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(Action value);

	/**
	 * Returns the value of the '<em><b>Sub Functions</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.Function}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Functions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Functions</em>' containment reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunction_SubFunctions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Function> getSubFunctions();

	/**
	 * Returns the value of the '<em><b>Function Connectors</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.FunctionConnector}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Connectors</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Connectors</em>' containment reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunction_FunctionConnectors()
	 * @model containment="true"
	 * @generated
	 */
	EList<FunctionConnector> getFunctionConnectors();

	/**
	 * Returns the value of the '<em><b>Function Var Decls</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.FunctionVarDecl}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Var Decls</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Var Decls</em>' containment reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunction_FunctionVarDecls()
	 * @model containment="true"
	 * @generated
	 */
	EList<FunctionVarDecl> getFunctionVarDecls();

	/**
	 * Returns the value of the '<em><b>Time Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Reference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Reference</em>' containment reference.
	 * @see #setTimeReference(FunctionTimeReference)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFunction_TimeReference()
	 * @model containment="true"
	 * @generated
	 */
	FunctionTimeReference getTimeReference();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.Function#getTimeReference <em>Time Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Reference</em>' containment reference.
	 * @see #getTimeReference()
	 * @generated
	 */
	void setTimeReference(FunctionTimeReference value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<Function> allSubFunctions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void dse_startEvalFunction();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void dse_stopEvalFunction();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isEnabled();

} // Function
