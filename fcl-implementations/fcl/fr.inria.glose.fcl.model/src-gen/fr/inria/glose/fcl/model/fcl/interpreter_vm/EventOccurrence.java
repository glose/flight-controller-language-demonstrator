/**
 */
package fr.inria.glose.fcl.model.fcl.interpreter_vm;

import fr.inria.glose.fcl.model.fcl.Event;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Occurrence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.EventOccurrence#getEvent <em>Event</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage#getEventOccurrence()
 * @model
 * @generated
 */
public interface EventOccurrence extends EObject {
	/**
	 * Returns the value of the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' reference.
	 * @see #setEvent(Event)
	 * @see fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage#getEventOccurrence_Event()
	 * @model
	 * @generated
	 */
	Event getEvent();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.interpreter_vm.EventOccurrence#getEvent <em>Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' reference.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(Event value);

} // EventOccurrence
