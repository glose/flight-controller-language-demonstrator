/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FclFactoryImpl extends EFactoryImpl implements FclFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FclFactory init() {
		try {
			FclFactory theFclFactory = (FclFactory) EPackage.Registry.INSTANCE.getEFactory(FclPackage.eNS_URI);
			if (theFclFactory != null) {
				return theFclFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FclFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FclFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case FclPackage.FCL_MODEL:
			return createFCLModel();
		case FclPackage.MODE_AUTOMATA:
			return createModeAutomata();
		case FclPackage.TRANSITION:
			return createTransition();
		case FclPackage.MODE:
			return createMode();
		case FclPackage.ACTION_BLOCK:
			return createActionBlock();
		case FclPackage.DATA_STRUCT_TYPE:
			return createDataStructType();
		case FclPackage.NAMED_ELEMENT:
			return createNamedElement();
		case FclPackage.FUNCTION_BLOCK_DATA_PORT:
			return createFunctionBlockDataPort();
		case FclPackage.PROCEDURE_PARAMETER:
			return createProcedureParameter();
		case FclPackage.FUNCTION_CONNECTOR:
			return createFunctionConnector();
		case FclPackage.EXTERNAL_EVENT:
			return createExternalEvent();
		case FclPackage.CAST:
			return createCast();
		case FclPackage.ASSIGNMENT:
			return createAssignment();
		case FclPackage.IF_ACTION:
			return createIfAction();
		case FclPackage.WHILE_ACTION:
			return createWhileAction();
		case FclPackage.PROCEDURE_CALL:
			return createProcedureCall();
		case FclPackage.VAR_DECL:
			return createVarDecl();
		case FclPackage.CALL_DECLARATION_REFERENCE:
			return createCallDeclarationReference();
		case FclPackage.FUNCTION_PORT_REFERENCE:
			return createFunctionPortReference();
		case FclPackage.BASIC_OPERATOR_UNARY_EXPRESSION:
			return createBasicOperatorUnaryExpression();
		case FclPackage.BASIC_OPERATOR_BINARY_EXPRESSION:
			return createBasicOperatorBinaryExpression();
		case FclPackage.VALUE_TYPE:
			return createValueType();
		case FclPackage.INTEGER_VALUE_TYPE:
			return createIntegerValueType();
		case FclPackage.FLOAT_VALUE_TYPE:
			return createFloatValueType();
		case FclPackage.BOOLEAN_VALUE_TYPE:
			return createBooleanValueType();
		case FclPackage.STRING_VALUE_TYPE:
			return createStringValueType();
		case FclPackage.ENUMERATION_VALUE_TYPE:
			return createEnumerationValueType();
		case FclPackage.ENUMERATION_LITERAL:
			return createEnumerationLiteral();
		case FclPackage.FUNCTION:
			return createFunction();
		case FclPackage.FUNCTION_TIME_REFERENCE:
			return createFunctionTimeReference();
		case FclPackage.FCL_PROCEDURE:
			return createFCLProcedure();
		case FclPackage.FCL_PROCEDURE_RETURN:
			return createFCLProcedureReturn();
		case FclPackage.JAVA_PROCEDURE:
			return createJavaProcedure();
		case FclPackage.INTEGER_VALUE:
			return createIntegerValue();
		case FclPackage.FLOAT_VALUE:
			return createFloatValue();
		case FclPackage.STRING_VALUE:
			return createStringValue();
		case FclPackage.BOOLEAN_VALUE:
			return createBooleanValue();
		case FclPackage.FUNCTION_VAR_DECL:
			return createFunctionVarDecl();
		case FclPackage.CALL_CONSTANT:
			return createCallConstant();
		case FclPackage.ENUMERATION_VALUE:
			return createEnumerationValue();
		case FclPackage.FMU_FUNCTION:
			return createFMUFunction();
		case FclPackage.FMU_FUNCTION_BLOCK_DATA_PORT:
			return createFMUFunctionBlockDataPort();
		case FclPackage.UNITY_FUNCTION:
			return createUnityFunction();
		case FclPackage.UNITY_FUNCTION_DATA_PORT:
			return createUnityFunctionDataPort();
		case FclPackage.CALL_PREVIOUS:
			return createCallPrevious();
		case FclPackage.DATA_STRUCT_PROPERTY:
			return createDataStructProperty();
		case FclPackage.DATA_STRUCT_PROPERTY_ASSIGNMENT:
			return createDataStructPropertyAssignment();
		case FclPackage.CALL_DATA_STRUCT_PROPERTY:
			return createCallDataStructProperty();
		case FclPackage.NEW_DATA_STRUCT:
			return createNewDataStruct();
		case FclPackage.STRUCT_VALUE:
			return createStructValue();
		case FclPackage.STRUCT_PROPERTY_VALUE:
			return createStructPropertyValue();
		case FclPackage.LOG:
			return createLog();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case FclPackage.DIRECTION_KIND:
			return createDirectionKindFromString(eDataType, initialValue);
		case FclPackage.BINARY_OPERATOR:
			return createBinaryOperatorFromString(eDataType, initialValue);
		case FclPackage.UNARY_OPERATOR:
			return createUnaryOperatorFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case FclPackage.DIRECTION_KIND:
			return convertDirectionKindToString(eDataType, instanceValue);
		case FclPackage.BINARY_OPERATOR:
			return convertBinaryOperatorToString(eDataType, instanceValue);
		case FclPackage.UNARY_OPERATOR:
			return convertUnaryOperatorToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FCLModel createFCLModel() {
		FCLModelImpl fclModel = new FCLModelImpl();
		return fclModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModeAutomata createModeAutomata() {
		ModeAutomataImpl modeAutomata = new ModeAutomataImpl();
		return modeAutomata;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition createTransition() {
		TransitionImpl transition = new TransitionImpl();
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mode createMode() {
		ModeImpl mode = new ModeImpl();
		return mode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionBlock createActionBlock() {
		ActionBlockImpl actionBlock = new ActionBlockImpl();
		return actionBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataStructType createDataStructType() {
		DataStructTypeImpl dataStructType = new DataStructTypeImpl();
		return dataStructType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedElement createNamedElement() {
		NamedElementImpl namedElement = new NamedElementImpl();
		return namedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionBlockDataPort createFunctionBlockDataPort() {
		FunctionBlockDataPortImpl functionBlockDataPort = new FunctionBlockDataPortImpl();
		return functionBlockDataPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureParameter createProcedureParameter() {
		ProcedureParameterImpl procedureParameter = new ProcedureParameterImpl();
		return procedureParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionConnector createFunctionConnector() {
		FunctionConnectorImpl functionConnector = new FunctionConnectorImpl();
		return functionConnector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalEvent createExternalEvent() {
		ExternalEventImpl externalEvent = new ExternalEventImpl();
		return externalEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cast createCast() {
		CastImpl cast = new CastImpl();
		return cast;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assignment createAssignment() {
		AssignmentImpl assignment = new AssignmentImpl();
		return assignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfAction createIfAction() {
		IfActionImpl ifAction = new IfActionImpl();
		return ifAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhileAction createWhileAction() {
		WhileActionImpl whileAction = new WhileActionImpl();
		return whileAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureCall createProcedureCall() {
		ProcedureCallImpl procedureCall = new ProcedureCallImpl();
		return procedureCall;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VarDecl createVarDecl() {
		VarDeclImpl varDecl = new VarDeclImpl();
		return varDecl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallDeclarationReference createCallDeclarationReference() {
		CallDeclarationReferenceImpl callDeclarationReference = new CallDeclarationReferenceImpl();
		return callDeclarationReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionPortReference createFunctionPortReference() {
		FunctionPortReferenceImpl functionPortReference = new FunctionPortReferenceImpl();
		return functionPortReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicOperatorUnaryExpression createBasicOperatorUnaryExpression() {
		BasicOperatorUnaryExpressionImpl basicOperatorUnaryExpression = new BasicOperatorUnaryExpressionImpl();
		return basicOperatorUnaryExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicOperatorBinaryExpression createBasicOperatorBinaryExpression() {
		BasicOperatorBinaryExpressionImpl basicOperatorBinaryExpression = new BasicOperatorBinaryExpressionImpl();
		return basicOperatorBinaryExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueType createValueType() {
		ValueTypeImpl valueType = new ValueTypeImpl();
		return valueType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerValueType createIntegerValueType() {
		IntegerValueTypeImpl integerValueType = new IntegerValueTypeImpl();
		return integerValueType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatValueType createFloatValueType() {
		FloatValueTypeImpl floatValueType = new FloatValueTypeImpl();
		return floatValueType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanValueType createBooleanValueType() {
		BooleanValueTypeImpl booleanValueType = new BooleanValueTypeImpl();
		return booleanValueType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringValueType createStringValueType() {
		StringValueTypeImpl stringValueType = new StringValueTypeImpl();
		return stringValueType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationValueType createEnumerationValueType() {
		EnumerationValueTypeImpl enumerationValueType = new EnumerationValueTypeImpl();
		return enumerationValueType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteral createEnumerationLiteral() {
		EnumerationLiteralImpl enumerationLiteral = new EnumerationLiteralImpl();
		return enumerationLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Function createFunction() {
		FunctionImpl function = new FunctionImpl();
		return function;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionTimeReference createFunctionTimeReference() {
		FunctionTimeReferenceImpl functionTimeReference = new FunctionTimeReferenceImpl();
		return functionTimeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FCLProcedure createFCLProcedure() {
		FCLProcedureImpl fclProcedure = new FCLProcedureImpl();
		return fclProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FCLProcedureReturn createFCLProcedureReturn() {
		FCLProcedureReturnImpl fclProcedureReturn = new FCLProcedureReturnImpl();
		return fclProcedureReturn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaProcedure createJavaProcedure() {
		JavaProcedureImpl javaProcedure = new JavaProcedureImpl();
		return javaProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerValue createIntegerValue() {
		IntegerValueImpl integerValue = new IntegerValueImpl();
		return integerValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatValue createFloatValue() {
		FloatValueImpl floatValue = new FloatValueImpl();
		return floatValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringValue createStringValue() {
		StringValueImpl stringValue = new StringValueImpl();
		return stringValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanValue createBooleanValue() {
		BooleanValueImpl booleanValue = new BooleanValueImpl();
		return booleanValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionVarDecl createFunctionVarDecl() {
		FunctionVarDeclImpl functionVarDecl = new FunctionVarDeclImpl();
		return functionVarDecl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FMUFunction createFMUFunction() {
		FMUFunctionImpl fmuFunction = new FMUFunctionImpl();
		return fmuFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallConstant createCallConstant() {
		CallConstantImpl callConstant = new CallConstantImpl();
		return callConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationValue createEnumerationValue() {
		EnumerationValueImpl enumerationValue = new EnumerationValueImpl();
		return enumerationValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public <T> FMUFunctionBlockDataPort<T> createFMUFunctionBlockDataPort() {
		FMUFunctionBlockDataPortImpl<T> fmuFunctionBlockDataPort = new FMUFunctionBlockDataPortImpl<T>();
		return fmuFunctionBlockDataPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnityFunction createUnityFunction() {
		UnityFunctionImpl unityFunction = new UnityFunctionImpl();
		return unityFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnityFunctionDataPort createUnityFunctionDataPort() {
		UnityFunctionDataPortImpl unityFunctionDataPort = new UnityFunctionDataPortImpl();
		return unityFunctionDataPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallPrevious createCallPrevious() {
		CallPreviousImpl callPrevious = new CallPreviousImpl();
		return callPrevious;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataStructProperty createDataStructProperty() {
		DataStructPropertyImpl dataStructProperty = new DataStructPropertyImpl();
		return dataStructProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataStructPropertyAssignment createDataStructPropertyAssignment() {
		DataStructPropertyAssignmentImpl dataStructPropertyAssignment = new DataStructPropertyAssignmentImpl();
		return dataStructPropertyAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallDataStructProperty createCallDataStructProperty() {
		CallDataStructPropertyImpl callDataStructProperty = new CallDataStructPropertyImpl();
		return callDataStructProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NewDataStruct createNewDataStruct() {
		NewDataStructImpl newDataStruct = new NewDataStructImpl();
		return newDataStruct;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StructValue createStructValue() {
		StructValueImpl structValue = new StructValueImpl();
		return structValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StructPropertyValue createStructPropertyValue() {
		StructPropertyValueImpl structPropertyValue = new StructPropertyValueImpl();
		return structPropertyValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Log createLog() {
		LogImpl log = new LogImpl();
		return log;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectionKind createDirectionKindFromString(EDataType eDataType, String initialValue) {
		DirectionKind result = DirectionKind.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDirectionKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryOperator createBinaryOperatorFromString(EDataType eDataType, String initialValue) {
		BinaryOperator result = BinaryOperator.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBinaryOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryOperator createUnaryOperatorFromString(EDataType eDataType, String initialValue) {
		UnaryOperator result = UnaryOperator.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertUnaryOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FclPackage getFclPackage() {
		return (FclPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FclPackage getPackage() {
		return FclPackage.eINSTANCE;
	}

} //FclFactoryImpl
