/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Struct Property Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment#getExpression <em>Expression</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment#getAssignable <em>Assignable</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getDataStructPropertyAssignment()
 * @model
 * @generated
 */
public interface DataStructPropertyAssignment extends DataStructPropertyReference, PrimitiveAction {
	/**
	 * Returns the value of the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' containment reference.
	 * @see #setExpression(Expression)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getDataStructPropertyAssignment_Expression()
	 * @model containment="true"
	 * @generated
	 */
	Expression getExpression();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' containment reference.
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(Expression value);

	/**
	 * Returns the value of the '<em><b>Assignable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assignable</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assignable</em>' reference.
	 * @see #setAssignable(Assignable)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getDataStructPropertyAssignment_Assignable()
	 * @model
	 * @generated
	 */
	Assignable getAssignable();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment#getAssignable <em>Assignable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assignable</em>' reference.
	 * @see #getAssignable()
	 * @generated
	 */
	void setAssignable(Assignable value);

} // DataStructPropertyAssignment
