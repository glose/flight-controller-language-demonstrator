/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.Expression;
import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;
import fr.inria.glose.fcl.model.fcl.FunctionVarDecl;
import fr.inria.glose.fcl.model.fcl.Value;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Block Data Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FunctionBlockDataPortImpl#getDefaultInputValue <em>Default Input Value</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FunctionBlockDataPortImpl#getFunctionVarStore <em>Function Var Store</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.FunctionBlockDataPortImpl#getCurrentValue <em>Current Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionBlockDataPortImpl extends FunctionPortImpl implements FunctionBlockDataPort {
	/**
	 * The cached value of the '{@link #getDefaultInputValue() <em>Default Input Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultInputValue()
	 * @generated
	 * @ordered
	 */
	protected Expression defaultInputValue;

	/**
	 * The cached value of the '{@link #getFunctionVarStore() <em>Function Var Store</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionVarStore()
	 * @generated
	 * @ordered
	 */
	protected FunctionVarDecl functionVarStore;

	/**
	 * The cached value of the '{@link #getCurrentValue() <em>Current Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentValue()
	 * @generated
	 * @ordered
	 */
	protected Value currentValue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionBlockDataPortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.FUNCTION_BLOCK_DATA_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getDefaultInputValue() {
		return defaultInputValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefaultInputValue(Expression newDefaultInputValue, NotificationChain msgs) {
		Expression oldDefaultInputValue = defaultInputValue;
		defaultInputValue = newDefaultInputValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					FclPackage.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE, oldDefaultInputValue,
					newDefaultInputValue);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultInputValue(Expression newDefaultInputValue) {
		if (newDefaultInputValue != defaultInputValue) {
			NotificationChain msgs = null;
			if (defaultInputValue != null)
				msgs = ((InternalEObject) defaultInputValue).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE, null, msgs);
			if (newDefaultInputValue != null)
				msgs = ((InternalEObject) newDefaultInputValue).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - FclPackage.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE, null, msgs);
			msgs = basicSetDefaultInputValue(newDefaultInputValue, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FclPackage.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE, newDefaultInputValue,
					newDefaultInputValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionVarDecl getFunctionVarStore() {
		if (functionVarStore != null && functionVarStore.eIsProxy()) {
			InternalEObject oldFunctionVarStore = (InternalEObject) functionVarStore;
			functionVarStore = (FunctionVarDecl) eResolveProxy(oldFunctionVarStore);
			if (functionVarStore != oldFunctionVarStore) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							FclPackage.FUNCTION_BLOCK_DATA_PORT__FUNCTION_VAR_STORE, oldFunctionVarStore,
							functionVarStore));
			}
		}
		return functionVarStore;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionVarDecl basicGetFunctionVarStore() {
		return functionVarStore;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionVarStore(FunctionVarDecl newFunctionVarStore) {
		FunctionVarDecl oldFunctionVarStore = functionVarStore;
		functionVarStore = newFunctionVarStore;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					FclPackage.FUNCTION_BLOCK_DATA_PORT__FUNCTION_VAR_STORE, oldFunctionVarStore, functionVarStore));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getCurrentValue() {
		if (currentValue != null && currentValue.eIsProxy()) {
			InternalEObject oldCurrentValue = (InternalEObject) currentValue;
			currentValue = (Value) eResolveProxy(oldCurrentValue);
			if (currentValue != oldCurrentValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							FclPackage.FUNCTION_BLOCK_DATA_PORT__CURRENT_VALUE, oldCurrentValue, currentValue));
			}
		}
		return currentValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value basicGetCurrentValue() {
		return currentValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentValue(Value newCurrentValue) {
		Value oldCurrentValue = currentValue;
		currentValue = newCurrentValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FclPackage.FUNCTION_BLOCK_DATA_PORT__CURRENT_VALUE,
					oldCurrentValue, currentValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FclPackage.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE:
			return basicSetDefaultInputValue(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE:
			return getDefaultInputValue();
		case FclPackage.FUNCTION_BLOCK_DATA_PORT__FUNCTION_VAR_STORE:
			if (resolve)
				return getFunctionVarStore();
			return basicGetFunctionVarStore();
		case FclPackage.FUNCTION_BLOCK_DATA_PORT__CURRENT_VALUE:
			if (resolve)
				return getCurrentValue();
			return basicGetCurrentValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE:
			setDefaultInputValue((Expression) newValue);
			return;
		case FclPackage.FUNCTION_BLOCK_DATA_PORT__FUNCTION_VAR_STORE:
			setFunctionVarStore((FunctionVarDecl) newValue);
			return;
		case FclPackage.FUNCTION_BLOCK_DATA_PORT__CURRENT_VALUE:
			setCurrentValue((Value) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE:
			setDefaultInputValue((Expression) null);
			return;
		case FclPackage.FUNCTION_BLOCK_DATA_PORT__FUNCTION_VAR_STORE:
			setFunctionVarStore((FunctionVarDecl) null);
			return;
		case FclPackage.FUNCTION_BLOCK_DATA_PORT__CURRENT_VALUE:
			setCurrentValue((Value) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE:
			return defaultInputValue != null;
		case FclPackage.FUNCTION_BLOCK_DATA_PORT__FUNCTION_VAR_STORE:
			return functionVarStore != null;
		case FclPackage.FUNCTION_BLOCK_DATA_PORT__CURRENT_VALUE:
			return currentValue != null;
		}
		return super.eIsSet(featureID);
	}

} //FunctionBlockDataPortImpl
