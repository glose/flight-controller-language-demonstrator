/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Primitive Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getPrimitiveAction()
 * @model abstract="true"
 * @generated
 */
public interface PrimitiveAction extends Action {
} // PrimitiveAction
