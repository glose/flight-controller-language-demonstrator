/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Value Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getBooleanValueType()
 * @model
 * @generated
 */
public interface BooleanValueType extends ValueType {
} // BooleanValueType
