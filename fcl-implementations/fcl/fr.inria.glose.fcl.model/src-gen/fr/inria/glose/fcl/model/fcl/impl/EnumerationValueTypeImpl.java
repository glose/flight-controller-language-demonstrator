/**
 */
package fr.inria.glose.fcl.model.fcl.impl;

import fr.inria.glose.fcl.model.fcl.EnumerationLiteral;
import fr.inria.glose.fcl.model.fcl.EnumerationValueType;
import fr.inria.glose.fcl.model.fcl.FclPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Enumeration Value Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.impl.EnumerationValueTypeImpl#getEnumerationLiteral <em>Enumeration Literal</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnumerationValueTypeImpl extends ValueTypeImpl implements EnumerationValueType {
	/**
	 * The cached value of the '{@link #getEnumerationLiteral() <em>Enumeration Literal</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnumerationLiteral()
	 * @generated
	 * @ordered
	 */
	protected EList<EnumerationLiteral> enumerationLiteral;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnumerationValueTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FclPackage.Literals.ENUMERATION_VALUE_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EnumerationLiteral> getEnumerationLiteral() {
		if (enumerationLiteral == null) {
			enumerationLiteral = new EObjectContainmentEList<EnumerationLiteral>(EnumerationLiteral.class, this,
					FclPackage.ENUMERATION_VALUE_TYPE__ENUMERATION_LITERAL);
		}
		return enumerationLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FclPackage.ENUMERATION_VALUE_TYPE__ENUMERATION_LITERAL:
			return ((InternalEList<?>) getEnumerationLiteral()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FclPackage.ENUMERATION_VALUE_TYPE__ENUMERATION_LITERAL:
			return getEnumerationLiteral();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FclPackage.ENUMERATION_VALUE_TYPE__ENUMERATION_LITERAL:
			getEnumerationLiteral().clear();
			getEnumerationLiteral().addAll((Collection<? extends EnumerationLiteral>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FclPackage.ENUMERATION_VALUE_TYPE__ENUMERATION_LITERAL:
			getEnumerationLiteral().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FclPackage.ENUMERATION_VALUE_TYPE__ENUMERATION_LITERAL:
			return enumerationLiteral != null && !enumerationLiteral.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EnumerationValueTypeImpl
