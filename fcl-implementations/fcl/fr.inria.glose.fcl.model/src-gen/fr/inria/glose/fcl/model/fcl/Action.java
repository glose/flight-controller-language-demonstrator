/**
 */
package fr.inria.glose.fcl.model.fcl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getAction()
 * @model abstract="true"
 * @generated
 */
public interface Action extends EObject {
} // Action
