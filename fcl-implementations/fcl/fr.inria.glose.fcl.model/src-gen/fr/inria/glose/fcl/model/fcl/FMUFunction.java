/**
 */
package fr.inria.glose.fcl.model.fcl;

import fr.inria.glose.fcl.model.fcl.interpreter_vm.FMUSimulationWrapper;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FMU Function</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FMUFunction#getRunnableFmuPath <em>Runnable Fmu Path</em>}</li>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.FMUFunction#getFmuSimulationWrapper <em>Fmu Simulation Wrapper</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFMUFunction()
 * @model
 * @generated
 */
public interface FMUFunction extends Function {
	/**
	 * Returns the value of the '<em><b>Runnable Fmu Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runnable Fmu Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runnable Fmu Path</em>' attribute.
	 * @see #setRunnableFmuPath(String)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFMUFunction_RunnableFmuPath()
	 * @model
	 * @generated
	 */
	String getRunnableFmuPath();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.FMUFunction#getRunnableFmuPath <em>Runnable Fmu Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Runnable Fmu Path</em>' attribute.
	 * @see #getRunnableFmuPath()
	 * @generated
	 */
	void setRunnableFmuPath(String value);

	/**
	 * Returns the value of the '<em><b>Fmu Simulation Wrapper</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fmu Simulation Wrapper</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fmu Simulation Wrapper</em>' reference.
	 * @see #setFmuSimulationWrapper(FMUSimulationWrapper)
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getFMUFunction_FmuSimulationWrapper()
	 * @model
	 * @generated
	 */
	FMUSimulationWrapper getFmuSimulationWrapper();

	/**
	 * Sets the value of the '{@link fr.inria.glose.fcl.model.fcl.FMUFunction#getFmuSimulationWrapper <em>Fmu Simulation Wrapper</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fmu Simulation Wrapper</em>' reference.
	 * @see #getFmuSimulationWrapper()
	 * @generated
	 */
	void setFmuSimulationWrapper(FMUSimulationWrapper value);

} // FMUFunction
