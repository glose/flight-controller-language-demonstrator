/**
 */
package fr.inria.glose.fcl.model.fcl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enumeration Value Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.EnumerationValueType#getEnumerationLiteral <em>Enumeration Literal</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getEnumerationValueType()
 * @model
 * @generated
 */
public interface EnumerationValueType extends ValueType {
	/**
	 * Returns the value of the '<em><b>Enumeration Literal</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.EnumerationLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Literal</em>' containment reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getEnumerationValueType_EnumerationLiteral()
	 * @model containment="true"
	 * @generated
	 */
	EList<EnumerationLiteral> getEnumerationLiteral();

} // EnumerationValueType
