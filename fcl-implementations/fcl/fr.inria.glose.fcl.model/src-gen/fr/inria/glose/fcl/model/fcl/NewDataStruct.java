/**
 */
package fr.inria.glose.fcl.model.fcl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>New Data Struct</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.NewDataStruct#getPropertyAssignments <em>Property Assignments</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getNewDataStruct()
 * @model
 * @generated
 */
public interface NewDataStruct extends Expression {
	/**
	 * Returns the value of the '<em><b>Property Assignments</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.DataStructPropertyAssignment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Assignments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Assignments</em>' containment reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getNewDataStruct_PropertyAssignments()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataStructPropertyAssignment> getPropertyAssignments();

} // NewDataStruct
