/**
 */
package fr.inria.glose.fcl.model.fcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getCall()
 * @model abstract="true"
 * @generated
 */
public interface Call extends Expression {
} // Call
