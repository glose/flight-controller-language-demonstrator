/**
 */
package fr.inria.glose.fcl.model.fcl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Struct Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.fcl.model.fcl.StructValue#getStructProperties <em>Struct Properties</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getStructValue()
 * @model
 * @generated
 */
public interface StructValue extends Value {
	/**
	 * Returns the value of the '<em><b>Struct Properties</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.fcl.model.fcl.StructPropertyValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Struct Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Struct Properties</em>' containment reference list.
	 * @see fr.inria.glose.fcl.model.fcl.FclPackage#getStructValue_StructProperties()
	 * @model containment="true"
	 * @generated
	 */
	EList<StructPropertyValue> getStructProperties();

} // StructValue
