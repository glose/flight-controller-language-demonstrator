/**
 */
package fr.inria.glose.fcl.model.fcl.provider;

import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.IfAction;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.inria.glose.fcl.model.fcl.IfAction} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class IfActionItemProvider extends ControlStructureActionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfActionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(FclPackage.Literals.IF_ACTION__CONDITION);
			childrenFeatures.add(FclPackage.Literals.IF_ACTION__THEN_ACTION);
			childrenFeatures.add(FclPackage.Literals.IF_ACTION__ELSE_ACTION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns IfAction.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/IfAction"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_IfAction_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(IfAction.class)) {
		case FclPackage.IF_ACTION__CONDITION:
		case FclPackage.IF_ACTION__THEN_ACTION:
		case FclPackage.IF_ACTION__ELSE_ACTION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(FclPackage.Literals.IF_ACTION__CONDITION, FclFactory.eINSTANCE.createCast()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__CONDITION,
				FclFactory.eINSTANCE.createProcedureCall()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__CONDITION,
				FclFactory.eINSTANCE.createCallDeclarationReference()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__CONDITION,
				FclFactory.eINSTANCE.createBasicOperatorUnaryExpression()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__CONDITION,
				FclFactory.eINSTANCE.createBasicOperatorBinaryExpression()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__CONDITION,
				FclFactory.eINSTANCE.createCallConstant()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__CONDITION,
				FclFactory.eINSTANCE.createCallPrevious()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__CONDITION,
				FclFactory.eINSTANCE.createCallDataStructProperty()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__CONDITION,
				FclFactory.eINSTANCE.createNewDataStruct()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__THEN_ACTION,
				FclFactory.eINSTANCE.createActionBlock()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__THEN_ACTION,
				FclFactory.eINSTANCE.createAssignment()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__THEN_ACTION,
				FclFactory.eINSTANCE.createIfAction()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__THEN_ACTION,
				FclFactory.eINSTANCE.createWhileAction()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__THEN_ACTION,
				FclFactory.eINSTANCE.createProcedureCall()));

		newChildDescriptors.add(
				createChildParameter(FclPackage.Literals.IF_ACTION__THEN_ACTION, FclFactory.eINSTANCE.createVarDecl()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__THEN_ACTION,
				FclFactory.eINSTANCE.createFCLProcedureReturn()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__THEN_ACTION,
				FclFactory.eINSTANCE.createDataStructPropertyAssignment()));

		newChildDescriptors.add(
				createChildParameter(FclPackage.Literals.IF_ACTION__THEN_ACTION, FclFactory.eINSTANCE.createLog()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__ELSE_ACTION,
				FclFactory.eINSTANCE.createActionBlock()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__ELSE_ACTION,
				FclFactory.eINSTANCE.createAssignment()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__ELSE_ACTION,
				FclFactory.eINSTANCE.createIfAction()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__ELSE_ACTION,
				FclFactory.eINSTANCE.createWhileAction()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__ELSE_ACTION,
				FclFactory.eINSTANCE.createProcedureCall()));

		newChildDescriptors.add(
				createChildParameter(FclPackage.Literals.IF_ACTION__ELSE_ACTION, FclFactory.eINSTANCE.createVarDecl()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__ELSE_ACTION,
				FclFactory.eINSTANCE.createFCLProcedureReturn()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.IF_ACTION__ELSE_ACTION,
				FclFactory.eINSTANCE.createDataStructPropertyAssignment()));

		newChildDescriptors.add(
				createChildParameter(FclPackage.Literals.IF_ACTION__ELSE_ACTION, FclFactory.eINSTANCE.createLog()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify = childFeature == FclPackage.Literals.IF_ACTION__CONDITION
				|| childFeature == FclPackage.Literals.IF_ACTION__THEN_ACTION
				|| childFeature == FclPackage.Literals.IF_ACTION__ELSE_ACTION;

		if (qualify) {
			return getString("_UI_CreateChild_text2",
					new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
