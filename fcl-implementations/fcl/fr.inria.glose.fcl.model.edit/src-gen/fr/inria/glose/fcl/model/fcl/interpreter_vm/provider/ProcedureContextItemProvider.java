/**
 */
package fr.inria.glose.fcl.model.fcl.interpreter_vm.provider;

import fr.inria.glose.fcl.model.fcl.FclFactory;

import fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmFactory;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.Interpreter_vmPackage;
import fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext;

import fr.inria.glose.fcl.model.fcl.provider.FclEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.inria.glose.fcl.model.fcl.interpreter_vm.ProcedureContext} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ProcedureContextItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureContextItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(Interpreter_vmPackage.Literals.PROCEDURE_CONTEXT__DECLARATION_MAP_ENTRIES);
			childrenFeatures.add(Interpreter_vmPackage.Literals.PROCEDURE_CONTEXT__RESULT_VALUE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ProcedureContext.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ProcedureContext"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_ProcedureContext_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ProcedureContext.class)) {
		case Interpreter_vmPackage.PROCEDURE_CONTEXT__DECLARATION_MAP_ENTRIES:
		case Interpreter_vmPackage.PROCEDURE_CONTEXT__RESULT_VALUE:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors
				.add(createChildParameter(Interpreter_vmPackage.Literals.PROCEDURE_CONTEXT__DECLARATION_MAP_ENTRIES,
						Interpreter_vmFactory.eINSTANCE.createDeclarationMapEntry()));

		newChildDescriptors.add(createChildParameter(Interpreter_vmPackage.Literals.PROCEDURE_CONTEXT__RESULT_VALUE,
				FclFactory.eINSTANCE.createIntegerValue()));

		newChildDescriptors.add(createChildParameter(Interpreter_vmPackage.Literals.PROCEDURE_CONTEXT__RESULT_VALUE,
				FclFactory.eINSTANCE.createFloatValue()));

		newChildDescriptors.add(createChildParameter(Interpreter_vmPackage.Literals.PROCEDURE_CONTEXT__RESULT_VALUE,
				FclFactory.eINSTANCE.createStringValue()));

		newChildDescriptors.add(createChildParameter(Interpreter_vmPackage.Literals.PROCEDURE_CONTEXT__RESULT_VALUE,
				FclFactory.eINSTANCE.createBooleanValue()));

		newChildDescriptors.add(createChildParameter(Interpreter_vmPackage.Literals.PROCEDURE_CONTEXT__RESULT_VALUE,
				FclFactory.eINSTANCE.createEnumerationValue()));

		newChildDescriptors.add(createChildParameter(Interpreter_vmPackage.Literals.PROCEDURE_CONTEXT__RESULT_VALUE,
				FclFactory.eINSTANCE.createStructValue()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return FclEditPlugin.INSTANCE;
	}

}
