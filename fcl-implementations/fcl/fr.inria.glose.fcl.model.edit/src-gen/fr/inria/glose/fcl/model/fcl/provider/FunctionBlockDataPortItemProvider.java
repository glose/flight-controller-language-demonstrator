/**
 */
package fr.inria.glose.fcl.model.fcl.provider;

import fr.inria.glose.fcl.model.fcl.DirectionKind;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.inria.glose.fcl.model.fcl.FunctionBlockDataPort} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FunctionBlockDataPortItemProvider extends FunctionPortItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionBlockDataPortItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addDefaultInputValuePropertyDescriptor(object);
			addFunctionVarStorePropertyDescriptor(object);
			addCurrentValuePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Default Input Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDefaultInputValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_FunctionBlockDataPort_defaultInputValue_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_FunctionBlockDataPort_defaultInputValue_feature",
						"_UI_FunctionBlockDataPort_type"),
				FclPackage.Literals.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE, true, false, true, null, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Function Var Store feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFunctionVarStorePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_FunctionBlockDataPort_functionVarStore_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_FunctionBlockDataPort_functionVarStore_feature",
						"_UI_FunctionBlockDataPort_type"),
				FclPackage.Literals.FUNCTION_BLOCK_DATA_PORT__FUNCTION_VAR_STORE, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Current Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCurrentValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_FunctionBlockDataPort_currentValue_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_FunctionBlockDataPort_currentValue_feature",
						"_UI_FunctionBlockDataPort_type"),
				FclPackage.Literals.FUNCTION_BLOCK_DATA_PORT__CURRENT_VALUE, true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(FclPackage.Literals.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns FunctionBlockDataPort.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		if (object instanceof FunctionBlockDataPort) {
			FunctionBlockDataPort o = (FunctionBlockDataPort) object;
			if (o.getDirection().equals(DirectionKind.IN)) {
				return overlayImage(object, getResourceLocator().getImage("full/obj16/FunctionBlockDataPort_in"));
			}

			if (o.getDirection().equals(DirectionKind.OUT)) {
				return overlayImage(object, getResourceLocator().getImage("full/obj16/FunctionBlockDataPort_out"));
			}
		}
		return overlayImage(object, getResourceLocator().getImage("full/obj16/FunctionBlockDataPort"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((FunctionBlockDataPort) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_FunctionBlockDataPort_type")
				: getString("_UI_FunctionBlockDataPort_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(FunctionBlockDataPort.class)) {
		case FclPackage.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE,
				FclFactory.eINSTANCE.createCast()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE,
				FclFactory.eINSTANCE.createProcedureCall()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE,
				FclFactory.eINSTANCE.createCallDeclarationReference()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE,
				FclFactory.eINSTANCE.createBasicOperatorUnaryExpression()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE,
				FclFactory.eINSTANCE.createBasicOperatorBinaryExpression()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE,
				FclFactory.eINSTANCE.createCallConstant()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE,
				FclFactory.eINSTANCE.createCallPrevious()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE,
				FclFactory.eINSTANCE.createCallDataStructProperty()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FUNCTION_BLOCK_DATA_PORT__DEFAULT_INPUT_VALUE,
				FclFactory.eINSTANCE.createNewDataStruct()));
	}

}
