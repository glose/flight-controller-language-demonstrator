/**
 */
package fr.inria.glose.fcl.model.fcl.provider;

import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.FclPackage;
import fr.inria.glose.fcl.model.fcl.ProcedureCall;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.inria.glose.fcl.model.fcl.ProcedureCall} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ProcedureCallItemProvider extends PrimitiveActionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureCallItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addResultTypePropertyDescriptor(object);
			addProcedurePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Result Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addResultTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_Expression_resultType_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_Expression_resultType_feature",
								"_UI_Expression_type"),
						FclPackage.Literals.EXPRESSION__RESULT_TYPE, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Procedure feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProcedurePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ProcedureCall_procedure_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ProcedureCall_procedure_feature",
								"_UI_ProcedureCall_type"),
						FclPackage.Literals.PROCEDURE_CALL__PROCEDURE, true, false, true, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(FclPackage.Literals.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ProcedureCall.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ProcedureCall"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_ProcedureCall_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ProcedureCall.class)) {
		case FclPackage.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS,
				FclFactory.eINSTANCE.createCast()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS,
				FclFactory.eINSTANCE.createProcedureCall()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS,
				FclFactory.eINSTANCE.createCallDeclarationReference()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS,
				FclFactory.eINSTANCE.createBasicOperatorUnaryExpression()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS,
				FclFactory.eINSTANCE.createBasicOperatorBinaryExpression()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS,
				FclFactory.eINSTANCE.createCallConstant()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS,
				FclFactory.eINSTANCE.createCallPrevious()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS,
				FclFactory.eINSTANCE.createCallDataStructProperty()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.PROCEDURE_CALL__PROCEDURE_CALL_ARGUMENTS,
				FclFactory.eINSTANCE.createNewDataStruct()));
	}

}
