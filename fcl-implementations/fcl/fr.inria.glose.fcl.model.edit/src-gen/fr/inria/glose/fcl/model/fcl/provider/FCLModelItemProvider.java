/**
 */
package fr.inria.glose.fcl.model.fcl.provider;

import fr.inria.glose.fcl.model.fcl.FCLModel;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.FclPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.inria.glose.fcl.model.fcl.FCLModel} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FCLModelItemProvider extends NamedElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FCLModelItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addReceivedEventsPropertyDescriptor(object);
			addProcedureStackPropertyDescriptor(object);
			addIndentationPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Received Events feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReceivedEventsPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_FCLModel_receivedEvents_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_FCLModel_receivedEvents_feature",
								"_UI_FCLModel_type"),
						FclPackage.Literals.FCL_MODEL__RECEIVED_EVENTS, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Procedure Stack feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProcedureStackPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_FCLModel_procedureStack_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_FCLModel_procedureStack_feature",
								"_UI_FCLModel_type"),
						FclPackage.Literals.FCL_MODEL__PROCEDURE_STACK, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Indentation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIndentationPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_FCLModel_indentation_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_FCLModel_indentation_feature",
								"_UI_FCLModel_type"),
						FclPackage.Literals.FCL_MODEL__INDENTATION, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(FclPackage.Literals.FCL_MODEL__MODE_AUTOMATA);
			childrenFeatures.add(FclPackage.Literals.FCL_MODEL__EVENTS);
			childrenFeatures.add(FclPackage.Literals.FCL_MODEL__PROCEDURES);
			childrenFeatures.add(FclPackage.Literals.FCL_MODEL__DATA_TYPES);
			childrenFeatures.add(FclPackage.Literals.FCL_MODEL__DATAFLOW);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns FCLModel.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/FCLModel"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((FCLModel) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_FCLModel_type")
				: getString("_UI_FCLModel_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(FCLModel.class)) {
		case FclPackage.FCL_MODEL__INDENTATION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case FclPackage.FCL_MODEL__MODE_AUTOMATA:
		case FclPackage.FCL_MODEL__EVENTS:
		case FclPackage.FCL_MODEL__PROCEDURES:
		case FclPackage.FCL_MODEL__DATA_TYPES:
		case FclPackage.FCL_MODEL__DATAFLOW:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_MODEL__MODE_AUTOMATA,
				FclFactory.eINSTANCE.createModeAutomata()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_MODEL__EVENTS,
				FclFactory.eINSTANCE.createExternalEvent()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_MODEL__PROCEDURES,
				FclFactory.eINSTANCE.createFCLProcedure()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_MODEL__PROCEDURES,
				FclFactory.eINSTANCE.createJavaProcedure()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_MODEL__DATA_TYPES,
				FclFactory.eINSTANCE.createDataStructType()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_MODEL__DATA_TYPES,
				FclFactory.eINSTANCE.createValueType()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_MODEL__DATA_TYPES,
				FclFactory.eINSTANCE.createIntegerValueType()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_MODEL__DATA_TYPES,
				FclFactory.eINSTANCE.createFloatValueType()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_MODEL__DATA_TYPES,
				FclFactory.eINSTANCE.createBooleanValueType()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_MODEL__DATA_TYPES,
				FclFactory.eINSTANCE.createStringValueType()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_MODEL__DATA_TYPES,
				FclFactory.eINSTANCE.createEnumerationValueType()));

		newChildDescriptors.add(
				createChildParameter(FclPackage.Literals.FCL_MODEL__DATAFLOW, FclFactory.eINSTANCE.createFunction()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_MODEL__DATAFLOW,
				FclFactory.eINSTANCE.createFMUFunction()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_MODEL__DATAFLOW,
				FclFactory.eINSTANCE.createUnityFunction()));
	}

}
