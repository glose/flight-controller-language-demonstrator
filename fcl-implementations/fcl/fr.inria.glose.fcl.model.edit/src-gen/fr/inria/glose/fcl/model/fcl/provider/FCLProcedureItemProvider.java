/**
 */
package fr.inria.glose.fcl.model.fcl.provider;

import fr.inria.glose.fcl.model.fcl.FCLProcedure;
import fr.inria.glose.fcl.model.fcl.FclFactory;
import fr.inria.glose.fcl.model.fcl.FclPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.inria.glose.fcl.model.fcl.FCLProcedure} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FCLProcedureItemProvider extends ProcedureItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FCLProcedureItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(FclPackage.Literals.FCL_PROCEDURE__ACTION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns FCLProcedure.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/FCLProcedure"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((FCLProcedure) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_FCLProcedure_type")
				: getString("_UI_FCLProcedure_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(FCLProcedure.class)) {
		case FclPackage.FCL_PROCEDURE__ACTION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_PROCEDURE__ACTION,
				FclFactory.eINSTANCE.createActionBlock()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_PROCEDURE__ACTION,
				FclFactory.eINSTANCE.createAssignment()));

		newChildDescriptors.add(
				createChildParameter(FclPackage.Literals.FCL_PROCEDURE__ACTION, FclFactory.eINSTANCE.createIfAction()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_PROCEDURE__ACTION,
				FclFactory.eINSTANCE.createWhileAction()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_PROCEDURE__ACTION,
				FclFactory.eINSTANCE.createProcedureCall()));

		newChildDescriptors.add(
				createChildParameter(FclPackage.Literals.FCL_PROCEDURE__ACTION, FclFactory.eINSTANCE.createVarDecl()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_PROCEDURE__ACTION,
				FclFactory.eINSTANCE.createFCLProcedureReturn()));

		newChildDescriptors.add(createChildParameter(FclPackage.Literals.FCL_PROCEDURE__ACTION,
				FclFactory.eINSTANCE.createDataStructPropertyAssignment()));

		newChildDescriptors
				.add(createChildParameter(FclPackage.Literals.FCL_PROCEDURE__ACTION, FclFactory.eINSTANCE.createLog()));
	}

}
