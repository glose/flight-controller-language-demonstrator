package unity;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.browser.ProgressAdapter;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class UnityBrowser {
	
	private Browser browser;
	
	public void run(String location) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setText("Unity Visualization");
		shell.setSize(1024, 728);
		shell.setLayout(new FillLayout());
		
		this.browser = new Browser(shell, SWT.NONE);
		this.browser.setUrl("file://" + location + "/unity/webgl/index.html");
		
		shell.open();
		
		float i = 0f;
		while (!shell.isDisposed()) {
	        if (!display.readAndDispatch()) {
	            display.sleep();
	        }
	        i = (i + 1f) % 360;
	        this.setCoordinates(10, 10, 10, i, i, i);
		}
		display.dispose();
	}
	
	public void setCoordinates(float x, float y, float z, float tx, float ty, float tz) {
		this.browser.execute("unityInstance.SendMessage('Drone', 'setX', " + x + ");\n"
				+ "unityInstance.SendMessage('Drone', 'setY', " + y + ");\n"
				+ "unityInstance.SendMessage('Drone', 'setZ', " + z + ");\n"
				+ "unityInstance.SendMessage('Drone', 'setTX', " + tx + ");\n"
				+ "unityInstance.SendMessage('Drone', 'setTY', " + ty + ");\n"
				+ "unityInstance.SendMessage('Drone', 'setTZ', " + tz + ");");
	}
	
	public static void main(String args[]) {
		UnityBrowser browser = new UnityBrowser();
		browser.run("/home/dvojtise/git/gitlab_glose/flight-controller-language-demonstrator/misc/");
	}
	
}
