FlightControllerModel BareUnity {
	mainModeStateMachine {
		mode SimpleMode {
			enabledFunctions (
				UnityOutput
			)
		}
		final mode Stopped{}
		initialMode SimpleMode
		
		transition  SimpleMode -> Stopped {
			when powerButtonPressed
		}
	}
	mainFunction {
		ports {
			dataPort in x : Position defaultValue 0.0 // north-south in ?
			dataPort in y : Position defaultValue 0.0 // west-east
			dataPort in z : Position defaultValue 0.0 // altitude
			dataPort in az 	: Angle defaultValue 0.0  // yaw in degree
			dataPort in ay 	: Angle defaultValue 0.0  // pitch in degree
			dataPort in ax 	: Angle  defaultValue 0.0 // roll in degree
		}
		variables {
			var Time globalClock := 0.0
		}
		timeReference {
			increment 0.01
			observableVar globalClock
		}
		dataFlow {

			unityFunction UnityOutput {
				unityWebPagePath "/bare-unity/webgl/index.html"
				ports {
					
					dataPort in x : Position nodeName "Drone" varName "Z"
					dataPort in y : Position nodeName "Drone" varName "X"
					dataPort in z : Position nodeName "Drone" varName "Y"
					dataPort in az 	: Angle nodeName "Drone" varName "TY"
					dataPort in ay 	: Angle  nodeName "Drone" varName "TX" 
					dataPort in ax 	: Angle nodeName "Drone" varName "TZ"
				}
			}

			connect x <-> UnityOutput.x
			connect y <-> UnityOutput.y
			connect z <-> UnityOutput.z
			connect ax <-> UnityOutput.ax
			connect az <-> UnityOutput.az
			connect ay <-> UnityOutput.ay
		}
	}
	events {
		external powerButtonPressed
		//external powerButtonReleased
		external RCCommandReceived
	}
	
	dataTypes {
		BooleanValueType Boolean {
			description "generic boolean"
		}
		IntegerValueType Integer {
			description "generic integer"
		}
		FloatValueType Float {
			description "generic float"
		}
		FloatValueType Time {
			description "unit: second"
		}
		FloatValueType Percentage {
			description "unit percentage, min 0, max 100"
		}
		IntegerValueType JoystickInput {
			description "min -100, max 100"
		}
		FloatValueType Position {
			description "x, y or z, unit: meter"
		}
		FloatValueType Angle {
			description "attitude, unit, rad"
		}
		FloatValueType EnginePower {
			description "unit percentage, min 0, max 100"
		}
		// 1st derivative
		FloatValueType AngularVelocity {
			description "unit: rad-per-second"
		}
		FloatValueType Speed {
			description "unit:meter-per-second"
		}
		// 2nd derivative		
		FloatValueType Acceleration {
			description "unit:meter-per-second-per-second"
		}
		FloatValueType AngularAcceleration {
			description "unit:rad-per-second-per-second"
		}

	}
}
