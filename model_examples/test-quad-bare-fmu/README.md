In order to run this exemple you need a valid Dymola licence and also indicate it in your environment variable.

To run this demo, launch the modeling workbench with the DYMOLA_RUNTIME_LICENSE variable set in the environment tab of the launch configuration

it mustpoint to your licence file (ex: $HOME/.dynasim/dymola.lic)

If the license is not found the execution will fail during the initializeModel method and a message will be displayed in the Default MessagingSystem console
